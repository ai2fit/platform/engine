Singularity Engine
=============================

Easy-to-use visual environment for predictive analytics. No programming required. AI2FIT is easily the most powerful
and intuitive graphical user interface for the design of analysis processes. Forget sifting through code! You can also
choose to run in batch mode. Whatever you prefer, AI2FIT has it all.

This project contains the source of [Singularity Engine](https://gitlab.oldworldcomputing.com/singularity/engine).

## Getting Started

* [Install](https://gitlab.oldworldcomputing.com/singularity/engine) Singularity Engine
* Have questions? Check out our official [community](https://ai2fit.org/)
  and [documentation](https://ai2fit.org/)

## Singularity Engine as Dependency

Using Gradle:

```groovy
apply plugin: 'java'

repositories {
    maven {
        name 'owc-releases'
        url 'https://repo.oldworldcomputing.com/repository/releases/'
        // credentials should be defined in your ~/.gradle/gradle.properties
        credentials {
            username project.mavenUser
            password project.mavenPass
        }
    }
}

dependencies {
    implementaion group: 'com.owc.singularity', name: 'engine', version: '+'
}
```

Using Maven:

```xml

<project>
    ...
    <repositories>
        <repository>
            <id>owc-releases</id>
            <url>https://repo.oldworldcomputing.com/repository/releases/</url>
        </repository>
    </repositories>
    ...
    <dependency>
        <groupId>com.owc.singularity</groupId>
        <artifactId>engine</artifactId>
        <version>LATEST</version>
    </dependency>
    ...
</project>
```

## Build Singularity Engine from Source

1. Clone singularity-engine using [git](https://git-scm.com/) into a folder named `singularity-engine`
2. Execute `gradlew jar`
3. The jar file will be located under __build/libs__

Please have in mind that the jar file still require all dependencies listed in the [build.gradle](build.gradle) file.

## Import Singularity Engine into your IDE

1. Your IDE has to support Gradle projects.
    1. Install [Gradle 2.3+](https://gradle.org/gradle-download/)
    2. Install and configure a Gradle plugin for your IDE
2. Import _singularity-engine_ as a Gradle project

### Start the Singularity Engine GUI

To start the graphical user interface of Singularity Engine creates a new `GuiLauncher.java` file in __src/main/java__
and run it with your IDE. If you want to use the generated jar, add the jar and all dependencies to the Java class
path `java -cp "all;required;jars" GuiLauncher`. You can list the runtime dependencies by
executing `gradlew dependencies --configuration runtime`.

```java
import com.owc.singularity;

class GuiLauncher {
    public static void main(String args[]) throws Exception {
        System.setProperty(PlatformUtilities.PROPERTY_SINGULARITY_HOME, Paths.get("").toAbsolutePath().toString());
        SingularityStudio.main(args);
    }
}
```

### Run Singularity Engine in CLI mode

**Prerequisite**: Start the Singularity Engine GUI at least once and accept the EULA.

To run Singularity Engine in command line mode create a new `CliLauncher.java` file in __src/main/java__ with the
following content:

```java
import com.owc.singularity;
import com.owc.singularity.engine.concurrency;

class CliLauncher {
    public static void main(String args[]) throws Exception {
        System.setProperty(PlatformUtilities.PROPERTY_SINGULARITY_HOME, Paths.get("").toAbsolutePath().toString());
        SingularityEngine.setExecutionMode(EngineExecutionMode.COMMAND_LINE);
        SingularityEngine.init(new LocalConcurrencyExecutionService());
    }
}
```

## Maintainer
- (Sebastian Land)[https://gitlab.com/sebastianLand] 
