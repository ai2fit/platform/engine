package com.owc.singularity.studio.gui.tools;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.owc.singularity.tools.VersionNumber;

class VersionNumberTest {

    @Test
    void equalityTests() {
        VersionNumber self = new VersionNumber(2, 1, 0);
        VersionNumber other = new VersionNumber(2, 1, 0);
        assertEquals(self, other);
        assertEquals(self, self);
        assertEquals(self, new VersionNumber(2, 1, 0, ""));
        assertEquals(self, new VersionNumber(2, 1, 0, null));
        assertEquals(self, new VersionNumber("2.1.0"));

        assertEquals(new VersionNumber("2.1.0"), new VersionNumber("2.1.0"));
        assertEquals(new VersionNumber("2.1.0"), new VersionNumber("2.1"));
        assertEquals(new VersionNumber("2.0.0"), new VersionNumber("2"));
        assertEquals(new VersionNumber("0.4.3-001-ge114b7f-SNAPSHOT"), new VersionNumber("0.4.3-001-ge114b7f-SNAPSHOT"));
    }

    @Test
    void compareWithoutClassifier() {
        VersionNumber self = new VersionNumber(5, 5, 5);
        assertThat(self, is(greaterThan(new VersionNumber(5, 5, 4))));
        assertThat(self, is(greaterThan(new VersionNumber(5, 4, 4))));
        assertThat(self, is(greaterThan(new VersionNumber(4, 4, 4))));

        assertThat(self, is(greaterThan(new VersionNumber(5, 4, 5))));
        assertThat(self, is(greaterThan(new VersionNumber(4, 5, 5))));
    }

    @Test
    void compareWithDevClassifier() {
        VersionNumber self = new VersionNumber(5, 5, 5, "019-WIP");
        assertThat(self, is(greaterThan(new VersionNumber(5, 5, 5))));
        assertThat(self, is(greaterThan(new VersionNumber(5, 5, 5, "019"))));

        assertThat(self, is(lessThan(new VersionNumber(5, 5, 5, "020-WIP"))));

        VersionNumber test = new VersionNumber(5, 5, 6);
        assertThat(test, is(greaterThan(new VersionNumber(5, 5, 5, "000-WIP"))));
        assertThat(test, is(lessThan(new VersionNumber(5, 5, 6, "000-WIP"))));
    }
}
