/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.security;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.EngineExecutionMode;
import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.tools.cipher.CipherTools;
import com.owc.singularity.engine.tools.cipher.KeyGenerationException;
import com.owc.singularity.engine.tools.cipher.KeyGeneratorTool;


/**
 * @author Andreas Timm
 * @since 8.1
 */
public class WalletTest {

    private static File tmpRMuserDir;
    private static String originalUserHome;
    private static String secretsXmlContentBefore;

    @BeforeAll
    public static void setup() throws KeyGenerationException, IOException {
        originalUserHome = System.getProperty("user.home");

        File tmpDir = File.createTempFile("wallet", "test");
        tmpDir.delete();
        tmpDir.mkdir();
        tmpRMuserDir = new File(tmpDir, FileSystemService.SINGULARITY_USER_FOLDER);
        tmpRMuserDir.mkdir();
        System.setProperty("user.home", tmpDir.getAbsolutePath());

        SingularityEngine.setExecutionMode(EngineExecutionMode.TEST);

        if (!CipherTools.isKeyAvailable()) {
            KeyGeneratorTool.storeKey(KeyGeneratorTool.createSecretKey().getEncoded(), new File(tmpRMuserDir, "cipher.key").toPath());
        }

        URL resource = WalletTest.class.getResource("secrets.xml");
        File newSecretsxml = new File(tmpRMuserDir, "secrets.xml");
        OutputStream fos = new FileOutputStream(newSecretsxml);
        copy(resource.openStream(), fos);
        fos.close();

        secretsXmlContentBefore = readFile(new File(tmpRMuserDir, "secrets.xml").getAbsolutePath(), Charset.defaultCharset());
    }

    @AfterAll
    public static void cleanUp() {
        System.setProperty("user.home", originalUserHome);
    }

    @Test
    @Disabled("until we find a better way for encrypting wallet credentials")
    public void testReadSecureStorage() throws IOException, TransformerException {
        // expecting migration while loading so the content should have changed
        String secretsXmlContentAfter = readFile(new File(tmpRMuserDir, "secrets.xml").getAbsolutePath(), Charset.defaultCharset());
        assertNotEquals(secretsXmlContentBefore, secretsXmlContentAfter);
        Wallet.getInstance().readCache();
        assertEquals(6, Wallet.getInstance().getKeys().size());
    }

    @Test
    void addEntrySuccessfully() {
        Wallet wallet = Wallet.getInstance();

        URI uri = URI.create("https://secure-website.net/");
        char[] password = "password".toCharArray();
        wallet.addEntry(uri, new UserCredential(uri, "foo", password));

        password[0] = '1';
        List<UserCredential> entries = wallet.getEntries(uri);
        assertThat(entries, hasSize(1));
        UserCredential storedCredential = entries.get(0);

        assertNotEquals(Arrays.toString(password), Arrays.toString(storedCredential.getPassword()));
    }

    @Test
    void addMultipleEntriesToSameResource() {
        Wallet wallet = Wallet.getInstance();

        URI uri = URI.create("https://some-secure-website.net/");
        char[] password = "password".toCharArray();
        wallet.addEntry(uri, new UserCredential(uri, "foo1", "somethingElse".toCharArray()));
        // UserCredentials should be overwritten.
        wallet.addEntry(uri, new UserCredential(uri, "foo1", password));
        wallet.addEntry(uri, new UserCredential(uri, "foo2", "somethingElse".toCharArray()));

        List<UserCredential> entries = wallet.getEntries(uri);
        assertThat(entries, hasSize(2));
        UserCredential storedCredentialForFoo1 = entries.get(0);
        assertEquals(new String(password), new String(storedCredentialForFoo1.getPassword()));
        UserCredential storedCredentialForFoo2 = entries.get(1);
        assertEquals("somethingElse", new String(storedCredentialForFoo2.getPassword()));
    }

    @Test
    void removeEntrySuccessfully() {
        Wallet wallet = Wallet.getInstance();

        URI uri = URI.create("https://removable-secure-website.net/");
        char[] password = "password".toCharArray();
        wallet.addEntry(uri, new UserCredential(uri, "foo", password));
        wallet.addEntry(uri, new UserCredential(uri, "bar", password));

        List<UserCredential> entries = wallet.getEntries(uri);
        assertThat(entries, hasSize(2));

        wallet.removeEntry(uri, "foo");

        entries = wallet.getEntries(uri);
        assertThat(entries, hasSize(1));
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private static long copy(InputStream source, OutputStream sink) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[1024];
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;
        }
        return nread;
    }
}
