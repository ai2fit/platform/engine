package com.owc.singularity.engine.object.data.exampleset;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.LongStream;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.operator.error.OperatorException;

public class ExampleSetCreatorTest {

    @Test
    void testRowCreation() throws OperatorException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false });
        int numberOfRows = 400000;
        for (int i = 0; i < numberOfRows; i++) {
            creator.setNumericValue(0, i * 3d);
            creator.setNominalValue(1, "value" + i % 100);
            creator.setTimestampValue(2, i + 2178361287l);
            creator.setNumericValue(3, i * 3.2d);
            creator.setNominalValue(4, "value" + i % 230);
            creator.setTimestampValue(5, i + 12178361287l);
            creator.setNumericValue(6, i * 1.3d);
            creator.setNominalValue(7, "value" + i % 122);
            creator.setTimestampValue(8, i + 23178361287l);

            creator.commit();
        }

        ExampleSet set = creator.finish();

        assertEquals(numberOfRows, set.streamNumericAttribute(set.getAttributes().get("A")).count());
        // checking sum
        assertEquals(LongStream.range(0, numberOfRows).mapToDouble(i -> i * 3d).sum(), set.streamNumericAttribute(0).sum());

    }

    @Test
    void testRowHandleCreation() throws OperatorException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false });
        int numberOfRows = 400000;
        LongStream.range(0, numberOfRows).parallel().forEach(i -> {
            int rowHandle = creator.reserveRow();
            creator.setNumericValueWithHandle(rowHandle, 0, i * 3d);
            creator.setNominalValueWithHandle(rowHandle, 1, "value" + i % 100);
            creator.setTimestampValueWithHandle(rowHandle, 2, i + 2178361287l);
            creator.setNumericValueWithHandle(rowHandle, 3, i * 3.2d);
            creator.setNominalValueWithHandle(rowHandle, 4, "value" + i % 230);
            creator.setTimestampValueWithHandle(rowHandle, 5, i + 12178361287l);
            creator.setNumericValueWithHandle(rowHandle, 6, i * 1.3d);
            creator.setNominalValueWithHandle(rowHandle, 7, "value" + i % 122);
            creator.setTimestampValueWithHandle(rowHandle, 8, i + 23178361287l);

            creator.releaseRow(rowHandle);
        });

        ExampleSet set = creator.finish();

        assertEquals(numberOfRows, set.streamNumericAttribute(set.getAttributes().get("A")).count());
        // checking sum
        assertEquals(LongStream.range(0, numberOfRows).mapToDouble(i -> i * 3d).sum(), set.streamNumericAttribute(0).sum());

    }

    @Test
    void testColumnFillerCreation() throws OperatorException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false });
        int numberOfRows = 400000;
        creator.setSize(numberOfRows);

        creator.withNumericAttributeContent(0, row -> row * 3d);
        creator.withNominalAttributeContent(1, i -> "value" + i % 100);
        creator.withTimestampAttributeContent(2, i -> i + 2178361287l);
        creator.withNumericAttributeContent(3, row -> row * 3.2d);
        creator.withNominalAttributeContent(4, i -> "value" + i % 230);
        creator.withTimestampAttributeContent(5, i -> i + 12178361287l);
        creator.withNumericAttributeContent(6, row -> row * 1.3d);
        creator.withNominalAttributeContent(7, i -> "value" + i % 122);
        creator.withTimestampAttributeContent(8, i -> i + 23178361287l);

        ExampleSet set = creator.finish();

        assertEquals(numberOfRows, set.streamNumericAttribute(set.getAttributes().get("A")).count());
        // checking sum
        assertEquals(LongStream.range(0, numberOfRows).mapToDouble(i -> i * 3d).sum(), set.streamNumericAttribute(0).sum());

        // check length of data columns
        for (DataColumn column : set.dataColumns) {
            assertEquals(numberOfRows, column.size);
        }
    }

    @Test
    void singleRowCreation() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false }, 1);
        creator.setSize(1);
        for (DataColumn column : getDataColumns(creator)) {
            assertEquals(1, column.size);
        }
        int i = 0;
        creator.setNumericValue(0, i * 3d);
        creator.setNominalValue(1, "value" + i % 100);
        creator.setTimestampValue(2, i + 2178361287l);
        creator.setNumericValue(3, i * 3.2d);
        creator.setNominalValue(4, "value" + i % 230);
        creator.setTimestampValue(5, i + 12178361287l);
        creator.setNumericValue(6, i * 1.3d);
        creator.setNominalValue(7, "value" + i % 122);
        creator.setTimestampValue(8, i + 23178361287l);

        creator.commit();
        for (DataColumn column : getDataColumns(creator)) {
            assertEquals(1, column.size);
        }
        ExampleSet set = creator.finish();
        for (DataColumn column : set.dataColumns) {
            assertEquals(1, column.size);
        }
    }

    @Test
    void singleRowRepeatedCreation() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false }, 1);
        creator.setSize(1);
        for (DataColumn column : getDataColumns(creator)) {
            assertEquals(1, column.size);
        }
        int i = 0;
        creator.setNumericValue(0, i * 3d);
        creator.setNominalValue(1, "value" + i % 100);
        creator.setTimestampValue(2, i + 2178361287l);
        creator.setNumericValue(3, i * 3.2d);
        creator.setNominalValue(4, "value" + i % 230);
        creator.setTimestampValue(5, i + 12178361287l);
        creator.setNumericValue(6, i * 1.3d);
        creator.setNominalValue(7, "value" + i % 122);
        creator.setTimestampValue(8, i + 23178361287l);

        creator.commit();
        for (DataColumn column : getDataColumns(creator)) {
            assertEquals(1, column.size);
        }
        ExampleSet set = creator.finishAndReset();
        creator.setSize(1);
        for (DataColumn column : set.dataColumns) {
            assertEquals(1, column.size);
        }

        creator.setNumericValue(0, i * 3d);
        creator.setNominalValue(1, "value" + i % 100);
        creator.setTimestampValue(2, i + 2178361287l);
        creator.setNumericValue(3, i * 3.2d);
        creator.setNominalValue(4, "value" + i % 230);
        creator.setTimestampValue(5, i + 12178361287l);
        creator.setNumericValue(6, i * 1.3d);
        creator.setNominalValue(7, "value" + i % 122);
        creator.setTimestampValue(8, i + 23178361287l);

        creator.commit();
        for (DataColumn column : getDataColumns(creator)) {
            assertEquals(1, column.size);
        }
        set = creator.finishAndReset();
        for (DataColumn column : set.dataColumns) {
            assertEquals(1, column.size);
        }
    }

    @Test
    void manyCreators() {
        int size = 10;
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C", "A2", "B2", "C2", "A3", "B3", "C3" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP,
                        ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP },
                new String[] { null, null, "label", null, null, null, null, null, null },
                new boolean[] { false, false, true, false, false, false, false, false, false }, size);
        List<ExampleSet> sets = new LinkedList<>();
        for (int iteration = 0; iteration < 1000; iteration++) {
            for (int i = 0; i < size; i++) {

                creator.setNumericValue(0, i * 3d);
                creator.setNominalValue(1, "value" + i % 100);
                creator.setTimestampValue(2, i + 2178361287l);
                creator.setNumericValue(3, i * 3.2d);
                creator.setNominalValue(4, "value" + i % 230);
                creator.setTimestampValue(5, i + 12178361287l);
                creator.setNumericValue(6, i * 1.3d);
                creator.setNominalValue(7, "value" + i % 122);
                creator.setTimestampValue(8, i + 23178361287l);

                creator.commit();
            }
            ExampleSet set = creator.finishAndReset();
            sets.add(set);
        }
    }

    private DataColumn[] getDataColumns(ExampleSetCreator creator)
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        Field field = ExampleSetCreator.class.getDeclaredField("dataColumns");
        field.setAccessible(true);
        return (DataColumn[]) field.get(creator);
    }
}
