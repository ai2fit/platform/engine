package com.owc.singularity.engine.object.data.exampleset;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.operator.error.OperatorException;

public class ExampleSetTest {

    @Test
    void testPureColumnFillerCreation() throws OperatorException {
        long start = System.currentTimeMillis();
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP }, new String[] { null, null, "label" },
                new boolean[] { false, true, true });
        int numberOfRows = 40000;
        creator.withNumericAttributeContent(0, row -> (row % 3) - 1);

        creator.withNominalAttributeContent(1, row -> "" + (row % 11));
        creator.withTimestampAttributeContent(2, row -> row);
        creator.setSize(numberOfRows);
        ExampleSet set = creator.finish();
        long end = System.currentTimeMillis();
        System.out.println("Creation took " + (end - start));

        start = System.currentTimeMillis();
        assertEquals(-1, set.streamNumericAttribute(set.getAttributes().get("A")).parallel().sum());
        assertEquals((12383 % 3) - 1, set.getNumericValue(12383, set.getAttributes().get("A").getIndex()));
        assertEquals("" + (12383 % 11), set.getNominalValue(12383, set.getAttributes().get("B").getIndex()));
        assertEquals(12383, set.getTimestampValue(12383, set.getAttributes().get("C").getIndex()));
        end = System.currentTimeMillis();
        System.out.println("Reading took " + (end - start));
    }

    @Test
    void testCreation() throws OperatorException {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "A", "B", "C" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP }, new String[] { null, null, "label" },
                new boolean[] { false, true, true });
        int numberOfRows = 40000;
        for (int row = 0; row < numberOfRows; row++) {
            creator.setNumericValue(0, row % 100);
            creator.setNominalValue(1, "" + (row % 11));
            creator.commit();
        }
        creator.withTimestampAttributeContent(2, row -> row);
        ExampleSet set = creator.finish();

        assertEquals(numberOfRows, set.streamNumericAttribute(set.getAttributes().get("A")).count());
        // checking index
        assertEquals(12383, set.getTimestampValue(12383, set.getAttributes().get("C").getIndex()));

        // checking all values
        for (int row = 0; row < numberOfRows; row++) {
            assertEquals(row % 100, set.getNumericValue(row, 0));
            assertEquals("" + (row % 11), set.getNominalValue(row, 1));
        }

        ExampleSet set2 = set.transform()
                .withGeneratedNumericAttribute("Generated::A", row -> (row % 100) + 1)
                .withGeneratedNominalAttribute("Generated::B", row -> "" + (row % 7))
                .withGeneratedTimestampAttribute("Generated::C", row -> row + 1)
                .transform();

        assertEquals(numberOfRows, set2.streamNumericAttribute(set2.getAttributes().get("Generated::A")).count());
        assertEquals(12384 % 100, set2.getNumericValue(12383, set2.getAttributes().get("Generated::A").getIndex()));
        assertEquals("" + (12383 % 7), set2.getNominalValue(12383, set2.getAttributes().get("Generated::B").getIndex()));
        assertEquals(12384, set2.getTimestampValue(12383, set2.getAttributes().get("Generated::C").getIndex()));

        // untouched attributes remain the same
        assertEquals(numberOfRows, set2.streamNumericAttribute(set2.getAttributes().get("A")).count());
        assertEquals(12383 % 100, set2.getNumericValue(12383, set2.getAttributes().get("A").getIndex()));
        assertEquals("" + (12383 % 11), set2.getNominalValue(12383, set2.getAttributes().get("B").getIndex()));
        assertEquals(12383, set2.getTimestampValue(12383, set2.getAttributes().get("C").getIndex()));


        // old set should remain the same
        assertEquals(numberOfRows, set.streamNumericAttribute(set.getAttributes().get("A")).count());
        assertEquals(12383 % 100, set.getNumericValue(12383, set.getAttributes().get("A").getIndex()));
        assertEquals("" + (12383 % 11), set.getNominalValue(12383, set.getAttributes().get("B").getIndex()));
        assertEquals(12383, set.getTimestampValue(12383, set.getAttributes().get("C").getIndex()));

    }
}
