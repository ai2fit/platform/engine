package com.owc.singularity.engine.object.data.exampleset;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;

import org.junit.jupiter.api.Test;

public class ExampleSetMapperTest {

    @Test
    public void testCreationAndReusage() {
        long epochMilli = Instant.now().toEpochMilli();

        long numericIndexBaseAddress = MemoryUtils.UNSAFE.allocateMemory(1000 * 8);
        long numericBaseAddress = MemoryUtils.UNSAFE.allocateMemory(1000 * 8);
        long nominalBaseAddress = MemoryUtils.UNSAFE.allocateMemory(1000 * 4);
        long timestampBaseAddress = MemoryUtils.UNSAFE.allocateMemory(1000 * 8);

        for (int i = 0; i < 1000; i++) {
            MemoryUtils.UNSAFE.putDouble(numericIndexBaseAddress + i * 8, i * 1d);
            MemoryUtils.UNSAFE.putDouble(numericBaseAddress + i * 8, 3.5d);
            MemoryUtils.UNSAFE.putInt(nominalBaseAddress + i * 4, i % 3);
            MemoryUtils.UNSAFE.putLong(timestampBaseAddress + i * 8, epochMilli);
        }

        ExampleSetMapper mapper = new ExampleSetMapper(new String[] { "A", "B", "C", "Index" },
                new ValueType[] { ValueType.NUMERIC, ValueType.NOMINAL, ValueType.TIMESTAMP, ValueType.NUMERIC }, new String[] { null, null, "id", null },
                1000);
        mapper.setAttributeBaseAddress(0, numericBaseAddress);
        mapper.setAttributeBaseAddress(1, nominalBaseAddress);
        mapper.setAttributeBaseAddress(2, timestampBaseAddress);
        mapper.setAttributeBaseAddress(3, numericIndexBaseAddress);

        mapper.mapValues(1, new String[] { "Value0", "Value1", "Value2" });

        ExampleSet exampleSet = mapper.createExampleSet();
        assertEquals(0, exampleSet.getNumericValue(0, 3));
        assertEquals(100, exampleSet.getNumericValue(100, 3));
        assertEquals(1000 * 3.5, exampleSet.streamNumericAttribute(0).sum());
        assertEquals("Value0", exampleSet.getNominalValue(0, 1));
        assertEquals("Value1", exampleSet.getNominalValue(1, 1));
        assertEquals("Value1", exampleSet.getNominalValue(100, 1));
        assertEquals(epochMilli, exampleSet.getTimestampValue(799, 2));

        // check if memory changes are reflected in example set
        MemoryUtils.UNSAFE.putDouble(numericBaseAddress, 4.5);
        assertEquals(4.5, exampleSet.getNumericValue(0, 0));


        // re usage
        mapper.setAttributeBaseAddress(0, numericBaseAddress + 100 * 8);
        mapper.setAttributeBaseAddress(1, nominalBaseAddress + 100 * 4);
        mapper.setAttributeBaseAddress(2, timestampBaseAddress + 100 * 8);
        mapper.setAttributeBaseAddress(3, numericIndexBaseAddress + 100 * 8);
        mapper.setSize(900);
        exampleSet = mapper.createExampleSet();
        assertEquals(900 * 3.5, exampleSet.streamNumericAttribute(0).sum());

        // add new nominal values
        mapper.mapValues(1, new String[] { "nextValueA", "nextValueB" });
        MemoryUtils.UNSAFE.putInt(nominalBaseAddress + 100 * 4, 3);
        MemoryUtils.UNSAFE.putInt(nominalBaseAddress + 101 * 4, 4);
        exampleSet = mapper.createExampleSet();

        assertEquals("nextValueA", exampleSet.getNominalValue(0, 1));
        assertEquals("nextValueB", exampleSet.getNominalValue(1, 1));
        assertEquals("Value1", exampleSet.getNominalValue(3, 1));
        assertEquals("Value2", exampleSet.getNominalValue(4, 1));
        assertEquals("Value0", exampleSet.getNominalValue(5, 1));
    }
}
