package com.owc.singularity.engine.object.data.exampleset.transformer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetTransformer;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.OperatorException;

class ExampleSetTransformerTest {

    @Test
    void attributeNameBestPractices() {
        assertThrows(IllegalArgumentException.class, () -> ExampleSetTransformer.getDerivedAttributeName("Attribute", null));
        assertThrows(IllegalArgumentException.class, () -> ExampleSetTransformer.getDerivedAttributeName("Attribute", ""));

        String specifier = "Spec3";
        assertEquals("Attribute_Spec3", ExampleSetTransformer.getDerivedAttributeName("Attribute", specifier));
        assertEquals("Namespace::Attribute_Spec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::Attribute", specifier));
        assertEquals("Namespace::Attribute1_Spec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::Attribute1", specifier));
        assertEquals("Namespace::AttributeName1_Spec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::AttributeName1", specifier));
        assertEquals("Namespace::Attribute1_SpecSpec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::Attribute1_Spec", specifier));
        assertEquals("Namespace::Attribute1_Spec2Spec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::Attribute1_Spec2", specifier));
        assertEquals("Namespace::Attribute1_Spec1Spec2Spec3", ExampleSetTransformer.getDerivedAttributeName("Namespace::Attribute1_Spec1Spec2", specifier));
    }

    @Test
    void inheritAttributeRoleSuccessfully() throws OperatorException {
        // given
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "Source" }, new ValueType[] { ValueType.NUMERIC }, new String[] { "Role1" },
                new boolean[] { true });
        ExampleSet exampleSet = creator.finish();

        // when
        ExampleSet transformedExampleSet = exampleSet.transform()
                .withGeneratedNominalAttribute("Destination", String::valueOf)
                .inheritRole("Source", "Destination")
                .transform();

        // then
        Attribute originalSource = exampleSet.getAttributes().get("Source");
        Attribute transformedSource = transformedExampleSet.getAttributes().get("Source");
        Attribute destination = transformedExampleSet.getAttributes().get("Destination");

        assertEquals(originalSource.getRole(), destination.getRole());
        assertEquals(originalSource.isSpecial(), destination.isSpecial());

        assertEquals(originalSource.getRole(), transformedSource.getRole());
        assertEquals(originalSource.isSpecial(), transformedSource.isSpecial());
    }

    @Test
    void inheritAttributeRoleOfRemovedAttributeSuccessfully() throws OperatorException {
        // given
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "Source" }, new ValueType[] { ValueType.NUMERIC }, new String[] { "Role1" },
                new boolean[] { true });
        ExampleSet exampleSet = creator.finish();

        // when
        ExampleSet transformedExampleSet = exampleSet.transform()
                .withGeneratedNominalAttribute("Destination", String::valueOf)
                .inheritRole("Source", "Destination")
                .withoutAttribute("Source")
                .transform();

        // then
        Attribute originalSource = exampleSet.getAttributes().get("Source");
        Attribute destination = transformedExampleSet.getAttributes().get("Destination");

        assertEquals(originalSource.getRole(), destination.getRole());
        assertEquals(originalSource.isSpecial(), destination.isSpecial());

        assertNull(transformedExampleSet.getAttributes().get("Source"));
    }
}
