package com.owc.singularity.engine.object.data.exampleset;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.operator.error.OperatorException;

public class ExampleSetHeaderTest {

    @Test
    /**
     * WARNING!!! This test works non deterministically! If this test fails at any time, the
     * implementation needs to be corrected. However, the issue was reproduced with high
     * probability.
     */
    void testParallelGetAttributeIndex() throws OperatorException, InterruptedException, ExecutionException {
        String[] attributeNames = new String[1000];
        ValueType[] attributeTypes = new ValueType[1000];
        String[] roles = new String[1000];
        boolean[] special = new boolean[1000];
        for (int i = 0; i < 1000; i++) {
            attributeNames[i] = "Attribute_" + i;
            attributeTypes[i] = ValueType.NUMERIC;
        }
        ExampleSetCreator creator = new ExampleSetCreator(attributeNames, attributeTypes, roles, special);
        creator.setSize(0);
        ExampleSet set = creator.finish();

        ThreadPoolExecutor executor = new ThreadPoolExecutor(32, 32, 1000l, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(300));

        for (int headerIndex = 0; headerIndex < 10000; headerIndex++) {
            ExampleSetHeader header = set.getHeader();
            List<Future<Boolean>> anyErrorFuture = executor.invokeAll(IntStream.range(0, 32).mapToObj(i -> new Callable<Boolean>() {

                @Override
                public Boolean call() throws Exception {
                    return header.streamRegularAttributes().mapToInt(name -> header.getAttributeIndex(name)).anyMatch(i2 -> i2 == -1);
                };
            }).collect(Collectors.toList()));

            for (Future<Boolean> future : anyErrorFuture)
                assertFalse(future.get());
        }
    }

}
