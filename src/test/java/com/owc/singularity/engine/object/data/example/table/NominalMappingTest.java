package com.owc.singularity.engine.object.data.example.table;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Random;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetTransformer;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

class NominalMappingTest {

    @Nested
    class ConcurrencyTests {


        @RepeatedTest(10)
        void mapping() {
            LinkedHashMap<String, ValueType> nameTypeMap = new LinkedHashMap<>();
            nameTypeMap.put("First", ValueType.NUMERIC);
            nameTypeMap.put("Second", ValueType.NOMINAL);
            nameTypeMap.put("Third", ValueType.TIMESTAMP);
            nameTypeMap.put("Fourth", ValueType.NUMERIC);
            ExampleSetCreator exampleSetCreator = new ExampleSetCreator(nameTypeMap);
            Random random = new Random();
            for (int i = 0; i < 1000; i++) {
                exampleSetCreator.setNumericValue(0, i * 1.5);
                exampleSetCreator.setNominalValue(1, "row " + i);
                exampleSetCreator.setTimestampValue(2, Instant.now().minusMillis(random.nextInt(100_000)).toEpochMilli());
                exampleSetCreator.setNumericValue(3, i);

                // add some missing along the way
                if (i % 5 == 0) {
                    exampleSetCreator.setMissing(3);
                }
                exampleSetCreator.commit();
            }
            ExampleSet set = exampleSetCreator.finish();
            ExampleSetTransformer transformer = set.transform().withDerivedNominalAttribute("First_Nominal", "First", (double value) -> {
                if (ValueType.isMissing(value))
                    return ValueType.MISSING_NOMINAL;
                if (value > 10 && value < 50) {
                    return "same";
                } else {
                    return String.valueOf(value);
                }
            });
            assertDoesNotThrow(transformer::transform);
        }
    }
}
