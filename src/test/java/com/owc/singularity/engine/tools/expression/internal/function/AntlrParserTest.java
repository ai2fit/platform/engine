/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function;


import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser;


/**
 * Tests the results of {@link AntlrParser#parse(String)}.
 *
 * The tests for each function should include at least:
 * <ul>
 * <li>a single correct value</li>
 * <li>two correct values</li>
 * <li>an incorrect value</li>
 * <li>combinations of correct and incorrect types</li>
 * <li>other special cases unique to the function or group of function</li>
 * </ul>
 *
 *
 * @author Gisa Schaefer
 *
 */

public class AntlrParserTest {

    @BeforeAll
    public static void beforeClass() throws Exception {
        if (!I18N.isProviderAvailable()) {
            I18N.setProvider(new LocalI18NResourcesProvider());
        }
    }

    @BeforeEach
    public void setUp() throws Exception {
        Locale.setDefault(Locale.ENGLISH);
    }

    /**
     * Parses string expressions into {@link Expression}s if those expressions don't use any
     * functions, variables, attributes or other variables.
     */
    protected Expression getExpressionWithoutContext(String expression) throws ExpressionException {
        AntlrParser parser = new AntlrParser(null);
        return parser.parse(expression);
    }

    /**
     * Parses string expressions into {@link Expression}s if those expressions use known functions
     * and no variables, attributes or other variables.
     */
    protected Expression getExpressionWithFunctionContext(String expression) throws ExpressionException {
        ExpressionParserBuilder builder = new ExpressionParserBuilder();
        ExpressionParser parser = builder.withModules(ExpressionRegistry.INSTANCE.getAll()).build();
        return parser.parse(expression);
    }

    /**
     * Parses string expressions into {@link Expression}s if those expressions use known functions
     * and variables but no attributes or other variables.
     */
    protected Expression getExpressionWithFunctionsAndVariables(String expression, VariableResolver resolver) throws ExpressionException {
        ExpressionParserBuilder builder = new ExpressionParserBuilder();
        ExpressionParser parser = builder.withModules(ExpressionRegistry.INSTANCE.getAll()).withScope(resolver).build();
        return parser.parse(expression);
    }

    /**
     * Parses the string expression into a {@link Expression} using the given
     * {@link ExampleResolver}.
     */
    protected Expression getExpressionWithFunctionsAndExamples(String expression, ExampleResolver resolver) throws ExpressionException {
        ExpressionParserBuilder builder = new ExpressionParserBuilder();
        ExpressionParser parser = builder.withModules(ExpressionRegistry.INSTANCE.getAll()).withDynamics(resolver).build();
        return parser.parse(expression);
    }

    /**
     * Parses the string expression into a {@link Expression} using the given
     * {@link ExampleResolver} and the given {@link VariableResolver}.
     *
     * @throws ExpressionException
     */
    protected Expression getExpressionWithFunctionsAndExamplesAndVariables(String expression, ExampleResolver resolver, VariableResolver variableResolver)
            throws ExpressionException {
        ExpressionParserBuilder builder = new ExpressionParserBuilder();
        ExpressionParser parser = builder.withModules(ExpressionRegistry.INSTANCE.getAll()).withDynamics(resolver).withScope(variableResolver).build();
        return parser.parse(expression);
    }

    /**
     * @return a exampleSet containing a single example with a single integer attribute called
     *         "integer" with value NaN.
     */
    protected ExampleSet makeMissingNumericExampleSet() {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "integer" }, new ValueType[] { ValueType.NUMERIC });
        creator.setMissing(0);
        creator.commit();

        return creator.finish();
    }

}
