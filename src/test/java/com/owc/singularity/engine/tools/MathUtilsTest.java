/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedHashMap;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.math.MathFunctions;

/**
 * A test for the {@link MathFunctions}.
 * 
 * @author Michael Wurst, Ingo Mierswa
 */
public class MathUtilsTest {

    @Test
    public void testVariance() {
        assertEquals(MathFunctions.variance(new double[] { 0.1, 0.1, 0.0, -0.1 }, Double.NEGATIVE_INFINITY), 0.006875, 0.001);
        assertEquals(MathFunctions.variance(new double[] { 0.0, 0.0, 0.0 }, -1.0), 0.0);
    }

    @Test
    public void testCorrelation() {
        assertEquals(MathFunctions.correlation(new double[] { 0.1, 0.2, -0.3, 0.0 }, new double[] { 0.0, 0.1, 0.1, -0.1 }), -0.161, 0.001);
    }

    @Test
    public void testExampleSetCorrelation() {
        LinkedHashMap<String, ValueType> generatedAttributes = new LinkedHashMap<>();
        generatedAttributes.put("first", ValueType.NUMERIC);
        generatedAttributes.put("second", ValueType.NUMERIC);
        ExampleSetCreator creator = new ExampleSetCreator(generatedAttributes);
        creator.setSize(4);
        double[] firstValues = new double[] { 0.1, 0.2, -0.3, 0.0 };
        double[] secondValues = new double[] { 0.0, 0.1, 0.1, -0.1 };

        creator.withNumericAttributeContent(0, row -> firstValues[row]);
        creator.withNumericAttributeContent(1, row -> secondValues[row]);
        ExampleSet exampleSet = creator.finish();

        assertEquals(MathFunctions.correlationNumeric(exampleSet, exampleSet.getAttributes().get("first"), exampleSet.getAttributes().get("second"), false),
                -0.161, 0.001);
    }


}
