/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools.container;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.WeakHashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link com.owc.singularity.engine.tools.container.StackingMap}.
 *
 * @author Jan Czogalla
 * @since 8.2
 */
public class StackingMapTest {

    public static final int TEST_SEED = 284092;
    public static final int TEST_MAP_SIZE = 20;
    public static final int MAXIMUM_RANDOM_STRING_LENGTH = 20;
    private static Map<String, String> wrappableHashMap;
    private static Map<String, String> wrappableTreeMap;

    @BeforeAll
    public static void prepareWrappableMaps() {
        wrappableHashMap = new HashMap<>();
        wrappableTreeMap = new TreeMap<>();
        Random rng = new Random(TEST_SEED);
        for (int i = 0; i < TEST_MAP_SIZE; i++) {
            String k = randomString(rng);
            String v = randomString(rng);
            wrappableHashMap.put(k, v);
            wrappableTreeMap.put(k, v);
        }
    }

    private static String randomString(Random rng) {
        int size = 1 + rng.nextInt(MAXIMUM_RANDOM_STRING_LENGTH);
        StringBuilder builder = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            boolean upper = rng.nextBoolean();
            builder.append((char) ((upper ? 'A' : 'a') + rng.nextInt(26)));
        }
        return builder.toString();
    }

    /**
     * Test if all entries are present in a wrapped map
     */
    @Test
    public void wrapHashMap() {
        StackingMap<String, String> wrappedMap = new StackingMap<>(wrappableHashMap);
        Assertions.assertEquals(wrappableHashMap.size(), wrappedMap.size(), "Different size of maps");
        for (Entry<String, String> e : wrappableHashMap.entrySet()) {
            String v = wrappedMap.get(e.getKey());
            Assertions.assertNotNull(v, "Key not present: " + e.getKey());
            Assertions.assertEquals(e.getValue(), v, "Different values for same key");
        }
    }

    /**
     * Test if the backing map is actually a tree map by comparing iteration order
     */
    @Test
    public void wrapTreeMap() {
        StackingMap<String, String> wrappedMap = new StackingMap<>(wrappableTreeMap);
        Iterator<Entry<String, String>> wrappedIterator = wrappedMap.entrySet().iterator();
        Iterator<Entry<String, String>> treeIterator = wrappableTreeMap.entrySet().iterator();
        while (wrappedIterator.hasNext() && treeIterator.hasNext()) {
            Entry<String, String> a = wrappedIterator.next();
            Entry<String, String> b = treeIterator.next();
            Assertions.assertEquals(b.getKey(), a.getKey(), "Wrong order for keys");
            Assertions.assertEquals(b.getValue(), a.getValue(), "Different values for same key");
        }
        Assertions.assertEquals(treeIterator.hasNext(), wrappedIterator.hasNext(), "Different size of maps");
    }

    /**
     * Test if a weak hash map as a backing map works as expected
     */
    @Test
    public void backedByWeakHashMap() {
        StackingMap<Object, String> weakBackend = new StackingMap<>(WeakHashMap.class);
        Assertions.assertTrue(weakBackend.isEmpty(), "Newly created map is not empty");
        Object a = new Object();
        weakBackend.push(a, "test");
        Assertions.assertFalse(weakBackend.isEmpty(), "Present key object is not present in map after insertion");
        Assertions.assertNotNull("Present key object is not present in map after insertion", weakBackend.get(a));
        a = null;
        System.gc();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        Assertions.assertTrue(weakBackend.isEmpty(), "Weak hash map does not get rid of key");
    }

    /**
     * Test if null as a value argument to push is ignored
     */
    @Test
    public void ignoreNullValue() {
        StackingMap<Object, Object> stackingMap = new StackingMap<>();
        stackingMap.push(new Object(), null);
        Assertions.assertTrue(stackingMap.isEmpty(), "Map is not empty after insertion of null value");
    }

    /**
     * Test if values are stacked and can be accessed correctly
     */
    @Test
    public void stackValues() {
        StackingMap<Object, Object> stackingMap = new StackingMap<>();
        Object key = new Object();
        Object item1 = new Object();
        stackingMap.push(key, item1);
        Assertions.assertEquals(1, stackingMap.size(), "Size is wrong");
        Object item2 = new Object();
        stackingMap.push(key, item2);
        Assertions.assertEquals(1, stackingMap.size(), "Size is wrong");
        Assertions.assertEquals(item2, stackingMap.peek(key), "Top item is not the same as last pushed item");
        Assertions.assertEquals(item2, stackingMap.pop(key), "Top item is not the same as last pushed item");
        Assertions.assertEquals(1, stackingMap.size(), "Size is wrong");
        Assertions.assertEquals(item1, stackingMap.pop(key), "Top item is not the same as last pushed item");
        Assertions.assertTrue(stackingMap.isEmpty(), "Popped map is not empty");
    }
}
