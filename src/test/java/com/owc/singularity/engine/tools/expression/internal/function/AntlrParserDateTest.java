/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function;


import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.ExampleResolver;
import com.owc.singularity.engine.tools.expression.Expression;
import com.owc.singularity.engine.tools.expression.ExpressionException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser;


/**
 * Tests the results of {@link AntlrParser#parse(String)} for date functions.
 *
 * @author David Arnu
 *
 */
public class AntlrParserDateTest extends AntlrParserTest {

    // double values for some date entries
    static long sometime = Instant.now().toEpochMilli();
    static long sometime_before = sometime - 5000;
    static long sometime_after = sometime + 5000;

    private static ExampleSet makeDateExampleSet() {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "date_time", "date_time_before", "date_time_after", "date_time_missing", "integer" },
                new ValueType[] { ValueType.TIMESTAMP, ValueType.TIMESTAMP, ValueType.TIMESTAMP, ValueType.TIMESTAMP, ValueType.NUMERIC });
        creator.setTimestampValue(0, sometime);
        creator.setTimestampValue(1, sometime_before);
        creator.setTimestampValue(2, sometime_after);
        creator.setMissing(3);
        creator.setMissing(4);
        creator.commitAndKeep();
        creator.commitAndKeep();
        creator.commitAndKeep();
        creator.commitAndKeep();

        return creator.finish();
    }

    // date_now
    @Test
    public void dateNowBasic() {
        try {

            Expression expression = getExpressionWithFunctionContext("date_now()");
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            // for testing we just assume that the dates are close enough, some delay might occur
            assertTrue(Math.abs(Instant.now().toEpochMilli() - expression.evaluateTimestamp()) < 10, "Dates aren't close enough to each other!");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateNowArgumentNumerical() {
        try {
            getExpressionWithFunctionContext("date_now(5)");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateNowArgumentString() {
        try {
            getExpressionWithFunctionContext("date_now(\"bla\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_before

    @Test
    public void dateBeforeTRUE() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_before(date_time, date_time_after)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertTrue(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateBeforeFALSE() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_before(date_time, date_time_before)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertFalse(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateBeforeEqual() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time, date_time)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertFalse(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateBeforeMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_before(date_time_missing, date_time)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertNull(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateBeforeMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_before(date_time, date_time_missing)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertNull(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateBeforeWrongFirst() {
        try {
            getExpressionWithFunctionContext("date_before(\"bla\", date_now())");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateBeforeWrongSecond() {
        try {
            getExpressionWithFunctionContext("date_before(date_now(), \"bla\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_after

    @Test
    public void dateAfterTRUE() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time, date_time_before)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertTrue(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAfterFALSE() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time, date_time_after)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertFalse(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAfterEqual() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time, date_time)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertFalse(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAfterMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time_missing, date_time)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertNull(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAfterMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_after(date_time, date_time_missing)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());
            assertNull(expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAfterWrongFirst() {
        try {
            getExpressionWithFunctionContext("date_after(\"bla\", date_now())");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateAfterWrongSecond() {
        try {
            getExpressionWithFunctionContext("date_after(date_now(), \"bla\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_diff
    @Test
    public void dateDiffBasic() {
        try {
            Expression expression = getExpressionWithFunctionContext("date_diff(date_now(), date_now())");
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            // for testing we just assume that the dates are close enough, some delay might occur
            assertTrue(Math.abs(expression.evaluateNumeric()) < 10, "Dates aren't close enough to each other!");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffPositive() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_after)", resolver);
            assertEquals(sometime_after - sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffNegative() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_before)", resolver);
            assertEquals(sometime_before - sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffEqual() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time)", resolver);
            assertEquals(0, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time_missing, date_time)", resolver);
            assertEquals(Double.NaN, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_missing)", resolver);
            assertEquals(Double.NaN, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffWrongFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionContext("date_diff(\"bla\", date_now())");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateDiffWrongSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionContext("date_diff(date_now(), \"bla\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateDiffWithLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_after, \"us\", \"America/Los Angeles\")", resolver);
            assertEquals(sometime_after - sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffLocaleTZWrongFormat() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_after, \"abcd\", \"abcd\")", resolver);
            assertEquals(sometime_after - sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateDiffLocaleTZWrongType() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_after, 5, 5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateDiffLocaleTZMissing() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_diff(date_time, date_time_after, MISSING_NOMINAL, MISSING_NOMINAL)", resolver);
            assertEquals(sometime_after - sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    // date_add

    @Test
    public void dateAddBasic() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), 5, DATE_UNIT_HOUR)", resolver);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 5);
            double diff = cal.getTimeInMillis() - expression.evaluateTimestamp();
            assertTrue(Math.abs(diff) < 10, "Dates aren't close enough to each other!");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddWithReals() throws ExpressionException {
        // only integer portion should be taken into account according to docs
        ExampleSet exampleSet = makeDateExampleSet();
        ExampleResolver resolver = new ExampleResolver(exampleSet);
        resolver.bind(exampleSet.getExample(0));
        Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), 5.9, DATE_UNIT_HOUR)", resolver);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 5);
        double diff = cal.getTimeInMillis() - expression.evaluateTimestamp();
        assertTrue(Math.abs(diff) < 10, "Dates aren't close enough to each other!");

        expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), 6.0, DATE_UNIT_HOUR)", resolver);
        cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 6);
        diff = cal.getTimeInMillis() - expression.evaluateTimestamp();
        assertTrue(Math.abs(diff) < 10, "Dates aren't close enough to each other!");
    }

    @Test
    public void dateAddWithLongs() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            long overFlow = 20L;
            long addValue = overFlow + 2L * Integer.MAX_VALUE;
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), " + addValue + ", DATE_UNIT_HOUR)", resolver);
            long evaluateDate = expression.evaluateTimestamp();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR_OF_DAY, Integer.MAX_VALUE);
            cal.add(Calendar.HOUR_OF_DAY, Integer.MAX_VALUE);
            cal.add(Calendar.HOUR_OF_DAY, (int) overFlow);
            double diff = cal.getTimeInMillis() - evaluateDate;
            assertTrue(Math.abs(diff) < 15, "Dates aren't close enough to each other!" + "(Diff was " + diff + ")");

            addValue = -overFlow + 2L * Integer.MIN_VALUE;
            expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), " + addValue + ", DATE_UNIT_HOUR)", resolver);
            evaluateDate = expression.evaluateTimestamp();
            cal = Calendar.getInstance();
            cal.add(Calendar.HOUR_OF_DAY, Integer.MIN_VALUE + 1000);
            cal.add(Calendar.HOUR_OF_DAY, Integer.MIN_VALUE + 1000);
            cal.add(Calendar.HOUR_OF_DAY, (int) -overFlow - 2000);
            diff = cal.getTimeInMillis() - evaluateDate;
            assertTrue(Math.abs(diff) < 15, "Dates aren't close enough to each other!" + "(Diff was " + diff + ")");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddBasicWithLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), 5, DATE_UNIT_HOUR)", resolver);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 5);
            double diff = cal.getTimeInMillis() - expression.evaluateTimestamp();
            assertTrue(Math.abs(diff) < 10, "Dates aren't close enough to each other!");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddBasicWithLocaleTZMissing() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_now(), 5, DATE_UNIT_HOUR, MISSING_NOMINAL, MISSING_NOMINAL)",
                    resolver);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, 5);
            double diff = cal.getTimeInMillis() - expression.evaluateTimestamp();
            assertTrue(Math.abs(diff) < 10, "Dates aren't close enough to each other!");
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_time_missing, 5, DATE_UNIT_HOUR)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_time, [integer], DATE_UNIT_HOUR)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddMissingThird() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_time, 5, MISSING_NOMINAL)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateAddWrongFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_add(5, 5, DATE_UNIT_HOUR)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateAddWrongSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_add(date_time, \"abc\", DATE_UNIT_HOUR)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateAddWrongThirdType() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_add(date_time, 5, 5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateAddWrongThirdConstant() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_add(date_time, 5, \"abc\")", resolver);
            // fails during runtime
            expression.evaluateTimestamp();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateAddWrongLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_add(date_time, 5, DATE_UNIT_HOUR , 5,,5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_set

    @Test
    public void dateSetBasic() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 11, DATE_UNIT_MONTH)", resolver);
            assertEquals(11, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.systemDefault()).getMonthValue());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetWithReals() throws ExpressionException {
        // only integer portion should be taken into account according to docs
        ExampleSet exampleSet = makeDateExampleSet();
        ExampleResolver resolver = new ExampleResolver(exampleSet);
        resolver.bind(exampleSet.getExample(0));
        Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 10.9, DATE_UNIT_MONTH)", resolver);
        assertEquals(10, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.systemDefault()).getMonthValue());

        expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 11.0, DATE_UNIT_MONTH)", resolver);
        assertEquals(11, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.systemDefault()).getMonthValue());
    }

    @Test
    public void dateSetWithLongs() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            long setValue = 23;
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, " + setValue + ", DATE_UNIT_MINUTE)", resolver);
            assertEquals(setValue, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.systemDefault()).getMinute());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetBasicWithLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 10, DATE_UNIT_MINUTE, \"us\", \"America/Los Angeles\")",
                    resolver);
            assertEquals(10, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.of("America/Los_Angeles")).getMinute());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetBasicWithLocaleTZMissing() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_now(), 5, DATE_UNIT_HOUR, MISSING_NOMINAL, MISSING_NOMINAL)",
                    resolver);
            assertEquals(5, Instant.ofEpochMilli(expression.evaluateTimestamp()).atZone(ZoneId.systemDefault()).getHour());
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time_missing, 5, DATE_UNIT_HOUR)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, [integer], DATE_UNIT_HOUR)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetMissingThird() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 5, MISSING_NOMINAL)", resolver);
            assertTrue(ValueType.isMissing(expression.evaluateTimestamp()));
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateSetWrongFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_set(5, 5, DATE_UNIT_HOUR)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateSetWrongSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_set(date_time, \"abc\", DATE_UNIT_HOUR)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateSetWrongThirdType() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_set(date_time, 5, 5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateSetWrongThirdConstant() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_set(date_time, 5, \"abc\")", resolver);
            // fails during runtime
            expression.evaluateTimestamp();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateSetWrongLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_set(date_time, 5, DATE_UNIT_HOUR , 5, 5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_get

    @Test
    public void dateGetBasic() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            final Attribute dateTimeAttribute = exampleSet.getAttributes().get("date_time");
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time, DATE_UNIT_HOUR)", resolver);
            Calendar cal = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
            cal.setTime(exampleSet.getExample(0).getDateValue(dateTimeAttribute));
            assertEquals(cal.get(Calendar.HOUR_OF_DAY), expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateGetMissingFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time_missing, DATE_UNIT_HOUR)", resolver);
            assertEquals(Double.NaN, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateGetMissingSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time, MISSING_NOMINAL)", resolver);
            assertEquals(Double.NaN, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateGetBasicWithLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            final Attribute dateTimeAttribute = exampleSet.getAttributes().get("date_time");
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time, DATE_UNIT_HOUR, \"fr\", \"Europe/Madrid\")", resolver);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"), Locale.forLanguageTag("fr"));
            cal.setTime(exampleSet.getExample(0).getDateValue(dateTimeAttribute));
            assertEquals(cal.get(Calendar.HOUR_OF_DAY), expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateGetWrongFirst() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_get(5, DATE_UNIT_HOUR, \"us\", \"America/Los Angeles\")", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateGetWrongSecond() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_get(date_time, 5.5 )", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateGetWrongUnit() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time, \"X\")", resolver);
            expression.evaluate();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateGetMissingLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            final Attribute dateTimeAttribute = exampleSet.getAttributes().get("date_time");
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_get(date_time, DATE_UNIT_HOUR, MISSING_NOMINAL, MISSING_NOMINAL)", resolver);
            Calendar cal = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
            cal.setTime(exampleSet.getExample(0).getDateValue(dateTimeAttribute));
            assertEquals(cal.get(Calendar.HOUR_OF_DAY), expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateGetWrongLocaleTZ() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            getExpressionWithFunctionsAndExamples("date_get(date_time, DATE_UNIT_HOUR, 5, 5)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    // date_time
    @Test
    public void dateMillis() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_millis(date_time)", resolver);
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            assertEquals(sometime, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }

    @Test
    public void dateMillisWrongType() {
        try {
            getExpressionWithFunctionContext("date_millis(5)");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void dateMillisMissing() {
        try {
            ExampleSet exampleSet = makeDateExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            resolver.bind(exampleSet.getExample(0));
            Expression expression = getExpressionWithFunctionsAndExamples("date_millis(date_time_missing)", resolver);
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            assertEquals(ValueType.MISSING_NUMERIC, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e);
        }
    }
}
