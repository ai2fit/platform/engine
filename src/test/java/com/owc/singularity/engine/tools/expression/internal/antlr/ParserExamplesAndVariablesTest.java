/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.antlr;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.*;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.expression.*;

/**
 * Tests the results of
 * {@link com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser#parse(String)} for
 * example values and variables.
 *
 * @author Gisa Schaefer
 */
public class ParserExamplesAndVariablesTest {

    private static ExampleSet exampleSet;
    private static ExampleResolver resolver;
    private static ExpressionParser parser;

    @BeforeAll
    public static void setUpForAll() {
        exampleSet = makeExampleSet();
        resolver = new ExampleResolver(exampleSet);

        VariableHandler handler = new VariableHandler(null);
        handler.addVariable("my variable", "my value");
        handler.addVariable("Number_variable", "5");
        handler.addVariable("Attribute Variable", "numerical");
        handler.addVariable("my\\ {bracket}", "bracket");
        VariableResolver variableResolver = new VariableResolver(handler);

        ExpressionParserBuilder builder = new ExpressionParserBuilder();
        parser = builder.withDynamics(resolver).withScope(variableResolver).build();
    }

    @AfterEach
    public void cleanResolver() {
        resolver.unbind();
    }

    private static ExampleSet makeExampleSet() {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "nominal", "numerical", "date_time", "t[i]m\\e", "real]", "integer" },
                new ValueType[] { ValueType.NOMINAL, ValueType.NUMERIC, ValueType.TIMESTAMP, ValueType.TIMESTAMP, ValueType.NUMERIC, ValueType.NUMERIC });

        creator.setNominalValue(0, "dog");
        creator.setNumericValue(1, 1.5);
        creator.setTimestampValue(2, 123142311234L);
        creator.setTimestampValue(3, 123142311234L);
        creator.setNumericValue(4, 5d);
        creator.setNumericValue(5, 1123424d);
        creator.commit();

        creator.setNominalValue(0, "cat");
        creator.setNumericValue(1, 0.1234123134);
        creator.setTimestampValue(2, 123142313411234L);
        creator.setTimestampValue(3, 123142311234L);
        creator.setNumericValue(4, 11.25d);
        creator.setNumericValue(5, 11d);
        creator.commit();

        creator.setNominalValue(0, ValueType.MISSING_NOMINAL);
        creator.setNumericValue(1, ValueType.MISSING_NUMERIC);
        creator.setTimestampValue(2, ValueType.MISSING_TIMESTAMP);
        creator.setTimestampValue(3, ValueType.MISSING_TIMESTAMP);
        creator.setNumericValue(4, 0.13445e-12d);
        creator.setNumericValue(5, ValueType.MISSING_NUMERIC);
        creator.commit();

        return creator.finish();
    }

    private Expression getExpressionWithExamplesAndVariables(String expression) throws ExpressionException {
        return parser.parse(expression);
    }

    @Test
    public void numericalAttribute() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("[numerical]");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("numerical");
            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getNumericValue(attribute), expression.evaluateNumeric(), 1e-15);
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void realAttribute() {

        try {

            Expression expression = getExpressionWithExamplesAndVariables("[real\\]]");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("real]");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getNumericValue(attribute), expression.evaluateNumeric(), 1e-15);
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void integerAttribute() {

        try {

            Expression expression = getExpressionWithExamplesAndVariables("integer");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("integer");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(Math.floor(example.getNumericValue(attribute)), expression.evaluateNumeric(), 1e-15);
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void dateTimeAttribute() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("[date_time]");
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("date_time");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getTimestampValue(attribute), expression.evaluateTimestamp());
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void dateAttribute() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("date_time");
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("date_time");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getTimestampValue(attribute), expression.evaluateTimestamp());
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void timeAttribute() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("[t\\[i\\]m\\\\e]");
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("t[i]m\\e");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getTimestampValue(attribute), expression.evaluateTimestamp());
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void myVariable() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("%{my variable}");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals("my value", expression.evaluateNominal());

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void myBracketVariable() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("%{my\\\\ \\{bracket\\}}");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals("bracket", expression.evaluateNominal());

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void numberVariable() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("%{Number_variable}");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals("5", expression.evaluateNominal());

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void attributeVariable() {

        try {
            Expression expression = getExpressionWithExamplesAndVariables("#{Attribute Variable}");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            Attribute attribute = exampleSet.getAttributes().get("numerical");

            for (Example example : exampleSet) {
                resolver.bind(example);
                assertEquals(example.getNumericValue(attribute), expression.evaluateNumeric(), 1e-15);
            }

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void unknownVariable() {
        try {
            getExpressionWithExamplesAndVariables("%{unknown}");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void unknownAttribute() {
        try {
            getExpressionWithExamplesAndVariables("[unknown]");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

}
