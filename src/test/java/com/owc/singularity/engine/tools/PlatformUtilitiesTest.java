package com.owc.singularity.engine.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.Test;


public class PlatformUtilitiesTest {

    @Test
    public void getVersion() {
        try (final InputStream resourceAsStream = PlatformUtilities.class.getResourceAsStream("/com/owc/singularity/resources/tools/version.properties")) {
            Properties props = new Properties();
            props.load(resourceAsStream);
            final String specificationVersion = props.getProperty("version");
            final String releaseVersion = PlatformUtilities.getReleaseVersion();
            assertEquals(specificationVersion, releaseVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
