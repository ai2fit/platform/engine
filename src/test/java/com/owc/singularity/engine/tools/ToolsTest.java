/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.PropertyService;

/**
 * Tests for methods of the {@link com.owc.singularity.engine.tools.Tools} class.
 *
 * @author Marco Boeck
 */
public class ToolsTest {

    @BeforeAll
    static void beforeAll() {
        PropertyService.init();
    }

    @Test
    public void formatIntegerIfPossibleNoArgs() {
        // default fraction digits are 3

        String formatted = Tools.formatIntegerIfPossible(0.001);
        assertEquals("0.001000", formatted);
        formatted = Tools.formatIntegerIfPossible(0.0001);
        assertEquals("0.000100", formatted);
        formatted = Tools.formatIntegerIfPossible(0.000000000000001);
        assertEquals("0.000000", formatted);
        formatted = Tools.formatIntegerIfPossible(0.0000000000000001);
        assertEquals("0", formatted);

        formatted = Tools.formatIntegerIfPossible(1.01);
        assertEquals("1.010000", formatted);
        // the test below could be expected to work but will fail due to how IEEE 754 works
        // we calculate (Math.abs(1 - 1.001) < 0.001) which returns 0.0009999999999 instead of 0.001
        formatted = Tools.formatIntegerIfPossible(1.0001);
        assertEquals("1.000100", formatted);

        formatted = Tools.formatIntegerIfPossible(4.3751);
        assertEquals("4.375100", formatted);
    }

    @Test
    public void formatIntegerIfPossibleAllArgs() {
        int fractionDigits = 5;
        double valueToFormat = 0.001;
        String formatted = Tools.formatIntegerIfPossible(valueToFormat, fractionDigits, false);
        assertEquals("0.00100", formatted);

        formatted = Tools.formatIntegerIfPossible(valueToFormat, fractionDigits, false);
        assertEquals("0.00100", formatted);
        formatted = Tools.formatIntegerIfPossible(valueToFormat, fractionDigits - 2, false);
        assertEquals("0.001", formatted);
    }

    @Test
    public void testExcelColumnNames() {
        // test error case
        assertEquals("error", Tools.getExcelColumnName(-1), "Negative indices should create \"error\" result.");
        // test base case
        for (int i = 0; i < 26; i++) {
            assertEquals("" + (char) ('A' + i), Tools.getExcelColumnName(i), "Index " + i + " creates wrong output.");
        }
        // edge cases
        assertEquals("AA", Tools.getExcelColumnName(26), "Index 26 creates wrong output");
        assertEquals("AZ", Tools.getExcelColumnName(51), "Index 51 creates wrong output");

    }

}
