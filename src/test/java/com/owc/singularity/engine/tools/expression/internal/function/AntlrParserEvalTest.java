/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser;


/**
 * Tests the results of {@link AntlrParser#parse(String)} for the eval function.
 *
 * @author Gisa Schaefer
 *
 */
public class AntlrParserEvalTest extends AntlrParserTest {

    private ExampleSet makeExampleSet() {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "nominal", "numerical", "integer" },
                new ValueType[] { ValueType.NOMINAL, ValueType.NUMERIC, ValueType.NUMERIC });
        creator.setNominalValue(0, "contains(\"a\", \"b\")");
        creator.setNumericValue(1, 1.5);
        creator.setNumericValue(2, 5);
        creator.commit();
        creator.setNominalValue(0, "concat(\"a\", \"b\")");
        creator.setNumericValue(1, 3);
        creator.setMissing(2);
        creator.commit();


        return creator.finish();
    }

    @Test
    public void evalWrongNumberOfArguments() {
        try {
            getExpressionWithFunctionContext("eval()");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalWrongInputType() {
        try {
            getExpressionWithFunctionContext("eval(2 ,1.5 )");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void multiplyIntsViaEval() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"3*5\")");
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            assertEquals(3 * 5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalMissingString() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(MISSING_NOMINAL)");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void multiplyIntsViaEvalToDouble() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"3*5\",NUMERIC)");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(3 * 5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void multiplyDoublesViaEval() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"3.0\"+\"*5\")");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(3.0 * 5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void divideIntsViaEvalWithType() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"4 /2\",NUMERIC)");
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(4.0 / 2, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void divideDoublesViaEvalWithWrongType() {
        try {
            getExpressionWithFunctionContext("eval(\"5.0 /2\",INTEGER)");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalWithInvalidType() {
        try {
            getExpressionWithFunctionContext("eval(\"5.0 /2\",\"blup\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void moduloIntsViaEvalWithStringType() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"5 %2\",NOMINAL)");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(5 % 2 + "", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalWithStringTypeMissing() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"MISSING_NOMINAL\",NOMINAL)");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalWithStringTypeMissingNumerical() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"MISSING_NUMERIC\",NOMINAL)");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalWithStringTypeMissingDate() {
        try {
            Expression expression = getExpressionWithFunctionContext("eval(\"MISSING_DATE\",NOMINAL)");
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void differentPointOperationsWithNestedEvalFail() {
        try {
            getExpressionWithFunctionContext("eval(\"4%3 *\"+\"eval(\"+\"5/2\"+\")\")");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void powerIntsEvalWithVariable() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("my variable", "2^3^2");
            VariableResolver resolver = new VariableResolver(handler);

            Expression expression = getExpressionWithFunctionsAndVariables("eval(%{my variable})", resolver);
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            assertEquals(Math.pow(2, Math.pow(3, 2)), expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void differentPointOperationsWithNestedEval() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("my variable", "\"5/2\"");
            VariableResolver resolver = new VariableResolver(handler);

            Expression expression = getExpressionWithFunctionsAndVariables("eval(\"4%3 *\"+\"eval(\"+%{my variable}+\")\")", resolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(4 % 3 * 5 / 2.0, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithoutSecond() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("eval([nominal])", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }


    @Test
    public void evalAttributeWithSecond() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[integer],NUMERIC)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(3 * 5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToDouble() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[integer],NUMERIC)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(3.0 * 5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToString() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[integer],NOMINAL)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(3 * 5 + "", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondNotConstant() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("eval(\"3*\"+[integer],[nominal])", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToStringMissing() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"MISSING_DATE\"+[integer],NOMINAL)", resolver);
            resolver.bind(exampleSet.getExample(1));

            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToStringMissingBoth() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(MISSING+[integer],NOMINAL)", resolver);
            resolver.bind(exampleSet.getExample(1));

            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());
            assertEquals(null, expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToDate() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[integer],DATE)", resolver);
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            resolver.bind(exampleSet.getExample(0));
            expression.evaluateTimestamp();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalRealAttributeWithSecondToDouble() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[numerical],NUMERIC)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(3 * 1.5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalRealAttributeWithSecondToInteger() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"3*\"+[numerical],INTEGER)", resolver);
            assertEquals(ExpressionType.INTEGER, expression.getExpressionType());
            resolver.bind(exampleSet.getExample(0));
            expression.evaluateNumeric();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalStringAttributeWithSecond() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval([nominal],NOMINAL)", resolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals("false", expression.evaluateNominal());

            resolver.bind(exampleSet.getExample(1));
            assertEquals("ab", expression.evaluateNominal());

        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalBooleanAttributeWithSecond() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval([nominal],BOOLEAN)", resolver);
            assertEquals(ExpressionType.BOOLEAN, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals(false, expression.evaluateBoolean());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeFromString() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("eval(\"integer\")", resolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals(5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void attributeFromVariable() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("attribute", "integer");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("eval(%{attribute})", resolver, variableResolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals(5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void attributeFromVariableWithType() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("attribute", "integer");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("eval(%{attribute},NOMINAL)", resolver, variableResolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals("5", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

}
