/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.AnalysisPipeline;

/**
 * Tests for the {@link com.owc.singularity.engine.tools.ProcessTools} utility class.
 *
 * @author Jan Czogalla
 * @since 9.6
 */
public class ProcessToolsTest {

    private static OperatorDescription desc;

    @BeforeAll
    public static void setup() throws OperatorCreationException {
        if (!I18N.isProviderAvailable()) {
            final LocalI18NResourcesProvider service = new LocalI18NResourcesProvider();
            I18N.setProvider(service);
            service.initialize();
        }
        desc = mock(OperatorDescription.class);
        when(desc.getGroup()).thenReturn("");
        when(desc.getFullyQuallifiedKey()).thenReturn("process");
        when(desc.isEnabled()).thenReturn(true);
        when(desc.getKey()).thenReturn("process");
        Class<? extends Operator> rootClass = AbstractRootOperator.class;
        when(desc.getOperatorClass()).then(invocation -> rootClass);

        OperatorService.registerOperator(desc);
    }

    @AfterAll
    public static void tearDown() {
        OperatorService.unregisterOperator(desc);
        desc = null;
    }

    /**
     * Test that parent process propagation works as expected
     */
    @Test
    public void testProcessParent() {
        AbstractPipeline process = new AnalysisPipeline();
        assertSame(process, ProcessTools.getParentProcess(process));
        AbstractPipeline child = new AnalysisPipeline();
        assertNotSame(process, child);
        ProcessTools.setParentProcess(child, process);
        assertSame(process, ProcessTools.getParentProcess(child));
        AbstractPipeline grandChild = new AnalysisPipeline();
        assertNotSame(process, grandChild);
        assertNotSame(child, grandChild);
        ProcessTools.setParentProcess(grandChild, child);
        assertSame(process, ProcessTools.getParentProcess(grandChild));
    }
}
