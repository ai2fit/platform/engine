package com.owc.singularity.engine.tools;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

import java.io.IOException;
import java.util.ResourceBundle;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;


class I18NTest {

    @BeforeAll
    static void beforeAll() {
        if (!I18N.isProviderAvailable()) {
            I18N.setProvider(new LocalI18NResourcesProvider());
        }
    }

    @Nested
    class ReplacementsTests {

        @Test
        void replacesThrowableCorrectly() {
            ResourceBundle bundle = mock(ResourceBundle.class);
            Throwable throwable = new IOException("Something", new IOException("File", new IOException(null, new RuntimeException("Root"))));
            // given
            given(bundle.getString(eq("myKey"))).willReturn("Message with one throwable {0}");

            // when
            String result = I18N.getMessage(bundle, "myKey", throwable);

            // then
            then(bundle).should().getString(eq("myKey"));
            assertEquals("Message with one throwable Something caused by: File caused by: Root", result);
        }
    }
}
