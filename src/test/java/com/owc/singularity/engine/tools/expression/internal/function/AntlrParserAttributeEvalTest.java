/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser;


/**
 * Tests the results of {@link AntlrParser#parse(String)} for the attribute eval function.
 *
 * @author Gisa Meier
 *
 */
public class AntlrParserAttributeEvalTest extends AntlrParserTest {

    @Test
    public void evalWrongNumberOfArguments() {
        try {
            getExpressionWithFunctionContext("attribute()");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalWrongInputType() {
        try {
            getExpressionWithFunctionContext("attribute(2 ,1.5 )");
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }


    @Test
    public void withEvalWithVariable() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("my variable", "\"int\"+\"eger\"");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("attribute(eval(%{my variable}))", resolver, variableResolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            resolver.bind(exampleSet.getExample(1));
            assertEquals(Double.NaN, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalWithVariableAndAttribute() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("my variable", "numeric");
            handler.addVariable("al", "al");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("eval(\"attribute(%{my variable}+%{al})*2\")", resolver,
                    variableResolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            resolver.bind(exampleSet.getExample(0));
            assertEquals(2 * 1.5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeConstantNaN() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("attribute(MISSING_NOMINAL)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalAttributeConstant() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"nominal 1\")", resolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals("a", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeConstantWrongType() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("attribute(\"nominal1\", REAL)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalAttributeNonConstant() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"nominal \"+floor(integer), NOMINAL)", resolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals("a", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeNonConstantWrongType() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"nominal\"+integer, REAL)", resolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            expression.evaluateNumeric();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalAttributeNonConstantNaN() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"nominal\"+integer, NOMINAL)", resolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(1));
            expression.evaluateNominal();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    private ExampleSet makeExampleSet() {
        ExampleSetCreator creator = new ExampleSetCreator(new String[] { "nominal 1", "numerical", "integer" },
                new ValueType[] { ValueType.NOMINAL, ValueType.NUMERIC, ValueType.NUMERIC });
        creator.setNominalValue(0, "a");
        creator.setNumericValue(1, 1.5);
        creator.setNumericValue(2, 1);
        creator.commit();

        creator.setNominalValue(0, "b");
        creator.setNumericValue(1, 3.0);
        creator.setMissing(2);
        creator.commit();

        return creator.finish();
    }

    @Test
    public void evalAttributeWithSecond() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"int\"+ \"eger\",NUMERIC)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(1, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToDouble() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(lower(\"NUmerical\"),NUMERIC)", resolver);
            resolver.bind(exampleSet.getExample(0));

            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());
            assertEquals(1.5, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void evalAttributeWithSecondNotConstant() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("attribute(\"integer\",[nominal])", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void evalAttributeWithSecondToStringMissing() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            getExpressionWithFunctionsAndExamples("attribute(\"nominal\", MISSING_NOMINAL)", resolver);
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }


    @Test
    public void evalAttributeWithSecondToDate() {
        try {
            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamples("attribute(\"inte\"+\"ger\",DATE)", resolver);
            assertEquals(ExpressionType.TIMESTAMP, expression.getExpressionType());
            resolver.bind(exampleSet.getExample(0));
            expression.evaluateTimestamp();
            fail();
        } catch (ExpressionException e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void attributeFromVariable() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("attribute", "integer");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("attribute(%{attribute})", resolver, variableResolver);
            assertEquals(ExpressionType.DOUBLE, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals(1, expression.evaluateNumeric(), 1e-15);
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void attributeFromVariableWithType() {
        try {
            VariableHandler handler = new VariableHandler(null);
            handler.addVariable("attribute", "integer");
            VariableResolver variableResolver = new VariableResolver(handler);

            ExampleSet exampleSet = makeExampleSet();
            ExampleResolver resolver = new ExampleResolver(exampleSet);
            Expression expression = getExpressionWithFunctionsAndExamplesAndVariables("attribute(%{attribute},NOMINAL)", resolver, variableResolver);
            assertEquals(ExpressionType.NOMINAL, expression.getExpressionType());

            resolver.bind(exampleSet.getExample(0));
            assertEquals("1", expression.evaluateNominal());
        } catch (ExpressionException e) {
            fail(e.getMessage());
        }
    }

}
