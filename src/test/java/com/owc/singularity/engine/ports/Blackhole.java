package com.owc.singularity.engine.ports;


public class Blackhole extends SortedInputPort {

    public Blackhole(String name) {
        super(null, "blackhole-" + name, 1);
    }

    public Blackhole() {
        super(null, "blackhole", 1);
    }

    private final Object portsLock = new Object();
    private Ports<? extends Port> currentPorts = null;

    public void consume(OutputPort outputPort) {
        synchronized (portsLock) {
            currentPorts = outputPort.getPorts();
            outputPort.connectTo(this);
        }
    }

    @Override
    public Ports<? extends Port> getPorts() {
        synchronized (portsLock) {
            if (currentPorts != null) {
                return currentPorts;
            }
        }
        return super.getPorts();
    }

    @Override
    protected void fireUpdate() {
        // do nothing
    }

    @Override
    protected void fireUpdate(Port argument) {
        // do nothing
    }
}
