package com.owc.singularity.engine.process.parameter;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

import java.util.Collections;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeExpression;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;

@ExtendWith(MockitoExtension.class)
class ParameterTypeExpressionTest {

    @Spy
    Operator mockedOperator;

    static MockedStatic<OperatorService> operatorServiceMockedStatic;

    @SuppressWarnings("unchecked")
    @BeforeAll
    static void beforeAll() {
        operatorServiceMockedStatic = mockStatic(OperatorService.class);
        operatorServiceMockedStatic.when(() -> OperatorService.getOperatorDescription(any(Class.class)))
                .thenReturn(new OperatorDescription("owc", "mock", "mocked", Operator.class, "operator-name", "..", "testIcon"));
    }

    @AfterAll
    static void afterAll() {
        operatorServiceMockedStatic.close();
    }

    @BeforeEach
    void setUp() {
        AbstractPipeline mockedProcess = mock(AbstractPipeline.class);
        VariableHandler variableHandler = new VariableHandler(mockedProcess);
        variableHandler.addVariable("variable1", "5");
        given(mockedProcess.getVariableHandler()).willReturn(variableHandler);
        given(mockedOperator.getPipeline()).willReturn(mockedProcess);
    }


    @Test
    void getSimpleValueCorrectly() throws UndefinedParameterError {
        ParameterTypeExpression parameterType = new ParameterTypeExpression("key", "description");

        // given
        given(mockedOperator.getParameterTypes()).willReturn(Collections.singletonList(parameterType));

        // when
        mockedOperator.setParameter("key", "5+4*9");

        // then
        assertEquals("5+4*9", mockedOperator.getParameter("key"));
    }

    @Test
    void getValueWithVariablesWithoutReplace() throws UndefinedParameterError {
        ParameterTypeExpression parameterType = new ParameterTypeExpression("key", "description");
        // given
        given(mockedOperator.getParameterTypes()).willReturn(Collections.singletonList(parameterType));

        // when
        mockedOperator.setParameter("key", "eval(%{variable1})+4*9");

        // then
        assertEquals("eval(%{variable1})+4*9", mockedOperator.getParameter("key"));
    }

}
