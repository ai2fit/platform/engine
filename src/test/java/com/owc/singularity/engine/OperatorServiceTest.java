package com.owc.singularity.engine;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;
import com.owc.singularity.engine.operator.AnalysisRootOperator;
import com.owc.singularity.engine.operator.CompositeDummyOperator;
import com.owc.singularity.engine.operator.DummyOperator;
import com.owc.singularity.engine.operator.WebserviceRootOperator;
import com.owc.singularity.engine.tools.I18N;

class OperatorServiceTest {

    @BeforeAll
    static void beforeAll() {
        if (!I18N.isProviderAvailable()) {
            I18N.setProvider(new LocalI18NResourcesProvider());
        }
    }

    @Test
    void initSuccessfully() {
        OperatorService.init();
        // it should contain only the dummy, process roots
        assertThat(OperatorService.getOperatorKeys(), hasSize(8));
        assertNotNull(OperatorService.getOperatorDescription(DummyOperator.class));
        assertNotNull(OperatorService.getOperatorDescription(CompositeDummyOperator.class));
        assertNotNull(OperatorService.getOperatorDescription(AnalysisRootOperator.class));
        assertNotNull(OperatorService.getOperatorDescription(WebserviceRootOperator.class));
    }
}
