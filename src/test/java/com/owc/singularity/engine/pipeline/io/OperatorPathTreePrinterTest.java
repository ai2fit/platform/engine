package com.owc.singularity.engine.pipeline.io;

import static org.mockito.Mockito.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.pipeline.AnalysisPipeline;
import com.owc.singularity.engine.pipeline.OperatorPath;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;

class OperatorPathTreePrinterTest {


    @Test
    void formatSimpleTreePathCorrectly() {
        AbstractRootOperator root = spy(AnalysisRootOperator.class);
        ExecutionUnit unit = root.getSubprocess(0);
        SimpleOperator targetOperator = spy(SimpleOperator.class);
        Operator siblingOperatorBefore = spy(SimpleOperator.class);
        Operator siblingOperatorAfter = spy(SimpleOperator.class);
        unit.addOperator(siblingOperatorBefore);
        unit.addOperator(targetOperator);
        unit.addOperator(siblingOperatorAfter);
        OutputPort outputBefore = createOutput(siblingOperatorBefore);
        InputPort inputForTarget = createInput(targetOperator);
        OutputPort outputFromTarget = createOutput(targetOperator);
        InputPort inputAfter = createInput(siblingOperatorAfter);

        outputBefore.connectTo(inputForTarget);
        outputFromTarget.connectTo(inputAfter);

        AnalysisPipeline pipeline = new AnalysisPipeline(root);
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(OperatorPath.of(targetOperator), 3);
        root.walk(pathPrinter);
        String result = pathPrinter.getResult();
        Assertions.assertEquals("""
                + AnalysisPipeline[<unknown>] (Analysis Pipeline) [Analysis]
                |--- simple[-1] (Simple Operator)
                |==> simple (2)[-1] (Simple Operator)
                |--- simple (3)[-1] (Simple Operator)""", result);
    }

    @Test
    void formatTreePathSuppressingTooManySiblings() {
        AbstractRootOperator root = spy(AnalysisRootOperator.class);
        ExecutionUnit unit = root.getSubprocess(0);
        Operator targetOperator = spy(SimpleOperator.class);
        int siblingCount = 10;
        for (int i = 0; i < siblingCount / 2; i++) {
            SimpleOperator operator = spy(SimpleOperator.class);
            unit.addOperator(operator);
            createInput(operator);
            createOutput(operator);
        }
        unit.addOperator(targetOperator);
        createInput(targetOperator);
        createOutput(targetOperator);
        for (int i = 0; i < siblingCount / 2; i++) {
            SimpleOperator operator = spy(SimpleOperator.class);
            unit.addOperator(operator);
            createInput(operator);
            createOutput(operator);
        }
        for (int i = 1; i < unit.getOperators().size(); i++) {
            unit.getOperators().get(i - 1).getOutputPorts().getPortByIndex(0).connectTo(unit.getOperators().get(i).getInputPorts().getPortByIndex(0));
        }

        AnalysisPipeline pipeline = new AnalysisPipeline(root);
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(OperatorPath.of(targetOperator), 3);
        root.walk(pathPrinter);
        String result = pathPrinter.getResult();
        Assertions.assertEquals("""
                + AnalysisPipeline[<unknown>] (Analysis Pipeline) [Analysis]
                |   ...
                |--- simple (3)[-1] (Simple Operator)
                |--- simple (4)[-1] (Simple Operator)
                |--- simple (5)[-1] (Simple Operator)
                |==> simple (6)[-1] (Simple Operator)
                |--- simple (7)[-1] (Simple Operator)
                |--- simple (8)[-1] (Simple Operator)
                |--- simple (9)[-1] (Simple Operator)
                |   ...""", result);
    }

    @Test
    void formatTreePathWithOnlyConnectedOperators() {
        AbstractRootOperator root = spy(AnalysisRootOperator.class);
        ExecutionUnit unit = root.getSubprocess(0);
        Operator targetOperator = spy(SimpleOperator.class);
        int siblingCount = 10;
        for (int i = 0; i < siblingCount / 2; i++) {
            SimpleOperator operator = spy(SimpleOperator.class);
            unit.addOperator(operator);
            createInput(operator);
            createOutput(operator);
        }
        unit.addOperator(targetOperator);
        createInput(targetOperator);
        createOutput(targetOperator);
        for (int i = 0; i < siblingCount / 2; i++) {
            SimpleOperator operator = spy(SimpleOperator.class);
            unit.addOperator(operator);
            createInput(operator);
            createOutput(operator);
        }

        for (int i = 3; i < unit.getOperators().size(); i += 2) {
            unit.getOperators().get(i - 2).getOutputPorts().getPortByIndex(0).connectTo(unit.getOperators().get(i).getInputPorts().getPortByIndex(0));
        }
        AnalysisPipeline pipeline = new AnalysisPipeline(root);
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(OperatorPath.of(targetOperator), 3);
        root.walk(pathPrinter);
        String result = pathPrinter.getResult();
        Assertions.assertEquals("""
                + AnalysisPipeline[<unknown>] (Analysis Pipeline) [Analysis]
                |--- simple (2)[-1] (Simple Operator)
                |--- simple (4)[-1] (Simple Operator)
                |==> simple (6)[-1] (Simple Operator)
                |--- simple (8)[-1] (Simple Operator)
                |--- simple (10)[-1] (Simple Operator)""", result);
    }

    @ParameterizedTest
    @MethodSource("formatNestedTreePathCorrectly")
    void formatNestedTreePathCorrectly(int levelOneIndex, int levelTwoIndex) {
        AbstractRootOperator root = spy(AnalysisRootOperator.class);
        ExecutionUnit rootUnit = root.getSubprocess(0);
        SimpleOperatorChain firstLevel = mock(SimpleOperatorChain.class,
                withSettings().useConstructor((Object) new String[] { "First", "Second", "Third", "Fourth", "Fifth" }).defaultAnswer(CALLS_REAL_METHODS));
        SimpleOperatorChain secondLevel = mock(SimpleOperatorChain.class,
                withSettings().useConstructor((Object) new String[] { "One", "Two", "Three", "Four", "Five" }).defaultAnswer(CALLS_REAL_METHODS));
        ExecutionUnit unit = secondLevel.getSubprocess(levelOneIndex);
        SimpleOperator targetOperator = spy(SimpleOperator.class);
        Operator siblingOperatorBefore = spy(SimpleOperator.class);
        Operator siblingOperatorAfter = spy(SimpleOperator.class);
        unit.addOperator(siblingOperatorBefore);
        unit.addOperator(targetOperator);
        unit.addOperator(siblingOperatorAfter);

        OutputPort outputBefore = createOutput(siblingOperatorBefore);
        InputPort inputForTarget = createInput(targetOperator);
        OutputPort outputFromTarget = createOutput(targetOperator);
        InputPort inputAfter = createInput(siblingOperatorAfter);

        outputBefore.connectTo(inputForTarget);
        outputFromTarget.connectTo(inputAfter);

        firstLevel.getSubprocess(levelTwoIndex).addOperator(secondLevel);
        rootUnit.addOperator(firstLevel);
        AnalysisPipeline pipeline = new AnalysisPipeline(root);
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(OperatorPath.of(targetOperator), 3);
        root.walk(pathPrinter);
        String result = pathPrinter.getResult();
        System.out.println(result);
    }

    private static Stream<Arguments> formatNestedTreePathCorrectly() {
        // cartesian product of arguments
        return Stream.of(0, 1, 2, 3, 4).flatMap(a -> Stream.of(0, 1, 2, 3, 4).flatMap(b -> Stream.of(Arguments.of(a, b))));
    }

    private static InputPort createInput(Operator targetOperator) {
        return targetOperator.getInputPorts().createPort("input", 0);
    }

    private static OutputPort createOutput(Operator siblingOperatorBefore) {
        return siblingOperatorBefore.getOutputPorts().createPort("output", 0);
    }
}
