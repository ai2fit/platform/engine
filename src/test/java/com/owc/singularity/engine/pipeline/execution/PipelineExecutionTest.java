package com.owc.singularity.engine.pipeline.execution;


import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.EngineExecutionMode;
import com.owc.singularity.engine.concurrency.LocalConcurrencyExecutionService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.AnalysisPipeline.AnalysisPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.PortIdentifier;
import com.owc.singularity.test.repository.ResourcesMount;

@Disabled
public class PipelineExecutionTest {

    @BeforeAll
    static void init() {
        SingularityEngine.setExecutionMode(EngineExecutionMode.COMMAND_LINE);
        SingularityEngine.init(new LocalConcurrencyExecutionService());
    }

    @Test
    public void testNoContextError() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("MixedComputationWithLoops", AbstractPipeline.class);
            assertThrows(IllegalArgumentException.class, () -> PipelineExecutionBuilder.configure(pipeline).start());
        }
    }

    @Test
    public void testNoPathError() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("RetrieveData", AbstractPipeline.class);

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline)
                    .withExecutionContext(new AnalysisPipelineExecutionContext())
                    .start()
                    .waitForDone();
            assertEquals(PipelineExecutionState.ERROR, execution.getState());
            assertEquals(1, execution.getExceptions().size());
        }
    }

    @Test
    public void testExecution() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("MixedComputationWithLoops", AbstractPipeline.class);
            pipeline.setPath(mount.getMountRepositoryPath().resolve("MixedComputationWithLoops"));

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline)
                    .withExecutionContext(new AnalysisPipelineExecutionContext())
                    .start()
                    .waitForDone();
            assertEquals(PipelineExecutionState.FINISHED, execution.getState());
            assertTrue(() -> execution.getEndTimestamp() - execution.getStartTimestamp() > 0);
            assertEquals(ExampleSet.class, execution.getResults().get(new PortIdentifier("result 1")).getClass());
        }
    }

    @Test
    public void testExecutionInPool() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("MixedComputationWithLoops", AbstractPipeline.class);

            pipeline.setPath(mount.getMountRepositoryPath().resolve("MixedComputationWithLoops"));

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline)
                    .withExecutionContext(new AnalysisPipelineExecutionContext())
                    .using(new ForkJoinPool(4))
                    .start()
                    .waitForDone();
            assertEquals(PipelineExecutionState.FINISHED, execution.getState());
            assertTrue(() -> execution.getEndTimestamp() - execution.getStartTimestamp() > 0);
            assertNotNull(execution.getResults().get(new PortIdentifier("result 1")));
            assertEquals(ExampleSet.class, execution.getResults().get(new PortIdentifier("result 1")).getClass());
        }
    }

    @Test
    public void testVariables() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("MixedComputationWithLoops", AbstractPipeline.class);

            pipeline.setPath(mount.getMountRepositoryPath().resolve("MixedComputationWithLoops"));

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline)
                    .withExecutionContext(new AnalysisPipelineExecutionContext(Map.of("widthIterations", "1")))
                    .start()
                    .waitForDone();
            assertEquals(PipelineExecutionState.FINISHED, execution.getState());
            assertTrue(() -> execution.getEndTimestamp() - execution.getStartTimestamp() > 0);
            PortIdentifier exampleSetPort = new PortIdentifier("result 1");
            assertNotNull(execution.getResults().get(exampleSetPort));
            assertEquals(ExampleSet.class, execution.getResults().get(exampleSetPort).getClass());
            assertEquals(4, ((ExampleSet) execution.getResults().get(exampleSetPort)).size());
        }
    }

    @Test
    public void testWaitingNoPathError() throws IOException, URISyntaxException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("RetrieveData", AbstractPipeline.class);

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline).withExecutionContext(new AnalysisPipelineExecutionContext()).start();
            assertThrows(OperatorException.class, () -> execution.waitForResults());
            assertEquals(PipelineExecutionState.ERROR, execution.getState());
            assertEquals(1, execution.getExceptions().size());
        }
    }

    @Test
    public void testWaitingExecution() throws IOException, URISyntaxException, OperatorException {
        try (ResourcesMount mount = new ResourcesMount("com/owc/singularity/resources/pipelines/")) {
            AbstractPipeline pipeline = mount.load("MixedComputationWithLoops", AbstractPipeline.class);
            pipeline.setPath(mount.getMountRepositoryPath().resolve("MixedComputationWithLoops"));

            PipelineExecution execution = PipelineExecutionBuilder.configure(pipeline).withExecutionContext(new AnalysisPipelineExecutionContext()).start();
            Map<PortIdentifier, IOObject> results = execution.waitForResults();
            assertEquals(PipelineExecutionState.FINISHED, execution.getState());
            assertTrue(() -> execution.getEndTimestamp() - execution.getStartTimestamp() > 0);
            assertEquals(ExampleSet.class, results.get(new PortIdentifier("result 1")).getClass());
        }
    }
}
