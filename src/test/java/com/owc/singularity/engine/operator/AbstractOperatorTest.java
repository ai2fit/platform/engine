package com.owc.singularity.engine.operator;

import static com.owc.singularity.engine.operator.TimingTestHelpers.between;
import static com.owc.singularity.engine.operator.TimingTestHelpers.sleepFor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.api.parallel.Isolated;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InOrder;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.ports.Blackhole;

@Isolated
@Execution(ExecutionMode.SAME_THREAD)
class AbstractOperatorTest {

    @RepeatedTest(10)
    void shouldRunStagesInOrder() throws OperatorException {
        // given
        SimpleOperator operator = spy(SimpleOperator.class);
        String firstStage = "stage-1";
        String secondStage = "stage-2";
        String thirdStage = "stage-3";
        SimpleOperator.StageFunction firstStageFunction = mock(SimpleOperator.StageFunction.class);
        SimpleOperator.StageFunction secondStageFunction = mock(SimpleOperator.StageFunction.class);
        SimpleOperator.StageFunction thirdStageFunction = mock(SimpleOperator.StageFunction.class);
        operator.addStage(firstStage, firstStageFunction);
        operator.addStage(secondStage, secondStageFunction);
        operator.addStage(thirdStage, thirdStageFunction);
        // 2 -> 1 -> 3
        operator.registerStageDependency(thirdStage, firstStage);
        operator.registerStageDependency(firstStage, secondStage);

        operator.defineOutputPort("output", 1, operationDescriptionContext -> null, (operationExecutionContext, unused) -> null);
        operator.registerOutputDependency("output", firstStage, secondStage, thirdStage);

        Blackhole blackhole = new Blackhole();
        blackhole.consume(operator.getOutputPorts().getPortByName("output"));

        // when
        operator.doWork();

        // then
        InOrder order = inOrder(firstStageFunction, secondStageFunction, thirdStageFunction);
        order.verify(secondStageFunction).apply(any(), any());
        order.verify(firstStageFunction).apply(any(), any());
        order.verify(thirdStageFunction).apply(any(), any());
    }

    @ParameterizedTest
    @ValueSource(ints = { 2, 3, 4, 5 })
    @Disabled("We do not support work-stealing from other stages, this would be a good case for VirtualThreads")
    void shouldStealWorkFromWaitingStages(int factor) throws OperatorException, ExecutionException, InterruptedException {
        // we create tasks more than we can distribute threads for i.e., tasks > threads
        int parallelism = ForkJoinPool.commonPool().getParallelism();
        int numberOfStages = parallelism * factor;
        long eachRunTimeCost = 500;

        // given
        CountDownLatch latch = new CountDownLatch(numberOfStages);
        CompletableFuture<Long> stealableWorkDoneIn = CompletableFuture.supplyAsync(() -> {
            try {
                latch.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return System.currentTimeMillis();
        });
        SimpleOperator operator = spy(SimpleOperator.class);
        String[] stages = new String[numberOfStages];
        for (int i = 0; i < numberOfStages; i++) {
            stages[i] = "stage-" + i;
            operator.addStage(stages[i], (operationExecutionContext, unused) -> {
                // stealable work
                latch.countDown();
                // waiting
                sleepFor(eachRunTimeCost);
                // result
                return null;
            });
        }

        operator.defineOutputPort("output", 1, operationDescriptionContext -> null, (operationExecutionContext, unused) -> null);
        operator.registerOutputDependency("output", stages);

        Blackhole blackhole = new Blackhole();
        blackhole.consume(operator.getOutputPorts().getPortByName("output"));

        // when
        long startTime = System.currentTimeMillis();
        operator.doWork();
        double stealableWorkTook = stealableWorkDoneIn.get() - startTime;
        // threads that are waiting should steal work from other 'tasks'
        // therefore the time to execute all tasks 'work'
        assertThat(stealableWorkTook, is(between(eachRunTimeCost * 1.0, eachRunTimeCost * 1.3 + parallelism)));
    }

}
