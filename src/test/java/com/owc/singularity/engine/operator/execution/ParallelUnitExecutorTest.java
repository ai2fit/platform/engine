package com.owc.singularity.engine.operator.execution;

import static com.owc.singularity.engine.operator.TimingTestHelpers.between;
import static com.owc.singularity.engine.operator.TimingTestHelpers.sleepFor;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.api.parallel.Isolated;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InOrder;
import org.mockito.Mockito;

import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.ports.Blackhole;
import com.owc.singularity.engine.tools.I18N;

@Isolated
@Execution(ExecutionMode.SAME_THREAD)
class ParallelUnitExecutorTest {

    @BeforeAll
    static void beforeAll() {
        if (!I18N.isProviderAvailable())
            I18N.setProvider(new LocalI18NResourcesProvider());
    }

    @Test
    void shouldRunSimpleUnitSuccessfully() throws OperatorException {
        OperatorChain operatorChain = spy(AnalysisRootOperator.class);
        ExecutionUnit unit = operatorChain.getSubprocess(0);
        Operator operator = spy(SimpleOperator.class);
        unit.addOperator(operator);

        ParallelUnitExecutor executor = new ParallelUnitExecutor();
        executor.execute(unit);

        then(operator).should().execute();
    }

    @ParameterizedTest
    @ValueSource(longs = { 0, 200, 500, 1000, 1500 })
    void shouldRunIndependentOperatorsInParallel(long eachRunTimeCost) throws OperatorException {
        OperatorChain operatorChain = spy(AnalysisRootOperator.class);
        ExecutionUnit unit = operatorChain.getSubprocess(0);
        int parallelism = ForkJoinPool.commonPool().getParallelism();
        for (int i = 0; i < parallelism; i++) {
            SimpleOperator operator = spy(SimpleOperator.class);
            operator.addAction("action", (operationExecutionContext, unused) -> {
                sleepFor(eachRunTimeCost);
            });
            unit.addOperator(operator);
        }
        ParallelUnitExecutor executor = new ParallelUnitExecutor();
        long startTime = System.currentTimeMillis();
        executor.execute(unit);
        if (eachRunTimeCost == 0) {
            // warm up
            return;
        }
        double totalTimeToExecute = System.currentTimeMillis() - startTime;
        // n tasks / n threads = n * time to run each
        // plus additional logic cost and thread creation ~1ms per thread
        assertThat(totalTimeToExecute, is(between(eachRunTimeCost * 1.0, eachRunTimeCost * 1.3 + parallelism)));
    }

    @ParameterizedTest
    @ValueSource(ints = { 0, 2, 5 })
    void shouldStealWorkFromWaitingOperators(int numberOfLayers) throws OperatorException {
        OperatorChain operatorChain = new AnalysisRootOperator();
        ExecutionUnit unit = operatorChain.getSubprocess(0);
        // we create tasks more than we can distribute threads for i.e. tasks > threads
        int parallelism = ForkJoinPool.commonPool().getParallelism();
        long eachRunTimeCost = 500;
        SimpleOperator[][] operatorGraph = new SimpleOperator[numberOfLayers][parallelism];
        // where N = numberOfLayers and P = parallelism
        // layer 1 | layer 2 |... | layer N
        // op0 ----> op0 ---->... ---> blackhole
        // op1 ----> op1 ---->... ---> blackhole
        // ...
        // opP ----> opP ---->... ---> blackhole
        for (int layer = 0; layer < numberOfLayers; layer++) {
            final int finalLevel = layer;
            for (int i = 0; i < parallelism; i++) {
                final int operatorNumber = i;
                SimpleOperator operator = spy(SimpleOperator.class);
                operator.addOutputPort("output", null, (operationExecutionContext, unused) -> {
                    sleepFor(eachRunTimeCost);
                    return mock(IOObject.class, "layer-" + finalLevel + "-output-" + operatorNumber);
                });
                operatorGraph[layer][i] = operator;
                unit.addOperator(operator);
                if (layer > 0) {
                    // intermediate layer
                    operator.addInputPortExtender("blackhole", null, 0);
                    SimpleOperator previousOperator = operatorGraph[layer - 1][operatorNumber];
                    previousOperator.getOutputPorts().getPortByName("output").connectTo(operator.getInputPorts().getPortByIndex(0));
                }
                if (layer == numberOfLayers - 1) {
                    // last layer
                    Blackhole blackhole = new Blackhole("last-layer");
                    blackhole.consume(operator.getOutputPorts().getPortByName("output"));
                }
            }
        }
        ParallelUnitExecutor executor = new ParallelUnitExecutor();
        long startTime = System.currentTimeMillis();
        executor.execute(unit);
        double totalTimeToExecute = System.currentTimeMillis() - startTime;
        if (numberOfLayers == 0) {
            // warm up
            return;
        }

        for (int i = 0; i < parallelism; i++) {
            // check the same level across layers
            final int index = i;
            SimpleOperator[] operatorDependencyRow = Arrays.stream(operatorGraph).map(graphLevel -> graphLevel[index]).toArray(SimpleOperator[]::new);
            // whether each layer was executed before the next one
            InOrder order = Mockito.inOrder((Object[]) operatorDependencyRow);
            for (int level = 0; level < numberOfLayers; level++) {
                order.verify(operatorDependencyRow[level]).execute();
            }
        }
        // layers * n tasks / n threads -> (time to run each) * layers
        // plus additional logic cost and thread creation ~1ms per thread
        assertThat(totalTimeToExecute, is(between(eachRunTimeCost * 1.0, numberOfLayers * eachRunTimeCost * 1.3 + parallelism)));
    }

}
