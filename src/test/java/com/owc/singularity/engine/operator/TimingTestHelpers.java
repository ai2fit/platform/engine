package com.owc.singularity.engine.operator;

import static org.hamcrest.Matchers.*;

public class TimingTestHelpers {

    public static <T extends java.lang.Comparable<T>> org.hamcrest.Matcher<T> between(T lowerBound, T upperBound) {
        return both(greaterThanOrEqualTo(lowerBound)).and(lessThan(upperBound));
    }

    public static void sleepFor(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
