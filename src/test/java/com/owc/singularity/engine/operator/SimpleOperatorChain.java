package com.owc.singularity.engine.operator;

import com.owc.singularity.engine.operator.annotations.DefinesOperator;

@DefinesOperator(key = "simple_operator_chain", module = "test", shortName = "simple chain", group = "tests", icon = "", name = "Simple Operator Chain")
public abstract class SimpleOperatorChain extends OperatorChain {

    public SimpleOperatorChain(String... subprocessNames) {
        super(subprocessNames);
    }


}
