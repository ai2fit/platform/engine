package com.owc.singularity.engine.operator;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.tools.function.ExceptionHandler;

@DefinesOperator(key = "simple_operator", module = "test", shortName = "simple", group = "tests", icon = "", name = "Simple Operator")
public abstract class SimpleOperator extends AbstractOperator<Void> {

    public void addAction(String key, ExceptionHandler.ThrowingBiConsumer<OperationExecutionContext, Void> action) {
        defineAction(key, action);
    }

    public void addStage(String name, OutputFunction<OperationExecutionContext, Void, Object> resultProvider) {
        defineStage(name, resultProvider);
    }

    public void addOutputPort(String name, ExceptionHandler.ThrowingFunction<OperationDescriptionContext, ? extends MetaData> metaDataProvider,
            OutputFunction<OperationExecutionContext, Void, IOObject> resultProvider) {
        defineOutputPort(name, 0, metaDataProvider, resultProvider);
    }

    public void addInputPortExtender(String name, MetaData desiredMetaData, int numberOfMinimalRequiredConnections) {
        defineInputPortExtender(name, 0, desiredMetaData, numberOfMinimalRequiredConnections);
    }

    @Override
    protected Void configure(OperationExecutionContext operationContext, InputValidator inputValidator) {
        return null;
    }

    public abstract static class StageFunction implements AbstractOperator.OutputFunction<AbstractOperator.OperationExecutionContext, Void, Object> {

    }
}
