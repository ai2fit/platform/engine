/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.test.asserter;


import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.*;

import org.junit.jupiter.api.Assertions;
import org.opentest4j.AssertionFailedError;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ConfigurationBuilder;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.Attributes;
import com.owc.singularity.engine.object.data.exampleset.Example;


/**
 * Extension for JUnit's Assert for testing SingularityEngine objects.
 *
 * @author Simon Fischer, Marcin Skirzynski, Marius Helf
 *
 */
public class AsserterService {

    public static final double DELTA = 0.000000001;
    public static final double MAX_RELATIVE_ERROR = 0.000000001;

    public static final AsserterRegistry ASSERTER_REGISTRY = new AsserterRegistry();
    private static final boolean ignoreRepositoryNameForSourceAnnotation = true;
    private static boolean initialized = false;

    public static void initialize() {
        if (isInitialized())
            return;
        Reflections reflections = new Reflections(new ConfigurationBuilder().forPackage("com.owc.singularity.test")
                .setScanners(Scanners.SubTypes)
                .addClassLoaders(ModuleService.getMajorClassLoader()));

        // operator specific rules
        reflections.get(Scanners.SubTypes.with(AsserterFactory.class).asClass()).stream().forEach(asserterClass -> {
            try {
                AsserterFactory asserterFactory = (AsserterFactory) asserterClass.getDeclaredConstructor().newInstance();
                ASSERTER_REGISTRY.registerAllAsserters(asserterFactory);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                    | SecurityException e) {
            }
        });
        initialized = true;
    }

    public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Returns <code>true</code> if the ioobjects class is supported for comparison in the test
     * extension and <code>false</code> otherwise.
     */
    public static boolean comparable(IOObject ioobject) {
        return ASSERTER_REGISTRY.getAsserterForObject(ioobject) != null;
    }

    /**
     * Returns <code>true</code> if both ioobject classes are comparable to each other and
     * <code>false</code> otherwise.
     */
    public static boolean comparable(IOObject ioobject1, IOObject ioobject2) {
        return ASSERTER_REGISTRY.getAsserterForObjects(ioobject1, ioobject2) != null;
    }

    /**
     * Extends the Junit assertEquals method by additionally checking the doubles for NaN.
     *
     * @param message
     *            message to display if an error occurs
     * @param expected
     *            expected value
     * @param actual
     *            actual value
     */
    public static void assertEqualsNaN(String message, double expected, double actual) {
        if (Double.isNaN(expected)) {
            if (!Double.isNaN(actual)) {
                throw new AssertionFailedError(message + " expected: <" + expected + "> but was: <" + actual + ">");
            }
        } else {
            Assertions.assertEquals(expected, actual, DELTA, message);
        }
    }

    /**
     * Attention: Does not work with values near 0!!
     */
    public static void assertEqualsWithRelativeErrorOrBothNaN(String message, double expected, double actual) {
        if (expected == actual) {
            return;
        }

        if (Double.isNaN(expected) && !Double.isNaN(actual)) {
            throw new AssertionFailedError(message + " expected: <" + expected + "> but was: <" + actual + ">");
        }

        if (!Double.isNaN(expected) && Double.isNaN(actual)) {
            throw new AssertionFailedError(message + " expected: <" + expected + "> but was: <" + actual + ">");
        }

        double relativeError;
        if (Math.abs(actual) > Math.abs(expected)) {
            relativeError = Math.abs((expected - actual) / actual);
        } else {
            relativeError = Math.abs((expected - actual) / expected);
        }
        if (relativeError > MAX_RELATIVE_ERROR) {
            throw new AssertionFailedError(message + " expected: <" + expected + "> but was: <" + actual + ">");
        }
    }

    /**
     * Tests two attributes by using the name, type, block, type, default value and the nominal
     * mapping
     *
     * @param message
     *            message to display if an error occurs
     * @param expected
     *            expected value
     * @param actual
     *            actual value
     */
    public static void assertEquals(String message, Attribute expected, Attribute actual) {
        Assertions.assertEquals(expected.getName(), actual.getName(), message + " (attribute name)");
        Assertions.assertEquals(expected.getValueType(), actual.getValueType(), message + " (attribute type of attribute '" + expected.getName()
                + "': expected '" + expected.getValueType().toString() + "' but was '" + actual.getValueType().toString() + "')");
    }


    public static void assertArrayEquals(String message, byte[] expected, byte[] actual) {
        if (expected == null) {
            Assertions.assertNull(actual);
            return;
        }
        if (actual == null) {
            throw new AssertionFailedError(message + " (expected " + Arrays.toString(expected) + " , but is null)");
        }
        Assertions.assertEquals(expected.length, actual.length, message + " (array length is not equal)");
        for (int i = 0; i < expected.length; i++) {
            Assertions.assertEquals(expected[i], actual[i], message);
        }
    }

    /**
     * Compares a string linewise, i.e. ignores different linebreak characters.
     *
     * Does this by incrementally reading all expected an actual lines and comparing them linewise.
     *
     * @param message
     * @param expected
     * @param actual
     */
    public static void assertLinewiseEquals(String message, String expected, String actual) {
        try (Scanner expectedScanner = new Scanner(expected); Scanner actualScanner = new Scanner(actual)) {

            String expectedLine = null;
            String actualLine = null;
            int lineCounter = 1;
            while (expectedScanner.hasNextLine()) {
                expectedLine = expectedScanner.nextLine();
                if (actualScanner.hasNextLine()) {
                    actualLine = actualScanner.nextLine();
                } else {
                    Assertions.fail("Line " + lineCounter + ": actual input has less lines then expected result! Expected: " + expectedLine);
                }
                Assertions.assertEquals(expectedLine, actualLine,
                        "Line " + lineCounter + ": " + message + "\n\nExpected:\n" + expected + "\nActual:\n" + actual);
                ++lineCounter;
            }
        }
    }

    /**
     * Tests if both list of ioobjects are equal.
     *
     * @param expected
     *            expected value
     * @param actual
     *            actual value
     */
    public static void assertEquals(String message, List<IOObject> expected, List<IOObject> actual) {
        assertSize(expected, actual);

        Iterator<IOObject> expectedIter = expected.iterator();
        Iterator<IOObject> actualIter = actual.iterator();

        int objectIndex = 1;
        while (expectedIter.hasNext() && actualIter.hasNext()) {
            IOObject expectedIOO = expectedIter.next();
            IOObject actualIOO = actualIter.next();
            String subMessage = message + " IOObject \"" + actualIOO.getSource() + "\" at position " + objectIndex + " does not match the expected value ";
            assertEquals(subMessage, expectedIOO, actualIOO);
            objectIndex++;
        }

    }

    /**
     * Tests if both lists of IOObjects have the same size.
     *
     * @param expected
     * @param actual
     */
    public static void assertSize(List<IOObject> expected, List<IOObject> actual) {
        Assertions.assertEquals(expected.size(), actual.size(),
                "Number of connected output ports in the process is not equal with the number of ioobjects contained in the same folder with the format 'processname-expected-port-1', 'processname-expected-port-2', ...");
    }

    /**
     * Tests if the two IOObjects are equal and appends the given message.
     *
     * @param expectedIOO
     * @param actualIOO
     */
    public static void assertEquals(String message, IOObject expectedIOO, IOObject actualIOO) {
        assertEquals(message, expectedIOO, actualIOO, false);
    }

    /**
     * Tests if the two IOObjects are equal and appends the given message.
     *
     * @param assertEqualAnnotations
     *            if true, annotations will be compared. If false, they will be ignored.
     * @param expectedIOO
     * @param actualIOO
     */
    public static void assertEquals(String message, IOObject expectedIOO, IOObject actualIOO, boolean assertEqualAnnotations) {

        /*
         * Do not forget to add a newly supported class to the ASSERTER_REGISTRY!!!
         */
        List<Asserter> asserterList = ASSERTER_REGISTRY.getAsserterForObjects(expectedIOO, actualIOO);
        if (asserterList != null) {
            for (Asserter asserter : asserterList) {
                asserter.assertEquals(message, expectedIOO, actualIOO);
            }
        } else {
            throw new AssertionFailedError(
                    "Comparison of the two given IOObject classes " + expectedIOO.getClass() + " and " + actualIOO.getClass() + " is not supported. ",
                    expectedIOO.toString(), actualIOO.toString());
        }
    }

    /**
     * Tests the two examples by testing the value of the examples for every given attribute. This
     * method is sensitive to the regular attribute ordering.
     *
     * @param message
     *            message to display if an error occurs. If it contains "{0}" and "{1}", it will be
     *            replaced with the attribute name and attribute type, if an inequality occurs.
     * @param expected
     *            expected value
     * @param actual
     *            actual value
     */
    public static void assertEquals(String message, Example expected, Example actual) {
        Assertions.assertEquals(expected.getAttributes().allSize(), actual.getAttributes().allSize(), message + " (number of attributes)");

        Assertions.assertEquals(expected.getAttributes().specialSize(), actual.getAttributes().specialSize(), message + " (number of special attributes)");

        // get all attributes as list
        Iterator<Attribute> allExpectedAttributesIterator = expected.getAttributes().allAttributes();
        Iterator<Attribute> allActualAttributesIterator = actual.getAttributes().allAttributes();
        List<Attribute> allExpectedAttributes = new ArrayList<Attribute>();
        while (allExpectedAttributesIterator.hasNext()) {
            allExpectedAttributes.add(allExpectedAttributesIterator.next());
        }
        List<Attribute> allActualAttributes = new ArrayList<Attribute>();
        while (allActualAttributesIterator.hasNext()) {
            allActualAttributes.add(allActualAttributesIterator.next());
        }

        // get regular attributes as iterator
        Iterator<Attribute> expectedAttributesToConsider = expected.getAttributes().iterator();
        Iterator<Attribute> actualAttributesToConsider = actual.getAttributes().iterator();

        // first check regular attributes sensitive to the attribute ordering
        while (expectedAttributesToConsider.hasNext() && actualAttributesToConsider.hasNext()) {
            Attribute a1 = expectedAttributesToConsider.next();
            Attribute a2 = actualAttributesToConsider.next();
            if (!a1.getName().equals(a2.getName())) {
                // this should have been detected by previous checks already
                throw new AssertionFailedError("Attribute ordering does not match: " + a1.getName() + "," + a2.getName());
            }

            if (a1.isNominal()) {
                Assertions.assertEquals(expected.getNominalValue(a1), actual.getNominalValue(a2), MessageFormat.format(message, "nominal", a1.getName()));
            } else if (a1.isNumerical()) {
                assertEqualsWithRelativeErrorOrBothNaN(MessageFormat.format(message, "numerical", a1.getName()), expected.getNumericValue(a1),
                        actual.getNumericValue(a2));
            } else {
                Assertions.assertEquals(expected.getTimestampValue(a1), actual.getTimestampValue(a2));
            }

            // passed, so delete regular attribute from all attributes list.
            allExpectedAttributes.remove(a1);
            allActualAttributes.remove(a2);
        }

        // check the remaining special attributes
        for (int i = 0; i < allExpectedAttributes.size(); i++) {
            Attribute expectedSpecial = allExpectedAttributes.get(i);
            String expectedName = expectedSpecial.getName();
            String actualName = null;
            for (int j = 0; j < allActualAttributes.size(); j++) {
                Attribute actualSpecial = allActualAttributes.get(j);
                actualName = actualSpecial.getName();
                if (expectedName.equals(actualName)) {
                    if (expectedSpecial.isNominal()) {
                        Assertions.assertEquals(MessageFormat.format(message, "nominal", expectedSpecial.getName()), expected.getNominalValue(expectedSpecial),
                                actual.getNominalValue(actualSpecial));
                    } else if (expectedSpecial.isNumerical()) {
                        assertEqualsWithRelativeErrorOrBothNaN(MessageFormat.format(message, "numerical", expectedSpecial.getName()),
                                expected.getNumericValue(expectedSpecial), actual.getNumericValue(actualSpecial));
                    } else {
                        Assertions.assertEquals(expected.getTimestampValue(expectedSpecial), actual.getTimestampValue(actualSpecial));
                    }
                    // remove from list
                    allExpectedAttributes.remove(expectedSpecial);
                    allActualAttributes.remove(actualSpecial);
                    i--;
                    break;
                }
            }
            if (!expectedName.equals(actualName)) {
                throw new AssertionFailedError("Expected attribute not found: " + expectedSpecial.getName());
            }
        }

    }

    /**
     * Tests if all attributes are equal. This method is sensitive to the regular attribute
     * ordering.
     *
     * Optionally compares the default values of the attributes. The default value is only relevant
     * for sparse data rows, so it should not be compared for non-sparse data.
     *
     * @param message
     *            message to display if an error occurs
     * @param expected
     *            expected value
     * @param actual
     *            actual value
     * @param compareDefaultValues
     *            specifies if the attributes default values should be compared.
     */
    public static void assertEquals(String message, Attributes expected, Attributes actual) {
        Assertions.assertEquals(expected, actual);
    }
}
