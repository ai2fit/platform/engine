package com.owc.singularity.test.repository;

import java.io.Closeable;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.file.FileRepositoryMount;

/**
 * This class allows to conveniently load an Repository Entry from the resources temporarily under a
 * unique path. This can be used for tests requiring objects or pipelines.
 */
public class ResourcesMount implements AutoCloseable, Closeable {

    private final RepositoryPath mountRepositoryPath;

    public ResourcesMount(String resourceName) throws IOException, URISyntaxException {
        this(resourceName, ModuleService.getMajorClassLoader());
    }

    public ResourcesMount(String resourceName, ClassLoader classLoader) throws IOException, URISyntaxException {
        this(resourceName, UUID.randomUUID().toString(), classLoader);
    }

    private ResourcesMount(String resourceName, String mountPath, ClassLoader classLoader) throws IOException, URISyntaxException {
        // mount the folder into the repository, so we can load data from it
        Path localTestDataFolderPath = Path.of(Objects.requireNonNull(classLoader.getResource(resourceName)).toURI());
        mountRepositoryPath = RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(mountPath);
        RepositoryManager.getFileSystem()
                .mount(mountRepositoryPath, FileRepositoryMount.TYPE_NAME,
                        Map.of(FileRepositoryMount.OPTION_BASE_DIRECTORY, localTestDataFolderPath.toString(), FileRepositoryMount.OPTION_READ_ONLY, "true"));
    }

    public <T> T load(String path, Class<T> clazz) throws IOException {
        return Entries.loadData(mountRepositoryPath.resolve(path), clazz, ModuleService.getMajorClassLoader());
    }

    public RepositoryPath getMountRepositoryPath() {
        return mountRepositoryPath;
    }

    @Override
    public void close() throws IOException {
        // remove mount
        RepositoryManager.getFileSystem().unmount(mountRepositoryPath, false);
    }
}
