package com.owc.singularity.tools;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;

import java.time.Duration;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeCategory;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeInt;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeTupel;

public class TimeTools {

    static final ChronoUnit[] timeUnits = new ChronoUnit[] { MILLIS, SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS };
    static final String[] timeUnitNames = new String[] { MILLIS.name(), SECONDS.name(), MINUTES.name(), HOURS.name(), DAYS.name(), WEEKS.name(), MONTHS.name(),
            YEARS.name() };
    static final int unitSecondsIndex = 1;

    public static String getShortForm(ChronoUnit chrono) {
        if (chrono == null)
            return "";

        return switch (chrono) {
            case DAYS -> "d";
            case HOURS -> "h";
            case MILLIS -> "ms";
            case MINUTES -> "m";
            case MONTHS -> "M";
            case SECONDS -> "s";
            case WEEKS -> "w";
            case YEARS -> "y";
            default -> "";
        };
    }

    public static ParameterType getOnlyUnitParameterType(String key, String description, boolean expert) {
        return new ParameterTypeCategory(key, "Select time unit.", timeUnitNames, unitSecondsIndex);
    }

    public static ParameterType getDurationParameterTypeTupel(String key, String description, boolean optional, boolean expert, int minValue, int maxValue) {
        ParameterType type = new ParameterTypeTupel(key, description, new ParameterTypeInt("time_value", "Set time value.", minValue, maxValue, optional),
                new ParameterTypeCategory("Select_time_unit", "Select time unit.", timeUnitNames, unitSecondsIndex));
        type.setOptional(optional);
        return type;
    }

    public static ParameterType getDurationParameterTypeTupel(String key, String description, int defaultValue, boolean expert, int minValue, int maxValue) {
        ParameterType type = new ParameterTypeTupel(key, description, new ParameterTypeInt("time_value", "Set time value.", minValue, maxValue, defaultValue),
                new ParameterTypeCategory("Select_time_unit", "Select time unit.", timeUnitNames, unitSecondsIndex));
        type.setOptional(false);
        return type;
    }

    public static ChronoUnit evaluateTimeUnitFromParameter(String key, Operator operator) throws UserError {
        String unit = operator.getParameterAsString(key);
        return ChronoUnit.valueOf(unit);
    }

    public static TemporalAmount transformParameterToTemporalAmount(String value) throws NumberFormatException {
        String[] tupel = ParameterTypeTupel.transformString2Tupel(value);
        ChronoUnit chronoUnit = ChronoUnit.valueOf(tupel[1]);
        int intValue = Integer.parseInt(tupel[0]);
        if (chronoUnit.isTimeBased())
            return Duration.of(intValue, chronoUnit);
        else {
            return switch (chronoUnit) {
                case DAYS -> Period.ofDays(intValue);
                case WEEKS -> Period.ofWeeks(intValue);
                case MONTHS -> Period.ofMonths(intValue);
                case YEARS -> Period.ofYears(intValue);
                default -> throw new NumberFormatException("Not a valid unit");
            };
        }
    }

    /**
     * This method returns the parameter value as a temporal amount.
     * 
     * @param key
     *            parameter key
     * @param operator
     *            operator
     * @return The temporal amount or null if no integer value is given.
     * @throws UserError
     *             If the parameter is not set or the value is illegal.
     */
    public static TemporalAmount getParameterAsTemporalAmount(String key, Operator operator) throws UserError {

        try {
            String value = operator.getParameterAsString(key);
            String[] tupel = ParameterTypeTupel.transformString2Tupel(value);
            if (tupel[0] == null || tupel[0].trim().isEmpty())
                return null;
            return transformParameterToTemporalAmount(value);
        } catch (NumberFormatException e) {
            throw new UserError(operator, e, "toolkit.illegal_numeric_parameter_value", key);
        }
    }
}
