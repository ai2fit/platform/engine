package com.owc.singularity.tools;


import java.util.regex.Pattern;

/**
 * A class holding methods useful for Strings
 *
 * @author Sigrun Fraunhoffer
 *
 */
public class StringTools {

    /**
     * This method is to rename a String to UpperCamelCase and split tokens on everything that is
     * not a letter or a digit.
     */
    public static String toCamelCase(String name) {
        return toCamelCase(name, Pattern.compile("[\\W_]"));
    }

    /**
     * This method is to rename a String to UpperCamelCase and split tokens on an adjustable
     * splitPattern.
     */
    public static String toCamelCase(String name, Pattern splitPattern) {
        String[] tokens = splitPattern.split(name);
        StringBuilder newName = new StringBuilder();
        for (String token : tokens) {
            if (!token.isEmpty()) {
                newName.append(token.substring(0, 1).toUpperCase()).append(token.substring(1).toLowerCase());
            }
        }
        return newName.toString();
    }

}
