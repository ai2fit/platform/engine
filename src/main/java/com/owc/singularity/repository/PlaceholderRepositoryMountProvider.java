package com.owc.singularity.repository;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;


/**
 * An SPI provider for creating {@link PlaceholderRepositoryMount} with
 * {@value PlaceholderRepositoryMount#TYPE_NAME} type
 * 
 * @author Hatem Hamad
 */
public class PlaceholderRepositoryMountProvider implements RepositoryMountProvider {

    @Override
    public String getTypeId() {
        return PlaceholderRepositoryMount.TYPE_NAME;
    }

    @Override
    public RepositoryMount create(RepositoryPath mountPath, Map<String, String> options) throws IOException {
        return new PlaceholderRepositoryMount(mountPath, options);
    }

    @Override
    public Map<String, String> validate(Map<String, String> options) {
        return Collections.emptyMap();
    }

    @Override
    public Set<String> getOptionsKeys() {
        return Collections.emptySet();
    }

    @Override
    public boolean isOptionRequired(String optionKey) {
        return false;
    }

    @Override
    public boolean allowsAdditionalOptions() {
        return false;
    }

    @Override
    public Class<?> getOptionType(String optionKey) {
        return String.class;
    }

    @Override
    public boolean isOptionConfidential(String optionKey) {
        return false;
    }

    @Override
    public String getOptionDefaultValue(String optionKey) {
        return null;
    }
}
