package com.owc.singularity.repository;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.reflections.Reflections;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.security.CredentialStorage;

/**
 * This is the main management point for managing the repositories. It offers more high level
 * functionality than the raw nio API. It should be used to create, remove and configure
 * {@link RepositoryFileSystem}s and mount {@link RepositoryMount}s. It offers functionality to
 * persist the configuration and reload it from disk.
 * 
 * @author Sebastian Land, Hatem Hamad
 *
 */
public class RepositoryManager {

    public static class RepositoryMountConfiguration {

        private final String mountType;

        private final Map<String, String> options;

        @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
        public RepositoryMountConfiguration(@JsonProperty("mountType") String mountType, @JsonProperty("options") Map<String, String> options) {
            this.mountType = mountType;
            this.options = options;
        }

        public String getMountType() {
            return mountType;
        }

        public Map<String, String> getOptions() {
            return Collections.unmodifiableMap(options);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            RepositoryMountConfiguration that = (RepositoryMountConfiguration) o;
            return Objects.equals(getMountType(), that.getMountType()) && Objects.equals(getOptions(), that.getOptions());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getMountType(), getOptions());
        }
    }

    public static class RepositoryConfiguration {

        private Map<String, RepositoryMountConfiguration> mounts;

        @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
        public RepositoryConfiguration(@JsonProperty("mounts") Map<String, RepositoryMountConfiguration> mounts) {
            this.mounts = mounts;
        }

        public Map<String, RepositoryMountConfiguration> getMounts() {
            return mounts;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            RepositoryConfiguration that = (RepositoryConfiguration) o;
            return Objects.equals(getMounts(), that.getMounts());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getMounts());
        }
    }

    private RepositoryManager() {}

    public static final String DEFAULT_FILESYSTEM_NAME = "local";
    public static final RepositoryPath DEFAULT_FILESYSTEM_ROOT = RepositoryPath.of("//" + DEFAULT_FILESYSTEM_NAME);
    private static RepositoryFileSystem DEFAULT_FILE_SYSTEM;
    private static final RepositoryConfigurationPersistenceListener persistenceListener = new RepositoryConfigurationPersistenceListener();
    private static final ExtendableRepositoryFileSystemConfigurationListener configurationChangeListeners = new ExtendableRepositoryFileSystemConfigurationListener(
            persistenceListener);
    private static final ExtendableCredentialStorage<String, char[]> credentialStorages = new ExtendableCredentialStorage<>();
    private static boolean initialized = false;

    /**
     * This method initializes the Repository Manager, so that repository filesystems may be
     * registered.
     */
    public static void initialize(ClassLoader classLoader, Reflections reflections) throws IOException {
        if (!isInitialized()) {
            Repository.init(classLoader, reflections);
            DEFAULT_FILE_SYSTEM = (RepositoryFileSystem) FileSystems.newFileSystem(DEFAULT_FILESYSTEM_ROOT.toUri(),
                    Map.of(RepositoryFileSystemProvider.OPTION_CONFIGURATION_LISTENER, configurationChangeListeners,
                            RepositoryFileSystemProvider.OPTION_CREDENTIAL_STORAGE, credentialStorages));
            initialized = true;
        }
    }

    public static void addRepositoryConfigurationChangeListener(RepositoryFileSystemConfigurationListener listener) {
        configurationChangeListeners.addListener(listener);
    }

    public static void removeRepositoryConfigurationChangeListener(RepositoryFileSystemConfigurationListener listener) {
        configurationChangeListeners.removeListener(listener);
    }

    public static void addCredentialStorage(CredentialStorage<String, char[]> storage) {
        credentialStorages.addStorage(storage);
    }

    public static void removeCredentialStorage(CredentialStorage<String, char[]> storage) {
        credentialStorages.removeStorage(storage);
    }

    /**
     * Add a repository mount into the {@link #getFileSystem() Repository FileSystem} from the given
     * configuration.
     *
     * @param pathString
     *            the path under the repository root where the mount should be mounted
     * @param mountConfig
     *            the mount configuration object
     * @throws IOException
     *             if the mounting operation throws
     */
    public static void addMount(String pathString, RepositoryMountConfiguration mountConfig) throws IOException {
        if (!RepositoryManager.isInitialized())
            throw new IllegalStateException(RepositoryManager.class.getSimpleName() + " is not initialized.");
        RepositoryManager.getFileSystem()
                .mount(RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(pathString), mountConfig.getMountType(), mountConfig.getOptions());
    }

    /**
     * Add a mount placeholder for a future mount. This will add a special template mount which
     * contain template configuration and an optional previous error
     * 
     * @param pathString
     *            the path under the repository root where the mount should be mounted
     * @param mountConfigTemplate
     *            the mount configuration object of the template mount
     * @param throwable
     *            the error during the mount operation
     */
    public static void addMountPlaceholder(String pathString, RepositoryMountConfiguration mountConfigTemplate, IOException throwable) throws IOException {
        if (!RepositoryManager.isInitialized())
            throw new IllegalStateException(RepositoryManager.class.getSimpleName() + " is not initialized.");
        String mountType = mountConfigTemplate != null ? mountConfigTemplate.getMountType() : null;
        Map<String, String> mountOptions = new HashMap<>();
        mountOptions.put("template.type", mountType);
        Map<String, String> templateOptions = mountConfigTemplate != null ? mountConfigTemplate.getOptions() : Collections.emptyMap();
        templateOptions.forEach((key, value) -> mountOptions.put("template.option." + key, value));
        if (throwable != null) {
            mountOptions.put("error.message", throwable.getMessage());
            mountOptions.put("error.stacktrace", ExceptionUtils.getFullStackTrace(throwable));
        }
        RepositoryManager.getFileSystem().mount(RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(pathString), "placeholder", mountOptions);
    }

    /**
     * Removes a repository mount from the {@link #getFileSystem() Repository FileSystem}
     *
     * @param pathString
     *            the path under the repository root where the mount is located
     * @throws IOException
     *             if the unmounting operation throws
     */
    public static void removeMount(String pathString, boolean deleteLocalFiles) throws IOException {
        if (!RepositoryManager.isInitialized())
            throw new IllegalStateException(RepositoryManager.class.getSimpleName() + " is not initialized.");
        List<String> subDirectoryMounts = getConfiguration().getMounts()
                .keySet()
                .stream()
                .filter(mountPoints -> !Objects.equals(mountPoints, pathString))
                .filter(mountPoints -> mountPoints.startsWith(pathString))
                .toList();
        if (!subDirectoryMounts.isEmpty())
            throw new IllegalStateException("The provided mount point (" + pathString
                    + ") contains additional mounting points, which need to be unmounted first in order to unmount this repository. These points are: "
                    + subDirectoryMounts);
        RepositoryManager.getFileSystem().unmount(RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(pathString), deleteLocalFiles);
    }

    public static void writeConfiguration(OutputStream outputStream) throws IOException {
        persistenceListener.persist(outputStream);
    }

    public static RepositoryConfiguration getConfiguration() {
        return persistenceListener.getConfiguration();
    }

    public static RepositoryConfiguration readConfiguration(Path configurationFilePath) throws IOException {
        if (Files.exists(configurationFilePath)) {
            try (InputStream inputStream = Files.newInputStream(configurationFilePath)) {
                return readConfiguration(inputStream);
            }
        }
        return new RepositoryConfiguration(new HashMap<>());
    }

    public static RepositoryConfiguration readConfiguration(InputStream inputStream) throws IOException {
        return getObjectMapper().readValue(inputStream, RepositoryConfiguration.class);
    }

    private static ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    public static boolean isInitialized() {
        return initialized;
    }

    public static RepositoryFileSystem getFileSystem() {
        return DEFAULT_FILE_SYSTEM;
    }

    public static void closeRepositories() {
        if (!isInitialized())
            return;
        try {
            DEFAULT_FILE_SYSTEM.close();
        } catch (IOException e) {
            LogService.getRoot().error("com.owc.singularity.repository_close_error", e.getMessage(), e);
        }

    }

    private static class RepositoryConfigurationPersistenceListener implements RepositoryFileSystemConfigurationListener {

        private final RepositoryConfiguration configuration;

        public RepositoryConfigurationPersistenceListener() {
            configuration = new RepositoryConfiguration(new HashMap<>());
        }

        @Override
        public void updateMountPoints(Map<String[], RepositoryMount> unmodifiableMap) {
            Map<String, RepositoryMountConfiguration> mounts = new HashMap<>();
            unmodifiableMap.forEach((pathElementArray, mount) -> mounts.put(RepositoryPath.of("", pathElementArray).toString(),
                    new RepositoryMountConfiguration(mount.getType(), mount.getOptions())));
            configuration.mounts = mounts;
        }

        public void persist(OutputStream out) throws IOException {
            getObjectMapper().writerWithDefaultPrettyPrinter().writeValue(out, configuration);
        }

        public RepositoryConfiguration getConfiguration() {
            return configuration;
        }
    }

    private static class ExtendableRepositoryFileSystemConfigurationListener implements RepositoryFileSystemConfigurationListener {

        private final List<RepositoryFileSystemConfigurationListener> listeners;

        public ExtendableRepositoryFileSystemConfigurationListener(RepositoryFileSystemConfigurationListener... listeners) {
            this.listeners = Arrays.stream(listeners).filter(Objects::nonNull).collect(Collectors.toCollection(LinkedList::new));
        }

        public void addListener(RepositoryFileSystemConfigurationListener listener) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }

        public void removeListener(RepositoryFileSystemConfigurationListener listener) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }

        @Override
        public void updateMountPoints(Map<String[], RepositoryMount> unmodifiableMap) {
            synchronized (listeners) {
                for (RepositoryFileSystemConfigurationListener listener : listeners) {
                    listener.updateMountPoints(unmodifiableMap);
                }
            }
        }
    }

    private static class ExtendableCredentialStorage<P, C> implements CredentialStorage<P, C> {

        private final List<CredentialStorage<P, C>> storages = new LinkedList<>();

        private void addStorage(CredentialStorage<P, C> storage) {
            synchronized (storages) {
                storages.add(storage);
            }
        }

        private void removeStorage(CredentialStorage<P, C> storage) {
            synchronized (storages) {
                storages.remove(storage);
            }
        }

        @Override
        public boolean store(URI resourceUri, P principal, C credential) {
            synchronized (storages) {
                for (CredentialStorage<P, C> storage : storages) {
                    if (storage.store(resourceUri, principal, credential))
                        return true;
                }
                return false;
            }
        }

        @Override
        public C retrieve(URI resourceUri, P principal) {
            synchronized (storages) {
                for (CredentialStorage<P, C> storage : storages) {
                    C credential = storage.retrieve(resourceUri, principal);
                    if (credential != null)
                        return credential;
                }
            }
            return null;
        }

        @Override
        public Map.Entry<P, C> prompt(URI resourceUri, P principal) {
            synchronized (storages) {
                for (CredentialStorage<P, C> storage : storages) {
                    Map.Entry<P, C> entry = storage.prompt(resourceUri, principal);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }

        @Override
        public void close() throws IOException {
            List<IOException> errors = new ArrayList<>(storages.size());
            synchronized (storages) {
                for (CredentialStorage<P, C> storage : storages) {
                    try {
                        storage.close();
                    } catch (IOException e) {
                        errors.add(e);
                    }
                }
            }
            if (!errors.isEmpty()) {
                throw new IOException(errors.get(0));
            }
        }
    }
}
