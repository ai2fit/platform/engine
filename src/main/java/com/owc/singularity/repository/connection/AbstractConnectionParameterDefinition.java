/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.repository.connection;


import java.util.Map;

import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.studio.gui.connection.ConnectionI18N;


/**
 * Represents a general {@link ConnectionParameterDefinition} used to configure
 * {@link AbstractConnectionParameters}.
 *
 * @author Hatem Hamad
 */
public abstract class AbstractConnectionParameterDefinition<T extends AbstractConnectionParameters> implements ConnectionParameterDefinition<T> {

    /** The base key used in I18N property files. */
    public abstract String getI18NBaseKey();

    /**
     * Creates a new {@link ConnectionParameters} based on parameters. The parameters passed to this
     * method match the ones specified by {@link #getParameterTypes(ParameterHandler)}.
     *
     * @param name
     *            a user defined name identifying this {@link ConnectionParameters}.
     * @param parameters
     *            the map of key-value parameters
     * @param description
     *            optional description of the parameters
     * @param contactInformation
     *            the contact information of this connection administrator
     * @throws ConnectionParametersException
     *             if it was not able to instantiate a {@link ConnectionParameters} with the given
     *             arguments
     */
    @Override
    public T create(String name, Map<String, String> parameters, String description, String contactInformation) throws ConnectionParametersException {
        T instance;
        try {
            instance = getConnectionParametersClass().getDeclaredConstructor().newInstance();
            instance.setName(name);
            instance.setDescription(description);
            instance.setContactInformation(contactInformation);
            instance.configure(this, parameters);
        } catch (InstantiationException e) {
            throw new ConnectionParametersException("Cannot instantiate " + getConnectionParametersClass(), e);
        } catch (IllegalAccessException e) {
            throw new ConnectionParametersException("Cannot access " + getConnectionParametersClass(), e);
        } catch (Throwable e) {
            throw new ConnectionParametersException("Cannot instantiate " + getConnectionParametersClass() + " (fatal error)", e);
        }
        return instance;
    }

    /** The display name used in UI components. Based on {@link #getI18NBaseKey()}. */
    public String getName() {
        return ConnectionI18N.getConnectionGUILabel(getTypeId());
    }

    /** A short help text to be used in dialogs. Based on {@link #getI18NBaseKey()}. */
    public String getIconName() {
        return ConnectionI18N.getConnectionIconName(getTypeId());
    }

    @Override
    public String toString() {
        return getName();
    }

}
