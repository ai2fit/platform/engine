package com.owc.singularity.repository.connection;

import static org.reflections.util.ReflectionUtilsPredicates.withClassModifier;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.modules.ModuleService;

/**
 * An implementation of {@link ConnectionDefinitionRegistry} that scans all registered {@link Module
 * modules} all {@link ConnectionParameterDefinition}s. The search will be done once during
 * initialization using reflections.
 *
 * @author Hatem Hamad
 */
public final class DefaultConnectionDefinitionRegistry extends ConnectionDefinitionRegistry {

    private static final Logger log = LogManager.getLogger(DefaultConnectionDefinitionRegistry.class);
    private static DefaultConnectionDefinitionRegistry INSTANCE;

    private DefaultConnectionDefinitionRegistry() {

    }

    public static void initializeAndRegisterGlobally() {
        if (INSTANCE == null) {
            INSTANCE = createFromMajorClassLoader();
        }
        setInstance(INSTANCE);
    }

    @SuppressWarnings("rawtypes")
    private static DefaultConnectionDefinitionRegistry createFromMajorClassLoader() {
        DefaultConnectionDefinitionRegistry registry = new DefaultConnectionDefinitionRegistry();

        List<Class<? extends ConnectionParameterDefinition>> definitions = ModuleService.getMajorReflections()
                .getSubTypesOf(ConnectionParameterDefinition.class)
                .stream()
                .filter(withClassModifier(Modifier.ABSTRACT).negate())
                .toList();
        for (Class<? extends ConnectionParameterDefinition> definitionClass : definitions) {
            try {
                ConnectionParameterDefinition instance = definitionClass.getDeclaredConstructor().newInstance();
                registry.registerType(instance);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | IllegalArgumentException e) {
                log.atWarn().withThrowable(e).log("Skipping {}. Could not instantiate a connection definition from {}", definitionClass, definitionClass);
            } catch (NoSuchMethodException e) {
                log.warn(
                        "Skipping {}. The {} class does not match ConnectionParameterDefinition requirements. A ConnectionParameterDefinition class should always have a parameterless constructor!",
                        definitionClass, definitionClass);
            }
        }
        log.debug("Registered {} connection definitions", registry.getAllTypes().size());
        return registry;
    }
}
