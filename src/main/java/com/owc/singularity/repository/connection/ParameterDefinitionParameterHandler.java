package com.owc.singularity.repository.connection;

import java.util.List;
import java.util.Map;

import com.owc.singularity.engine.pipeline.parameter.AbstractParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;

public class ParameterDefinitionParameterHandler extends AbstractParameterHandler {

    private static final long serialVersionUID = -3791453385139108089L;

    private final ConnectionParameterDefinition<?> definition;

    public ParameterDefinitionParameterHandler(ConnectionParameterDefinition<?> definition) {
        super(definition.getParameterTypes(null));
        // dirty fix for this handler not being available in super-constructor call...
        this.parameters = new Parameters(definition.getParameterTypes(this));
        this.definition = definition;
    }

    public ParameterDefinitionParameterHandler fillWith(ConnectionParameters connectionParameters) {
        Map<String, String> rawParameters = connectionParameters.getRawParameters();
        for (String key : rawParameters.keySet()) {
            setParameter(key, rawParameters.get(key));
        }
        return this;
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        return definition.getParameterTypes(this);
    }
}
