/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.repository.connection;


import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.config.actions.ActionResult;


/**
 * Represent a map of key-value where the keys are defined in a
 * {@link ConnectionParameterDefinition} that share the same {@link #getTypeId() type Id}.
 *
 * @see #getParameter(String)
 *
 * @author Hatem Hamad
 *
 */
public interface ConnectionParameters extends Serializable {

    /**
     * Sets the given parameters.
     *
     * @see #getRawParameters()
     */
    void configure(ConnectionParameterDefinition<?> definition, Map<String, String> parameterValues);

    /**
     * The parameter values representing this ConnectionParameters.
     *
     * @see #configure(ConnectionParameterDefinition, Map)
     */
    Map<String, String> getRawParameters();

    String getName();

    String getDescription();

    String getContactInformation();

    /**
     * Gets the parameter value for the given key
     * 
     * @throws UndefinedParameterError
     *             if the parameter key was required but no there was no value provided
     */
    String getParameter(String key) throws UndefinedParameterError;

    /** Returns the unique type id of the corresponding {@link ConnectionParameterDefinition}. */
    String getTypeId();

    /**
     * Returns a {@link Callable} which tests the settings for the {@link ConnectionParameters}
     */
    Callable<ActionResult> getTestAction();
}
