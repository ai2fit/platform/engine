/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.repository.connection;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.config.actions.ActionResult;
import com.owc.singularity.engine.tools.config.actions.ActionResultFactory;


/**
 * Represents a general implementation of the {@link ConnectionParameters} class. Implementations of
 * this class will be searched on the classpath and registered in the
 * {@link ConnectionDefinitionRegistry} automatically.
 *
 * @author Hatem Hamad
 */
public abstract class AbstractConnectionParameters implements ConnectionParameters {

    private static final long serialVersionUID = -4070383223281747498L;

    private String name = "undefined";
    private final Map<String, String> rawParametersMap;
    private transient Parameters parameters;
    private String description;
    private String contactInformation;

    public AbstractConnectionParameters() {
        this.rawParametersMap = new HashMap<>();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the parameter value for the given key. The value has gone through the
     * {@link ParameterType#fromRawString(String)} method.
     *
     */
    @Override
    public String getParameter(String key) throws UndefinedParameterError {
        if (parameters == null) {
            if (rawParametersMap.isEmpty())
                throw new IllegalStateException("The instance is not configured yet.");
            // this happens when a connection object get loaded directly from repository
            // try to configure this instance with the current definition
            ConnectionParameterDefinition<?> definition = ConnectionDefinitionRegistry.getInstance().getDefinitionOfType(getTypeId());
            if (definition == null) {
                // no definition is available, direct access the raw map
                return rawParametersMap.get(key);
            }
            createParametersWrapper(definition);
        }
        return parameters.getParameter(key);
    }

    @Override
    public void configure(ConnectionParameterDefinition<?> definition, Map<String, String> parameters) {
        clear();
        if (parameters != null && !parameters.isEmpty()) {
            this.rawParametersMap.putAll(parameters);
            createParametersWrapper(definition);
        }
    }

    private void createParametersWrapper(ConnectionParameterDefinition<?> definition) {
        this.parameters = new ParameterDefinitionParameterHandler(definition).fillWith(this).getParameters();
    }

    private void clear() {
        this.rawParametersMap.clear();
        if (this.parameters != null)
            this.parameters = null;
    }

    @Override
    public Map<String, String> getRawParameters() {
        return Collections.unmodifiableMap(rawParametersMap);
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

    @Override
    public String getContactInformation() {
        return contactInformation;
    }

    @Override
    public Callable<ActionResult> getTestAction() {
        return ActionResultFactory::undetermined;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AbstractConnectionParameters that = (AbstractConnectionParameters) o;
        return getName().equals(that.getName()) && getRawParameters().equals(that.getRawParameters())
                && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getRawParameters(), getDescription());
    }
}
