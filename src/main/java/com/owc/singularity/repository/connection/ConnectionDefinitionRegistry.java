package com.owc.singularity.repository.connection;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Represent the global registry where all the accessible {@link ConnectionParameterDefinition} are
 * registered.
 *
 * @author Hatem Hamad
 */
public class ConnectionDefinitionRegistry {

    private static final Logger log = LogManager.getLogger(ConnectionDefinitionRegistry.class);

    private static ConnectionDefinitionRegistry GLOBAL_INSTANCE;

    ConnectionDefinitionRegistry() {

    }

    public static ConnectionDefinitionRegistry getInstance() {
        if (GLOBAL_INSTANCE == null) {
            throw new IllegalStateException("No configured instance was registered yet!");
        }
        return GLOBAL_INSTANCE;
    }

    static void setInstance(ConnectionDefinitionRegistry registry) {
        GLOBAL_INSTANCE = registry;
    }

    private final Map<String, ConnectionParameterDefinition<?>> typeToDefinitionMap = new ConcurrentHashMap<>();

    void registerType(ConnectionParameterDefinition<?> definition) {
        Objects.requireNonNull(definition, "definition must not be null");
        String typeId = definition.getTypeId();
        if (typeId == null || typeId.isEmpty()) {
            throw new IllegalArgumentException("definition typeId must no be null or empty");
        }
        if (typeToDefinitionMap.containsKey(typeId)) {
            log.warn("Overriding definition of type {} with a new definition {}", typeId, definition);
        }
        typeToDefinitionMap.put(typeId, definition);
    }

    public ConnectionParameterDefinition<?> getDefinitionOfType(String typeId) {
        return typeToDefinitionMap.get(Objects.requireNonNull(typeId, "typeId must not be null"));
    }

    @SuppressWarnings("unchecked")
    public <T extends ConnectionParameters> ConnectionParameterDefinition<T> getDefinitionOfParameters(T parameters) {
        ConnectionParameterDefinition<?> definition = typeToDefinitionMap.get(Objects.requireNonNull(parameters, "parameters must not be null").getTypeId());
        if (definition == null)
            return (ConnectionParameterDefinition<T>) getDefinitionOfParametersClass(parameters.getClass());
        if (!parameters.getClass().isAssignableFrom(definition.getConnectionParametersClass())) {
            log.warn("Registered type {} was queried with incompatible ConnectionParametersClass: registered {}, queried with {}. Retuning null",
                    parameters.getTypeId(), definition.getConnectionParametersClass(), parameters.getClass());
            return null;
        }
        return (ConnectionParameterDefinition<T>) definition;
    }

    @SuppressWarnings("unchecked")
    public <T extends ConnectionParameters> ConnectionParameterDefinition<T> getDefinitionOfParametersClass(Class<T> parametersClass) {
        Objects.requireNonNull(parametersClass, "parameterClass must not be null");
        Optional<ConnectionParameterDefinition<?>> parameterDefinition = typeToDefinitionMap.values()
                .stream()
                .filter(definition -> definition.getConnectionParametersClass().isAssignableFrom(parametersClass))
                .findFirst();
        if (parameterDefinition.isEmpty())
            return null;
        return (ConnectionParameterDefinition<T>) parameterDefinition.get();
    }

    public Set<String> getAllTypes() {
        return Collections.unmodifiableSet(typeToDefinitionMap.keySet());
    }
}
