/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.repository.connection;


import java.util.List;
import java.util.Map;

import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;


/**
 * Represent the definition of a {@link ConnectionParameters}. This is defined by the list of
 * {@link ParameterType} provided by {@link #getParameterTypes()}
 * 
 * @author Hatem Hamad
 */
public interface ConnectionParameterDefinition<T extends ConnectionParameters> {

    /**
     * Returns the {@link ConnectionParameters} implementation that this configurator can configure.
     */
    Class<T> getConnectionParametersClass();

    /**
     * Default implementation for all classes that extend {@link ConnectionParameterDefinition}
     * instead of the new {@link AbstractConnectionParameterDefinition} class. It just returns the
     * parameter types without correct dependency handling.
     */
    default List<ParameterType> getParameterTypes() {
        return getParameterTypes(new ParameterDefinitionParameterHandler(this));
    }

    List<ParameterType> getParameterTypes(ParameterHandler parameterHandler);

    /**
     * The ID used for identifying related {@link ConnectionParameters}. Should include the module
     * namespace. Example: "database:sql_connection".
     */
    String getTypeId();

    T create(String name, Map<String, String> parameters, String description, String contactInformation) throws ConnectionParametersException;
}
