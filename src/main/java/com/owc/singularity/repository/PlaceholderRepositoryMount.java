package com.owc.singularity.repository;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileTime;
import java.util.*;

import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.entry.VersionHistoryDifference;
import com.owc.singularity.repository.exception.MountHasNoRemoteException;
import com.owc.singularity.repository.vcs.MergeConflictResolver;
import com.owc.singularity.repository.vcs.MountTreeChange;
import com.owc.singularity.repository.vcs.MountTreeDifference;

/**
 * A {@link RepositoryMount} that represents an un-configured or failed to configure mount. The
 * options used to configure this mount are saved but trying to access any other functionality will
 * throw an {@link IOException}.
 *
 * @author Hatem Hamad
 */
public class PlaceholderRepositoryMount extends RepositoryMount {

    public static final String TYPE_NAME = "placeholder";
    public static final String PRESERVED_OPTIONS_PREFIX = "template.option.";
    private final String templateType;
    private final String errorMessage;
    private final String errorStacktrace;
    private final Map<String, String> templateOptions;

    public PlaceholderRepositoryMount(RepositoryPath mountPath, Map<String, String> options) {
        super(mountPath, options);
        templateType = getTemplateType(options);
        templateOptions = getTemplateOptions(options);
        errorMessage = getErrorMessage(options);
        errorStacktrace = getErrorStacktrace(options);
    }

    public String getTemplateType() {
        return templateType;
    }

    public Map<String, String> getTemplateOptions() {
        return templateOptions;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorStacktrace() {
        return errorStacktrace;
    }

    @Override
    protected void checkAccess(RepositoryPath path, AccessMode... modes) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected void copy(RepositoryPath source, RepositoryPath target, CopyOption... options) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected void createDirectory(RepositoryPath dir, FileAttribute<?>... attrs) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected void delete(RepositoryPath path) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected MountTreeDifference commit(RepositoryPath path, String comment) throws IOException {
        return null;
    }

    @Override
    protected boolean hasUncommittedChanges(RepositoryPath path) {
        return false;
    }

    @Override
    protected <A extends BasicFileAttributes> A readAttributes(RepositoryPath path, Class<A> type, LinkOption... options) throws IOException {
        return (A) new PlaceholderBasicFileAttributes(isDirectory(path));
    }

    @Override
    protected BasicFileAttributeView getBasicFileAttributeView(RepositoryPath path, LinkOption... options) {
        return null;
    }

    @Override
    protected FileStore getFileStore(RepositoryPath path) {
        return null;
    }

    @Override
    protected boolean isDirectory(RepositoryPath path) throws IOException {
        return path.getFileName().toString().isEmpty();
    }

    @Override
    protected Iterator<RepositoryPath> iterateDirectory(RepositoryPath dir, DirectoryStream.Filter<? super Path> filter) {
        return Collections.emptyIterator();
    }

    @Override
    protected Iterator<EntryVersion> iterateVersions(RepositoryPath path) {
        return Collections.emptyIterator();
    }

    @Override
    protected EntryVersion getVersion(RepositoryPath relativePath) throws IOException {
        return null;
    }

    @Override
    protected void move(RepositoryPath source, RepositoryPath target, CopyOption... options) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected SeekableByteChannel newByteChannel(RepositoryPath path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected MountTreeChange getUncommittedChanges(RepositoryPath path) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected Map<String, Object> readAttributes(RepositoryPath path, String attributes, LinkOption... options) {
        return null;
    }

    @Override
    protected void setAttribute(RepositoryPath path, String attribute, Object value, LinkOption... options) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    protected Iterable<FileStore> getFileStores() {
        return null;
    }

    @Override
    public MountTreeDifference compareVersionTrees(EntryVersion left, EntryVersion right) throws IOException {
        throw new IOException("The mount is only a placeholder and does not implement this operation");
    }

    @Override
    public VersionHistoryDifference fetch() throws IOException {
        throw new MountHasNoRemoteException();
    }

    @Override
    public MountTreeDifference update(MergeConflictResolver conflictMergeConflictResolver) throws IOException {
        throw new MountHasNoRemoteException();
    }

    @Override
    public void deleteLocalFiles() throws IOException {
        // nothing to delete as it is only a placeholder
    }

    @Override
    protected void closeResources() {

    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public boolean isReadOnly() {
        return true;
    }

    @Override
    public boolean hasRemote() {
        return false;
    }

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    public static String getErrorStacktrace(Map<String, String> options) {
        return options.get("error.stacktrace");
    }

    public static String getErrorMessage(Map<String, String> options) {
        return options.get("error.message");
    }

    public static Map<String, String> getTemplateOptions(Map<String, String> options) {
        final Map<String, String> templateOptions = new LinkedHashMap<>();
        options.keySet()
                .stream()
                .filter(key -> key.startsWith(PRESERVED_OPTIONS_PREFIX))
                .forEach(
                        key -> templateOptions.put(key.substring(key.indexOf(PRESERVED_OPTIONS_PREFIX) + PRESERVED_OPTIONS_PREFIX.length()), options.get(key)));
        return templateOptions;
    }

    public static String getTemplateType(Map<String, String> options) {
        return options.get("template.type");
    }

    private static class PlaceholderBasicFileAttributes implements BasicFileAttributes {

        private final boolean directory;

        public PlaceholderBasicFileAttributes(boolean directory) {
            this.directory = directory;
        }

        @Override
        public FileTime lastModifiedTime() {
            return null;
        }

        @Override
        public FileTime lastAccessTime() {
            return null;
        }

        @Override
        public FileTime creationTime() {
            return null;
        }

        @Override
        public boolean isRegularFile() {
            return !isDirectory();
        }

        @Override
        public boolean isDirectory() {
            return directory;
        }

        @Override
        public boolean isSymbolicLink() {
            return false;
        }

        @Override
        public boolean isOther() {
            return false;
        }

        @Override
        public long size() {
            return 0;
        }

        @Override
        public Object fileKey() {
            return null;
        }
    }
}
