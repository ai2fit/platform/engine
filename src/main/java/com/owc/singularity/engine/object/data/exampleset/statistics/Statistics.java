/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.statistics;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * The superclass for all attribute statistics objects. To calculate statistics, such as variance,
 * sum or maximums, call {@link #count(double, double)}, {@link #count(long, double)} or
 * {@link #count(String, double)}.
 * 
 * @author Ingo Mierswa
 */
public final class Statistics implements Serializable {

    private static final long serialVersionUID = 4665627127536463574L;

    private class WeightSum implements Serializable {

        private static final long serialVersionUID = 159409461480835018L;
        public double weight = 0d;
        public int count = 0;

        WeightSum(double initialValue) {
            weight = initialValue;
            count++;
        }

        void add(double weight) {
            this.weight += weight;
            count++;
        }
    }

    private int unknownCounter = 0;
    private double unknownWeight = 0;
    private int valuesCount = 0;
    private double valuesWeight = 0.0d;

    private long timestampFirst = Long.MAX_VALUE;
    private long timestampLast = Long.MIN_VALUE;

    private double numericMinimum = Double.POSITIVE_INFINITY;
    private double numericMaximum = Double.NEGATIVE_INFINITY;

    private double numericSum = 0.0d;
    private long timestampSum = 0;
    private long timestampSumOfSquares = 0;
    private double numericSumOfSquares = 0.0d;

    private double numericWeightedSum = 0.0d;

    private String nominalMode = null;
    private String nominalLeast = null;
    private double numericMode;
    private double numericLeast;

    private long timestampMode;
    private long timestampLeast;
    private int nominalLeastOccurrences;
    private long nominalModeOccurrences = 0;
    private HashMap<String, WeightSum> nominalOccurrences;
    private HashMap<Double, WeightSum> numericOccurrences;

    private HashMap<Long, WeightSum> timestampOccurrences;

    private boolean isPrecalculated;

    public Statistics(Attribute attribute) {
        switch (attribute.getValueType()) {
            case NOMINAL:
                this.nominalOccurrences = new HashMap<>();
                break;
            case NUMERIC:
                this.numericOccurrences = new HashMap<>();
                break;
            case TIMESTAMP:
                this.timestampOccurrences = new HashMap<>();
                break;
        }
    }

    public void count(long timestampValue, double weight) {
        if (!ValueType.isMissing(timestampValue)) {
            if (timestampLast < timestampValue)
                timestampLast = timestampValue;
            if (timestampFirst > timestampValue)
                timestampFirst = timestampValue;
            timestampSum += timestampValue;
            timestampSumOfSquares += timestampValue * timestampValue;
            valuesCount++;

            WeightSum weightSum = timestampOccurrences.putIfAbsent(timestampValue, new WeightSum(weight));
            if (weightSum != null)
                weightSum.add(weight);

            numericWeightedSum += (weight * timestampValue);
            valuesWeight += weight;
        } else {
            unknownCounter++;
            unknownWeight += weight;
        }
    }

    public void count(String nominalValue, double weight) {
        if (!ValueType.isMissing(nominalValue)) {
            WeightSum weightSum = nominalOccurrences.putIfAbsent(nominalValue, new WeightSum(weight));
            if (weightSum != null)
                weightSum.add(weight);
            valuesCount++;
            valuesWeight += weight;
        } else {
            unknownCounter++;
            unknownWeight += weight;
        }
    }

    public void count(double value, double weight) {
        if (!ValueType.isMissing(value)) {
            if (numericMinimum > value) {
                numericMinimum = value;
            }
            if (numericMaximum < value) {
                numericMaximum = value;
            }
            numericSum += value;
            numericSumOfSquares += value * value;
            valuesCount++;

            WeightSum weightSum = numericOccurrences.putIfAbsent(value, new WeightSum(weight));
            if (weightSum != null)
                weightSum.add(weight);

            numericWeightedSum += (weight * value);
            valuesWeight += weight;
        } else {
            unknownCounter++;
            unknownWeight += weight;
        }
    }

    public int getNumberOfUnknownValues() {
        return unknownCounter;
    }


    public double getWeightOfUnknownValues() {
        return unknownWeight;
    }


    public long getFirstTimestamp() {
        return timestampFirst;
    }


    public long getLastTimestamp() {
        return timestampLast;
    }


    public double getMinimumValue() {
        return numericMinimum;
    }


    public double getMaximumValue() {
        return numericMaximum;
    }

    /**
     * This returns the standard deviation for numeric attributes
     * 
     * @return the standard deviation
     */
    public double getStandardDeviation() {
        return Math.sqrt(getVariance());
    }

    /**
     * This returns the standard deviation for numeric attributes
     * 
     * @return the standard deviation
     */
    public double getVariance() {
        if (valuesCount <= 1) {
            return 0;
        }
        double variance = (numericSumOfSquares - numericSum * numericSum / valuesCount) / (valuesCount - 1);
        if (variance < 0) {
            return 0;
        }
        return variance;
    }

    public long getTimestampVariance() {
        if (valuesCount <= 1) {
            return 0;
        }
        long variance = (timestampSumOfSquares - (timestampSum * timestampSum) / valuesCount) / (valuesCount - 1);
        if (variance < 0) {
            return 0;
        }
        return variance;
    }

    public double getWeightedTimestampVariance() {
        if (valuesCount <= 1) {
            return 0;
        }
        double weightedVariance = (timestampSumOfSquares - (timestampSum * timestampSum) / valuesWeight)
                / (((double) (valuesCount - 1) / valuesCount) * valuesWeight);
        if (weightedVariance < 0) {
            return 0;
        }
        return weightedVariance;
    }

    /**
     * This returns the standard deviation for numeric attributes
     * 
     * @return the standard deviation
     */
    public double getWeightedVariance() {
        if (valuesCount <= 1) {
            return 0;
        }
        double variance = (numericSumOfSquares - (numericSum * numericSum) / valuesWeight) / (((double) (valuesCount - 1) / valuesCount) * valuesWeight);
        if (variance < 0) {
            return 0;
        }
        return variance;
    }

    /**
     * This returns the sum of all values of a numeric attribute
     * 
     * @return the sum
     */
    public double getNumericSum() {
        return numericSum;
    }

    public long getTimestampSum() {
        return timestampSum;
    }

    public double getWeightedSum() {
        return numericWeightedSum;
    }

    public double getWeightedAverage() {
        return numericSum / valuesWeight;
    }

    public double getAverage() {
        return numericSum / valuesCount;
    }

    public long getTimestampAverage() {
        return timestampSum / valuesCount;
    }

    public long getWeightedTimestampAverage() {
        return (long) (timestampSum / valuesWeight);
    }

    public int getNumberOfValues() {
        return valuesCount;
    }

    public double getTotalWeight() {
        return valuesWeight;
    }

    /**
     * This returns the most frequent nominal value, the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return nominalMode;
    }

    public double getNumericMode() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return numericMode;
    }

    public long getTimestampMode() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return timestampMode;
    }


    public long getModeOccurences() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return nominalModeOccurrences;
    }

    /**
     * This returns the most frequent nominal value, the mode.
     * 
     * @return the mode
     */
    public String getLeast() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return nominalLeast;
    }

    public double getNumericLeast() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return numericLeast;
    }

    public long getTimestampLeast() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return timestampLeast;
    }


    public long getLeastOccurences() {
        if (!isPrecalculated) {
            finishCounting();
        }
        return nominalLeastOccurrences;
    }


    private synchronized void finishCounting() {
        if (!isPrecalculated) {

            nominalLeastOccurrences = Integer.MAX_VALUE;
            nominalModeOccurrences = 0;
            if (nominalOccurrences != null) {
                for (Map.Entry<String, WeightSum> entry : nominalOccurrences.entrySet()) {
                    int occurrences = entry.getValue().count;
                    if (nominalLeastOccurrences > occurrences) {
                        nominalLeastOccurrences = occurrences;
                        nominalLeast = entry.getKey();
                    }
                    if (nominalModeOccurrences < occurrences) {
                        nominalModeOccurrences = occurrences;
                        nominalMode = entry.getKey();
                    }
                }
            }

            int numericLeastOccurrences = Integer.MAX_VALUE;
            long numericModeOccurrences = 0;
            if (numericOccurrences != null) {
                for (Map.Entry<Double, WeightSum> entry : numericOccurrences.entrySet()) {
                    int occurrences = entry.getValue().count;
                    if (numericLeastOccurrences > occurrences) {
                        numericLeastOccurrences = occurrences;
                        numericLeast = entry.getKey();
                    }
                    if (numericModeOccurrences < occurrences) {
                        numericModeOccurrences = occurrences;
                        numericMode = entry.getKey();
                    }
                }
            }

            int timestampLeastOccurrences = Integer.MAX_VALUE;
            long timestampModeOccurrences = 0;
            if (timestampOccurrences != null) {
                for (Map.Entry<Long, WeightSum> entry : timestampOccurrences.entrySet()) {
                    int occurrences = entry.getValue().count;
                    if (timestampLeastOccurrences > occurrences) {
                        timestampLeastOccurrences = occurrences;
                        timestampLeast = entry.getKey();
                    }
                    if (timestampModeOccurrences < occurrences) {
                        timestampModeOccurrences = occurrences;
                        timestampMode = entry.getKey();
                    }
                }
            }

            isPrecalculated = true;
        }
    }


    /**
     * This returns the number of occurrences of a string value or 0 if not present.
     * 
     * @param value
     *            the nominal value
     * @return number of occurrences
     */
    public int getOccurences(String value) {
        WeightSum weightSum = nominalOccurrences.get(value);
        if (weightSum == null)
            return 0;
        return weightSum.count;
    }

    /**
     * This returns the total weight of a string value or 0 if not present.
     * 
     * @param value
     *            the nominal value
     * @return number of occurrences
     */
    public double getWeight(String value) {
        WeightSum weightSum = nominalOccurrences.get(value);
        if (weightSum == null)
            return 0d;
        return weightSum.weight;
    }

    public Stream<String> streamNominalValues() {
        return nominalOccurrences.keySet().stream();
    }


}
