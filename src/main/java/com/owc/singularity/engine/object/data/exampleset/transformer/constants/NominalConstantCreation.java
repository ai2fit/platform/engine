package com.owc.singularity.engine.object.data.exampleset.transformer.constants;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

public class NominalConstantCreation extends ConstantCreation {

    public String value;

    public NominalConstantCreation(String name, String value) {
        super(name);
        this.value = value;
    }

    @Override
    public ColumnDescription apply() {
        DataColumn column = new DataColumn(1, ValueType.NOMINAL);
        column.setNominalValue(0, value);
        column.finishWriting(1);
        return new ColumnDescription(getName(), ValueType.NOMINAL, column);
    }
}
