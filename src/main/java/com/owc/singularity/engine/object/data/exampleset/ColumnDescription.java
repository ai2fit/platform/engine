package com.owc.singularity.engine.object.data.exampleset;

public class ColumnDescription {

    public ColumnDescription(String name, ValueType valueType, DataColumn dataColumn) {
        super();
        this.name = name;
        this.valueType = valueType;
        this.column = dataColumn;
    }

    String name;
    ValueType valueType;
    DataColumn column;
}
