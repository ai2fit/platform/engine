package com.owc.singularity.engine.object.io;

import java.io.*;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.tools.Tools;


public class DelegatingFileObject extends FileObject {

    private static final long serialVersionUID = 1L;

    @FunctionalInterface
    public static interface IOSupplier<T> {

        public T getChecked() throws IOException;
    }

    private transient File file = null;


    private transient IOSupplier<InputStream> delegationSupplier;


    private String sourceName;

    public DelegatingFileObject(IOSupplier<InputStream> delegationSupplier, String sourceName) {
        super();
        this.delegationSupplier = delegationSupplier;
        this.sourceName = sourceName;
    }

    @Override
    public InputStream openStream() throws OperatorException {
        try {
            return delegationSupplier.getChecked();
        } catch (IOException e) {
            throw new OperatorException("321", e, sourceName, e.getMessage());
        }
    }

    @Override
    public File getFile() throws OperatorException {
        if (file == null) {

            try {
                file = File.createTempFile("rm_file_", ".dump");
                try (FileOutputStream fos = new FileOutputStream(file); InputStream in = delegationSupplier.getChecked();) {
                    Tools.copyStreamSynchronously(in, fos, true);
                    file.deleteOnExit();
                }
            } catch (IOException e) {
                throw new OperatorException("321", e, sourceName, e.getMessage());
            }

        }
        return file;
    }

    /**
     * Firstly, this method calls {@link #getFile()}. As the file usually exists in the repository,
     * it is simply accessed. If it does not exist, a temporary file is created. Secondly, the
     * length of the file is returned.
     */
    @Override
    public long getLength() throws OperatorException {
        // There is no easier way to receive the length, as the underlying BlobEntry only supports
        // an InputStream
        return getFile().length();
    }


    @Serial
    private void writeObject(ObjectOutputStream oos) throws IOException {
        try (InputStream inputStream = openStream()) {
            byte[] data = Tools.readInputStream(inputStream);
            oos.writeObject(data);
        } catch (OperatorException e) {
            throw new RuntimeException(e);
        }
    }

    @Serial
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        byte[] data = (byte[]) stream.readObject();
        delegationSupplier = () -> new ByteArrayInputStream(data);
    }
}
