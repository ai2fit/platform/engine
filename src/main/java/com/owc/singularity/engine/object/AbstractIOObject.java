/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.repository.RepositoryPath;


/**
 * This is an abstract superclass for all IOObject. It provides basic implementations for all
 * methods of the IOObject interface. In addition, it also provides static methods which can be used
 * for reading IOObjects from XML strings and input streams / files containing the XML
 * serialization.
 * 
 * @author Ingo Mierswa
 */
public abstract class AbstractIOObject implements IOObject {

    private static final long serialVersionUID = 7131412868947165460L;


    /** The source of this IOObect. Might be null. */
    private String sourceOperator = null;
    private String sourcePort = null;


    private transient RepositoryPath path;

    @Override
    public void setSource(Operator operator, OutputPort port) {
        if (sourceOperator == null) {
            sourceOperator = operator.getName() + "[" + operator.getApplyCount() + "]";
            sourcePort = port.getName();
        }
        if (path == null && operator.getPipeline() != null) {
            path = operator.getPipeline().getPath();
        }
    }

    @Override
    public void setSourcePath(RepositoryPath path) {
        this.path = path;
    }

    @Override
    public RepositoryPath getSourcePath() {
        return path;
    }

    @Override
    public String getSourceOperator() {
        return sourceOperator;
    }

    @Override
    public String getSourcePort() {
        return sourcePort;
    }

    /**
     * The default implementation simply returns the result of the method {@link #toString()}.
     */
    @Override
    public String toResultString() {
        return toString();
    }
}
