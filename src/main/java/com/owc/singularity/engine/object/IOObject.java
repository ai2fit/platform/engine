/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object;


import java.io.Serializable;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.repository.RepositoryPath;


/**
 * This interface must be implemented by all objects that can be input/output objects for Operators.
 * The copy method is necessary in cases where meta operator chains want to copy the input
 * IOContainer before it is given to the children operators. Please note that the method only need
 * to be implemented like a usual <code>clone</code> method for IO objects which can be altered
 * after creation. In all other cases the implementation can simply return the same object. Hence,
 * we use the name <code>copy</code> instead of <code>clone</code>.
 * 
 * @author Ingo Mierswa
 */
public interface IOObject extends Serializable {


    /**
     * Returns a human-readable string describing the actual content of this object. This will be
     * severely longer than the {@link #toString()} method that will output summary information more
     * meant for the developer than the user.
     */
    public String toResultString();

    /** Returns the source of this IOObject (might return null if the source is unknown). */
    public String getSourceOperator();

    public String getSourcePort();

    public RepositoryPath getSourcePath();


    /**
     * This method is called to set the operator that created this object. It must not change again
     * as all IOObjects are immutable.
     */
    public void setSource(Operator operator, OutputPort port);

    /**
     * This will set the path this object is read from.
     * 
     * @param path
     */
    public void setSourcePath(RepositoryPath path);


    public default String getName() {
        return "IOObject";
    }

    public default String getSource() {
        if (getSourcePath() != null) {
            return getSourcePath().toString();
        } else if (getSourceOperator() != null) {
            return getSourcePort() + "@" + getSourceOperator();
        }
        return "";
    };


}
