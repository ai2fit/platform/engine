/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serial;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.UserError;


/**
 * This subclass of {@link ExampleSetRowFilter} serves to exclude examples with unknown labels from
 * an {@link ExampleSet}.
 * 
 * @author Ingo Mierswa ingomierswa Exp $
 */
public class NoMissingLabelsFilter implements ExampleSetRowFilter {

    @Serial
    private static final long serialVersionUID = 8047504208389222350L;
    private Attribute labelAttribute;

    /** Returns true if the label was defined. */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) {
        return switch (labelAttribute.getValueType()) {
            case NOMINAL -> !ValueType.isMissing(exampleSet.getNominalValue(row, labelAttribute.getIndex()));
            case NUMERIC -> !ValueType.isMissing(exampleSet.getNumericValue(row, labelAttribute.getIndex()));
            case TIMESTAMP -> !ValueType.isMissing(exampleSet.getTimestampValue(row, labelAttribute.getIndex()));
        };
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        labelAttribute = exampleSet.getAttributes().getLabel();
        if (labelAttribute == null)
            throw new UserError(null, 105);
    }
}
