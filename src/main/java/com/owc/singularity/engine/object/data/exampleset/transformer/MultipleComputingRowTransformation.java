package com.owc.singularity.engine.object.data.exampleset.transformer;

import java.util.Arrays;
import java.util.function.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

public final class MultipleComputingRowTransformation<C> implements ExampleSetTransformation {

    /**
     * This accessor allows to access values of a single row via it's respective
     * {@link #getNominal(int)}, {@link #getNumeric(int)}, {@link #getTimestamp(int)} methods.
     * 
     * @author Sebastian Land
     *
     */
    public static final class RedirectableRowAccessor {

        int row;
        private int[] directAttributeIndices;
        private int[] attributeIndices;
        private ExampleSet exampleSet;

        private RedirectableRowAccessor(ExampleSet exampleSet, int[] attributeIndices) {
            this.exampleSet = exampleSet;
            this.attributeIndices = attributeIndices;
            this.directAttributeIndices = attributeIndices;
        }

        public double getNumeric(int attributeNumber) {
            return exampleSet.getNumericValue(row, attributeIndices[attributeNumber]);
        }

        public String getNominal(int attributeNumber) {
            return exampleSet.getNominalValue(row, attributeIndices[attributeNumber]);
        }

        public long getTimestamp(int attributeNumber) {
            return exampleSet.getTimestampValue(row, attributeIndices[attributeNumber]);
        }

        private void setRow(int row) {
            this.row = row;
        }

        public void setIndirect(int[] attributeIndices) {
            this.attributeIndices = attributeIndices;
        }

        public void resetToDirect() {
            this.attributeIndices = directAttributeIndices;
        }
    }

    private ToDoubleFunction<C>[] numericDerives;
    private Function<C, String>[] nominalDerives;
    private ToLongFunction<C>[] timestampDerives;
    private String[] sourceAttributeNames;
    private BiConsumer<RedirectableRowAccessor, C> computationFunction;
    private Supplier<C> computationResultProvider;
    private String[] numericTargetNames;
    private String[] nominalTargetNames;
    private String[] timestampTargetNames;


    public MultipleComputingRowTransformation(String[] numericTargetNames, String[] nominalTargetNames, String[] timestampTargetNames,
            String[] sourceAttributeNames, Supplier<C> resultSupplier, BiConsumer<RedirectableRowAccessor, C> computationFunction,
            ToDoubleFunction<C>[] numericDerives, Function<C, String>[] nominalDerives, ToLongFunction<C>[] timestampDerives) {
        this.numericTargetNames = numericTargetNames;
        this.nominalTargetNames = nominalTargetNames;
        this.timestampTargetNames = timestampTargetNames;
        this.sourceAttributeNames = sourceAttributeNames;
        this.computationResultProvider = resultSupplier;
        this.computationFunction = computationFunction;
        this.numericDerives = numericDerives;
        this.nominalDerives = nominalDerives;
        this.timestampDerives = timestampDerives;

    }


    @Override
    public Stream<ColumnDescription> apply(ExampleSet sourceSet) {
        // preparing columns
        DataColumn[] numericDataColumns = new DataColumn[numericDerives.length];
        DataColumn[] nominalDataColumns = new DataColumn[nominalDerives.length];
        DataColumn[] timestampDataColumns = new DataColumn[timestampDerives.length];

        int numberOfRows = sourceSet.size();
        for (int i = 0; i < numericDataColumns.length; i++)
            numericDataColumns[i] = new DataColumn(numberOfRows, ValueType.NUMERIC);
        for (int i = 0; i < nominalDataColumns.length; i++)
            nominalDataColumns[i] = new DataColumn(numberOfRows, ValueType.NOMINAL);
        for (int i = 0; i < timestampDataColumns.length; i++)
            timestampDataColumns[i] = new DataColumn(numberOfRows, ValueType.TIMESTAMP);

        // preparing accessor and results
        int[] sourceAttributeIndices = Arrays.stream(sourceAttributeNames).mapToInt(name -> sourceSet.getAttributes().get(name).getIndex()).toArray();
        ThreadLocal<RedirectableRowAccessor> localAccessor = new ThreadLocal<>() {

            @Override
            protected RedirectableRowAccessor initialValue() {
                return new RedirectableRowAccessor(sourceSet, sourceAttributeIndices);
            }
        };
        ThreadLocal<C> localResult = new ThreadLocal<C>() {

            @Override
            protected C initialValue() {
                return computationResultProvider.get();
            };
        };

        // processing rows in parallel
        IntStream.range(0, numberOfRows).parallel().forEach(row -> {
            RedirectableRowAccessor accessor = localAccessor.get();
            accessor.setRow(row);
            C computationResult = localResult.get();
            // compute result
            computationFunction.accept(accessor, computationResult);
            for (int i = 0; i < numericDataColumns.length; i++)
                numericDataColumns[i].setNumericValue(row, numericDerives[i].applyAsDouble(computationResult));
            for (int i = 0; i < nominalDataColumns.length; i++)
                nominalDataColumns[i].setNominalValue(row, nominalDerives[i].apply(computationResult));
            for (int i = 0; i < timestampDataColumns.length; i++)
                timestampDataColumns[i].setTimestampValue(row, timestampDerives[i].applyAsLong(computationResult));
        });

        // finish writing in data columns
        for (int i = 0; i < numericDataColumns.length; i++)
            numericDataColumns[i].finishWriting(numberOfRows);
        for (int i = 0; i < nominalDataColumns.length; i++)
            nominalDataColumns[i].finishWriting(numberOfRows);
        for (int i = 0; i < timestampDataColumns.length; i++)
            timestampDataColumns[i].finishWriting(numberOfRows);

        // return results
        return Stream.concat(
                Stream.concat(
                        IntStream.range(0, numericDataColumns.length)
                                .mapToObj(i -> new ColumnDescription(numericTargetNames[i], ValueType.NUMERIC, numericDataColumns[i])),
                        IntStream.range(0, nominalDataColumns.length)
                                .mapToObj(i -> new ColumnDescription(nominalTargetNames[i], ValueType.NOMINAL, nominalDataColumns[i]))),
                IntStream.range(0, timestampDataColumns.length)
                        .mapToObj(i -> new ColumnDescription(timestampTargetNames[i], ValueType.TIMESTAMP, timestampDataColumns[i])));
    }
}
