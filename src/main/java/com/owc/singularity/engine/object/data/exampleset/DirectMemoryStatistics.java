package com.owc.singularity.engine.object.data.exampleset;

import java.util.concurrent.atomic.AtomicLong;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;

public class DirectMemoryStatistics {

    static final AtomicLong STATISTIC_CHUNKS_USED = new AtomicLong();
    static final AtomicLong STATISTIC_MEMORY_USED = new AtomicLong();
    static final AtomicLong STATISTIC_TOTAL_MEMORY_ALLOCATED = new AtomicLong();
    static final AtomicLong STATISTIC_TOTAL_MEMORY_FREEED = new AtomicLong();
    static Long TARGET_MEMORY = null;
    static String TARGET_MEMORY_STRING = null;

    public static long getNumberOfActiveChunks() {
        return STATISTIC_CHUNKS_USED.get();
    }

    public static long getSizeOfUsedMemory() {
        return STATISTIC_MEMORY_USED.get();
    }

    public static long getTargetSizeOfDirectMemory() {
        String targetOffHeapString = PropertyService.getParameterValue(EngineProperties.TARGET_OFF_HEAP_MEMORY, 8 * 1024 + "");
        if (targetOffHeapString.equals(TARGET_MEMORY_STRING)) {
            return TARGET_MEMORY;
        } else {
            try {
                long sizeMB = Long.parseLong(targetOffHeapString);
                TARGET_MEMORY = sizeMB * 1024 * 1024;
            } catch (NumberFormatException e) {
                TARGET_MEMORY = 8 * 1024 * 1024 * 1024l;
            }
            TARGET_MEMORY_STRING = targetOffHeapString;
            return TARGET_MEMORY;
        }
    }
}
