package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.IntToDoubleFunction;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractVectorTransformation;
import com.owc.singularity.engine.tools.function.ObjObjIntIntConsumer;


public class NumericContextualVectorTransformation<C> extends AbstractVectorTransformation {

    private final ObjObjIntIntConsumer<C, IntToDoubleFunction> contextUpdate;
    private final ToDoubleFunction<C> valueDerivationFunction;
    private final ThreadLocal<C> context;
    protected IntToDoubleFunction vectorAccessor;

    public NumericContextualVectorTransformation(String newAttributeName, String sourceAttributeName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntToDoubleFunction> contextUpdate, ToDoubleFunction<C> valueDerivationFunction) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.context = ThreadLocal.withInitial(contextSupplier);
        this.contextUpdate = contextUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        vectorAccessor = row -> sourceSet.getNumericValue(row, attributeIndex);
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            // first update the state
            C localContext = context.get();
            contextUpdate.accept(localContext, vectorAccessor, row, maxRows);
            dataColumn.setNumericValue(row, valueDerivationFunction.applyAsDouble(localContext));
        });
    }
}
