package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.ObjLongConsumer;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class TimestampStatefulPointTransformation<S> extends AbstractPointTransformation {

    private Supplier<S> stateSupplier;
    private ObjLongConsumer<S> stateUpdate;
    private ToLongFunction<S> valueDerivationFunction;
    private S state;

    public TimestampStatefulPointTransformation(String newAttributeName, String sourceAttributeName, Supplier<S> stateSupplier, ObjLongConsumer<S> stateUpdate,
            ToLongFunction<S> valueDerivationFunction) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.stateSupplier = stateSupplier;
        this.stateUpdate = stateUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        this.state = stateSupplier.get();
        for (int i = 0; i < sourceSet.size(); i++) {
            stateUpdate.accept(state, sourceSet.getTimestampValue(i, attributeIndex));
            dataColumn.setTimestampValue(i, valueDerivationFunction.applyAsLong(state));
        }
    }
}
