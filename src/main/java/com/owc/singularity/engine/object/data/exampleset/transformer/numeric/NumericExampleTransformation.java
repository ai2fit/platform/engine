package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.Example;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class NumericExampleTransformation extends AbstractRowTransformation {

    private final ToDoubleFunction<Example> derive;

    public NumericExampleTransformation(String newAttributeName, ToDoubleFunction<Example> derive) {
        super(newAttributeName, ValueType.NUMERIC);
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> dataColumn.setNumericValue(row, derive.applyAsDouble(sourceSet.getExample(row))));
    }

}
