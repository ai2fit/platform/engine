/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;

public enum ValueType {

    NOMINAL, NUMERIC, TIMESTAMP;

    ValueType() {}


    public static final ValueType[] ALL = new ValueType[] { NOMINAL, NUMERIC, TIMESTAMP };
    public static final double MISSING_NUMERIC = Double.NaN;
    public static final String MISSING_NOMINAL = null;
    public static final long MISSING_TIMESTAMP = Long.MIN_VALUE + 1;


    public static boolean isMissing(double value) {
        return value != value;
    }

    public static boolean isMissing(long value) {
        return value == MISSING_TIMESTAMP;
    }

    @SuppressWarnings("StringEquality")
    public static boolean isMissing(String value) {
        return value == MISSING_NOMINAL;
    }

    /**
     * This method allows to convert an old integer style coding into the new ValueType enum
     * constants.
     * 
     * @param oldCompatibilityIntegerCoding
     *            the integer denoting a value type
     * @return the current valuetype
     */
    public static ValueType valueOf(int oldCompatibilityIntegerCoding) {
        switch (oldCompatibilityIntegerCoding) {
            case 2:
            case 3:
            case 4:
                return ValueType.NUMERIC;
            case 9:
            case 10:
            case 11:
                return ValueType.TIMESTAMP;
            default:
                return ValueType.NOMINAL;
        }
    }

}
