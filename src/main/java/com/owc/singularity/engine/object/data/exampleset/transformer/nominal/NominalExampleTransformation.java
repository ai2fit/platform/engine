package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.Function;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.Example;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class NominalExampleTransformation extends AbstractRowTransformation {

    private final Function<Example, String> derive;

    public NominalExampleTransformation(String newAttributeName, Function<Example, String> derive) {
        super(newAttributeName, ValueType.NOMINAL);
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> dataColumn.setNominalValue(row, derive.apply(sourceSet.getExample(row))));
    }
}
