/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeDateFormat;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeTupel;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Tools;


/**
 * The condition is fulfilled if the individual filters are fulfilled. This filter can be
 * constructed from several conditions of the type {@link CustomFilters} which either must all be
 * fulfilled (AND) or only one must be fulfilled (OR).
 *
 * TODO: This class is insane bullshit. Parsing must be separated from checking and it should reuse
 * the existing {@link AttributeValueSingleConditionFilter} implementation instead of reinventing
 * the wheel over 1000 lines of code.
 *
 * @author Marco Boeck
 */
public class CustomFilter implements ExampleSetRowFilter {

    /**
     * Enum for custom filters.
     */
    public enum CustomFilters {

        EQUALS_NUMERICAL("gui.comparator.numerical.equals", "eq", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                // special case to handle missing values
                if (Double.isNaN(filter)) {
                    return Double.isNaN(input);
                }
                return input == filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        NOT_EQUALS_NUMERICAL("gui.comparator.numerical.not_equals", "ne", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                // special case to handle missing values
                if (Double.isNaN(input)) {
                    return !Double.isNaN(filter);
                }
                return input != filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        LESS("gui.comparator.numerical.less", "lt", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return input < filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        LESS_EQUALS("gui.comparator.numerical.less_equals", "le", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return input <= filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        GREATER_EQUALS("gui.comparator.numerical.greater_equals", "ge", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return input >= filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        GREATER("gui.comparator.numerical.greater", "gt", ValueType.NUMERIC) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return input > filter;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },

        EQUALS_NOMINAL("gui.comparator.nominal.equals", "equals", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return Objects.equals(input, filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        NOT_EQUALS_NOMINAL("gui.comparator.nominal.not_equals", "does_not_equal", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return !Objects.equals(input, filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        IS_IN_NOMINAL("gui.comparator.nominal.is_in", "is_in", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                List<String> filterList = Tools.unescape(filter, ESCAPE_CHAR, new char[] { SEPERATOR_CHAR }, SEPERATOR_CHAR);
                for (String filterString : filterList) {
                    if (Objects.equals(input, filterString)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        IS_NOT_IN_NOMINAL("gui.comparator.nominal.is_not_in", "is_not_in", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                List<String> filterList = Tools.unescape(filter, ESCAPE_CHAR, new char[] { SEPERATOR_CHAR }, SEPERATOR_CHAR);
                for (String filterString : filterList) {
                    if (Objects.equals(input, filterString)) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        CONTAINS("gui.comparator.nominal.contains", "contains", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                if (input == null || filter == null)
                    return false;
                return input.contains(filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        NOT_CONTAINS("gui.comparator.nominal.not_contains", "does_not_contain", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                if (input == null || filter == null)
                    return true;
                return !input.contains(filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        STARTS_WITH("gui.comparator.nominal.starts_with", "starts_with", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                if (input == null || filter == null) {
                    return false;
                }
                return input.startsWith(filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        ENDS_WITH("gui.comparator.nominal.ends_with", "ends_with", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                if (input == null || filter == null) {
                    return false;
                }
                return input.endsWith(filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        REGEX("gui.comparator.nominal.regex", "matches", ValueType.NOMINAL) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                if (input == null || filter == null)
                    return false;
                return input.matches(filter);
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return false;
            }
        },
        MISSING("gui.comparator.special.is_missing", "is_missing", null) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return isMissing;
            }
        },
        NOT_MISSING("gui.comparator.special.is_not_missing", "is_not_missing", null) {

            @Override
            public boolean isNumericalConditionFulfilled(final double input, final double filter) {
                return false;
            }

            @Override
            public boolean isNominalConditionFulfilled(final String input, final String filter) {
                return false;
            }

            @Override
            public boolean isMissingConditionFulfilled(final boolean isMissing) {
                return !isMissing;
            }
        };

        /** the symbol to seperate strings for IS_IN and IS_NOT_IN input */
        public static final char SEPERATOR_CHAR = ';';

        /** the symbol to escape seperator symbols in IS_IN and IS_NOT_IN input */
        public static final char ESCAPE_CHAR = '\\';

        /** the format string for date_time */
        public static final String DATE_TIME_FORMAT_STRING = "MM/dd/yyyy h:mm:ss a";

        /** the old (bugged) format string for date_time */
        public static final String DATE_TIME_FORMAT_STRING_OLD = "MM/dd/yy h:mm:ss a";

        /** the format string for date */
        public static final String DATE_FORMAT_STRING = ParameterTypeDateFormat.DATE_FORMAT_MM_DD_YYYY;

        /** the old (bugged) format string for date */
        public static final String DATE_FORMAT_STRING_OLD = "MM/dd/yy";

        /** the format string for time */
        public static final String TIME_FORMAT_STRING = "h:mm:ss a";

        // ThreadLocal because DateFormat is NOT threadsafe and creating a new DateFormat is
        // EXTREMELY expensive
        /** the format for date_time */
        private static final ThreadLocal<DateFormat> FORMAT_DATE_TIME = ThreadLocal
                .withInitial(() -> new SimpleDateFormat(DATE_TIME_FORMAT_STRING, Locale.ENGLISH));

        /** the old format for date_time */
        private static final ThreadLocal<DateFormat> FORMAT_DATE_TIME_OLD = ThreadLocal
                .withInitial(() -> new SimpleDateFormat(DATE_TIME_FORMAT_STRING_OLD, Locale.ENGLISH));

        // ThreadLocal because DateFormat is NOT threadsafe and creating a new DateFormat is
        // EXTREMELY expensive
        /** the format for date */
        private static final ThreadLocal<DateFormat> FORMAT_DATE = ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_FORMAT_STRING, Locale.ENGLISH));

        /** the old format for date */
        private static final ThreadLocal<DateFormat> FORMAT_DATE_OLD = ThreadLocal
                .withInitial(() -> new SimpleDateFormat(DATE_FORMAT_STRING_OLD, Locale.ENGLISH));

        // ThreadLocal because DateFormat is NOT threadsafe and creating a new DateFormat is
        // EXTREMELY expensive
        /** the format for time */
        private static final ThreadLocal<DateFormat> FORMAT_TIME = ThreadLocal.withInitial(() -> new SimpleDateFormat(TIME_FORMAT_STRING, Locale.ENGLISH));

        /** the label for this filter */
        private String label;

        /** the string representation for this filter */
        private String symbol;

        /** the help text for this filter */
        private String helptext;

        /** the valueType for this filter */
        private ValueType valueType;

        /**
         * Creates a new {@link CustomFilters} instance which is represented by the specified symbol
         * and accepts the given {@link ValueType}.
         *
         * @param key
         * @param symbol
         * @param numeric
         *            the applicable {@link ValueType}. If set to -1, denotes a special filter which
         *            is not restricted to any value type
         */
        private CustomFilters(final String key, final String symbol, final ValueType numeric) {
            this.symbol = symbol;
            this.label = I18N.getGUIMessage(key + ".label");
            this.helptext = I18N.getGUIMessage(key + ".tip");
            this.valueType = numeric;
        }

        /**
         * Returns the {@link String} label for this comparator.
         *
         * @return
         */
        public String getLabel() {
            return label;
        }

        /**
         * Returns the {@link String} representation for this comparator.
         *
         * @return
         */
        public String getSymbol() {
            return symbol;
        }

        /**
         * Returns the helptext for this comparator.
         *
         * @return
         */
        public String getHelptext() {
            return helptext;
        }

        /**
         * Returns <code>true</code> if this filter is applicable for numerical values;
         * <code>false</code> otherwise.
         *
         * @return
         */
        public boolean isNumericalFilter() {
            return !isMissingFilter() && valueType.equals(ValueType.NUMERIC);
        }

        /**
         * Returns <code>true</code> if this filter is applicable for nominal values;
         * <code>false</code> otherwise.
         *
         * @return
         */
        public boolean isNominalFilter() {
            return !isMissingFilter() && valueType.equals(ValueType.NOMINAL);
        }

        /**
         * Returns <code>true</code> if this filter is a special filter (e.g. missing value filter);
         * <code>false</code> otherwise. <br/>
         * These filters are applicable for all attribute types.
         *
         * @return
         */
        public boolean isMissingFilter() {
            return valueType == null;
        }

        /**
         * Returns <code>true</code> if the numerical condition for this filter is fulfilled for the
         * given value. Returns always <code>false</code> if the condition is for nominal values
         * only.
         *
         * @param input
         * @param filterValue
         * @return
         */
        public abstract boolean isNumericalConditionFulfilled(double input, double filterValue);

        /**
         * Returns <code>true</code> if the nominal condition for this filter is fulfilled for the
         * given value. Returns always <code>false</code> if the condition is for numerical values
         * only.
         *
         * @param input
         * @param filterValue
         * @return
         */
        public abstract boolean isNominalConditionFulfilled(String input, String filterValue);

        /**
         * Returns <code>true</code> if the special condition for this filter is fulfilled for the
         * given value.
         *
         * @param isMissing
         * @return
         */
        public abstract boolean isMissingConditionFulfilled(boolean isMissing);

        /**
         * Returns the {@link CustomFilters} matching the given label {@link String}. If none can be
         * found, returns <code>null</code>.
         *
         * @param label
         * @return
         */
        public static CustomFilters getByLabel(final String label) {
            for (CustomFilters filter : values()) {
                if (filter.getLabel().equals(label)) {
                    return filter;
                }
            }
            return null;
        }

        /**
         * Returns the {@link CustomFilters} matching the given symbol {@link String}. If none can
         * be found, returns <code>null</code>.
         *
         * @param symbol
         * @return
         */
        public static CustomFilters getBySymbol(final String symbol) {
            for (CustomFilters filter : values()) {
                if (filter.getSymbol().equals(symbol)) {
                    return filter;
                }
            }
            return null;
        }

        /**
         * Returns a list of {@link CustomFilters}s for the given {@link ValueType}. Returns an
         * empty list if no filter was found.
         *
         * @param valueType
         * @return
         */
        public static List<CustomFilters> getFiltersForValueType(final ValueType valueType) {
            List<CustomFilters> list = new LinkedList<>();
            for (CustomFilters filter : values()) {
                if (valueType == null) {
                    // unknown value type right now, allow all filters
                    list.add(filter);
                } else if (valueType.equals(ValueType.NOMINAL)) {
                    // only nominal filters
                    if (filter.isMissingFilter() || filter.isNominalFilter()) {
                        list.add(filter);
                    }
                } else if (valueType.equals(ValueType.NUMERIC)) {
                    // only numerical filters
                    if (filter.isMissingFilter() || filter.isNumericalFilter()) {
                        list.add(filter);
                    }
                } else if (valueType.equals(ValueType.TIMESTAMP)) {
                    // only numerical filters
                    if (filter.isMissingFilter() || filter.isNumericalFilter()) {
                        list.add(filter);
                    }
                } else {
                    // filter only defined for numerical or nominal or unknown
                }
            }

            return list;
        }

        /**
         * Returns a {@link Date} parsed via the date {@link String} or <code>null</code> if the
         * given string could not be parsed. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_DATE_TIME
         * @param dateTimeString
         * @return
         */
        public Date parseDateTime(final String dateTimeString) {
            try {
                return FORMAT_DATE_TIME.get().parse(dateTimeString);
            } catch (ParseException e) {
                return null;
            }
        }

        /**
         * Old parser for date_time.
         *
         * @param dateTimeString
         * @return
         */
        private Date parseDateTimeOld(final String dateTimeString) {
            try {
                return FORMAT_DATE_TIME_OLD.get().parse(dateTimeString);
            } catch (ParseException e) {
                return null;
            }
        }

        /**
         * Returns a {@link Date} parsed via the date {@link String} or <code>null</code> if the
         * given string could not be parsed. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_DATE
         * @param dateString
         * @return
         */
        public Date parseDate(final String dateString) {
            try {
                return FORMAT_DATE.get().parse(dateString);
            } catch (ParseException e) {
                return null;
            }
        }

        /**
         * Old parser for date.
         *
         * @param dateString
         * @return
         */
        private Date parseDateOld(final String dateString) {
            try {
                return FORMAT_DATE_OLD.get().parse(dateString);
            } catch (ParseException e) {
                return null;
            }
        }

        /**
         * Returns a {@link Date} parsed via the date {@link String} or <code>null</code> if the
         * given string could not be parsed. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_TIME
         * @param timeString
         * @return
         */
        public Date parseTime(final String timeString) {
            try {
                return FORMAT_TIME.get().parse(timeString);
            } catch (ParseException e) {
                return null;
            }
        }

        /**
         * Returns a {@link String} formatted from the {@link Date}. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_TIME
         * @param dateTime
         * @return
         */
        public String formatDateTime(final Date dateTime) {
            if (dateTime == null) {
                throw new IllegalArgumentException("dateTime must not be null!");
            }
            return FORMAT_DATE_TIME.get().format(dateTime);
        }

        /**
         * Old format for date_time.
         *
         * @param dateTime
         * @return
         */
        public String formatDateTimeOld(final Date dateTime) {
            if (dateTime == null) {
                throw new IllegalArgumentException("dateTime must not be null!");
            }
            return FORMAT_DATE_TIME_OLD.get().format(dateTime);
        }

        /**
         * Returns a {@link String} formatted from the {@link Date}. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_TIME
         * @param date
         * @return
         */
        public String formatDate(final Date date) {
            if (date == null) {
                throw new IllegalArgumentException("date must not be null!");
            }
            return FORMAT_DATE.get().format(date);
        }

        /**
         * Returns a {@link String} formatted from the {@link Date}. Uses {@link Locale#ENGLISH}.
         * <p>
         * Not static because {@link DateFormat} is NOT threadsafe.
         * </p>
         *
         * @see #FORMAT_TIME
         * @param time
         * @return
         */
        public String formatTime(final Date time) {
            if (time == null) {
                throw new IllegalArgumentException("time must not be null!");
            }
            return FORMAT_TIME.get().format(time);
        }
    }

    private static final long serialVersionUID = -1369785656210631292L;

    private static final String WHITESPACE = " ";
    private static final String BACKSLASH = "/";

    private static final int CONDITION_ARRAY_REQUIRED_SIZE = 2;
    private static final int CONDITION_ARRAY_CONDITION_INDEX = 1;

    private static final int CONDITION_TUPEL_REQUIRED_SIZE = 3;
    private static final int CONDITION_TUPEL_ATT_INDEX = 0;
    private static final int CONDITION_TUPEL_FILTER_INDEX = 1;
    private static final int CONDITION_TUPEL_VALUE_INDEX = 2;

    /** the list of all conditions */
    private List<String[]> conditions = new LinkedList<>();

    /**
     * an array which indicates if for the ordered filter index the old (bugged) date parsing should
     * be used
     */
    private boolean[] conditionsOldDateFilter;

    /**
     * the {@link VariableHandler}, will be used to resolve filter values, can be <code>null</code>
     */
    private VariableHandler variableHandler;

    private boolean fulfillAllConditions;
    private boolean resolveAttName;

    /**
     * Creates a new {@link CustomFilter} instance with the {@link CustomFilters} encoded in the
     * {@link List} of {@link String} arrays. The {@link Boolean} parameter defines if either all
     * conditions must be fulfilled or only one of them.
     *
     * @param conditions
     * @param fulfillAllConditions
     * @param resolveAttName
     *            whether to resolve attribute names with variables
     *
     * @since 9.6
     */
    public CustomFilter(final List<String[]> conditions, final boolean fulfillAllConditions, boolean resolveAttName) {
        if (conditions == null) {
            throw new IllegalArgumentException("typeList must not be null!");
        }
        this.resolveAttName = resolveAttName;

        conditionsOldDateFilter = new boolean[conditions.size()];


        this.conditions = conditions;
        this.fulfillAllConditions = fulfillAllConditions;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (String[] array : conditions) {
            builder.append(Arrays.toString(array));
            builder.append(' ');
        }
        return builder.toString();
    }

    @Override
    public boolean pass(int row, ExampleSet exampleSet) {
        boolean conditionsFulfilled = fulfillAllConditions;
        int counter = 0;
        for (String[] conditionArray : conditions) {
            // we checked for malformed conditions in the constructor so no need to do it again
            String condition = conditionArray[CONDITION_ARRAY_CONDITION_INDEX];
            String[] conditionTupel = ParameterTypeTupel.transformString2Tupel(condition);
            String attName = conditionTupel[CONDITION_TUPEL_ATT_INDEX];
            String filterSymbol = conditionTupel[CONDITION_TUPEL_FILTER_INDEX];
            CustomFilters filter = CustomFilters.getBySymbol(filterSymbol);
            String filterValue = conditionTupel[CONDITION_TUPEL_VALUE_INDEX];
            if (variableHandler != null) {
                filterValue = substituteVariables(filterValue, variableHandler);
                if (resolveAttName) {
                    attName = substituteVariables(attName, variableHandler);
                }
            }
            Attribute att = exampleSet.getAttributes().get(attName);

            // check if condition is fulfilled
            boolean fulfilled;
            if (filter.isMissingFilter()) {
                fulfilled = filter.isMissingConditionFulfilled(switch (att.getValueType()) {
                    case NOMINAL -> ValueType.isMissing(exampleSet.getNominalValue(row, att.getIndex()));
                    case NUMERIC -> ValueType.isMissing(exampleSet.getNumericValue(row, att.getIndex()));
                    case TIMESTAMP -> ValueType.isMissing(exampleSet.getTimestampValue(row, att.getIndex()));
                });
            } else if (filter.isNominalFilter()) {
                fulfilled = filter.isNominalConditionFulfilled(exampleSet.getNominalValue(row, att.getIndex()), filterValue);
            } else {
                fulfilled = checkNumericalCondition(exampleSet, row, att, filter, filterSymbol, filterValue, conditionsOldDateFilter[counter]);
            }

            // store result
            if (fulfillAllConditions) {
                conditionsFulfilled &= fulfilled;
            } else {
                conditionsFulfilled |= fulfilled;
            }

            // shortcut for OR - one fulfilled condition is enough
            if (!fulfillAllConditions && conditionsFulfilled) {
                return true;
            }

            counter++;
        }

        return conditionsFulfilled;
    }

    /**
     * Returns <code>true</code> if the given filter is fulfilled for the given value.
     *
     * @param exampleSet
     * @param att
     * @param filter
     * @param filterSymbol
     * @param filterValue
     * @param oldBehavior
     *            if <code>true</code>, old, bugged parsing with format dd/MM/yy will be used
     * @return
     */
    private boolean checkNumericalCondition(final ExampleSet exampleSet, int row, final Attribute att, final CustomFilters filter, final String filterSymbol,
            final String filterValue, final boolean oldBehavior) {
        // special handling because we can have DATE_TIME, DATE and TIME strings in human readable
        // format here
        double doubleOriginalValue;
        if (att.isNumerical())
            doubleOriginalValue = exampleSet.getNumericValue(row, att.getIndex());
        else
            doubleOriginalValue = exampleSet.getTimestampValue(row, att.getIndex());

        double doubleFilterValue;
        try {
            doubleFilterValue = Double.parseDouble(filterValue);
        } catch (NumberFormatException e1) {
            // if we have a date we are losing precision - therefore we need to convert the original
            // value back and forth once so both lose the same amount of precision -
            // otherwise the filters will not work correctly
            if (att.getValueType().equals(ValueType.TIMESTAMP)) {
                String formattedOriginal = filter.formatDateTime(new Date((long) doubleOriginalValue));
                doubleOriginalValue = filter.parseDateTime(formattedOriginal).getTime();
                // keep compatibility with processes from versions prior to 6.0.004
                if (oldBehavior) {
                    // if year consists of 2 chars, use old (bugged) version
                    doubleFilterValue = filter.parseDateTimeOld(filterValue).getTime();
                } else {
                    // new behavior
                    doubleFilterValue = filter.parseDateTime(filterValue).getTime();
                }
            } else {
                // because we have checked the filters in the constructor, this is the only option
                // left
                // special handling for ? as missing value
                doubleFilterValue = Double.NaN;
            }
        }

        return filter.isNumericalConditionFulfilled(doubleOriginalValue, doubleFilterValue);
    }

    /**
     * Tries to parse the given {@link String} to a {@link Double} and returns <code>true</code> if
     * successful; <code>false</code> otherwise.
     *
     * @param value
     * @param att
     * @return
     */
    private boolean isStringValidDoubleValue(final CustomFilters filter, final String value, final Attribute att) {
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException e1) {
            // if we have a date we are losing precision - therefore we need to convert the original
            // value back and forth once so both lose the same amount of precision -
            // otherwise the filters will not work correctly
            if (att.getValueType().equals(ValueType.TIMESTAMP)) {
                if (filter.parseDateTime(value) == null) {
                    return false;
                }
            } else {
                // special handling for ? as missing value
                // or all parsing tries failed
                return "?".equals(value);
            }
        }
        return true;
    }

    /**
     * Tries to substitute variables with their real value.
     *
     * @param value
     * @param variableHandler
     * @return
     */
    private static String substituteVariables(String value, final VariableHandler variableHandler) {
        int startIndex = value.indexOf("%{");
        if (startIndex == -1) {
            return value;
        }
        try {
            StringBuilder result = new StringBuilder();
            while (startIndex >= 0) {
                result.append(value, 0, startIndex);
                int endIndex = value.indexOf('}', startIndex + 2);
                String variableString = value.substring(startIndex + 2, endIndex);
                String variableValue = variableHandler.getVariable(variableString);
                if (variableValue != null) {
                    result.append(variableValue);
                } else {
                    result.append("%{").append(variableString).append('}');
                }
                value = value.substring(endIndex + 1);
                startIndex = value.indexOf("%{");
            }
            result.append(value);
            return result.toString();
        } catch (Exception e) {
            return value;
        }
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        // check if given conditions list is well formed and valid!
        int counter = 0;
        for (String[] conditionArray : conditions) {
            if (conditionArray.length != CONDITION_ARRAY_REQUIRED_SIZE) {
                throw new UserError(null, "custom_filters.exceeded_length_condition");
            }

            String condition = conditionArray[CONDITION_ARRAY_CONDITION_INDEX];
            String[] conditionTupel = ParameterTypeTupel.transformString2Tupel(condition);
            if (conditionTupel.length != CONDITION_TUPEL_REQUIRED_SIZE) {
                throw new UserError(null, "custom_filters.malformed_condition", conditionTupel.length);
            }

            String attName = conditionTupel[CONDITION_TUPEL_ATT_INDEX];
            String filterSymbol = conditionTupel[CONDITION_TUPEL_FILTER_INDEX];
            CustomFilters filter = CustomFilters.getBySymbol(filterSymbol);
            String filterValue = conditionTupel[CONDITION_TUPEL_VALUE_INDEX];

            Attribute att = exampleSet.getAttributes().get(attName);
            if (filter == null) {
                throw new UserError(null, "custom_filters.filter_not_found", filterSymbol);
            }
            if (att == null) {
                throw new UserError(null, "custom_filters.attribute_not_found", attName);
            }

            // special checks for numerical filters
            if (filter.isNumericalFilter()) {
                // check if attribute is numerical
                if (att.isNominal()) {
                    throw new UserError(null, "custom_filters.numerical_comparator_type_invalid", filter.getLabel(), att.getName());
                }
                // check if filter value works for numerical attribute
                if (att.isTimestamp()) {
                    // check if filter value works for date attribute
                    if (filterValue == null || filterValue.isEmpty() || !isStringValidDoubleValue(filter, filterValue, att)) {
                        throw new UserError(null, "custom_filters.illegal_date_value", filterValue, att.getName());
                    }
                } else if (att.isNumerical() && (filterValue == null || filterValue.isEmpty() || !isStringValidDoubleValue(filter, filterValue, att))) {
                    throw new UserError(null, "custom_filters.illegal_numerical_value", filterValue, att.getName());
                }

                // keep compatibility with processes from versions prior to 6.0.004
                // only affects DATE and DATE_TIME filters
                int yearIndex = filterValue.lastIndexOf(BACKSLASH) + 1;
                int firstWhitespaceIndex = filterValue.indexOf(WHITESPACE);
                String yearString = null;
                if (yearIndex > 0 && firstWhitespaceIndex > 0 && yearIndex < firstWhitespaceIndex) {
                    yearString = filterValue.substring(yearIndex, firstWhitespaceIndex);
                }

                // if true, the old (bugged) parsing will be used; otherwise the new yyyy parsing
                // will be used
                conditionsOldDateFilter[counter] = yearString != null && yearString.length() == 2;
            } else if (filter.isNominalFilter() && !att.isNominal()) {
                throw new UserError(null, "custom_filters.nominal_comparator_type_invalid", filter.getLabel(), att.getName());
            }

            counter++;
        }

    }
}
