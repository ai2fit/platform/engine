package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.Function;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NominalPointTransformation extends AbstractPointTransformation {

    private Function<String, String> pointDerivation;

    public NominalPointTransformation(String newAttributeName, String sourceAttributeName, Function<String, String> pointDerivation) {
        super(newAttributeName, ValueType.NOMINAL, sourceAttributeName);
        this.pointDerivation = pointDerivation;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNominalValue(row, pointDerivation.apply(sourceSet.getNominalValue(row, attributeIndex))));

    }
}
