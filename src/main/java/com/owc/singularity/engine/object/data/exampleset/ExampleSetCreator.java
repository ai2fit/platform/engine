package com.owc.singularity.engine.object.data.exampleset;


import java.io.Serializable;
import java.time.Instant;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Phaser;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;

/**
 * This is the central class for creating new {@link ExampleSet}s. There are three ways of operating
 * this creator:
 * <ul>
 * <li>Either go through the data set row by row, setting each value of a row with
 * {@link #setNominalValue(int, String)}, {@link #setNumericValue(int, double)},
 * {@link #setTimestampValue(int, long)} or, if no value present with {@link #setMissing(int)}.<br>
 * After ALL columns of a row have been set, the {@link #commit()} or {@link #commitAndKeep()}
 * method finish the row.</li>
 * <li>Or provide functions for filling columns using
 * {@link #withNominalAttributeContent(int, IntFunction)},
 * {@link #withNumericAttributeContent(int, IntToDoubleFunction)},
 * {@link #withTimestampAttributeContent(int, IntToLongFunction)}.</li>
 * <li>Or in cases a common complex calculation determines a set of rows and cannot be stored in
 * memory efficiently, you can resort to random access using
 * {@link #setNominalValue(int, int, String)}, {@link #setNumericValue(int, int, double)},
 * {@link #setTimestampValue(int, int, long)}.</li>
 * 
 * </ul>
 * The second mode performs much faster as it tries to fill the columns in parallel. So the provided
 * functions must be able to support random access on row content and must be multi-thread safe.<br>
 * The random access methods don't provide any safety and can result in Access Violations. Please be
 * careful and only use these methods if the other ways don't work.<br>
 * 
 * Please note that addressing the columns using indices is much faster than using the names of the
 * columns. So when using the row-wise method, please provide indexes instead of names. <br>
 * Roles of the created {@link ExampleSet} can be set using {@link #withRole(int, String)} or
 * {@link #withRole(String, String)} methods. Each attribute with a role will also be special. <br>
 * To create an {@link ExampleSet}, the {@link #finish()} and {@link #finishAndReset()} methods can
 * be used. The later will re-init this creator in order to create another {@link ExampleSet} with
 * the same structure.
 * 
 * @author sland
 *
 */
public final class ExampleSetCreator implements Serializable {

    private static final long serialVersionUID = 7265393408399330859L;
    /**
     * so we are using two pages of memory as most values are 8bytes. We reduce by one to compensate
     * for the single additional entry in each data column at the end. This way the capacity is
     * always aligned on page sizes.
     */
    private static final int INITIAL_CAPACITY = (MemoryUtils.UNSAFE.pageSize() >> 1) - 1;
    private static final int CAPACITY_INCREMENT = MemoryUtils.UNSAFE.pageSize();
    private static final int MAX_CAPACITY_INCREMENT = 1 << 20;

    private Map<String, Integer> attributeIndexMap;
    private Phaser phaser = new Phaser();

    private boolean isReopened;
    private String[] attributeNames;
    private ValueType[] valueTypes;
    private String[] attributeRoles;
    private boolean[] attributeIsSpecial;

    private transient DataColumn[] dataColumns;

    private transient LinkedList<IntConsumer> columnFillers = new LinkedList<>();

    private int initialCapacity;
    private transient int capacity = INITIAL_CAPACITY;
    private transient int capacityIncrement = CAPACITY_INCREMENT;
    private transient int currentRow = 0;
    private transient int size = 0;
    private transient int finalSize = -1;
    private transient int markedRow;
    private ExampleSetMetaData metaData;

    private String[] lastNominalValues;
    private long[] lastTimestampValues;
    private double[] lastNumericValues;

    /**
     *
     * This method creates a new ExampleSetCreator with the named attributes and their type. Order
     * of the map is preserved, all attributes will be regular.
     *
     * @param nameAndTypes
     *            Name and Types of the attributes
     */
    public ExampleSetCreator(LinkedHashMap<String, ValueType> nameAndTypes) {
        String[] attributeNames = new String[nameAndTypes.size()];
        ValueType[] attributeTypes = new ValueType[nameAndTypes.size()];
        int i = 0;
        for (Entry<String, ValueType> entry : nameAndTypes.entrySet()) {
            attributeNames[i] = entry.getKey();
            attributeTypes[i++] = entry.getValue();
        }
        init(attributeNames, attributeTypes, new String[attributeNames.length], new boolean[attributeNames.length], INITIAL_CAPACITY);
    }

    /**
     * This method creates a new ExampleSetCreator for an exampleSet with the specified attributes.
     * Notice that attributeNames and valueTypes must be of same length.
     *
     * @param attributeNames
     *            The names of the attributes part of the new example set. These attributes' names
     *            can be used for the setValue methods to specify the attribute.
     * @param valueTypes
     *            The value types of the attributes as specified by {@link ValueType}.
     */
    public ExampleSetCreator(String[] attributeNames, ValueType[] valueTypes) {
        init(attributeNames, valueTypes, new String[attributeNames.length], new boolean[attributeNames.length], INITIAL_CAPACITY);
    }


    /**
     * This method creates a new ExampleSetCreator for an exampleSet with the specified attributes.
     * Notice that attributeNames and valueTypes must be of same length.
     *
     * @param attributeNames
     *            The names of the attributes part of the new example set. These attributes' names
     *            can be used for the setValue methods to specify the attribute.
     * @param valueTypes
     *            The value types of the attributes as specified by {@link ValueType}.
     */
    public ExampleSetCreator(String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles, boolean[] isSpecialAttribute) {
        init(attributeNames, valueTypes, attributeRoles, isSpecialAttribute, INITIAL_CAPACITY);
    }

    /**
     * This method creates a new ExampleSetCreator for an exampleSet of fixed sized with the
     * specified attributes. This can be used to efficiently allocate memory.<br>
     * Notice that attributeNames and valueTypes must be of same length. This
     *
     * @param attributeNames
     *            The names of the attributes part of the new example set. These attributes' names
     *            can be used for the setValue methods to specify the attribute.
     * @param valueTypes
     *            The value types of the attributes as specified by {@link ValueType}.
     */
    public ExampleSetCreator(String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles, boolean[] isSpecialAttribute, int finalSize) {
        init(attributeNames, valueTypes, attributeRoles, isSpecialAttribute, finalSize);
        setSize(finalSize);
    }

    /**
     * This method creates a new exampleSetCreator with the attributeNames and types of the provided
     * exampleSet.
     *
     * @param exampleSet
     *            This is the example set from which the attributeNames and types are taken from.
     *
     */
    public ExampleSetCreator(ExampleSet exampleSet) {
        init(Arrays.stream(exampleSet.getAttributes().attributes).map(Attribute::getName).toArray(String[]::new),
                Arrays.stream(exampleSet.getAttributes().attributes).map(Attribute::getValueType).toArray(ValueType[]::new),
                exampleSet.getAttributes().attributesRole, exampleSet.getAttributes().attributesIsSpecial, INITIAL_CAPACITY);
    }

    private void init(String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles, boolean[] attributeIsSpecial, int capacity) {
        // We have to check whether all attribute names are unique
        HashSet<String> containedAttributeNames = new HashSet<>();
        for (String attributeName : attributeNames) {
            if (!containedAttributeNames.add(attributeName)) {
                throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, attributeName));
            }
        }

        this.attributeNames = attributeNames;
        this.valueTypes = valueTypes;
        this.attributeRoles = attributeRoles;
        this.attributeIsSpecial = attributeIsSpecial;
        this.capacity = capacity;
        this.capacityIncrement = CAPACITY_INCREMENT;
        this.attributeIndexMap = new HashMap<>();
        for (

                int i = 0; i < attributeNames.length; i++) {
            this.attributeIndexMap.put(attributeNames[i], i);
        }
        currentRow = 0;
        size = 0;
        finalSize = -1;
        markedRow = 0;

        // creating data columns for all attributes
        this.dataColumns = new DataColumn[attributeNames.length];
        for (int i = 0; i < valueTypes.length; i++)
            dataColumns[i] = new DataColumn(capacity, valueTypes[i]);

        // creating last values structure for #commitAndKeep
        this.lastNominalValues = new String[attributeNames.length];
        this.lastNumericValues = new double[attributeNames.length];
        this.lastTimestampValues = new long[attributeNames.length];
    }

    /**
     * This sets the role of the attribute with the given index to the given role. The attribute is
     * also automatically set to be special.
     * 
     * @param attributeIndex
     *            the attribute index referring to the order when this creator was constructed.
     * @param role
     *            the role
     */
    public void withRole(int attributeIndex, String role) {
        attributeRoles[attributeIndex] = role;
        attributeIsSpecial[attributeIndex] = true;
    }

    /**
     * This sets the role of the attribute with the given name to the given role. The attribute is
     * also automatically set to be special. An attribute with the given name must be part of this
     * creator.
     * 
     * @param attributeName
     *            the attribute name
     * @param role
     *            the role
     */
    public void withRole(String attributeName, String role) {
        withRole(attributeIndexMap.get(attributeName), role);
    }

    public void withSpecial(int attributeIndex) {
        attributeIsSpecial[attributeIndex] = true;
    }

    public void withSpecial(String attributeName) {
        withSpecial(attributeIndexMap.get(attributeName));
    }

    public void withNumericAttributeContent(String attributeName, IntToDoubleFunction attributeContentFunction) {
        withNumericAttributeContent(attributeIndexMap.get(attributeName), attributeContentFunction);
    }

    public void withNumericAttributeContent(int attributeIndex, IntToDoubleFunction attributeContentFunction) {
        DataColumn targetColumn = dataColumns[attributeIndex];
        columnFillers.add(row -> targetColumn.setNumericValue(row, attributeContentFunction.applyAsDouble(row)));
    }


    public void withNominalAttributeContent(String attributeName, IntFunction<String> attributeContentFunction) {
        withNominalAttributeContent(attributeIndexMap.get(attributeName), attributeContentFunction);
    }

    public void withNominalAttributeContent(int attributeIndex, IntFunction<String> attributeContentFunction) {
        DataColumn targetColumn = dataColumns[attributeIndex];
        columnFillers.add(row -> targetColumn.setNominalValue(row, attributeContentFunction.apply(row)));
    }


    public void withTimestampAttributeContent(String attributeName, IntToLongFunction attributeContentFunction) {
        withTimestampAttributeContent(attributeIndexMap.get(attributeName), attributeContentFunction);
    }

    public void withTimestampAttributeContent(int attributeIndex, IntToLongFunction attributeContentFunction) {
        DataColumn targetColumn = dataColumns[attributeIndex];
        columnFillers.add(row -> targetColumn.setTimestampValue(row, attributeContentFunction.applyAsLong(row)));
    }


    /**
     * Creates a new instance of {@link ExampleSetCreator}. This allows to only define one static
     * per class for the MetaData Propagation and then get an instance whenever one creates a new
     * example set. <b>Warning! This will not copy any content functions as they are usually tied to
     * external resources one does not want to copy.</b>
     *
     * @return A new instance creating the same header, but size 0 and no content functions
     */
    public ExampleSetCreator getInstance() {
        return new ExampleSetCreator(attributeNames, valueTypes, attributeRoles, attributeIsSpecial);
    }

    /**
     * Creates a new instance of {@link ExampleSetCreator} with a specific size. This allows for
     * more efficient memory allocation. This allows to only define one static per class for the
     * MetaData Propagation and then get an instance whenever one creates a new example set.
     * <b>Warning! This will not copy any content functions as they are usually tied to external
     * resources one does not want to copy.</b>
     *
     * @return A new instance creating the same header, but size 0 and no content functions
     */
    public ExampleSetCreator getInstance(int finalSize) {
        return new ExampleSetCreator(attributeNames, valueTypes, attributeRoles, attributeIsSpecial, finalSize);
    }

    /**
     * Creates a new instance of {@link ExampleSetCreator} with additional attributes. This allows
     * to only define one static per class for the MetaData Propagation and then get an instance
     * dynamically when you need to add additional previously unknown attributes.
     *
     * @return the extended instance
     */
    public ExampleSetCreator getExtendedInstance(String[] additionalAttributeNames, ValueType[] additionalAttributeValueTypes) throws OperatorException {
        // first check if attribute is already present
        for (String additionalName : additionalAttributeNames) {
            if (attributeIndexMap.containsKey(additionalName))
                throw new OperatorException("Duplicate attribute names");
        }

        int thisNumberOfAttributes = attributeNames.length;
        int newSize = thisNumberOfAttributes + additionalAttributeNames.length;
        String[] attributeNames = new String[newSize];
        ValueType[] valueTypes = new ValueType[newSize];
        String[] attributeRoles = new String[newSize];
        boolean[] attributeIsSpecial = new boolean[newSize];
        System.arraycopy(this.attributeNames, 0, attributeNames, 0, thisNumberOfAttributes);
        System.arraycopy(this.attributeRoles, 0, attributeRoles, 0, thisNumberOfAttributes);
        System.arraycopy(this.valueTypes, 0, valueTypes, 0, thisNumberOfAttributes);
        System.arraycopy(this.attributeIsSpecial, 0, attributeIsSpecial, 0, thisNumberOfAttributes);

        System.arraycopy(additionalAttributeNames, 0, attributeNames, thisNumberOfAttributes, additionalAttributeNames.length);
        System.arraycopy(additionalAttributeValueTypes, 0, valueTypes, thisNumberOfAttributes, additionalAttributeValueTypes.length);

        return new ExampleSetCreator(attributeNames, valueTypes, attributeRoles, attributeIsSpecial);
    }


    /*
     * Row wise access
     */

    /**
     * Set method for values of the current example. This method is usable for all attributes that
     * are of valuetype NUMERIC.
     * 
     * @param attributeName
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */
    @Deprecated
    public final void setValue(String attributeName, double value) {
        setNumericValue(attributeIndexMap.get(attributeName), value);
    }

    /**
     * Set method for numeric values of the current row. This method is usable for all attributes
     * that are of valuetype NUMERIC. For performance reasons no checking is performed, so you need
     * to make sure that the given index is indeed pointing to a numerical attribute
     * 
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */

    public final void setNumericValue(int attributeIndex, double value) {
        dataColumns[attributeIndex].setNumericValue(currentRow, value);
        lastNumericValues[attributeIndex] = value;
    }


    /**
     * This method allows random writes into the data columns AFTER {@link #setSize(int)} was
     * called!<br>
     * If you access a row beyond the currently set size, it will result into a access violation and
     * kill the program. <br>
     * This is intend as a very low level access for certain, non standard operations. If not
     * absolutely necessary, please resolve to the other methods, which are more secure and result
     * in less complex code.
     * 
     * @param row
     *            the row to access. Must be within current size
     * @param attributeIndex
     *            the attribute index
     * @param value
     *            the numerical value
     */
    public final void setNumericValue(int row, int attributeIndex, double value) {
        dataColumns[attributeIndex].setNumericValue(row, value);
    }


    /**
     * Set method for numeric values in the row previously reserved by {@link #reserveRow()} in
     * parallel execution scenarios. This method is usable for all attributes that are of valuetype
     * NUMERIC. For performance reasons no checking is performed, so you need to make sure that the
     * given index is indeed pointing to a numerical attribute
     * 
     * @param rowHandle
     *            the row handle returned previously by {@link #reserveRow()}
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */

    public final void setNumericValueWithHandle(int rowHandle, int attributeIndex, double value) {
        dataColumns[attributeIndex].setNumericValue(rowHandle, value);
    }


    /**
     * Set method for timestamp values of the current row. This method is usable for all attributes
     * that are of valuetype TIMESTAMP. For performance reasons no checking is performed, so you
     * need to make sure that the given index is indeed pointing to a numerical attribute
     * 
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */

    public final void setTimestampValue(int attributeIndex, long value) {
        dataColumns[attributeIndex].setTimestampValue(currentRow, value);
        lastTimestampValues[attributeIndex] = value;
    }

    /**
     * This method allows random writes into the data columns AFTER {@link #setSize(int)} was
     * called!<br>
     * If you access a row beyond the currently set size, it will result into a access violation and
     * kill the program. <br>
     * This is intend as a very low level access for certain, non standard operations. If not
     * absolutely necessary, please resolve to the other methods, which are more secure and result
     * in less complex code.
     * 
     * @param row
     *            the row to access. Must be within current size
     * @param attributeIndex
     *            the attribute index
     * @param value
     *            the timestamp value
     */
    public final void setTimestampValue(int row, int attributeIndex, long value) {
        dataColumns[attributeIndex].setTimestampValue(row, value);
    }

    /**
     * Set method for timestamp values in the row previously reserved by {@link #reserveRow()} in
     * parallel execution scenarios. This method is usable for all attributes that are of valuetype
     * TIMESTAMP. For performance reasons no checking is performed, so you need to make sure that
     * the given index is indeed pointing to a numerical attribute *
     * 
     * @param rowHandle
     *            the row handle returned previously by {@link #reserveRow()}
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */

    public final void setTimestampValueWithHandle(int rowHandle, int attributeIndex, long value) {
        dataColumns[attributeIndex].setTimestampValue(rowHandle, value);
    }

    /**
     * Set method for values of the current example. This method is usable for all attributes that
     * are of valuetype TIMESTAMP
     *
     * @param attributeName
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */
    @Deprecated
    public final void setValue(String attributeName, long value) {
        setTimestampValue(attributeIndexMap.get(attributeName), value);
    }

    /**
     * Set method for values of the current example. This method is usable for all attributes that
     * are of valuetype TIMESTAMP or subtypes.
     *
     * @param attributeName
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */
    @Deprecated
    public final void setValue(String attributeName, Instant value) {
        setTimestampValue(attributeIndexMap.get(attributeName), value.toEpochMilli());
    }

    /**
     * Set method for values of the current example. This method is usable for all attributes that
     * are of valuetype NOMINAL. boolean values are mapped to "true" and "false".
     *
     * @param attributeName
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired boolean value.
     */
    @Deprecated
    public final void setValue(String attributeName, boolean value) {
        setNominalValue(attributeIndexMap.get(attributeName), Boolean.toString(value));
    }

    /**
     * Set method for nominal values of the current row. This method is usable for all attributes
     * that are of valuetype NOMINAL. For performance reasons no checking is performed, so you need
     * to make sure that the given index is indeed pointing to a numerical attribute
     * 
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */
    public final void setNominalValue(int attributeIndex, String value) {
        dataColumns[attributeIndex].setNominalValue(currentRow, value);
        lastNominalValues[attributeIndex] = value;
    }


    /**
     * This method allows random writes into the data columns AFTER {@link #setSize(int)} was
     * called!<br>
     * If you access a row beyond the currently set size, it will result into a access violation and
     * kill the program. <br>
     * This is intend as a very low level access for certain, non standard operations. If not
     * absolutely necessary, please resolve to the other methods, which are more secure and result
     * in less complex code.
     * 
     * @param row
     *            the row to access. Must be within current size
     * @param attributeIndex
     *            the attribute index
     * @param value
     *            the nominal value
     */
    public final void setNominalValue(int row, int attributeIndex, String value) {
        dataColumns[attributeIndex].setNominalValue(row, value);
    }

    /**
     * Set method for nominal values in the row previously reserved by {@link #reserveRow()} in
     * parallel execution scenarios. This method is usable for all attributes that are of valuetype
     * NOMINAL. For performance reasons no checking is performed, so you need to make sure that the
     * given index is indeed pointing to a numerical attribute
     * 
     * @param rowHandle
     *            the row handle returned previously by {@link #reserveRow()}
     * 
     * @param attributeIndex
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired numerical value.
     */
    public final void setNominalValueWithHandle(int rowHandle, int attributeIndex, String value) {
        dataColumns[attributeIndex].setNominalValue(rowHandle, value);
    }


    /**
     * Set method for values of the current example. This method is usable for all attributes that
     * are of valuetype Ontology.NOMINAL. The mapping is automatically created.
     *
     * @param attributeName
     *            The name of the attribute as specified in constructor
     * @param value
     *            The desired nominal value as string.
     */
    @Deprecated
    public final void setValue(String attributeName, String value) {
        setNominalValue(attributeIndexMap.get(attributeName), value);
    }

    /**
     * This sets the given attribute as missing value.
     *
     * @param attributeName
     */
    @Deprecated
    public final void setMissing(String attributeName) {
        setMissing(attributeIndexMap.get(attributeName));
    }

    /**
     * This method copies all values from the given example to this currently created row.
     * Additional values may be set separately.
     *
     * @param example
     */
    public void setValuesByExample(Example example) {
        Iterator<Attribute> attributesIterator = example.getAttributes().allAttributes();
        Attribute attribute;
        while (attributesIterator.hasNext()) {
            attribute = attributesIterator.next();
            switch (attribute.getValueType()) {
                case NOMINAL -> setValue(attribute.getName(), example.getNominalValue(attribute));
                case NUMERIC -> setValue(attribute.getName(), example.getNumericValue(attribute));
                case TIMESTAMP -> setValue(attribute.getName(), example.getTimestampValue(attribute));
            }
        }
    }

    /**
     * This method copies all examples from the given exampleSet into the new created exampleSet.
     *
     * @param exampleSet
     */
    public void setRowsByExampleSet(ExampleSet exampleSet) {
        Attributes attributes = exampleSet.getAttributes();
        ValueType[] valueTypes = attributes.streamAttributes().map(Attribute::getValueType).toArray(ValueType[]::new);
        int[] externalIndexes = attributes.streamAttributes().mapToInt(Attribute::getIndex).toArray();
        int[] internalIndexes = attributes.streamAttributes().map(Attribute::getName).mapToInt(attributeName -> attributeIndexMap.get(attributeName)).toArray();

        for (int row = 0; row < exampleSet.size(); row++) {
            for (int column = 0; column < internalIndexes.length; column++) {
                switch (valueTypes[column]) {
                    case NOMINAL -> setNominalValue(internalIndexes[column], exampleSet.getNominalValue(row, externalIndexes[column]));
                    case NUMERIC -> setNumericValue(internalIndexes[column], exampleSet.getNumericValue(row, externalIndexes[column]));
                    case TIMESTAMP -> setTimestampValue(internalIndexes[column], exampleSet.getTimestampValue(row, externalIndexes[column]));
                }
            }
            commit();
        }
    }

    /**
     * This method moves the current edit cursor to a previous row. It will jump back once commit is
     * called. May only be called once before committing.
     *
     * @param index
     *            the row to access, must be smaller than size.
     */
    public void reopen(int index) {
        if (index >= size)
            throw new IllegalArgumentException("Row index out of range: " + index + " of " + size + " rows");
        if (isReopened)
            throw new IllegalStateException("Already reopened a row");
        markedRow = currentRow;
        currentRow = index;
        isReopened = true;
    }

    /**
     * This returns the number of rows added to the currently edited or the explicitly set number
     * this will later return.
     *
     * @return the size
     */
    public int size() {
        return size;
    }

    /**
     * Setting the size will make this example set creator create this number of rows. Committing
     * more rows will thrown an error. This method is only thought to be used when the exact number
     * of rows is known in advance to allow a more efficient memory allocation. Take care to set the
     * initial capacity during construction on the same value to avoid unnecessary memory
     * allocation.
     * 
     * @param size
     *            the final number of rows
     * @return this for chaining.
     */
    public ExampleSetCreator setSize(int size) {
        if (finalSize > 0 && finalSize != size)
            throw new IllegalStateException("Size was already set. Cannot change afterwards.");
        this.finalSize = size;
        this.size = size;
        if (size > capacity) {
            this.capacity = size;
            for (DataColumn dataColumn : dataColumns)
                dataColumn.resize(capacity);
        }
        return this;
    }

    /**
     * This method allows parallel filling of ExampleSets from indeterminate large sources. A thread
     * can reserve a row once it knows it has to write a new row and get's a handle returned. With
     * the setValueByHandle methods, the thread can then fill this row, while other threads can
     * already fill their rows. Only row reservation is synchronized.
     * <p>
     * You need to release rows again using the {@link #releaseRow(int)} method
     * 
     * @return the handle on the reserved row
     */
    public synchronized int reserveRow() {
        phaser.register();
        int reservedRowHandle = currentRow;
        // check if we are in fixed length mode
        if (finalSize < 0) {
            // increment current row and supply memory if not in fixed length mode;
            currentRow++;
            if (currentRow > size)
                size = currentRow;

            if (currentRow >= capacity) {
                // we first have to wait until all threads finished their reserved handles
                phaser.arriveAndAwaitAdvance();

                // now we can safely increment capacity
                capacity += capacityIncrement;
                capacityIncrement = Math.min(MAX_CAPACITY_INCREMENT, capacityIncrement * 2);
                // then we need to resize columns
                for (DataColumn dataColumn : dataColumns)
                    dataColumn.resize(capacity);
            }
        } else {
            // if row exceeds memory switch to ring buffer mode
            currentRow = (currentRow + 1) % finalSize;
        }
        return reservedRowHandle;
    }

    /**
     * This method allows to release a previously reserved row. Callers of {@link #reserveRow()}
     * need to call this after they don't access the row anymore. This is to prevent the memory base
     * being changed while other threads are still writing.
     * 
     */
    public void releaseRow(int rowHandle) {
        phaser.arriveAndDeregister();
    }

    /**
     * This method finishes the currently created row and adds it to the example set. Afterwards the
     * values of the next row can be edited. Only rows that have been committed will be part of the
     * final example set. <br>
     * If a final size has been set and one would write into the row beyond the size, it will start
     * writing in the very first row again, effectively creating a ring buffer.
     */
    public void commit() {
        if (!isReopened) {
            // if no final size has been set, we increment memory if necessary. Otherwise the memory
            // is already allocated.
            if (finalSize < 0) {
                currentRow++;
                if (currentRow > size)
                    size = currentRow;
                if (currentRow >= capacity) {
                    capacity += capacityIncrement;
                    capacityIncrement = Math.min(MAX_CAPACITY_INCREMENT, capacityIncrement * 2);
                    // then we need to resize columns
                    for (DataColumn dataColumn : dataColumns)
                        dataColumn.resize(capacity);
                }
            } else {
                currentRow = (currentRow + 1) % finalSize;
            }
        } else {
            isReopened = false;
            currentRow = markedRow;
        }
    }

    /**
     * This method finishes the currently created row and adds it to the example set. Afterwards the
     * values of the next row can be edited. The next row will be initialized with values of the
     * former row. Only rows that have been committed will be part of the final example set.
     * 
     * 
     */
    public void commitAndKeep() {
        commit();
        // then we need to copy content
        for (int i = 0; i < valueTypes.length; i++) {
            DataColumn dataColumn = dataColumns[i];
            switch (valueTypes[i]) {
                case NOMINAL:
                    dataColumn.setNominalValue(currentRow, lastNominalValues[i]);
                    break;
                case NUMERIC:
                    dataColumn.setNumericValue(currentRow, lastNumericValues[i]);
                    break;
                case TIMESTAMP:
                    dataColumn.setTimestampValue(currentRow, lastTimestampValues[i]);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * This method resets the entire row to missing values
     */
    public void resetRow() {
        for (int i = 0; i < valueTypes.length; i++) {
            DataColumn dataColumn = dataColumns[i];
            switch (valueTypes[i]) {
                case NOMINAL:
                    dataColumn.setNominalValue(currentRow, ValueType.MISSING_NOMINAL);
                    break;
                case NUMERIC:
                    dataColumn.setNumericValue(currentRow, ValueType.MISSING_NUMERIC);
                    break;
                case TIMESTAMP:
                    dataColumn.setTimestampValue(currentRow, ValueType.MISSING_TIMESTAMP);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * This method resets a single entry in the given row and in the last values array to missing
     * value
     */
    @Deprecated
    public void resetCurrentAndRememberedValue(String attributeName) {
        setMissing(attributeIndexMap.get(attributeName));
        switch (valueTypes[attributeIndexMap.get(attributeName)]) {
            case NOMINAL:
                lastNominalValues[attributeIndexMap.get(attributeName)] = ValueType.MISSING_NOMINAL;
                break;
            case NUMERIC:
                lastNumericValues[attributeIndexMap.get(attributeName)] = ValueType.MISSING_NUMERIC;
                break;
            case TIMESTAMP:
                lastTimestampValues[attributeIndexMap.get(attributeName)] = ValueType.MISSING_TIMESTAMP;
                break;
        }
    }

    /**
     * This method resets a single entry to missing value
     */
    public void setMissing(int attributeIndex) {
        DataColumn dataColumn = dataColumns[attributeIndex];
        switch (valueTypes[attributeIndex]) {
            case NOMINAL:
                dataColumn.setNominalValue(currentRow, ValueType.MISSING_NOMINAL);
                break;
            case NUMERIC:
                dataColumn.setNumericValue(currentRow, ValueType.MISSING_NUMERIC);
                break;
            case TIMESTAMP:
                dataColumn.setTimestampValue(currentRow, ValueType.MISSING_TIMESTAMP);
                break;
            default:
                break;
        }
    }

    /**
     * This method resets a single entry to missing value in the row previously reserved by
     * {@link #reserveRow()} in parallel execution scenarios.
     */
    public void setMissingWithHandle(int rowHandle, int attributeIndex) {
        DataColumn dataColumn = dataColumns[attributeIndex];
        switch (valueTypes[attributeIndex]) {
            case NOMINAL:
                dataColumn.setNominalValue(rowHandle, ValueType.MISSING_NOMINAL);
                break;
            case NUMERIC:
                dataColumn.setNumericValue(rowHandle, ValueType.MISSING_NUMERIC);
                break;
            case TIMESTAMP:
                dataColumn.setTimestampValue(rowHandle, ValueType.MISSING_TIMESTAMP);
                break;
            default:
                break;
        }
    }

    /**
     * This method returns the final set with all attributes and the examples that have been
     * committed.
     *
     * @return The final exampleset
     */
    public ExampleSet finish() {
        // fill content provider
        if (!columnFillers.isEmpty()) {
            IntConsumer[] columnFillersArray = columnFillers.toArray(IntConsumer[]::new);
            IntStream.range(0, size).parallel().forEach(row -> {
                for (IntConsumer intConsumer : columnFillersArray)
                    intConsumer.accept(row);
            });
        }

        // finalize data columns to switch them to read-only mode
        this.capacity = size;
        Arrays.stream(dataColumns).parallel().forEach(column -> column.finishWriting(size));

        return new ExampleSet(size, attributeNames, valueTypes, dataColumns, attributeRoles, attributeIsSpecial);
    }

    /**
     * This resets the creator and prepares it to be used again. No functions will be attached to
     * any attribute.
     */
    public void reset() {
        init(attributeNames, valueTypes, attributeRoles, attributeIsSpecial, initialCapacity);
    }

    /**
     * s This method returns the final set with all attributes and the examples that have been
     * committed. It then restarts the builder and prepares it to be used again. The result will not
     * have any functions attached.
     * 
     * @return The final exampleset
     */
    public ExampleSet finishAndReset() {
        ExampleSet set = finish();
        reset();
        return set;
    }

    /**
     * This returns the ExampleSetMeta Data of the created example set.
     *
     * @return
     */
    public ExampleSetMetaData getMetaData() {
        if (this.metaData == null)
            this.metaData = ExampleSetMetaData.of(attributeNames, valueTypes, attributeRoles);
        return metaData;
    }
}
