package com.owc.singularity.engine.object.data.exampleset.transformer;

import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;


public abstract class AbstractRowTransformation implements ExampleSetTransformation {

    protected final String newAttributeName;
    private ValueType valueType;


    public AbstractRowTransformation(String newAttributeName, ValueType valueType) {
        this.newAttributeName = newAttributeName;
        this.valueType = valueType;
    }

    @Override
    public Stream<ColumnDescription> apply(ExampleSet sourceSet) {
        DataColumn dataColumn = new DataColumn(sourceSet.size(), valueType);
        apply(dataColumn, sourceSet);
        dataColumn.finishWriting(sourceSet.size());
        return Stream.of(new ColumnDescription(newAttributeName, valueType, dataColumn));
    }

    protected abstract void apply(DataColumn dataColumn, ExampleSet sourceSet);
}
