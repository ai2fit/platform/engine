package com.owc.singularity.engine.object.data.exampleset;

import java.util.LinkedHashMap;
import java.util.function.*;

import com.owc.singularity.engine.object.data.exampleset.transformer.MultipleComputingRowTransformation;
import com.owc.singularity.engine.operator.error.OperatorException;

public class MultipleComputingRowTransformationConfiguration<C> {

    private final ExampleSetTransformer transformer;
    private final String[] sourceAttributeNames;
    private final BiConsumer<MultipleComputingRowTransformation.RedirectableRowAccessor, C> computationFunction;
    private Supplier<C> resultSupplier;
    private final LinkedHashMap<String, ToDoubleFunction<C>> numericDerivesMap;
    private final LinkedHashMap<String, Function<C, String>> nominalDerivesMap;
    private final LinkedHashMap<String, ToLongFunction<C>> timestampDerivesMap;


    MultipleComputingRowTransformationConfiguration(ExampleSetTransformer transformer, String[] sourceAttributeNames, Supplier<C> resultSupplier,
            BiConsumer<MultipleComputingRowTransformation.RedirectableRowAccessor, C> computationFunction) {
        this.transformer = transformer;
        this.sourceAttributeNames = sourceAttributeNames;
        this.resultSupplier = resultSupplier;
        this.computationFunction = computationFunction;
        this.numericDerivesMap = new LinkedHashMap<>();
        this.nominalDerivesMap = new LinkedHashMap<>();
        this.timestampDerivesMap = new LinkedHashMap<>();
    }

    public MultipleComputingRowTransformationConfiguration<C> withCalculatedNumeric(String targetName, ToDoubleFunction<C> numericDerive) {
        numericDerivesMap.put(targetName, numericDerive);
        return this;
    }

    public MultipleComputingRowTransformationConfiguration<C> withCalculatedNominal(String targetName, Function<C, String> nominalDerive) {
        nominalDerivesMap.put(targetName, nominalDerive);
        return this;
    }

    public MultipleComputingRowTransformationConfiguration<C> withCalculatedTimestamp(String targetName, ToLongFunction<C> timestampDerive) {
        timestampDerivesMap.put(targetName, timestampDerive);
        return this;
    }

    @SuppressWarnings("unchecked")
    public ExampleSetTransformer finish() throws OperatorException {
        String[] numericTargetNames = numericDerivesMap.keySet().toArray(String[]::new);
        String[] nominalTargetNames = nominalDerivesMap.keySet().toArray(String[]::new);
        String[] timestampTargetNames = timestampDerivesMap.keySet().toArray(String[]::new);


        ToDoubleFunction<C>[] numericDerives = new ToDoubleFunction[numericTargetNames.length];
        for (int i = 0; i < numericTargetNames.length; i++) {
            numericDerives[i] = numericDerivesMap.get(numericTargetNames[i]);
        }

        Function<C, String>[] nominalDerives = new Function[nominalTargetNames.length];
        for (int i = 0; i < nominalTargetNames.length; i++) {
            nominalDerives[i] = nominalDerivesMap.get(nominalTargetNames[i]);
        }

        ToLongFunction<C>[] timestampDerives = new ToLongFunction[timestampTargetNames.length];
        for (int i = 0; i < timestampTargetNames.length; i++) {
            timestampDerives[i] = timestampDerivesMap.get(timestampTargetNames[i]);
        }

        return transformer
                .withDerivation(
                        new MultipleComputingRowTransformation<>(numericTargetNames, nominalTargetNames, timestampTargetNames, sourceAttributeNames,
                                resultSupplier, computationFunction, numericDerives, nominalDerives, timestampDerives),
                        numericTargetNames, nominalTargetNames, timestampTargetNames);
    }
}
