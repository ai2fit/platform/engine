package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.Arrays;
import java.util.function.ToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class TimestampRowTransformation extends AbstractRowTransformation {

    /**
     * This accessor allows to access numeric values of a single row via it's {@link #get(int)}
     * method.
     * 
     * @author Sebastian Land
     *
     */
    public static class TimestampRowAccessor {

        int row;
        private int[] attributeIndices;
        private ExampleSet exampleSet;

        private TimestampRowAccessor(ExampleSet exampleSet, int[] attributeIndices) {
            this.exampleSet = exampleSet;
            this.attributeIndices = attributeIndices;
        }

        public long get(int attributeNumber) {
            return exampleSet.getTimestampValue(row, attributeIndices[attributeNumber]);
        }

        private void setRow(int row) {
            this.row = row;
        }
    }

    private ToLongFunction<TimestampRowAccessor> derive;
    private String[] numericSourceAttributeNames;


    public TimestampRowTransformation(String derivedName, String[] numericSourceAttributeNames, ToLongFunction<TimestampRowAccessor> derive) {
        super(derivedName, ValueType.NUMERIC);
        this.numericSourceAttributeNames = numericSourceAttributeNames;
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        int[] sourceAttributeIndices = Arrays.stream(numericSourceAttributeNames).mapToInt(name -> sourceSet.getAttributes().get(name).getIndex()).toArray();
        ThreadLocal<TimestampRowAccessor> localAccessor = new ThreadLocal<>() {

            @Override
            protected TimestampRowAccessor initialValue() {
                return new TimestampRowAccessor(sourceSet, sourceAttributeIndices);
            }
        };
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            TimestampRowAccessor accessor = localAccessor.get();
            accessor.setRow(row);
            dataColumn.setTimestampValue(row, derive.applyAsLong(accessor));
        });
    }

}
