/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;

import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This is a mapping from an int to a String and reverse. It is used in {@link DataColumn} of
 * nominal type. As only writing can change the mapping, it offers a thread-safe implementation of
 * the get and map method.
 *
 * @author Ingo Mierswa
 */
final class NominalMapping {


    private String[] valueByIndex = null;
    private final ConcurrentHashMap<String, Integer> indexByValue = new ConcurrentHashMap<>();
    private AtomicInteger valueCounter = new AtomicInteger(-1);

    String mapIndex(int index) {
        // this way we don't need a branch here and still return null
        return valueByIndex[index + 1];
    }

    int mapString(String value) {
        if (value == null)
            return -1;
        Integer indexObject = indexByValue.get(value);
        if (indexObject == null) {
            // the following method is much slower than the simple get, hence we only apply it when
            // atomic operation is necessary
            return indexByValue.computeIfAbsent(value, key -> valueCounter.incrementAndGet());
        }
        return indexObject;
    }

    /**
     * This method will add the values and will update the valueByIndex array. As this method is
     * thought in situations where new mappings will be added over time, the array will be
     * overprovisioned to allow more efficient growing.<br/>
     * It is assumed that the values are new and checks will only be performed in
     * {@link DataColumn}.DEVELOPMENT_MODE = true. Please be aware, that this cannot be used
     * together with the regular {@link #mapString(String)} method
     * 
     * @param values
     */
    void mapStringsAndUpdate(String[] values) {
        // possibly perform check in dev mode
        if (DataColumn.DEVELOPMENT_MODE) {
            for (int i = 0; i < values.length; i++)
                if (indexByValue.contains(values[i]))
                    throw new IllegalStateException("Nominal Mapping already contains value " + values[i]);
        }

        // update index
        int oldSize = indexByValue.size();
        for (int i = 0; i < values.length; i++) {
            indexByValue.put(values[i], valueCounter.incrementAndGet());
        }

        // initializing or growing array: Exchange should not cause crash as parallel reads might
        // access old or new array regardless.
        if (valueByIndex == null) {
            valueByIndex = new String[values.length * 2 + 8];
        } else if (valueByIndex.length < indexByValue.size() + 1) {
            String[] newIndex = new String[indexByValue.size() + 2 * values.length + indexByValue.size() >> 4 + 8];
            System.arraycopy(valueByIndex, 0, newIndex, 0, oldSize + 1);
            valueByIndex = newIndex;
        }
        // adding new values
        System.arraycopy(values, 0, valueByIndex, oldSize + 1, values.length);

    }

    void finishWriting() {
        valueByIndex = new String[indexByValue.size() + 1];
        for (Entry<String, Integer> entry : indexByValue.entrySet()) {
            valueByIndex[entry.getValue() + 1] = entry.getKey();
        }
    }

    int size() {
        return indexByValue.size();
    }

    String[] values() {
        return valueByIndex;
    }
}
