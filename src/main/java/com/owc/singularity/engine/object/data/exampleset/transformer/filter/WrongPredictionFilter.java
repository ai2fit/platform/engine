/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.operator.error.UserError;


/**
 * This subclass of {@link ExampleSetRowFilter} serves to accept all examples which are wrongly
 * predicted.
 * 
 * @author Ingo Mierswa ingomierswa Exp $
 */
public class WrongPredictionFilter implements ExampleSetRowFilter {

    private static final long serialVersionUID = -2971139314612252923L;
    private transient Attribute labelAttribute;
    private transient Attribute predictedLabelAttribute;


    /** Returns true if the example is correctly predicted. */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) {
        return switch (labelAttribute.getValueType()) {
            case NOMINAL -> {
                String nominalValue = exampleSet.getNominalValue(row, labelAttribute.getIndex());
                yield nominalValue == null || !nominalValue.equals(exampleSet.getNominalValue(row, predictedLabelAttribute.getIndex()));
            }
            case NUMERIC -> exampleSet.getNumericValue(row, labelAttribute.getIndex()) != exampleSet.getNumericValue(row, predictedLabelAttribute.getIndex());
            case TIMESTAMP -> exampleSet.getTimestampValue(row, labelAttribute.getIndex()) != exampleSet.getTimestampValue(row,
                    predictedLabelAttribute.getIndex());
        };
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        labelAttribute = exampleSet.getAttributes().getLabel();
        if (labelAttribute == null) {
            throw new UserError(null, 105);
        }
        predictedLabelAttribute = exampleSet.getAttributes().getPredictedLabel();
        if (predictedLabelAttribute == null) {
            throw new UserError(null, 107);
        }

    }
}
