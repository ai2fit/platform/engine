package com.owc.singularity.engine.object.data.exampleset.transformer;

import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

public abstract class AbstractGenerationTransformation implements ExampleSetTransformation {

    private final String attributeName;
    private ValueType valueType;


    protected AbstractGenerationTransformation(String attributeName, ValueType valueType) {
        this.attributeName = attributeName;
        this.valueType = valueType;
    }

    @Override
    public Stream<ColumnDescription> apply(ExampleSet sourceSet) {
        DataColumn dataColumn = new DataColumn(sourceSet.size(), valueType);
        apply(dataColumn, sourceSet.size());
        dataColumn.finishWriting(sourceSet.size());
        return Stream.of(new ColumnDescription(attributeName, valueType, dataColumn));
    }

    protected abstract void apply(DataColumn dataColumn, int size);
}
