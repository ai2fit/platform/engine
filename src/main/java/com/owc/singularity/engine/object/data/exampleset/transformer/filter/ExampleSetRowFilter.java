/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetTransformer;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.operator.tools.ExpressionEvaluationException;


/**
 * Filter implementation for {@link ExampleSetTransformer#filter} method.
 * 
 * @author Sebastian Land
 */
public interface ExampleSetRowFilter extends Serializable {

    /** Array of short names for the known conditions. */
    public static final String[] KNOWN_CONDITION_NAMES = { "all", "attribute_value_filter", "correct_predictions", "custom_filters", "expression",
            "missing_attributes", "missing_labels", "no_missing_attributes", "no_missing_labels", "wrong_predictions" };

    public static final int CONDITION_ALL = 0;
    public static final int CONDITION_ATTRIBUTE_VALUE_FILTER = 1;
    public static final int CONDITION_CORRECT_PREDICTIONS = 2;
    public static final int CONDITION_CUSTOM_FILTER = 3;
    public static final int CONDITION_EXPRESSION = 4;
    public static final int CONDITION_MISSING_ATTRIBUTES = 5;
    public static final int CONDITION_MISSING_LABELS = 6;
    public static final int CONDITION_NO_MISSING_ATTRIBUTES = 7;
    public static final int CONDITION_NO_MISSING_LABELS = 8;
    public static final int CONDITION_WRONG_PREDICTIONS = 9;

    /**
     * Array of fully qualified classnames of implementations of {@link ExampleSetRowFilter} that
     * are useful independently of special applications. All conditions given here must provide a
     * construtor with arguments (ExampleSet data, String parameters).
     */
    public static final String[] KNOWN_CONDITION_FILTERS = { AcceptAllFilter.class.getName(), AttributeValueFilter.class.getName(),
            CorrectPredictionFilter.class.getName(), CustomFilter.class.getName(), ExpressionFilter.class.getName(), MissingValuesFilter.class.getName(),
            MissingLabelsFilter.class.getName(), NoMissingValuesFilter.class.getName(), NoMissingLabelsFilter.class.getName(),
            WrongPredictionFilter.class.getName() };

    /**
     * Checks if the given name is the short name of a known condition and creates it. If the name
     * is not known, this method creates a new instance of className which must be an implementation
     * of {@link ExampleSetRowFilter} by calling its two argument constructor passing it the example
     * set and the parameter string
     * 
     * @throws OperatorException
     *             thrown in case of instanciation error
     */
    public static ExampleSetRowFilter createCondition(String name, Object... args) throws OperatorException {
        String className = null;
        for (int i = 0; i < KNOWN_CONDITION_NAMES.length; i++) {
            if (KNOWN_CONDITION_NAMES[i].equals(name)) {
                className = KNOWN_CONDITION_FILTERS[i];
                break;
            }
        }
        if (className == null)
            throw new OperatorException("No such row filter known: " + name);
        try {
            Class<?> clazz = ExampleSetRowFilter.class.getClassLoader().loadClass(className);
            Constructor<?>[] constructors = clazz.getConstructors();
            for (Constructor<?> constructor : constructors) {
                Class<?>[] parameterTypes = constructor.getParameterTypes();
                if (parameterTypes.length == args.length) {
                    int i = 0;
                    boolean isCompatible = true;
                    for (Class<?> parameterType : parameterTypes) {
                        isCompatible &= parameterType.isAssignableFrom(args[i].getClass());
                        i++;
                    }
                    if (isCompatible)
                        return (ExampleSetRowFilter) constructor.newInstance(args);
                }
            }
            throw new OperatorException("'" + className + "' does not provide compatible constructor.");
        } catch (ClassNotFoundException e) {
            throw new OperatorException("Cannot find class '" + className + "'. Check your classpath.", e);
        } catch (IllegalAccessException e) {
            throw new OperatorException("'" + className + "' cannot access two argument constructor " + className + "(ExampleSet, String)!", e);
        } catch (InstantiationException e) {
            throw new OperatorException(className + ": cannot create condition (" + e.getMessage() + ").", e);
        } catch (IllegalArgumentException e) {
            throw new OperatorException("'" + className + "' does not provide compatible constructor.", e);
        } catch (InvocationTargetException e) {
            if (e.getTargetException() instanceof OperatorException) {
                throw (OperatorException) e.getTargetException();
            }
            throw new OperatorException("'" + className + "' does not provide allowed constructor.", e);
        }
    }

    /**
     * This method is called before the filter is used. The implementation can use the example set
     * to resolve any attributes from given strings.
     * 
     * @param exampleSet
     *            the example set to be filtered
     * @throws UserError
     */
    public void prepare(ExampleSet exampleSet) throws UserError;

    /**
     * Should return true if the given row passes the filter.
     * 
     * @param row
     *            the row
     * @return true if the row is to be kept in the result.
     * @throws ExpressionEvaluationException
     *             if the condition cannot be evaluated
     */
    public boolean pass(int row, ExampleSet exampleSet) throws ExpressionEvaluationException;


}
