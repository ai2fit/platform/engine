package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.IntFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractGenerationTransformation;

public final class NominalGenerationTransformation extends AbstractGenerationTransformation {

    private final IntFunction<String> function;

    public NominalGenerationTransformation(String attributeName, IntFunction<String> function) {
        super(attributeName, ValueType.NOMINAL);
        this.function = function;
    }

    @Override
    protected void apply(DataColumn dataColumn, int size) {
        IntStream.range(0, size).parallel().forEach(row -> dataColumn.setNominalValue(row, function.apply(row)));
    }
}
