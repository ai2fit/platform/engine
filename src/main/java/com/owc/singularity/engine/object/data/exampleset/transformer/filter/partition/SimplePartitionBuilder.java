/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter.partition;

import java.util.Arrays;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;

/**
 * Creates a simple non-shuffled partition for an {@link ExampleSet}.
 * 
 * @author Ingo Mierswa ingomierswa Exp $
 */
public class SimplePartitionBuilder implements PartitionBuilder {

    /** Returns a simple non-shuffled partition for an example set. */
    @Override
    public int[] createPartition(double[] ratio, int size) {
        // determine partition starts
        int[] partitionStartIndices = new int[ratio.length + 1];
        partitionStartIndices[0] = 0;
        double ratioSum = 0;
        for (int i = 1; i < partitionStartIndices.length; i++) {
            ratioSum += ratio[i - 1];
            partitionStartIndices[i] = (int) Math.round(size * ratioSum);
        }

        // create a simple partition
        int p = 0;
        int[] rowAssignments = new int[size];
        for (int row = 0; row < rowAssignments.length; row++) {
            if (row >= partitionStartIndices[p + 1]) {
                p++;
                if (p == ratio.length) {
                    Arrays.fill(rowAssignments, row, size, Partition.DROP);
                    return rowAssignments;
                }
            }
            rowAssignments[row] = p;
        }

        return rowAssignments;
    }
}
