package com.owc.singularity.engine.object.data.exampleset.transformer.constants;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

public class NumericConstantCreation extends ConstantCreation {

    public double value;

    public NumericConstantCreation(String name, double value) {
        super(name);
        this.value = value;
    }

    @Override
    public ColumnDescription apply() {
        DataColumn column = new DataColumn(1, ValueType.NUMERIC);
        column.setNumericValue(0, value);
        column.finishWriting(1);
        return new ColumnDescription(getName(), ValueType.NUMERIC, column);
    }
}
