/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;


/**
 * Provides some tools for calculation of certain measures and feature generation.
 *
 * @author Simon Fischer, Ingo Mierswa
 */
public class ExampleSetChecks {

    /**
     * The data set is not allowed to contain missing values. If it does, a {@link UserError} is
     * thrown. Special attributes will be ignored, except they are explicitly listed at
     * specialAttributes. Furthermore, if a specified special attribute does not exist a
     * {@link UserError} is also thrown!
     *
     * @param exampleSet
     *            the {@link ExampleSet} to check
     * @param task
     *            will be shown as the origin of the error
     * @param operator
     *            the offending operator. Can be <code>null</code>
     * @param specialAttributes
     *            contains the special attributes which have to be checked. If a listed attribute
     *            does not exist or contains missing values, a {@link UserError} is thrown
     **/
    public static void onlyNonMissingValues(ExampleSet exampleSet, String task, Operator operator, String... specialAttributes) throws OperatorException {
        Set<String> specialAttributesToCheck = Arrays.stream(specialAttributes).collect(Collectors.toSet());
        Optional<Attribute> attributeWithMissings = exampleSet.getAttributes()
                .streamAttributes()
                .parallel()
                .filter(a -> !a.isSpecial() || specialAttributesToCheck.contains(a.getName()))
                .filter(a -> switch (a.getValueType()) {
                case NOMINAL -> exampleSet.streamNominalAttribute(a).anyMatch(ValueType::isMissing);
                case NUMERIC -> exampleSet.streamNumericAttribute(a).anyMatch(ValueType::isMissing);
                case TIMESTAMP -> exampleSet.streamTimestampAttribute(a).anyMatch(ValueType::isMissing);
                })
                .findAny();
        if (attributeWithMissings.isPresent())
            throw new UserError(operator, 139, task);
    }

    /**
     * The attributes all have to be numerical.
     *
     * @param exampleSet
     *            the example set
     * @throws UserError
     */
    public static void onlyNumericalAttributes(ExampleSet exampleSet, String task) throws UserError {
        onlyNumericalAttributes(exampleSet.getAttributes(), task);
    }

    /**
     * The attributes all have to be numerical.
     *
     * @param attributes
     *            the attributes
     * @param task
     *            task name used for the error message
     * @throws UserError
     */
    public static void onlyNumericalAttributes(Attributes attributes, String task) throws UserError {
        for (Attribute attribute : attributes) {
            if (!attribute.getValueType().equals(ValueType.NUMERIC)) {
                throw new UserError(null, 104, task, attribute.getName());
            }
        }
    }

    /**
     * The attributes all have to be nominal or binary.
     *
     * @param exampleSet
     *            the example set
     * @param task
     *            task name used for the error message
     * @throws UserError
     */
    public static void onlyNominalAttributes(ExampleSet exampleSet, String task) throws UserError {
        onlyNominalAttributes(exampleSet.getAttributes(), task);
    }

    /**
     * The attributes all have to be nominal or binary.
     *
     * @param attributes
     *            the attributes
     * @param task
     *            task name used for the error message
     * @throws UserError
     */
    public static void onlyNominalAttributes(Attributes attributes, String task) throws UserError {
        for (Attribute attribute : attributes) {
            if (!attribute.getValueType().equals(ValueType.NOMINAL)) {
                throw new UserError(null, 103, task, attribute.getName());
            }
        }
    }

    /**
     * The example set has to contain labels.
     *
     * @param exampleSet
     *            the example set
     * @throws UserError
     */
    public static void isLabelled(ExampleSet exampleSet) throws UserError {
        if (exampleSet.getAttributes().getLabel() == null) {
            throw new UserError(null, 105);
        }
    }

    /**
     * The example set has to be tagged with ids.
     *
     * @param exampleSet
     *            the example set
     * @throws UserError
     */
    public static void hasIds(ExampleSet exampleSet) throws UserError {
        if (exampleSet.getAttributes().getId() == null) {
            throw new UserError(null, 129);
        }
    }


    /**
     * The example set has to have nominal labels. Generalized form allows for better use with other
     * algorithms than clustering.
     *
     * @param exampleSet
     *            the example set
     * @param algorithm
     *            the name of the algorithm
     * @throws UserError
     */
    public static void hasNominalLabels(ExampleSet exampleSet, String algorithm) throws UserError {
        isLabelled(exampleSet);
        Attribute a = exampleSet.getAttributes().getLabel();
        if (!a.getValueType().equals(ValueType.NOMINAL)) {
            throw new UserError(null, 101, algorithm, a.getName());
        }
    }

    public static void hasNumericLabel(ExampleSet exampleSet, String algorithm) throws UserError {
        isLabelled(exampleSet);
        Attribute a = exampleSet.getAttributes().getLabel();
        if (!a.getValueType().equals(ValueType.NUMERIC)) {
            throw new UserError(null, 102, algorithm, a.getName());
        }
    }

    /**
     * The example set has to contain at least one example.
     *
     * @param exampleSet
     *            the example set
     * @throws UserError
     */
    public static void isNonEmpty(ExampleSet exampleSet) throws UserError {
        if (exampleSet.size() == 0) {
            throw new UserError(null, 117);
        }
    }

    /**
     * The example set has to contain at least one regular attribute.
     *
     * @param exampleSet
     *            the example set
     * @throws UserError
     * @since 7.6
     */
    public static void hasRegularAttributes(ExampleSet exampleSet) throws UserError {
        if (exampleSet.getAttributes().size() == 0) {
            throw new UserError(null, 106);
        }
    }

    public static void hasNoMissingValues(ExampleSet exampleSet, String algorithm) throws UserError {
        int[] attributeIndexes = exampleSet.getAttributes().streamAttributes().mapToInt(Attribute::getIndex).toArray();
        ValueType[] attributeValueTypes = exampleSet.getAttributes().streamAttributes().map(Attribute::getValueType).toArray(ValueType[]::new);

        for (int row = 0; row < exampleSet.size(); row++) {
            for (int i = 0; i < attributeIndexes.length; i++) {
                switch (attributeValueTypes[i]) {
                    case NOMINAL -> {
                        if (ValueType.isMissing(exampleSet.getNominalValue(row, attributeIndexes[i]))) {
                            throw new UserError(null, 139, algorithm);
                        }
                    }
                    case NUMERIC -> {
                        if (ValueType.isMissing(exampleSet.getNumericValue(row, attributeIndexes[i]))) {
                            throw new UserError(null, 139, algorithm);
                        }
                    }
                    case TIMESTAMP -> {
                        if (ValueType.isMissing(exampleSet.getTimestampValue(row, attributeIndexes[i]))) {
                            throw new UserError(null, 139, algorithm);
                        }
                    }
                }
            }
        }
    }
}
