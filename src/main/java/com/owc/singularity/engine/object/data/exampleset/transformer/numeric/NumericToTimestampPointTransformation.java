package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.DoubleToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NumericToTimestampPointTransformation extends AbstractPointTransformation {

    private DoubleToLongFunction pointDerivation;

    public NumericToTimestampPointTransformation(String newAttributeName, String sourceAttributeName, DoubleToLongFunction pointDerivation) {
        super(newAttributeName, ValueType.TIMESTAMP, sourceAttributeName);
        this.pointDerivation = pointDerivation;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setTimestampValue(row, pointDerivation.applyAsLong(sourceSet.getNumericValue(row, attributeIndex))));

    }
}
