/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter.partition;


import java.io.Serializable;
import java.util.Arrays;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetTransformer;
import com.owc.singularity.engine.tools.logging.LogService;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;


/**
 * Implements a partition. A partition is used to divide an {@link ExampleSet} into different parts.
 * A partition can be used for {@link ExampleSetTransformer#filter} method. It does not have to
 * cover all examples.
 *
 * @author Simon Fischer, Ingo Mierswa
 */
public class Partition implements Cloneable, Serializable {

    /** Indicates a non-shuffled sampling for partition building. */
    public static final String[] SAMPLING_NAMES = { "linear sampling", "shuffled sampling", "stratified sampling", "automatic" };

    /** Indicates a non-shuffled sampling for partition building. */
    public static final int LINEAR_SAMPLING = 0;

    /** Indicates a shuffled sampling for partition building. */
    public static final int SHUFFLED_SAMPLING = 1;

    /** Indicates a stratified shuffled sampling for partition building. */
    public static final int STRATIFIED_SAMPLING = 2;

    /**
     * Indicates the usage of the automatic mode, where stratified sampling is used per default for
     * partition building. If it isn't applicable, shuffled sampling will be used instead.
     */
    public static final int AUTOMATIC = 3;

    private static final long serialVersionUID = 6126334515107973287L;

    private static final int UNUSED_INDEX = 0;
    private static final int UNUSED_OFFSET = 1;
    public static final int DROP = -1;


    /** Mask for the selected partitions. */
    private boolean[] isPartitionEnabled;

    /** Maps every example to its partition index. */
    private int[] rowPartitionAssignments;

    /**
     * Creates a new partition of a given size consisting of <tt>ratio.length</tt> sets. The set
     * <i>i</i> will be of size of <i>size x ratio[i]</i>, i.e. the sum of all <i>ratio[i]</i> must
     * be 1. Initially all partitions are selected.
     */
    public Partition(double[] ratio, int size, PartitionBuilder builder) {
        rowPartitionAssignments = builder.createPartition(ratio, size);
        isPartitionEnabled = new boolean[ratio.length + UNUSED_OFFSET];
    }

    /**
     * Creates a new partition of a given size consisting of <i>numberOfPartitions</i> equally sized
     * sets. Initially all partitions are selected.
     */
    public Partition(int numberOfPartitions, int size, PartitionBuilder builder) {
        double[] ratio = new double[numberOfPartitions];
        for (int i = 0; i < ratio.length; i++) {
            ratio[i] = 1 / (double) numberOfPartitions;
        }
        rowPartitionAssignments = builder.createPartition(ratio, size);
        this.isPartitionEnabled = new boolean[numberOfPartitions + UNUSED_OFFSET];
        Arrays.fill(isPartitionEnabled, true);
        isPartitionEnabled[UNUSED_INDEX] = false;
    }

    /**
     * Creates a partition from the given row assignment. Partition numbering starts at 0. No
     * partition is enabled.
     */
    public Partition(int[] rowPartitionAssignments, int numberOfPartitions) {
        this.rowPartitionAssignments = rowPartitionAssignments;
        this.isPartitionEnabled = new boolean[numberOfPartitions + UNUSED_OFFSET];
    }

    public Partition(int[] rowPartitionAssignments, int numberOfPartitions, int... selectedPartitions) {
        this.rowPartitionAssignments = rowPartitionAssignments;
        this.isPartitionEnabled = new boolean[numberOfPartitions + UNUSED_OFFSET];
        for (int i = 0; i < selectedPartitions.length; i++)
            isPartitionEnabled[selectedPartitions[i] + UNUSED_OFFSET] = true;
    }

    private Partition(Partition parent, boolean[] isPartitionEnabled) {
        this.rowPartitionAssignments = parent.rowPartitionAssignments;
        this.isPartitionEnabled = isPartitionEnabled;
    }

    public Partition getInvertedSelection() {
        boolean[] isPartitionEnabled = new boolean[this.isPartitionEnabled.length];
        for (int i = UNUSED_OFFSET; i < isPartitionEnabled.length; i++) {
            isPartitionEnabled[i] = !this.isPartitionEnabled[i];
        }
        return new Partition(this, isPartitionEnabled);
    };

    /** Marks only the given subset as selected. */
    public Partition getForSingleSubset(int i) {
        boolean[] isPartitionEnabled = new boolean[this.isPartitionEnabled.length];
        isPartitionEnabled[i + UNUSED_OFFSET] = true;
        return new Partition(this, isPartitionEnabled);
    }

    /** Returns the number of subsets. */
    public int getNumberOfPartitions() {
        return isPartitionEnabled.length - UNUSED_OFFSET;
    }


    /**
     * Returns true iff the example with the given index is selected according to the current
     * selection mask.
     */
    public boolean isSelected(int index) {
        return this.isPartitionEnabled[rowPartitionAssignments[index] + UNUSED_OFFSET];
    }


    /**
     * Works only for nominal attributes. If <i>k</i> is the number of different values, this method
     * splits the example set into <i>k</i> subsets according to the value of the given attribute.
     */
    public static Partition splitByAttribute(ExampleSet exampleSet, Attribute attribute) {
        int attributeIndex = attribute.getIndex();
        int[] elements = new int[exampleSet.size()];
        Object2IntOpenHashMap<String> indexMap = new Object2IntOpenHashMap<String>(100);
        indexMap.defaultReturnValue(-1);
        int currentIndex = 0;
        for (int row = 0; row < exampleSet.size(); row++) {
            String value = exampleSet.getNominalValue(row, attributeIndex);
            int index = indexMap.getInt(value);
            if (index == -1) {
                indexMap.put(value, currentIndex);
                elements[row] = currentIndex;
                currentIndex++;
            } else {
                elements[row] = index;
            }
        }

        return new Partition(elements, indexMap.size());
    }

    /**
     * Works only for real-value attributes. Returns an example set splitted into two parts
     * containing all examples providing a greater (smaller) value for the given attribute than the
     * given value. The first partition contains all examples providing a smaller or the same value
     * than the given one.
     */
    public static Partition splitByAttribute(ExampleSet exampleSet, Attribute attribute, double value) {
        final int[] elements = new int[exampleSet.size()];
        final int attributeIndex = attribute.getIndex();
        for (int row = 0; row < exampleSet.size(); row++) {
            double currentValue = exampleSet.getNumericValue(row, attributeIndex);
            if (currentValue <= value) {
                elements[row++] = 0;
            } else {
                elements[row++] = 1;
            }
        }
        return new Partition(elements, 2);
    }

    public static Partition splitByRandom(ExampleSet exampleSet, double[] ratios, int samplingType, boolean useLocalRandomSeed, int seed,
            boolean autoSwitchToShuffled) {
        PartitionBuilder builder = null;
        switch (samplingType) {
            case LINEAR_SAMPLING:
                builder = new SimplePartitionBuilder();
                break;
            case SHUFFLED_SAMPLING:
                builder = new ShuffledPartitionBuilder(useLocalRandomSeed, seed);
                break;
            case STRATIFIED_SAMPLING:
            case AUTOMATIC:
            default:
                Attribute label = exampleSet.getAttributes().getLabel();
                if (label != null && label.isNominal()) {
                    builder = new StratifiedPartitionBuilder(exampleSet, useLocalRandomSeed, seed);
                } else {
                    if ((autoSwitchToShuffled || samplingType == AUTOMATIC) && (label == null || !label.isNominal())) {
                        LogService.getRoot().warn("Example set has no nominal label: using shuffled partition instead of stratified partition");
                        builder = new ShuffledPartitionBuilder(useLocalRandomSeed, seed);
                        break;
                    }
                    builder = new ShuffledPartitionBuilder(useLocalRandomSeed, seed);
                }
                break;
        }
        return new Partition(builder.createPartition(ratios, exampleSet.size()), ratios.length);

    }

    public static Partition splitByRandom(ExampleSet set, int numberOfPartitions, int samplingType, boolean useLocalRandomSeed, int localRandomSeed,
            boolean autoSwitchToShuffled) {
        double[] ratio = new double[numberOfPartitions];
        for (int i = 0; i < ratio.length; i++) {
            ratio[i] = 1 / (double) numberOfPartitions;
        }
        return splitByRandom(set, ratio, samplingType, useLocalRandomSeed, localRandomSeed, autoSwitchToShuffled);
    }


}
