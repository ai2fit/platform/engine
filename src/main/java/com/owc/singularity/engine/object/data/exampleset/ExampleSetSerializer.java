/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.IntStream;

import com.owc.singularity.engine.tools.BiDirectionalIntStringMapping;
import com.owc.singularity.repository.serialization.DefinesSerializer;
import com.owc.singularity.repository.serialization.Serializer;


/**
 * Serializes and deserializes dense example sets to and from streams.
 * <p>
 * The format is as follows:
 * <ol>
 * <li>byte: Version id</li>
 * <li>3 bytes: Reserved</li>
 * <li>long: number of rows</li>
 * <li>int: number of columns</li>
 * <li>long: Length of ColumnDescriptions: used for calculating target offset when using direct
 * mapping of columns into memory</li>
 * </ol>
 * <h2>Column Descriptions Section</h2> Repeated as often as there are columns. Each Description
 * contains the following bytes:
 * <ol>
 * <li>int: byte length of name string in utf8</li>
 * <li>int: byte length of role string in utf8</li>
 * <li>byte: type (0=numeric, 1=nominal, 2=timestamp)</li>
 * <li>int: number of nominal value mappings</li>
 * <li>byte: number of bytes for representing a single value.</li>
 * <li>2 bytes: reserved</li>
 * <li>variable length utf-8 string for name</li>
 * <li>variable length utf-8 string for role</li>
 * </ol>
 * <h3>Value Mapping Section</h3> Repeated as often as there are values. Each Mapping contains the
 * following bytes:
 * <ol>
 * <li>int: numerical code</li>
 * <li>int: byte length of nominal value string in utf8</li>
 * <li>variable length utf-8 string</li>
 * </ol>
 * 
 * <h2>Data Payload Section</h2>Here for each column depending on their type and number of bytes
 * representing a single value, data is stored column wise.
 * 
 * @author Sebastian Land
 *
 */
@DefinesSerializer(supportedClass = ExampleSet.class)
public class ExampleSetSerializer implements Serializer {

    private static final byte TYPE_NUMERIC = 0;
    private static final byte TYPE_NOMINAL = 1;
    private static final byte TYPE_TIMESTAMP = 2;

    private class ValueMapping {

        byte[] valueBytes = null;
        int valueCode;


        public ValueMapping(String value, int valueCode) {
            if (value != null)
                valueBytes = value.getBytes(StandardCharsets.UTF_8);
            this.valueCode = valueCode;
        }

        public ValueMapping(DataInputStream in) throws IOException {
            this.valueCode = in.readInt();
            int valueLength = in.readInt();
            if (valueLength >= 0)
                this.valueBytes = in.readNBytes(valueLength);
        }

        public void serialize(DataOutputStream out) throws IOException {
            out.writeInt(valueCode);
            if (valueBytes != null) {
                out.writeInt(valueBytes.length);
                out.write(valueBytes);
            } else {
                out.writeInt(-1);
            }
        }

        public long getByteLength() {
            if (valueBytes != null)
                return Integer.BYTES + Integer.BYTES + valueBytes.length;
            return Integer.BYTES + Integer.BYTES;
        }

        public String getValue() {
            if (valueBytes != null)
                return new String(valueBytes, StandardCharsets.UTF_8);
            return null;
        }

        public int getIndex() {
            return valueCode;
        }
    }

    private class ColumnDescription {

        byte type;
        int numberOfNominalValues;
        byte lengthOfValueRepresentation;
        byte[] nameBytes;
        byte[] roleBytes = null;
        ValueMapping[] mappings = null;
        BiDirectionalIntStringMapping mapping = null;

        public ColumnDescription(ExampleSet exampleSet, Attribute attribute) {
            nameBytes = attribute.getName().getBytes(StandardCharsets.UTF_8);
            if (attribute.isSpecial())
                this.roleBytes = attribute.getRole().getBytes(StandardCharsets.UTF_8);
            if (attribute.isNominal()) {
                type = TYPE_NOMINAL;
                String[] values = exampleSet.dataColumns[attribute.attributeIndex].mapping.values();
                mapping = new BiDirectionalIntStringMapping(IntStream.range(1, values.length).mapToObj(i -> values[i]));
                numberOfNominalValues = mapping.size();
                if (numberOfNominalValues < Byte.MAX_VALUE)
                    lengthOfValueRepresentation = 1;
                else if (numberOfNominalValues < Short.MAX_VALUE)
                    lengthOfValueRepresentation = 2;
                else
                    lengthOfValueRepresentation = 4;
                mappings = new ValueMapping[numberOfNominalValues];
                for (int i = 0; i < numberOfNominalValues; i++)
                    mappings[i] = new ValueMapping(mapping.mapIndex(i), i);
            } else if (attribute.isNumerical()) {
                type = TYPE_NUMERIC;
                numberOfNominalValues = 0;
                lengthOfValueRepresentation = 8;
            } else {
                type = TYPE_TIMESTAMP;
                numberOfNominalValues = 0;
                lengthOfValueRepresentation = 8;
            }

        }

        public ColumnDescription(DataInputStream in) throws IOException {
            int namesLength = in.readInt();
            int rolesLength = in.readInt();
            type = in.readByte();
            numberOfNominalValues = in.readInt();
            lengthOfValueRepresentation = in.readByte();
            // reserved
            in.readByte();
            in.readByte();
            nameBytes = in.readNBytes(namesLength);
            if (rolesLength > 0)
                roleBytes = in.readNBytes(rolesLength);

            if (type == TYPE_NOMINAL) {
                mappings = new ValueMapping[numberOfNominalValues];
                for (int i = 0; i < numberOfNominalValues; i++) {
                    mappings[i] = new ValueMapping(in);
                }

            }
        }

        public void serialize(DataOutputStream out) throws IOException {
            out.writeInt(nameBytes.length);
            out.writeInt(roleBytes != null ? roleBytes.length : 0);
            out.writeByte(type);
            out.writeInt(numberOfNominalValues);
            out.writeByte(lengthOfValueRepresentation);
            // reserved
            out.writeByte(0);
            out.writeByte(0);
            out.write(nameBytes);
            if (roleBytes != null)
                out.write(roleBytes);
            if (mappings != null)
                for (ValueMapping mapping : mappings) {
                    mapping.serialize(out);
                }
        }

        public long getByteLength() {
            return 3 * Integer.BYTES + 4 * Byte.BYTES + nameBytes.length + (roleBytes != null ? roleBytes.length : 0)
                    + (mappings != null ? Arrays.stream(mappings).mapToLong(ValueMapping::getByteLength).sum() : 0);
        }

        public String getName() {
            return new String(nameBytes, StandardCharsets.UTF_8);
        }

        public boolean isSpecial() {
            return roleBytes != null;
        }

        public String getRole() {
            if (roleBytes != null)
                return new String(roleBytes, StandardCharsets.UTF_8);
            return null;
        }

        public ValueType getValueType() {
            switch (type) {
                case TYPE_NOMINAL:
                    return ValueType.NOMINAL;
                case TYPE_NUMERIC:
                    return ValueType.NUMERIC;
                case TYPE_TIMESTAMP:
                default:
                    return ValueType.TIMESTAMP;
            }
        }
    }

    private class PayLoad {

        private ExampleSet exampleSet;
        private ValueType[] attributeTypes;
        private ColumnDescription[] columnDescriptions;

        public PayLoad(ExampleSet exampleSet, Header header) {
            this.exampleSet = exampleSet;

            this.attributeTypes = exampleSet.getAttributes().streamAttributes().map(Attribute::getValueType).toArray(ValueType[]::new);
            columnDescriptions = header.columnDescriptions;
        }

        public PayLoad(ValueType[] attributeTypes, ColumnDescription[] columnDescriptions) {
            this.columnDescriptions = columnDescriptions;
            this.attributeTypes = attributeTypes;
        }

        public void serialize(DataOutputStream dataOut) throws IOException {
            // column wise
            for (int attributeIndex = 0; attributeIndex < attributeTypes.length; attributeIndex++) {
                switch (attributeTypes[attributeIndex]) {
                    case NOMINAL:
                        ColumnDescription columnDescription = columnDescriptions[attributeIndex];
                        switch (columnDescription.lengthOfValueRepresentation) {
                            case 1:
                                for (int row = 0; row < exampleSet.size; row++) {
                                    dataOut.writeByte(columnDescription.mapping.mapString(exampleSet.getNominalValue(row, attributeIndex)));
                                }
                                break;
                            case 2:
                                for (int row = 0; row < exampleSet.size; row++) {
                                    dataOut.writeShort(columnDescription.mapping.mapString(exampleSet.getNominalValue(row, attributeIndex)));
                                }
                                break;
                            default:
                                for (int row = 0; row < exampleSet.size; row++) {
                                    dataOut.writeInt(columnDescription.mapping.mapString(exampleSet.getNominalValue(row, attributeIndex)));
                                }
                                break;
                        }
                        break;
                    case NUMERIC:
                        for (int row = 0; row < exampleSet.size; row++) {
                            dataOut.writeDouble(exampleSet.getNumericValue(row, attributeIndex));
                        }
                        break;
                    case TIMESTAMP:
                        for (int row = 0; row < exampleSet.size; row++) {
                            dataOut.writeLong(exampleSet.getTimestampValue(row, attributeIndex));
                        }
                        break;
                }
            }
        }

        public void deserialize(int numberOfRows, DataInputStream dataIn, DataColumn[] columns) throws IOException {
            // column wise
            for (int columnIndex = 0; columnIndex < attributeTypes.length; columnIndex++) {
                DataColumn column = columns[columnIndex];
                switch (attributeTypes[columnIndex]) {
                    case NOMINAL:
                        switch (columnDescriptions[columnIndex].lengthOfValueRepresentation) {
                            case 1:
                                for (int row = 0; row < numberOfRows; row++)
                                    column.setNominalIndexValue(row, dataIn.readByte());
                                break;
                            case 2:
                                for (int row = 0; row < numberOfRows; row++)
                                    column.setNominalIndexValue(row, dataIn.readShort());
                                break;
                            default:
                                for (int row = 0; row < numberOfRows; row++)
                                    column.setNominalIndexValue(row, dataIn.readInt());
                                break;
                        }
                        break;
                    case NUMERIC:
                        for (int row = 0; row < numberOfRows; row++)
                            column.setNumericValue(row, dataIn.readDouble());
                        break;
                    case TIMESTAMP:
                        for (int row = 0; row < numberOfRows; row++) {
                            column.setTimestampValue(row, dataIn.readLong());
                        }
                        break;
                }
            }
        }
    }

    private class Header {

        byte versionId;
        long numberOfRows;
        ColumnDescription[] columnDescriptions;
        long lengthOfColumnDescriptions;


        public Header(ExampleSet exampleSet) {
            super();
            this.versionId = 1;
            this.numberOfRows = exampleSet.size();
            columnDescriptions = exampleSet.getAttributes().streamAttributes().map(a -> new ColumnDescription(exampleSet, a)).toArray(ColumnDescription[]::new);
            lengthOfColumnDescriptions = Arrays.stream(columnDescriptions).mapToLong(ColumnDescription::getByteLength).sum();
        }

        public Header(DataInputStream in) throws IOException {
            versionId = in.readByte();
            // reserved
            in.readByte();
            in.readByte();
            in.readByte();
            numberOfRows = in.readLong();
            int numberOfColumns = in.readInt();
            columnDescriptions = new ColumnDescription[numberOfColumns];
            lengthOfColumnDescriptions = in.readLong();
            for (int i = 0; i < numberOfColumns; i++) {
                columnDescriptions[i] = new ColumnDescription(in);
            }
        }

        public void serialize(DataOutputStream out) throws IOException {
            out.writeByte(versionId);
            out.writeByte(0);
            out.writeByte(0);
            out.writeByte(0);
            out.writeLong(numberOfRows);
            out.writeInt(columnDescriptions.length);
            out.writeLong(lengthOfColumnDescriptions);
            for (ColumnDescription description : columnDescriptions)
                description.serialize(out);
        }

    }

    @Override
    public void serialize(Object object, OutputStream out) throws IOException {
        ExampleSet exampleSet = (ExampleSet) object;
        Header header = new Header(exampleSet);
        PayLoad payLoad = new PayLoad(exampleSet, header);

        DataOutputStream dataOut = new DataOutputStream(out);
        header.serialize(dataOut);
        payLoad.serialize(dataOut);
    }

    @Override
    public Object deserialize(InputStream in, Class<?> targetClass, ClassLoader classLoader, byte version) throws IOException {
        DataInputStream dataIn = new DataInputStream(in);
        Header header = new Header(dataIn);
        String[] attributeNames = Arrays.stream(header.columnDescriptions).map(ColumnDescription::getName).toArray(String[]::new);
        String[] attributeRoles = Arrays.stream(header.columnDescriptions).map(ColumnDescription::getRole).toArray(String[]::new);
        ValueType[] attributeTypes = Arrays.stream(header.columnDescriptions).map(ColumnDescription::getValueType).toArray(ValueType[]::new);
        boolean[] attributeIsSpecial = new boolean[attributeRoles.length];
        for (int i = 0; i < attributeNames.length; i++)
            attributeIsSpecial[i] = attributeRoles[i] != null;

        // create columns
        int numberOfRows = (int) header.numberOfRows;
        DataColumn[] dataColumns = Arrays.stream(header.columnDescriptions)
                .map(cd -> new DataColumn(numberOfRows, cd.getValueType()))
                .toArray(DataColumn[]::new);
        // premap nominals
        for (int i = 0; i < attributeTypes.length; i++) {
            if (attributeTypes[i] == ValueType.NOMINAL) {
                NominalMapping nominalMapping = dataColumns[i].mapping;
                for (ValueMapping mappingPair : header.columnDescriptions[i].mappings) {
                    if (mappingPair.getIndex() == nominalMapping.size())
                        nominalMapping.mapString(mappingPair.getValue());
                    else
                        throw new IOException("Illegal data content.");
                }
            }
        }

        // read the payload into the rows
        PayLoad payLoad = new PayLoad(attributeTypes, header.columnDescriptions);
        payLoad.deserialize(numberOfRows, dataIn, dataColumns);

        for (DataColumn column : dataColumns)
            column.finishWriting(numberOfRows);

        return new ExampleSet(numberOfRows, attributeNames, attributeTypes, dataColumns, attributeRoles, attributeIsSpecial);
    }

    @Override
    public byte getCurrentVersion() {
        return 2;
    }

}
