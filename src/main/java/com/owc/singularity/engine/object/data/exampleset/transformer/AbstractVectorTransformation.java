package com.owc.singularity.engine.object.data.exampleset.transformer;

import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;


public abstract class AbstractVectorTransformation implements ExampleSetTransformation {

    protected final String newAttributeName;
    protected final String sourceAttributeName;
    protected int maxRows;
    private ValueType valueType;

    public AbstractVectorTransformation(String newAttributeName, ValueType valueType, String sourceAttributeName) {
        this.newAttributeName = newAttributeName;
        this.valueType = valueType;
        this.sourceAttributeName = sourceAttributeName;
    }

    @Override
    public Stream<ColumnDescription> apply(ExampleSet sourceSet) {
        DataColumn dataColumn = new DataColumn(sourceSet.size(), valueType);
        maxRows = sourceSet.size();
        apply(dataColumn, sourceSet, sourceSet.getAttributeIndex(sourceSet.getAttributes().get(sourceAttributeName)));
        dataColumn.finishWriting(sourceSet.size());
        return Stream.of(new ColumnDescription(newAttributeName, valueType, dataColumn));
    }

    protected abstract void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex);
}
