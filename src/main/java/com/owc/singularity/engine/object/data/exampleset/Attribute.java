/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.io.Serializable;
import java.util.Objects;


/**
 * Attributes should hold all information about a single attribute.
 * <ul>
 * <li>the name</li>
 * <li>the value type (nominal, numerical, ...)</li>
 * </ul>
 * 
 * Two attributes are equal when their name and type is the same. Please notice that this does not
 * mean they are from the same {@link ExampleSet}.
 * 
 * @author Sebastian Land
 */
public final class Attribute implements Serializable {

    private static final long serialVersionUID = -7578509560353695598L;

    final String name;
    final ValueType valueType;
    final ExampleSet exampleSet;
    final int attributeIndex;

    public Attribute(String name, ValueType valueType, ExampleSet exampleSet, int attributeIndex) {
        this.name = name;
        this.valueType = valueType;
        this.exampleSet = exampleSet;
        this.attributeIndex = attributeIndex;
    }

    /** Returns the name of the attribute. */
    public String getName() {
        return name;
    }

    /**
     * Returns the value type of this attribute.
     * 
     * @see com.owc.singularity.engine.object.data.exampleset.ValueType
     */
    public ValueType getValueType() {
        return valueType;
    }

    public boolean isSpecial() {
        return exampleSet.getAttributes().attributesIsSpecial[attributeIndex];
    }

    public boolean isRegular() {
        return !exampleSet.getAttributes().attributesIsSpecial[attributeIndex];
    }

    public String getRole() {
        return exampleSet.getAttributes().attributesRole[attributeIndex];
    }

    /** Returns a human-readable string that describes this attribute. */
    @Override
    public String toString() {
        return name + "[" + valueType + "]";
    };

    /** Returns true if the attribute is nominal. */
    public boolean isNominal() {
        return valueType == ValueType.NOMINAL;
    }

    /** Returns true if the attribute is numerical. */
    public boolean isNumerical() {
        return valueType == ValueType.NUMERIC;
    };

    /** Returns true if the attribute is date_time. */
    public boolean isTimestamp() {
        return valueType == ValueType.TIMESTAMP;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, valueType);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Attribute other = (Attribute) obj;
        return Objects.equals(name, other.name) && valueType == other.valueType;
    }

    /**
     * This method returns the attribute's index within it's {@link ExampleSet}. This can be used
     * for efficient access by storing the indices and accessing
     * {@link ExampleSet#getNominalValue(int, int)} or similar methods.
     * 
     * @return the index
     */
    public int getIndex() {
        return attributeIndex;
    };
}
