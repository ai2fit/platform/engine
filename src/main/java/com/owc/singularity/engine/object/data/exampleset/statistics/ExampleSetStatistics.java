package com.owc.singularity.engine.object.data.exampleset.statistics;

import java.util.*;
import java.util.stream.Collectors;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;

/**
 * This class can be used to calculate the {@link Statistics} of {@link Attribute}s from an
 * {@link ExampleSet}. Statistics can be calculated per attribute or for the whole ExampleSet.
 * Statistics only need to be calculated once and can be retrieved again without much overhead.
 */
public class ExampleSetStatistics {

    private ExampleSet exampleSet;
    /** Maps attribute names to list of statistics objects. */
    private final Map<String, Statistics> statisticsMap = new HashMap<>();

    public ExampleSetStatistics(ExampleSet exampleSet) {
        this.exampleSet = exampleSet;
    }

    /**
     * Recalculates the attribute statistics for all attributes. They are average value, variance,
     * minimum, and maximum. For nominal attributes the occurrences for all values are counted. This
     * method collects all attributes (regular and special) in a list and invokes
     * <code>recalculateAttributeStatistics(List attributes)</code> and performs only one data scan.
     * <p>
     * The statistics calculation is stopped by {@link Thread#interrupt()}.
     */
    public ExampleSetStatistics calculateAllAttributeStatistics() {
        List<Attribute> allAttributes = new ArrayList<>();
        Iterator<Attribute> a = exampleSet.getAttributes().allAttributes();
        while (a.hasNext()) {
            allAttributes.add(a.next());
        }
        calculateAttributeStatistics(allAttributes);
        return this;
    }

    /**
     * Recalculate the attribute statistics of the given attribute.
     * <p>
     * The statistics calculation is stopped by {@link Thread#interrupt()}.
     */
    public ExampleSetStatistics calculateAttributeStatistics(Attribute attribute) {
        List<Attribute> allAttributes = new ArrayList<>();
        allAttributes.add(attribute);
        calculateAttributeStatistics(allAttributes);
        return this;
    }

    /**
     * Here the Example Set is parsed only once, all the information is retained for each example
     * set.
     * <p>
     * The statistics calculation is stopped by {@link Thread#interrupt()}.
     */
    private synchronized void calculateAttributeStatistics(List<Attribute> attributeList) {
        // do nothing if not desired
        if (!attributeList.isEmpty()) {
            // calculate statistics
            final Attribute weightAttribute = exampleSet.getAttributes().getWeight();
            if (weightAttribute != null) {
                int weightIndex = weightAttribute.getIndex();
                statisticsMap.putAll(exampleSet.getAttributes().streamAttributes().parallel().collect(Collectors.toMap(Attribute::getName, a -> {
                    Statistics statistics = new Statistics(a);
                    int attributeIndex = a.getIndex();
                    int maxRows = exampleSet.size();
                    switch (a.getValueType()) {
                        case NOMINAL -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getNominalValue(i, attributeIndex), exampleSet.getNumericValue(i, weightIndex));
                            }
                        }
                        case NUMERIC -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getNumericValue(i, attributeIndex), exampleSet.getNumericValue(i, weightIndex));
                            }
                        }
                        case TIMESTAMP -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getTimestampValue(i, attributeIndex), exampleSet.getNumericValue(i, weightIndex));
                            }
                        }
                    }
                    return statistics;
                })));
            } else {
                statisticsMap.putAll(exampleSet.getAttributes().streamAttributes().parallel().collect(Collectors.toMap(Attribute::getName, a -> {
                    Statistics statistics = new Statistics(a);
                    int attributeIndex = a.getIndex();
                    int maxRows = exampleSet.size();
                    switch (a.getValueType()) {
                        case NOMINAL -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getNominalValue(i, attributeIndex), 1d);
                            }
                        }
                        case NUMERIC -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getNumericValue(i, attributeIndex), 1d);
                            }
                        }
                        case TIMESTAMP -> {
                            for (int i = 0; i < maxRows; i++) {
                                statistics.count(exampleSet.getTimestampValue(i, attributeIndex), 1d);
                            }
                        }
                    }
                    return statistics;
                })));
            }
        }
    }

    public Statistics get(Attribute attribute) {
        return statisticsMap.get(attribute.getName());
    }

    public Statistics getStatistics(Attribute attribute) {
        return statisticsMap.get(attribute.getName());
    }
}
