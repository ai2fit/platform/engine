/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.operator.error.UserError;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;


/**
 * This {@link ExampleSet} is a clone of the attributes without reference to any data. Therefore, it
 * can be used as a data header description. Since no data reference exists, all example based
 * methods will throw an {@link UnsupportedOperationException}.
 * 
 * @author Ingo Mierswa
 */
public class ExampleSetHeader implements Serializable {

    /** Determines how to compare the sets of Attributes of training to apply set. */
    public static enum TrainingSetRelation {
        /** Both sets of regular attributes must be exactly the same. */
        EQUAL,
        /**
         * The application set Attributes can contain additional attributes compared to training
         * set. They are usually then ignored.
         */
        ALLOW_SUPERSET,
        /**
         * The application set of Attributes can contain additional attributes AND may contain not
         * all of the attributes in the training set.
         */
        ALLOW_INTERSECTION
    }

    private static final ExampleSetHeader EMPTY_HEADER = new ExampleSetHeader(new String[0], new ValueType[0], new String[0], new boolean[0]);

    private static final long serialVersionUID = -255270841843010670L;
    private String[] attributeNames;
    private ValueType[] attributeTypes;
    private String[] attributeRoles;
    private boolean[] attributeIsSpecial;
    private transient Object2IntOpenHashMap<String> attributeIndicesByNameMap;

    public ExampleSetHeader() {
        attributeNames = EMPTY_HEADER.attributeNames;
        attributeTypes = EMPTY_HEADER.attributeTypes;
        attributeRoles = EMPTY_HEADER.attributeRoles;
        attributeIsSpecial = EMPTY_HEADER.attributeIsSpecial;
    }

    ExampleSetHeader(ExampleSet parent) {
        this(parent.getAttributes());
    }


    /**
     * Creates a new header example set with the given attribute. The attributes are not cloned
     * automatically and must be cloned before calling this constructor if they are shared with
     * another {@link ExampleSet}.
     *
     * @param attributes
     *            the attributes for the header example set
     * @since 8.1.0
     */
    public ExampleSetHeader(Attributes attributes) {
        this.attributeNames = Arrays.stream(attributes.attributes).map(Attribute::getName).toArray(String[]::new);
        this.attributeTypes = Arrays.stream(attributes.attributes).map(Attribute::getValueType).toArray(ValueType[]::new);
        this.attributeIsSpecial = attributes.attributesIsSpecial;
        this.attributeRoles = attributes.attributesRole;
    }

    public ExampleSetHeader(ExampleSetMetaData trainingSetMetaData) {
        if (trainingSetMetaData == null) {
            attributeNames = EMPTY_HEADER.attributeNames;
            attributeTypes = EMPTY_HEADER.attributeTypes;
            attributeRoles = EMPTY_HEADER.attributeRoles;
            attributeIsSpecial = EMPTY_HEADER.attributeIsSpecial;
        } else {
            Collection<AttributeMetaData> attributeCollection = trainingSetMetaData.getAllAttributes();
            Iterator<AttributeMetaData> attributeIterator = attributeCollection.iterator();

            attributeNames = new String[attributeCollection.size()];
            attributeTypes = new ValueType[attributeCollection.size()];
            attributeRoles = new String[attributeCollection.size()];
            attributeIsSpecial = new boolean[attributeCollection.size()];
            for (int i = 0; attributeIterator.hasNext(); i++) {
                AttributeMetaData amd = attributeIterator.next();
                attributeNames[i] = amd.getName();
                attributeTypes[i] = amd.getValueType();
                attributeRoles[i] = trainingSetMetaData.getRole(amd);
                attributeIsSpecial[i] = attributeRoles[i] != null;
            }
        }
    }

    private ExampleSetHeader(String[] attributeNames, ValueType[] attributeTypes, String[] attributeRoles, boolean[] attributeIsSpecial) {
        this.attributeNames = attributeNames;
        this.attributeTypes = attributeTypes;
        this.attributeRoles = attributeRoles;
        this.attributeIsSpecial = attributeIsSpecial;
    }

    public String[] getRegularAttributeNames() {
        return IntStream.range(0, attributeIsSpecial.length).filter(i -> !attributeIsSpecial[i]).mapToObj(i -> attributeNames[i]).toArray(String[]::new);
    }

    public int getNumberOfAttributes() {
        return attributeNames.length;
    }

    public String getAttributeName(int index) {
        return attributeNames[index];
    }

    public String getAttributeRole(int index) {
        return attributeRoles[index];
    }

    public ValueType getAttributeType(int index) {
        return attributeTypes[index];
    }

    /**
     * This method will only work for attributes that are indeed part of this header.
     * 
     * @param name
     * @return
     */
    public ValueType getAttributeType(String name) {
        return attributeTypes[getAttributeIndex(name)];
    }

    public boolean isSpecialAttribute(int index) {
        return attributeIsSpecial[index];
    }

    /**
     * This returns the name of the attribute with the specified role or null if no such attribute
     * is present.
     * 
     * @param role
     *            name of the role
     * @return the name of the attribute
     */
    public String getAttributeNameForRole(String role) {
        for (int i = 0; i < attributeRoles.length; i++)
            if (Objects.equals(role, attributeRoles[i]))
                return attributeNames[i];
        return null;
    }

    /**
     * This checks whether this header is compatible with the given applicationAttributes. This is
     * usually required if a model is trained on a given set and then is applied on another set. Of
     * course the columns must usually be present that have been present during training.
     * 
     * @param applySetHeader
     *            The attributes of the new set where the model is applied on
     * @param allowedRelation
     *            the option how to compare
     * @throws UserError
     */
    public boolean isMatching(ExampleSetHeader applySetHeader, ExampleSetHeader.TrainingSetRelation allowedRelation) {
        // check what has been present during training for cases that applySet must be at least
        // containing all training attributes
        for (int i = 0; i < attributeNames.length; i++) {
            if (!attributeIsSpecial[i]) {
                int applySetIndex = applySetHeader.getAttributeIndex(attributeNames[i]);
                if (applySetIndex != -1) {
                    ValueType originalValueType = attributeTypes[i];
                    ValueType comparedValueType = applySetHeader.getAttributeType(applySetIndex);
                    if (originalValueType != comparedValueType) {
                        return false;
                    }
                } else {
                    if (allowedRelation == ExampleSetHeader.TrainingSetRelation.EQUAL) {
                        return false;
                    }
                    if (allowedRelation == ExampleSetHeader.TrainingSetRelation.ALLOW_SUPERSET) {
                        return false;
                    }
                }
            }
        }

        // check what is present in the apply set for cases that the apply set must be equal
        if (allowedRelation == ExampleSetHeader.TrainingSetRelation.EQUAL) {
            for (int applySetIndex = 0; applySetIndex < applySetHeader.attributeNames.length; applySetIndex++) {
                if (!applySetHeader.attributeIsSpecial[applySetIndex]) {
                    int trainSetIndex = getAttributeIndex(applySetHeader.attributeNames[applySetIndex]);
                    if (trainSetIndex == -1 || attributeIsSpecial[trainSetIndex]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void checkMatching(ExampleSetHeader applySetHeader, ExampleSetHeader.TrainingSetRelation allowedRelation) throws UserError {
        // check what has been present during training for cases that applySet must be at least
        // containing all training attributes
        for (int i = 0; i < attributeNames.length; i++) {
            if (!attributeIsSpecial[i]) {
                int applySetIndex = applySetHeader.getAttributeIndex(attributeNames[i]);
                if (applySetIndex != -1) {
                    ValueType originalValueType = attributeTypes[i];
                    ValueType comparedValueType = applySetHeader.getAttributeType(applySetIndex);
                    if (originalValueType != comparedValueType) {
                        throw new UserError(null, 963, applySetHeader.getAttributeName(applySetIndex), originalValueType, comparedValueType);
                    }
                } else {
                    if (allowedRelation == ExampleSetHeader.TrainingSetRelation.EQUAL) {
                        throw new UserError(null, 960, attributeNames[i]);
                    }
                    if (allowedRelation == ExampleSetHeader.TrainingSetRelation.ALLOW_SUPERSET) {
                        throw new UserError(null, 962, attributeNames[i]);
                    }
                }
            }
        }

        // check what is present in the apply set for cases that the apply set must be equal
        if (allowedRelation == ExampleSetHeader.TrainingSetRelation.EQUAL) {
            for (int applySetIndex = 0; applySetIndex < applySetHeader.attributeNames.length; applySetIndex++) {
                if (!applySetHeader.attributeIsSpecial[applySetIndex]) {
                    int trainSetIndex = getAttributeIndex(applySetHeader.attributeNames[applySetIndex]);
                    if (trainSetIndex == -1 || attributeIsSpecial[trainSetIndex]) {
                        throw new UserError(null, 960, applySetHeader.getAttributeName(applySetIndex));
                    }
                }
            }
        }
    }

    public boolean containsRegular(String attributeName) {
        int attributeIndex = getAttributeIndex(attributeName);
        return attributeIndex != -1 && !attributeIsSpecial[attributeIndex];
    }

    public boolean contains(String attributeName) {
        return getAttributeIndex(attributeName) != -1;
    }

    /**
     * This returns the index of the given attribute name or -1 if not present. This code can be
     * called thread safely, although the lazy implementation might be performed several times. We
     * use locking mechanism as it proofs to be equally fast in heavy parallel access while not
     * mattering in sequential access as there will be only a single lock..<br>
     * We keep the lazy approach as the data is not used always and will create overhead on memory
     * if performed all the time.
     * 
     * @param attributeName
     *            Name of the attribute to get the index of
     * @return the index or -1
     */
    public int getAttributeIndex(String attributeName) {
        // this map is computed lazily so check on presence first
        if (attributeIndicesByNameMap == null) {
            synchronized (this) {
                if (attributeIndicesByNameMap == null) {
                    Object2IntOpenHashMap<String> attributeIndicesByNameMap = new Object2IntOpenHashMap<>(attributeNames.length);
                    for (int i = 0; i < attributeNames.length; i++)
                        attributeIndicesByNameMap.put(attributeNames[i], i);
                    this.attributeIndicesByNameMap = attributeIndicesByNameMap;
                }
            }
        }

        return attributeIndicesByNameMap.getOrDefault(attributeName, -1);
    }

    public Stream<String> streamRegularAttributes() {
        return IntStream.range(0, attributeNames.length).filter(i -> !attributeIsSpecial[i]).mapToObj(i -> attributeNames[i]);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < attributeNames.length; i++) {
            stringBuilder.append(attributeNames[i])
                    .append(" ")
                    .append(attributeTypes[i])
                    .append(" ")
                    .append(attributeRoles[i] != null ? attributeRoles[i] : "")
                    .append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }
}
