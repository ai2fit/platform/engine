package com.owc.singularity.engine.object.data.exampleset;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang.ObjectUtils;

import com.owc.singularity.engine.object.data.exampleset.transformer.ExampleSetTransformation;
import com.owc.singularity.engine.object.data.exampleset.transformer.MultipleComputingRowTransformation.RedirectableRowAccessor;
import com.owc.singularity.engine.object.data.exampleset.transformer.constants.ConstantCreation;
import com.owc.singularity.engine.object.data.exampleset.transformer.constants.NominalConstantCreation;
import com.owc.singularity.engine.object.data.exampleset.transformer.constants.NumericConstantCreation;
import com.owc.singularity.engine.object.data.exampleset.transformer.constants.TimestampConstantCreation;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.partition.Partition;
import com.owc.singularity.engine.object.data.exampleset.transformer.nominal.*;
import com.owc.singularity.engine.object.data.exampleset.transformer.nominal.NominalRowTransformation.NominalRowAccessor;
import com.owc.singularity.engine.object.data.exampleset.transformer.numeric.*;
import com.owc.singularity.engine.object.data.exampleset.transformer.numeric.NumericRowTransformation.NumericRowAccessor;
import com.owc.singularity.engine.object.data.exampleset.transformer.timestamp.*;
import com.owc.singularity.engine.object.data.exampleset.transformer.timestamp.TimestampRowTransformation.TimestampRowAccessor;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;
import com.owc.singularity.engine.operator.tools.ExpressionEvaluationException;
import com.owc.singularity.engine.tools.function.ObjObjIntIntConsumer;

import it.unimi.dsi.fastutil.ints.IntArrays;
import it.unimi.dsi.fastutil.ints.IntComparator;


/**
 * This is the class that takes an existing {@link ExampleSet} and offers methods that can safely be
 * used within multi-threaded applications to transform it. <br>
 * <b>Attention: This will change the given example set. If you need to preserve the original one,
 * you need to clone it and call the {@link ExampleSet#transform} on the clone.</b>
 * <p>
 * 
 * @author Sebastian Land
 *
 */
public class ExampleSetTransformer {

    public enum SortingDirection {
        INCREASING, DECREASING
    }

    private final ExampleSet sourceSet;


    private final LinkedList<IntPredicate> filters = new LinkedList<>();

    private IntUnaryOperator reorderFunction;
    private int reorderSize;

    private boolean shuffle;
    private Random shuffleRandom;

    private LinkedList<String> sortAttributeNames = new LinkedList<>();
    private LinkedList<SortingDirection> sortDirections = new LinkedList<>();

    private final LinkedList<ExampleSetTransformation> derivations = new LinkedList<>();
    private final LinkedList<ConstantCreation> constants = new LinkedList();

    private final HashMap<String, String> attributeRenamings = new HashMap<>();
    private final HashMap<String, String> attributeRoles = new HashMap<>();
    private final HashMap<String, String> attributeRolesRemapping = new HashMap<>();

    private final HashSet<String> removeAttributeNames = new HashSet<>();
    private final HashSet<String> containedAttributeNames = new HashSet<>();

    private final LinkedList<Runnable> cleanUpOperations = new LinkedList<>();


    private OperatorException exception;


    private List<String> attributeOrder = null;


    public ExampleSetTransformer(ExampleSet source) {
        this.sourceSet = source;
        sourceSet.attributes.streamAttributes().forEach(att -> containedAttributeNames.add(att.getName()));
    }


    /**
     * This creates a new nominal attribute that is based upon the values of an existing numeric
     * attribute. If the derived attribute is named the same as the source attribute, the source
     * attribute will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNominalAttribute(String derivedName, String sourceName, DoubleFunction<String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericToNominalPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new nominal attribute that is based upon the values of an existing timestamp
     * attribute. If the derived attribute is named the same as the source attribute, the source
     * attribute will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNominalAttribute(String derivedName, String sourceName, LongFunction<String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampToNominalPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a state that is updated by the values in order
     * of the example set.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param stateSupplier
     *            a supplier for a new state object
     * @param stateUpdate
     *            the update function for the state
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the state
     * @return this transformer for chaining
     * 
     */
    public <S> ExampleSetTransformer withStatefulDerivedNominalAttribute(String derivedName, String sourceName, Supplier<S> stateSupplier,
            BiConsumer<S, String> stateUpdate, Function<S, String> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalStatefulPointTransformation<S>(derivedName, sourceName, stateSupplier, stateUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new nominal attribute that is based upon the values of an existing nominal
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a context around the current column positions's
     * value. The context is updated by being supplied with position and a function for accessing
     * column positions.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param contextSupplier
     *            a supplier for a new context object
     * @param contextUpdate
     *            the update function for the context. Must be able to init itself once given a
     *            position in the vector.
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the context
     * @return this transformer for chaining
     * 
     */
    public <C> ExampleSetTransformer withContextuallyDerivedNominalAttribute(String derivedName, String sourceName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntFunction<String>> contextUpdate, Function<C, String> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalContextualVectorTransformation<C>(derivedName, sourceName, contextSupplier, contextUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new nominal attribute that is based upon values of multiple existing nominal
     * attributes. Therefore the derive function will be supplied with a {@link NominalRowAccessor}
     * which grants access to nominal attribute values under the index as specified by
     * nominalSourceAttributeNames.
     * 
     * @param derivedName
     *            the name of the new attribute
     * @param derive
     *            the function to compute the new attribute from values of the specified attributes
     * @param nominalSourceAttributeNames
     *            the attributes to use
     * @return this transformer for chaining
     * 
     *         thrown if attribute is already present
     */
    public ExampleSetTransformer withRowDerivedNominalAttribute(String derivedName, Function<NominalRowAccessor, String> derive,
            String... nominalSourceAttributeNames) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalRowTransformation(derivedName, nominalSourceAttributeNames, derive));
        return this;
    }

    /**
     * This creates a new nominal attribute that is based upon the values of an existing one. If the
     * derived attribute is named the same as the source attribute, the source attribute will be
     * removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNominalAttribute(String derivedName, String sourceName, Function<String, String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new polynominal attribute that is based upon the values of attributes within
     * the same example. If the name already exists, the existing attribute will be removed.
     * 
     * @deprecated this method should be avoided as creating {@link Example} objects creates
     *             unnecessary overhead. You should rather use the
     *             {@link #withGeneratedNominalAttribute(String, IntFunction)} and accessing the
     *             original nominal values using {@link ExampleSet#getNominalValue(int, int)} to
     *             acquire the corresponding value of the current row.
     * @param derivedName
     *            name of the new attribute
     * @param derive
     *            the function to map the examples to the new attribute
     * @return this transformer for chaining
     * 
     */
    @Deprecated
    public ExampleSetTransformer withDerivedNominalAttribute(String derivedName, Function<Example, String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalExampleTransformation(derivedName, derive));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNumericAttribute(String derivedName, String sourceName, ToDoubleFunction<String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalToNumericPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing timestamp
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNumericAttribute(String derivedName, String sourceName, LongToDoubleFunction derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampToNumericPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a state that is updated by the values in order
     * of the example set.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param stateSupplier
     *            a supplier for a new state object
     * @param stateUpdate
     *            the update function for the state
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the state
     * @return this transformer for chaining
     * 
     */
    public <S> ExampleSetTransformer withStatefulDerivedNumericAttribute(String derivedName, String sourceName, Supplier<S> stateSupplier,
            ObjDoubleConsumer<S> stateUpdate, ToDoubleFunction<S> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericStatefulPointTransformation<S>(derivedName, sourceName, stateSupplier, stateUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of the specified numeric,
     * nominal and timestamp attributes. If the derived attribute is named the same as another
     * attribute, the previous attribute will be removed. The derivation is based upon a state that
     * is updated by the values of all specified attributes in order of the example set.
     *
     * @param derivedName
     *            name of the new attribute
     * @param numericAttributeIndexes
     *            index of the numeric attributes
     * @param nominalAttributeIndexes
     *            index of the nominal attributes
     * @param timestampAttributeIndexes
     *            index of the timestamp attributes
     * @param stateSupplier
     *            a supplier for a new state object
     * @param stateUpdate
     *            the update function for the state
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the state
     * @return this transformer for chaining
     * 
     */
    public <S> ExampleSetTransformer withRowStatefulDerivedNumericAttribute(String derivedName, int[] numericAttributeIndexes, int[] nominalAttributeIndexes,
            int[] timestampAttributeIndexes, Supplier<S> stateSupplier,
            NumericStatefulRowPointTransformation.ObjIntArrayIntArrayIntArrayConsumer<S> stateUpdate, ToDoubleFunction<S> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericStatefulRowPointTransformation<>(derivedName, stateSupplier, stateUpdate, valueDerivationFunction, numericAttributeIndexes,
                nominalAttributeIndexes, timestampAttributeIndexes));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a context around the current column positions's
     * value. The context is updated by being supplied with position and a function for accessing
     * column positions.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param contextSupplier
     *            a supplier for a new context object
     * @param contextUpdate
     *            the update function for the context. Must be able to init itself once given a
     *            position in the vector.
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the context
     * @return this transformer for chaining
     * 
     */
    public <C> ExampleSetTransformer withContextuallyDerivedNumericAttribute(String derivedName, String sourceName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntToDoubleFunction> contextUpdate, ToDoubleFunction<C> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericContextualVectorTransformation<C>(derivedName, sourceName, contextSupplier, contextUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing timestamp
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a context around the current column positions's
     * value. The context is updated by being supplied with position and a function for accessing
     * column positions. Note: This case should be replaced by a more general API call, where the
     * value type of the source attribute and the derived attribute is specified
     *
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param contextSupplier
     *            a supplier for a new context object
     * @param contextUpdate
     *            the update function for the context. Must be able to init itself once given a
     *            position in the vector.
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the context
     * @return this transformer for chaining
     * 
     */
    public <C> ExampleSetTransformer withContextuallyDerivedNumericAttributeFromTimestampAttribute(String derivedName, String sourceName,
            Supplier<C> contextSupplier, ObjObjIntIntConsumer<C, IntToLongFunction> contextUpdate, ToDoubleFunction<C> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations
                .add(new TimestampToNumericContextualVectorTransformation<C>(derivedName, sourceName, contextSupplier, contextUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new nominal attribute that is based upon the values of an existing nominal
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a context around the current column positions's
     * value. The context is updated by being supplied with position and a function for accessing
     * column positions. Note: This case should be replaced by a more general API call, where the
     * value type of the source attribute and the derived attribute is specified
     *
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param contextSupplier
     *            a supplier for a new context object
     * @param contextUpdate
     *            the update function for the context. Must be able to init itself once given a
     *            position in the vector.
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the context
     * @return this transformer for chaining
     * 
     */
    public <C> ExampleSetTransformer withContextuallyDerivedNumericAttributeFromNominalAttribute(String derivedName, String sourceName,
            Supplier<C> contextSupplier, ObjObjIntIntConsumer<C, IntFunction<String>> contextUpdate, ToDoubleFunction<C> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalToNumericContextualVectorTransformation<>(derivedName, sourceName, contextSupplier, contextUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon values of multiple existing numeric
     * attributes. Therefore the derive function will be supplied with a {@link NumericRowAccessor}
     * which grants access to numeric attribute values under the index as specified by
     * numericSourceAttributeNames.
     * 
     * @param derivedName
     *            the name of the new attribute
     * @param derive
     *            the function to compute the new attribute from values of the specified attributes
     * @param numericSourceAttributeNames
     *            the attributes to use
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withRowDerivedNumericAttribute(String derivedName, ToDoubleFunction<NumericRowAccessor> derive,
            String... numericSourceAttributeNames) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericRowTransformation(derivedName, numericSourceAttributeNames, derive));
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedNumericAttribute(String derivedName, String sourceName, DoubleUnaryOperator derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericPointTransformation(derivedName, sourceName, derive));
        return this;
    }


    /**
     * This creates a new numeric attribute that is based upon the values of attributes within the
     * same example. If the name already exists, the existing attribute will be removed.
     * * @deprecated this method should be avoided as creating {@link Example} objects creates
     * unnecessary overhead. You should rather use the
     * {@link #withGeneratedNumericAttribute(String, IntToDoubleFunction)} and accessing the
     * original nominal values using {@link ExampleSet#getNumericValue(int, int)} to acquire the
     * corresponding value of the current row.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param derive
     *            the function to map the examples to the double value of the new attribute
     * @return this transformer for chaining
     * 
     */
    @Deprecated
    public ExampleSetTransformer withDerivedNumericAttribute(String derivedName, ToDoubleFunction<Example> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericExampleTransformation(derivedName, derive));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon values of multiple existing
     * timestamp attributes. Therefore the derive function will be supplied with a
     * {@link TimestampRowAccessor} which grants access to timestamp attribute values under the
     * index as specified by timestampSourceAttributeNames.
     * 
     * @param derivedName
     *            the name of the new attribute
     * @param derive
     *            the function to compute the new attribute from values of the specified attributes
     * @param timestampSourceAttributeNames
     *            the attributes to use
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withRowDerivedTimestampAttribute(String derivedName, ToLongFunction<TimestampRowAccessor> derive,
            String... timestampSourceAttributeNames) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampRowTransformation(derivedName, timestampSourceAttributeNames, derive));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon the values of an existing one. If
     * the derived attribute is named the same as the source attribute, the source attribute will be
     * removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedTimestampAttribute(String derivedName, String sourceName, LongUnaryOperator derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon the values of an existing numeric
     * attribute. If the derived attribute is named the same as the source attribute, the source
     * attribute will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedTimestampAttribute(String derivedName, String sourceName, DoubleToLongFunction derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NumericToTimestampPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon the values of an existing nominal
     * attribute. If the derived attribute is named the same as the source attribute, the source
     * attribute will be removed.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param derive
     *            the function to map the values of source attribute to the new attribute
     * @return this transformer for chaining
     * 
     */
    public ExampleSetTransformer withDerivedTimestampAttribute(String derivedName, String sourceName, ToLongFunction<String> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new NominalToTimestampPointTransformation(derivedName, sourceName, derive));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon the values of attributes within the
     * same example. If the name already exists, the existing attribute will be removed.
     * * @deprecated this method should be avoided as creating {@link Example} objects creates
     * unnecessary overhead. You should rather use the
     * {@link #withGeneratedTimestampAttribute(String, IntToLongFunction)} and accessing the
     * original nominal values using {@link ExampleSet#getTimestampValue(int, int)} to acquire the
     * corresponding value of the current row.
     * 
     * @param name
     *            name of the new attribute
     * @param derive
     *            the function to map the examples to the double value of the new attribute
     * @return this transformer for chaining
     * 
     */
    @Deprecated
    public ExampleSetTransformer withDerivedTimestampAttribute(String name, ToLongFunction<Example> derive) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        derivations.add(new TimestampExampleTransformation(name, derive));
        return this;
    }

    /**
     * This method allows to compute multiple columns from a result of a computation that is based
     * upon a row of data. An example could be a prediction model, that computes a crisp prediction
     * and class confidences with a single computation.<br>
     * This method returns a configuration, with that you can specify an arbitrary number of
     * attributes that are generated from the computed result. You need to call
     * {@link MultipleComputingRowTransformationConfiguration#finish()} to actually make this part
     * of this transformer.<br>
     * Please notice that for efficiency reasons, you have to provide a {@link Supplier} for result
     * objects. They will be kept thread local, to avoid unnecessary heap polution. Hence you will
     * get an object passed to the computationFunction, which needs to fill in the actual results.
     * 
     * @param <C>
     *            Class of your result. Should be very lightweight.
     * @param sourceAttributeNames
     *            Names of attributes that should be part of the {@link RedirectableRowAccessor} as
     *            they are needed for the computation
     * @param resultObjectSupplier
     *            A supplier that creates a new empty result object.
     * @param computationFunction
     *            A consumer that depending on the values of the rowAccessor fills in the results
     *            into the provided result object.
     * @return a configuration that needs to be finished before it will affect the transformer.
     */
    public <C> MultipleComputingRowTransformationConfiguration<C> configureCalculatedAttributes(String[] sourceAttributeNames, Supplier<C> resultObjectSupplier,
            BiConsumer<RedirectableRowAccessor, C> computationFunction) {
        return new MultipleComputingRowTransformationConfiguration<>(this, sourceAttributeNames, resultObjectSupplier, computationFunction);
    }

    ExampleSetTransformer withDerivation(ExampleSetTransformation transformation, String[] derivedNumericNames, String[] derivedNominalNames,
            String[] derivedTimestampNames) {

        // We have to check whether the names are actually possible
        for (int i = 0; i < derivedNumericNames.length; i++)
            if (!containedAttributeNames.add(derivedNumericNames[i])) {
                throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedNumericNames[i]));
            }
        for (int i = 0; i < derivedNominalNames.length; i++)
            if (!containedAttributeNames.add(derivedNominalNames[i])) {
                throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedNominalNames[i]));
            }
        for (int i = 0; i < derivedTimestampNames.length; i++)
            if (!containedAttributeNames.add(derivedTimestampNames[i])) {
                throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedTimestampNames[i]));
            }


        derivations.add(transformation);
        return this;
    }

    /**
     * This creates a new numeric attribute that is based upon the values of an existing numeric
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a state that is updated by the values in order
     * of the example set.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param stateSupplier
     *            a supplier for a new state object
     * @param stateUpdate
     *            the update function for the state
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the state
     * @return this transformer for chaining
     * 
     */
    public <S> ExampleSetTransformer withStatefulDerivedTimestampAttribute(String derivedName, String sourceName, Supplier<S> stateSupplier,
            ObjLongConsumer<S> stateUpdate, ToLongFunction<S> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampStatefulPointTransformation<S>(derivedName, sourceName, stateSupplier, stateUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This creates a new timestamp attribute that is based upon the values of an existing timestamp
     * one. If the derived attribute is named the same as the source attribute, the source attribute
     * will be removed. The derivation is based upon a context around the current column positions's
     * value. The context is updated by being supplied with position and a function for accessing
     * column positions.
     * 
     * @param derivedName
     *            name of the new attribute
     * @param sourceName
     *            name of the source attribute
     * @param contextSupplier
     *            a supplier for a new context object
     * @param contextUpdate
     *            the update function for the context. Must be able to init itself once given a
     *            position in the vector.
     * @param valueDerivationFunction
     *            the function to get the new value in the attribute from the context
     * @return this transformer for chaining
     * 
     */
    public <C> ExampleSetTransformer withContextuallyDerivedTimestampAttribute(String derivedName, String sourceName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntToLongFunction> contextUpdate, ToLongFunction<C> valueDerivationFunction) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(derivedName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, derivedName));
        }

        derivations.add(new TimestampContextualVectorTransformation<C>(derivedName, sourceName, contextSupplier, contextUpdate, valueDerivationFunction));
        return this;
    }

    /**
     * This method adds a newly generated nominal attribute, whose nominal value is generated from
     * the row number. Use only, if not depending on the data itself. Otherwise see deriving
     * methods.
     * 
     * @param name
     *            the name of the new attribute
     * @param function
     *            function that transforms the row number into a string
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withGeneratedNominalAttribute(String name, IntFunction<String> function) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        derivations.add(new NominalGenerationTransformation(name, function));
        return this;
    }

    /**
     * This method adds a newly generated real attribute, whose value is generated from the row
     * number. Use only, if not depending on the data itself. Otherwise see deriving methods.
     * 
     * @param name
     *            the name of the new attribute
     * @param function
     *            function that transforms the row number into a double
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withGeneratedNumericAttribute(String name, IntToDoubleFunction function) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        derivations.add(new NumericGenerationTransformation(name, function));
        return this;
    }

    /**
     * This method adds a newly generated timestamp attribute, whose value is generated from the row
     * number. Use only, if not depending on the data itself. Otherwise see deriving methods.
     * 
     * @param name
     *            the name of the new attribute
     * @param function
     *            function that transforms the row number into a double
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withGeneratedTimestampAttribute(String name, IntToLongFunction function) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }


        derivations.add(new TimestampGenerationTransformation(name, function));
        return this;
    }


    /**
     * This method adds a constant attribute being backed by a single value.
     * 
     * @param name
     *            the name of the new attribute
     * @param value
     *            the constant value
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withConstantTimestampAttribute(String name, long timestampValue) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        constants.add(new TimestampConstantCreation(name, timestampValue));
        return this;
    }

    /**
     * This method adds a constant attribute being backed by a single value.
     * 
     * @param name
     *            the name of the new attribute
     * @param value
     *            the constant value
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withConstantNumericAttribute(String name, double numericValue) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        constants.add(new NumericConstantCreation(name, numericValue));
        return this;
    }

    /**
     * This method adds a constant attribute being backed by a single value.
     * 
     * @param name
     *            the name of the new attribute
     * @param value
     *            the constant value
     * @return this transformer for chaining.
     * 
     */
    public ExampleSetTransformer withConstantNominalAttribute(String name, String nominalValue) {
        // We have to check whether this name is actually possible
        if (!containedAttributeNames.add(name)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, name));
        }

        constants.add(new NominalConstantCreation(name, nominalValue));
        return this;
    }

    /**
     * This simply removes an attribute if one is present with the given name.
     * 
     * @param attributeName
     *            name of attribute to remove
     */
    public ExampleSetTransformer withoutAttribute(String attributeName) {
        containedAttributeNames.remove(attributeName);
        removeAttributeNames.add(attributeName);
        return this;
    }

    public ExampleSetTransformer reorderAttributes(List<String> attributeOrder) {
        this.attributeOrder = attributeOrder;
        return this;
    }


    /**
     * Calling this method will cause the resulting example set being sorted by the named attribute
     * and in given direction. If this method is called multiple times, first call specifies most
     * significant attribute.
     * 
     * @param attributeNames
     *            name of the attribute to sort according to
     * @param direction
     *            the direction of sorting
     * @return the tranformer for chaining
     */


    public ExampleSetTransformer sort(SortingDirection direction, String... attributeNames) {
        for (String attributeName : attributeNames) {
            sortAttributeNames.add(attributeName);
            sortDirections.add(direction);
        }
        return this;

    }

    public ExampleSetTransformer renameAttribute(String oldName, String newName) {
        // We have to check whether this renaming is actually possible
        if (!containedAttributeNames.add(newName)) {
            throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, newName));
        }
        containedAttributeNames.remove(oldName);

        this.attributeRenamings.put(oldName, newName);

        return this;
    }

    /**
     * This sets the role of the specified attribute and automatically sets the attribute to
     * special.
     * 
     * @param attributeName
     *            the name of the attribute
     * @param role
     *            the role the attribute should have after transforming or null, if the attribute
     *            should be regular.
     * @return this for chaining
     */
    public ExampleSetTransformer setRole(String attributeName, String role) {
        this.attributeRoles.put(attributeName, role);
        return this;
    }

    /**
     * This copies the role of the specified source attribute to the destination attribute. After
     * this transformation, both the source and destination attributes will have the same role and
     * special state.
     *
     * @param sourceAttributeName
     *            the name of the source attribute
     * @param destinationAttributeName
     *            the name of the destination attribute
     * @return this for chaining
     */
    public ExampleSetTransformer inheritRole(String sourceAttributeName, String destinationAttributeName) {
        this.attributeRolesRemapping.put(sourceAttributeName, destinationAttributeName);
        return this;
    }

    public ExampleSetTransformer shuffle(Random random) {
        this.shuffleRandom = random;
        this.shuffle = true;
        return this;
    }


    /**
     * This method adds this predicate as filter to this transformer. Filters will be executed at
     * first and potentially remove rows. Only rows that pass the filter will be used for
     * transformations.
     * 
     * @param filterFunction
     *            the filter
     * @return this transformer
     */
    public ExampleSetTransformer filter(IntPredicate filterFunction) {
        this.filters.add(filterFunction);
        return this;
    }


    /**
     * This method adds filter to this transformer. Filters will be executed at first and
     * potentially remove rows. Only rows that pass the filter will be used for transformations.
     * 
     * @param invert
     *            only rows that don't pass the filters are kept
     * @param filters
     *            the filters
     * @return this transformer
     *
     *         TODO: This needs object creation and should be avoided. Instead
     *         {@link ExampleSetRowFilter} should work directly on the row index.
     */
    public ExampleSetTransformer filter(boolean invert, ExampleSetRowFilter... filters) {
        this.filters.add(row -> {
            boolean pass = true;
            for (int i = 0; i < filters.length && pass; i++) {
                try {
                    pass = filters[i].pass(row, sourceSet);
                } catch (ExpressionEvaluationException e) {
                    this.exception = e;
                }
            }
            return pass ^ invert;
        });
        return this;
    }


    /**
     * This method will filter the underlying ExampleSet according to the given partition. All
     * activated partitions will be part of the transformed exampleset. Will be performed before
     * transformations.
     * 
     * @param partition
     *            the partition to apply
     * @return this transformer
     */
    public ExampleSetTransformer filter(Partition partition) {
        this.filters.add(partition::isSelected);
        return this;
    }

    /**
     * This method allows to reorder the underlying example set according to the given function. The
     * resulting exampleSet's x-th row will be the reorderFunction(x)-th row of the underlying
     * example set. <br>
     * This also allows to duplicate rows. <br>
     * Only one reorder call per transformation is supported!
     * 
     * @param newSize
     *            the size of the new example set. Can be larger of the underlying set, but
     *            reorderFunction must supply valid values for each integer up to newSize
     *            (exclusive).
     * @param reorderFunction
     *            A function returning a valid value between 0 and size-1 of the underlying
     *            exampleSet for each value between 0 and newSize-1.
     * @return this transformer
     */
    public ExampleSetTransformer reorder(int newSize, IntUnaryOperator reorderFunction) {
        if (this.reorderFunction != null)
            throw new IllegalStateException("Already registered a reorder on this transformer. Only one reordering is supported.");
        this.reorderFunction = reorderFunction;
        this.reorderSize = newSize;
        return this;
    }

    /**
     * Derive a new name for the attribute with a specifier based on best practices. The best
     * practice for attribute names is: (Namespace::)AttributeName(_Specifier1Specifier2)
     * 
     * @param attributeName
     *            the name of the original attribute
     * @param specifier
     *            the specifier
     * @return a new derived name
     */
    public static String getDerivedAttributeName(String attributeName, String specifier) {
        if (specifier == null || specifier.isBlank())
            throw new IllegalArgumentException("specifier must not be null or a blank string");
        int lastUnderscoreIndex = attributeName.lastIndexOf('_');
        if (lastUnderscoreIndex > 0) {
            return attributeName + specifier;
        } else {
            return attributeName + '_' + specifier;
        }
    }

    public ExampleSetTransformer addCleanUpOperation(Runnable r) {
        this.cleanUpOperations.add(r);
        return this;
    }


    /**
     * This method performs the actual transformation of all scheduled single transformations.
     * Before this is called, no changes will be performed.<br>
     * First all filter will be applied. Then the ordering will be determined by sort, shuffle or
     * reorder transformations. Only then the actual new attributes will be derived.<br>
     * Finally the new exampleSet will be created without the removed attributes.
     * 
     * @return the transformed result.
     * 
     *         thrown if any problems occur
     */
    public ExampleSet transform() throws OperatorException {
        try {
            ExampleSet set = sourceSet;
            int size = set.size;

            // first we start with everything relating to changing the access indices in the column
            // groups
            {
                int[][] columnGroups = sourceSet.columnGroups;
                int numberOfColumnGroups = columnGroups.length;
                // filtering
                if (!filters.isEmpty()) {
                    boolean[] keepRow = new boolean[size];
                    IntPredicate[] filtersArray = filters.toArray(IntPredicate[]::new);
                    IntStream.range(0, size).parallel().forEach(row -> {
                        boolean keep = true;
                        for (IntPredicate intPredicate : filtersArray) {
                            keep &= intPredicate.test(row);
                        }
                        keepRow[row] = keep;
                    });

                    // check for errors
                    if (exception != null)
                        throw exception;

                    // count new rows to size new columnGroups
                    int newSize = 0;
                    for (int i = 0; i < size; i++)
                        if (keepRow[i])
                            newSize++;
                    // create new groups
                    int[][] oldColumnGroups = columnGroups;
                    int[][] newColumnGroups = new int[numberOfColumnGroups][newSize];
                    int newRow = 0;
                    for (int oldRow = 0; oldRow < size; oldRow++) {
                        if (keepRow[oldRow]) {
                            for (int groupIndex = 0; groupIndex < numberOfColumnGroups; groupIndex++) {
                                newColumnGroups[groupIndex][newRow] = oldColumnGroups[groupIndex][oldRow];
                            }
                            newRow++;
                        }
                    }

                    columnGroups = newColumnGroups;
                    size = newSize;
                }
                // now do the potential reordering
                if (reorderFunction != null) {
                    int[][] oldColumnGroups = columnGroups;
                    int[][] newColumnGroups = new int[numberOfColumnGroups][reorderSize];
                    for (int newRow = 0; newRow < reorderSize; newRow++) {
                        int oldRowIndex = reorderFunction.applyAsInt(newRow);
                        for (int groupIndex = 0; groupIndex < numberOfColumnGroups; groupIndex++) {
                            newColumnGroups[groupIndex][newRow] = oldColumnGroups[groupIndex][oldRowIndex];
                        }
                    }

                    columnGroups = newColumnGroups;
                    size = reorderSize;
                }

                // now do the potential shuffling
                if (shuffle) {
                    int[][] newColumnGroups;
                    // avoid creation of new array if unnecessary
                    if (columnGroups == sourceSet.columnGroups) {
                        newColumnGroups = new int[numberOfColumnGroups][size];
                        for (int i = 0; i < numberOfColumnGroups; i++) {
                            newColumnGroups[i] = new int[columnGroups[i].length];
                            System.arraycopy(columnGroups[i], 0, newColumnGroups[i], 0, columnGroups[i].length);
                        }
                    } else {
                        newColumnGroups = columnGroups;
                    }

                    // do shuffling of indices on array in place
                    for (int i = size; i > 1; i--) {
                        int a = i - 1;
                        int b = shuffleRandom.nextInt(i);
                        for (int groupIndex = 0; groupIndex < numberOfColumnGroups; groupIndex++) {
                            int swapValue = newColumnGroups[groupIndex][a];
                            newColumnGroups[groupIndex][a] = newColumnGroups[groupIndex][b];
                            newColumnGroups[groupIndex][b] = swapValue;
                        }
                    }
                    columnGroups = newColumnGroups;
                }


                // sorting
                if (!sortAttributeNames.isEmpty()) {
                    set = new ExampleSet(set, columnGroups);
                    Attribute[] sortAttribute = sortAttributeNames.stream().map(set.getAttributes()::get).toArray(Attribute[]::new);
                    SortingDirection[] sortDirection = sortDirections.toArray(SortingDirection[]::new);
                    // identity array to sort
                    int[] sorting = new int[size];
                    for (int i = 0; i < size; i++) {
                        sorting[i] = i;
                    }

                    IntComparator comparator = creatorComparator(set, sortAttribute[0], sortDirection[0]);
                    for (int i = 1; i < sortAttribute.length; i++) {
                        comparator = comparator.thenComparing(creatorComparator(set, sortAttribute[i], sortDirection[i]));
                    }
                    comparator = comparator.thenComparing(Integer::compare);
                    IntArrays.parallelQuickSort(sorting, comparator);

                    // now reorder column groups
                    int[][] oldColumnGroups = columnGroups;
                    int[][] newColumnGroups = new int[numberOfColumnGroups][size];
                    for (int newRow = 0; newRow < size; newRow++) {
                        for (int groupIndex = 0; groupIndex < numberOfColumnGroups; groupIndex++) {
                            newColumnGroups[groupIndex][newRow] = oldColumnGroups[groupIndex][sorting[newRow]];
                        }
                    }

                    columnGroups = newColumnGroups;
                }

                // if filtering changed, we create new set reflecting the view
                if (columnGroups != sourceSet.columnGroups)
                    set = new ExampleSet(sourceSet, columnGroups);
            }


            // now do transformations to add new columns
            List<ColumnDescription> createdAttributes = derivations.stream().parallel().flatMap(t -> t.apply(sourceSet)).collect(Collectors.toList());

            // and finally create constants
            List<ColumnDescription> createdConstants = constants.stream().map(ConstantCreation::apply).collect(Collectors.toList());


            // create final set
            Stream<Attribute> finalAttributeStream = set.attributes.streamAttributes().filter(a -> !removeAttributeNames.contains(a.getName()));
            if (attributeOrder != null)
                finalAttributeStream = finalAttributeStream.filter(a -> attributeOrder.contains(a.getName()))
                        .sorted((a1, a2) -> Integer.compare(attributeOrder.indexOf(a1.getName()), attributeOrder.indexOf(a2.getName())));
            Attribute[] keptAttributes = finalAttributeStream.toArray(Attribute[]::new);
            int newNumberOfAttributes = keptAttributes.length + createdAttributes.size() + createdConstants.size();
            String[] attributeNames = new String[newNumberOfAttributes];
            ValueType[] attributeType = new ValueType[newNumberOfAttributes];
            String[] attributeRoles = new String[newNumberOfAttributes];
            boolean[] isSpecial = new boolean[newNumberOfAttributes];
            DataColumn[] dataColumns = new DataColumn[newNumberOfAttributes];
            int[][] attributesColumnGroup = new int[newNumberOfAttributes][];


            // copying kept attributes
            for (int i = 0; i < keptAttributes.length; i++) {
                attributeNames[i] = keptAttributes[i].name;
                attributeType[i] = keptAttributes[i].valueType;
                attributeRoles[i] = keptAttributes[i].getRole();
                isSpecial[i] = keptAttributes[i].isSpecial();
                dataColumns[i] = set.dataColumns[keptAttributes[i].attributeIndex];
                attributesColumnGroup[i] = set.attributesColumnGroup[keptAttributes[i].attributeIndex];
            }

            // adding new attributes
            if (!createdAttributes.isEmpty()) {
                createdAttributes.sort(Comparator.comparing(o -> o.name));
                int[] identityColumnGroup = new int[set.size()];
                for (int i = 0; i < identityColumnGroup.length; i++)
                    identityColumnGroup[i] = i;
                int i = keptAttributes.length;
                for (ColumnDescription description : createdAttributes) {
                    attributeNames[i] = description.name;
                    attributeType[i] = description.valueType;
                    attributeRoles[i] = null;
                    isSpecial[i] = false;
                    dataColumns[i] = description.column;
                    attributesColumnGroup[i] = identityColumnGroup;
                    i++;
                }
            }

            // adding constants
            if (!createdConstants.isEmpty()) {
                createdConstants.sort(Comparator.comparing(o -> o.name));
                // we search if we already have a constant column group
                int[] constantColumnGroup = null;
                for (int columnIndex = 0; columnIndex < set.dataColumns.length; columnIndex++) {
                    // check whether it is a constant column: Always if it has only a single entry
                    if (set.dataColumns[columnIndex].size == 1) {
                        constantColumnGroup = set.attributesColumnGroup[columnIndex];
                    }
                }
                // check whether we already had one or need to find a new one.
                if (constantColumnGroup == null) {
                    constantColumnGroup = new int[set.size()];// array is automatically 0
                                                              // everywhere
                }
                int i = keptAttributes.length + createdAttributes.size();
                for (ColumnDescription description : createdConstants) {
                    attributeNames[i] = description.name;
                    attributeType[i] = description.valueType;
                    attributeRoles[i] = null;
                    isSpecial[i] = false;
                    dataColumns[i] = description.column;
                    attributesColumnGroup[i] = constantColumnGroup;
                    i++;
                }
            }

            // remap roles
            for (String sourceAttributeName : this.attributeRolesRemapping.keySet()) {
                // Since the source attribute may get removed by prior transformations
                // we copy the role from the original set
                Attribute sourceAttribute = set.getAttributes().get(sourceAttributeName);
                String sourceRole = sourceAttribute.getRole();
                boolean sourceIsSpecial = sourceAttribute.isSpecial();
                // copy role to destination of transformer result
                String destinationAttributeName = this.attributeRolesRemapping.get(sourceAttributeName);
                for (int destinationIndex = 0; destinationIndex < attributeNames.length; destinationIndex++) {
                    if (attributeNames[destinationIndex].equals(destinationAttributeName)) {
                        attributeRoles[destinationIndex] = sourceRole;
                        isSpecial[destinationIndex] = sourceIsSpecial;
                        break;
                    }
                }
            }

            // renaming attributes
            for (int i = 0; i < attributeNames.length; i++) {
                attributeNames[i] = attributeRenamings.getOrDefault(attributeNames[i], attributeNames[i]);
            }

            // Setting roles
            for (int i = 0; i < attributeNames.length; i++) {
                // reset role of source attributes if duplicate role is specified
                String currentRole = attributeRoles[i];
                if (currentRole != null && this.attributeRoles.containsValue(currentRole)) {
                    attributeRoles[i] = null;
                    isSpecial[i] = false;
                }
                // set role
                if (this.attributeRoles.containsKey(attributeNames[i])) {
                    String targetRole = this.attributeRoles.get(attributeNames[i]);
                    attributeRoles[i] = targetRole;
                    isSpecial[i] = targetRole != null;
                }
            }

            return new ExampleSet(attributeNames, attributeType, dataColumns, attributeRoles, isSpecial, attributesColumnGroup, size);
        } finally

        {
            // perform clean-ups
            for (Runnable r : cleanUpOperations)
                r.run();
        }
    }

    /**
     * Returns whether the given name will be contained in the target example set.
     * 
     * @param name
     *            of the attribute
     * @return true if this will be contained with the current configuration of this transformer
     */
    public boolean willContainAttribute(String name) {
        return containedAttributeNames.contains(name);
    }


    private IntComparator creatorComparator(ExampleSet set, Attribute attribute, SortingDirection sortingDirection) {
        int attributeIndex = attribute.getIndex();
        int direction = sortingDirection == SortingDirection.INCREASING ? 1 : -1;
        return switch (attribute.getValueType()) {
            case NOMINAL -> (x, y) -> ObjectUtils.compare(set.getNominalValue(x, attributeIndex), set.getNominalValue(y, attributeIndex)) * direction;
            case NUMERIC -> (x, y) -> Double.compare(set.getNumericValue(x, attributeIndex), set.getNumericValue(y, attributeIndex)) * direction;
            case TIMESTAMP -> (x, y) -> Long.compare(set.getTimestampValue(x, attributeIndex), set.getTimestampValue(y, attributeIndex)) * direction;
        };
    }


}
