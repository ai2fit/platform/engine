package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.LongToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class TimestampToNumericPointTransformation extends AbstractPointTransformation {

    private LongToDoubleFunction pointDerivation;

    public TimestampToNumericPointTransformation(String newAttributeName, String sourceAttributeName, LongToDoubleFunction derive) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.pointDerivation = derive;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNumericValue(row, pointDerivation.applyAsDouble(sourceSet.getTimestampValue(row, attributeIndex))));

    }
}
