package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.LongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class TimestampToNominalPointTransformation extends AbstractPointTransformation {

    private final LongFunction<String> pointDerivation;

    public TimestampToNominalPointTransformation(String newAttributeName, String sourceAttributeName, LongFunction<String> derive) {
        super(newAttributeName, ValueType.NOMINAL, sourceAttributeName);
        this.pointDerivation = derive;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNominalValue(row, pointDerivation.apply(sourceSet.getTimestampValue(row, attributeIndex))));

    }
}
