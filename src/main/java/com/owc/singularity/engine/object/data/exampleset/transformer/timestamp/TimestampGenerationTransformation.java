package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.IntToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractGenerationTransformation;

public final class TimestampGenerationTransformation extends AbstractGenerationTransformation {

    private final IntToLongFunction function;

    public TimestampGenerationTransformation(String attributeName, IntToLongFunction function) {
        super(attributeName, ValueType.TIMESTAMP);
        this.function = function;
    }

    @Override
    protected void apply(DataColumn dataColumn, int size) {
        IntStream.range(0, size).parallel().forEach(row -> dataColumn.setTimestampValue(row, function.applyAsLong(row)));
    }
}
