package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.ToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.Example;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class TimestampExampleTransformation extends AbstractRowTransformation {

    private final ToLongFunction<Example> derive;

    public TimestampExampleTransformation(String newAttributeName, ToLongFunction<Example> derive) {
        super(newAttributeName, ValueType.TIMESTAMP);
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> dataColumn.setTimestampValue(row, derive.applyAsLong(sourceSet.getExample(row))));
    }

}
