package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.DoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NumericToNominalPointTransformation extends AbstractPointTransformation {

    private DoubleFunction<String> pointDerivation;

    public NumericToNominalPointTransformation(String newAttributeName, String sourceAttributeName, DoubleFunction<String> pointDerivation) {
        super(newAttributeName, ValueType.NOMINAL, sourceAttributeName);
        this.pointDerivation = pointDerivation;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNominalValue(row, pointDerivation.apply(sourceSet.getNumericValue(row, attributeIndex))));

    }
}
