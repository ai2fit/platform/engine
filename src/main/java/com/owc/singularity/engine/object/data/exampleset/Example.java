/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;

import com.owc.singularity.engine.tools.Tools;


/**
 * An example is only a reference to a certain row in a certain {@link ExampleSet}. It is here for
 * compatibility reasons and shouldn't be used anymore to avoid object creation per value request.
 * 
 * @author Sebastian Land
 */
public class Example implements Serializable {

    private static final long serialVersionUID = 7761687908683290928L;


    /** The parent example set holding all attribute information for this data row. */
    private ExampleSet parentExampleSet;


    private int row;

    public Example(ExampleSet parentExampleSet, int row) {
        this.parentExampleSet = parentExampleSet;
        this.row = row;
    }


    /**
     * Returns the nominal value for the given attribute. Attribute MUST be of type nominal.
     * Otherwise, system might crash. No checks performed due to performance reasons.
     */
    public String getNominalValue(Attribute a) {
        return parentExampleSet.getNominalValue(row, a.attributeIndex);
    }

    /**
     * Returns the numerical value for the given attribute. Attribute MUST be of type numeric.
     * Otherwise, system might crash. No checks performed due to performance reasons.
     *
     */
    public double getNumericValue(Attribute a) {
        return parentExampleSet.getNumericValue(row, a.attributeIndex);
    }

    /**
     * Returns the numerical value for the given attribute. Attribute MUST be of type timestamp.
     * Otherwise, system might crash. No checks performed due to performance reasons.
     */
    public long getTimestampValue(Attribute a) {
        return parentExampleSet.getTimestampValue(row, a.attributeIndex);
    }

    /**
     * Delivers the attributes. Should not be necessary to access attributes PER example. Will
     * anyway have strong performance effects. TODO: Replace and remove from here
     */
    @Deprecated
    public Attributes getAttributes() {
        return this.parentExampleSet.getAttributes();
    }

    /**
     * TODO: Inline
     */
    @Deprecated
    public Date getDateValue(Attribute a) {
        return new Date(parentExampleSet.getTimestampValue(row, a.attributeIndex));
    }

    /**
     * Returns a dense string representation with all possible fraction digits. Nominal values will
     * be quoted with double quotes.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<Attribute> iterator = parentExampleSet.getAttributes().allAttributes();
        while (iterator.hasNext()) {
            Attribute attribute = iterator.next();
            if (first) {
                first = false;
            } else {
                result.append(", ");
            }
            switch (attribute.getValueType()) {
                case NOMINAL:
                    result.append("\"");
                    result.append(getNominalValue(attribute));
                    result.append("\"");
                    break;
                case NUMERIC:
                    result.append(getNumericValue(attribute));
                    break;
                case TIMESTAMP:
                    result.append(Tools.formatDateAsUserSpecified(getTimestampValue(attribute)));
                    break;
                default:
                    break;

            }
        }
        return result.toString();
    }
}
