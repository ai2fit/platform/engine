/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serial;
import java.util.Objects;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.operator.tools.ExpressionEvaluationException;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.expression.ExampleResolver;
import com.owc.singularity.engine.tools.expression.Expression;
import com.owc.singularity.engine.tools.expression.ExpressionException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.internal.ExpressionParserUtils;


/**
 * The condition is fulfilled if the expression evaluates to <code>true</code>. Respectively the
 * condition is not fulfilled if the expression evaluates to <code>false</code> or <code>null</code>
 * .
 *
 * @author Marco Boeck
 */
public class ExpressionFilter implements ExampleSetRowFilter {

    @Serial
    private static final long serialVersionUID = -8663210021090219277L;

    private transient ExampleResolver resolver;

    private transient ExpressionType type;

    private transient Expression expression;

    private final String expressionString;

    private Operator operator;

    /**
     * Creates a new {@link ExpressionFilter} instance with the given expression. The expression is
     * evaluated via the expression parser and examples are ok if the expression evaluates to
     * <code>true</code>.
     * 
     * @param expression
     *            the expression to use
     * @param operator
     * 
     */
    public ExpressionFilter(String expression, Operator operator) {
        if (expression == null) {
            throw new IllegalArgumentException("expression must not be null!");
        }
        this.expressionString = expression;
        this.operator = operator;
    }


    @Override
    public String toString() {
        return expressionString;
    }

    /** Returns true if all conditions are fulfilled for the given example. */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) throws ExpressionEvaluationException {
        try {
            resolver.bind(exampleSet.getExample(row));

            if (type == ExpressionType.BOOLEAN) {
                Boolean resultValue = expression.evaluateBoolean();
                return Objects.requireNonNullElse(resultValue, false);
            } else if (type == ExpressionType.DOUBLE) {
                double resultValue = expression.evaluateNumeric();
                if (resultValue == 1d || resultValue == 0d) {
                    return resultValue == 1d;
                }
            }
            throw new ExpressionEvaluationException(I18N.getErrorMessageOrNull("expression_filter.expression_not_boolean", expressionString));
        } catch (ExpressionException e1) {
            // all parsing tries failed, show warning and return false
            throw new ExpressionEvaluationException(I18N.getErrorMessageOrNull("expression_filter.parser_parsing_failed", expressionString));
        } finally {
            // avoid memory leak
            resolver.unbind();
        }
    }


    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        this.resolver = new ExampleResolver(exampleSet);
        try {
            this.expression = ExpressionParserUtils.createAllModulesParser(operator, resolver).parse(expressionString);
        } catch (ExpressionException e) {
            throw ExpressionParserUtils.convertToUserError(operator, expressionString, e);
        }
        this.type = expression.getExpressionType();

    }

}
