package com.owc.singularity.engine.object.io;


import java.io.*;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.tools.Tools;

public abstract class AbstractTemporaryFileObject extends FileObject {

    private static final String TEMP_FILE_PREFIX = "rm_fileobject_";
    private static final String TEMP_FILE_SUFFIX = ".temp";

    private File temporaryFile = null;

    @Override
    public final InputStream openStream() throws OperatorException {
        if (temporaryFile != null) {
            try {
                return new FileInputStream(temporaryFile);
            } catch (FileNotFoundException e) {
                throw new OperatorException("303", e, temporaryFile, e.getMessage());
            }
        } else {
            return openSourceStream();
        }
    }

    @Override
    public final File getFile() throws OperatorException {
        if (temporaryFile == null) {
            try {
                temporaryFile = File.createTempFile(TEMP_FILE_PREFIX, TEMP_FILE_SUFFIX);
                temporaryFile.deleteOnExit();
            } catch (IOException e) {
                throw new OperatorException("303", e, "File in " + System.getProperty("java.io.tmpdir"), e.getMessage());
            }
            try (FileOutputStream fos = new FileOutputStream(temporaryFile); InputStream in = openSourceStream();) {
                Tools.copyStreamSynchronously(in, fos, true);
            } catch (IOException e) {
                throw new OperatorException("303", e, temporaryFile, e.getMessage());
            }
        }
        return temporaryFile;
    }

    @Override
    public final long getLength() throws OperatorException {
        return getFile().length();
    }

    /**
     * This method reads in the data from the original source. If a temporary file needs to be
     * created in between, it will read from the source and store the result into a temporary file.
     * The {@link #openStream()} method will then read from this file.
     * 
     * @return
     * @throws OperatorException
     */
    public abstract InputStream openSourceStream() throws OperatorException;

}
