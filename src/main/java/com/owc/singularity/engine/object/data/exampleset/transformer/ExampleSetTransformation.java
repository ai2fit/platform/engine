package com.owc.singularity.engine.object.data.exampleset.transformer;

import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;

public interface ExampleSetTransformation {

    Stream<ColumnDescription> apply(ExampleSet sourceSet);
}
