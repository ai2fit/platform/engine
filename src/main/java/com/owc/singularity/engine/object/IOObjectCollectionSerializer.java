package com.owc.singularity.engine.object;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.repository.serialization.DefinesSerializer;
import com.owc.singularity.repository.serialization.SerializationService;
import com.owc.singularity.repository.serialization.Serializer;

/**
 * On disk layout uses zip plus headers. Looks like:
 * <ul>
 * <li>INT: Number of entries in collection</li>
 * <li>ZIP: One numbered entry per object in the collection.
 * <ul>
 * <li>INT: Number of bytes for class name of object</li>
 * <li>STRING: UTF-8 String of above byte length for class name</li>
 * <li>BYTE: single byte for serialization version of object</li>
 * <li>PAYLOAD: An arbitrary number of bytes per entry</li>
 * </ul>
 * </li>
 * </ul>
 * 
 * @author Sebastian Land
 *
 */
@DefinesSerializer(supportedClass = IOObjectCollection.class)
public class IOObjectCollectionSerializer implements Serializer {

    @Override
    public void serialize(Object object, OutputStream out) throws IOException {
        IOObjectCollection<?> collection = (IOObjectCollection<?>) object;

        out.write(intToByteArray(collection.size()));
        ZipOutputStream zipOutputStream = new ZipOutputStream(out);
        zipOutputStream.setLevel(0);

        for (int i = 0; i < collection.size(); i++) {
            zipOutputStream.putNextEntry(new ZipEntry(Integer.toString(i)));
            IOObject collectionObject = collection.getElement(i, false);
            byte[] classNameBytes = collectionObject.getClass().getName().getBytes(StandardCharsets.UTF_8);
            zipOutputStream.write(intToByteArray(classNameBytes.length));
            zipOutputStream.write(classNameBytes);
            zipOutputStream.write(SerializationService.getVersion(object));
            SerializationService.serializeObject(collectionObject, zipOutputStream);
            zipOutputStream.closeEntry();
        }
        zipOutputStream.flush();
    }

    @Override
    public Object deserialize(InputStream in, Class<?> targetClass, ClassLoader classLoader, byte version) throws IOException {
        byte[] intBuffer = new byte[4];
        in.readNBytes(intBuffer, 0, 4);
        int numberOfElements = byteArrayToInt(intBuffer);
        ArrayList<IOObject> objects = new ArrayList<>(numberOfElements);
        ZipInputStream zipInputStream = new ZipInputStream(in);
        try {
            for (int i = 0; i < numberOfElements; i++) {
                // start reading next entry
                zipInputStream.getNextEntry();
                // entry starts with byte length of class name
                zipInputStream.readNBytes(intBuffer, 0, 4);
                int classNameLength = byteArrayToInt(intBuffer);
                // read class name
                String className = new String(zipInputStream.readNBytes(classNameLength), StandardCharsets.UTF_8);
                // read version
                byte objectSerializationVersion = (byte) zipInputStream.read();
                // deserialize object with remainder of entry
                objects.add((IOObject) SerializationService.deserializeObject(Class.forName(className, true, ModuleService.getMajorClassLoader()),
                        ModuleService.getMajorClassLoader(), zipInputStream, objectSerializationVersion));
            }
        } catch (ClassNotFoundException e) {
            throw new IOException("Cannot load Object Collection as element is of unknown class.", e);
        }
        return new IOObjectCollection<>(objects);
    }

    public static final int byteArrayToInt(byte[] bytes) {
        return ((bytes[0] & 0xFF) << 24) | ((bytes[1] & 0xFF) << 16) | ((bytes[2] & 0xFF) << 8) | ((bytes[3] & 0xFF) << 0);
    }

    public static final byte[] intToByteArray(int value) {
        return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value };
    }

    @Override
    public byte getCurrentVersion() {
        return 2;
    }
}
