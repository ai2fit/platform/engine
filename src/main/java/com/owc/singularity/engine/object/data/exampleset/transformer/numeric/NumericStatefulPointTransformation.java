package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.ObjDoubleConsumer;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NumericStatefulPointTransformation<S> extends AbstractPointTransformation {

    private final Supplier<S> stateSupplier;
    private final ObjDoubleConsumer<S> stateUpdate;
    private final ToDoubleFunction<S> valueDerivationFunction;
    private S state;

    public NumericStatefulPointTransformation(String newAttributeName, String sourceAttributeName, Supplier<S> stateSupplier, ObjDoubleConsumer<S> stateUpdate,
            ToDoubleFunction<S> valueDerivationFunction) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.stateSupplier = stateSupplier;
        this.stateUpdate = stateUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        this.state = stateSupplier.get();
        for (int i = 0; i < sourceSet.size(); i++) {
            stateUpdate.accept(state, sourceSet.getNumericValue(i, attributeIndex));
            dataColumn.setNumericValue(i, valueDerivationFunction.applyAsDouble(state));
        }
    }
}
