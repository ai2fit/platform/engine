/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.io;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.owc.singularity.engine.object.AbstractIOObject;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.tools.Tools;


/**
 *
 * This class represents buffers, files or streams that can be parsed by Operators.
 *
 * @author Nils Woehler, Marius Helf
 *
 */
public abstract class FileObject extends AbstractIOObject {

    private static final long serialVersionUID = 1L;

    /**
     * Open Stream to read data in this Object.
     *
     * @throws OperatorException
     */
    public abstract InputStream openStream() throws OperatorException;

    /**
     * Returns the data as a file. Maybe slow if underlying implementation needs to copy the data
     * into the file first. This file should be used only for reading. Writing to this file has an
     * undefined effect.
     *
     * @throws OperatorException
     */
    public abstract File getFile() throws OperatorException;

    /**
     * Returns the size of the related file in number of bytes.
     *
     * @throws OperatorException
     */
    public abstract long getLength() throws OperatorException;

    @Override
    public String toResultString() {
        try (InputStream in = openStream()) {
            return new String(in.readNBytes(100 * 1024), StandardCharsets.UTF_8);

        } catch (OperatorException | IOException e) {
            return e.getMessage() + Tools.getLineSeparator() + ExceptionUtils.getStackTrace(e);
        }
    }
}
