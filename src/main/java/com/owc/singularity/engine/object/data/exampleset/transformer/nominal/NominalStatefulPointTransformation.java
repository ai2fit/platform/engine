package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NominalStatefulPointTransformation<S> extends AbstractPointTransformation {

    private Supplier<S> stateSupplier;
    private BiConsumer<S, String> stateUpdate;
    private Function<S, String> valueDerivationFunction;
    private S state;

    public NominalStatefulPointTransformation(String newAttributeName, String sourceAttributeName, Supplier<S> stateSupplier, BiConsumer<S, String> stateUpdate,
            Function<S, String> valueDerivationFunction) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.stateSupplier = stateSupplier;
        this.stateUpdate = stateUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        this.state = stateSupplier.get();
        for (int i = 0; i < sourceSet.size(); i++) {
            stateUpdate.accept(state, sourceSet.getNominalValue(i, attributeIndex));
            dataColumn.setNominalValue(i, valueDerivationFunction.apply(state));
        }
    }
}
