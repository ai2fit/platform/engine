/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.object;


import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.repository.connection.AbstractConnectionParameterDefinition;
import com.owc.singularity.repository.connection.ConnectionDefinitionRegistry;
import com.owc.singularity.repository.connection.ConnectionParameterDefinition;
import com.owc.singularity.repository.connection.ConnectionParameters;


/**
 * Container to pass the {@link ConnectionParameters} around as an
 * {@link com.owc.singularity.engine.object.IOObject IOObject}.
 *
 * @author Hatem Hamad
 * @since 9.3
 */
public class ConnectionParametersIOObject extends AbstractIOObject {

    private static final long serialVersionUID = -4221314813986064810L;

    // the contained connection information
    private final ConnectionParameters parameters;

    /**
     * Set this container up with the given {@link ConnectionParameters}.
     */
    public ConnectionParametersIOObject(ConnectionParameters parameters) {
        this.parameters = parameters;
    }

    /**
     * Access the {@link ConnectionParameters}
     *
     * @return the connection information
     */
    public ConnectionParameters getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        if (parameters == null) {
            return "Empty connection";
        }
        return "Connection: " + parameters.getName() + " of type " + parameters.getTypeId();
    }

    @Override
    public String toResultString() {
        ConnectionParameters configuration = parameters;
        if (configuration == null) {
            return "Empty connection";
        }
        StringBuilder result = new StringBuilder().append("<b>Name:</b> ").append(configuration.getName()).append("<br/><br/><b>Type:</b> ");
        String connectionType = configuration.getTypeId();
        ConnectionParameterDefinition<?> definition = ConnectionDefinitionRegistry.getInstance().getDefinitionOfType(connectionType);
        if (definition instanceof AbstractConnectionParameterDefinition) {
            String icon = ((AbstractConnectionParameterDefinition<?>) definition).getIconName();
            java.net.URL url = Tools.getResource("icons/16/" + icon);
            if (url != null) {
                result.append("<img src=\"").append(url).append("\"/> ");
            }
            result.append(((AbstractConnectionParameterDefinition<?>) definition).getName());
        } else {
            result.append(I18N.getGUILabel("connection.unknown_type.label"));
        }
        result.append("<br/><br/>");
        return result.toString();
    }
}
