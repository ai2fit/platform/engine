/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serial;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.operator.tools.ExpressionEvaluationException;


/**
 * The condition is fulfilled if an attribute has a value equal to, not equal to, less than, ... a
 * given value. This filter can be constructed from several conditions of the class
 * {@link AttributeValueSingleConditionFilter} which must all be fulfilled.
 * 
 * @author Ingo Mierswa
 */
public class AttributeValueFilter implements ExampleSetRowFilter {

    @Serial
    private static final long serialVersionUID = 6977275837081172924L;
    private Operator operator;

    private static final int AND = 0;
    private static final int OR = 1;

    /** The list of all single conditions. */
    private List<AttributeValueSingleConditionFilter> filterConditions = new LinkedList<>();

    private int combinationMode = AND;

    /**
     * Constructs an AttributeValueFilter for a given {@link ExampleSet} from a parameter string
     *
     * @param parameterString
     *            Must be of the form attribute1 R1 value1 RR attribute2 R2 value2 RR ..., where Ri
     *            is one out of =, != or &lt;&gt;, &lt;, &gt;, &lt;=, and &gt;= and all RR must be
     *            either || for OR or && for AND.
     */
    public AttributeValueFilter(String parameterString) throws UserError {
        if ((parameterString == null) || (parameterString.isBlank())) {
            throw new IllegalArgumentException("Parameter string must not be empty!");
        }

        String[] splitted = parameterString.split("\\|\\|");
        if (splitted.length > 1) {
            for (String condition : splitted) {
                condition = condition.trim();
                try {
                    addCondition(new AttributeValueSingleConditionFilter(condition));
                } catch (IllegalArgumentException e) {
                    throw new UserError(operator, e, 207, parameterString, "AttributeValueFilter", e.getMessage());
                }
            }
            this.combinationMode = OR;
        } else {
            splitted = parameterString.split("\\&\\&");
            if (splitted.length > 1) {
                for (String condition : splitted) {
                    condition = condition.trim();
                    try {
                        addCondition(new AttributeValueSingleConditionFilter(condition));
                    } catch (IllegalArgumentException e) {
                        throw new UserError(operator, e, 207, condition, "AttributeValueFilter", e.getMessage());
                    }
                }
                this.combinationMode = AND;
            } else {
                try {
                    addCondition(new AttributeValueSingleConditionFilter(parameterString));
                } catch (IllegalArgumentException e) {
                    throw new UserError(operator, e, 207, parameterString, "AttributeValueFilter", e.getMessage());
                }
                this.combinationMode = AND;
            }
        }
    }

    /**
     * Creates a new AttributeValueFilter. If attribute is not nominal, value must be a number.
     */
    public AttributeValueFilter(String attributeName, int comparisonType, String value) {
        addCondition(attributeName, comparisonType, value);
    }

    private void addCondition(String attributeName, int comparisonType, String value) {
        addCondition(new AttributeValueSingleConditionFilter(attributeName, comparisonType, value));
    }

    private void addCondition(AttributeValueSingleConditionFilter condition) {
        filterConditions.add(condition);
    }

    @Override
    public String toString() {
        return filterConditions.toString();
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        for (AttributeValueSingleConditionFilter filter : filterConditions)
            filter.prepare(exampleSet);
    }

    /** Returns true if all conditions are fulfilled for the given example. */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) throws ExpressionEvaluationException {
        for (AttributeValueSingleConditionFilter condition : filterConditions) {
            if (combinationMode == AND) {
                if (!condition.pass(row, exampleSet)) {
                    return false;
                }
            } else {
                if (condition.pass(row, exampleSet)) {
                    return true;
                }
            }
        }
        return combinationMode == AND;
    }
}
