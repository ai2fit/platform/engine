package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractGenerationTransformation;

public final class NumericGenerationTransformation extends AbstractGenerationTransformation {

    private final IntToDoubleFunction function;

    public NumericGenerationTransformation(String attributeName, IntToDoubleFunction function) {
        super(attributeName, ValueType.NUMERIC);
        this.function = function;
    }

    @Override
    protected void apply(DataColumn dataColumn, int size) {
        IntStream.range(0, size).parallel().forEach(row -> dataColumn.setNumericValue(row, function.applyAsDouble(row)));
    }
}
