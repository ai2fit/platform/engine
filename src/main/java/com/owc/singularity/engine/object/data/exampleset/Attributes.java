/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Attributes implements Iterable<Attribute>, Cloneable, Serializable {

    /** The name of the confidence special attributes. */
    public static final String CONFIDENCE_NAME = "confidence";

    /** The name of regular attributes. */
    public static final String ATTRIBUTE_NAME = "attribute";

    /** The name of the special attribute id. */
    public static final String ID_NAME = "id";

    /** The name of the special attribute label. */
    public static final String LABEL_NAME = "label";

    /** The name of the special attribute prediction. */
    public static final String PREDICTION_NAME = "prediction";

    /** The name of the special attribute cluster. */
    public static final String CLUSTER_NAME = "cluster";

    /** The name of the special attribute weight (example weights). */
    public static final String WEIGHT_NAME = "weight";

    /** The name of the special attribute outlier. */
    public static final String OUTLIER_NAME = "outlier";

    /** The name of the classification cost special attribute. */
    public static final String CLASSIFICATION_COST = "cost";


    /** All known names of regular and special attribute types as an array. */
    public static final String[] KNOWN_ATTRIBUTE_TYPES = new String[] { ATTRIBUTE_NAME, LABEL_NAME, ID_NAME, WEIGHT_NAME, CLUSTER_NAME, PREDICTION_NAME,
            OUTLIER_NAME, CLASSIFICATION_COST };

    private static final long serialVersionUID = 6388263725741578818L;

    Attribute[] attributes;
    String[] attributesRole;
    boolean[] attributesIsSpecial;

    private HashMap<String, Attribute> attributeNameMap;
    private HashMap<String, Attribute> attributeRoleMap;
    private int numberOfRegularAttributes;

    Attributes(ExampleSet exampleSet, String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles, boolean[] attributeIsSpecial) {
        this.attributes = new Attribute[attributeNames.length];
        for (int i = 0; i < attributeNames.length; i++)
            attributes[i] = new Attribute(attributeNames[i], valueTypes[i], exampleSet, i);
        this.attributesRole = attributeRoles;
        this.attributesIsSpecial = attributeIsSpecial;
        this.numberOfRegularAttributes = attributeNames.length;

        attributeNameMap = new HashMap<>();
        attributeRoleMap = new HashMap<>();
        for (int i = 0; i < attributes.length; i++) {
            attributeNameMap.put(attributes[i].getName(), attributes[i]);
            if (attributesRole[i] != null)
                attributeRoleMap.put(attributesRole[i], attributes[i]);
            if (attributeIsSpecial[i])
                numberOfRegularAttributes--;
        }

    }


    @Override
    public Iterator<Attribute> iterator() {
        return IntStream.range(0, attributes.length).filter(i -> !attributesIsSpecial[i]).mapToObj(i -> attributes[i]).iterator();
    }

    public Iterator<Attribute> allAttributes() {
        return Arrays.stream(attributes).iterator();
    }

    public Iterator<Attribute> specialAttributes() {
        return IntStream.range(0, attributes.length).filter(i -> attributesIsSpecial[i]).mapToObj(i -> attributes[i]).iterator();
    }

    /**
     * This returns the attribute with the given name or null if no such attribute exists. This will
     * return attributes independent of their role. To select only regular attributes, use
     * {@link #getRegular(String)}
     * 
     * @param name
     *            name of the attribute
     * @return the attribute or null if not present
     */
    public Attribute get(String name) {
        return attributeNameMap.get(name);
    }

    /**
     * This returns the attribute with the given name if it is a regular attribute. Otherwise it
     * returns null.
     * 
     * @param name
     *            the name of the attribute
     * @return the attribute with the name or null
     */
    public Attribute getRegular(String name) {
        Attribute attribute = attributeNameMap.get(name);
        if (attributesIsSpecial[attribute.attributeIndex])
            return null;
        return attribute;
    }

    /**
     * This returns the attribute with the given role if it exists. Otherwise it returns null.
     * 
     * @param role
     *            of the attribute
     * @return the attribute with the role or null
     */
    public Attribute getAttributeByRole(String role) {
        return attributeRoleMap.get(role);
    }

    public Attribute getLabel() {
        return getAttributeByRole(LABEL_NAME);
    }

    public Attribute getPredictedLabel() {
        return getAttributeByRole(PREDICTION_NAME);
    }


    public Attribute getConfidence(String classLabel) {
        return getAttributeByRole(CONFIDENCE_NAME + "_" + classLabel);
    }


    public Attribute getId() {
        return getAttributeByRole(ID_NAME);
    }

    public Attribute getWeight() {
        return getAttributeByRole(WEIGHT_NAME);
    }


    public Attribute getCluster() {
        return getAttributeByRole(CLUSTER_NAME);
    }


    public Attribute getOutlier() {
        return getAttributeByRole(OUTLIER_NAME);
    }


    public Attribute getCost() {
        return getAttributeByRole(CLASSIFICATION_COST);
    }


    public Attribute[] createRegularAttributeArray() {
        return IntStream.range(0, attributes.length).filter(i -> !attributesIsSpecial[i]).mapToObj(i -> attributes[i]).toArray(Attribute[]::new);
    }

    /** Returns a string representation of this attribute set. */

    @Override
    public String toString() {
        StringBuffer result = new StringBuffer(getClass().getSimpleName() + ": ");
        boolean first = true;
        for (int i = 0; i < attributes.length; i++) {
            if (!first) {
                result.append(", ");
            }
            result.append(attributes[i].getName());
            if (attributesIsSpecial[i]) {
                result.append("[");
                if (attributesRole[i] != null) {
                    result.append("@");
                    result.append(attributesRole[i]);
                }
                result.append("]");
            }
            first = false;
        }
        return result.toString();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Attributes other = (Attributes) obj;
        return Arrays.equals(attributes, other.attributes) && Arrays.equals(attributesIsSpecial, other.attributesIsSpecial)
                && Arrays.equals(attributesRole, other.attributesRole);
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(attributes);
        result = prime * result + Arrays.hashCode(attributesIsSpecial);
        result = prime * result + Arrays.hashCode(attributesRole);
        return result;
    }


    public int size() {
        return numberOfRegularAttributes;
    }


    public int allSize() {
        return attributes.length;
    }


    public int specialSize() {
        return attributes.length - numberOfRegularAttributes;
    }

    public boolean isSpecial(Attribute attribute) {
        return attributesIsSpecial[attribute.attributeIndex];
    }

    /**
     * This streams all attributes
     * 
     * @return
     */
    public Stream<Attribute> streamAttributes() {
        return Arrays.stream(attributes);
    }

    public boolean contains(String name) {
        return attributeNameMap.containsKey(name);
    }

    /**
     * Checks whether this attributes contain a role with the given name.
     * 
     * @param role
     *            the role to search for
     * @return true if role is contained
     */
    public boolean containsRole(String role) {
        return attributeRoleMap.containsKey(role);
    }

}
