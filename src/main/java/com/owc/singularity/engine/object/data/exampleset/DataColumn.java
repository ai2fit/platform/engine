package com.owc.singularity.engine.object.data.exampleset;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.*;

/**
 * <p>
 * This is the back end for data storage which supports a first writing (and reading) phase and
 * then, when writing is finished by calling {@link #finishWriting(int)}, it gets into a second,
 * read only phase. Get methods are only exposed to the package for usage as backend for
 * {@link ExampleSet}s. It is to avoid that this class is in any way part of the api and should only
 * be used by the example set implementation. Unfortunately the writing is needed in
 * subpackages.<br>
 * The DataColumn will allocate memory from the operating system to fit the data. Depending on the
 * given {@link ValueType} the size might change. It offers get and set methods for each type. Only
 * get and set methods of the actual type will work, otherwise the jvm might crash entirely. <br>
 * This class supports a development mode using {@link #DEVELOPMENT_MODE}. Then type checks are
 * performed before access, causing runtime disadvantages but easing the developers life.
 * </p>
 * <p>
 * Each DataColumn will have an additional missing value beyond it's data size. This can be used
 * when an missing value needs to be introduced but not data copy should be used. In this situation
 * the access index can simply be set to {@link #getMissingIndex()} to have an missing value.
 * Usefull in Left, Right and outer joins for example.
 * </p>
 * 
 * @author Sebastian Land
 *
 */
public final class DataColumn {

    private static final class DataColumnReference extends PhantomReference<DataColumn> {

        long columnBaseAddress = 0;
        long memorySize = 0;

        public DataColumnReference(DataColumn dataColumn, ReferenceQueue<? super DataColumn> q) {
            super(dataColumn, q);
        }

        public void finalizeColumn() {
            DirectMemoryStatistics.STATISTIC_MEMORY_USED.addAndGet(-memorySize);
            DirectMemoryStatistics.STATISTIC_TOTAL_MEMORY_FREEED.addAndGet(memorySize);
            DirectMemoryStatistics.STATISTIC_CHUNKS_USED.decrementAndGet();
            MemoryUtils.UNSAFE.freeMemory(columnBaseAddress);
        }
    }

    /**
     * This constant is used to include or ignore the type checks when accessing data. In
     * development mode it is very useful to be able to have checks throwing exceptions. If all
     * programming errors are removed, this flag can be set to false. This will not only ignore the
     * checks, but due to its static final constant nature, the compiler is able to remove the
     * entire code branch, hence allowing much more inlining and avoiding clock cycles in the most
     * often called methods.
     */
    public static final boolean DEVELOPMENT_MODE = false;

    private static final ReferenceQueue<DataColumn> referenceQueue = new ReferenceQueue<DataColumn>();
    // This is needed to keep all references for later put them into the reference queue by garbage
    // collection. Otherwise the reference is gc'ed immediately.
    private static final Set<DataColumnReference> references = Collections.synchronizedSet(new HashSet<>());
    static {
        new Timer("Direct Memory Cleaner", true).scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                DataColumnReference referenceFromQueue;
                while ((referenceFromQueue = (DataColumnReference) referenceQueue.poll()) != null) {
                    referenceFromQueue.finalizeColumn();
                    references.remove(referenceFromQueue);
                    referenceFromQueue.clear();
                }
            }
        }, 10000, 2000);
    }

    // we use three different variables to make sure we are accessing the right type and avoiding a
    // sanity check
    long readNominalBaseAddress;
    long readNumericBaseAddress;
    long readTimestampBaseAddress;
    long writeNominalBaseAddress;
    long writeNumericBaseAddress;
    long writeTimestampBaseAddress;

    int size;
    int entryBytes;
    private final DataColumnReference reference;

    final NominalMapping mapping;
    private ValueType type;

    public DataColumn(int size, ValueType type) {
        this.size = size;
        this.type = type;
        entryBytes = switch (type) {
            case NOMINAL -> 2;
            case NUMERIC -> 3;
            case TIMESTAMP -> 3;
        };
        long memorySize = ((long) size + 1) << entryBytes; // one additional for guaranteed missing
                                                           // value at the end
        long baseAddress = MemoryUtils.UNSAFE.allocateMemory(memorySize);
        writeNominalBaseAddress = (type == ValueType.NOMINAL) ? baseAddress : -1;
        writeNumericBaseAddress = (type == ValueType.NUMERIC) ? baseAddress : -1;
        writeTimestampBaseAddress = (type == ValueType.TIMESTAMP) ? baseAddress : -1;
        readNominalBaseAddress = writeNominalBaseAddress;
        readNumericBaseAddress = writeNumericBaseAddress;
        readTimestampBaseAddress = writeTimestampBaseAddress;
        mapping = type == ValueType.NOMINAL ? new NominalMapping() : null;

        // update statistics
        DirectMemoryStatistics.STATISTIC_CHUNKS_USED.incrementAndGet();
        DirectMemoryStatistics.STATISTIC_MEMORY_USED.addAndGet(memorySize);
        DirectMemoryStatistics.STATISTIC_TOTAL_MEMORY_ALLOCATED.addAndGet(memorySize);

        // add to clean-up queue
        references.add(reference = new DataColumnReference(this, referenceQueue));
        reference.columnBaseAddress = baseAddress;
        reference.memorySize = memorySize;
    }

    DataColumn(int size, ValueType type, long baseAddress, NominalMapping mapping) {
        this.size = size;
        this.type = type;
        entryBytes = switch (type) {
            case NOMINAL -> 2;
            case NUMERIC, TIMESTAMP -> 3;
        };
        writeNominalBaseAddress = 0;
        writeNumericBaseAddress = 0;
        writeTimestampBaseAddress = 0;
        readNominalBaseAddress = (type == ValueType.NOMINAL) ? baseAddress : -1;
        readNumericBaseAddress = (type == ValueType.NUMERIC) ? baseAddress : -1;
        readTimestampBaseAddress = (type == ValueType.TIMESTAMP) ? baseAddress : -1;

        this.mapping = mapping;

        // We may not free memory not managed by us
        reference = null;
    }

    final double getNumericValue(long index) {
        if (DEVELOPMENT_MODE && readNumericBaseAddress <= 0)
            throw new IllegalStateException("Cannot get numeric value from column of type " + type.toString() + " or column is still writable.");
        return MemoryUtils.UNSAFE.getDouble(readNumericBaseAddress + (index << entryBytes));
    }

    final long getTimestampValue(long index) {
        if (DEVELOPMENT_MODE && readTimestampBaseAddress <= 0)
            throw new IllegalStateException("Cannot get timestamp value from column of type " + type.toString() + " or column is still writable.");
        return MemoryUtils.UNSAFE.getLong(readTimestampBaseAddress + (index << entryBytes));
    }

    final String getNominalValue(long index) {
        if (DEVELOPMENT_MODE && readNominalBaseAddress <= 0)
            throw new IllegalStateException("Cannot get nominal value from column of type " + type.toString() + " or column is still writable.");
        return mapping.mapIndex(MemoryUtils.UNSAFE.getInt(readNominalBaseAddress + (index << entryBytes)));
    }

    public final void setNumericValue(long index, double value) {
        if (DEVELOPMENT_MODE && writeNumericBaseAddress <= 0)
            throw new IllegalStateException("Cannot set numeric value in column of type " + type.toString() + " or column is already read-only.");
        MemoryUtils.UNSAFE.putDouble(writeNumericBaseAddress + (index << entryBytes), value);
    }

    public final void setTimestampValue(long index, long value) {
        if (DEVELOPMENT_MODE && writeTimestampBaseAddress <= 0)
            throw new IllegalStateException("Cannot set timestamp value in column of type " + type.toString() + " or column is already read-only.");
        MemoryUtils.UNSAFE.putLong(writeTimestampBaseAddress + (index << entryBytes), value);
    }

    public final void setNominalValue(long index, String value) {
        if (DEVELOPMENT_MODE && writeNominalBaseAddress <= 0)
            throw new IllegalStateException("Cannot set nominal value in column of type " + type.toString() + " or column is already read-only.");
        MemoryUtils.UNSAFE.putInt(writeNominalBaseAddress + (index << entryBytes), mapping.mapString(value));
    }

    /**
     * This method is reserved for internal use during deserialization. If this method is used, the
     * NominalMapping needs to be set separately. As all set methods, this method will only work
     * before {@link #finishWriting(int)} ()} is called. Otherwise a AccessViolation is triggered.
     * 
     * @param index
     *            index within this column
     * @param nominalValueIndex
     *            the internal index of the nominal value
     * 
     */
    void setNominalIndexValue(int index, int nominalValueIndex) {
        if (DEVELOPMENT_MODE && writeNominalBaseAddress <= 0)
            throw new IllegalStateException("Cannot set nominal value in column of type " + type.toString() + " or column is already read-only.");
        MemoryUtils.UNSAFE.putInt(writeNominalBaseAddress + (index << entryBytes), nominalValueIndex);
    }

    final public void finishWriting(int finalSize) {
        if (writeNominalBaseAddress == 0 && writeNumericBaseAddress == 0 && writeTimestampBaseAddress == 0)
            throw new IllegalStateException("Column already in read-only mode.");
        if (finalSize != size)
            resize(finalSize);
        switch (type) {
            case NOMINAL:
                mapping.finishWriting();
                setNominalValue(getMissingIndex(), ValueType.MISSING_NOMINAL);
                break;
            case NUMERIC:
                setNumericValue(getMissingIndex(), ValueType.MISSING_NUMERIC);
                break;
            case TIMESTAMP:
                setTimestampValue(getMissingIndex(), ValueType.MISSING_TIMESTAMP);
                break;
        }
        writeNominalBaseAddress = 0;
        writeNumericBaseAddress = 0;
        writeTimestampBaseAddress = 0;
    }

    final void resize(int newSize) {
        if (writeNominalBaseAddress == 0 && writeNumericBaseAddress == 0 && writeTimestampBaseAddress == 0)
            throw new IllegalStateException("Column already in read-only mode.");
        // we will allocate an additional entry for a guaranteed missing value at the end
        long oldMemorySize = ((long) size + 1) << entryBytes;
        long newMemorySize = ((long) newSize + 1) << entryBytes;
        DirectMemoryStatistics.STATISTIC_MEMORY_USED.addAndGet(newMemorySize - oldMemorySize);
        DirectMemoryStatistics.STATISTIC_TOTAL_MEMORY_ALLOCATED.addAndGet(newMemorySize - oldMemorySize);
        reference.memorySize = newMemorySize;
        this.size = newSize;
        switch (type) {
            case NOMINAL:
                writeNominalBaseAddress = MemoryUtils.UNSAFE.reallocateMemory(writeNominalBaseAddress, newMemorySize);
                readNominalBaseAddress = writeNominalBaseAddress;
                reference.columnBaseAddress = writeNominalBaseAddress;
                break;
            case NUMERIC:
                writeNumericBaseAddress = MemoryUtils.UNSAFE.reallocateMemory(writeNumericBaseAddress, newMemorySize);
                readNumericBaseAddress = writeNumericBaseAddress;
                reference.columnBaseAddress = writeNumericBaseAddress;
                break;
            case TIMESTAMP:
                writeTimestampBaseAddress = MemoryUtils.UNSAFE.reallocateMemory(writeTimestampBaseAddress, newMemorySize);
                readTimestampBaseAddress = writeTimestampBaseAddress;
                reference.columnBaseAddress = writeTimestampBaseAddress;
                break;
        }
    }

    /**
     * This returns the index that can be used to access a missing value in this DataColumn.
     * 
     * @return the index of the missing value writen after the payload data
     */
    final public int getMissingIndex() {
        return size;
    }
}
