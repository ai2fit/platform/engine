package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.IntToLongFunction;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractVectorTransformation;
import com.owc.singularity.engine.tools.function.ObjObjIntIntConsumer;


public class TimestampContextualVectorTransformation<C> extends AbstractVectorTransformation {

    private ObjObjIntIntConsumer<C, IntToLongFunction> contextUpdate;
    private ToLongFunction<C> valueDerivationFunction;
    private ThreadLocal<C> context;
    protected IntToLongFunction vectorAccessor;

    public TimestampContextualVectorTransformation(String newAttributeName, String sourceAttributeName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntToLongFunction> contextUpdate, ToLongFunction<C> valueDerivationFunction) {
        super(newAttributeName, ValueType.TIMESTAMP, sourceAttributeName);
        this.context = ThreadLocal.withInitial(contextSupplier);
        this.contextUpdate = contextUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        vectorAccessor = row -> sourceSet.getTimestampValue(row, attributeIndex);
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            // first update the state
            C localContext = context.get();
            contextUpdate.accept(localContext, vectorAccessor, row, maxRows);
            dataColumn.setTimestampValue(row, valueDerivationFunction.applyAsLong(localContext));
        });
    }
}
