package com.owc.singularity.engine.object.data.exampleset.transformer.constants;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;

public abstract class ConstantCreation {

    private String name;

    public ConstantCreation(String name) {
        this.name = name;
    }

    public abstract ColumnDescription apply();

    public String getName() {
        return name;
    }
}
