/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serial;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.tools.Tools;


/**
 * The condition is fulfilled if an attribute has a value equal to, not equal to, less than, ... a
 * given value.
 * 
 * @author Ingo Mierswa, Nils Woehler
 */
public class AttributeValueSingleConditionFilter implements ExampleSetRowFilter {

    @Serial
    private static final long serialVersionUID = 1537763901048986863L;

    private static final String[] COMPARISON_TYPES = { "<=", ">=", "!=", "<>", "=", "<", ">" };

    private static final String MISSING_ENCODING = "\\?";

    public static final int LEQ = 0;

    public static final int GEQ = 1;

    public static final int NEQ1 = 2;

    public static final int NEQ2 = 3;

    public static final int EQUALS = 4;

    public static final int LESS = 5;

    public static final int GREATER = 6;

    public static String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss Z";

    private int comparisonType = EQUALS;

    private String attributeName;
    private transient Attribute attribute;

    private String rawValue;

    private double numericalValue;

    private String nominalValue;

    private long timestampValue;

    private boolean isMissingAllowed = false;

    /**
     * Creates a new AttributeValueFilter. If attribute is not nominal, value must be either a
     * number or a date string.
     */
    public AttributeValueSingleConditionFilter(String attributeName, int comparisonType, String value) {
        this.attributeName = attributeName;
        this.comparisonType = comparisonType;
        rawValue = value;
    }

    /**
     * Constructs an AttributeValueFilter for a given {@link ExampleSet} from a parameter string
     * 
     * @param parameterString
     *            Must be of the form attribute R value, where R is one out of =, !=, &lt&, &gt;,
     *            &lt;=, and &gt;=.
     */
    public AttributeValueSingleConditionFilter(String parameterString) {
        if ((parameterString == null) || (parameterString.isBlank())) {
            throw new IllegalArgumentException("Parameter string must not be empty!");
        }

        int compIndex = -1;
        for (comparisonType = 0; comparisonType < COMPARISON_TYPES.length; comparisonType++) {
            compIndex = parameterString.indexOf(COMPARISON_TYPES[comparisonType]);
            if (compIndex != -1) {
                break;
            }
        }
        if (compIndex == -1) {
            throw new IllegalArgumentException("Parameter string must have the form 'attribute {=|<|>|<=|>=|!=} value'");
        }
        attributeName = parameterString.substring(0, compIndex).trim();
        if ((attributeName.isEmpty()) || (parameterString.substring(compIndex + COMPARISON_TYPES[comparisonType].length()).trim().isEmpty())) {
            throw new IllegalArgumentException("Parameter string must have the form 'attribute {=|<|>|<=|>=|!=} value'");
        }
        rawValue = parameterString.substring(compIndex + COMPARISON_TYPES[comparisonType].length()).trim();
    }

    @Override
    public String toString() {
        return attribute.getName() + " " + COMPARISON_TYPES[comparisonType] + " " + (attribute.isNominal() ? nominalValue : "" + numericalValue);
    }

    /**
     * Returns true if the condition is fulfilled for the given example. Comparisons with NaN and
     * <code>null</code> return <code>false</code>.
     */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) {
        switch (attribute.getValueType()) {
            case NOMINAL:
                String value = exampleSet.getNominalValue(row, attribute.getIndex());
                if (ValueType.isMissing(value)) {
                    return switch (comparisonType) {
                        case NEQ1, NEQ2 -> !isMissingAllowed;
                        case EQUALS -> isMissingAllowed;
                        default -> false;
                    };
                } else {
                    return switch (comparisonType) {
                        case NEQ1, NEQ2 -> !nominalValue.equals(value);
                        case EQUALS -> nominalValue.equals(value);
                        default -> false;
                    };
                }
            case NUMERIC:
                return switch (comparisonType) {
                    case LEQ -> Tools.isLessEqual(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    case GEQ -> Tools.isGreaterEqual(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    case NEQ1, NEQ2 -> Tools.isNotEqual(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    case EQUALS -> Tools.isEqual(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    case LESS -> Tools.isLess(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    case GREATER -> Tools.isGreater(exampleSet.getNumericValue(row, attribute.getIndex()), numericalValue);
                    default -> false;
                };
            case TIMESTAMP:
                long currentDateValue = exampleSet.getTimestampValue(row, attribute.getIndex());
                return switch (comparisonType) {
                    case LEQ -> Tools.isLessEqual(currentDateValue, timestampValue);
                    case GEQ -> Tools.isGreaterEqual(currentDateValue, timestampValue);
                    case NEQ1, NEQ2 -> Tools.isNotEqual(currentDateValue, timestampValue);
                    case EQUALS -> Tools.isEqual(currentDateValue, timestampValue);
                    case LESS -> Tools.isLess(currentDateValue, timestampValue);
                    case GREATER -> Tools.isGreater(currentDateValue, timestampValue);
                    default -> false;
                };
            default:
                return false;
        }
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        attribute = exampleSet.getAttributes().get(attributeName);
        if (attribute == null)
            throw new UserError(null, 163, attributeName);

        if (attribute.isNominal()) {
            if ((comparisonType != EQUALS) && (comparisonType != NEQ1 && comparisonType != NEQ2)) {
                throw new IllegalArgumentException("For nominal attributes only '=' and '!=' or '<>' is allowed!");
            }
            this.nominalValue = rawValue;
            // Check if this string is equal to missing
            this.isMissingAllowed = nominalValue.equals(MISSING_ENCODING);
        } else if (attribute.isNumerical()) {
            if (rawValue.equals("?")) {
                this.numericalValue = Double.NaN;
            } else {
                try {
                    this.numericalValue = Double.parseDouble(rawValue);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Value for attribute '" + attribute.getName() + "' must be numerical, but was '" + rawValue + "'!");
                }
            }
        } else { // date
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
            try {
                if (rawValue.equals("?")) {
                    this.timestampValue = ValueType.MISSING_TIMESTAMP;
                } else {
                    this.timestampValue = dateFormat.parse(rawValue).getTime();
                }
            } catch (ParseException e) {
                throw new IllegalArgumentException("Could not parse value '" + rawValue + "' with date pattern " + DATE_PATTERN);
            }
        }
    }
}
