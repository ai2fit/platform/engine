/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset;


import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import com.owc.singularity.engine.object.AbstractIOObject;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.repository.entry.EntryTypeAlias;


/**
 * An example set is an immutable, read-only, off-heap memory structure to hold large amounts of
 * data. Data is organized in single columns. Columns are grouped together by columnGroups which
 * contain an access index. This allows two example sets to be based upon the same data but
 * accessing it in a different order or different parts of it without the need to copy it.<br>
 * Due to the immutability it's possible to use this for efficiently store multiple data copies in
 * memory that are usually created during processing. <br>
 * From the outside point of view, the columns are visible as {@link Attribute}s. Each
 * {@link Attribute} is tied to this specific example set. Each {@link Attribute} can be special or
 * regular and special attributes can have a role.
 * 
 * @author Sebastian Land
 */
@EntryTypeAlias(alias = "com.owc.singularity.engine.object.data.example.ExampleSet")
public final class ExampleSet extends AbstractIOObject implements Iterable<Example> {

    private static final long serialVersionUID = -8253989994276366820L;
    /** Holds all information about the attributes. */

    final DataColumn[] dataColumns;
    final Attributes attributes;
    final int[][] attributesColumnGroup;
    final int[][] columnGroups;
    final int size;

    /**
     * This constructor allows to create a new view onto the unchanged data of an underlying example
     * set by providing changed columnGroups. They will be automatically set to the according
     * attributes using them.
     * 
     * @param underlyingSet
     *            the underlying set
     * @param columnGroups
     *            the new view on the underlying set's data columns
     */
    ExampleSet(ExampleSet underlyingSet, int[][] columnGroups) {
        this.dataColumns = underlyingSet.dataColumns;
        HashMap<int[], int[]> translationMap = new HashMap<>();
        for (int i = 0; i < columnGroups.length; i++) {
            translationMap.put(underlyingSet.columnGroups[i], columnGroups[i]);
        }
        attributesColumnGroup = IntStream.range(0, underlyingSet.attributesColumnGroup.length)
                .mapToObj(i -> translationMap.get(underlyingSet.attributesColumnGroup[i]))
                .toArray(int[][]::new);
        this.columnGroups = columnGroups;
        if (columnGroups.length > 0)
            this.size = columnGroups[0].length;
        else
            this.size = 0;
        this.attributes = underlyingSet.attributes;
    }

    public ExampleSet(int size, String[] attributeNames, ValueType[] valueTypes, DataColumn[] dataColumns, String[] attributeRoles,
            boolean[] attributeIsSpecial) {
        this.dataColumns = dataColumns;
        this.attributesColumnGroup = new int[attributeNames.length][];
        int[] identity = new int[size];
        for (int i = 0; i < size; i++)
            identity[i] = i;
        for (int i = 0; i < attributesColumnGroup.length; i++)
            attributesColumnGroup[i] = identity;
        columnGroups = new int[][] { identity };
        this.size = size;
        this.attributes = new Attributes(this, attributeNames, valueTypes, attributeRoles, attributeIsSpecial);
    }

    ExampleSet(String[] attributeNames, ValueType[] valueTypes, DataColumn[] dataColumns, String[] attributeRoles, boolean[] attributeIsSpecial,
            int[][] attributesColumnGroup, int size) {
        this.dataColumns = dataColumns;
        this.attributesColumnGroup = attributesColumnGroup;

        // finding unique column groups
        HashSet<int[]> columnGroupSet = new HashSet<>();
        for (int i = 0; i < attributesColumnGroup.length; i++)
            columnGroupSet.add(attributesColumnGroup[i]);
        this.columnGroups = columnGroupSet.toArray(int[][]::new);

        this.size = size;
        this.attributes = new Attributes(this, attributeNames, valueTypes, attributeRoles, attributeIsSpecial);
    }

    public int getAttributeIndex(Attribute attribute) {
        if (attribute.exampleSet != this)
            throw new IllegalArgumentException("Can only access attributes of this example set!");
        return attribute.attributeIndex;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    /**
     * This method shouldn't be used anymore as it is only creating an object to indirectly
     * reference this table. Use the get or stream methods directly.
     */
    @Deprecated
    public Example getExample(int index) {
        return new Example(this, index);
    }


    /**
     * This method shouldn't be used anymore as it is only creating an object to indirectly
     * reference this table. Use the get or stream methods directly.
     */
    @Deprecated
    @Override
    public Iterator<Example> iterator() {
        return IntStream.range(0, size).mapToObj(this::getExample).iterator();
    }

    /**
     * This returns the size of this example set
     * 
     * @return
     */
    public int size() {
        return size;
    }


    /**
     * This method returns an example set transformer for this specific object. The transformer will
     * allow to register transformation rules how to derive a new ExampleSet. On
     * {@link ExampleSetTransformer#transform()} these rules will be applied and a new object will
     * be returned without changing this object at all.
     * 
     * @return the example set transformer
     */
    public ExampleSetTransformer transform() {
        return new ExampleSetTransformer(this);
    }

    /**
     * This method returns an example set combiner for this specific ExampleSet. The combiner allows
     * to combine this example set with others into new {@link ExampleSet}s without changing any of
     * the source objects.
     * 
     * @return the example set combiner
     */
    public ExampleSetCombiner combine() {
        return new ExampleSetCombiner(this);
    }

    /**
     * This method returns a description of this data set containing the attribute names, types,
     * specials and roles. This can be used for later compatibility checks agains other example
     * sets.
     * 
     * @return the header
     */
    public ExampleSetHeader getHeader() {
        return new ExampleSetHeader(this);
    }

    /**
     * This returns the nominal value of the specified row of specified attribute index. This is a
     * very lightweight and performant method that should be called whenever performance is a
     * requirement. <br>
     * <b>BE CAREFULL: NO SANITY CHECKS ARE PERFORMED. They need to be done outside any loop over
     * the actual content</b>
     * 
     * @param row
     *            the row index between 0 and {@link #size}
     * @param attributeIndex
     *            a valid index of an attribute of this table. See
     *            {@link #getAttributeIndex(Attribute)}
     * @return
     */
    public final String getNominalValue(int row, int attributeIndex) {
        return dataColumns[attributeIndex].getNominalValue(attributesColumnGroup[attributeIndex][row]);
    }


    public final double getNumericValue(int row, int attributeIndex) {
        return dataColumns[attributeIndex].getNumericValue(attributesColumnGroup[attributeIndex][row]);
    }


    public final long getTimestampValue(int row, int attributeIndex) {
        return dataColumns[attributeIndex].getTimestampValue(attributesColumnGroup[attributeIndex][row]);
    }

    public IntStream streamRows() {
        return IntStream.range(0, size);
    }

    public DoubleStream streamNumericAttribute(Attribute attribute) {
        DataColumn dataColumn = dataColumns[attribute.attributeIndex];
        return Arrays.stream(attributesColumnGroup[attribute.attributeIndex]).mapToDouble(dataColumn::getNumericValue);
    }

    public Stream<String> streamNominalAttribute(Attribute attribute) {
        DataColumn dataColumn = dataColumns[attribute.attributeIndex];
        return Arrays.stream(attributesColumnGroup[attribute.attributeIndex]).mapToObj(dataColumn::getNominalValue);
    }

    public LongStream streamTimestampAttribute(Attribute attribute) {
        DataColumn dataColumn = dataColumns[attribute.attributeIndex];
        return Arrays.stream(attributesColumnGroup[attribute.attributeIndex]).mapToLong(dataColumn::getTimestampValue);
    }

    public DoubleStream streamNumericAttribute(int attributeIndex) {
        DataColumn dataColumn = dataColumns[attributeIndex];
        return Arrays.stream(attributesColumnGroup[attributeIndex]).mapToDouble(dataColumn::getNumericValue);
    }

    public Stream<String> streamNominalAttribute(int attributeIndex) {
        DataColumn dataColumn = dataColumns[attributeIndex];
        return Arrays.stream(attributesColumnGroup[attributeIndex]).mapToObj(dataColumn::getNominalValue);
    }

    public LongStream streamTimestampAttribute(int attributeIndex) {
        DataColumn dataColumn = dataColumns[attributeIndex];
        return Arrays.stream(attributesColumnGroup[attributeIndex]).mapToLong(dataColumn::getTimestampValue);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ExampleSet with ")
                .append(size)
                .append(" rows and ")
                .append(attributes.allSize())
                .append(" attributes (")
                .append(attributes.specialSize())
                .append(" with special role)\n\n");
        if (attributes.size() > 0) {
            int maxNameLength = Math.min(125, attributes.streamAttributes().map(Attribute::getName).mapToInt(String::length).max().getAsInt());
            OptionalInt potentialMaxRoleLength = attributes.streamAttributes().map(Attribute::getRole).filter(Objects::nonNull).mapToInt(String::length).max();
            int maxRoleLength;
            if (potentialMaxRoleLength.isPresent())
                maxRoleLength = Math.min(125, potentialMaxRoleLength.getAsInt());
            else
                maxRoleLength = 0;

            builder.append("Attribute                                                                                                                    ", 0,
                    maxNameLength);
            builder.append("   Type       ");
            builder.append("Role                                                                                                                         ", 0,
                    maxRoleLength);

            if (size >= 2) {
                builder.append("   Row 1    ...    Row ").append(size);
            }
            builder.append("\n");

            for (Attribute attribute : attributes.attributes) {
                if (attribute.getName().length() <= 125) {
                    builder.append(attribute.getName());
                    builder.append(
                            "                                                                                                                             ", 0,
                            maxNameLength - attribute.getName().length());
                } else {
                    builder.append(attribute.getName().substring(0, 125));
                }
                builder.append(switch (attribute.getValueType()) {
                    case NOMINAL -> "   NOMINAL    ";
                    case NUMERIC -> "   NUMERIC    ";
                    case TIMESTAMP -> "   TIMESTAMP  ";
                });
                String role = attribute.isSpecial() ? attribute.getRole() : "";
                if (role.length() <= 125) {
                    builder.append(role);
                    builder.append(
                            "                                                                                                                             ", 0,
                            maxRoleLength - role.length() + 3);
                } else {
                    builder.append(role.substring(0, 125));
                }

                if (size >= 2) {
                    String value = switch (attribute.getValueType()) {
                        case NOMINAL -> {
                            String nominalValue = getNominalValue(0, attribute.attributeIndex);
                            if (ValueType.isMissing(nominalValue))
                                yield "MISSING";
                            yield nominalValue;
                        }
                        case NUMERIC -> {
                            double numericValue = getNumericValue(0, attribute.attributeIndex);
                            if (ValueType.isMissing(numericValue))
                                yield "MISSING";
                            yield Tools.formatIntegerIfPossible(numericValue);
                        }
                        case TIMESTAMP -> {
                            long timestampValue = getTimestampValue(0, attribute.attributeIndex);
                            if (ValueType.isMissing(timestampValue))
                                yield "MISSING";
                            yield Tools.formatDateAsUserSpecified(timestampValue);
                        }
                    };
                    if (value.length() > 14) {
                        builder.append(value, 0, 11);
                        builder.append("...   ");

                    } else {
                        builder.append(value);
                        builder.append("                         ", 0, 2 + 14 - value.length());
                    }

                    value = switch (attribute.getValueType()) {
                        case NOMINAL -> {
                            String nominalValue = getNominalValue(size - 1, attribute.attributeIndex);
                            if (ValueType.isMissing(nominalValue))
                                yield "MISSING";
                            yield nominalValue;
                        }
                        case NUMERIC -> {
                            double numericValue = getNumericValue(size - 1, attribute.attributeIndex);
                            if (ValueType.isMissing(numericValue))
                                yield "MISSING";
                            yield Tools.formatIntegerIfPossible(numericValue);
                        }
                        case TIMESTAMP -> {
                            long timestampValue = getTimestampValue(size - 1, attribute.attributeIndex);
                            if (ValueType.isMissing(timestampValue))
                                yield "MISSING";
                            yield Tools.formatDateAsUserSpecified(timestampValue);
                        }
                    };
                    if (value.length() > 10) {
                        builder.append(value, 0, 8);
                        builder.append("...  ");

                    } else {
                        builder.append(value);
                        builder.append("                         ", 0, 10 - value.length());
                    }
                }

                builder.append("\n");
            }
        }
        return builder.toString();
    }


    @Override
    public int hashCode() {
        return getAttributes().hashCode();
    }

    /**
     * Returns true, if all attributes including labels and other special attributes are equal.
     */

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ExampleSet es)) {
            return false;
        }
        return getAttributes().equals(es.getAttributes());
    }

}
