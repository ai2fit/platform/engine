package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.ToLongFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NominalToTimestampPointTransformation extends AbstractPointTransformation {

    private ToLongFunction<String> pointDerivation;

    public NominalToTimestampPointTransformation(String newAttributeName, String sourceAttributeName, ToLongFunction<String> derive) {
        super(newAttributeName, ValueType.TIMESTAMP, sourceAttributeName);
        this.pointDerivation = derive;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setTimestampValue(row, pointDerivation.applyAsLong(sourceSet.getNominalValue(row, attributeIndex))));
    }
}
