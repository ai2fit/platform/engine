/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.object.data.exampleset.transformer.filter;


import java.io.Serial;
import java.util.LinkedList;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.UserError;


/**
 * This subclass of {@link ExampleSetRowFilter} serves to excludes all examples containing missing
 * values within specified attributes from an {@link ExampleSet}. The parameters might be specified
 * using a regular expression as parameter string
 * 
 * @author Sebastian Land ingomierswa Exp $
 */
public class NoMissingValuesFilter implements ExampleSetRowFilter {

    @Serial
    private static final long serialVersionUID = -6043772701857922762L;

    private transient Attribute[] nominalAttributes;
    private transient Attribute[] numericAttributes;
    private transient Attribute[] timestampAttributes;


    /** Returns true if the example does not contain missing values within regarded attributes. */
    @Override
    public boolean pass(int row, ExampleSet exampleSet) {
        for (Attribute nominalAttribute : nominalAttributes) {
            if (ValueType.isMissing(exampleSet.getNominalValue(row, nominalAttribute.getIndex())))
                return false;
        }
        for (Attribute numericAttribute : numericAttributes) {
            if (ValueType.isMissing(exampleSet.getNumericValue(row, numericAttribute.getIndex())))
                return false;
        }
        for (Attribute timestampAttribute : timestampAttributes) {
            if (ValueType.isMissing(exampleSet.getTimestampValue(row, timestampAttribute.getIndex())))
                return false;
        }
        return true;
    }

    @Override
    public void prepare(ExampleSet exampleSet) throws UserError {
        LinkedList<Attribute> nominalAttributeList = new LinkedList<>();
        LinkedList<Attribute> numericAttributeList = new LinkedList<>();
        LinkedList<Attribute> timestampAttributeList = new LinkedList<>();
        for (Attribute attribute : exampleSet.getAttributes()) {
            switch (attribute.getValueType()) {
                case NOMINAL -> nominalAttributeList.add(attribute);
                case NUMERIC -> numericAttributeList.add(attribute);
                case TIMESTAMP -> timestampAttributeList.add(attribute);
            }
        }

        nominalAttributes = nominalAttributeList.toArray(Attribute[]::new);
        numericAttributes = numericAttributeList.toArray(Attribute[]::new);
        timestampAttributes = timestampAttributeList.toArray(Attribute[]::new);
    }
}
