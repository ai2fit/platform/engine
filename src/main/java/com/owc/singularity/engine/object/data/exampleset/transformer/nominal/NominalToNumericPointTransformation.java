package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NominalToNumericPointTransformation extends AbstractPointTransformation {

    private final ToDoubleFunction<String> pointDerivation;

    public NominalToNumericPointTransformation(String newAttributeName, String sourceAttributeName, ToDoubleFunction<String> derive) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.pointDerivation = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNumericValue(row, pointDerivation.applyAsDouble(sourceSet.getNominalValue(row, attributeIndex))));

    }
}
