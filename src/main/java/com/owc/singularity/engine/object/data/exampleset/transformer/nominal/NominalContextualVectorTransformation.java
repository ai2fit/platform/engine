package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractVectorTransformation;
import com.owc.singularity.engine.tools.function.ObjObjIntIntConsumer;


public class NominalContextualVectorTransformation<C> extends AbstractVectorTransformation {

    private ObjObjIntIntConsumer<C, IntFunction<String>> contextUpdate;
    private Function<C, String> valueDerivationFunction;
    private ThreadLocal<C> context;
    private IntFunction<String> vectorAccessor;

    public NominalContextualVectorTransformation(String newAttributeName, String sourceAttributeName, Supplier<C> contextSupplier,
            ObjObjIntIntConsumer<C, IntFunction<String>> stateUpdate, Function<C, String> valueDerivationFunction) {
        super(newAttributeName, ValueType.NOMINAL, sourceAttributeName);
        this.context = ThreadLocal.withInitial(contextSupplier);
        this.contextUpdate = stateUpdate;
        this.valueDerivationFunction = valueDerivationFunction;

    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        vectorAccessor = row -> sourceSet.getNominalValue(row, attributeIndex);
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            // first update the state
            C localContext = context.get();
            contextUpdate.accept(localContext, vectorAccessor, row, maxRows);
            dataColumn.setNominalValue(row, valueDerivationFunction.apply(localContext));
        });
    }
}
