package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.*;
import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.ExampleSetTransformation;


public class NumericStatefulRowPointTransformation<S> implements ExampleSetTransformation {

    protected final String newAttributeName;
    private final ValueType valueType;
    private final Supplier<S> stateSupplier;
    private final ObjIntArrayIntArrayIntArrayConsumer<S> stateUpdate;
    private final ToDoubleFunction<S> valueDerivationFunction;
    private final int[] numericAttributeIndexes;
    private final int[] nominalAttributeIndexes;
    private final int[] timestampAttributeIndexes;
    private final int numberOfNominalSubsetAttributes;
    private final int numberOfNumericSubsetAttributes;
    private final int numberOfTimestampSubsetAttributes;

    public NumericStatefulRowPointTransformation(String newAttributeName, Supplier<S> stateSupplier, ObjIntArrayIntArrayIntArrayConsumer<S> stateUpdate,
            ToDoubleFunction<S> valueDerivationFunction, int[] numericAttributeIndexes, int[] nominalAttributeIndexes, int[] timestampAttributeIndexes) {
        this.newAttributeName = newAttributeName;
        this.valueType = ValueType.NUMERIC;
        this.stateSupplier = stateSupplier;
        this.stateUpdate = stateUpdate;
        this.valueDerivationFunction = valueDerivationFunction;
        this.numericAttributeIndexes = numericAttributeIndexes;
        this.nominalAttributeIndexes = nominalAttributeIndexes;
        this.timestampAttributeIndexes = timestampAttributeIndexes;
        this.numberOfNominalSubsetAttributes = nominalAttributeIndexes.length;
        this.numberOfNumericSubsetAttributes = numericAttributeIndexes.length;
        this.numberOfTimestampSubsetAttributes = timestampAttributeIndexes.length;
    }

    @Override
    public Stream<ColumnDescription> apply(ExampleSet sourceSet) {
        DataColumn dataColumn = new DataColumn(sourceSet.size(), valueType);
        derive(dataColumn, sourceSet);
        dataColumn.finishWriting(sourceSet.size());
        return Stream.of(new ColumnDescription(newAttributeName, valueType, dataColumn));
    }

    protected void derive(DataColumn dataColumn, ExampleSet sourceSet) {
        S state = stateSupplier.get();
        for (int i = 0; i < sourceSet.size(); i++) {
            double[] numericValues = new double[numberOfNumericSubsetAttributes];
            String[] nominalValues = new String[numberOfNominalSubsetAttributes];
            long[] timestampValues = new long[numberOfTimestampSubsetAttributes];
            for (int j = 0; j < numberOfNumericSubsetAttributes; j++) {
                numericValues[j] = sourceSet.getNumericValue(i, numericAttributeIndexes[j]);
            }
            for (int j = 0; j < numberOfNominalSubsetAttributes; j++) {
                nominalValues[j] = sourceSet.getNominalValue(i, nominalAttributeIndexes[j]);
            }
            for (int j = 0; j < numberOfTimestampSubsetAttributes; j++) {
                timestampValues[j] = sourceSet.getTimestampValue(i, timestampAttributeIndexes[j]);
            }
            stateUpdate.accept(state, numericValues, nominalValues, timestampValues);
            dataColumn.setNumericValue(i, valueDerivationFunction.applyAsDouble(state));
        }
    }

    @FunctionalInterface
    public interface ObjIntArrayIntArrayIntArrayConsumer<T> {

        void accept(T t, double[] numericValues, String[] nominalValues, long[] timestampValues);
    }
}
