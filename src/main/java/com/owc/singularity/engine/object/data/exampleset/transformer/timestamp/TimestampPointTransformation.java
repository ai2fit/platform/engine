package com.owc.singularity.engine.object.data.exampleset.transformer.timestamp;

import java.util.function.LongUnaryOperator;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class TimestampPointTransformation extends AbstractPointTransformation {

    private LongUnaryOperator pointDerivation;

    public TimestampPointTransformation(String newAttributeName, String sourceAttributeName, LongUnaryOperator derive) {
        super(newAttributeName, ValueType.TIMESTAMP, sourceAttributeName);
        this.pointDerivation = derive;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setTimestampValue(row, pointDerivation.applyAsLong(sourceSet.getTimestampValue(row, attributeIndex))));

    }
}
