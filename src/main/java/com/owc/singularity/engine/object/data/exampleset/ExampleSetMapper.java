package com.owc.singularity.engine.object.data.exampleset;

/**
 * This class offers a public api to map foreign memory into an example set. The requirements are:
 * <ul>
 * <li>Columns of the data are stored in consecutive memory regions. Storing either double for
 * numerics, long for timestamps or ints for nominal values.</li>
 * <li>nominal values are assigned increasing integer values. A translation must be available</li>
 * <li>the foreign memory content does not change from external during the lifetime of the created
 * example set. Otherwise the data in the example set will also change.</li>
 * <li>the foreign memory remains accessible under the given base address through the entire
 * lifetime of the created example set or derived example sets</li>
 * <li>caller is responsible for freeing the memory once no example set uses it.</li>
 * </ul>
 * Each mapper object contains a state, which can be modified to derive new example sets. This can
 * be used in situations, where example sets grow in memory or new nominal values become known, but
 * the signature remains the same. This approach is preferred rather than re-creating the mapper
 * every time.
 * 
 */

public final class ExampleSetMapper {

    private String[] attributeNames;
    private ValueType[] attributeTypes;
    private String[] attributeRoles;
    private int size;
    private long[] attributeBaseAdresses;
    private NominalMapping[] nominalMappings;
    private boolean[] attributeSpecial;
    private DataColumn[] dataColumns;

    public ExampleSetMapper(String[] attributeNames, ValueType[] attributeTypes, String[] attributeRoles, int size) {
        this.attributeNames = attributeNames;
        this.attributeTypes = attributeTypes;
        this.attributeRoles = attributeRoles;
        this.size = size;

        this.attributeBaseAdresses = new long[attributeNames.length];
        this.attributeSpecial = new boolean[attributeNames.length];
        this.nominalMappings = new NominalMapping[attributeNames.length];
        for (int i = 0; i < attributeNames.length; i++) {
            if (attributeTypes[i] == ValueType.NOMINAL)
                nominalMappings[i] = new NominalMapping();
            if (attributeRoles[i] != null)
                attributeSpecial[i] = true;
        }

        this.dataColumns = new DataColumn[attributeNames.length];
    }

    /**
     * Sets the base address of the given attribute. Each attribute must have a valid address,
     * meaning that each memory address between the base address and base addres + (size*type
     * length) are accessible.
     * 
     * @param attributeIndex
     *            the index of the attribute according to order during creation of this mapper
     * @param baseAdress
     *            the base address in memory
     */
    public void setAttributeBaseAddress(int attributeIndex, long baseAdress) {
        this.attributeBaseAdresses[attributeIndex] = baseAdress;
    }

    /**
     * Using this method you can extend the known mapped values. Please not that values may not be
     * known already and that each new value receives the next int index.<br>
     * It is absolutely crucial that these codings are consistent with the memory content!
     * 
     * @param attributeIndex
     *            the index of the attribute according to the order during creation of this mapper
     * @param nextValues
     *            the array of new values. On first call the first value will be mapped to 0, the
     *            second to 1, etc. Consecutive calls will continue the numbering.
     */
    public void mapValues(int attributeIndex, String[] nextValues) {
        this.nominalMappings[attributeIndex].mapStringsAndUpdate(nextValues);
    }

    /**
     * This sets the size of the created example set. Make sure that the entire address range
     * between the base address and base address + (size*type length) are accessible.
     * 
     * @param size
     *            the new size
     */
    public void setSize(int size) {
        this.size = size;
    }

    public ExampleSet createExampleSet() {
        // creating new data columns
        for (int i = 0; i < attributeNames.length; i++) {
            dataColumns[i] = new DataColumn(size, attributeTypes[i], attributeBaseAdresses[i], nominalMappings[i]);
        }

        return new ExampleSet(size, attributeNames, attributeTypes, dataColumns, attributeRoles, attributeSpecial);
    }
}
