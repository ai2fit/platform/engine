package com.owc.singularity.engine.object.data.exampleset.transformer.constants;

import com.owc.singularity.engine.object.data.exampleset.ColumnDescription;
import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

public class TimestampConstantCreation extends ConstantCreation {

    public long value;

    public TimestampConstantCreation(String name, long value) {
        super(name);
        this.value = value;
    }

    @Override
    public ColumnDescription apply() {
        DataColumn column = new DataColumn(1, ValueType.TIMESTAMP);
        column.setTimestampValue(0, value);
        column.finishWriting(1);
        return new ColumnDescription(getName(), ValueType.TIMESTAMP, column);
    }
}
