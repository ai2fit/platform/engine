package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.function.DoubleUnaryOperator;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractPointTransformation;


public class NumericPointTransformation extends AbstractPointTransformation {

    private DoubleUnaryOperator pointDerivation;

    public NumericPointTransformation(String newAttributeName, String sourceAttributeName, DoubleUnaryOperator pointDerivation) {
        super(newAttributeName, ValueType.NUMERIC, sourceAttributeName);
        this.pointDerivation = pointDerivation;
    }


    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet, int attributeIndex) {
        IntStream.range(0, sourceSet.size())
                .parallel()
                .forEach(row -> dataColumn.setNumericValue(row, pointDerivation.applyAsDouble(sourceSet.getNumericValue(row, attributeIndex))));

    }
}
