package com.owc.singularity.engine.object.data.exampleset.transformer.nominal;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class NominalRowTransformation extends AbstractRowTransformation {

    /**
     * This accessor allows to access numeric values of a single row via it's {@link #get(int)}
     * method.
     *
     * @author Sebastian Land
     *
     */
    public static class NominalRowAccessor {

        int row;
        private int[] attributeIndices;
        private ExampleSet exampleSet;

        private NominalRowAccessor(ExampleSet exampleSet, int[] attributeIndices) {
            this.exampleSet = exampleSet;
            this.attributeIndices = attributeIndices;
        }

        public String get(int attributeNumber) {
            return exampleSet.getNominalValue(row, attributeIndices[attributeNumber]);
        }

        private void setRow(int row) {
            this.row = row;
        }
    }

    private Function<NominalRowAccessor, String> derive;
    private String[] numericSourceAttributeNames;


    public NominalRowTransformation(String derivedName, String[] numericSourceAttributeNames, Function<NominalRowAccessor, String> derive) {
        super(derivedName, ValueType.NUMERIC);
        this.numericSourceAttributeNames = numericSourceAttributeNames;
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        int[] sourceAttributeIndices = Arrays.stream(numericSourceAttributeNames).mapToInt(name -> sourceSet.getAttributes().get(name).getIndex()).toArray();
        ThreadLocal<NominalRowAccessor> localAccessor = new ThreadLocal<>() {

            @Override
            protected NominalRowAccessor initialValue() {
                return new NominalRowAccessor(sourceSet, sourceAttributeIndices);
            }
        };
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            NominalRowAccessor accessor = localAccessor.get();
            accessor.setRow(row);
            dataColumn.setNominalValue(row, derive.apply(accessor));
        });
    }

}
