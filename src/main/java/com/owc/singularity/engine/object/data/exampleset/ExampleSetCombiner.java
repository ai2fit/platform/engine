package com.owc.singularity.engine.object.data.exampleset;

import java.util.*;
import java.util.stream.Stream;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;
import com.owc.singularity.engine.operator.error.UserError;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

public class ExampleSetCombiner {

    private static final class JoinGroup {

        int[] joinIndices;
        int[] columnGroup;

        public JoinGroup(int[] joinIndices, int[] columnGroup) {
            super();
            this.joinIndices = joinIndices;
            this.columnGroup = columnGroup;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + columnGroup.hashCode();
            result = prime * result + joinIndices.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            JoinGroup other = (JoinGroup) obj;
            return columnGroup == other.columnGroup && joinIndices == other.joinIndices;
        }
    }

    public static final String DUPLICATE_JOIN_ATTRIBUTE_SPECIFIER = "_right";
    public static final String DUPLICATE_JOIN_ROLE_SPECIFIER = "_right";
    private final ExampleSet baseSet;

    ExampleSetCombiner(ExampleSet baseSet) {
        this.baseSet = baseSet;
    }

    public ExampleSet join(ExampleSet rightSet, int[] sourceIndices, int[] rightIndices) {
        // we just attach all right columns on the right to the left set by combining while avoiding
        // duplicate names
        String[] newAttributeNames = Stream
                .concat(baseSet.getAttributes().streamAttributes().map(Attribute::getName),
                        rightSet.getAttributes()
                                .streamAttributes()
                                .map(Attribute::getName)
                                .map(name -> (baseSet.getAttributes().contains(name)) ? name + DUPLICATE_JOIN_ATTRIBUTE_SPECIFIER : name))
                .toArray(String[]::new);
        // check for uniqueness
        HashSet<String> containedNames = new HashSet<String>();
        for (String newAttributeName : newAttributeNames) {
            if (!containedNames.add(newAttributeName))
                throw new OperatorRuntimeException(new OperatorException("duplicate_attribute", null, newAttributeName));
        }

        String[] newAttributeRoles = Stream
                .concat(baseSet.getAttributes().streamAttributes().map(Attribute::getRole),
                        rightSet.getAttributes()
                                .streamAttributes()
                                .map(Attribute::getRole)
                                .map(role -> (baseSet.getAttributes().containsRole(role)) ? role + DUPLICATE_JOIN_ROLE_SPECIFIER : role))
                .toArray(String[]::new);
        ValueType[] newValueTypes = Stream
                .concat(baseSet.getAttributes().streamAttributes().map(Attribute::getValueType),
                        rightSet.getAttributes().streamAttributes().map(Attribute::getValueType))
                .toArray(ValueType[]::new);
        DataColumn[] newDataColumns = Stream.concat(Arrays.stream(baseSet.dataColumns), Arrays.stream(rightSet.dataColumns)).toArray(DataColumn[]::new);
        boolean[] newIsSpecial = new boolean[newAttributeNames.length];
        System.arraycopy(baseSet.attributes.attributesIsSpecial, 0, newIsSpecial, 0, baseSet.attributes.attributesIsSpecial.length);
        System.arraycopy(rightSet.attributes.attributesIsSpecial, 0, newIsSpecial, baseSet.attributes.attributesIsSpecial.length,
                rightSet.attributes.attributesIsSpecial.length);

        // now we have to calculate the new column group access indices
        HashMap<JoinGroup, int[]> columnGroupTranslation = new HashMap<>();
        translateColumnGroups(baseSet, sourceIndices, columnGroupTranslation);
        translateColumnGroups(rightSet, rightIndices, columnGroupTranslation);
        int[][] newAttributeAccessIndices = Stream
                .concat(Arrays.stream(baseSet.attributesColumnGroup).map(columnGroup -> new JoinGroup(sourceIndices, columnGroup)),
                        Arrays.stream(rightSet.attributesColumnGroup).map(columnGroup -> new JoinGroup(rightIndices, columnGroup)))
                .parallel()
                .map(columnGroupTranslation::get)
                .toArray(int[][]::new);
        int size = 0;
        if (newAttributeAccessIndices.length > 0)
            size = newAttributeAccessIndices[0].length;
        return new ExampleSet(newAttributeNames, newValueTypes, newDataColumns, newAttributeRoles, newIsSpecial, newAttributeAccessIndices, size);
    }

    private void translateColumnGroups(ExampleSet joinSet, int[] joinIndices, HashMap<JoinGroup, int[]> columnGroupTranslation) {
        for (int columnIndex = 0; columnIndex < joinSet.dataColumns.length; columnIndex++) {
            int[] groupAccessIndices = joinSet.attributesColumnGroup[columnIndex];
            JoinGroup key = new JoinGroup(joinIndices, groupAccessIndices);
            if (!columnGroupTranslation.containsKey(key)) {
                int[] newGroupAccessIndices = new int[joinIndices.length];
                int missingIndex = joinSet.dataColumns[columnIndex].getMissingIndex();
                for (int i = 0; i < newGroupAccessIndices.length; i++) {
                    newGroupAccessIndices[i] = (joinIndices[i] != -1) ? groupAccessIndices[joinIndices[i]] : missingIndex;
                }

                columnGroupTranslation.put(key, newGroupAccessIndices);
            }
        }
    }

    public ExampleSet updateBy(ExampleSet changeSet, Attribute[] baseSetKeyAttributes, Attribute[] changeSetKeyAttributes, Attribute[] baseSetDataAttributes,
            Attribute[] changeSetDataAttributes, boolean ensureIdentity, boolean insertNewRows) throws UserError {
        // extract value types and test equality
        if (baseSetKeyAttributes.length != changeSetKeyAttributes.length)
            throw new IllegalArgumentException("key attribue arrays must be of same length");

        int[] baseSetKeyIndices = new int[baseSetKeyAttributes.length];
        int[] changeSetKeyIndices = new int[baseSetKeyAttributes.length];
        ValueType[] valueTypes = new ValueType[baseSetKeyAttributes.length];
        Map<Attribute, Attribute> attributesUpdatedByMap = new HashMap<>();

        for (int i = 0; i < baseSetKeyAttributes.length; i++) {
            baseSetKeyIndices[i] = baseSetKeyAttributes[i].getIndex();
            changeSetKeyIndices[i] = changeSetKeyAttributes[i].getIndex();
            valueTypes[i] = baseSetKeyAttributes[i].getValueType();
            attributesUpdatedByMap.put(baseSetKeyAttributes[i], changeSetKeyAttributes[i]);
            if (baseSetKeyAttributes[i].getValueType() != changeSetKeyAttributes[i].getValueType())
                throw new UserError(null, "update.value_types_must_match", baseSetKeyAttributes[i].getName(), changeSetKeyAttributes[i].getName(),
                        baseSetKeyAttributes[i].getValueType(), changeSetKeyAttributes[i].getValueType());
        }

        if (baseSetDataAttributes.length != changeSetDataAttributes.length)
            throw new IllegalArgumentException("data attribue arrays must be of same length");
        for (int i = 0; i < baseSetDataAttributes.length; i++)
            attributesUpdatedByMap.put(baseSetDataAttributes[i], changeSetDataAttributes[i]);


        // prepare lookup table for updates
        Object2IntOpenHashMap<KeyProvider> keyToChangeRowMap = new Object2IntOpenHashMap<>(changeSet.size());
        for (int i = 0; i < changeSet.size(); i++) {
            keyToChangeRowMap.put(new KeyProvider(changeSet, changeSetKeyIndices, valueTypes, i), i);
        }

        // prepare data structures
        boolean[] usedForUpdates = new boolean[changeSet.size];
        ThreadLocal<KeyProvider> keyProviderLocal = ThreadLocal.withInitial(() -> new KeyProvider(baseSet, baseSetKeyIndices, valueTypes, 0));
        int[] updatedByRows = baseSet.streamRows().sequential().map(row -> {
            KeyProvider localeKeyProvider = keyProviderLocal.get();
            localeKeyProvider.row = row;
            int updatedByRow = keyToChangeRowMap.getOrDefault(localeKeyProvider, -1);
            if (updatedByRow >= 0) {
                if (ensureIdentity && usedForUpdates[updatedByRow]) {
                    // in this case it's not an identity update
                    throw new OperatorRuntimeException(new UserError(null, "update.no_identity_update", row, updatedByRow));
                }
                usedForUpdates[updatedByRow] = true;
            }
            return updatedByRow;
        }).toArray();

        // count new rows
        int numberOfNewRows = 0;
        int[] newRowChangeSetIndices;
        if (insertNewRows) {
            for (int i = 0; i < usedForUpdates.length; i++)
                if (!usedForUpdates[i])
                    numberOfNewRows++;

            newRowChangeSetIndices = new int[numberOfNewRows];
            // new rows access indices
            int newRowIndex = 0;
            for (int i = 0; i < changeSet.size(); i++) {
                if (!usedForUpdates[i]) {
                    newRowChangeSetIndices[newRowIndex++] = i;
                }
            }
        } else {
            newRowChangeSetIndices = new int[0];
        }
        // create new example set
        ExampleSetCreator creator = new ExampleSetCreator(baseSet);
        int size = baseSet.size();
        creator.setSize(size + numberOfNewRows);

        baseSet.getAttributes().allAttributes().forEachRemaining(attribute -> {
            int attributeIndex = attribute.getIndex();
            if (attributesUpdatedByMap.containsKey(attribute)) {
                // then it is a data attribute updated by another attribute
                Attribute updatedBy = attributesUpdatedByMap.get(attribute);
                switch (attribute.getValueType()) {
                    case NOMINAL:
                        creator.withNominalAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                // base data
                                if (updatedByRows[row] >= 0)
                                    return changeSet.getNominalValue(updatedByRows[row], updatedBy.getIndex());
                                else
                                    return baseSet.getNominalValue(row, attributeIndex);
                            } else {
                                // newly inserted rows
                                return changeSet.getNominalValue(newRowChangeSetIndices[row - size], updatedBy.getIndex());
                            }
                        });
                        break;
                    case NUMERIC:
                        creator.withNumericAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                // base data
                                if (updatedByRows[row] >= 0)
                                    return changeSet.getNumericValue(updatedByRows[row], updatedBy.getIndex());
                                else
                                    return baseSet.getNumericValue(row, attributeIndex);
                            } else {
                                // newly inserted rows
                                return changeSet.getNumericValue(newRowChangeSetIndices[row - size], updatedBy.getIndex());
                            }
                        });
                        break;
                    case TIMESTAMP:
                        creator.withTimestampAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                // base data
                                if (updatedByRows[row] >= 0)
                                    return changeSet.getTimestampValue(updatedByRows[row], updatedBy.getIndex());
                                else
                                    return baseSet.getTimestampValue(row, attributeIndex);
                            } else {
                                // newly inserted rows
                                return changeSet.getTimestampValue(newRowChangeSetIndices[row - size], updatedBy.getIndex());
                            }
                        });
                        break;
                }
            } else {
                // no update: copy value from first rows and set unknown for all new
                switch (attribute.getValueType()) {
                    case NOMINAL:
                        creator.withNominalAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                return baseSet.getNominalValue(row, attributeIndex);
                            } else {
                                return ValueType.MISSING_NOMINAL;
                            }
                        });
                        break;
                    case NUMERIC:
                        creator.withNumericAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                return baseSet.getNumericValue(row, attributeIndex);
                            } else {
                                return ValueType.MISSING_NUMERIC;
                            }
                        });
                        break;
                    case TIMESTAMP:
                        creator.withTimestampAttributeContent(attributeIndex, row -> {
                            if (row < size) {
                                return baseSet.getTimestampValue(row, attributeIndex);
                            } else {
                                return ValueType.MISSING_TIMESTAMP;
                            }
                        });
                        break;
                }
            }
        });

        return creator.finish();
    }

    private static class KeyProvider {

        ExampleSet referenceSet;
        int[] columnIndices;
        ValueType[] valueTypes;
        int row;

        public KeyProvider(ExampleSet referenceSet, int[] columnIndices, ValueType[] valueTypes, int row) {
            super();
            this.referenceSet = referenceSet;
            this.columnIndices = columnIndices;
            this.valueTypes = valueTypes;
            this.row = row;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            for (int i = 0; i < columnIndices.length; i++) {
                switch (valueTypes[i]) {
                    case NOMINAL:
                        result = prime * result + Objects.hash(referenceSet.getNominalValue(row, columnIndices[i]));
                        break;
                    case NUMERIC:
                        result = prime * result + Double.hashCode(referenceSet.getNumericValue(row, columnIndices[i]));
                        break;
                    case TIMESTAMP:
                        result = prime * result + Long.hashCode(referenceSet.getTimestampValue(row, columnIndices[i]));
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            KeyProvider other = (KeyProvider) obj;
            for (int i = 0; i < columnIndices.length; i++) {
                switch (valueTypes[i]) {
                    case NOMINAL:
                        if (!Objects.equals(referenceSet.getNominalValue(row, columnIndices[i]),
                                other.referenceSet.getNominalValue(other.row, other.columnIndices[i])))
                            return false;
                    case NUMERIC:
                        if (!Objects.equals(referenceSet.getNumericValue(row, columnIndices[i]),
                                other.referenceSet.getNumericValue(other.row, other.columnIndices[i])))
                            return false;

                        break;
                    case TIMESTAMP:
                        if (!Objects.equals(referenceSet.getTimestampValue(row, columnIndices[i]),
                                other.referenceSet.getTimestampValue(other.row, other.columnIndices[i])))
                            return false;
                        break;
                    default:
                        break;
                }
            }
            return true;
        }
    }

    /**
     * This appends the given list of example sets to the base set. First checks are performed
     * whether all example sets are compatible.
     * 
     * @param appendSets
     *            Sets to append to the base set
     * @return the appended example set
     * @throws OperatorException
     *             if given sets are not compatible
     */
    public ExampleSet append(List<ExampleSet> appendSets) throws OperatorException {
        // checks if all example sets have the same signature
        checkForCompatibility(appendSets);


        // create new example creator
        ExampleSetCreator creator = new ExampleSetCreator(baseSet);
        List<ExampleSet> allExampleSets = new LinkedList<>();
        allExampleSets.add(baseSet);
        allExampleSets.addAll(appendSets);

        int totalRows = allExampleSets.stream().mapToInt(ExampleSet::size).sum();
        creator.setSize(totalRows);

        // prepare column access
        int[] rowTranslationTable = new int[totalRows];
        int[] setTranslationTable = new int[totalRows];
        ExampleSet[] sets = allExampleSets.toArray(ExampleSet[]::new);
        int[][] attributeIndices = new int[sets.length][];
        int targetRow = 0;

        for (int setIndex = 0; setIndex < sets.length; setIndex++) {
            ExampleSet set = sets[setIndex];
            attributeIndices[setIndex] = baseSet.getAttributes()
                    .streamAttributes()
                    .map(a -> set.getAttributes().get(a.getName()))
                    .mapToInt(Attribute::getIndex)
                    .toArray();
            for (int row = 0; row < set.size(); row++) {
                setTranslationTable[targetRow] = setIndex;
                rowTranslationTable[targetRow++] = row;
            }
        }

        // configure column fillers

        Attribute[] firstAttributes = baseSet.getAttributes().streamAttributes().toArray(Attribute[]::new);
        for (int firstAttributeIndex = 0; firstAttributeIndex < firstAttributes.length; firstAttributeIndex++) {
            Attribute attribute = firstAttributes[firstAttributeIndex];
            int finalAttributeIndex = firstAttributeIndex;
            switch (attribute.getValueType()) {
                case NOMINAL -> creator.withNominalAttributeContent(attribute.getName(), row -> sets[setTranslationTable[row]]
                        .getNominalValue(rowTranslationTable[row], attributeIndices[setTranslationTable[row]][finalAttributeIndex]));
                case NUMERIC -> creator.withNumericAttributeContent(attribute.getName(), row -> sets[setTranslationTable[row]]
                        .getNumericValue(rowTranslationTable[row], attributeIndices[setTranslationTable[row]][finalAttributeIndex]));
                case TIMESTAMP -> creator.withTimestampAttributeContent(attribute.getName(), row -> sets[setTranslationTable[row]]
                        .getTimestampValue(rowTranslationTable[row], attributeIndices[setTranslationTable[row]][finalAttributeIndex]));
            }
        }

        return creator.finish();
    }

    /**
     * Checks whether all attributes in set 1 occur in the others as well. Types are (deliberately)
     * not checked. Type check happens in {@link #appendExampleSets(List)} itself.
     *
     * @throws OperatorException
     *             failed check
     */
    private void checkForCompatibility(List<ExampleSet> allExampleSets) throws OperatorException {
        for (ExampleSet allExampleSet : allExampleSets) {
            checkForCompatibility(baseSet, allExampleSet);
        }
    }

    private void checkForCompatibility(ExampleSet first, ExampleSet second) throws OperatorException {
        if (first.getAttributes().allSize() != second.getAttributes().allSize()) {
            throw new UserError(null, 925, "numbers of attributes are different");
        }

        Iterator<Attribute> firstIterator = first.getAttributes().allAttributes();
        while (firstIterator.hasNext()) {
            Attribute firstAttribute = firstIterator.next();
            Attribute secondAttribute = second.getAttributes().get(firstAttribute.getName());
            if (secondAttribute == null) {
                throw new UserError(null, 925, "Attribute with name '" + firstAttribute.getName() + "' is not part of second example set.");
            }
            // we don't need to check if second has attributes not present in first as the size is
            // the same.
            if (!firstAttribute.getValueType().equals(secondAttribute.getValueType())) {
                throw new UserError(null, "attribute_types_not_equal", firstAttribute.getName(), firstAttribute.getValueType(), secondAttribute.getValueType());
            }
        }
    }
}
