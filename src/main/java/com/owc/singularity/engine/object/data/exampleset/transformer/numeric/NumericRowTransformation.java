package com.owc.singularity.engine.object.data.exampleset.transformer.numeric;

import java.util.Arrays;
import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

import com.owc.singularity.engine.object.data.exampleset.DataColumn;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.AbstractRowTransformation;

public final class NumericRowTransformation extends AbstractRowTransformation {

    /**
     * This accessor allows to access numeric values of a single row via it's {@link #get(int)}
     * method.
     * 
     * @author Sebastian Land
     *
     */
    public static class NumericRowAccessor {

        int row;
        private int[] attributeIndices;
        private ExampleSet exampleSet;

        private NumericRowAccessor(ExampleSet exampleSet, int[] attributeIndices) {
            this.exampleSet = exampleSet;
            this.attributeIndices = attributeIndices;
        }

        public double get(int attributeNumber) {
            return exampleSet.getNumericValue(row, attributeIndices[attributeNumber]);
        }

        private void setRow(int row) {
            this.row = row;
        }
    }

    private ToDoubleFunction<NumericRowAccessor> derive;
    private String[] numericSourceAttributeNames;


    public NumericRowTransformation(String derivedName, String[] numericSourceAttributeNames, ToDoubleFunction<NumericRowAccessor> derive) {
        super(derivedName, ValueType.NUMERIC);
        this.numericSourceAttributeNames = numericSourceAttributeNames;
        this.derive = derive;
    }

    @Override
    protected void apply(DataColumn dataColumn, ExampleSet sourceSet) {
        int[] sourceAttributeIndices = Arrays.stream(numericSourceAttributeNames).mapToInt(name -> sourceSet.getAttributes().get(name).getIndex()).toArray();
        ThreadLocal<NumericRowAccessor> localAccessor = new ThreadLocal<>() {

            @Override
            protected NumericRowAccessor initialValue() {
                return new NumericRowAccessor(sourceSet, sourceAttributeIndices);
            }
        };
        IntStream.range(0, sourceSet.size()).parallel().forEach(row -> {
            NumericRowAccessor accessor = localAccessor.get();
            accessor.setRow(row);
            dataColumn.setNumericValue(row, derive.applyAsDouble(accessor));
        });
    }

}
