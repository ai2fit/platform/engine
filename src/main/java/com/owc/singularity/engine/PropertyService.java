/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine;


import static com.owc.singularity.engine.EngineProperties.*;
import static com.owc.singularity.engine.EnginePropertyConstants.GENERAL_LOCALE_LANGUAGE_VALUES;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.tools.RandomGenerator;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.WebServiceTools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.parameter.*;
import com.owc.singularity.engine.tools.parameter.admin.ParameterEnforcer;


/**
 * This class loads the SingularityEngine property files and provides methods to access them. It
 * also stores the values of the properties. They are still mirrored in the System properties for
 * keeping compatibility, but it is strongly recommended to use this class to get access.
 *
 * During init this class will try to load settings from various sources. Sources with a higher
 * specificy will overwrite settings with a lower. The sequence is as follows while only the first
 * step is executed if the {@link EngineExecutionMode} forbidds file access:
 * <ol>
 * <li>if the system property <code>singularity.config.dir</code> is set, the file singularityrc
 * inside this directory will be loaded.</li>
 * <li>if the property is not set, the environment variable
 * <code>ENVIRONMENT_VARIABLE_CONFIG_DIR</code> will be evaluated in the same way.</li>
 * <li>the file singularity-studio-settings.cfg in the user's .singularity directory will be
 * loaded.</li>
 * <li>the file singularity-studio-settings.cfg in the user's home directory will be loaded.</li>
 * <li>the file denoted by the System property <code>singularity.rcfile</code> will be loaded if
 * defined.</li>
 * </ol>
 * It also provides methods to create files relative to the SingularityEngine home directory. The
 * way to access the properties via System.getProperty is deprecated and should be replaced by
 * #getProperty(String).
 *
 * @implNote (extensions-breaker) <a href=
 *           "https://gitlab.oldworldcomputing.com/singularity/engine/-/wikis/Extensions-Breaker">read
 *           more</a>
 *
 * @author Simon Fischer, Ingo Mierswa, Sebastian Land, Marco Boeck
 */
public class PropertyService {

    private static final Logger LOGGER = LogManager.getLogger(PropertyService.class.getSimpleName());

    public static final String SINGULARITY_CONFIG_FILE_NAME = "singularity-studio.settings";
    public static final String SYSTEM_PROPERTY_CONFIG_DIR = "singularity.config.dir";
    public static final String ENVIRONMENT_VARIABLE_CONFIG_DIR = "SINGULARITY_CONFIG_DIR";

    private static boolean initialized = false;
    private static final List<ParameterChangeListener> PARAMETER_LISTENERS = new LinkedList<>();
    private static final Map<String, Parameter> PARAMETER_MAP = new TreeMap<>();
    private static final List<ParameterWriter> PARAMETER_WRITERS = new LinkedList<>();

    private static final ParameterEnforcer ENFORCED_PARAMETER = new ParameterEnforcer(PropertyService::setParameterValue, PropertyService::getParameterValue,
            s -> Optional.ofNullable(PropertyService.getParameterType(s)).map(ParameterType::getDefaultValue).map(Object::toString).orElse(""));

    static {
        PARAMETER_WRITERS.add(new WindowsExeParameterWriter());
        PARAMETER_WRITERS.add(new WindowsBatParameterWriter());
    }

    private static final PropertyService INSTANCE = new PropertyService();

    private PropertyService() {
        // static class
    }

    /**
     * Reads the configuration file if allowed by the
     * {@link com.owc.singularity.engine.pipeline.ExecutionMode}.
     */
    public static void init() {
        if (!initialized) {
            registerAllParametersWithDefaultValues();
            // then try to read configuration from file system if allowed to do so.
            if (SingularityEngine.getExecutionMode().canAccessFilesystem()) {
                List<File> configFilesList = new LinkedList<>();

                // adding global config file defined by parameter or environment variable
                File globalConfigFile = getGlobalConfigFile(SINGULARITY_CONFIG_FILE_NAME);
                if (globalConfigFile != null) {
                    configFilesList.add(globalConfigFile);
                }

                // add user specific config file from .Singularity directory
                File userConfigFile = FileSystemService.getUserConfigFile(SINGULARITY_CONFIG_FILE_NAME);
                configFilesList.add(userConfigFile);


                // finally read all collected files if existing
                for (File configFile : configFilesList) {
                    if (configFile.exists()) {
                        try {
                            setParameters(configFile);
                            LOGGER.debug("Trying rcfile '" + configFile.getAbsolutePath() + "'...success");
                        } catch (IOException e) {
                            LOGGER.debug("Trying rcfile '" + configFile.getAbsolutePath() + "'...skipped");
                        }
                    }
                }
            } else {
                LOGGER.debug("Execution mode " + SingularityEngine.getExecutionMode() + " does not permit file access. Ignoring all rcfiles.");
            }
            // ENFORCED_PARAMETER.init();

            // set flag to avoid second call
            initialized = true;
        }
    }

    private static void registerAllParametersWithDefaultValues() {
        ParameterType parameterType = new ParameterTypeCategory(GENERAL_LOCALE_LANGUAGE, "", GENERAL_LOCALE_LANGUAGE_VALUES, 0);
        parameterType.setHidden(true);
        registerParameter(parameterType);
        registerParameter(new ParameterTypeDateFormat(GENERAL_LOCALE_DATE_FORMAT, ""));
        registerParameter(new ParameterTypeInt(GENERAL_FRACTIONDIGITS_NUMBERS, "", 0, Integer.MAX_VALUE, DEFAULT_GENERAL_FRACTION_DIGITS));
        parameterType = new ParameterTypeInt(GENERAL_FRACTIONDIGITS_PERCENT, "", 0, Integer.MAX_VALUE, 2);
        parameterType.setHidden(true);
        registerParameter(parameterType);
        registerParameter(new ParameterTypeInt(GENERAL_MAX_TEST_ROWS, "", 0, Integer.MAX_VALUE, 100));
        registerParameter(new ParameterTypeBoolean(GENERAL_DEBUGMODE, "", false));
        registerParameter(new ParameterTypeString(GENERAL_DEFAULT_ENCODING, "", GlobalDefaults.SYSTEM_ENCODING_NAME));
        registerParameter(new ParameterTypeCategory(GENERAL_TIME_ZONE, "", Tools.getAllTimeZones(), Tools.SYSTEM_TIME_ZONE));
        registerParameter(new ParameterTypeCategory(GENERAL_LOCALE, "", Tools.getAllLocaleNames(), Tools.SYSTEM_DEFAULT_LOCALE));

        // registerParameter(new
        // ParameterTypeBoolean(CapabilityProvider.PROPERTY_SINGULARITY_GENERAL_CAPABILITIES_WARN,
        // "", false));

        registerParameter(new ParameterTypeBoolean(MODULES_ENABLED, "", true));
        registerParameter(new ParameterTypeDirectory(INIT_PLUGINS_LOCATION, "", true));

        // System parameter types
        registerParameter(new ParameterTypeInt(TARGET_OFF_HEAP_MEMORY, "", 1024, Integer.MAX_VALUE, true), "system");

        registerParameter(new ParameterTypeInt(WebServiceTools.WEB_SERVICE_TIMEOUT, "", 1, Integer.MAX_VALUE, 20000), "system");

        registerParameter(new ParameterTypeBoolean(NETWORK_FOLLOW_HTTP_TO_HTTPS, "", true), "system");
        registerParameter(new ParameterTypeBoolean(NETWORK_FOLLOW_HTTPS_TO_HTTP, "", true), "system");

        registerParameter(new ParameterTypeString(NETWORK_DEFAULT_USER_AGENT, "", true), "system");
        registerParameter(new ParameterTypeInt(GENERAL_RANDOMSEED, "", -1, Integer.MAX_VALUE, RandomGenerator.DEFAULT_SEED));
    }

    /**
     * This method sets the given parameter to the given value. If the parameter is not known, yet,
     * it will be added as a defined parameter with a default scope.
     */
    public static void setParameterValue(ParameterType type, String value) {
        Parameter parameter = PARAMETER_MAP.get(type.getKey());
        if (parameter == null) {
            parameter = new Parameter(type, value);
            PARAMETER_MAP.put(type.getKey(), parameter);
        }
        setParameterValue(type.getKey(), value);
    }

    /**
     * This method sets the parameter with the given key to the given value. If the parameter does
     * not yet exist a new non defined parameter will be created. The value can then be retrieved,
     * but it won't be saved in any configuration file and will be lost after restarting
     * SingularityEngine.
     * <p>
     * For compatibility reasons this will set the parameter also in the System properties. This
     * might be removed in further versions.
     */
    public static void setParameterValue(String key, String value) {
        // Check if the value is enforced
        String adminValue = ENFORCED_PARAMETER.getProperty(key);
        if (adminValue != null && !adminValue.equals(value)) {
            return;
        }

        // setting parameter
        Parameter parameter = PARAMETER_MAP.get(key);
        if (parameter == null) {
            parameter = new Parameter(value);
            PARAMETER_MAP.put(key, parameter);
        }
        parameter.setValue(value);

        informListenerOfChange(key, value);
    }

    /**
     * This method returns the value of the given parameter or null if this parameter is unknown.
     * For compatibility reasons this will return defined parameters as well as undefined.
     */
    public static String getParameterValue(String key) {
        Parameter parameter = PARAMETER_MAP.get(key);
        if (parameter != null) {
            return parameter.getValue();
        }
        return null;
    }

    /**
     * This method returns the value of the given parameter or the given default if this parameter
     * is unknown. For compatibility reasons this will return defined parameters as well as
     * undefined.
     */
    public static String getParameterValue(String key, String defaultValue) {
        Parameter parameter = PARAMETER_MAP.get(key);
        if (parameter != null) {
            return parameter.getValue();
        }
        return defaultValue;
    }

    /**
     * This will return the group of the parameter with the given key. If the key is unknown, null
     * will be returned.
     */
    public static String getGroupKey(String key) {
        Parameter parameter = PARAMETER_MAP.get(key);
        if (parameter != null) {
            return parameter.getGroup();
        }
        return null;
    }

    /**
     * This method returns the type of the defined parameter identified by key or null if this key
     * is unknown.
     */
    public static ParameterType getParameterType(String key) {
        Parameter parameter = PARAMETER_MAP.get(key);
        if (parameter != null) {
            return parameter.getType();
        }
        return null;
    }

    /**
     * This method returns all keys of all parameters, implicit as well as defined ones.
     */
    public static Collection<String> getParameterKeys() {
        return PARAMETER_MAP.keySet();
    }

    /**
     * This method will return a Collection of all keys of defined parameter types. Undefined types
     * will not be returned.
     */
    public static Collection<String> getDefinedParameterKeys() {
        LinkedList<String> keys = new LinkedList<>();
        for (Entry<String, Parameter> entry : PARAMETER_MAP.entrySet()) {
            if (entry.getValue().isDefined()) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }

    /**
     * This method will return a Collection of all keys of defined parameter types. Undefined types
     * will not be returned.
     */
    public static Set<ParameterType> getDefinedParameterTypes() {
        HashSet<ParameterType> types = new HashSet<>();
        for (Entry<String, Parameter> entry : PARAMETER_MAP.entrySet()) {
            if (entry.getValue().isDefined()) {
                types.add(entry.getValue().getType());
            }
        }
        return types;
    }

    /**
     * This sets the parameters to the values given by a properties file denoted by the given file
     * object.
     *
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void setParameters(File file) throws FileNotFoundException, IOException {
        setParameters(new FileInputStream(file));
    }

    /**
     * This method reads the input stream that streams in a properties file and sets the parameter
     * values accordingly. If the stream cannot be accessed an exception will be thrown.
     *
     * @throws IOException
     */
    public static void setParameters(InputStream in) throws IOException {
        Properties properties = new Properties();
        properties.load(in);

        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            setParameterValue((String) entry.getKey(), (String) entry.getValue());
        }

        try {
            in.close();
        } catch (IOException e) {
            // can't help it
        }
    }

    /**
     * This returns the file with the given fileName from the directory denoted by first the
     * Parameter named {@value #SYSTEM_PROPERTY_CONFIG_DIR} and if this one is not defined by the
     * environment variable {@value #ENVIRONMENT_VARIABLE_CONFIG_DIR}. If neither one is defined,
     * null is returned.
     */
    public static File getGlobalConfigFile(String fileName) {
        File dir = getGlobalConfigDir();
        if (dir != null) {
            File result = new File(dir, fileName);
            if (result.exists()) {
                if (result.canRead()) {
                    return result;
                } else {
                    LOGGER.warn("Config file " + result.getAbsolutePath() + " is not readable.");
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private static File getGlobalConfigDir() {
        String configDir = System.getProperty(SYSTEM_PROPERTY_CONFIG_DIR);
        if (configDir == null) {
            configDir = System.getenv(ENVIRONMENT_VARIABLE_CONFIG_DIR);
        }
        if (configDir != null) {
            File dir = new File(configDir);
            if (dir.exists()) {
                if (dir.canRead()) {
                    return dir;
                } else {
                    LOGGER.warn("Directory " + dir.getAbsolutePath() + " specified by environment variable " + ENVIRONMENT_VARIABLE_CONFIG_DIR
                            + " is not readable.");
                    return null;
                }
            } else {
                LOGGER.warn(
                        "Directory " + dir.getAbsolutePath() + " specified by environment variable " + ENVIRONMENT_VARIABLE_CONFIG_DIR + " does not exist.");
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * This method will save all currently known defined parameter types into the version and os
     * dependent config file in the user's SingularityEngine directory. This method will also
     * generate files that are needed for preStartParameter that affect as environment variables the
     * staring JVM. If file access isn't allowed, nothing is done at all.
     */
    public static void saveParameters() {
        saveParameters(FileSystemService.getMainUserConfigFile());

        // now export properties using all additional registered writers
        for (ParameterWriter writer : PARAMETER_WRITERS) {
            writer.writeParameters(PARAMETER_MAP);
        }

        informListenerOfSave();
    }

    /**
     * This method will save all currently known defined parameters into the given file. If file
     * access isn't allowed by the execution mode, nothing is done.
     * <p>
     * Please notice that in contrast to {@link #saveParameters()}, no preStartParameter files are
     * written.
     */
    public static void saveParameters(File configFile) {
        if (!SingularityEngine.getExecutionMode().canAccessFilesystem()) {
            LogService.getRoot()
                    .debug("com.owc.singularity.engine.tools.PropertyService.ignoring_request_to_save_properties", SingularityEngine.getExecutionMode());
            return;
        }

        // building properties object to save to file
        Properties properties = new Properties();
        for (Entry<String, Parameter> entry : PARAMETER_MAP.entrySet()) {
            Parameter parameter = entry.getValue();
            String value = parameter.getValue();
            String key = entry.getKey();
            // don't store enforced parameters
            if (isValueEnforced(key)) {
                value = ENFORCED_PARAMETER.getOriginalValue(key);
            }
            if (value != null) {
                properties.put(key, value);
            }
        }
        BufferedOutputStream out = null;
        try (FileOutputStream fos = new FileOutputStream(configFile)) {
            out = new BufferedOutputStream(fos);
            properties.store(out, "");
        } catch (IOException e) {
            LogService.getRoot().warn("com.owc.singularity.engine.tools.PropertyService.writing_user_properties_error", e.getMessage(), e);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    LogService.getRoot().warn("com.owc.singularity.engine.tools.PropertyService.closing_user_properties_file_error", e.getMessage(), e);
                }
            }
        }
    }

    /**
     * This method lets register the given {@link ParameterType} with defaults settings. To have
     * more control over the scope and group name refer to any other registerParameter method.
     * <p>
     * If an implicit Parameter is already defined with the key, it will be converted to an explicit
     * without losing the data.
     */
    public static void registerParameter(ParameterType type) {
        registerParameter(type, null, new ParameterScope());
    }

    /**
     * This method allows to set the group explicitly rather than deriving it from the key.
     */
    public static void registerParameter(ParameterType type, String group) {
        registerParameter(type, group, new ParameterScope());
    }

    /**
     * This method can be used to register the given {@link ParameterType} on the given
     * {@link ParameterScope}. This method can be used to define for example preStartParameters like
     * memory size...
     */
    public static void registerParameter(ParameterType type, String group, ParameterScope scope) {
        Parameter parameter = PARAMETER_MAP.get(type.getKey());
        if (parameter == null) {
            parameter = group == null ? new Parameter(type) : new Parameter(type, group);
            PARAMETER_MAP.put(type.getKey(), parameter);
        } else {
            parameter.setType(type);
            if (group != null) {
                parameter.setGroup(group);
            }
        }
        parameter.setScope(scope);
    }

    private static void informListenerOfChange(String key, String value) {
        for (ParameterChangeListener listener : PARAMETER_LISTENERS) {
            try {
                listener.informParameterChanged(key, value);
            } catch (Throwable e) {
                LogService.getRoot().warn("com.owc.singularity.engine.tools.PropertyService.listener_error", e);
            }
        }
    }

    private static void informListenerOfSave() {
        for (ParameterChangeListener listener : PARAMETER_LISTENERS) {
            try {
                listener.informParameterSaved();
            } catch (Throwable e) {
                LogService.getRoot().warn("com.owc.singularity.engine.tools.PropertyService.listener_error", e);
            }
        }
    }

    /**
     * This will add the given listener to the list of listers. It will be informed whenever a
     * setting is changed.
     */
    public static void registerParameterChangeListener(ParameterChangeListener listener) {
        PARAMETER_LISTENERS.add(listener);
    }

    /**
     * This method will remove the given listener from the list. It won't be informed anymore.
     */
    public static void removeParameterChangeListener(ParameterChangeListener listener) {
        PARAMETER_LISTENERS.remove(listener);
    }

    /**
     * Check if the value is enforced by the administrator
     *
     * @param key
     *            the key to check
     * @return {@code true} if the value is enforced
     */
    public static boolean isValueEnforced(String key) {
        return ENFORCED_PARAMETER.containsKey(key);
    }

    /**
     * Check if admin enforced settings exists
     *
     * @return {@code true} if values are enforced
     */
    public static boolean hasEnforcedValues() {
        return !ENFORCED_PARAMETER.isEmpty();
    }
}
