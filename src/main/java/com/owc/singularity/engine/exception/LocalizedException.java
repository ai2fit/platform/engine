package com.owc.singularity.engine.exception;

import java.util.function.Supplier;

/**
 * Represents an exception whose message is provided through a {@link Supplier<String> message
 * supplier} which will be resolved only once at the first call of {@link #getMessage()}. Subsequent
 * calls will return the cached message.
 *
 * @author Hatem Hamad
 */
public class LocalizedException extends Exception {

    private static final Supplier<String> EMPTY_MESSAGE_SUPPLIER = () -> null;
    private final Supplier<String> localizedMessageSupplier;
    private String localizedMessage;

    public LocalizedException(Supplier<String> localizedMessageSupplier, Throwable cause) {
        super(null, cause);
        this.localizedMessageSupplier = localizedMessageSupplier == null ? EMPTY_MESSAGE_SUPPLIER : localizedMessageSupplier;
    }

    @Override
    public String getMessage() {
        if (localizedMessage == null) {
            localizedMessage = localizedMessageSupplier.get();
        }
        return localizedMessage;
    }
}
