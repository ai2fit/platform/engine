/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine;


import static org.reflections.util.ReflectionUtilsPredicates.withClassModifier;

import java.lang.ref.WeakReference;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.parameter.ParameterChangeListener;


/**
 * This class maintains all registered operators in the current context. There exists a listener
 * concept that will alert all listeners, if new operators are added or removed.
 * <p>
 * It provides convenience methods for creating new {@link Operator}s. See the description of the
 * {@link #createOperator(Class)} method.
 *
 * <p>
 * This class also reads the xml definitions of the SingularityEngine Studio Core and Extension
 * operators. These descriptions are entries in a XML file like OperatorsCore.xml.
 * </p>
 *
 * @author Ingo Mierswa, Simon Fischer, Sebastian Land
 */
public class OperatorService {

    private static final Logger log = LogManager.getLogger(OperatorService.class);

    private OperatorService() {
        // static class
    }

    /**
     * The interface for all Listener to the {@link OperatorService}.
     *
     * @author Sebastian Land
     */
    public interface OperatorServiceListener {

        /**
         * This will be called if an operator is registered.
         * <p>
         * ATTENTION!!! You must ensure that bundle might be null!
         */
        void operatorRegistered(OperatorDescription description);

        /**
         * This method will be called if an operator is removed.
         */
        void operatorUnregistered(OperatorDescription description);
    }

    public static final String OPERATOR_BLACKLIST_KEY = "singularity.operator.blacklist";

    private static Set<String> operatorBlacklist = Collections.emptySet();

    private static final LinkedList<WeakReference<OperatorServiceListener>> listeners = new LinkedList<>();
    /**
     * Maps operator keys as defined in the OperatorsCore.xml to operator descriptions.
     */
    private static final Map<String, OperatorDescription> KEYS_TO_DESCRIPTIONS_MAP = new ConcurrentHashMap<>();
    private static final Map<Class<? extends Operator>, OperatorDescription> CLASS_TO_DESCRIPTIONS_MAP = new ConcurrentHashMap<>();

    /** Set of all Operator classes registered. */
    private static final Set<Class<? extends Operator>> REGISTERED_OPERATOR_CLASSES = Collections.synchronizedSet(new HashSet<>());

    /** Maps deprecated operator names to new names. */
    private static final Map<String, String> DEPRECATION_MAP = new HashMap<>();

    /** Updates the operator blacklist when it changes */
    private static final ParameterChangeListener UPDATE_BLACKLIST_LISTENER = new ParameterChangeListener() {

        @Override
        public void informParameterSaved() {
            // not necessary
        }

        @Override
        public void informParameterChanged(String key, String value) {
            if (OPERATOR_BLACKLIST_KEY.equals(key)) {
                updateBlacklist();
            }
        }
    };


    public static void init() {
        log.debug("Scanning for operators...");
        long startTime = System.nanoTime();
        // reset
        KEYS_TO_DESCRIPTIONS_MAP.clear();
        CLASS_TO_DESCRIPTIONS_MAP.clear();
        REGISTERED_OPERATOR_CLASSES.clear();

        // annotated operators
        Reflections reflections = ModuleService.getMajorReflections();
        reflections.get(Scanners.TypesAnnotated.with(DefinesOperator.class).asClass(reflections.getConfiguration().getClassLoaders()))
                .stream()
                .parallel()
                .filter(withClassModifier(Modifier.ABSTRACT).negate())
                .filter(Operator.class::isAssignableFrom)
                .map(c -> {
                    DefinesOperator annotation = c.getAnnotation(DefinesOperator.class);
                    // noinspection unchecked
                    return new OperatorDescription(annotation.module(), annotation.key(), annotation.group(), (Class<? extends Operator>) c, annotation.name(),
                            annotation.shortName().equals(DefinesOperator.NO_VALUE) ? annotation.name() : annotation.shortName(), annotation.icon());
                })
                .forEach(OperatorService::registerOperator);


        log.debug("Registered {} operators", REGISTERED_OPERATOR_CLASSES::size);

        updateBlacklist();
        PropertyService.removeParameterChangeListener(UPDATE_BLACKLIST_LISTENER);
        PropertyService.registerParameterChangeListener(UPDATE_BLACKLIST_LISTENER);
        log.debug("Scanning for operators...DONE (took {})", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - startTime)));
    }

    private static void updateBlacklist() {
        String blacklistString = PropertyService.getParameterValue(OPERATOR_BLACKLIST_KEY);
        if (blacklistString != null) {
            Set<String> newOperatorBlacklist = new HashSet<>(Arrays.asList(blacklistString.trim().split("\\s*,\\s*")));

            // Calculate the Symmetric Difference between the sets
            Set<String> symmetricDifference = new HashSet<>(operatorBlacklist);
            symmetricDifference.addAll(newOperatorBlacklist);
            Set<String> tmp = new HashSet<>(operatorBlacklist);
            tmp.retainAll(newOperatorBlacklist);
            symmetricDifference.removeAll(tmp);

            operatorBlacklist = newOperatorBlacklist;
        }
    }

    /**
     * Registers the given operator description. Please note that two different descriptions must
     * not have the same name. Otherwise the second description overwrite the first in the
     * description map.
     * <p>
     * If there's no icon defined for the given {@link OperatorDescription}, the group icon will be
     * set here.
     *
     * @param description
     *            the description of the operator
     */
    public static void registerOperator(OperatorDescription description) {
        // check if this operator was not registered earlier
        OperatorDescription oldDescription = KEYS_TO_DESCRIPTIONS_MAP.get(description.getFullyQuallifiedKey());
        if (oldDescription != null) {
            log.warn("Operator key {} was already registered for class {}. Overwriting with {}.", description.getFullyQuallifiedKey(),
                    oldDescription.getOperatorClass().getName(), description.getOperatorClass());
        }


        // register in maps
        KEYS_TO_DESCRIPTIONS_MAP.put(description.getFullyQuallifiedKey(), description);
        CLASS_TO_DESCRIPTIONS_MAP.put(description.getOperatorClass(), description);
        REGISTERED_OPERATOR_CLASSES.add(description.getOperatorClass());

        // inform listener
        invokeOperatorRegisteredListener(description);
    }

    /**
     * This method can be used to dynamically remove Operators from the number of defined operators.
     */
    public static void unregisterOperator(OperatorDescription description) {
        KEYS_TO_DESCRIPTIONS_MAP.remove(description.getFullyQuallifiedKey());
        CLASS_TO_DESCRIPTIONS_MAP.remove(description.getOperatorClass());
        REGISTERED_OPERATOR_CLASSES.remove(description.getOperatorClass());

        // inform all listener including GroupTree
        invokeOperatorUnregisteredListener(description);
    }


    /**
     * Returns a collection of all operator keys.
     */
    public static Set<String> getOperatorKeys() {
        return KEYS_TO_DESCRIPTIONS_MAP.keySet();
    }

    // ================================================================================
    // Operator Factory Methods
    // ================================================================================

    /**
     * Returns the operator descriptions for the operators which uses the given class. Performs a
     * linear search through all operator descriptions.
     */
    public static OperatorDescription[] getOperatorDescriptions(Class<?> clazz) {
        if (clazz == null) {
            return new OperatorDescription[0];
        }
        List<OperatorDescription> result = new ArrayList<>(1);
        for (OperatorDescription current : KEYS_TO_DESCRIPTIONS_MAP.values()) {
            if (current.getOperatorClass().equals(clazz)) {
                result.add(current);
            }
        }
        return result.toArray(new OperatorDescription[0]);
    }


    /**
     * Use this method to create an operator from the given class name (from operator description
     * file operators.xml, not from the Java class name). For most operators, is is recommended to
     * use the method {@link #createOperator(Class)} which can be checked during compile time. This
     * is, however, not possible for some generic operators like the Weka operators. In that case,
     * you have to use this method with the argument from the operators.xml file, e.g.
     * <tt>createOperator(&quot;J48&quot;)</tt> for a J48 decision tree learner.
     */
    public static Operator createOperator(String typeName) throws OperatorCreationException {
        OperatorDescription description = getOperatorDescription(typeName);
        if (description == null) {
            throw new OperatorCreationException(OperatorCreationException.NO_DESCRIPTION_ERROR, typeName, null);
        }
        return createOperator(description);
    }

    /** Use this method to create an operator of a given description object. */
    public static Operator createOperator(OperatorDescription description) throws OperatorCreationException {
        if (description == null)
            throw new OperatorCreationException(OperatorCreationException.ILLEGAL_ACCESS_ERROR, "null", new NullPointerException("Operator unknown"));
        Class<? extends Operator> clazz = description.getOperatorClass();
        String key = description.getFullyQuallifiedKey();
        if (!description.isEnabled()) {
            throw new OperatorCreationException(OperatorCreationException.OPERATOR_DISABLED_ERROR, key + "(" + clazz.getName() + ")", null);
        }
        try {
            java.lang.reflect.Constructor<? extends Operator> constructor = clazz.getConstructor();
            return constructor.newInstance();
        } catch (InstantiationException e) {
            throw new OperatorCreationException(OperatorCreationException.INSTANTIATION_ERROR, key + "(" + clazz.getName() + ")", e);
        } catch (IllegalAccessException e) {
            throw new OperatorCreationException(OperatorCreationException.ILLEGAL_ACCESS_ERROR, key + "(" + clazz.getName() + ")", e);
        } catch (NoSuchMethodException e) {
            throw new OperatorCreationException(OperatorCreationException.NO_CONSTRUCTOR_ERROR, key + "(" + clazz.getName() + ")", e);
        } catch (java.lang.reflect.InvocationTargetException e) {
            throw new OperatorCreationException(OperatorCreationException.CONSTRUCTION_ERROR, key + "(" + clazz.getName() + ")", e);
        } catch (Throwable t) {
            throw new OperatorCreationException(OperatorCreationException.INSTANTIATION_ERROR, "(" + clazz.getName() + ")", t);
        }
    }

    /**
     * <p>
     * Use this method to create an operator from an operator class. This is the only method which
     * ensures operator existence checks during compile time (and not during runtime) and the usage
     * of this method is therefore the recommended way for operator creation.
     * </p>
     *
     * <p>
     * It is, however, not possible to create some generic operators with this method (this mainly
     * applies to the Weka operators). Please use the method {@link #createOperator(String)} for
     * those generic operators.
     * </p>
     *
     * <p>
     * If you try to create a generic operator with this method, the OperatorDescription will not be
     * unique for the given class and an OperatorCreationException is thrown.
     * </p>
     *
     * <p>
     * Please note that is is not necessary to cast the operator to the desired class.
     * </p>
     */
    @SuppressWarnings("unchecked")
    public static <T extends Operator> T createOperator(Class<T> clazz) throws OperatorCreationException {
        return (T) createOperator(getOperatorDescription(clazz));
    }

    /**
     * Returns a replacement if the given operator class is deprecated, and null otherwise. The
     * deprecated Key is the key with that this operator was used in SingularityEngine 4.x
     */
    public static String getReplacementForDeprecatedClass(String deprecatedKey) {
        return DEPRECATION_MAP.get(deprecatedKey);
    }


    /*
     * Listener to changes in available Operators
     */
    /**
     * This method can be used to add an listener to the OperatorService that is informed whenever
     * the set of available operators changes. Internally WeakReferences are used so that there's no
     * need to deregister listeners.
     */
    public static void addOperatorServiceListener(OperatorServiceListener listener) {
        listeners.add(new WeakReference<>(listener));
    }

    /**
     * Checks if any blacklisted operators exist
     *
     * @return {@code true} if the blacklisted operators exist
     * @since 9.0.0
     */
    public static boolean hasBlacklistedOperators() {
        return !operatorBlacklist.isEmpty();
    }

    /**
     * Checks if the operator with the given key is blacklisted.
     * 
     * @param opKey
     *            operator key
     * @return {@code true} if the operator is blacklisted, or {@code false} if it is not
     *         blacklisted.
     * @since 9.0.0
     */
    public static boolean isOperatorBlacklisted(String opKey) {
        return operatorBlacklist.contains(opKey);
    }

    /**
     * This method will inform all listeners of a change in the available operators.
     */
    private static void invokeOperatorRegisteredListener(OperatorDescription description) {
        List<WeakReference<OperatorServiceListener>> listenersCopy = new LinkedList<>(listeners);
        for (WeakReference<OperatorServiceListener> listenerRef : listenersCopy) {
            OperatorServiceListener operatorServiceListener = listenerRef.get();
            if (operatorServiceListener != null) {
                operatorServiceListener.operatorRegistered(description);
            }
        }
        Iterator<WeakReference<OperatorServiceListener>> iterator = listenersCopy.iterator();
        while (iterator.hasNext()) {
            OperatorServiceListener operatorServiceListener = iterator.next().get();
            if (operatorServiceListener == null) {
                iterator.remove();
            }
        }

    }

    /**
     * This method will inform all listeners of a change in the available operators.
     */
    private static void invokeOperatorUnregisteredListener(OperatorDescription description) {
        List<WeakReference<OperatorServiceListener>> listenersCopy = new LinkedList<>(listeners);
        for (WeakReference<OperatorServiceListener> listenerRef : listenersCopy) {
            OperatorServiceListener operatorServiceListener = listenerRef.get();
            if (operatorServiceListener != null) {
                operatorServiceListener.operatorUnregistered(description);
            }
        }
        Iterator<WeakReference<OperatorServiceListener>> iterator = listenersCopy.iterator();
        while (iterator.hasNext()) {
            OperatorServiceListener operatorServiceListener = iterator.next().get();
            if (operatorServiceListener == null) {
                iterator.remove();
            }
        }
    }

    /**
     * This method returns the operator description for the given operator class
     * 
     * @param operatorClass
     *            the class implementing the operator
     * @return the description
     */
    public static OperatorDescription getOperatorDescription(Class<? extends Operator> operatorClass) {
        return CLASS_TO_DESCRIPTIONS_MAP.computeIfAbsent(operatorClass, OperatorService::getOperatorDescriptionUsingAnnotation);
    }

    /**
     * Used to extract a new {@link OperatorDescription} from the given class annotations.
     * 
     * @param operatorClass
     *            the implementing class
     * @return an {@link OperatorDescription} from the {@link DefinesOperator} annotation, null if
     *         annotation not found.
     */
    private static OperatorDescription getOperatorDescriptionUsingAnnotation(Class<? extends Operator> operatorClass) {
        DefinesOperator annotation = operatorClass.getAnnotation(DefinesOperator.class);
        if (annotation == null)
            return null;
        return new OperatorDescription(annotation.module(), annotation.key(), annotation.group(), operatorClass, annotation.name(),
                annotation.shortName().equals(DefinesOperator.NO_VALUE) ? annotation.name() : annotation.shortName(), annotation.icon());
    }

    /**
     * Returns the operator description for a given key from the operators.xml file, e.g.
     * &quot;read_csv&quot; for the Read CSV operator. Returns null if operator key is unknown.
     */
    public static OperatorDescription getOperatorDescription(String key) {
        return KEYS_TO_DESCRIPTIONS_MAP.get(key);
    }

    /**
     * This returns the human-readable name of the given group key.
     * 
     * @param group
     *            the group key, dot separated list of keys
     * @return human-readable name
     */
    public static String getGroupName(String group) {
        // TODO
        return group;
    }

}
