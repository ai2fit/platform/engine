package com.owc.singularity.engine.visualization.datatable;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.partition.Partition;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.tools.BiDirectionalIntStringMapping;


/**
 * This class can be used to use an example set as data table. The data is directly read from the
 * example set instead of building a copy. Please note that the method for adding new rows is not
 * supported by this type of data tables.
 *
 * @author Ingo Mierswa
 */
public class ExampleSetDataTable extends AbstractDataTable {

    /**
     * This class allows to use {@link com.owc.singularity.engine.object.data.exampleset.Example}s
     * as basis for {@link com.owc.singularity.engine.visualization.datatable.DataTableRow}.
     * 
     * @author Ingo Mierswa
     */
    public class Example2DataTableRowWrapper implements DataTableRow {

        private final int row;
        private final ExampleSetDataTable table;

        /**
         * Creates a new wrapper. If the Id Attribute is null, the DataTableRow will not contain an
         * Id.
         */
        public Example2DataTableRowWrapper(int row, ExampleSetDataTable table) {
            this.row = row;
            this.table = table;
        }

        @Override
        public String getId() {
            if (idAttribute == null) {
                return null;
            } else {
                switch (idAttribute.getValueType()) {
                    case NOMINAL:
                        return this.table.exampleSet.getNominalValue(row, idAttribute.getIndex());
                    case NUMERIC:
                        double numericIdValue = this.table.exampleSet.getNumericValue(row, idAttribute.getIndex());
                        if (ValueType.isMissing(numericIdValue))
                            return ValueType.MISSING_NOMINAL;
                        return Double.toString(numericIdValue);
                    case TIMESTAMP:
                        long timestampIdValue = this.table.exampleSet.getTimestampValue(row, idAttribute.getIndex());
                        if (ValueType.isMissing(timestampIdValue))
                            return ValueType.MISSING_NOMINAL;
                        return Double.toString(timestampIdValue);
                }
            }
            return null;
        }

        @Override
        public double getValue(int column) {
            Attribute attribute = table.allAttributes.get(column);
            return switch (attribute.getValueType()) {
                case NOMINAL -> {
                    String nominalValue = this.table.exampleSet.getNominalValue(row, attribute.getIndex());
                    if (ValueType.isMissing(nominalValue))
                        yield ValueType.MISSING_NUMERIC;
                    yield table.nominalMappings[column].mapString(nominalValue);
                }
                case NUMERIC -> {
                    double numericValue = table.exampleSet.getNumericValue(row, attribute.getIndex());
                    if (Double.isInfinite(numericValue))
                        yield ValueType.MISSING_NUMERIC;
                    yield numericValue;
                }
                case TIMESTAMP -> {
                    long timestampValue = table.exampleSet.getTimestampValue(row, attribute.getIndex());
                    if (ValueType.isMissing(timestampValue))
                        yield ValueType.MISSING_NUMERIC;
                    yield timestampValue;
                }
            };
        }

        @Override
        public int getNumberOfValues() {
            return allAttributes.size();
        }
    }

    private ExampleSet exampleSet;

    private List<Attribute> allAttributes = new ArrayList<>();
    private final BiDirectionalIntStringMapping[] nominalMappings;
    private final int numberOfRegularAttributes;


    private final Attribute idAttribute;

    public ExampleSetDataTable(ExampleSet exampleSet) {
        this(exampleSet, true);
    }

    /**
     * @param ignoreId
     *            If this variable is true, the id will not be visible in the data table.
     */
    public ExampleSetDataTable(ExampleSet exampleSet, boolean ignoreId) {
        super("Data Table");
        this.exampleSet = exampleSet;

        for (Attribute attribute : exampleSet.getAttributes()) {
            allAttributes.add(attribute);
        }

        this.idAttribute = exampleSet.getAttributes().getId();

        Iterator<Attribute> specialAttributeIterator = exampleSet.getAttributes().specialAttributes();
        while (specialAttributeIterator.hasNext()) {
            Attribute specialAttribute = specialAttributeIterator.next();
            if (!ignoreId || idAttribute == null || !idAttribute.getName().equals(specialAttribute.getName())) {
                allAttributes.add(specialAttribute);
            }
        }

        this.numberOfRegularAttributes = exampleSet.getAttributes().size();
        nominalMappings = allAttributes.stream().parallel().map(attribute -> {
            if (attribute.isNominal()) {
                return new BiDirectionalIntStringMapping(exampleSet.streamNominalAttribute(attribute).sequential());
            }
            return null;
        }).toArray(BiDirectionalIntStringMapping[]::new);
    }

    public ExampleSetDataTable(ExampleSetDataTable dataTableExampleSetAdapter) {
        super(dataTableExampleSetAdapter.getName());
        this.exampleSet = dataTableExampleSetAdapter.exampleSet; // shallow clone
        this.allAttributes = dataTableExampleSetAdapter.allAttributes; // shallow clone
        this.numberOfRegularAttributes = dataTableExampleSetAdapter.numberOfRegularAttributes;
        this.idAttribute = dataTableExampleSetAdapter.idAttribute; // shallow clone
        this.nominalMappings = dataTableExampleSetAdapter.nominalMappings;
    }

    @Override
    public int getNumberOfSpecialColumns() {
        return allAttributes.size() - numberOfRegularAttributes;
    }

    @Override
    public boolean isSpecial(int index) {
        return index >= numberOfRegularAttributes;
    }

    @Override
    public boolean isNominal(int index) {
        return allAttributes.get(index).getValueType().equals(ValueType.NOMINAL);
    }

    @Override
    public boolean isTime(int index) {
        return false;
    }

    @Override
    public boolean isDate(int index) {
        return false;
    }

    @Override
    public boolean isTimestamp(int index) {
        return allAttributes.get(index).getValueType().equals(ValueType.TIMESTAMP);
    }

    @Override
    public boolean isNumeric(int index) {
        return allAttributes.get(index).getValueType().equals(ValueType.NUMERIC);
    }

    public String getLabelName() {
        Attribute label = this.exampleSet.getAttributes().getLabel();
        if (label != null) {
            return label.getName();
        } else {
            return null;
        }
    }

    public String getClusterName() {
        Attribute cluster = this.exampleSet.getAttributes().getCluster();
        if (cluster != null) {
            return cluster.getName();
        } else {
            return null;
        }
    }

    public boolean isLabelNominal() {
        Attribute label = this.exampleSet.getAttributes().getLabel();
        if (label != null) {
            return label.isNominal();
        } else {
            return false; // if there is no label, it should not be nominal
        }
    }

    @Override
    public String mapIndex(int column, int value) {
        try {
            return nominalMappings[column].mapIndex(value);
        } catch (RuntimeException e) {
            // Can throw AttributeTypeException in case mapping is empty
            return "?";
        }
    }

    @Override
    public int mapString(int column, String value) {
        return nominalMappings[column].get(value);
    }

    @Override
    public int getNumberOfValues(int column) {
        return nominalMappings[column].size();
    }

    @Override
    public String getColumnName(int i) {
        return allAttributes.get(i).getName();
    }

    @Override
    public int getColumnIndex(String name) {
        for (int i = 0; i < allAttributes.size(); i++) {
            if (allAttributes.get(i).getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean isSupportingColumnWeights() {
        return false;
    }

    @Override
    public double getColumnWeight(int column) {
        return Double.NaN;
    }

    @Override
    public int getNumberOfColumns() {
        return this.allAttributes.size();
    }

    @Override
    public void add(DataTableRow row) {
        throw new RuntimeException("DataTableExampleSetAdapter: adding new rows is not supported!");
    }

    @Override
    public DataTableRow getRow(int row) {
        return new Example2DataTableRowWrapper(row, this);
    }

    @Override
    public Iterator<DataTableRow> iterator() {
        return exampleSet.streamRows().mapToObj(this::getRow).iterator();
    }

    @Override
    public int getNumberOfRows() {
        return this.exampleSet.size();
    }

    @Override
    public DataTable sample(int newSize) {
        ExampleSetDataTable result = new ExampleSetDataTable(this);

        double ratio = (double) newSize / (double) getNumberOfRows();
        try {
            result.exampleSet = exampleSet.transform()
                    .filter(Partition.splitByRandom(this.exampleSet, new double[] { ratio, 1d - ratio }, Partition.SHUFFLED_SAMPLING, false, 0, false)
                            .getForSingleSubset(0))
                    .transform();
        } catch (OperatorException e) {
            return result;
        }
        return result;
    }

}
