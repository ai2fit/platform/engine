/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine;


import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.Reporter;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.tools.*;


/**
 * The IOObjectService is the basic provider for all registered IOObjects and their reporters. All
 * {@link IOObject}s which want to provide a Reportable for visualization and reporting must place
 * an entry in the <code>ioobjects.xml</xml> file in order to allow for retrieval.
 * 
 * @author Ingo Mierswa, Nils Woehler
 */
public class IOObjectService {

    private static final Logger log = LogManager.getLogger(IOObjectService.class);

    private static final Set<String> objectNames = new TreeSet<>();

    private static final Map<String, Class<? extends IOObject>> objectClassesByReportableName = new HashMap<>();

    private static final Map<String, Class<? extends IOObject>> objectClassesByClassName = new HashMap<>();

    /** Set of names of reportable objects. */
    private static final Set<String> reportableMap = new HashSet<>();
    private static final Map<String, Map<String, Class<? extends Reporter>>> reportableNameToReportableClasses = new HashMap<>();
    private static final Map<String, List<Reporter>> objectReporters = new HashMap<>();

    private static final Map<Class<?>, String> class2NameMap = new HashMap<>();

    public static void registerReporters(String owner, InputStream in, ClassLoader classLoader) {
        log.trace("Loading reporters from {}", owner);
        try {
            Document document = XMLTools.createDocumentBuilder().parse(in);
            Element ioObjectsElement = document.getDocumentElement();
            if (ioObjectsElement.getTagName().equals("ioobjects")) {
                NodeList ioObjectNodes = ioObjectsElement.getElementsByTagName("ioobject");
                for (int i = 0; i < ioObjectNodes.getLength(); i++) {
                    Node ioObjectNode = ioObjectNodes.item(i);
                    if (ioObjectNode instanceof Element ioObjectElement) {

                        String name = ioObjectElement.getAttribute("name");
                        String className = ioObjectElement.getAttribute("class");
                        List<String> reportables = new LinkedList<>();

                        NodeList reportableNodes = ioObjectElement.getElementsByTagName("reporter");
                        for (int k = 0; k < reportableNodes.getLength(); k++) {
                            Node reportableNode = reportableNodes.item(k);
                            if (reportableNode instanceof Element reportableElement) {
                                String reportableName = reportableElement.getTextContent();
                                reportables.add(reportableName);
                            }
                        }

                        registerReporters(name, className, classLoader, reportables);
                    }
                }
            } else {
                log.warn(
                        "Loading reporters from {}: Cannot initialize io object description: Outermost tag of an ioobjects.xml definition must be <ioobjects>!",
                        owner);
            }
        } catch (XMLParserException e) {
            log.warn("Loading reporters from {}: Cannot initialize io object description due to an exception", owner, e);
        } catch (IOException | SAXException e) {
            log.warn("Loading reporters from {}: Cannot initialize io object description: Cannot parse document", owner, e);
        }
    }

    @SuppressWarnings("unchecked")
    public static synchronized void registerReporters(String reportableName, String className, ClassLoader classLoader, List<String> reportableClassNames) {
        objectNames.add(reportableName);

        try {

            Class<? extends IOObject> clazz = (Class<? extends IOObject>) Class.forName(className, true, classLoader);

            objectClassesByReportableName.put(reportableName, clazz);
            objectClassesByClassName.put(className, clazz);
            class2NameMap.put(clazz, reportableName);
            List<Reporter> reporters = new LinkedList<>();
            if (!reportableClassNames.isEmpty())
                reportableMap.add(reportableName);
            Map<String, Class<? extends Reporter>> reportableClassMap = new HashMap<>();
            for (String reportableClassName : reportableClassNames) {
                Class<? extends Reporter> reportableClass = (Class<? extends Reporter>) Class.forName(reportableClassName, true, classLoader);
                Reporter reporter = reportableClass.getDeclaredConstructor().newInstance();
                reporters.add(reporter);
                reportableClassMap.put(reporter.getName(), reportableClass);
            }
            reportableNameToReportableClasses.put(reportableName, reportableClassMap);
            objectReporters.put(reportableName, reporters);

        } catch (ClassNotFoundException e) {
            log.warn("Cannot register reportable. Unknown class '{}'", className, e);
        } catch (Throwable e) {
            log.warn("Cannot register reportable due to an exception", e);

        }
    }

    public static Set<String> getAllIOObjectNames() {
        return objectNames;
    }

    public static Set<String> getAllReportableObjectNames() {
        Set<String> result = new TreeSet<>();
        for (String name : objectNames) {
            if (reportableMap.contains(name)) {
                result.add(name);
            }
        }
        return result;
    }

    /**
     * Returns the Reportable name for objects of the given class.
     *
     * @return the name of the IOObject as specified in the ioobjectsXXX.xml file. Returns null if
     *         the object is not registered
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static String getName(Class<?> clazz) {
        String result = class2NameMap.get(clazz);
        if (result == null) {

            Class<?> parentClass = new DominatingClassFinder().findNextDominatingClass(clazz, class2NameMap.keySet());

            if (parentClass == null) {
                return null;
            } else {
                return class2NameMap.get(parentClass);
            }
        }
        return result;
    }

    /**
     * This returns the highest super class of the report type with the given reportable name.
     */
    public static Class<? extends IOObject> getIOObjectClass(String reportableName) {
        return objectClassesByReportableName.get(reportableName);
    }

    /**
     * This returns the highest super class of the report type with the given class name.
     */
    public static Class<? extends IOObject> getClassByClassName(String className) {
        return objectClassesByClassName.get(className);
    }

    public static Reporter createReportable(IOObject ioobject, String reporterName) {
        String reportableName = IOObjectService.getName(ioobject.getClass());
        Map<String, Class<? extends Reporter>> reportableClassMap = reportableNameToReportableClasses.get(reportableName);
        if (reportableClassMap == null) {
            throw new IllegalArgumentException("Illegal reportable name: " + reporterName);
        }
        Class<? extends Reporter> reportableClass = reportableClassMap.get(reporterName);
        if (reportableClass == null) {
            throw new IllegalArgumentException("Illegal reportable name: " + reporterName);
        }
        try {
            return reportableClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to create reportable: " + e, e);
        }
    }

    public static List<Reporter> getReporters(String reportableName) {
        List<Reporter> reporters = objectReporters.get(reportableName);
        if (reporters != null) {
            return reporters;
        }
        return new LinkedList<>();
    }

    public static List<Reporter> getReporters(IOObject ioo) {
        String reportableName = getName(ioo.getClass());
        return getReporters(reportableName);
    }

}
