/*

 *

 *

 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.service.network;


import static com.owc.singularity.engine.EngineProperties.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeCategory;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeInt;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeString;
import com.owc.singularity.engine.pipeline.parameter.conditions.EqualTypeCondition;
import com.owc.singularity.engine.tools.GlobalAuthenticator;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.engine.tools.SystemInfoUtilities.OperatingSystem;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * This class applies the proxy settings on the JVM environment
 *
 * @author Jonas Wilms-Pfau
 *
 */
public class ProxyConfigurationService {

    /**
     * Non-Proxy Settings
     * <p>
     * Set some random proxy but ignore every URL -> Proxy is bypassed
     * </p>
     */
    // Could be every non-empty String
    private final static String NON_PROXY_HOST = "127.0.0.1";
    // Could be every valid Port
    private final static String NON_PROXY_PORT = "80";
    // Must be * to ignore all proxy settings
    private final static String NON_PROXY_RULE = "*";
    /** Default value for Linux and Windows */
    private static final String DEFAULT = "";

    public static final String PROXY_PREFIX = "singularity.proxy.";
    public static final String SYSTEM_PREFIX = "";

    private static final String HTTP_NON_PROXY_RULE = "http.nonProxyHosts";
    private static final String FTP_NON_PROXY_RULE = "ftp.nonProxyHosts";
    private static final String SOCKS_NON_PROXY_RULE = "socksNonProxyHosts";

    public final static String SINGULARITY_PROXY_MODE_SYSTEM = "System proxy";
    public final static String SINGULARITY_PROXY_MODE_DIRECT = "Direct (no proxy)";
    public final static String SINGULARITY_PROXY_MODE_MANUAL = "Manual proxy configuration";

    public final static String[] SINGULARITY_PROXY_MODES = { SINGULARITY_PROXY_MODE_SYSTEM, SINGULARITY_PROXY_MODE_DIRECT, SINGULARITY_PROXY_MODE_MANUAL };

    public static final String[] SINGULARITY_SOCKS_VERSIONS = { "Version 4", "Version 5" };

    /* SingularityEngine proxy settings */
    private final static String[] PROXY_HOSTS = { EngineProperties.HTTP_PROXY_HOST, EngineProperties.HTTPS_PROXY_HOST, EngineProperties.FTP_PROXY_HOST,
            EngineProperties.SOCKS_PROXY_HOST };
    private final static String[] PROXY_PORTS = { EngineProperties.HTTP_PROXY_PORT, EngineProperties.HTTPS_PROXY_PORT, EngineProperties.FTP_PROXY_PORT,
            EngineProperties.SOCKS_PROXY_PORT };
    /* Real system settings */
    private final static String[] PROXY_RULES = { HTTP_NON_PROXY_RULE, FTP_NON_PROXY_RULE, SOCKS_NON_PROXY_RULE };
    private static final ProxyConfigurationService INSTANCE = new ProxyConfigurationService();

    private ProxyConfigurationService() {
        // static class
    }

    /**
     * To support OSX we have to store the System settings before the SingularityEngine cfg file is
     * loaded
     */
    public static void storeSystemSettings() {
        if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
            Arrays.stream(toNative(PROXY_HOSTS)).forEach(SystemSettings::store);
            Arrays.stream(toNative(PROXY_PORTS)).forEach(SystemSettings::store);
            Arrays.asList(PROXY_RULES).forEach(SystemSettings::store);
        } else {
            Arrays.stream(toNative(PROXY_HOSTS)).forEach(SystemSettings::storeDefault);
            Arrays.stream(toNative(PROXY_PORTS)).forEach(SystemSettings::storeDefault);
            Arrays.asList(PROXY_RULES).forEach(SystemSettings::storeDefault);
        }

    }

    /**
     * This Method
     * <ul>
     * <li>Migrates the old proxy settings if needed</li>
     * <li>Applies the current proxy Settings</li>
     * <li>Registers a ChangeListener to change the proxy settings on save</li>
     * </ul>
     *
     */
    public static void initialize() {
        storeSystemSettings();
        ProxyIntegrator.updateOldInstallation();
        registerConfigurationProperties();
        GlobalAuthenticator.getInstance().registerProxyAuthenticator(new ProxyAuthenticator("http"));
        GlobalAuthenticator.getInstance().registerProxyAuthenticator(new ProxyAuthenticator("https"));
        GlobalAuthenticator.getInstance().registerProxyAuthenticator(new ProxyAuthenticator("ftp"));
    }

    private static void registerConfigurationProperties() {
        // parameters for proxy settings
        PropertyService.registerParameter(new ParameterTypeCategory(PROXY_MODE, "", SINGULARITY_PROXY_MODES, 0), "proxy");
        ParameterType[] manualTypes = {
                // Global exclusion list (applies on all NON_PROXY_HOSTS)
                new ParameterTypeString(PROXY_EXCLUDE, "", true),
                // HTTP
                new ParameterTypeString(HTTP_PROXY_HOST, "", true), new ParameterTypeInt(HTTP_PROXY_PORT, "", 0, 65535, true),
                // HTTPS
                new ParameterTypeString(HTTPS_PROXY_HOST, "", true), new ParameterTypeInt(HTTPS_PROXY_PORT, "", 0, 65535, true),
                // FTP
                new ParameterTypeString(FTP_PROXY_HOST, "", true), new ParameterTypeInt(FTP_PROXY_PORT, "", 0, 65535, true),
                // SOCKS
                new ParameterTypeString(SOCKS_PROXY_HOST, "", true), new ParameterTypeInt(SOCKS_PROXY_PORT, "", 0, 65535, true),
                new ParameterTypeCategory(SOCKS_VERSION, "", SINGULARITY_SOCKS_VERSIONS, 1) };
        // register dependency to manual proxy mode
        int manualIndex = Arrays.asList(SINGULARITY_PROXY_MODES).indexOf(SINGULARITY_PROXY_MODE_MANUAL);
        EqualTypeCondition manualSelected = new EqualTypeCondition(null, PROXY_MODE, SINGULARITY_PROXY_MODES, false, manualIndex);
        for (ParameterType type : manualTypes) {
            type.registerDependencyCondition(manualSelected);
            PropertyService.registerParameter(type, "proxy");
        }
    }

    /**
     * Applies the SingularityEngine proxy settings on the corresponding JVM System properties
     */
    public static void apply() {

        switch (PropertyService.getParameterValue(EngineProperties.PROXY_MODE)) {
            case SINGULARITY_PROXY_MODE_SYSTEM:
                // System Proxy
                SystemSettings.apply();
                break;
            case SINGULARITY_PROXY_MODE_DIRECT:
                // No Proxy
                setSystemValue(NON_PROXY_HOST, toNative(PROXY_HOSTS));
                setSystemValue(NON_PROXY_PORT, toNative(PROXY_PORTS));
                setSystemValue(NON_PROXY_RULE, PROXY_RULES);
                break;
            case SINGULARITY_PROXY_MODE_MANUAL:
                // User Settings
                copyParameterToSystem(PROXY_HOSTS, toNative(PROXY_HOSTS));
                copyParameterToSystem(PROXY_PORTS, toNative(PROXY_PORTS));
                String exclusionRule = PropertyService.getParameterValue(EngineProperties.PROXY_EXCLUDE);
                setSystemValue(exclusionRule, PROXY_RULES);
                // Apply Socks Version
                int socksVersionOffset = Arrays.asList(SINGULARITY_SOCKS_VERSIONS).indexOf(PropertyService.getParameterValue(EngineProperties.SOCKS_VERSION));
                int initialSocksVersion = 4;
                PropertyService.setParameterValue(toNative(EngineProperties.SOCKS_VERSION), String.valueOf(initialSocksVersion + socksVersionOffset));
                break;
        }
    }

    /**
     * Set one value to all System Keys
     *
     * @param value
     * @param systemKeys
     */
    private static void setSystemValue(String value, String[] systemKeys) {
        for (String parameterKey : systemKeys) {
            setSystemProperty(parameterKey, value);
        }
    }

    private static void setSystemProperty(String key, String value) {
        if (value != null && key != null) {
            System.setProperty(key, value);
        }

    }

    /**
     * Copies the PropertyService values from the source keys to target System property keys
     * <p>
     * Warning: both arrays must have the same length.
     * </p>
     *
     * @param sourceKeys
     *            PropertyService keys
     * @param targetKeys
     *            System keys
     */
    private static void copyParameterToSystem(String[] sourceKeys, String[] targetKeys) {
        for (int i = 0; i < sourceKeys.length; i++) {
            String sourceValue = PropertyService.getParameterValue(sourceKeys[i]);
            setSystemProperty(targetKeys[i], sourceValue);
        }
    }

    /**
     * Converts the given keys to native System keys
     *
     * @param keys
     * @return
     */
    private static String[] toNative(String[] keys) {
        return Arrays.stream(keys).map(ProxyConfigurationService::toNative).toArray(String[]::new);
    }

    /**
     * Converts the given key to a native System key
     *
     * @param key
     * @return
     */
    private static String toNative(String key) {
        return key.replace(PROXY_PREFIX, SYSTEM_PREFIX);
    }

    public static ProxyConfigurationService getInstance() {
        return INSTANCE;
    }


    /**
     * This class migrates the old proxy settings into the new structure.
     * <p>
     * Use ProxyService.init() to check for updates
     * </p>
     *
     * @author Jonas Wilms-Pfau
     *
     */
    private static class ProxyIntegrator {

        private static final String OLD_KEY = "http.proxyUsername";
        private static final String NEW_KEY = EngineProperties.SOCKS_VERSION;

        /**
         * Update an old installation
         * <p>
         * Copies the old native properties into the new SingularityEngine properties
         * </p>
         *
         */
        private static void updateOldInstallation() {
            if (PropertyService.getParameterValue(OLD_KEY) != null && PropertyService.getParameterValue(NEW_KEY) == null) {
                LogService.getRoot().log(Level.INFO, "com.owc.singularity.engine.tools.ProxyService.migrate");
                // Copy from old System properties to new SingularityEngine properties
                copyParameterValues(toNative(PROXY_HOSTS), PROXY_HOSTS);
                copyParameterValues(toNative(PROXY_PORTS), PROXY_PORTS);

                // merge exclusionRules together
                HashSet<String> rules = new LinkedHashSet<>();
                for (String ruleKey : PROXY_RULES) {
                    String rule = System.getProperty(ruleKey);
                    if (rule != null && !"".equals(rule)) {
                        rules.addAll(Arrays.asList(rule.split("\\|")));
                    }
                }
                String exclusionRule = String.join("|", rules);
                setParameterValue(EngineProperties.PROXY_EXCLUDE, exclusionRule);
                setSystemValue(exclusionRule, PROXY_RULES);
            }
        }

        /**
         * Copies the parameter values from source to target
         * <p>
         * Warning: both arrays must have the same length.
         * </p>
         *
         * @param sourceKeys
         *            PropertyService keys
         * @param targetKeys
         *            PropertyService keys
         */
        private static void copyParameterValues(String[] sourceKeys, String[] targetKeys) {
            for (int i = 0; i < sourceKeys.length; i++) {
                String sourceValue = PropertyService.getParameterValue(sourceKeys[i]);
                setParameterValue(targetKeys[i], sourceValue);
            }
        }

        /**
         * Set a value to the given PropertyService key
         *
         * @param key
         * @param value
         */
        private static void setParameterValue(String key, String value) {
            if (value != null && !value.isEmpty()) {
                PropertyService.setParameterValue(key, value);
            }
        }

    }

    /**
     * Helper Class to support Mac OS X
     */
    private static class SystemSettings {

        private static final HashMap<String, String> settings = new HashMap<String, String>();

        /**
         * Stores the current System property or the default value for the given key
         *
         * @param key
         */
        private static void store(String key) {
            settings.putIfAbsent(key, System.getProperty(key, DEFAULT));
        }

        /**
         * Stores the default value for the given key
         *
         * @param key
         */
        private static void storeDefault(String key) {
            settings.putIfAbsent(key, DEFAULT);
        }

        /**
         * Applies all stored settings to the system
         */
        private static void apply() {
            settings.forEach(System::setProperty);
        }

    }

}
