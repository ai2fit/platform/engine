package com.owc.singularity.engine.service.network;

import java.io.IOException;
import java.net.*;

import org.apache.logging.log4j.LogManager;

import com.owc.singularity.engine.tools.GlobalAuthenticator;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.tools.PasswordDialog;

public class ProxyAuthenticator implements GlobalAuthenticator.URLAuthenticator {

    private static final int STATUS_FIELD = 0;

    public static final String SOCKS = "socks";

    private final String protocol;

    public ProxyAuthenticator(String protocol) {
        this.protocol = protocol;
    }

    @Override
    public PasswordAuthentication getAuthentication(URL url) {
        return getAuthentication(url, "auth.proxy", false);
    }

    public PasswordAuthentication getAuthentication(URL url, String i18n, boolean forceRefresh) {
        if (protocol.equals(SOCKS) || url.getProtocol().equals(protocol)) {
            String proxyType = protocol.toUpperCase();
            String proxyURL = getProxyAddress(url).toString().replaceAll("/", "");
            String authMessage = getAuthMessage(url.getProtocol());

            final URI uri;
            try {
                uri = new URI(proxyURL);
            } catch (URISyntaxException e) {
                return null;
            }
            PasswordAuthentication passwordAuthentication = PasswordDialog.getPasswordAuthentication(uri, url.getUserInfo(), forceRefresh, true, i18n,
                    proxyType, uri, authMessage);
            if (passwordAuthentication == null) {
                return null;
            }

            // Verify Settings
            if (!verify(url, passwordAuthentication) && !forceRefresh) {
                passwordAuthentication = getAuthentication(url, "auth.proxy.wrong.credentials", true);
            }

            return passwordAuthentication;
        }
        return null;
    }

    /**
     * Returns a message to make the authentication mechanism transparent to the user
     *
     * @return The i18n message for the given authentication scheme
     */
    private String getAuthMessage(String scheme) {
        final String i18n;
        if (scheme != null) {
            i18n = switch (scheme.trim().toLowerCase()) {
                case "basic":
                    yield "gui.dialog.auth.proxy.basic";
                case "digest":
                    yield "gui.dialog.auth.proxy.digest";
                case "ntlm":
                    yield "gui.dialog.auth.proxy.ntlm";
                case "spnego", "negotiate":
                    yield "gui.dialog.auth.proxy.negotiate";
                case "kerberos":
                    yield "gui.dialog.auth.proxy.kerberos";
                default:
                    yield "gui.dialog.auth.proxy.unknown";
            };
        } else {
            i18n = "gui.dialog.auth.proxy.unknown";
        }
        return I18N.getGUIMessage(i18n);
    }

    /**
     * The current ProxyAddress
     *
     * @return The SocketAddress as given by the GlobalAuthenticator
     */
    public static InetSocketAddress getProxyAddress(URL url) {
        return new InetSocketAddress(url.getHost(), url.getPort());
    }

    /**
     * Verify the proxy by accessing the url using the given PasswordAuthentication
     *
     * @param url
     *            The URL to test
     * @param authentication
     *            username+password to test
     * @return A valid passwordAuthentication or null
     */
    public static boolean verify(URL url, PasswordAuthentication authentication) {
        try {
            // make sure to only call foo.bar not foo.bar/destroy/world
            URL safeUrl = new URL(url.getProtocol(), url.getHost(), url.getPort(), "");
            if (safeUrl.openConnection().getHeaderField(STATUS_FIELD).contains("407")) {
                return false;
            }
            return true;
        } catch (IOException e) {
            LogManager.getLogger(ProxyAuthenticator.class.getName()).error(I18N.getErrorMessage("proxy.credentials.verify.failure", e));
        }
        return false;
    }
}
