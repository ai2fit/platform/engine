package com.owc.singularity.engine.service.repository;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.file.FileRepositoryMount;

public class RepositoryConfigurator {

    private static final Logger log = LogService.getI18NLogger(RepositoryConfigurator.class);

    public static void initialize() throws IOException {
        String repositoryConfigFileParameter = PropertyService.getParameterValue(EngineProperties.GENERAL_REPOSITORY_CONFIG_FILE,
                FileSystemService.getUserConfigFile(EngineProperties.DEFAULT_GENERAL_REPOSITORY_CONFIG_FILE).toString());
        Path repositoryConfigurationFilePath = Path.of(repositoryConfigFileParameter);
        RepositoryManager.RepositoryConfiguration repositoryConfiguration;
        if (Files.exists(repositoryConfigurationFilePath)) {
            try (InputStream inputStream = Files.newInputStream(repositoryConfigurationFilePath)) {
                repositoryConfiguration = RepositoryManager.readConfiguration(inputStream);
            } catch (IOException e) {
                log.atWarn()
                        .withThrowable(e)
                        .log("com.owc.singularity.repository.RepositoryManager.reset_invalid_configuration", repositoryConfigurationFilePath);
                // reset the configuration
                repositoryConfiguration = new RepositoryManager.RepositoryConfiguration(new HashMap<>());
            }
        } else {
            // there is no configuration file
            repositoryConfiguration = new RepositoryManager.RepositoryConfiguration(new HashMap<>());
        }

        if (repositoryConfiguration.getMounts().isEmpty()) {
            // no mounts were configured
            log.warn("com.owc.singularity.repository.create_default");
            // init standard root
            Map<String, String> options = new HashMap<>();
            Path repositoryPath = FileSystemService.getUserSingularityDirectory().toPath().resolve("repository");
            Files.createDirectories(repositoryPath);
            options.put(FileRepositoryMount.OPTION_BASE_DIRECTORY, repositoryPath.toString());
            options.put(FileRepositoryMount.OPTION_READ_ONLY, "false");
            RepositoryManager.RepositoryMountConfiguration defaultMountConfiguration = new RepositoryManager.RepositoryMountConfiguration(
                    FileRepositoryMount.TYPE_NAME, options);
            repositoryConfiguration.getMounts().put("", defaultMountConfiguration);
        }
        Map<String, RepositoryManager.RepositoryMountConfiguration> mounts = repositoryConfiguration.getMounts();


        for (Map.Entry<String, RepositoryManager.RepositoryMountConfiguration> entry : mounts.entrySet()) {
            String pathString = entry.getKey().isBlank() ? "/" : entry.getKey();
            RepositoryManager.RepositoryMountConfiguration mountConfig = entry.getValue();
            try {
                log.debug("com.owc.singularity.repository.adding_mount", mountConfig.getMountType(), pathString);
                long startTime = System.nanoTime();
                RepositoryManager.addMount(pathString, mountConfig);
                log.debug("com.owc.singularity.repository.mount_success", mountConfig.getMountType(), pathString,
                        Tools.formatDuration(Duration.ofNanos(System.nanoTime() - startTime)));
            } catch (IOException e) {
                log.atError().withThrowable(e).log("com.owc.singularity.repository.mount_fail", mountConfig.getMountType(), pathString);
                RepositoryManager.addMountPlaceholder(pathString, mountConfig, e);
            }
        }
    }
}
