package com.owc.singularity.engine;


import java.util.Locale;

/**
 * A set of constant values used in the {@link com.owc.singularity.engine.PropertyService}
 * 
 * @author Hatem Hamad
 */
public class EnginePropertyConstants {

    public static final String[] GENERAL_LOCALE_LANGUAGE_VALUES = { Locale.ENGLISH.getLanguage() };

    public static final String[] TOOLS_MAIL_METHOD_VALUES = { "sendmail", "SMTP" };
    public static final int TOOLS_MAIL_METHOD_SENDMAIL = 0;
    public static final int TOOLS_MAIL_METHOD_SMTP = 1;

    public static final String SMTP_SECURITY_NONE = "None";
    public static final String SMTP_SECURITY_STARTTLS = "StartTLS";
    public static final String SMTP_SECURITY_STARTTLS_ENFORCE = "Enforce StartTLS";
    public static final String SMTP_SECURITY_STARTTLS_ENFORCE_PFS = "Enforce StartTLS - TLS 1.2 + PFS";
    public static final String SMTP_SECURITY_TLS = "TLS";
    public static final String SMTP_SECURITY_TLS_PFS = "TLS 1.2 + PFS";
    public static final String[] SMTP_SECURITY_VALUES = { SMTP_SECURITY_NONE, SMTP_SECURITY_STARTTLS, SMTP_SECURITY_STARTTLS_ENFORCE,
            SMTP_SECURITY_STARTTLS_ENFORCE_PFS, SMTP_SECURITY_TLS, SMTP_SECURITY_TLS_PFS };

    public static final String SMTP_AUTHENTICATION_AUTO = "Auto";
    public static final String SMTP_AUTHENTICATION_CRAM_MD5 = "CRAM-MD5";
    public static final String SMTP_AUTHENTICATION_NTLM = "NTLM";
    public static final String[] SMTP_AUTHENTICATION_VALUES = { SMTP_AUTHENTICATION_AUTO, SMTP_AUTHENTICATION_CRAM_MD5, SMTP_AUTHENTICATION_NTLM };

}
