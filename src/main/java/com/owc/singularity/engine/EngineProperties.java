package com.owc.singularity.engine;

/**
 * This is a simple class holding all configuration constants of the
 * {@link com.owc.singularity.SingularityEngine}.
 *
 * @author Hatem Hamad
 */
public class EngineProperties {

    /**
     * Set this parameter to {@code true} to allow https to http redirects
     */
    public static final String NETWORK_FOLLOW_HTTPS_TO_HTTP = "singularity.system.network.follow_https_to_http";

    /**
     * Set this parameter to {@code true} to allow http to https redirects
     */
    public static final String NETWORK_FOLLOW_HTTP_TO_HTTPS = "singularity.system.network.follow_http_to_https";

    /**
     * Defines a default user-agent
     */
    public static final String NETWORK_DEFAULT_USER_AGENT = "singularity.system.network.default_user_agent";

    public static final String GENERAL_LOCALE_LANGUAGE = "singularity.general.locale.language";
    public static final String GENERAL_LOCALE_DATE_FORMAT = "singularity.general.locale.date_format";
    /**
     * The name of the property indicating the preferred globally used locale.
     */
    public static final String GENERAL_LOCALE = "singularity.general.locale";

    /**
     * The name of the property indicating the version of SingularityEngine (read only).
     */
    public static final String VERSION = "singularity.version";

    /**
     * Boolean parameter indicating if the plugins should be initialized at all.
     */
    public static final String MODULES_ENABLED = "singularity.modules.enabled";
    /**
     * A file path to the directory containing the plugin Jar files.
     */
    public static final String INIT_PLUGINS_LOCATION = "singularity.init.plugins.location";
    /**
     * The property name for &quot;The number of fraction digits of formatted numbers.&quot;
     */
    public static final String GENERAL_FRACTIONDIGITS_NUMBERS = "singularity.general.fractiondigits.numbers";
    public static final int DEFAULT_GENERAL_FRACTION_DIGITS = 6;
    /**
     * The property name for &quot;The number of fraction digits of formatted percent values.&quot;
     */
    public static final String GENERAL_FRACTIONDIGITS_PERCENT = "singularity.general.fractiondigits.percent";


    /**
     * The name of the property defining how many lines are read for guessing values types for input
     * operations without defined value type.
     */
    public static final String GENERAL_MAX_TEST_ROWS = "singularity.general.max_rows_used_for_guessing";

    /** The property name for &quot;The default random seed (-1: random random seed).&quot; */
    public static final String GENERAL_RANDOMSEED = "singularity.general.randomseed";

    /**
     * The property name for &quot;Indicates if SingularityEngine should be used in debug mode
     * (print exception stacks and shows more technical error messages)&quot;
     */
    public static final String GENERAL_DEBUGMODE = "singularity.general.debugmode";
    /**
     * The name of the property indicating the default encoding for files.
     */
    public static final String GENERAL_DEFAULT_ENCODING = "singularity.general.encoding";
    /**
     * The name of the property indicating the preferred globally used time zone.
     */
    public static final String GENERAL_TIME_ZONE = "singularity.general.timezone";

    /**
     * The name of the property indicating the path to the repository configuration file.
     */
    public static final String GENERAL_REPOSITORY_CONFIG_FILE = "singularity.general.repository_config_file";
    public static final String DEFAULT_GENERAL_REPOSITORY_CONFIG_FILE = "repository_config.json";
    public static final String DEFAULT_WEBSERVICE_REPOSITORY_CONFIG_FILE = "webservice_config.json";

    /**
     * this property can be used to set the target of the off-heap memory engine will use (in MB)
     */
    public static final String TARGET_OFF_HEAP_MEMORY = "singularity.system.target_off_heap_memory";

    public static final String PROXY_MODE = "singularity.proxy.mode";
    public static final String PROXY_EXCLUDE = "singularity.proxy.exclude";
    /**
     * These settings are the JVM Params with a prepended singularity.proxy
     */
    public static final String HTTP_PROXY_HOST = "singularity.proxy.http.proxyHost";
    public static final String HTTP_PROXY_PORT = "singularity.proxy.http.proxyPort";
    public static final String HTTP_PROXY_NON_PROXY_HOSTS = "singularity.proxy.http.nonProxyHosts";
    public static final String HTTPS_PROXY_HOST = "singularity.proxy.https.proxyHost";
    public static final String HTTPS_PROXY_PORT = "singularity.proxy.https.proxyPort";
    public static final String FTP_PROXY_HOST = "singularity.proxy.ftp.proxyHost";
    public static final String FTP_PROXY_PORT = "singularity.proxy.ftp.proxyPort";
    public static final String SOCKS_PROXY_HOST = "singularity.proxy.socksProxyHost";
    public static final String SOCKS_PROXY_PORT = "singularity.proxy.socksProxyPort";
    public static final String SOCKS_VERSION = "singularity.proxy.socksProxyVersion";;
}
