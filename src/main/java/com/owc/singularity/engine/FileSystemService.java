/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine;

import static com.owc.singularity.engine.PropertyService.SINGULARITY_CONFIG_FILE_NAME;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.tools.PlatformUtilities;


/**
 * This service offers methods for accessing the file system. For example to get the current
 * SingularityEngine directory, used home directory and several else.
 *
 * @author Sebastian Land
 */
public class FileSystemService {

    private static final Logger log = LogManager.getLogger(FileSystemService.class);

    /** folder which can be used to load additional building blocks */
    public static final String SINGULARITY_BUILDINGBLOCKS = "buildingblocks";

    public static final String SINGULARITY_USER_FOLDER = ".ai2fit";
    private static File singularityConfigDirectory;

    /** Returns the main user configuration file. */
    public static File getMainUserConfigFile() {
        return FileSystemService.getUserConfigFile(SINGULARITY_CONFIG_FILE_NAME);
    }

    /** Returns the memory configuration file containing the max memory. */
    public static File getMemoryConfigFile() {
        return new File(getUserSingularityDirectory(), "memory");
    }

    /** Returns the SingularityEngine log file. */
    public static File getLogFile() {
        return new File(getUserSingularityDirectory(), "singularity-studio.log");
    }

    /**
     * Returns the configuration file in the user dir {@link #SINGULARITY_USER_FOLDER}.
     */
    public static File getUserConfigFile(String configName) {
        return new File(getUserSingularityDirectory(), configName);
    }

    public static File getUserSingularityDirectory() {
        if (singularityConfigDirectory == null) {
            String customHome = System.getProperty("singularity.user-home");
            if (customHome != null && !customHome.trim().isEmpty()) {
                singularityConfigDirectory = new File(customHome);
            } else {
                File homeDir = new File(System.getProperty("user.home"));
                singularityConfigDirectory = new File(homeDir, SINGULARITY_USER_FOLDER);
            }

            checkAndCreateFolder(singularityConfigDirectory);
            if (SingularityEngine.getExecutionMode().hasMainFrame())
                checkAndCreateFolder(new File(singularityConfigDirectory, SINGULARITY_BUILDINGBLOCKS));
            log.info("Using " + singularityConfigDirectory.toString() + " as user config directory...");
        }
        return singularityConfigDirectory;
    }


    public static File getSingularityProgramDirectory() throws IOException {
        String property = System.getProperty(PlatformUtilities.PROPERTY_SINGULARITY_HOME);
        if (property == null) {
            throw new IOException("Property " + PlatformUtilities.PROPERTY_SINGULARITY_HOME + " is not set");
        }
        // remove any line breaks that snuck in for some reason
        property = property.replaceAll("\\r|\\n", "");
        return new File(property);
    }

    public static File getLibraryFile(String name) throws IOException {
        File home = getSingularityProgramDirectory();
        return new File(home, "lib" + File.separator + name);
    }


    /**
     * Tries to create the given folder location if it does not yet exist.
     *
     * @param newFolder
     *            the folder location in question.
     */
    private static void checkAndCreateFolder(File newFolder) {
        if (!newFolder.exists()) {
            log.debug("Creating directory {}", newFolder);
            boolean result = newFolder.mkdirs();
            if (!result) {
                log.warn("Error when trying to create folder {}", newFolder);
            }
        }
    }

}
