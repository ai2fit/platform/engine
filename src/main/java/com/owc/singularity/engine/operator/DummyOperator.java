package com.owc.singularity.engine.operator;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.operator.error.SimpleProcessSetupError;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.pipeline.io.transformer.OperatorReplacementTransformer;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeList;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeString;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * This operator cannot be executed. It is merely used by a {@link XMLImporter} to create a dummy
 * operator that acts as a placeholder for an operator contained in a plugin that is not installed.
 *
 * @author Simon Fischer
 *
 */
@DefinesOperator(module = "core", key = "dummy", group = "deprecated", name = "Unknown Operator", icon = "question.png")
public class DummyOperator extends AbstractOperator<DummyOperator.Configuration> {

    public static final String BAD_IMPORT_ACTION = "throws";

    public static final String PARAMETER_INSTALL_EXTENSION = "install_extension";
    public static final String PARAMETER_REPLACES = "replaces";
    public static final String PARAMETER_PARAMETERS = "parameters";
    public static final String PARAMETER_LISTS = "lists";

    public static final String PARAMETER_INPUT_PORT_REPLACEMENTS = "input_port_replacements";
    public static final String PARAMETER_OUTPUT_PORT_REPLACEMENTS = "output_port_replacements";

    private QuickFix installFix = null;

    public DummyOperator() {
        super();
        defineThroughPorts();
        defineAction(BAD_IMPORT_ACTION, (context, configuration) -> {
            throw new UserError(this, 151, getName(), configuration.replaces);
        });
    }

    @Override
    protected Configuration configure(OperationExecutionContext operationContext, InputValidator inputValidator) throws UserError, OperatorException {
        Configuration configuration = new Configuration();
        try {
            configuration.replaces = operationContext.parameters.getParameterAsString(PARAMETER_REPLACES);
        } catch (UndefinedParameterError e) {
            configuration.replaces = "";
        }
        return configuration;
    }

    @Override
    protected void performAdditionalChecks() {
        super.performAdditionalChecks();
        if (installFix != null) {
            addError(new SimpleProcessSetupError(Severity.ERROR, getPortOwner(), Collections.singletonList(installFix), "dummy_operator", getReplaces()));
        } else {
            addError(new SimpleProcessSetupError(Severity.ERROR, getPortOwner(), "dummy_operator", getReplaces()));
        }
    }

    public String getReplaces() {
        try {
            return getParameterAsString(PARAMETER_REPLACES);
        } catch (UndefinedParameterError e) {
            return "";
        }
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeString(PARAMETER_REPLACES, "The module and key of the operator missing."));
        ParameterType type = new ParameterTypeList(PARAMETER_PARAMETERS, "List of parameters for the missing operator.",
                new ParameterTypeString("former_parameter_key", "Specifies the key of the former parameter"),
                new ParameterTypeString("former_parameter_value", "Specifies the value of the former parameter"));
        type.setPrimary(true);
        types.add(type);
        type = new ParameterTypeList(PARAMETER_LISTS, "List of parameters for the missing operator.",
                new ParameterTypeString("former_list_key", "Specifies the key of the former list"),
                new ParameterTypeString("list_values", "Specifies the values of the former list"));
        types.add(type);
        types.add(new ParameterTypeList(PARAMETER_INPUT_PORT_REPLACEMENTS, "List of replaced input ports",
                new ParameterTypeString("port", "The name of the port of this dummy operator."),
                new ParameterTypeString("original port", "The name of the original port of the replaced operator.")));
        types.add(new ParameterTypeList(PARAMETER_OUTPUT_PORT_REPLACEMENTS, "List of replaced output ports",
                new ParameterTypeString("port", "The name of the port of this dummy operator."),
                new ParameterTypeString("original port", "The name of the original port of the replaced operator.")));
        return types;
    }

    public static class Configuration {

        String replaces;
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class DummyPortRenameTransformer extends OperatorReplacementTransformer {

        public DummyPortRenameTransformer() throws Exception {
            super("core:intersect_advanced", "core:intersect", Map.of(), Set.of(),
                    IntStream.range(1, 10).mapToObj(String::valueOf).collect(Collectors.toMap(index -> "in " + index, index -> "through " + index)),
                    IntStream.range(1, 10).mapToObj(String::valueOf).collect(Collectors.toMap(index -> "out " + index, index -> "through " + index)));
        }
    }
}
