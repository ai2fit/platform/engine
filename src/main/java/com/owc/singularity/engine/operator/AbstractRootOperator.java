/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;


import java.util.*;
import java.util.stream.Collectors;

import javax.xml.xpath.*;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.rules.SubprocessTransformRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.*;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformationLog;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformer;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.pipeline.io.transformer.OperatorReplacementTransformer;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.tools.ListenerTools;
import com.owc.singularity.engine.tools.RandomGenerator;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Each pipeline must contain exactly one operator of this class, and it must be the root operator
 * of the pipeline. The only purpose of this operator is to provide some parameters that have global
 * relevance.
 *
 * @author Ingo Mierswa
 */
public abstract class AbstractRootOperator extends OperatorChain {

    public static final String PARAMETER_TAGS = "tags";
    public static final String PARAMETER_DESCRIPTION = "description";
    public static final String PARAMETER_RANDOM_SEED = "random_seed";

    /** The list of listeners for process events. */
    private final List<PipelineExecutionListener> listenerList = new LinkedList<>();

    /** The process which is connected to this process operator. */
    private AbstractPipeline pipeline;


    /** Creates a new process operator without reference to an process. */
    public AbstractRootOperator(String subPipelineName) {
        super(subPipelineName);


        rename("Root");

        getTransformer().addRule(new SubprocessTransformRule(getSubprocess(0)));
    }

    /**
     * The root operator is responsible to take the context and make sure that the execution is
     * performed under this context.
     * 
     * @param context
     * @param preserveEmptyResults
     * @return
     * @throws OperatorException
     */
    public abstract List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException;

    /** Sets the process. */
    public void setPipeline(AbstractPipeline pipeline) {
        this.pipeline = pipeline;
        registerOperator(this.pipeline);
    }


    /**
     * Returns the process of this operator if available. Overwrites the method from the superclass.
     */
    @Override
    public AbstractPipeline getPipeline() {
        return pipeline;
    }

    /** Adds an process listener to the list of listeners. */
    public void addPipelineListener(PipelineExecutionListener l) {
        synchronized (listenerList) {
            listenerList.add(l);
        }
    }

    /** Removes an process listener from the list of listeners. */
    public void removePipelineListener(PipelineExecutionListener l) {
        synchronized (listenerList) {
            listenerList.remove(l);
        }
    }

    public List<PipelineExecutionListener> getListenerListCopy() {
        synchronized (listenerList) {
            if (listenerList.isEmpty()) {
                return Collections.emptyList();
            } else {
                return new LinkedList<>(listenerList);
            }
        }
    }

    /**
     * Called at the beginning of the process. Notifies all listeners and the children operators
     * (super method).
     */
    @Override
    public void pipelineStarts() throws OperatorException {
        ListenerTools.informAllAndThrow(x -> super.pipelineStarts(), getListenerListCopy(), l -> l.pipelineStarts(pipeline));
    }

    /**
     * Called at the end of the process. Notifies all listeners and the children operators (super
     * method).
     */
    @Override
    public void pipelineFinished() throws OperatorException {
        ListenerTools.informAllAndThrow(x -> super.pipelineFinished(), getListenerListCopy(), l -> l.pipelineFinished(pipeline));
    }

    /** Counts the step and notifies all process listeners. */
    public void fireEventPipelineOperatorStarted(Operator op) {
        for (PipelineExecutionListener l : getListenerListCopy())
            l.pipelineOperatorStarted(pipeline, op);
    }

    /** Counts the step and notifies all process listeners. */
    public void fireEventPipelineOperatorFinished(Operator op) {
        for (PipelineExecutionListener l : getListenerListCopy()) {
            l.pipelineOperatorFinished(pipeline, op);
        }
    }

    /**
     * This method allows to propagate metadata over the entire pipeline and receive the results as
     * delivered to this operators output ports.
     * 
     * @param inputMetaData
     *            input metadata in order of input ports
     * @return result meta data with null if port didn't receive meta data
     */
    public List<MetaData> transformMetaData(List<MetaData> inputMetaData) {
        for (int i = 0; i < getSubprocess(0).getInnerSources().getNumberOfConnectedPorts() && i < inputMetaData.size(); i++) {
            MetaData metaData = inputMetaData.get(i);
            getSubprocess(0).getInnerSources().getPortByIndex(i).deliverMD(metaData);
        }
        transformMetaData();
        return getSubprocess(0).getInnerSinks().getAllPorts().stream().map(p -> p.getMetaData(MetaData.class)).collect(Collectors.toList());
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeString(PARAMETER_TAGS,
                "A comma separated list of tags. They can be used for advanced searching and filtering the repository and list of executions."));
        types.add(new ParameterTypeText(PARAMETER_DESCRIPTION, "A description of this pipeline in Markdown.", TextType.PLAIN));

        int seed = RandomGenerator.DEFAULT_SEED;
        String seedProperty = PropertyService.getParameterValue(EngineProperties.GENERAL_RANDOMSEED);
        try {
            if (seedProperty != null) {
                seed = Integer.parseInt(seedProperty);
            }
        } catch (NumberFormatException e) {
            logWarning("Bad integer in property 'singularity.general.randomseed', using default seed (" + RandomGenerator.DEFAULT_SEED + ").");
        }
        types.add(new ParameterTypeInt(PARAMETER_RANDOM_SEED, "Global random seed for random generators (-1 for initialization by system time).",
                Integer.MIN_VALUE, Integer.MAX_VALUE, seed));

        return types;
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class AnalysisPipelineRename extends OperatorReplacementTransformer {

        public AnalysisPipelineRename() throws Exception {
            super("core:process", "core:pipeline", null, Set.of("send_mail", "notification_email", "process_duration_for_mail", "resultfile"));
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT + 1)
    public static class LogLevelMigration extends OperatorReplacementTransformer {

        public LogLevelMigration() throws Exception {
            super("core:pipeline", "core:analysis_pipeline", Map.of("logverbosity", ExecutableRootOperator.PARAMETER_LOG_LEVEL), Set.of("logfile", "encoding"));
        }

        @Override
        protected HashMap<String, String> deriveParameterValues(HashMap<String, String> parameterValues) {
            HashMap<String, String> values = new HashMap<>();
            String logVerbosity = parameterValues.get(ExecutableRootOperator.PARAMETER_LOG_LEVEL);
            // { "all", "io", "status", "init", "notes", "warning", "error", "fatal", "almost_none",
            // "off" };
            String newName = switch (logVerbosity) {
                case "warning" -> Level.WARN.name();
                case "error" -> Level.ERROR.name();
                case "fatal" -> Level.FATAL.name();
                case "almost_none" -> Level.OFF.name();
                case "io" -> LogService.LEVEL_IO.name();
                case "all" -> Level.ALL.name();
                default -> null;
            };
            values.put(ExecutableRootOperator.PARAMETER_LOG_LEVEL, newName);
            return values;
        }
    }


    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT + 2)
    public static class PipelineUpgradeTransformer implements CompatibilityTransformer {

        protected static final XPathFactory xpathfactory = XPathFactory.newInstance();
        private XPathExpression operatorExpr;
        private XPathExpression portExpr;
        private XPath xPath;
        private XPathExpression variablesExpr;
        private XPathExpression valueExpr;
        private XPathExpression keyExpr;
        {
            xPath = xpathfactory.newXPath();
            try {
                operatorExpr = xPath.compile("//operator[@class='core:analysis_pipeline']");
                portExpr = xPath.compile("pipeline/connect[not(@from_op)]");
                variablesExpr = xPath.compile("//context/variables/variable");
                keyExpr = xPath.compile("key");
                valueExpr = xPath.compile("value");
            } catch (Exception e) {
                // should never occur
                LogService.getRoot().error(e.getMessage());
            }
        }

        @Override
        public void apply(Document document, CompatibilityTransformationLog log) {
            try {
                // change operator class key
                NodeList operatorNodes = (NodeList) operatorExpr.evaluate(document, XPathConstants.NODESET);
                for (int i = 0; i < operatorNodes.getLength(); i++) {
                    // these are executing operators. We need to transform them to either
                    // Execute
                    // Analysis or Include Sub-Pipeline
                    Node operatorNode = operatorNodes.item(i);
                    NamedNodeMap attributes = operatorNode.getAttributes();
                    String operatorName = attributes.getNamedItem("name").getNodeValue();

                    // see whether we have incoming ports to set operator type
                    NodeList connectedPortsList = (NodeList) portExpr.evaluate(operatorNode, XPathConstants.NODESET);
                    if (connectedPortsList.getLength() > 0) {
                        attributes.getNamedItem("class").setNodeValue("core:sub_pipeline");
                        log.log(Level.INFO, "Pipeline converted to new sub-pipeline type.");
                    }

                    // processing variables
                    NodeList variableEntries = (NodeList) variablesExpr.evaluate(document, XPathConstants.NODESET);
                    if (variableEntries.getLength() > 0) {
                        // add parameter for definition
                        Element listElement = document.createElement("list");
                        listElement.setAttribute("key", "variable_definitions");
                        operatorNode.appendChild(listElement);

                        // copy variables in list entries
                        for (int j = 0; j < variableEntries.getLength(); j++) {
                            Node variableDefinitionNode = variableEntries.item(j);

                            String key = ((Node) keyExpr.evaluate(variableDefinitionNode, XPathConstants.NODE)).getTextContent();
                            String value = ((Node) valueExpr.evaluate(variableDefinitionNode, XPathConstants.NODE)).getTextContent();

                            // append new parameter for variable
                            Element parameterElement = document.createElement("parameter");
                            parameterElement.setAttribute("key", key);
                            parameterElement.setAttribute("value",
                                    ParameterTypeTupel.transformTupel2String(new String[] { VariableType.STRING.toString(), value, "" }));
                            listElement.appendChild(parameterElement);
                        }
                        log.log(Level.INFO, "Pipelines variable definition updated.");
                    }
                }
            } catch (

            Exception e) {
                log.log(Level.ERROR, "Operator conversion failed: " + e.getMessage());
            }
        }

    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT + 3)
    public static class PipelineTypeUpgradeTransformer implements CompatibilityTransformer {

        protected static final XPathFactory xpathfactory = XPathFactory.newInstance();
        private XPath xPath;
        private XPathExpression rootPipelineExpr;
        {
            xPath = xpathfactory.newXPath();
            try {
                rootPipelineExpr = xPath.compile("/pipeline[not(@type)]");
            } catch (Exception e) {
                // should never occur
                LogService.getRoot().error(e.getMessage());
            }
        }

        @Override
        public void apply(Document document, CompatibilityTransformationLog log) {
            // check if already type attribute present, then it's already up to date
            try {
                NodeList rootPipelineNode = (NodeList) rootPipelineExpr.evaluate(document, XPathConstants.NODESET);
                if (rootPipelineNode.getLength() > 0) {
                    Element pipelineElement = (Element) rootPipelineNode.item(0);
                    Element rootOperator = XMLTools.getChildElement(pipelineElement, "operator", false);
                    switch (rootOperator.getAttribute("class")) {
                        case "core:analysis_pipeline":
                            pipelineElement.setAttribute("type", AnalysisPipeline.class.getName());
                            break;
                        case "core:sub_pipeline":
                            pipelineElement.setAttribute("type", SubPipeline.class.getName());
                            break;
                        case "core:webservice_pipeline":
                            pipelineElement.setAttribute("type", WebservicePipeline.class.getName());
                            break;
                        case "core:implementing_pipeline":
                            pipelineElement.setAttribute("type", ImplementingPipeline.class.getName());
                            break;
                        default:
                    }

                }
            } catch (XPathExpressionException | XMLException e) {
            }
        }
    }
}
