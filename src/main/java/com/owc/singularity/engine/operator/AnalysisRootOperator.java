package com.owc.singularity.engine.operator;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AnalysisPipeline.AnalysisPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.ports.extender.InputPortExtender;

/**
 * The root operator for any {@link com.owc.singularity.engine.pipeline.AnalysisPipeline}
 */
@DefinesOperator(module = "core", key = "analysis_pipeline", group = "", name = "Analysis Pipeline", icon = "pipeline_spreadsheet_chart.png")
public class AnalysisRootOperator extends ExecutableRootOperator {

    public static final String PARAMETER_VARIABLE_DEFINITIONS = "variable_definitions";
    private final InputPortExtender resultPortExtender = new InputPortExtender("result", 0d, getSubprocess(0).getInnerSinks());


    public AnalysisRootOperator() {
        super("Analysis");

        resultPortExtender.start();
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = new LinkedList<>();
        types.add(new ParameterTypeList(PARAMETER_VARIABLE_DEFINITIONS, "Defines a set of variables to use throughout the pipeline.", new ParameterTypeString(
                "variable_name",
                "Name of the variable, should start with lowercase character, no blanks. Can be accessed using %{name} notation in parameters within the pipeline."),
                new ParameterTypeTupel("type,_default_and_description", "",
                        new ParameterTypeEnumCategory<VariableType>("variable_type", "The type of the variable", VariableType.class, VariableType.STRING),
                        new ParameterTypeString("default_value", "The default value as string. Please make sure that default value fits to selected type."),
                        new ParameterTypeText("description", "Description of the variable", TextType.PLAIN))));
        types.addAll(super.getParameterTypes());

        return types;
    }

    @Override
    public List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        AnalysisPipelineExecutionContext myContext = (AnalysisPipelineExecutionContext) context;
        // apply variables
        VariableHandler handler = getPipeline().getVariableHandler();
        // add undefined variable defaults
        for (String[] variableType : getParameterList(PARAMETER_VARIABLE_DEFINITIONS)) {
            if (variableType[0] != null && !variableType[0].isBlank()) {
                String variable = variableType[0];
                String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableType[1]);
                if (typeDefaultDescription[1] != null && !typeDefaultDescription[1].isBlank() && !handler.isVariableSet(variable, this))
                    handler.addVariable(variable, typeDefaultDescription[1]);
            }
        }

        // add variable values from context
        for (Entry<String, String> variableEntry : myContext.variableValues.entrySet()) {
            handler.addVariable(variableEntry.getKey(), variableEntry.getValue());
        }

        // check definition of variables
        for (String[] variableType : getParameterList(PARAMETER_VARIABLE_DEFINITIONS)) {
            if (variableType[0] != null && !variableType[0].isBlank()) {
                String variable = variableType[0];
                String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableType[1]);
                if (handler.isVariableSet(variable, this)) {
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                    String variableValue = handler.getVariable(variable, this);
                    if (!type.isCompatible(variableValue)) {
                        throw new UserError(this, "pipeline.variable_content_does_not_match", variable, type, variableValue);
                    }
                } else {
                    throw new UserError(this, "pipeline.variable_has_no_value", variable);
                }
            }
        }


        execute();

        return resultPortExtender.getData(IOObject.class, preserveEmptyResults, false);
    }
}
