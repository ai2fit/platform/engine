/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator.execution;


import java.util.Enumeration;

import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;


/**
 * Executes an {@link ExecutionUnit} by invoking the operators in their (presorted) ordering.
 * Instances of this class can be shared.
 *
 * @author Simon Fischer, Marco Boeck
 *
 */
public class SimpleUnitExecutor implements UnitExecutor {

    @Override
    public void execute(ExecutionUnit unit) throws OperatorException {
        Logger logger = unit.getEnclosingOperator().getLogger();
        if (logger.isDebugEnabled()) {
            logger.debug("Executing sub-pipeline {}.{}. Execution order is: {}", unit.getEnclosingOperator().getName(), unit.getName(), unit.getOperators());
        }
        Enumeration<Operator> opEnum = unit.getOperatorEnumeration();


        Operator lastOperator = null;
        Operator operator = opEnum.hasMoreElements() ? opEnum.nextElement() : null;
        while (operator != null) {
            // execute the operator
            operator.execute();

            lastOperator = operator;
            operator = opEnum.hasMoreElements() ? opEnum.nextElement() : null;
            lastOperator.freeMemory();
        }

    }

}
