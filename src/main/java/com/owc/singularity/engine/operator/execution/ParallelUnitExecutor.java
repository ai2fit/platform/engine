/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator.execution;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

import com.owc.singularity.engine.operator.AbstractOperator;
import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;


/**
 * Executes an {@link ExecutionUnit} by invoking the operators in their (presorted) ordering.
 * Instances of this class can be shared.
 *
 * @author Simon Fischer, Marco Boeck
 *
 */
public class ParallelUnitExecutor implements UnitExecutor {

    @Override
    public void execute(ExecutionUnit unit) throws OperatorException {
        HashMap<Operator, Set<Operator>> dependencyMap = new HashMap<>();

        for (Operator operator : unit.getOperators()) {
            dependencyMap.put(operator,
                    operator.getInputPorts()
                            .getAllPorts()
                            .stream()
                            .filter(Port::isConnected)
                            .map(p -> p.getSource().getPorts().getOwner().getOperator())
                            .filter(o -> o != unit.getEnclosingOperator())
                            .collect(Collectors.toSet()));
        }


        List<Operator> operatorsReady = dependencyMap.entrySet().stream().filter(e -> e.getValue().isEmpty()).map(Map.Entry::getKey).toList();
        operatorsReady.forEach(dependencyMap::remove);
        List<ForkJoinTask<Void>> tasks = operatorsReady.stream().map(o -> new ExecuteOperatorTask(o, dependencyMap).fork()).toList();


        // now execute all tasks
        try {
            for (ForkJoinTask<?> task : tasks) {
                task.join();
            }
        } catch (RuntimeException e) {
            AbstractOperator.processThrowable(e);
        }
    }

    private static class ExecuteOperatorTask extends RecursiveAction {

        private static final long serialVersionUID = -5607769824316259772L;
        private final Operator operator;
        private HashMap<Operator, Set<Operator>> dependencyMap;

        public ExecuteOperatorTask(Operator operator, HashMap<Operator, Set<Operator>> dependencyMap) {
            this.operator = operator;
            this.dependencyMap = dependencyMap;
        }

        @Override
        protected void compute() {
            // execute the operator itself
            try {
                operator.execute();
            } catch (OperatorException e) {
                throw new OperatorRuntimeException(e);
            }
            // clear input ports and source output ports
            for (InputPort inputPort : operator.getInputPorts().getAllPorts()) {
                inputPort.freeMemory();
                OutputPort source = inputPort.getSource();
                if (source != null)
                    source.freeMemory();
            }
            // clear all output ports not connected
            for (OutputPort outputPort : operator.getOutputPorts().getAllPorts()) {
                if (!outputPort.isConnected()) {
                    outputPort.freeMemory();
                }
            }
            // operator chains need to free inner execution units
            if (operator instanceof OperatorChain) {
                OperatorChain operatorChain = (OperatorChain) operator;
                operatorChain.getSubprocesses().forEach(ExecutionUnit::freeMemory);
            }
            List<Operator> operatorsReady;
            synchronized (dependencyMap) {
                // remove this now executed operator from all dependencies
                operator.getOutputPorts()
                        .getAllPorts()
                        .stream()
                        .filter(Port::isConnected)
                        .map(p -> p.getDestination().getPorts().getOwner().getOperator())
                        .forEach(o -> {
                            Set<Operator> dependencies = dependencyMap.get(o);
                            if (dependencies != null)
                                dependencies.remove(operator);
                        });
                operatorsReady = dependencyMap.entrySet().stream().filter(e -> e.getValue().isEmpty()).map(Map.Entry::getKey).toList();
                operatorsReady.forEach(dependencyMap::remove);
            }

            List<ForkJoinTask<Void>> nextTasks = operatorsReady.stream().map(o -> new ExecuteOperatorTask(o, dependencyMap).fork()).toList();
            // now execute all tasks
            for (ForkJoinTask<?> task : nextTasks) {
                task.join();
            }
        }
    }

}
