package com.owc.singularity.engine.operator;

import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeCategory;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeInt;
import com.owc.singularity.engine.tools.logging.LogService;

/**
 * This is a super operator for all root operators that are executable on an Execution Service.
 */
public abstract class ExecutableRootOperator extends AbstractRootOperator {

    public static final String PARAMETER_LOG_LEVEL = "log_level";
    public static final String PARAMETER_CPU_CORES = "requires_cpu_cores";
    public static final String PARAMETER_MEMORY = "requires_memory";

    public ExecutableRootOperator(String rootExecutionUnitName) {
        super(rootExecutionUnitName);
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = new LinkedList<>();
        types.add(new ParameterTypeInt(PARAMETER_CPU_CORES,
                "Specifies the number of cpu cores used in an execution environment for executing this pipeline. Can be left blank, then the default of the connection is used.",
                2, Integer.MAX_VALUE, true));
        types.add(new ParameterTypeInt(PARAMETER_MEMORY,
                "Specifies the memory size in MB used in an execution environment for executing this pipeline. Can be left blank, then the default of the connection is used.",
                1024, Integer.MAX_VALUE, true));
        types.add(new ParameterTypeCategory(ExecutableRootOperator.PARAMETER_LOG_LEVEL, "The level of messages that are logged from the process execution.",
                LogService.SELECTABLE_LEVEL_NAMES, LogService.DEFAULT_LEVEL_INDEX));
        types.addAll(super.getParameterTypes());
        return types;
    }
}
