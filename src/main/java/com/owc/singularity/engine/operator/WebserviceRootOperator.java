package com.owc.singularity.engine.operator;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.rules.MDTransformationRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.object.io.FileObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.engine.pipeline.WebservicePipeline.WebservicePipelineExecutionContext;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.pipeline.parameter.conditions.BooleanParameterCondition;
import com.owc.singularity.engine.pipeline.parameter.conditions.EqualEnumCategoryCondition;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;

/**
 * Root Operator for {@link com.owc.singularity.engine.pipeline.WebservicePipeline}s.
 */
@DefinesOperator(module = "core", key = "webservice_pipeline", group = "", name = "Webservice Pipeline", icon = "pipeline_webservice.png")
public class WebserviceRootOperator extends AbstractRootOperator {

    private OutputPort bodySource;
    private InputPort responseSink;

    public static final String PARAMETER_URL = "url";
    public static final String PARAMETER_HTTP_METHOD = "method";
    public static final String PARAMETER_ACCEPT_MULTIPART = "accept_multipart_body";
    public static final String PARAMETER_MIME_TYPE = "mime_type";
    public static final String PARAMETER_AUTHORIZED_USERS = "authorized_users";
    public static final String PARAMETER_AUTHORIZED_USERS_NAME = "user_name";
    public static final String PARAMETER_AUTHORIZED_ROLES = "authorized_roles";
    public static final String PARAMETER_AUTHORIZED_ROLE = "authorized_role";
    public static final String PARAMETER_IS_PUBLIC = "is_public";
    public static final String PARAMETER_ENABLE_CACHING = "caching";
    public static final String PARAMETER_USER_VARIABLE_NAME = "user_variable_name";
    public static final String PARAMETER_ROLES_VARIABLE_NAME = "roles_variable_name";
    public static final String PARAMETER_QUERY_PARAMETER_VARIABLE_MAP = "query_parameter_variable_map";
    public static final String PARAMETER_QUERY_PARAMETER = "query_parameter";
    public static final String PARAMETER_VARIABLE_NAME = "variable_name";
    public static final String PARAMETER_MAX_CACHE_AGE = "max_cache_age";


    public enum HttpMethods {

        GET(false), POST(true), PUT(true), DELETE(false), PATCH(true), HEAD(false);

        boolean hasBody;

        private HttpMethods(boolean hasBody) {
            this.hasBody = hasBody;
        }

        public boolean hasBody() {
            return this.hasBody;
        }
    }

    public WebserviceRootOperator() {
        super("Webservice Endpoint");

        bodySource = getSubprocess(0).getInnerSources().createPort("body", 1d);
        responseSink = getSubprocess(0).getInnerSinks().createPort("response", 1d, FileObject.class);

        getTransformer().addRule(new MDTransformationRule() {

            @Override
            public void transformMD() throws UndefinedParameterError {
                if (ParameterTypeEnumCategory.getParameterAsEnumValue(WebserviceRootOperator.this, PARAMETER_HTTP_METHOD, HttpMethods.class).hasBody()) {
                    if (getParameterAsBoolean(PARAMETER_ACCEPT_MULTIPART))
                        bodySource.deliverMD(new CollectionMetaData(new MetaData(FileObject.class)));
                    else
                        bodySource.deliverMD(new MetaData(FileObject.class));
                }
            }
        });
    }


    @Override
    public List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        WebservicePipelineExecutionContext myContext = (WebservicePipelineExecutionContext) context;
        if (myContext.bodyFileObjects.size() == 1) {
            if (getParameterAsBoolean(PARAMETER_ACCEPT_MULTIPART)) {
                bodySource.deliver(new IOObjectCollection<>(myContext.bodyFileObjects));
            } else
                bodySource.deliver(myContext.bodyFileObjects.get(0));
        } else if (myContext.bodyFileObjects.size() > 1) {
            if (getParameterAsBoolean(PARAMETER_ACCEPT_MULTIPART)) {
                bodySource.deliver(new IOObjectCollection<>(myContext.bodyFileObjects));
            } else
                throw new OperatorException("Multipart body not accepted by endpoint");
        }

        execute();

        return Collections.singletonList(responseSink.getData(FileObject.class));
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        LinkedList<ParameterType> types = new LinkedList<>();
        ParameterType type;
        types.add(new ParameterTypeString(PARAMETER_URL, "The url under that the endpoint implemented by this pipeline is published."));
        types.add(new ParameterTypeEnumCategory<>(PARAMETER_HTTP_METHOD, "The http method of this endpoint.", HttpMethods.class, HttpMethods.GET));
        types.add(type = new ParameterTypeBoolean(PARAMETER_ACCEPT_MULTIPART, "Check the endpoint expects a multipart body to upload multiple files at once.",
                false));
        type.registerDependencyCondition(
                new EqualEnumCategoryCondition<>(this, PARAMETER_HTTP_METHOD, false, HttpMethods.PATCH, HttpMethods.POST, HttpMethods.PUT));
        types.add(new ParameterTypeList(PARAMETER_QUERY_PARAMETER_VARIABLE_MAP, "A map between query parameters and their associated web-process variables.",
                new ParameterTypeString(PARAMETER_QUERY_PARAMETER, "The query parameter to be associated with"),
                new ParameterTypeString(PARAMETER_VARIABLE_NAME, "The process variable that will contain the value of the query parameter.")));
        types.add(new ParameterTypeString(PARAMETER_MIME_TYPE, "The mime type of this endpoint as returned as part of the http response header."));
        types.add(new ParameterTypeBoolean(PARAMETER_IS_PUBLIC, "Publishes the webservice without user access restrictions.", false));
        types.add(new ParameterTypeEnumeration(PARAMETER_AUTHORIZED_USERS,
                "A list of users that are authorized to access this webservice. Depends on authorization method configured on the rest server.",
                new ParameterTypeString(PARAMETER_AUTHORIZED_USERS_NAME, "The name of an authorized user."))
                        .registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_IS_PUBLIC, false, false)));
        types.add(new ParameterTypeEnumeration(PARAMETER_AUTHORIZED_ROLES, "A list of authorized roles.",
                new ParameterTypeString(PARAMETER_AUTHORIZED_ROLE, "The name of an authorized role."))
                        .registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_IS_PUBLIC, false, false)));
        types.add(new ParameterTypeString(PARAMETER_USER_VARIABLE_NAME, "A name for a process variable that will hold the name of the authorized user.", "user")
                .registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_IS_PUBLIC, false, false)));
        types.add(new ParameterTypeString(PARAMETER_ROLES_VARIABLE_NAME,
                "A name for a process variable containing a comma separated list of all roles of the authorized user.", "roles")
                        .registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_IS_PUBLIC, false, false)));
        types.add(new ParameterTypeBoolean(PARAMETER_ENABLE_CACHING, "Determined whether this process shall be cached.", true));
        types.add(new ParameterTypeInt(PARAMETER_MAX_CACHE_AGE, "Maximum cache age as hint for proxy servers before re-requesting resource.", 3600, 0,
                Integer.MAX_VALUE));
        types.addAll(super.getParameterTypes());
        return types;
    }

}
