package com.owc.singularity.engine.operator;

import java.util.List;

import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.pipeline.parameter.*;

/**
 * Root operator for {@link com.owc.singularity.engine.pipeline.ScheduledAnalysisPipeline}s.
 */
@DefinesOperator(module = "core", key = "scheduled_pipeline", group = "", name = "Scheduled Pipeline", icon = "pipeline_scheduled.png")
public class ScheduledAnalysisRootOperator extends AnalysisRootOperator {

    public static final String PARAMETER_EXECUTION_SERVICE = "execution_service";
    public static final String PARAMETER_ENABLE_SCHEDULING = "enable_scheduling";
    public static final String PARAMETER_CRON_EXPRESSION = "cron_expression";
    public static final String PARAMETER_ONCE = "once";

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeBoolean(PARAMETER_ENABLE_SCHEDULING,
                "If checked, this pipeline will be scheduled by scheduling services according to the given schedules.", true));
        types.add(new ParameterTypeCronExpression(PARAMETER_CRON_EXPRESSION, "A cron expression used for deciding, when this pipeline will be executed."));
        types.add(new ParameterTypeDate(PARAMETER_ONCE, "A timestamp when this pipeline is to be executed once."));
        types.add(new ParameterTypeConnectionLocation(PARAMETER_EXECUTION_SERVICE,
                "Optionally select a specific connection. If non specified the most specific connection is searched upwards in the repository folders.",
                "integration:execution_service_connection").setOptional(true));
        return types;
    }
}
