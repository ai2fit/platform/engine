package com.owc.singularity.engine.operator;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.extender.InputPortExtender;

public final class OperationExecutionInputHandler {

    private AbstractOperator<?> operator;

    public OperationExecutionInputHandler(AbstractOperator<?> operator) {
        this.operator = operator;
    }

    public InputPort getInputPort(String name) {
        return operator.getInputPorts().getPortByName(name);
    }

    public <T extends MetaData> T getMetaData(String portName, Class<T> objectClass) {
        return operator.getInputPorts().getPortByName(portName).getMetaData(objectClass);
    }

    public <T extends IOObject> T getData(String portName, Class<T> objectClass) throws UserError {
        return operator.getInputPorts().getPortByName(portName).getData(objectClass);
    }

    public <T extends IOObject> T getDataIfPresent(String portName, Class<T> objectClass) throws UserError {
        return operator.getInputPorts().getPortByName(portName).getDataOrNull(objectClass);
    }

    /**
     * This method returns the data delivered to an {@link InputPortExtender} as list. If boolean
     * flag is ordered, the returned list will have as many entries as the index of the highest
     * connected port. The content will correspond to the respective port. Otherwise, all present,
     * non-null results will be returned.
     * 
     * @param <T>
     *            type of the result. Will throw {@link UserError} if it doesn't fit
     * @param portGroupName
     *            name of the port group
     * @param objectClass
     *            desired type of the class
     * @param ordered
     *            if true, returns ordered list and does not only collect connected results
     * @return list of delivered objects
     * @throws UserError
     */
    public <T extends IOObject> List<T> getAllData(String portGroupName, Class<T> objectClass, boolean ordered) throws UserError {
        return operator.getInputPortGroup(portGroupName).getData(objectClass, ordered);
    }

    /**
     * This method returns the data delivered to an {@link InputPortExtender} as list. If boolean
     * flag is ordered, the returned list will have as many entries as the index of the highest
     * connected port. The content will correspond to the respective port. Otherwise, all present,
     * non-null results will be returned.
     *
     * @param <T>
     *            type of the result. Will throw {@link UserError} if it doesn't fit
     * @param portGroupName
     *            name of the port group
     * @param objectClass
     *            desired type of the class
     * @param ordered
     *            if true, returns ordered list and does not only collect connected results
     * @param unfold
     *            if true, returns collections as lists
     * @return list of delivered objects
     * @throws UserError
     */
    public <T extends IOObject> List<T> getAllData(String portGroupName, Class<T> objectClass, boolean ordered, boolean unfold) throws UserError {
        return operator.getInputPortGroup(portGroupName).getData(objectClass, ordered, unfold);
    }


    /**
     * This method returns the data delivered to an {@link InputPortExtender} as list. It will
     * return a list without nulls and unfold all collections by searching them for the requested
     * type. Objects of different types will be ignored.
     * 
     * @param <T>
     *            type of the result. Will throw {@link UserError} if it doesn't fit
     * @param portGroupName
     *            name of the port extender
     * @param objectClass
     *            desired type of the class
     * @return list of delivered objects
     * @throws UserError
     */
    @SuppressWarnings("unchecked")
    public <T extends IOObject> List<T> getAllUnfoldedData(String portGroupName, Class<T> objectClass) throws UserError {
        LinkedList<T> result = new LinkedList<>();

        for (IOObject object : operator.getInputPortGroup(portGroupName).getData(IOObject.class, false)) {
            if (objectClass.isAssignableFrom(object.getClass())) {
                result.add((T) object);
            } else if (object instanceof IOObjectCollection<?>) {
                for (IOObject collectedObject : ((IOObjectCollection<?>) object).getObjectsRecursive()) {
                    if (objectClass.isAssignableFrom(collectedObject.getClass())) {
                        result.add((T) collectedObject);
                    }
                }
            }
        }
        return result;
    }

    public List<MetaData> getAllMetaData(String portExtenderName) {
        return operator.getInputPortGroup(portExtenderName).getMetaData(false);
    }


    public void addMetaDataError(String portName, Function<InputPort, MetaDataError> errorProvider) {
        InputPort port = operator.getInputPorts().getPortByName(portName);
        port.addError(errorProvider.apply(port));
    }

}
