package com.owc.singularity.engine.operator.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Retention(RUNTIME)
@Target(TYPE)
public @interface MarkDeprecated {

    /**
     * @return This is the version string since when this operator is deprecated.
     * 
     */
    String since();

    /**
     * 
     * @return The reason why it has been deprecated.
     */
    String description();
}
