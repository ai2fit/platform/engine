package com.owc.singularity.engine.operator;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.Attributes;
import com.owc.singularity.engine.operator.AbstractOperator.OperationExecutionContext;
import com.owc.singularity.engine.operator.error.UserError;

public class InputValidator {

    private OperationExecutionContext operationContext;

    public InputValidator(OperationExecutionContext operationContext) {
        this.operationContext = operationContext;
    }

    /**
     * This method checks whether the attribute with the given name is present in the attributes of
     * an exampleSet and returns it. Otherwise, it will throw a UserError pointing to the defining
     * parameter and port.
     * 
     * @param attributeName
     * @param attributes
     * @param specifiyingParameter
     */
    public Attribute checkAttributePresent(String attributeName, Attributes attributes, String inputPortName, String specifiyingParameter) throws UserError {
        Attribute attribute = attributes.get(attributeName);
        if (attribute == null)
            throw new UserError(operationContext.operator, "validator.attributes_not_found", attributeName, specifiyingParameter, inputPortName);
        return attribute;
    }

    /**
     * This method checks whether the attributes specified by name and source
     * 
     * @param firstAttribute
     * @param secondAttribute
     * @throws UserError
     */

    public void checkAttributeSameTypes(Attribute firstAttribute, Attribute secondAttribute) throws UserError {
        if (!firstAttribute.getValueType().equals(secondAttribute.getValueType()))
            throw new UserError(operationContext.operator, "validator.attributes_unequal_type", firstAttribute.getName(), secondAttribute.getName(),
                    firstAttribute.getValueType(), secondAttribute.getValueType());
    }

}
