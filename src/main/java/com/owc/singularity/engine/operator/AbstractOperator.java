package com.owc.singularity.engine.operator;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.function.Function;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.SimpleMetaDataError;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;
import com.owc.singularity.engine.operator.error.ProcessSetupError;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.parameter.AbstractParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.ports.extender.InputPortExtender;
import com.owc.singularity.engine.ports.extender.OneToOneExtender;
import com.owc.singularity.engine.ports.extender.OutputPortExtender;
import com.owc.singularity.engine.tools.function.ExceptionHandler.ThrowingBiConsumer;
import com.owc.singularity.engine.tools.function.ExceptionHandler.ThrowingFunction;
import com.owc.singularity.engine.tools.function.ObjIntFunction;
import com.owc.singularity.repository.RepositoryPath;

/**
 * The AbstractOperator should be used instead of the {@link Operator} in almost all cases. It
 * splits the operator execution into stages and provides shortcuts for defining ports and actions.
 * An Action is always executed when an operator is executed, whereas output port objects are only
 * calculated if the port is connected. Stages function as follows: The configuration stage is
 * always executed and should be used for parameter retrieval and error checking. For all subsequent
 * stages, as well as any outputs and actions the configuration object is available. Operators can
 * define additional stages using {@link #defineStage(String, OutputFunction)} and add dependency to
 * those stages for actions, outputs or other stages using
 * {@link #registerStageDependency(String, String...)},
 * {@link #registerOutputDependency(String, String...)} or
 * {@link #registerActionDependency(String, String...)}. The stage results can be retrieved using
 * {@link OperationExecutionContext#getStageResult(String)}, which can be useful if multiple
 * outputs/actions depend on the same calculation.
 *
 * @param <C>
 *            the configuration class, which will be returned from the configuration phase
 */
public abstract class AbstractOperator<C> extends Operator {

    public static final double THROUGH_PORT_PRIORITY = 1000d;
    public static final String THROUGH_PORT_NAME = "through";

    @FunctionalInterface
    public interface OutputFunction<T, U, R> {

        R apply(T t, U u) throws OperatorException;
    }

    private interface OutputDefinition<C> {

        void computeResults(OperationExecutionContext context, C config) throws OperatorException;

        void computeMetaDataResults(OperationDescriptionContext context) throws OperatorException;

        boolean isDependingOnStage(String stageName);

        Set<String> getDependencies();

        void registerStageDependency(String stageName);

        boolean isConnected();

        String getPortBaseName();
    }

    private static class OutputPortDefinition<D> implements OutputDefinition<D> {

        private final OutputPort port;
        private final ThrowingFunction<OperationDescriptionContext, ? extends MetaData> metaDataProvider;
        private final OutputFunction<OperationExecutionContext, D, IOObject> resultProvider;
        private final Set<String> stageDependencies = new HashSet<>();

        public OutputPortDefinition(OutputPort port, ThrowingFunction<OperationDescriptionContext, ? extends MetaData> metaDataProvider,
                OutputFunction<OperationExecutionContext, D, IOObject> resultProvider) {
            this.port = port;
            this.metaDataProvider = metaDataProvider;
            this.resultProvider = resultProvider;
        }

        @Override
        public void computeResults(OperationExecutionContext context, D config) throws OperatorException {
            port.deliver(resultProvider.apply(context, config));
        }

        @Override
        public void computeMetaDataResults(OperationDescriptionContext context) throws OperatorException {
            port.deliverMD(metaDataProvider.apply(context));
        }

        @Override
        public boolean isDependingOnStage(String stageName) {
            return stageDependencies.contains(stageName);
        }

        @Override
        public void registerStageDependency(String stageName) {
            stageDependencies.add(stageName);
        }

        @Override
        public Set<String> getDependencies() {
            return stageDependencies;
        }

        @Override
        public boolean isConnected() {
            return port.isConnected();
        }

        @Override
        public String getPortBaseName() {
            return port.getName();
        }

    }

    private class OutputPortGroupDefinition<D> implements OutputDefinition<D> {

        private final OutputPortGroup portGroup;
        private final OutputFunction<OperationExecutionContext, D, List<? extends IOObject>> resultProvider;
        private final Set<String> stageDependencies = new HashSet<>();
        private final String baseName;
        private final ThrowingFunction<OperationDescriptionContext, List<MetaData>> metaDataProvider;

        public OutputPortGroupDefinition(OutputPortGroup portGroup, String baseName,
                ThrowingFunction<OperationDescriptionContext, List<MetaData>> metaDataProvider,
                OutputFunction<OperationExecutionContext, D, List<? extends IOObject>> resultProvider) {
            this.portGroup = portGroup;
            this.baseName = baseName;
            this.metaDataProvider = metaDataProvider;
            this.resultProvider = resultProvider;
        }

        public OutputPortGroupDefinition(OutputPortGroupDefinition<C> oldDefinition,
                ThrowingFunction<OperationDescriptionContext, List<MetaData>> metaDataProvider,
                OutputFunction<OperationExecutionContext, D, List<? extends IOObject>> resultProvider) {
            this.portGroup = oldDefinition.portGroup;
            this.baseName = oldDefinition.baseName;
            this.metaDataProvider = metaDataProvider;
            this.resultProvider = resultProvider;
        }

        @Override
        public void computeResults(OperationExecutionContext context, D config) throws OperatorException {
            portGroup.deliver(resultProvider.apply(context, config));
        }

        @Override
        public void computeMetaDataResults(OperationDescriptionContext context) throws OperatorException {
            portGroup.deliverMetaData(metaDataProvider.apply(context));
        }

        @Override
        public boolean isDependingOnStage(String stageName) {
            return stageDependencies.contains(stageName);
        }

        @Override
        public void registerStageDependency(String stageName) {
            stageDependencies.add(stageName);
        }

        @Override
        public Set<String> getDependencies() {
            return stageDependencies;
        }

        @Override
        public boolean isConnected() {
            return portGroup.isAnyPortConnected();
        }

        @Override
        public String getPortBaseName() {
            return baseName;
        }

    }


    private class StageDefinition {

        String name;
        OutputFunction<OperationExecutionContext, C, Object> resultProvider;
        Set<String> stageDependencies = new HashSet<>();

        public StageDefinition(String name, OutputFunction<OperationExecutionContext, C, Object> resultProvider) {
            this.name = name;
            this.resultProvider = resultProvider;
        }
    }

    private class ActionDefinition {

        String name;
        ThrowingBiConsumer<OperationExecutionContext, C> action;
        Set<String> stageDependencies = new HashSet<>();

        public ActionDefinition(String name, ThrowingBiConsumer<OperationExecutionContext, C> action) {
            this.name = name;
            this.action = action;
        }
    }

    public static final class OperationExecutionContext {

        public ParameterHandler parameters;
        public OperationExecutionInputHandler inputs;
        public HashMap<String, Object> stageResults = new HashMap<>();
        public Operator operator;
        public RepositoryPath workingPath;

        public OperationExecutionContext(AbstractOperator<?> operator) {
            this.operator = operator;
            this.parameters = operator;
            this.inputs = new OperationExecutionInputHandler(operator);
            workingPath = (operator.getPipeline() != null) ? operator.getPipeline().getPath() : null;
        }

        @SuppressWarnings("unchecked")
        public <T> T getStageResult(String name, Class<? extends T> clazz) {
            return (T) stageResults.get(name);
        }

        public Object getStageResult(String name) {
            return stageResults.get(name);
        }
    }

    public static class OperationDescriptionContext implements Serializable {

        private static final class DescriptionParameterHandler extends AbstractParameterHandler implements Serializable {

            private static final long serialVersionUID = -3960384342261193889L;
            private final List<ParameterType> types;

            private DescriptionParameterHandler(ParameterHandler handler) {
                super(handler);
                types = handler.getParameterTypes();
            }

            @Override
            public List<ParameterType> getParameterTypes() {
                return types;
            }
        }

        private static final long serialVersionUID = 2547626839764264597L;
        public ParameterHandler parameters;
        public OperationDescriptionInputHandler inputs;
        private String path;
        public transient Operator operator;

        public OperationDescriptionContext(Operator operator) {
            this.operator = operator;
            this.parameters = new DescriptionParameterHandler(operator);
            this.inputs = new OperationDescriptionInputHandler(operator);
            path = null;
            AbstractPipeline pipeline = operator.getPipeline();
            if (pipeline != null) {
                RepositoryPath processLocation = pipeline.getPath();
                if (processLocation != null)
                    path = processLocation.toString();
            }
        }

        public void addError(Function<PortOwner, ProcessSetupError> errorProvider) {
            if (operator != null)
                operator.addError(errorProvider.apply(operator.getPortOwner()));
        }

        public RepositoryPath getPath() {
            if (path == null)
                return null;
            return RepositoryPath.of(path);
        }
    }

    private final LinkedHashMap<String, InputPortGroup> inputs = new LinkedHashMap<>(1);

    private final LinkedList<StageDefinition> stages = new LinkedList<>();

    private final LinkedHashMap<String, ActionDefinition> actions = new LinkedHashMap<>();
    private final LinkedHashMap<String, OutputDefinition<C>> outputs = new LinkedHashMap<>();


    /**
     * This method can be used to define an input port extender for this operator. It will be named
     * according to name and the preconditionProvider will be used to add a precondition. At least
     * one precondition e.g. for a certain type must be registered. If subclasses need to refine the
     * precondition, they can add additional preconditions by calling the same method again. <br>
     * The data from this input port extender can be collected as list from the
     * {@link OperationExecutionContext#inputs}
     * 
     * @param name
     *            the name of the port
     * @param desiredMetaData
     *            specifies the type of required object
     * @param numberOfMinimalRequiredConnections
     *            number of ports that need to be connected
     */
    protected final void defineInputPortExtender(String name, double priority, MetaData desiredMetaData, int numberOfMinimalRequiredConnections) {
        InputPortExtender extender = new InputPortExtender(name, priority, getInputPorts(), desiredMetaData, numberOfMinimalRequiredConnections);
        extender.start();
        inputs.put(name, extender);
    }

    /**
     * This method can be used to define an input port extender for this operator. It will be named
     * according to name and the preconditionProvider will be used to add a precondition. At least
     * one precondition e.g. for a certain type must be registered. If subclasses need to refine the
     * precondition, they can add additional preconditions by calling the same method again. <br>
     * The data from this input port extender can be collected as list from the
     * {@link OperationExecutionContext#inputs}
     * 
     * @param name
     *            the name of the port
     * @param preconditionProvider
     *            a provider giving the precondition for a port with an index
     */
    protected final void defineInputPortExtender(String name, double priority, ObjIntFunction<InputPort, Precondition> preconditionProvider) {
        InputPortExtender extender = new InputPortExtender(name, priority, getInputPorts(), preconditionProvider);
        extender.start();
        inputs.put(name, extender);
    }

    /**
     * This method can be used to define an input port for this operator. It will be named according
     * to name and the preconditionProvider will be used to add a precondition. At least one
     * precondition e.g. for a certain type must be registered. If subclasses need to refine the
     * precondition, they can add additional preconditions by calling the same method again.
     * 
     * @param name
     *            the name of the port
     * @param portPriority
     *            priority of the port. lowest priority will be first port
     * @param preconditionProvider
     *            a provider delivering the precondition, given the port object
     */
    protected final void defineInputPort(String name, double portPriority, Function<InputPort, Precondition> preconditionProvider) {
        InputPort port = getInputPorts().getPortByName(name);
        if (port == null)
            port = getInputPorts().createPort(name, portPriority);
        port.addPrecondition(preconditionProvider.apply(port));
    }

    protected final void registerPortPrecondition(InputPort port, Function<InputPort, Precondition> preconditionProvider) {
        port.addPrecondition(preconditionProvider.apply(port));
    }

    protected final void defineThroughPorts() {
        OneToOneExtender extender = new OneToOneExtender(THROUGH_PORT_NAME, THROUGH_PORT_PRIORITY, getInputPorts(), getOutputPorts(),
                (ObjIntFunction<InputPort, Precondition>) null, 0).start();
        inputs.put(THROUGH_PORT_NAME, extender);
        getTransformer().addRule(extender.makePassThroughRule());

        outputs.put(THROUGH_PORT_NAME,
                new OutputPortGroupDefinition<C>(extender, THROUGH_PORT_NAME, context -> context.inputs.getAllMetaData(THROUGH_PORT_NAME),
                        (context, c) -> context.inputs.getAllData(THROUGH_PORT_NAME, IOObject.class, true)));
    }

    /**
     * This method allows operators to define actions that are executed independent of the data flow
     * and any connections. This can be used for side effects like changing pipeline variables or
     * interacting with external systems like file system, databases or webservices.
     * 
     * @param action
     *            the action that will be executed by this operator during exection. Action is
     *            independent of any
     */
    protected final void defineAction(String name, ThrowingBiConsumer<OperationExecutionContext, C> action) {
        actions.put(name, new ActionDefinition(name, action));
    }

    /**
     * This method defines a stage during this operators execution. Stages can be used, when results
     * of a computation are used by more than one output port or action definition. Stages deliver
     * an intermediate result that can be accessed by the outputport or action definition over the
     * {@link OperationExecutionContext#getStageResult(String)}. method. <br/>
     * Stages are only computed if there's an outputport connected or an action defined that depends
     * on this stage's result. Dependencies are registered in an operators's constructor using the
     * {@link #registerActionDependency(String, String...)},
     * {@link #registerOutputDependency(String, String...)}. <br>
     * Stages can also depend on other stages. This type of dependency can be registered by
     * {@link #registerStageDependency(String, String...)}. Identification works over the names of
     * actions, output ports and stages. Hence they have to be unique.
     * 
     * @param name
     *            the name of this stage for later registering dependencies
     * @param resultProvider
     *            the provider that delivers results.
     */
    protected final void defineStage(String name, OutputFunction<OperationExecutionContext, C, Object> resultProvider) {
        stages.add(new StageDefinition(name, resultProvider));
    }

    /**
     * This can be called to define an output port with the given name. Ports are ordered by
     * portPriority, so you can use it to have a sensible order. Please notice, that priority can
     * only be assigned during first call and will be igored on subsequent calls.<br>
     * Subsequent calls can be used to refine the metadata provision or output function in cases,
     * where subclasses need to overwrite part of the logic.
     *
     * @param name
     *            name of the port
     * @param portPriority
     *            priority used for ordering on the first definition only
     * @param metaDataProvider
     *            the metaDataprovider giving the meta data results of this port
     * @param resultProvider
     *            the result provider giving the real results of this port
     */
    protected final void defineOutputPort(String name, double portPriority, ThrowingFunction<OperationDescriptionContext, ? extends MetaData> metaDataProvider,
            OutputFunction<OperationExecutionContext, C, IOObject> resultProvider) {

        OutputPort port = getOutputPorts().getPortByName(name);
        if (port == null) {
            OutputPort newPort = getOutputPorts().createPort(name, portPriority);
            getTransformer().addRule(() -> {
                try {
                    outputs.get(name).computeMetaDataResults(new OperationDescriptionContext(AbstractOperator.this));
                } catch (OperatorException e) {
                    newPort.addError(new SimpleMetaDataError(Severity.WARNING, newPort, "cannot_determine_metadata", e.getMessage()));
                }
            });
            port = newPort;
        }
        outputs.put(name, new OutputPortDefinition<C>(port, metaDataProvider, resultProvider));

    }

    /**
     * This can be called to define an output port extender with the given name. Ports are ordered
     * by portPriority, so you can use it to have a sensible order. Ports of an extender will be
     * kept together but you can use this priority to put it in a global order. Please notice, that
     * priority can only be assigned during first call and will be igored on subsequent calls.<br>
     * Subsequent calls can be used to refine the metadata provision or output function in cases,
     * where subclasses need to overwrite part of the logic.
     *
     * @param name
     *            name of the port
     * @param portPriority
     *            priority used for ordering on the first definition only
     * @param metaDataProvider
     *            the metaDataprovider giving a list of meta data results of this extender's ports
     * @param resultProvider
     *            the result provider giving a list of the real results of this extender's ports
     */
    protected final void defineOutputPortExtender(String name, double portPriority,
            ThrowingFunction<OperationDescriptionContext, List<MetaData>> metaDataProvider,
            OutputFunction<OperationExecutionContext, C, List<? extends IOObject>> resultProvider) {
        OutputDefinition<C> oldDefinition = outputs.get(name);
        if (oldDefinition == null) {
            OutputPortExtender portExtender = new OutputPortExtender(name, portPriority, getOutputPorts());
            portExtender.start();
            getTransformer().addRule(() -> {
                try {
                    outputs.get(name).computeMetaDataResults(new OperationDescriptionContext(AbstractOperator.this));
                } catch (OperatorException e) {
                    List<OutputPort> managedPorts = portExtender.getManagedPorts();
                    if (!managedPorts.isEmpty())
                        managedPorts.get(0)
                                .addError(new SimpleMetaDataError(Severity.WARNING, managedPorts.get(0), "cannot_determine_metadata", e.getMessage()));
                }
            });
            outputs.put(name, new OutputPortGroupDefinition<C>(portExtender, name, metaDataProvider, resultProvider));
        } else {
            // otherwise we override the meta and result provider
            outputs.put(name,
                    new OutputPortGroupDefinition<C>((AbstractOperator<C>.OutputPortGroupDefinition<C>) oldDefinition, metaDataProvider, resultProvider));
        }

    }


    protected final void registerStageDependency(String stageName, String... dependingOnStages) {
        for (StageDefinition stageDefinition : stages) {
            if (stageDefinition.name.equals(stageName)) {
                stageDefinition.stageDependencies.addAll(Arrays.asList(dependingOnStages));
                return;
            }
        }
        throw new IllegalArgumentException("Stage with name " + stageName + " is unknown.");
    }

    protected final void registerOutputDependency(String portName, String... dependingOnStages) {
        OutputDefinition<C> outputDefinition = outputs.get(portName);
        if (outputDefinition != null)
            for (String stage : dependingOnStages)
                outputDefinition.registerStageDependency(stage);
        else
            throw new IllegalArgumentException("Port with name " + portName + " is unknown.");
    }

    protected final void registerActionDependency(String actionName, String... dependingOnStages) {
        ActionDefinition actionDefinition = actions.get(actionName);
        if (actionDefinition != null)
            Collections.addAll(actionDefinition.stageDependencies, dependingOnStages);
        else
            throw new IllegalArgumentException("Action with name " + actionName + " is unknown.");
    }

    @Override
    public void doWork() throws OperatorException {
        OperationExecutionContext operationContext = new OperationExecutionContext(this);

        // create configuration for futures
        C configuration = configure(operationContext, new InputValidator(operationContext));

        // with this configuration we now create completable futures for each stage.
        HashMap<String, ForkJoinTask<?>> stageFutures = new HashMap<>();
        Set<ForkJoinTask<?>> startedStageTasks = new HashSet<>();
        for (StageDefinition stage : stages) {
            createStageFuture(stage, stageFutures, startedStageTasks, operationContext, configuration);
        }

        // and now we compute the results for each ports and each action while using the stages'
        // completeable
        // futures to calculate their results
        try {
            List<ForkJoinTask<Void>> outputFutures = outputs.values().stream().filter(OutputDefinition::isConnected).map(outputDefinition -> {
                // search predecessor stages
                ForkJoinTask<?>[] predecessors = new ForkJoinTask[outputDefinition.getDependencies().size()];
                int i = 0;
                for (String dependency : outputDefinition.getDependencies()) {
                    predecessors[i++] = stageFutures.get(dependency);
                }
                // require calculation of predecessors first, then calculate result
                return new RecursiveAction() {

                    private static final long serialVersionUID = 1L;

                    @Override
                    protected void compute() {
                        // starting computation that has not been done
                        synchronized (startedStageTasks) {
                            for (ForkJoinTask<?> predecessor : predecessors) {
                                if (!startedStageTasks.contains(predecessor)) {
                                    predecessor.fork();
                                    startedStageTasks.add(predecessor);
                                }
                            }
                        }
                        // waiting for all to finish
                        for (ForkJoinTask<?> predecessor : predecessors)
                            predecessor.join();
                        try {
                            outputDefinition.computeResults(operationContext, configuration);
                        } catch (OperatorException e) {
                            throw new OperatorRuntimeException(e);
                        }
                    }
                }.fork();
            }).toList();
            List<ForkJoinTask<Void>> actionFutures = actions.values().stream().map(actionDefinition -> {
                // search predecessor stages
                ForkJoinTask<?>[] predecessors = new ForkJoinTask[actionDefinition.stageDependencies.size()];
                int i = 0;
                for (String dependency : actionDefinition.stageDependencies) {
                    predecessors[i++] = stageFutures.get(dependency);
                }
                // require calculation of predecessors first, then perform action
                return new RecursiveAction() {

                    private static final long serialVersionUID = 1L;

                    @Override
                    protected void compute() {
                        // starting computation that has not been done
                        synchronized (startedStageTasks) {
                            for (ForkJoinTask<?> predecessor : predecessors) {
                                if (!startedStageTasks.contains(predecessor)) {
                                    predecessor.fork();
                                    startedStageTasks.add(predecessor);
                                }
                            }
                        }
                        // waiting for all to finish
                        for (ForkJoinTask<?> predecessor : predecessors)
                            predecessor.join();
                        try {
                            actionDefinition.action.apply(operationContext, configuration);
                        } catch (OperatorException e) {
                            throw new OperatorRuntimeException(e);
                        }
                    }
                }.fork();
            }).toList();

            // executing both streams at the same time
            outputFutures.forEach(t -> t.join());
            actionFutures.forEach(t -> t.join());
        } catch (RuntimeException | Error e) {
            processThrowable(e);
        }
    }

    public static void processThrowable(Throwable e) throws OperatorException {
        if (e instanceof OperatorException)
            throw (OperatorException) e;
        else if (e instanceof OperatorRuntimeException)
            throw ((OperatorRuntimeException) e).toOperatorException();
        else if ((e instanceof RuntimeException || e instanceof ExecutionException) && e != e.getCause() && e.getCause() != null) {
            processThrowable(e.getCause());
        }
        throw new OperatorException("Unknown error during parallel result computation.", e);
    }

    private ForkJoinTask<?> createStageFuture(AbstractOperator<C>.StageDefinition stage, HashMap<String, ForkJoinTask<?>> stageFutures,
            Set<ForkJoinTask<?>> startedStageTasks, OperationExecutionContext operationContext, C configuration) {
        if (!stageFutures.containsKey(stage.name)) {
            ForkJoinTask<?>[] predecessors = new ForkJoinTask[stage.stageDependencies.size()];
            int i = 0;
            for (String dependency : stage.stageDependencies) {
                ForkJoinTask<?> stageFuture = stageFutures.get(dependency);
                if (stageFuture == null) {
                    stageFuture = createStageFuture(getStageDefinition(dependency), stageFutures, startedStageTasks, operationContext, configuration);
                    stageFutures.put(dependency, stageFuture);
                }
                predecessors[i++] = stageFuture;
            }
            stageFutures.put(stage.name, new RecursiveAction() {

                private static final long serialVersionUID = -8419545599825160871L;

                @Override
                protected void compute() {
                    // starting computation that has not been done
                    synchronized (startedStageTasks) {
                        for (ForkJoinTask<?> predecessor : predecessors) {
                            if (!startedStageTasks.contains(predecessor)) {
                                predecessor.fork();
                                startedStageTasks.add(predecessor);
                            }
                        }
                    }
                    // waiting for all to finish
                    for (ForkJoinTask<?> predecessor : predecessors)
                        predecessor.join();
                    try {
                        operationContext.stageResults.put(stage.name, stage.resultProvider.apply(operationContext, configuration));
                    } catch (OperatorException e) {
                        throw new OperatorRuntimeException(e);
                    }
                }
            });
        }
        return stageFutures.get(stage.name);
    }

    private StageDefinition getStageDefinition(String name) {
        for (StageDefinition stage : stages)
            if (stage.name.equals(name))
                return stage;
        throw new IllegalArgumentException("Stage is unknown.");
    }

    /**
     * This derives a configuration object from the operationContext. This object is later used to
     * calculate the results in the operation's stages. All user input should be checked here to
     * avoid having that to do within multiple stages. So each stage calculation can rely on proper
     * input.
     * 
     * @param operationContext
     * @param inputValidator
     *            TODO
     * @return
     * @throws UserError
     * @throws OperatorException
     */
    protected abstract C configure(OperationExecutionContext operationContext, InputValidator inputValidator) throws UserError, OperatorException;

    protected InputPortGroup getInputPortGroup(String name) {
        return inputs.get(name);
    }
}
