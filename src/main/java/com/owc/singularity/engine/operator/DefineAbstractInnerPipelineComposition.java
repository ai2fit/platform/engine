package com.owc.singularity.engine.operator;


import java.util.List;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.metadata.rules.SubprocessesTransformationRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.CompositionImplementation;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.pipeline.io.transformer.OperatorReplacementTransformer;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeBoolean;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeString;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.ports.extender.InputPortExtender;
import com.owc.singularity.engine.ports.extender.OneToOneExtender;
import com.owc.singularity.engine.ports.extender.OutputPortExtender;
import com.owc.singularity.engine.tools.TreeVisitResult;
import com.owc.singularity.engine.tools.TreeVisitor;
import com.owc.singularity.studio.gui.MainFrame;

/**
 * This operator allows to mark a part of a pipeline as abstract. If it is executed it will throw an
 * extension unless it is executed within an ExecuteAbstractProcess that implements the abstract
 * sub-pipeline.
 * 
 * @author Sebastian Land
 *
 */
@DefinesOperator(module = "core", key = "define_abstract_inner_pipeline", group = "pipeline_control", name = "Define Abstract Inner Pipeline", shortName = "Define Abstract Inner Pipeline", icon = "selection.png")
public class DefineAbstractInnerPipelineComposition extends OperatorChain {

    public static final String PARAMETER_ABSTRACT_INNER_PIPELINE_NAME = ImplementingRootOperator.PARAMETER_ABSTRACT_SUBPROCESS_NAME;
    public static final String PARAMETER_HAS_DEFAULT_IMPLEMENTATION = "has_default_implementation";

    private final OneToOneExtender inputExtender = new OneToOneExtender("in", 1d, getInputPorts(), getSubprocess(0).getInnerSources());
    private final OneToOneExtender outputExtender = new OneToOneExtender("out", 1d, getSubprocess(0).getInnerSinks(), getOutputPorts());

    public DefineAbstractInnerPipelineComposition() {
        super("Default");

        inputExtender.start();
        outputExtender.start();

        getTransformer().addRule(inputExtender.makePassThroughRule());
        getTransformer().addRule(new SubprocessesTransformationRule(this));
        getTransformer().addRule(outputExtender.makePassThroughRule());
    }

    @Override
    public void doWork() throws OperatorException {
        CompositionImplementation implementingComposition = getPipeline().getExecutionContext()
                .getImplementation(getParameterAsString(PARAMETER_ABSTRACT_INNER_PIPELINE_NAME));
        if (implementingComposition != null) {
            // prepare execution unit
            ExecutionUnit executionUnit = addSubprocess(1);
            InputPortExtender sinkExtender = new InputPortExtender("out", 1d, executionUnit.getInnerSinks());
            sinkExtender.start();
            OutputPortExtender sourceExtender = new OutputPortExtender("in", 1d, executionUnit.getInnerSources());
            sourceExtender.start();

            // clone execution unit
            ExecutionUnit implementingExecutionUnit = implementingComposition.getExecutionUnit();
            executionUnit.cloneExecutionUnitFrom(implementingExecutionUnit, true);


            // wire data and execution
            sourceExtender.deliver(inputExtender.getData(IOObject.class, true));
            try {
                executionUnit.execute();
            } catch (OperatorException e) {
                // since we cloned the implementing execution unit, the errors from these clones
                // will point to the cloned operator and not the original
                // try to remap the clone to the original operator based on its name
                if (e.getOperator() != null) {
                    String operatorName = e.getOperator().getName();
                    implementingExecutionUnit.getAllInnerOperators()
                            .stream()
                            .filter(op -> op.getName().equals(operatorName))
                            .findFirst()
                            .ifPresent(e::setOperator);
                }
                throw e;
            }
            outputExtender.deliver(sinkExtender.getData(IOObject.class, true));

            // remove execution unit
            removeSubprocess(1);
        } else {
            if (getParameterAsBoolean(PARAMETER_HAS_DEFAULT_IMPLEMENTATION) || (getPipeline().getPath() != null
                    && (SingularityEngine.getExecutionMode().hasMainFrame() && MainFrame.isMainFrameProcessLocation(getPipeline().getPath())))) {
                inputExtender.passDataThrough();
                getSubprocess(0).execute();
                outputExtender.passDataThrough();
            } else {
                // if directly executing the process without implementation
                throw new UserError(this, "abstract_pipeline.no_implementation", getParameterAsString(PARAMETER_ABSTRACT_INNER_PIPELINE_NAME));
            }
        }
    }

    @Override
    public List<Operator> getAllInnerOperators() {
        AbstractPipeline pipeline = getPipeline();
        if (pipeline != null) {
            PipelineExecutionContext executionContext = pipeline.getExecutionContext();
            if (executionContext != null) {
                try {
                    CompositionImplementation implementingComposition = executionContext
                            .getImplementation(getParameterAsString(PARAMETER_ABSTRACT_INNER_PIPELINE_NAME));
                    if (implementingComposition != null)
                        return implementingComposition.getExecutionUnit().getAllInnerOperators();
                } catch (UndefinedParameterError e) {
                }
            }
        }
        return super.getAllInnerOperators();
    }

    @Override
    public TreeVisitResult walk(TreeVisitor<ExecutionUnit, Operator> visitor) {
        try {
            CompositionImplementation implementingComposition = getPipeline().getExecutionContext()
                    .getImplementation(getParameterAsString(PARAMETER_ABSTRACT_INNER_PIPELINE_NAME));
            if (implementingComposition != null) {
                ExecutionUnit implementingExecutionUnit = implementingComposition.getExecutionUnit();
                if (implementingExecutionUnit != null) {
                    TreeVisitResult visitResultBefore = visitor.preVisitNode(implementingExecutionUnit);
                    if (visitResultBefore == TreeVisitResult.TERMINATE || visitResultBefore == TreeVisitResult.SKIP_SIBLINGS) {
                        return visitResultBefore;
                    }
                    if (visitResultBefore == TreeVisitResult.CONTINUE) {
                        for (Operator op : implementingExecutionUnit.getOperators()) {
                            TreeVisitResult walkResult = op.walk(visitor);
                            if (walkResult == TreeVisitResult.SKIP_SIBLINGS || walkResult == TreeVisitResult.SKIP_SUBTREE)
                                break;
                            if (walkResult == TreeVisitResult.TERMINATE) {
                                return walkResult;
                            }
                            // continue
                        }
                    }
                    // we scanned/skipped all children
                    // no call the post-visit function
                    return visitor.postVisitNode(implementingExecutionUnit);
                }
            }
        } catch (UndefinedParameterError ignored) {
            // continue
        }

        return super.walk(visitor);
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeString(PARAMETER_ABSTRACT_INNER_PIPELINE_NAME,
                "This is the name of the abstract inner pipeline, that is used when implementing this abstract pipeline.", false));
        types.add(new ParameterTypeBoolean(PARAMETER_HAS_DEFAULT_IMPLEMENTATION,
                "If checked, the default implementation can be used and it does not necessarily needs to be overridden. If unchecked, the inner subprocess will just be used for development purposes.",
                false));

        return types;
    }


    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class LegacyAbstractSubProcessTransformer extends OperatorReplacementTransformer {

        public LegacyAbstractSubProcessTransformer() throws Exception {
            super("rmx_speds:define_abstract_subprocess", "core:define_abstract_inner_pipeline");
        }

    }

}
