package com.owc.singularity.engine.operator.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * This annotation makes an operator available for using. It is automatically detected by
 * reflection.
 * <p>
 * IMPORTANT: Every implementation being annotated with this type needs to be in a subpackage of
 * "com.owc.singularity" in order to be recognized.
 * 
 * @author Sebastian Land
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface DefinesOperator {

    public static final String NO_VALUE = "";

    /**
     * This is the module of this operator. Modules are used to separate operators when two modules
     * provide operators the same keys
     */
    String module();

    /**
     * The key of this operator. Used to persist processes so this may not be changed without making
     * process files unreadable.
     */
    String key();

    /**
     * This is the human-readable full name of this operator. May be changed at any time.
     */
    String name();

    /**
     * This is the short name of the operator if details can be reduced. Is used e.g. for naming
     * operators in a process. It is optional and can be left blank, then the short name is equal to
     * the name.
     */
    String shortName() default NO_VALUE;

    /**
     * This is a dot separated list of group keys, with that this operator is assigned a position in
     * the operator tree.
     */
    String group();

    /**
     * This returns an icon for the operator
     */
    String icon();
}
