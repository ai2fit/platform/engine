/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;

/**
 * The possible capabilities for all learners.
 * 
 * @author Julien Nioche, Ingo Mierswa
 */
public enum OperatorCapability {

    NOMINAL_ATTRIBUTES("nominal attributes"), NUMERICAL_ATTRIBUTES("numerical attributes"), MULTI_CLASS_LABEL("multi class label"), TWO_CLASS_LABEL(
            "two class label"), NUMERICAL_LABEL("numerical label"), ONE_CLASS_LABEL(
                    "one class label"), NO_LABEL("unlabeled"), UPDATABLE("updatable"), WEIGHTED_EXAMPLES("weighted examples"), MISSING_VALUES("missing values");


    private String description;

    private OperatorCapability(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}
