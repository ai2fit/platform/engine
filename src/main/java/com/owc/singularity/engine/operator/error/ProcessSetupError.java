/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator.error;


import java.util.List;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.ports.PortOwner;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * An error in the process setup that can be registered with the operator in which it appears using
 * {@link Operator#addError(ProcessSetupError)}. Beyond specifying its message, an error may also
 * provide a collections of quick fixes that solve this problem.
 * 
 * @author Simon Fischer
 * 
 */
public interface ProcessSetupError {

    /** Severity levels of ProcessSetupErrors. */
    public enum Severity {
        /**
         * This indicates that the corresponding message is just for information
         */
        INFORMATION,
        /**
         * This is an indicator of wrong experiment setup, but the process may run nevertheless.
         */
        WARNING,
        /** AbstractPipeline will definitely (well, say, most certainly) not run. */
        ERROR
    }

    /** Returns the human readable, formatted message. */
    public String getMessage();

    /**
     * Returns the owner of the port that should be displayed by the GUI to fix the error.
     */
    public PortOwner getOwner();

    /** If possible, return a list of fixes for the error. */
    public List<? extends QuickFix> getQuickFixes();

    /** Returns the severity of the error. */
    public Severity getSeverity();
}
