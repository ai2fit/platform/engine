/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.operator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.metadata.MDTransformer;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.operator.error.*;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.pipeline.*;
import com.owc.singularity.engine.pipeline.io.XMLExporter;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.tools.*;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.logging.LoggingHandler;
import com.owc.singularity.engine.tools.math.StringToMatrixConverter;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.editor.GUIProcessXMLFilter;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;
import com.owc.singularity.studio.gui.editor.quickfix.RelativizeRepositoryLocationQuickfix;
import com.owc.singularity.studio.gui.wizards.ConfigurationListener;
import com.owc.singularity.studio.gui.wizards.PreviewListener;


/**
 * This class is the central mess of singularity. It contains the settings, monitoring, logging and
 * algorithm implementation of each and every operator. It is at the same time the gui model, the
 * data structure for persistence, but also executing wit a state, that could change what is
 * persisted, etc.
 * <p>
 * Horrible mess that needs to be definitively changed and split into the various functions for gui
 * model, user settings and finally execution. Execution should not have any state in order to make
 * it parallizable.
 * 
 * 
 * @author Ralf Klinkenberg, Ingo Mierswa, Simon Fischer, Marius Helf, Sebastian Land
 */
public abstract class Operator extends AbstractObservable<Operator> implements ConfigurationListener, PreviewListener, LoggingHandler, ParameterHandler {

    private static final boolean CPU_TIME_SUPPORTED = ManagementFactory.getThreadMXBean().isThreadCpuTimeSupported();

    public static final OperatorVersion[] EMPTY_OPERATOR_VERSIONS_ARRAY = new OperatorVersion[0];

    private final Object parameterLock = new Object();
    /** Indicates if before / within / after this operator a breakpoint is set. */
    private boolean[] breakPoint = new boolean[BreakpointListener.BREAKPOINT_POS_NAME.length];

    /** Indicates if this operator is enabled. */
    private boolean enabled = true;

    /** Indicates if the tree node is expanded (only operator chains). */
    private boolean expanded = true;

    /** Name of the operators (for logging). */
    private String name;

    /** Number of times the operator was applied. */
    private AtomicInteger applyCount = new AtomicInteger();

    /** May differ from {@link #applyCount} if parallellized. */
    private int applyCountAtLastExecution = -1;

    /** System time when execution started. */
    private long startTime;

    /** Cpu time when execution started. */
    private long startCpuTime;

    /** System time when execution finished. */
    private long endTime;

    /** Cpu time when execution finished. */
    private long endCpuTime;

    /** System time when the current loop of execution started. */
    private long loopStartTime;

    /** Parameters for this Operator. */
    private Parameters parameters;

    /**
     * The values for this operator. The current value of a Value can be asked by the
     * ProcessLogOperator.
     */
    private final Map<String, Value> valueMap = new TreeMap<>();

    /**
     * Container for user data.
     */
    private Map<String, UserData<Object>> userData;

    /**
     * Lock object for user data container.
     */
    private final Object userDataLock = new Object();

    /**
     * The list which stores the errors of this operator (parameter not set, wrong children number,
     * wrong IO).
     */
    private final List<ProcessSetupError> errorList = Collections.synchronizedList(new LinkedList<>());

    /** Signals whether the output must be re-generated. */
    private boolean dirty = true;

    /**
     * Indicates whether {@link #propagateDirtyness()} was called after the last call to
     * {@link #makeDirty()}.
     */
    private boolean dirtynessWasPropagated = false;

    private transient Logger logger;

    private boolean isRunning = false;

    private boolean shouldStopStandaloneExecution = false;

    private OperatorVersion compatibilityLevel;

    /**
     * The {@link OperatorProgress} used to track progress during the execution of the operator.
     */
    private final OperatorProgress operatorProgress = new OperatorProgress(this);

    // -------------------- INITIALISATION --------------------

    /**
     * <p>
     * Creates an unnamed operator. Subclasses must pass the given description object to this
     * super-constructor (i.e. invoking super(OperatorDescription)). They might also add additional
     * values for pipeline logging.
     * </p>
     * <p>
     * NOTE: the preferred way for operator creation is using one of the factory methods of
     * {@link com.owc.singularity.engine.OperatorService}.
     * </p>
     */
    public Operator() {
        this.parameters = null;
        this.name = OperatorService.getOperatorDescription(this.getClass()).getShortName();

        inputPorts = createInputPorts(portOwner);
        outputPorts = createOutputPorts(portOwner);
        inputPorts.addObserver(delegatingPortObserver, false);
        outputPorts.addObserver(delegatingPortObserver, false);
        makeDirtyOnUpdate(inputPorts);

        addValue(new ValueDouble("applycount", "The number of times the operator was applied.", false) {

            @Override
            public double getDoubleValue() {
                return applyCountAtLastExecution;
            }
        });
        addValue(new ValueDouble("time", "The time elapsed since this operator started.", false) {

            @Override
            public double getDoubleValue() {
                return System.currentTimeMillis() - startTime;
            }
        });
        addValue(new ValueDouble("cpu-time", "The cpu time elapsed since this operator started.", false) {

            @Override
            public double getDoubleValue() {
                return getThreadCpuTime() - startCpuTime;
            }
        });
        addValue(new ValueDouble("execution-time", "The execution time of this operator.", false) {

            @Override
            public double getDoubleValue() {
                return endTime - startTime;
            }
        });
        addValue(new ValueDouble("cpu-execution-time", "The cpu execution time of this operator.", false) {

            @Override
            public double getDoubleValue() {
                return endCpuTime - startCpuTime;
            }
        });
        addValue(new ValueDouble("looptime", "The time elapsed since the current loop started.", false) {

            @Override
            public double getDoubleValue() {
                return System.currentTimeMillis() - loopStartTime;
            }
        });
    }

    /**
     * Observes the given {@link Observable} and sets this operators dirty flag to true upon any
     * update.
     */
    @SuppressWarnings("unchecked")
    protected void makeDirtyOnUpdate(Observable<?> observable) {
        observable.addObserver(dirtyObserver, false);
    }

    /** Returns the operator description of this operator. */
    public final OperatorDescription getOperatorDescription() {
        return OperatorService.getOperatorDescription(this.getClass());
    }

    /**
     * Returns the pipeline of this operator by asking the parent operator. If the operator itself
     * and all of its parents are not part of a pipeline, this method will return null. Please note
     * that some operators (e.g. ProcessLog) must be part of a pipeline in order to work properly.
     */
    @Override
    public AbstractPipeline getPipeline() {
        Operator parent = getParent();
        if (parent == null) {
            return null;
        } else {
            return parent.getPipeline();
        }
    }

    public Logger getLogger() {
        if (this.getPipeline() == null) {
            if (logger == null) {
                logger = LogService.getI18NLogger(getClass());
            }
            return logger;
        } else {
            return this.getPipeline().getLogger();
        }
    }

    @Override
    public void log(String message, int level) {
        getLogger().log(Level.forName("", level), message);
    }

    @Override
    public void log(String message) {
        getLogger().debug(getName() + ": " + message);
    }

    @Override
    public void logNote(String message) {
        getLogger().info(getName() + ": " + message);
    }

    @Override
    public void logWarning(String message) {
        getLogger().warn(getName() + ": " + message);
    }

    @Override
    public void logError(String message) {
        getLogger().error(getName() + ": " + message);
    }

    // --------------------------------------------------------------------------------

    /** Returns the name of the operator. */
    public final String getName() {
        return this.name;
    }

    /**
     * This method simply sets the name to the given one. Please note that it is not checked if the
     * name was already used in the pipeline. Please use the method {@link #rename(String)} for
     * usual renaming.
     */
    private void setName(String newName) {
        this.name = newName;
    }

    /**
     * This method unregisters the old name if this operator is already part of a
     * {@link AbstractPipeline}. Afterward, the new name is set and registered in the pipeline.
     * Please note that the name might be changed during registering in order to ensure that each
     * operator name is unique in its pipeline. The new name will be returned.
     */
    public final String rename(String newName) {
        AbstractPipeline pipeline = this.getPipeline();
        if (pipeline != null) {
            pipeline.unregisterName(this.name);
            String oldName = this.name;
            this.name = pipeline.registerName(newName, this);
            pipeline.notifyRenaming(oldName, this.name);
        } else {
            this.name = newName;
        }
        fireUpdate(this);
        return this.name;
    }

    /**
     * Looks up {@link UserData} entries. Returns null if key is unknown.
     *
     * @param key
     *            The key of the user data.
     * @return The user data.
     */
    public UserData<Object> getUserData(String key) {
        synchronized (this.userDataLock) {
            if (this.userData == null) {
                return null;
            } else {
                return this.userData.get(key);
            }
        }
    }

    /**
     * Stores arbitrary {@link UserData}.
     *
     * @param key
     *            The key to used to identify the data.
     * @param data
     *            The user data.
     */
    public void setUserData(String key, UserData<Object> data) {
        synchronized (this.userDataLock) {
            if (this.userData == null) {
                this.userData = new TreeMap<>();
            }
            this.userData.put(key, data);
        }
    }

    /**
     * The user specified comment for this operator.
     *
     * @deprecated use {@link GUIProcessXMLFilter#lookupOperatorAnnotations(Operator)} instead! This
     *             method will always return {@code null}.
     */
    @Deprecated
    public String getUserDescription() {
        getLogger().warn("Getting comments directly from operators has been deprecated. See JavaDoc for details.");
        return null;
    }


    public void removeAndKeepConnections(List<Operator> keepConnectionsTo) {

        getInputPorts().disconnectAllBut(keepConnectionsTo);
        getOutputPorts().disconnectAllBut(keepConnectionsTo);

        AbstractPipeline pipeline = this.getPipeline();
        if (enclosingExecutionUnit != null) {
            enclosingExecutionUnit.removeOperator(this);
        }
        if (pipeline != null) {
            unregisterOperator(pipeline);
        }
    }

    /** Removes this operator from its parent. */
    public void remove() {
        removeAndKeepConnections(null);
    }


    /**
     * Registers this operator in the given pipeline. Please note that this might change the name of
     * the operator.
     */
    protected void registerOperator(AbstractPipeline pipeline) {
        if (pipeline != null) {
            setName(pipeline.registerName(getName(), this));
        }
    }

    /** Deletes this operator removing it from the name map of the pipeline. */
    protected void unregisterOperator(AbstractPipeline pipeline) {
        pipeline.unregisterName(name);
    }

    /** Sets the activation mode. Inactive operators do not perform their action. */
    public void setEnabled(boolean enabled) {
        if (this.enabled != enabled) {
            this.enabled = enabled;
            fireUpdate(this);
        }
    }

    /** Sets the expansion mode which indicates if this operator is drawn expanded or not. */
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    /** Returns true if this operator should be painted expanded. */
    public boolean isExpanded() {
        return expanded;
    }

    /**
     * Returns true if this operator is enabled. No longer takes parent enabled status into account.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * This method must return true if the operator performs parallel execution of child operators
     * and false otherwise.
     */
    public boolean isParallel() {
        return false;
    }

    /** Returns the number of times this operator was already applied. */
    public int getApplyCount() {
        return applyCountAtLastExecution;
    }

    /**
     * Performs a deep clone on the most parts of this operator. The breakpointThread is empty (as
     * it is in initialization). The parent will be clone in the method of OperatorChain overwriting
     * this one. The in- and output containers are only cloned by reference copying. Use this method
     * only if you are sure what you are doing.
     *
     * @param name
     *            This parameter is no longer used.
     */
    public Operator cloneOperator(String name, boolean forParallelExecution) {
        Operator clone;
        try {
            clone = OperatorService.createOperator(this.getClass());
        } catch (Exception e) {
            getLogger().error("Can not create clone of operator '" + getName() + "': " + e, e);
            throw new RuntimeException("Can not create clone of operator '" + getName(), e);
        }
        clone.setName(getName());
        clone.breakPoint = new boolean[] { breakPoint[0], breakPoint[1] };
        clone.enabled = enabled;
        clone.expanded = expanded;

        // copy user data entries
        if (this.userData != null) {
            this.userData.forEach((k, data) -> {
                if (data != null)
                    clone.setUserData(k, data.copyUserData(clone));
            });
        }

        if (forParallelExecution) {
            clone.applyCount = this.applyCount;
        } else {
            clone.applyCount = new AtomicInteger();
        }
        clone.startTime = startTime;
        clone.startCpuTime = startCpuTime;
        clone.endTime = endTime;
        clone.endCpuTime = endCpuTime;
        clone.loopStartTime = loopStartTime;
        clone.getParameters().setParameters(this.getParameters());
        clone.compatibilityLevel = compatibilityLevel;
        clone.errorList.addAll(errorList);
        return clone;
    }

    // --------------------- Apply ---------------------

    /**
     * Performs the actual work of the operator and must be implemented by subclasses. Replaces the
     * old method <code>apply()</code>.
     */
    public void doWork() throws OperatorException {}

    // -------------------- Nesting --------------------
    /**
     * This method is invoked during the validation checks. It is invoked as a last check. The
     * default implementation does nothing. Subclasses might want to override this method to perform
     * some specialized checks, e.g. if an inner operator is of a specific class.
     */
    protected void performAdditionalChecks() {}

    /**
     * Will count an error if a non-optional property has no default value and is not defined by
     * user. Returns the total number of errors.
     */
    public int checkProperties() {
        if (!isEnabled()) {
            return 0;
        }
        int errorCount = 0;
        for (ParameterType type : getParameters().getParameterTypes()) {
            boolean optional = type.isOptional();
            boolean hidden = type.isHidden();
            String key = type.getKey();
            if (!optional && !hidden) {
                boolean parameterSet = getParameters().isSet(key);
                boolean isUndefined = false;
                try {
                    isUndefined = parameterSet && (type instanceof ParameterTypeAttribute || type instanceof CombinedParameterType)
                            && "".equals(getParameter(key));
                } catch (UndefinedParameterError e) {
                    // ignore
                }
                if (!parameterSet || isUndefined) {
                    addError(new UndefinedParameterSetupError(this, key));
                    errorCount++;
                }
            }
            if (type instanceof ParameterTypeRepositoryLocation) {
                String value = getParameters().getParameterOrNull(key);
                if (value == null || value.isEmpty() || ((ParameterTypeRepositoryLocation) type).isAllowAbsoluteEntries()
                        || !RepositoryPath.of(value).isAbsolute()) {
                    continue;
                }
                List<? extends QuickFix> quickfix = this.getPipeline().getPath() == null ? Collections.emptyList()
                        : Collections.singletonList(new RelativizeRepositoryLocationQuickfix(this, key, value));
                addError(new SimpleProcessSetupError(Severity.ERROR, getPortOwner(), quickfix, "absolute_repository_location", key.replace('_', ' '), value));
            } else if (type instanceof ParameterTypeDateFormat) {
                Locale locale = Locale.getDefault();
                try {
                    ParameterTypeDateFormat.createCheckedDateFormat(this, locale, true);
                } catch (UserError userError) {
                    // will not happen because of isSetup
                }

            } else if (!optional && !hidden && type instanceof ParameterTypeDate) {
                String value = getParameters().getParameterOrNull(key);
                if (value != null && !ParameterTypeDate.isValidDate(value)) {
                    addError(new SimpleProcessSetupError(Severity.WARNING, portOwner, "invalid_date_format", key.replace('_', ' '), value));
                }
            }
        }
        return errorCount;
    }

    /**
     * Will count the number of deprecated operators, i.e. the operators which
     * {@link OperatorDescription#getDeprecationDescription()} method does not return null. Returns
     * the total number of deprecations.
     */
    public int checkDeprecations() {
        if (getOperatorDescription().isDeprecated()) {
            String deprecationString = getOperatorDescription().getDeprecationDescription();
            addError(new SimpleProcessSetupError(Severity.WARNING, portOwner, "deprecation", getOperatorDescription().getName(), deprecationString));
            return 1;
        }
        return 0;
    }

    /**
     * Applies the operator. Don't override this method, but {@link #doWork()}
     */
    public final void execute() throws OperatorException {
        AbstractPipeline pipeline = this.getPipeline();
        Logger log = getLogger();
        if (pipeline == null) {
            log.debug("Pipeline of operator " + this.getName()
                    + " is not set, probably not registered! Trying to use this operator (should work in most cases anyway)...");
        }

        if (pipeline != null && pipeline.getExecutionMode() == ExecutionMode.ONLY_DIRTY && !isDirty()) {
            return;
        }

        if (getOperatorDescription().isDeprecated() && applyCount.get() == 0) {
            log.warn("Deprecation warning for " + getOperatorDescription().getName() + ": " + getOperatorDescription().getDeprecationDescription());
        }

        getOutputPorts().clear(Port.CLEAR_DATA);

        if (isEnabled()) {
            // check for stop
            checkForStop(pipeline);

            applyCountAtLastExecution = applyCount.incrementAndGet();
            startTime = loopStartTime = System.currentTimeMillis();
            startCpuTime = getThreadCpuTime();
            if (pipeline != null) {
                pipeline.setCurrentOperator(this);
                pipeline.getRootOperator().fireEventPipelineOperatorStarted(this);
            }

            if (breakPoint[BreakpointListener.BREAKPOINT_BEFORE]) {
                processBreakpoint(getInputPorts().createIOContainer(true).asList(), BreakpointListener.BREAKPOINT_BEFORE);
            }

            checkForStop(pipeline);
            log.debug("{}: Starting application {}", getName(), applyCountAtLastExecution);

            // reset progress listener to default value
            getProgress().setTotal(OperatorProgress.NO_PROGRESS);

            getOutputPorts().clear(Port.CLEAR_DATA);
            try {
                isRunning = true;

                // what is this needed for at all?
                // fireUpdate();

                doWork();
                log.debug("{}: Completed application {}", getName(), applyCountAtLastExecution);
            } catch (ProcessStoppedRuntimeException e) {
                // Convert unchecked exception to checked exception (unchecked exception might be
                // thrown from places where no checked exceptions are possible, e.g. thread pools).
                throw new ProcessStoppedException(this);
            } catch (RuntimeException e) {
                if (!(e instanceof OperatorRuntimeException)) {
                    OperatorException operatorException = new OperatorException("unknown_error", e, e.getMessage());
                    operatorException.setOperator(this);
                    throw operatorException;
                }
                OperatorException operatorException = ((OperatorRuntimeException) e).toOperatorException();
                if (operatorException.getOperator() == null) {
                    operatorException.setOperator(this);
                }
                throw operatorException;
            } catch (OperatorException e) {
                if (e.getOperator() == null) {
                    e.setOperator(this);
                }
                throw e;
            } finally {
                isRunning = false;
                endTime = System.currentTimeMillis();
                endCpuTime = getThreadCpuTime();

                // needs to be executed definitively
                if (pipeline != null) {
                    pipeline.getRootOperator().fireEventPipelineOperatorFinished(this);
                }
            }

            // logging?
            if (log.isEnabled(LogService.LEVEL_IO)) {
                StringBuilder builder = new StringBuilder(getName());
                builder.append(" returned with output:");
                formatIO(getOutputPorts(), builder);
                log.log(LogService.LEVEL_IO, builder.toString());
            }
            log.trace("{}: application {} took {} ms", getName(), applyCountAtLastExecution, endTime - startTime);


            setNotDirty();
            if (breakPoint[BreakpointListener.BREAKPOINT_AFTER]) {
                processBreakpoint(getOutputPorts().createIOContainer(true).asList(), BreakpointListener.BREAKPOINT_AFTER);
            }
            checkForStop();
        }
    }

    private void formatIO(Ports<? extends Port> ports, StringBuilder builder) {
        for (Port port : ports.getAllPorts()) {
            builder.append("\n  ");
            builder.append(port.getName());
            IOObject data = port.getDataOrNull();
            builder.append(data == null ? "-/-" : data.toString());
        }
    }

    /**
     * This method should be called within long-running loops of an operator to check if the user
     * has canceled the execution in the meanwhile. This then will throw a
     * {@link ProcessStoppedException} to cancel the execution.
     */
    public final void checkForStop() throws ProcessStoppedException {
        if (getParent() != null) {
            checkForStop(getParent().getPipeline());
        } else {
            checkForStop(this.getPipeline());
        }
    }

    private void checkForStop(AbstractPipeline pipeline) throws ProcessStoppedException {
        if (pipeline != null && pipeline.shouldStop()) {
            stop();
            return;
        }
        if (pipeline != null && pipeline.shouldPause()) {
            getLogger().info("Pipeline interrupted in " + getName());
            processBreakpoint(null, BreakpointListener.BREAKPOINT_AFTER);
        }
        if (pipeline == null && shouldStopStandaloneExecution) {
            stop();
        }
    }

    /**
     * This method will cause the execution of this operator to stop at the next call of
     * checkForStop() in the executing thread. When this will be depended on the operator. Some
     * operations like huge matrix inversions cannot be aborted prematurely at all. A
     * ProcessStoppedException will be thrown in case of stopping, so prepare to catch it when
     * executing the operator. Please keep in mind, that this method will have an effect only if the
     * operator is executed without a pipeline context directly from the API.
     */
    public final void shouldStopStandaloneExecution() throws ProcessStoppedException {
        if (this.getPipeline() == null) {
            this.shouldStopStandaloneExecution = true;
        }
    }

    private void stop() throws ProcessStoppedException {
        throw new ProcessStoppedException(this);
    }

    private void processBreakpoint(List<IOObject> container, int breakpointType) throws ProcessStoppedException {
        AbstractPipeline pipeline = this.getPipeline();
        if (pipeline.shouldStop()) {
            stop();
        }
        pipeline.pause(this, container, breakpointType);
    }


    /** Called when the pipeline starts. Resets all counters. */
    public void pipelineStarts() throws OperatorException {
        applyCount.set(0);
        applyCountAtLastExecution = 0;
    }

    /**
     * Called at the end of the pipeline. The default implementation does nothing.
     */
    public void pipelineFinished() throws OperatorException {}

    /**
     * Sets or clears a breakpoint at the given position.
     *
     * @param position
     *            One out of BREAKPOINT_BEFORE and BREAKPOINT_AFTER
     */
    public void setBreakpoint(int position, boolean on) {
        breakPoint[position] = on;
        fireUpdate(this);
    }

    /**
     * Returns true iff this operator has a breakpoint at any possible position.
     */
    public boolean hasBreakpoint() {
        return hasBreakpoint(BreakpointListener.BREAKPOINT_BEFORE) || hasBreakpoint(BreakpointListener.BREAKPOINT_AFTER);
    }

    /**
     * Returns true iff a breakpoint is set at the given position
     *
     * @param position
     *            One out of BREAKPOINT_BEFORE and BREAKPOINT_AFTER
     */
    public boolean hasBreakpoint(int position) {
        return breakPoint[position];
    }

    /**
     * Should be called if this operator performs a loop (for the loop time resetting used for Value
     * creation used by DataTables). This method also invokes {@link #checkForStop()}.
     */
    public void inApplyLoop() throws ProcessStoppedException {
        loopStartTime = System.currentTimeMillis();
        checkForStop();
    }

    /** Adds an implementation of Value. */
    public void addValue(Value value) {
        valueMap.put(value.getKey(), value);
    }

    /** Returns the value of the Value with the given key. */
    public final Value getValue(String key) {
        return valueMap.get(key);
    }

    /** Returns all Values sorted by key. */
    public Collection<Value> getValues() {
        return valueMap.values();
    }

    // -------------------- parameter wrapper --------------------

    /**
     * Returns a collection of all parameters of this operator. If the parameters object has not
     * been created yet, it will now be created. Creation had to be moved out of constructor for
     * metadata handling in subclasses needing a port.
     */
    @Override
    public Parameters getParameters() {
        synchronized (parameterLock) {
            if (parameters == null) {
                // if not loaded already: do now
                parameters = new DynamicParameters(this::getParameterTypes, this::isParameterTypeUpdateNecessary);
                parameters.addObserver(delegatingParameterObserver, false);

                makeDirtyOnUpdate(parameters);
            }
            return parameters;
        }
    }

    public boolean hasParameters() {
        synchronized (parameterLock) {
            return parameters != null;
        }
    }

    /**
     * Returns the first primary {@link ParameterType} of this operator that is not hidden if it
     * exists. May return {@code null}.
     *
     * @return the primary parameter or {@code null}
     */
    public ParameterType getPrimaryParameter() {
        for (ParameterType param : getParameters().getParameterTypes()) {
            if (param.isPrimary() && !param.isHidden()) {
                return param;
            }
        }
        return null;
    }

    @Override
    public ParameterHandler getParameterHandler() {
        return this;
    }

    /**
     * Sets the given single parameter to the Parameters object of this operator. For parameter list
     * the method {@link #setListParameter(String, List)} should be used.
     */
    @Override
    public void setParameter(String key, String value) {
        getParameters().setRawParameter(key, value);
    }

    /**
     * Sets the given parameter list to the Parameters object of this operator. For single
     * parameters the method {@link #setParameter(String, String)} should be used.
     */
    @Override
    public void setListParameter(String key, List<String[]> list) {
        getParameters().setRawParameter(key, ParameterTypeList.transformList2String(list));
    }

    /**
     * Returns a single parameter retrieved from the {@link Parameters} of this Operator.
     */
    @Override
    public String getParameter(String key) throws UndefinedParameterError {
        try {
            return replaceVariables(getParameters().getRawParameter(key), key);
        } catch (UndefinedParameterError e) {
            e.setOperator(this);
            throw e;
        }
    }

    /**
     * Returns true iff the parameter with the given name is set. If no parameters object has been
     * created yet, false is returned. This can be used to break initialization loops.
     */
    @Override
    public boolean isParameterSet(String key) {
        return getParameters().isSet(key);
    }

    /** Returns a single named parameter and casts it to String. */
    @Override
    public String getParameterAsString(String key) throws UndefinedParameterError {
        return getParameter(key);
    }

    /** Returns a single named parameter and casts it to String. */
    @Override
    public String getParameterAsString(String key, boolean resolveVariables) throws UndefinedParameterError {
        if (resolveVariables)
            return getParameter(key);
        return parameters.getRawParameter(key);
    }

    /** Returns a single named parameter and casts it to char. */
    @Override
    public char getParameterAsChar(String key) throws UndefinedParameterError {
        String parameterValue = getParameter(key);
        if (!parameterValue.isEmpty()) {
            return parameterValue.charAt(0);
        }
        return 0;
    }

    /** Returns a single named parameter and casts it to int. */
    @Override
    public int getParameterAsInt(String key) throws UndefinedParameterError {
        return getParameterAsIntNumber(key, Integer::valueOf, Integer::intValue);
    }

    /** Returns a single named parameter and casts it to long. */
    @Override
    public long getParameterAsLong(String key) throws UndefinedParameterError {
        return getParameterAsIntNumber(key, Long::valueOf, Long::valueOf);
    }

    private <N extends Number> N getParameterAsIntNumber(String key, Function<String, N> transformer, Function<Integer, N> caster)
            throws UndefinedParameterError {
        ParameterType type = this.getParameters().getParameterType(key);
        String value = getParameter(key);
        if (type instanceof ParameterTypeCategory) {
            try {
                return transformer.apply(value);
            } catch (NumberFormatException e) {
                ParameterTypeCategory categoryType = (ParameterTypeCategory) type;
                return caster.apply(categoryType.getIndex(value));
            }
        }
        try {
            return transformer.apply(value);
        } catch (NumberFormatException e) {
            throw new UndefinedParameterError(key, this, "Expected long but found '" + value + "'.");
        }
    }

    /** Returns a single named parameter and casts it to double. */
    @Override
    public double getParameterAsDouble(String key) throws UndefinedParameterError {
        String value = getParameter(key);
        if (value == null) {
            throw new UndefinedParameterError(key, this);
        }
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new UndefinedParameterError(key, this, "Expected real number but found '" + value + "'.");
        }
    }

    /**
     * Returns a single named parameter and casts it to boolean. This method never throws an
     * exception since there are no non-optional boolean parameters.
     */
    @Override
    public boolean getParameterAsBoolean(String key) {
        try {
            return Boolean.parseBoolean(getParameter(key));
        } catch (UndefinedParameterError ignored) {
        }
        return false; // cannot happen
    }

    /**
     * Returns a single named parameter and casts it to List. The list returned by this method
     * contains the user defined key-value pairs. Each element is a String array of length 2. The
     * first element is the key, the second the parameter value. The caller have to perform the
     * casts to the correct types himself.
     */
    @Override
    public List<String[]> getParameterList(String key) throws UndefinedParameterError {
        String value = getParameter(key);
        if (value == null) {
            throw new UndefinedParameterError(key);
        }
        return ParameterTypeList.transformString2ListWithParameterError(value, key);
    }

    /**
     * Returns a Pair of Strings, the Strings are in the order of type definition of the subtypes.
     */
    @Override
    public String[] getParameterTupel(String key) throws UndefinedParameterError {
        return ParameterTypeTupel.transformString2Tupel(getParameter(key));
    }

    /** Returns a single named parameter and casts it to Color. */
    @Override
    public java.awt.Color getParameterAsColor(String key) throws UndefinedParameterError {
        return com.owc.singularity.engine.pipeline.parameter.ParameterTypeColor.string2Color(getParameter(key));
    }

    /**
     * Returns a single named parameter and tries to handle it as URL. If this works, this method
     * creates an input stream from this URL and delivers it. If not, this method tries to cast the
     * parameter value to a file. This file is already resolved against the pipeline definition
     * file. If the parameter name defines a non-optional parameter which is not set and has no
     * default value, a UndefinedParameterError will be thrown. If the parameter is optional and was
     * not set this method returns null.
     *
     * @throws IOException
     * @throws UserError
     */
    @Override
    public InputStream getParameterAsInputStream(String key) throws IOException, UserError {
        String urlString = getParameter(key);
        if (urlString == null) {
            return null;
        }

        try {
            URL url = new URL(urlString);
            return WebServiceTools.openStreamFromURL(url);
        } catch (MalformedURLException e) {
            // URL did not work? Try as file...
            File file = getParameterAsFile(key);
            if (file != null) {
                return new FileInputStream(file);
            } else {
                return null;
            }
        }
    }

    /**
     * Returns a single named parameter and casts it to File. This file is already resolved against
     * the pipeline definition file but missing directories will not be created. If the parameter
     * name defines a non-optional parameter which is not set and has no default value, a
     * UndefinedParameterError will be thrown. If the parameter is optional and was not set this
     * method returns null.
     */
    @Override
    public java.io.File getParameterAsFile(String key) throws UserError {
        return getParameterAsFile(key, false);
    }

    /**
     * Returns the parameter values as file. Value should contain an absolute path or it is resolved
     * against the working directory of the jvm.
     *
     * @throws UserError
     *             if there's no defined parameter value or the directory does not exist.
     */
    @Override
    public File getParameterAsFile(String key, boolean createMissingDirectories) throws UserError {
        String fileName = getParameter(key);
        if (fileName == null) {
            return null;
        }
        File result = new File(fileName);

        if (createMissingDirectories) {
            File parent = result.getParentFile();
            if (parent != null && !parent.exists() && !parent.mkdirs()) {
                throw new UserError(null, "io.dir_creation_fail", parent.getAbsolutePath());
            }
        }
        return result;
    }

    /**
     * This method returns the parameter identified by key as a RepositoryPath. For this the string
     * is resolved against this operators pipeline location in the Repository.
     */
    public RepositoryPath getParameterAsRepositoryPath(String key) throws UserError {
        return ParameterTypeRepositoryLocation.getPath(getParameter(key), this);
    }

    /** Returns a single named parameter and casts it to a double matrix. */
    @Override
    public double[][] getParameterAsMatrix(String key) throws UndefinedParameterError {
        String matrixLine = getParameter(key);
        try {
            return StringToMatrixConverter.parseMatlabString(matrixLine);
        } catch (OperatorException e) {
            throw new UndefinedParameterError(e.getMessage(), this);
        }
    }

    /**
     * Replaces existing variables in the given value string by the variable values defined for the
     * pipeline. Please note that this is basically only supported for string type parameter values.
     * <p>
     * This method replaces the predefined variables like %{pipeline_name}, %{pipeline_file}, and
     * %{pipeline_path} and tries to replace variables surrounded by &quot;%{&quot; and
     * &quot;}&quot; with help of the {@link VariableHandler} of the {@link AbstractPipeline}.
     *
     * @param parameterKey
     *            the key of the parameter type the variable is looked up for. If the referenced
     *            parameterType is {@code null} the value-string will be directly passed to the
     *            VariableHandler
     *
     * @throws UndefinedParameterError
     *             will be thrown if value contains an undefined variable
     */
    private String replaceVariables(String value, String parameterKey) throws UndefinedParameterError {
        if (value == null || this.getPipeline() == null) {
            return value;
        }
        String returnValue = value;
        try {
            ParameterType parameterType = getParameters().getParameterType(parameterKey);
            if (parameterType == null) {
                returnValue = this.getPipeline().getVariableHandler().resolveVariables(parameterKey, value);
                returnValue = this.getPipeline().getVariableHandler().resolvePredefinedVariables(returnValue, this);
            } else {
                returnValue = parameterType.substituteVariables(value, this.getPipeline().getVariableHandler());
                returnValue = parameterType.substitutePredefinedVariables(returnValue, this);
            }
        } catch (UndefinedVariableError e) {
            // throw the error so that the user recognizes that the variable was undefined
            e.setOperator(this);
            throw e;
        } catch (RuntimeException e) {
            LogService.getRoot().warn("Error resolving Variable values", e);
            throw e;
        }
        return returnValue;
    }

    /**
     * Returns a list of <tt>ParameterTypes</tt> describing the parameters of this operator. The
     * default implementation returns an empty list if no input objects can be retained and special
     * parameters for those input objects which can be prevented from being consumed.
     * <p>
     * ATTENTION! This will create new parameterTypes. For calling already existing parameter types
     * use getParameters().getParameterTypes();
     */
    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = new LinkedList<>();
        return types;
    }

    /**
     * This returns whether the types have changed depending on the parameters value.
     * 
     * @param parameters
     *            current values
     * @return true if types need to be updated
     */
    protected boolean isParameterTypeUpdateNecessary(Parameters parameters) {
        return false;
    }

    /**
     * Returns the parameter type with the given name. Will return null if this operator does not
     * have a parameter with the given name.
     */
    public ParameterType getParameterType(String name) {
        for (ParameterType current : getParameters().getParameterTypes()) {
            if (current.getKey().equals(name)) {
                return current;
            }
        }
        return null;
    }

    /**
     * This returns this operator with all its parameter settings as a {@link Document}
     */
    public Document getDOMRepresentation() throws IOException {
        return new XMLExporter().exportProcess(this, false);
    }

    /** Same as getXML(hideDefault, false). */
    public String getXML(boolean hideDefault) {
        return getXML(hideDefault, false);
    }

    /**
     * Returns the XML representation of this operator.
     *
     * @param hideDefault
     *            if true, default parameters will be ignored when creating the xml representation
     * @param onlyCoreElements
     *            if true, GUI and other additional information will be ignored.
     */
    public String getXML(boolean hideDefault, boolean onlyCoreElements) {
        try {
            return XMLTools.toString(new XMLExporter(onlyCoreElements).exportProcess(this, hideDefault));
        } catch (Exception e) {
            getLogger().warn(LogService.MARKER_DEVELOPMENT, "com.owc.singularity.engine.operator.Operator.generating_xml_process_error", e.getMessage(), e);
            return e.toString();
        }
    }

    private void checkOperator() {
        if (!isEnabled()) {
            return;
        }
        checkProperties();
        checkDeprecations();
        performAdditionalChecks();
    }

    /**
     * Clears all errors, checks the operator and its children and propagates meta data, propgatates
     * dirtyness and sorts execution order.
     */
    public void checkAll() {
        getRoot().clear(Port.CLEAR_METADATA | Port.CLEAR_ALL_ERRORS);
        if (isEnabled()) {
            checkOperator();
            getRoot().transformMetaData();
            propagateDirtyness();
        }
    }

    /** As check all, but does not check the meta data for performance reasons. */
    public void checkAllExcludingMetaData() {
        getRoot().clear(Port.CLEAR_METADATA | Port.CLEAR_SIMPLE_ERRORS);
        if (isEnabled()) {
            checkOperator();
            propagateDirtyness();
        }
    }


    public void addError(ProcessSetupError error) {
        errorList.add(error);
    }


    /**
     * Returns a List of Strings containing error messages.
     *
     */
    public List<ProcessSetupError> getErrorList() {
        List<ProcessSetupError> errors = new LinkedList<>();
        collectErrors(errors);
        errors.sort(Comparator.comparing(ProcessSetupError::getSeverity).reversed());
        return errors;
    }

    protected void collectErrors(List<ProcessSetupError> errors) {
        errors.addAll(errorList);
        for (Port port : getInputPorts().getAllPorts()) {
            Collection<MetaDataError> portErrors = port.getErrors();
            if (portErrors != null) {
                try {
                    errors.addAll(portErrors);
                } catch (NullPointerException e) {
                    // TODO: Can it be avoided to have NullPointerExceptions here when an error is
                    // created just after the operator has been inserted?
                }
            }
        }
        for (Port port : getOutputPorts().getAllPorts()) {
            Collection<MetaDataError> portErrors = port.getErrors();
            if (portErrors != null) {
                errors.addAll(port.getErrors());
            }
        }
    }

    /** Returns the system time when the operator was started. */
    public long getStartTime() {
        return startTime;
    }

    /** Returns the name. */
    @Override
    public String toString() {
        String type;
        if (getOperatorDescription() != null) {
            type = this.getName();
        } else {
            type = getClass().getName();
        }
        return (breakPoint[0] || breakPoint[1] ? "* " : "") + name + " (" + type + ")";
    }

    /** Returns this operator's name and class. */
    public String createProcessTree(int indent) {
        return createProcessTree(indent, "", "", null, null);
    }

    /** Returns this operator's name and class. */
    protected String createProcessTree(int indent, String selfPrefix, String childPrefix, Operator markOperator, String mark) {
        return createProcessTreeEntry(indent, selfPrefix, markOperator, mark);
    }

    /** Returns this operator's name and class in a list. */
    protected List<String> createProcessTreeList(int indent, String selfPrefix, String childPrefix, Operator markOperator, String mark) {
        List<String> processTreeList = new LinkedList<>();
        processTreeList.add(createProcessTreeEntry(indent, selfPrefix, markOperator, mark));
        return processTreeList;
    }

    private String createProcessTreeEntry(int indent, String selfPrefix, Operator markOperator, String mark) {
        if (markOperator != null && getName().equals(markOperator.getName())) {
            return Tools.indent(indent - mark.length()) + mark + selfPrefix + getName() + "[" + applyCount + "]" + " (" + this.getName() + ")";
        } else {
            return Tools.indent(indent) + selfPrefix + getName() + "[" + applyCount + "]" + " (" + this.getName() + ")";
        }
    }

    public boolean isDebugMode() {
        String debugProperty = PropertyService.getParameterValue(EngineProperties.GENERAL_DEBUGMODE);
        return Tools.booleanValue(debugProperty, false);
    }

    private final PortOwner portOwner = new PortOwner() {

        @Override
        public OperatorChain getPortHandler() {
            return getParent();
        }

        @Override
        public String getName() {
            return Operator.this.getName();
        }

        @Override
        public Operator getOperator() {
            return Operator.this;
        }

        @Override
        public ExecutionUnit getConnectionContext() {
            return getExecutionUnit();
        }
    };

    private final InputPorts inputPorts;
    private final OutputPorts outputPorts;
    private final MDTransformer transformer = new MDTransformer(this);
    private final Observer<Port> delegatingPortObserver = new DelegatingObserver<>(this, this);
    private final Observer<String> delegatingParameterObserver = new DelegatingObserver<>(this, this);
    /** Sets the dirty flag on any update. */
    @SuppressWarnings("rawtypes")
    private final Observer dirtyObserver = (observable, arg) -> makeDirty();
    private ExecutionUnit enclosingExecutionUnit;

    /**
     * Returns the operator containing the enclosing pipeline or null if this is the root operator.
     */
    public final OperatorChain getParent() {
        if (enclosingExecutionUnit != null) {
            return enclosingExecutionUnit.getEnclosingOperator();
        } else {
            return null;
        }
    }

    /**
     * This method returns the {@link InputPorts} object that gives access to all defined
     * {@link InputPort}s of this operator. This object can be used to create a new
     * {@link InputPort} for an operator using one of the
     * {@link InputPorts#createPort(String, double)} methods.
     */
    public final InputPorts getInputPorts() {
        return inputPorts;
    }

    /**
     * This method returns the {@link OutputPorts} object that gives access to all defined
     * {@link OutputPort}s of this operator. This object can be used to create a new
     * {@link OutputPort} for an operator using one of the
     * {@link OutputPorts#createPort(String, double)} methods.
     */
    public final OutputPorts getOutputPorts() {
        return outputPorts;
    }

    /**
     * This method returns an {@link InputPorts} object for port initialization. Useful for adding
     * an arbitrary implementation (e.g. changing port creation & (dis)connection behavior,
     * optionally by customized {@link InputPort} instances) by overriding this method.
     *
     * @param portOwner
     *            The owner of the ports.
     * @return The {@link InputPorts} instance, never {@code null}.
     */
    protected InputPorts createInputPorts(PortOwner portOwner) {
        return new SortingInputPorts(portOwner);
    }

    /**
     * This method returns an {@link OutputPorts} object for port initialization. Useful for adding
     * an arbitrary implementation (e.g. changing port creation & (dis)connection behavior,
     * optionally by customized {@link OutputPort} instances) by overriding this method.
     *
     * @param portOwner
     *            The owner of the ports.
     * @return The {@link OutputPorts} instance, never {@code null}.
     */
    protected OutputPorts createOutputPorts(PortOwner portOwner) {
        return new SortingOutputPorts(portOwner);
    }

    /**
     * This method returns the {@link MDTransformer} object of this operator. This object will
     * pipeline all metadata of all ports of this operator according to the rules registered to it.
     * This method can be used to get the transformer and register new Rules for
     * MetaDataTransformation for the ports using the
     * {@link MDTransformer#addRule(com.owc.singularity.engine.metadata.rules.MDTransformationRule)}
     * method or one of it's more specialized sisters.
     * <p>
     * 
     * This method should not be used outside {@link AbstractOperator} anymore. It should be
     * replaced by inheriting from {@link AbstractOperator} and using the defineXXX methods. They
     * will automatically register transformers as needed.
     */
    @Deprecated
    public final MDTransformer getTransformer() {
        return transformer;
    }

    /** Returns the ExecutionUnit that contains this operator. */
    public final ExecutionUnit getExecutionUnit() {
        return enclosingExecutionUnit;
    }

    final protected void setEnclosingProcess(ExecutionUnit parent) {
        if (parent != null && this.enclosingExecutionUnit != null) {
            throw new IllegalStateException("Parent already set.");
        }
        this.enclosingExecutionUnit = parent;
    }

    /** Clears output and input ports. */
    public void clear(int clearFlags) {
        if ((clearFlags & Port.CLEAR_SIMPLE_ERRORS) > 0) {
            errorList.clear();
        }
        getInputPorts().clear(clearFlags);
        getOutputPorts().clear(clearFlags);
    }


    /**
     * If this method is called for perform the meta data transformation on this operator. It needs
     * the meta data on the input Ports to be already calculated.
     */
    public void transformMetaData() {
        clear(Port.CLEAR_META_DATA_ERRORS);
        if (!isEnabled()) {
            return;
        }
        getInputPorts().checkPreconditions(new OperationDescriptionContext(this));
        getTransformer().transformMetaData();
    }

    /**
     * By default, all ports will be auto-connected by
     * {@link ExecutionUnit#autoWire(boolean, boolean)}. Optional outputs are computed iff the
     * corresponding port is connected. For backward compatibility, operators can check if we should
     * auto-connect a port by overriding this method (e.g. by checking a deprecated parameter).
     * TODO: Remove in later versions
     */
    public boolean shouldAutoConnect(OutputPort outputPort) {
        return true;
    }

    /**
     * @see #shouldAutoConnect(OutputPort)
     */
    public boolean shouldAutoConnect(InputPort inputPort) {
        return true;
    }

    /**
     * Returns the first ancestor that does not have a parent. Note that this is not necessarily a
     * AbstractRootOperator!
     */
    public Operator getRoot() {
        OperatorChain parent = getParent();
        if (parent == null) {
            return this;
        } else {
            return parent.getRoot();
        }
    }

    /**
     * This method is called when the operator with "oldName" is renamed to "newName". It provides a
     * hook, when this operator's parameter are depending on operator names. The
     * {@link ParameterTypeInnerOperator} is an example for such a dependency. This way it is
     * possible to change the parameter's according to the renaming.
     */
    public void notifyRenaming(String oldName, String newName) {
        getParameters().notifyRenaming(oldName, newName);
    }

    /**
     * This method is called when the operator given by {@code oldName} (and {@code oldOp} if it is
     * not {@code null}) was replaced with the operator described by {@code newName} and
     * {@code newOp}. This will inform the {@link Parameters} of the replacing.
     *
     * @param oldName
     *            the name of the old operator
     * @param oldOp
     *            the old operator; can be {@code null}
     * @param newName
     *            the name of the new operator
     * @param newOp
     *            the new operator; must not be {@code null}
     * @see Parameters#notifyReplacing(String, Operator, String, Operator)
     * @since 9.3
     */
    public void notifyReplacing(String oldName, Operator oldOp, String newName, Operator newOp) {
        getParameters().notifyReplacing(oldName, oldOp, newName, newOp);
    }

    @Override
    protected void fireUpdate(Operator operator) {
        super.fireUpdate(operator);
        AbstractPipeline pipeline = this.getPipeline();
        if (pipeline != null) {
            pipeline.fireOperatorChanged(this);
        }
    }

    /**
     * This method will flag this operator's results as dirty. Currently unused feature.
     */
    public void makeDirty() {
        if (!dirty) {
            this.dirty = true;
            AbstractPipeline pipeline = this.getPipeline();
            if (pipeline != null) {
                if (pipeline.getDebugMode() == DebugMode.COLLECT_METADATA_AFTER_EXECUTION) {
                    clear(Port.CLEAR_REAL_METADATA);
                }
            }
            dirtynessWasPropagated = false;

            // This kills the awt event thread. Needs to check whether we actually need this
            // fireUpdate();
        }
    }

    protected void propagateDirtyness() {
        if (isDirty() && !dirtynessWasPropagated) {
            dirtynessWasPropagated = true;
            for (OutputPort port : getOutputPorts().getAllPorts()) {
                if (port.isConnected()) {
                    Operator operator = port.getDestination().getPorts().getOwner().getOperator();
                    operator.makeDirty();
                    operator.propagateDirtyness();
                }
            }
        }
    }

    private void setNotDirty() {
        this.dirty = false;

        // This kills the awt event thread. Needs to check whether we actually need this
        // fireUpdate();
    }

    /**
     * Returns whether the results on the output ports of this operator are dirty. This is the case
     * when the results depend on old parameter settings or old data from an input port, whose
     * connected output port is flaged as dirty.
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * This returns the number of breakpoints: 0, 1 or 2.
     */
    public int getNumberOfBreakpoints() {
        int num = 0;
        for (boolean bp : breakPoint) {
            if (bp) {
                num++;
            }
        }
        return num;
    }

    /**
     * Returns true if this operator contains at least one {@link InputPort} which accepts an input
     * of the given class (loose checking).
     */
    public boolean acceptsInput(Class<? extends IOObject> inputClass) {
        MetaData metaData = new MetaData(inputClass);
        for (InputPort inPort : getInputPorts().getAllPorts()) {
            if (inPort.isInputCompatible(metaData)) {
                return true;
            }
        }
        return false;
    }


    /**
     * This returns the {@link PortOwner} of this operator. See {@link PortOwner} for more details.
     */
    public PortOwner getPortOwner() {
        return portOwner;
    }

    /**
     * This method is called before auto-wiring an operator. Operators can reorder outputs in order
     * to influence how subsequent operators are wired. This is only necessary for legacy operators
     * like IOConsumer or IOSelector. Don't override this method for new operators.
     */
    protected LinkedList<OutputPort> preAutoWire(LinkedList<OutputPort> readyOutputs) throws OperatorException {
        return readyOutputs;
    }

    /**
     * Releases of any resources held by this operator due since its execution. In particular,
     * removes all hard references to IOObjects stored at the ports.
     */
    public void freeMemory() {
        getInputPorts().freeMemory();
        getOutputPorts().freeMemory();
    }

    /**
     * Looks up an operator with the given name in the containing pipeline.
     *
     * TODO: This method is slow since it scans operators several times. Simply looking at the
     * {@link AbstractPipeline#operatorNameMap} does not work for parallel execution, however.
     */
    protected Operator lookupOperator(String operatorName) {
        if (getName().equals(operatorName)) {
            return this;
        }
        ExecutionUnit executionUnit = getExecutionUnit();
        if (executionUnit == null) {
            return null;
        }
        for (Operator sibling : executionUnit.getAllInnerOperators()) {
            if (sibling.getName().equals(operatorName)) {
                return sibling;
            }
        }

        OperatorChain parent = getParent();
        if (parent != null) {
            return parent.lookupOperator(operatorName);
        } else {
            return null;
        }
    }

    /**
     * The {@link OperatorProgress} should be initialized when starting the operator execution by
     * setting the total amount of progress (which is {@link OperatorProgress#NO_PROGRESS} by
     * default) by calling {@link OperatorProgress#setTotal(int)}. Afterward the progress can be
     * reported by calling {@link OperatorProgress#setCompleted(int)}. The progress will be reset
     * before the operator is being executed.
     *
     * @return the {@link OperatorProgress} to report progress during operator execution.
     */
    public final OperatorProgress getProgress() {
        return operatorProgress;
    }

    /**
     * Returns if this operators {@link #execute()} method is currently executed.
     */
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Returns if this operator should currently show progress animation. This method can be
     * overridden to provide unique animation display criteria. By default, it checks if the
     * operator is running. Please note that
     * {@link com.owc.singularity.studio.gui.animation.OperatorAnimationProcessListener#pipelineOperatorFinished}
     * also depends on this method.
     */
    public boolean isAnimating() {
        return isRunning();
    }

    /**
     * @see OperatorVersion
     */
    public void setCompatibilityLevel(OperatorVersion compatibilityLevel) {
        this.compatibilityLevel = compatibilityLevel;
        fireUpdate();
    }

    /**
     * @see OperatorVersion
     */
    public OperatorVersion getCompatibilityLevel() {
        if (compatibilityLevel == null) {
            compatibilityLevel = OperatorVersion.getLatestVersion(this.getOperatorDescription());
        }
        return compatibilityLevel;
    }

    /**
     * Returns the versions of an operator <strong>after which its behavior incompatibly
     * changed</strong> in random order. Only the versions after which the new behavior was
     * introduced are returned. See comment of {@link OperatorVersion} for details.
     */
    public OperatorVersion[] getIncompatibleVersionChanges() {
        return EMPTY_OPERATOR_VERSIONS_ARRAY;
    }

    /**
     * Visitor pattern visiting all operators in sub-pipelines and the operator itself.
     *
     * @return
     */
    public TreeVisitResult walk(TreeVisitor<ExecutionUnit, Operator> visitor) {
        return visitor.visitLeaf(this);
    }

    /** Returns the current CPU time if supported, or the current system time otherwise. */
    private static long getThreadCpuTime() {
        return CPU_TIME_SUPPORTED ? ManagementFactory.getThreadMXBean().getThreadCpuTime(Thread.currentThread().getId())
                : System.currentTimeMillis() * 1000000L;
    }


}
