package com.owc.singularity.engine.operator;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.metadata.SimpleMetaDataError;
import com.owc.singularity.engine.operator.error.ProcessSetupError;
import com.owc.singularity.engine.ports.InputPort;

public final class OperationDescriptionInputHandler implements Serializable {

    private static final long serialVersionUID = -4203970592463481769L;

    private transient Operator operator;

    private LinkedHashMap<String, MetaData> inputDescriptionMap = new LinkedHashMap<>();

    public OperationDescriptionInputHandler(Operator operator) {
        this.operator = operator;
        operator.getInputPorts().getAllPorts().forEach(port -> inputDescriptionMap.put(port.getName(), port.getMetaData()));
    }

    public MetaData getMetaDataOrNull(String portName) {
        return inputDescriptionMap.get(portName);
    }

    @SuppressWarnings("unchecked")
    public <T extends MetaData> T getMetaData(String portName, Class<T> objectClass) {
        MetaData metaData = inputDescriptionMap.get(portName);
        if (metaData != null) {
            if (!objectClass.isAssignableFrom(metaData.getClass())) {
                addMetaDataError(portName, port -> new SimpleMetaDataError(ProcessSetupError.Severity.ERROR, port, "incompatible_input_object_classes",
                        port.getName(), objectClass.getSimpleName().replace("MetaData", ""), metaData.getObjectClass().getSimpleName()));
                return null;
            }
            return (T) metaData;
        }
        addMetaDataError(portName, port -> new SimpleMetaDataError(ProcessSetupError.Severity.WARNING, port, "null_input", port.getName(),
                objectClass.getSimpleName().replace("MetaData", "")));
        return null;
    }

    public List<MetaData> getAllMetaData(String portExtenderName) {
        return inputDescriptionMap.keySet()
                .stream()
                .filter(name -> name.startsWith(portExtenderName) && name.substring(portExtenderName.length()).matches("\\s[0-9]+"))
                .map(inputDescriptionMap::get)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <T extends MetaData> List<T> getAllMetaData(String portExtenderName, Class<T> objectClass) {
        return inputDescriptionMap.keySet()
                .stream()
                .filter(name -> name.startsWith(portExtenderName) && name.substring(portExtenderName.length()).matches("\\s[0-9]+"))
                .map(inputDescriptionMap::get)
                .filter(Objects::nonNull)
                .filter(object -> objectClass.isAssignableFrom(object.getClass()))
                .map(object -> (T) object)
                .collect(Collectors.toList());
    }

    public void addMetaDataError(String portName, Function<InputPort, MetaDataError> errorProvider) {
        if (operator != null) {
            InputPort port = operator.getInputPorts().getPortByName(portName);
            if (port != null)
                port.addError(errorProvider.apply(port));
        }
    }

}
