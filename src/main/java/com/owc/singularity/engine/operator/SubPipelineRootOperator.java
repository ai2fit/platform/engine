package com.owc.singularity.engine.operator;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.engine.pipeline.SubPipeline.SubPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.ports.extender.InputPortExtender;
import com.owc.singularity.engine.ports.extender.OutputPortExtender;

/**
 * Root Operator for {@link com.owc.singularity.engine.pipeline.SubPipeline}s.
 */
@DefinesOperator(module = "core", key = "sub_pipeline", group = "", name = "Sub-Pipeline", icon = "pipeline_piece.png")
public class SubPipelineRootOperator extends AbstractRootOperator {

    public static final String PARAMETER_VARIABLE_DEFINITIONS = "variable_definitions";
    public static final String PARAMETER_INPUT_DEFINITIONS = "input_definitions";
    public static final String PARAMETER_OUTPUT_DEFINITIONS = "output_definitions";

    private final InputPortExtender resultPortExtender = new InputPortExtender("result", 0d, getSubprocess(0).getInnerSinks());

    private final OutputPortExtender processInputExtender = new OutputPortExtender("input", 0d, getSubprocess(0).getInnerSources());

    public SubPipelineRootOperator() {
        super("Sub-Pipeline");

        resultPortExtender.start();
        processInputExtender.start();
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = new LinkedList<>();
        types.add(new ParameterTypeList(PARAMETER_VARIABLE_DEFINITIONS, "Defines a set of variables to use throughout the pipeline.", new ParameterTypeString(
                "variable_name",
                "Name of the variable, should start with lowercase character, no blanks. Can be accessed using %{name} notation in parameters within the pipeline."),
                new ParameterTypeTupel("type,_default_and_description", "",
                        new ParameterTypeEnumCategory<VariableType>("variable_type", "The type of the variable", VariableType.class, VariableType.STRING),
                        new ParameterTypeString("default_value", "The default value as string. Please make sure that default value fits to selected type."),
                        new ParameterTypeText("description", "Description of the variable", TextType.PLAIN))));
        String[] ioobjectNames = IOObjectService.getAllIOObjectNames().toArray(String[]::new);
        int exampleSetIndex = Arrays.asList(ioobjectNames).indexOf("Example Set");
        types.add(new ParameterTypeList(PARAMETER_INPUT_DEFINITIONS,
                "Can be used to give input ports a reasonable name and specify the type of delivered objects. List is according to port sequence.",
                new ParameterTypeString("port_name", "A descriptive port name for this input port."),
                new ParameterTypeTupel("type_and_description", "",
                        new ParameterTypeCategory("object_type", "The type of object expected.", ioobjectNames, exampleSetIndex),
                        new ParameterTypeText("description", "Description of the variable", TextType.PLAIN))));
        types.add(new ParameterTypeList(PARAMETER_OUTPUT_DEFINITIONS,
                "Can be used to give output ports a reasonable name and specify the type of delivered objects. List is according to port sequence.",
                new ParameterTypeString("port_name", "A descriptive port name for this input port."),
                new ParameterTypeCategory("object_type", "The type of object expected.", ioobjectNames, exampleSetIndex)));

        types.addAll(super.getParameterTypes());

        return types;
    }


    @Override
    public List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        SubPipelineExecutionContext subpipelineContext = (SubPipelineExecutionContext) context;
        processInputExtender.deliver(subpipelineContext.inputs);

        VariableHandler handler = getPipeline().getVariableHandler();
        // add undefined variable defaults
        for (String[] variableType : getParameterList(PARAMETER_VARIABLE_DEFINITIONS)) {
            if (variableType[0] != null && !variableType[0].isBlank()) {
                String variable = variableType[0];
                String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableType[1]);
                if (typeDefaultDescription[1] != null && !typeDefaultDescription[1].isBlank() && !handler.isVariableSet(variable, this))
                    handler.addVariable(variable, typeDefaultDescription[1]);
            }
        }

        // add variable values from context
        for (Entry<String, String> variableEntry : subpipelineContext.variableValues.entrySet()) {
            handler.addVariable(variableEntry.getKey(), variableEntry.getValue());
        }

        // check definition of variables
        for (String[] variableType : getParameterList(PARAMETER_VARIABLE_DEFINITIONS)) {
            if (variableType[0] != null && !variableType[0].isBlank()) {
                String variable = variableType[0];
                String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableType[1]);
                if (handler.isVariableSet(variable, this)) {
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                    String variableValue = handler.getVariable(variable, this);
                    if (!type.isCompatible(variableValue)) {
                        throw new UserError(this, "pipeline.variable_content_does_not_match", variable, type, variableValue);
                    }
                } else {
                    throw new UserError(this, "pipeline.variable_has_no_value", variable);
                }
            }
        }


        execute();
        return resultPortExtender.getData(IOObject.class, preserveEmptyResults, false);
    }
}
