/*

 * 

 * 

 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.operator.annotations.MarkDeprecated;


/**
 * Data container for name, class, short name, path and the (very short) description of an operator.
 * If the corresponding operator is not marked as deprecated the deprecation info string should be
 * null. If the icon string is null, the group icon will be used.
 *
 * @author Ingo Mierswa
 */
public class OperatorDescription implements Comparable<OperatorDescription> {

    /** the small icon for the constraint violation operators */
    private static final String I18N_UNSUPPORTED_SMALL_ICON_KEY = "gui.constraint.operator.unsupported_datasource.icon";

    /** the icon for the constraint violation operators */
    private static final String I18N_UNSUPPORTED_ICON_KEY = "gui.constraint.operator.unsupported_datasource.icon";

    /** the large icon for the constraint violation operators */
    private static final String I18N_UNSUPPORTED_LARGE_ICON_KEY = "gui.constraint.operator.unsupported_datasource.icon";

    private static final String DEPRECATED_GROUP_KEY = "deprecated";

    public static final String EXTENSIONS_GROUP_IDENTIFIER = "extensions";

    private final String module;
    private final String key;
    private final String group;
    private final Class<? extends Operator> operatorClass;

    private final String iconName;
    private List<String> replacesDeprecatedKeys;

    private final boolean enabled = true;

    private final String shortName;

    private final String name;


    public OperatorDescription(String module, String key, String group, Class<? extends Operator> clazz, String name, String shortName, String iconName) {
        this.module = module;
        this.key = key;
        this.group = group;
        this.operatorClass = clazz;
        this.name = name;
        this.shortName = shortName;
        this.iconName = iconName;
    }

    /**
     * This returns the human-readable name of this operator.
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * This returns a short form of the human-readable name, usable where space is critical. Should
     * contain most significant information.
     * 
     * @return shortName
     */
    public String getShortName() {
        return shortName;
    }

    public Class<? extends Operator> getOperatorClass() {
        return operatorClass;
    }

    /**
     * This returns the qualified, dot separated key of the containing group.
     */
    public String getGroup() {
        return group;
    }

    public String getAbbreviatedClassName() {
        return getOperatorClass().getName().replace("com.owc.singularity.engine.operator.", "c.o.s.e.o.");
    }

    public boolean isDeprecated() {
        return operatorClass.isAnnotationPresent(MarkDeprecated.class);
    }

    public String getDeprecationDescription() {
        MarkDeprecated annotation = operatorClass.getAnnotation(MarkDeprecated.class);
        if (annotation == null)
            return null;
        return annotation.description();
    }

    public String getModule() {
        return module;
    }

    /**
     * 
     * @return This returns the key with the module prefix.
     */
    public String getFullyQuallifiedKey() {
        return module + ":" + key;
    }

    /**
     * Returns the key of this operator without any prefix.
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Some operators may be disabled, e.g. because they cannot be applied inside an application
     * server (file access etc.)
     */
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return "key='" + key + "'; name='" + getName() + "'; " + (replacesDeprecatedKeys != null ? "replaces: " + replacesDeprecatedKeys : "")
                + "; implemented by " + operatorClass.getName() + "; group: " + group + "; icon: " + getIconName();
    }

    @Override
    public int compareTo(final OperatorDescription d) {
        String myName = this.getName();
        String otherName = d.getName();
        return myName.compareTo(otherName);
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof OperatorDescription other)) {
            return false;
        } else {
            return this.getFullyQuallifiedKey().equals(other.getFullyQuallifiedKey());
        }
    }

    @Override
    public int hashCode() {
        return this.getFullyQuallifiedKey().hashCode();
    }

    public void addIsReplacementFor(final String opName) {
        if (replacesDeprecatedKeys == null) {
            replacesDeprecatedKeys = new LinkedList<>();
        }
        replacesDeprecatedKeys.add(opName);
    }

    /** Returns keys of deprecated operators replaced by this operator. */
    public List<String> getReplacedKeys() {
        if (replacesDeprecatedKeys != null) {
            return replacesDeprecatedKeys;
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Returns the filename of the current icon
     *
     * @return the icon name, or {@code null} if no icon is set
     */
    public String getIconName() {
        return iconName;
    }
}
