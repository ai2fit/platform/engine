package com.owc.singularity.engine.operator;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import javax.xml.xpath.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.*;
import com.owc.singularity.engine.pipeline.AnalysisPipeline.AnalysisPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.ImplementingPipeline.ImplementingPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.SubPipeline.SubPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformationLog;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformer;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.extender.InputPortExtender;
import com.owc.singularity.engine.ports.extender.OutputPortExtender;
import com.owc.singularity.engine.tools.*;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.parameters.wizards.OpenProcessWizard;
import com.owc.singularity.studio.gui.parameters.wizards.RefreshParametersWizard;
import com.owc.singularity.studio.gui.parameters.wizards.RefreshParametersWizard.ParameterRefresher;
import com.owc.singularity.studio.gui.tools.ResourceAction;

/**
 * The root operator for any {@link ImplementingPipeline}.
 */
@DefinesOperator(module = "core", key = "implementing_pipeline", group = "", name = "Implementing Pipeline", icon = "implementing_pipeline.png")
public class ImplementingRootOperator extends AbstractRootOperator implements ParameterRefresher {

    public static final String PARAMETER_ABSTRACT_SUBPROCESS_NAME = "name";

    private static final String INNER_SINK_NAME = "out";
    private static final String INNER_SOURCE_NAME = "in";

    public static final String PARAMETER_VARIABLE_DEFINITIONS = "variable_definitions";
    public static final String PARAMETER_IMPLEMENTS_PIPELINE = "implements";

    public static final String PARAMETER_OPEN_PIPELINE = "open_pipeline";
    public static final String PARAMETER_REFRESH_PARAMETERS = "refresh_parameters";

    public static final String PARAMETER_IMPLEMENTATION_NAMES = "names";
    public static final String PARAMETER_IMPLEMENTATION_NAME = "name";

    private final InputPort inThroughPort = getInputPorts().createPort("through", 1d);
    private final OutputPort outThroughPort = getOutputPorts().createPort("through", 1d);

    private final InputPortExtender inputPortExtender = new InputPortExtender("input", 0d, getInputPorts());
    private final OutputPortExtender outputPortExtender = new OutputPortExtender("output", 0d, getOutputPorts());

    private AbstractPipeline abstractPipeline;
    private String abstractPath;

    public ImplementingRootOperator() {
        super("Implementation");

        inputPortExtender.start();
        outputPortExtender.start();

        InputPortExtender sub0InputExtender = new InputPortExtender(INNER_SINK_NAME, 1d, getSubprocess(0).getInnerSinks());
        sub0InputExtender.start();
        OutputPortExtender sub0OutputExtender = new OutputPortExtender(INNER_SOURCE_NAME, 1d, getSubprocess(0).getInnerSources());
        sub0OutputExtender.start();

        getTransformer().addPassThroughRule(inThroughPort, outThroughPort);

        if (!SingularityEngine.getExecutionMode().isHeadless()) {
            getParameters().addObserver(new Observer<String>() {

                private String lastParameterValue = null;

                @Override
                public void update(Observable<String> observable, String arg) {
                    try {
                        if (isParameterSet(PARAMETER_IMPLEMENTATION_NAMES)) {
                            if (!getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES).equals(lastParameterValue)) {
                                lastParameterValue = getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES);
                                normalizeSubprocessNames();
                            }
                        }
                    } catch (UndefinedParameterError ignored) {
                    }
                    getParameters().setTypes(getParameterTypes());
                }
            }, true);
        }


    }

    @Override
    public List<MetaData> transformMetaData(List<MetaData> inputMetaData) {
        AbstractPipeline implementedPipeline = getImplementedPipeline();
        if (implementedPipeline != null) {
            return implementedPipeline.getRootOperator().transformMetaData(inputMetaData);
        }
        return Collections.emptyList();
    }

    @Override
    public List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        RepositoryPath path = getParameterAsRepositoryPath(PARAMETER_IMPLEMENTS_PIPELINE);
        try {
            AbstractPipeline implementedPipeline = abstractPipeline = Entries.loadData(path, AbstractPipeline.class, ModuleService.getMajorClassLoader());
            implementedPipeline.setPath(getPipeline().getPath());

            if (context instanceof ImplementingPipelineExecutionContext) {
                // we are in debug mode in studio and need to derive real context
                ImplementingPipelineExecutionContext myContext = (ImplementingPipelineExecutionContext) context;
                // get locally defined variables
                HashMap<String, String> localVariables = myContext.variableValues;
                context = implementedPipeline.derivePipelineExecutionContext(myContext.developmentContext);
                if (context instanceof AnalysisPipelineExecutionContext) {
                    ((AnalysisPipelineExecutionContext) context).variableValues.putAll(localVariables);
                } else if (context instanceof SubPipelineExecutionContext) {
                    ((SubPipelineExecutionContext) context).variableValues.putAll(localVariables);
                }
            }

            // adding implementations
            String[] implementationNames = ParameterTypeEnumeration.transformString2Enumeration(getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES));
            for (int i = 0; i < implementationNames.length; i++) {
                context.provideImplementation(ParameterTypeTupel.transformString2Tupel(implementationNames[i])[0], new CompositionImplementation(this, i));
            }

            // setting variables
            if (implementedPipeline instanceof AnalysisPipeline) {
                deriveVariables(implementedPipeline, ((AnalysisPipelineExecutionContext) context).variableValues);
            }
            if (implementedPipeline instanceof SubPipeline) {
                deriveVariables(implementedPipeline, ((SubPipelineExecutionContext) context).variableValues);
            }

            // breakpoint handling
            implementedPipeline.getAllOperators().forEach(operator -> {
                operator.setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, false);
                operator.setBreakpoint(BreakpointListener.BREAKPOINT_BEFORE, false);
            });
            implementedPipeline.addBreakpointListener(new BreakpointListener() {

                @Override
                public void breakpointReached(AbstractPipeline process, Operator operator, List<IOObject> objects, int location) {
                    getPipeline().pause(getPipeline().getOperator(operator.getName()), objects, location);
                }

                @Override
                public void resume() {
                    // resume is triggered from implementing pipeline
                }

            });

            // stop handling
            PipelineLifeCycleEventListener stopListener = new PipelineLifeCycleEventListener() {

                @Override
                public String getIdentifier() {
                    return "implementing_pipeline";
                }

                @Override
                public void onAfterPipelineStop(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
                    implementedPipeline.stop();
                }

                @Override
                public void onAfterPipelineResume(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
                    implementedPipeline.resume();
                }
            };
            getPipeline().registerLifeCycleEventListener(stopListener);
            try {
                return implementedPipeline.run(context, preserveEmptyResults);
            } catch (OperatorException e) {
                if (e.getOperator() != null && e.getOperator().getPipeline() == implementedPipeline) {
                    // the error comes from the outside scope, i.e., from the abstract pipeline
                    throw new OperatorException("Abstract pipeline caused error: " + e.getMessage(), e);
                }
                // error from within
                throw e;
            } finally {
                getPipeline().unregisterLifeCycleEventListener(stopListener);
            }
        } catch (IOException e) {
            throw new OperatorException(this, "Could not load abstract pipeline " + getParameterAsString(PARAMETER_IMPLEMENTS_PIPELINE), e);
        }
    }

    private Map<String, String> deriveVariables(AbstractPipeline implementedPipeline, Map<String, String> variableMap) throws UndefinedParameterError {
        // set defaults of locally defined variables
        for (String[] variableType : getParameterList(PARAMETER_VARIABLE_DEFINITIONS)) {
            if (variableType[0] != null && !variableType[0].isBlank()) {
                String variable = variableType[0];
                String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableType[1]);
                if (typeDefaultDescription[1] != null && !typeDefaultDescription[1].isBlank() && !variableMap.containsKey(variable))
                    variableMap.put(variable, typeDefaultDescription[1]);
            }
        }

        // making variables available for parameter resolution of fixed variables
        variableMap.forEach(getPipeline().getVariableHandler()::addVariable);

        // taking fixed variables of implemented root
        AbstractRootOperator rootOperator = implementedPipeline.getRootOperator();
        if (rootOperator.isParameterSet(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
            Map<String, String> defaultValues = rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                    .stream()
                    .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
            for (String[] variableDefinition : rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                if (variableDefinition[0] != null && !variableDefinition[0].isBlank()) {
                    String variable = variableDefinition[0];
                    // get default first
                    String value = defaultValues.get(variable);
                    String variableTypeName = ParameterTypeTupel.transformString2Tupel(variableDefinition[1])[0];

                    // then get from variable and override
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(variableTypeName, VariableType.class);
                    String variableParameterKey = "set_" + variable;
                    if (isParameterSet(variableParameterKey)) {
                        try {
                            String variableTupelValue = getParameter(variableParameterKey);
                            String[] variableTupelValues = ParameterTypeTupel.transformString2Tupel(variableTupelValue);
                            String variableValue = variableTupelValues[1];
                            if ("true".equals(variableTupelValues[0]) && !variableValue.isEmpty()) {
                                value = switch (type) {
                                    case DATE -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                    case TIMESTAMP -> VariableHandler.dateFormatter
                                            .format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                    default -> variableValue;
                                };

                                variableMap.put(variable, value);
                            }
                        } catch (ParseException e) {
                        }
                    }

                }
            }
        }
        return variableMap;
    }

    @Override
    public TreeVisitResult walk(TreeVisitor<ExecutionUnit, Operator> visitor) {
        if (abstractPipeline != null) {
            // if available, walk as the abstract pipeline tree
            return abstractPipeline.walk(visitor);
        }
        // otherwise, walk as a simple operator
        return super.walk(visitor);
    }

    @Override
    public String getParameter(String key) throws UndefinedParameterError {
        if (super.isParameterSet(key))
            return super.getParameter(key);
        AbstractPipeline implementedPipeline = getImplementedPipeline();
        if (implementedPipeline != null)
            return implementedPipeline.getRootOperator().getParameter(key);
        return null;
    }

    @Override
    public boolean isParameterSet(String key) {
        if (super.isParameterSet(key))
            return true;
        AbstractPipeline implementedPipeline = getImplementedPipeline();
        if (implementedPipeline != null)
            return implementedPipeline.getRootOperator().isParameterSet(key);
        return false;
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = new LinkedList<>();
        types.add(new ParameterTypeList(PARAMETER_VARIABLE_DEFINITIONS, "Defines a set of variables to use throughout the pipeline.", new ParameterTypeString(
                "variable_name",
                "Name of the variable, should start with lowercase character, no blanks. Can be accessed using %{name} notation in parameters within the pipeline."),
                new ParameterTypeTupel("type,_default_and_description", "",
                        new ParameterTypeEnumCategory<VariableType>("variable_type", "The type of the variable", VariableType.class, VariableType.STRING),
                        new ParameterTypeString("default_value", "The default value as string. Please make sure that default value fits to selected type."),
                        new ParameterTypeText("description", "Description of the variable", TextType.PLAIN))));
        types.add(new ParameterTypeRepositoryLocation(PARAMETER_IMPLEMENTS_PIPELINE, "Abstract pipeline which will be extended.", false, true));
        ParameterType type = new ParameterTypeConfiguration(OpenProcessWizard.class,
                Collections.singletonMap(OpenProcessWizard.PIPELINE_PARAMETER, PARAMETER_IMPLEMENTS_PIPELINE), this);
        type.setKey(PARAMETER_OPEN_PIPELINE);
        type.setDescription("This button will open the linked pipelines.");
        types.add(type);
        type = new ParameterTypeConfiguration(RefreshParametersWizard.class, null, this);
        type.setKey(PARAMETER_REFRESH_PARAMETERS);
        type.setDescription("This button will refresh parameters for variables if the linked pipeline changed.");
        types.add(type);
        types.add(new ParameterTypeEnumeration(PARAMETER_IMPLEMENTATION_NAMES,
                "Names of the abstract subprocesses implemented by this operators subprocesses. The first name matches the first subprocess from the left, the second the second, etc.",
                new ParameterTypeTupel(PARAMETER_IMPLEMENTATION_NAME, "The names of the abstract subprocesses.",
                        new ParameterTypeSuggestion("suggestion", "The names of the abstract subprocesses.", new SuggestionProvider<String>() {

                            private String pipelinePathString;
                            private List<String> subprocesses;

                            @Override
                            public List<String> getSuggestions(Operator op, ProgressListener pl) {
                                if (!SingularityEngine.getExecutionMode().isHeadless()) {
                                    try {
                                        if (pipelinePathString == null || !pipelinePathString.equals(op.getParameterAsString(PARAMETER_IMPLEMENTS_PIPELINE))) {
                                            // we load the process entry
                                            RepositoryPath pipelinePath = op.getParameterAsRepositoryPath(PARAMETER_IMPLEMENTS_PIPELINE);
                                            AbstractPipeline process = Entries.loadData(pipelinePath, AbstractPipeline.class,
                                                    ModuleService.getMajorClassLoader());
                                            HashSet<String> subprocessesSet = new HashSet<>();
                                            for (Operator operator : process.getAllOperators()) {
                                                if (operator instanceof DefineAbstractInnerPipelineComposition) {
                                                    subprocessesSet.add(operator
                                                            .getParameter(DefineAbstractInnerPipelineComposition.PARAMETER_ABSTRACT_INNER_PIPELINE_NAME));
                                                }
                                            }
                                            ArrayList<String> subprocesses = new ArrayList<>(subprocessesSet);
                                            Collections.sort(subprocesses);
                                            this.subprocesses = subprocesses;
                                            this.pipelinePathString = op.getParameterAsString(PARAMETER_IMPLEMENTS_PIPELINE);
                                        }
                                    } catch (Exception e) {
                                        return Collections.emptyList();
                                    }
                                    return subprocesses;
                                } else {
                                    return Collections.emptyList();
                                }
                            }

                            @Override
                            public ResourceAction getAction() {
                                // we don't have an action here, so return null
                                return null;
                            }
                        }, true))));

        AbstractPipeline pipeline = getImplementedPipeline();
        if (pipeline != null && (pipeline instanceof SubPipeline || pipeline instanceof AnalysisPipeline)) {
            // types for variables
            try {
                List<String[]> variableDefintions = pipeline.getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS);
                Map<String, String> defaultVariableValue = variableDefintions.stream()
                        .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
                for (String[] variableTupel : variableDefintions) {
                    if (variableTupel[0] != null && !variableTupel[0].isBlank()) {
                        String variable = variableTupel[0];
                        String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableTupel[1]);
                        VariableType variableType = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                        String defaultValue = defaultVariableValue.get(variable);

                        boolean isOptional = (defaultValue != null && !defaultValue.isEmpty());
                        ParameterType parameterType = switch (variableType) {
                            case DATE -> (new ParameterTypeDate("variable_" + variable, typeDefaultDescription[2], isOptional));
                            case FILE_PATH -> new ParameterTypeFile("variable_" + variable, typeDefaultDescription[2], null, isOptional);
                            case INTEGER -> new ParameterTypeInt("variable_" + variable, typeDefaultDescription[2], Integer.MIN_VALUE, Integer.MAX_VALUE,
                                    isOptional);
                            case REAL -> new ParameterTypeDouble("variable_" + variable, typeDefaultDescription[2], Double.NEGATIVE_INFINITY,
                                    Double.POSITIVE_INFINITY, isOptional);
                            case REPOSITORY_PATH -> new ParameterTypeRepositoryLocation("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case STRING -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case TIMESTAMP -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                        };

                        if (isOptional) {
                            parameterType.setDefaultValueAsRawString(defaultValue);
                        }
                        types.add(new ParameterTypeTupel("set_" + variable, "If checked, the variable of the implemented pipeline will be set to this value.",
                                new double[] { 0d, 1d }, new ParameterTypeBoolean("set_to", "If checked, variable will be set."), parameterType));
                    }

                }
            } catch (UndefinedParameterError e) {
            }
        }


        return types;
    }

    public AbstractPipeline getImplementedPipeline() {
        if (hasParameters() && super.isParameterSet(PARAMETER_IMPLEMENTS_PIPELINE)) {
            try {
                String pipelinePath = getParameterAsString(PARAMETER_IMPLEMENTS_PIPELINE);
                if (!pipelinePath.equals(abstractPath)) {
                    RepositoryPath path = ParameterTypeRepositoryLocation.getPath(pipelinePath, this);
                    if (path.isAbsolute()) {
                        abstractPipeline = Entries.loadData(path, AbstractPipeline.class, ModuleService.getMajorClassLoader());
                        abstractPipeline.setPath(path);
                        abstractPath = pipelinePath;
                    }
                }
                return abstractPipeline;
            } catch (UndefinedParameterError | IOException e) {
                abstractPath = null;
            }
        }
        return null;
    }

    @Override
    public final boolean areSubprocessesExtendable() {
        return true;
    }


    @Override
    protected final ExecutionUnit createSubprocess(int index) {
        return new ExecutionUnit(this, "Implementation");
    }

    @Override
    public final ExecutionUnit addSubprocess(int index) {

        // now construct subprocess
        ExecutionUnit newProcess = super.addSubprocess(index);
        InputPortExtender inputExtender = new InputPortExtender(INNER_SINK_NAME, 1d, newProcess.getInnerSinks());
        inputExtender.start();
        OutputPortExtender outputExtender = new OutputPortExtender(INNER_SOURCE_NAME, 1d, newProcess.getInnerSources());
        outputExtender.start();

        // get names of abstract subprocesses
        List<String> names = getAbstractSubprocessNames();
        // getting existing names
        String[] unitNames = new String[0];
        if (isParameterSet(PARAMETER_IMPLEMENTATION_NAMES)) {
            try {
                unitNames = ParameterTypeEnumeration.transformString2Enumeration(getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES));
                for (int i = 0; i < unitNames.length; i++)
                    unitNames[i] = ParameterTypeTupel.transformString2Tupel(unitNames[i])[0];
            } catch (UndefinedParameterError ignore) {
            }
        }
        for (String name : unitNames) {
            names.remove(name);
        }

        // now if one is missing, we add to parameter
        String newName = "implementation " + (index + 1);
        if (names.size() > 0) {
            newName = names.get(0);
        }
        List<String> encodedNewNames = new LinkedList<>();
        if (isParameterSet(PARAMETER_IMPLEMENTATION_NAMES)) {
            try {
                unitNames = ParameterTypeEnumeration.transformString2Enumeration(getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES));
                Collections.addAll(encodedNewNames, unitNames);
            } catch (UndefinedParameterError ignored) {
            }
        }
        if (this.getSubprocesses().size() > encodedNewNames.size()) {
            if (index < encodedNewNames.size())
                encodedNewNames.add(index, ParameterTypeTupel.transformTupel2String(new String[] { newName }));
            else
                encodedNewNames.add(ParameterTypeTupel.transformTupel2String(new String[] { newName }));

            setParameter(PARAMETER_IMPLEMENTATION_NAMES, ParameterTypeEnumeration.transformEnumeration2String(encodedNewNames));
        }

        normalizeSubprocessNames();
        return newProcess;
    }

    @Override
    public final ExecutionUnit removeSubprocess(int index) {
        // getting existing names
        String[] unitNames = new String[0];
        if (isParameterSet(PARAMETER_IMPLEMENTATION_NAMES)) {
            try {
                unitNames = ParameterTypeEnumeration.transformString2Enumeration(getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES));
            } catch (UndefinedParameterError ignored) {
            }
        }
        ArrayList<String> encodedNewNames = new ArrayList<>(Arrays.asList(unitNames));
        if (encodedNewNames.size() > index) {
            encodedNewNames.remove(index);
            setParameter(PARAMETER_IMPLEMENTATION_NAMES, ParameterTypeEnumeration.transformEnumeration2String(encodedNewNames));
        }

        // new also delete the subprocess
        ExecutionUnit oldProcess = super.removeSubprocess(index);
        normalizeSubprocessNames();

        // cleaning port extender map
        return oldProcess;
    }

    private void normalizeSubprocessNames() {
        String[] unitNames = new String[0];
        if (isParameterSet(PARAMETER_IMPLEMENTATION_NAMES)) {
            try {
                unitNames = ParameterTypeEnumeration.transformString2Enumeration(getParameterAsString(PARAMETER_IMPLEMENTATION_NAMES));
                for (int i = 0; i < unitNames.length; i++)
                    unitNames[i] = ParameterTypeTupel.transformString2Tupel(unitNames[i])[0];
            } catch (UndefinedParameterError ignored) {
            }
        }
        for (int i = 0; i < getNumberOfSubprocesses(); i++) {
            if (i < unitNames.length) {
                getSubprocess(i).setName(unitNames[i]);
            } else {
                getSubprocess(i).setName("Implementation " + (i + 1));
            }
        }
    }

    private List<String> getAbstractSubprocessNames() {
        try {
            // we load the pipeline entry
            RepositoryPath pipelineLocation = getParameterAsRepositoryPath(PARAMETER_IMPLEMENTS_PIPELINE);
            if (pipelineLocation.isAbsolute()) {
                AbstractPipeline pipeline = Entries.loadData(pipelineLocation, AbstractPipeline.class, ModuleService.getMajorClassLoader());
                HashSet<String> subprocessesSet = new HashSet<>();
                for (Operator operator : pipeline.getAllOperators()) {
                    if (operator instanceof DefineAbstractInnerPipelineComposition) {
                        subprocessesSet.add(operator.getParameter(PARAMETER_ABSTRACT_SUBPROCESS_NAME));
                    }
                }
                ArrayList<String> subprocesses = new ArrayList<>(subprocessesSet);
                Collections.sort(subprocesses);
                return subprocesses;
            } else {
                return Collections.emptyList();
            }
        } catch (UserError | IOException e) {
            return Collections.emptyList();
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.PROCESS_REPLACEMENT)
    public static class ImplementingPipelineTypeUpgradeTransformer implements CompatibilityTransformer {

        protected static final XPathFactory xpathfactory = XPathFactory.newInstance();
        private XPath xPath;
        private XPathExpression implementationOperatorExpr;
        private XPathExpression pipelineExpr;
        private XPathExpression implementsParameterExpr;
        private XPathExpression macrosParameterExpr;
        {
            xPath = xpathfactory.newXPath();
            try {
                implementationOperatorExpr = xPath.compile("//operator[@class='rmx_speds:execute_abstract_process' and @activated='true']");
                pipelineExpr = xPath.compile("/pipeline");
                implementsParameterExpr = xPath.compile("parameter[@key='process_location']");
                macrosParameterExpr = xPath.compile("list[@key='macros']/parameter");
            } catch (Exception e) {
                // should never occur
                e.printStackTrace();
            }
        }

        @Override
        public void apply(Document document, CompatibilityTransformationLog log) {
            // check if already type attribute present, then it's already up to date
            try {
                NodeList impementationNodes = (NodeList) implementationOperatorExpr.evaluate(document, XPathConstants.NODESET);
                if (impementationNodes.getLength() == 1) {
                    Element implementationElement = (Element) impementationNodes.item(0);
                    Element pipelineElement = (Element) pipelineExpr.evaluate(document, XPathConstants.NODE);
                    Element rootOperator = XMLTools.getChildElement(pipelineElement, "operator", false);

                    // move parameters to implementing element
                    XMLTools.getChildElements(rootOperator, "parameter").forEach(element -> {
                        rootOperator.removeChild(element);
                        implementationElement.appendChild(element);
                    });
                    XMLTools.getChildElements(rootOperator, "list").forEach(element -> {
                        rootOperator.removeChild(element);
                        implementationElement.appendChild(element);
                    });
                    XMLTools.getChildElements(rootOperator, "enumeration").forEach(element -> {
                        rootOperator.removeChild(element);
                        implementationElement.appendChild(element);
                    });

                    // move annotations to first inner pipeline
                    Element firstImplementingInnerPipeline = XMLTools.getChildElements(implementationElement, "pipeline").iterator().next();
                    Element rootPipeline = XMLTools.getChildElement(rootOperator, "pipeline", true);
                    XMLTools.getChildElements(rootPipeline, "description").forEach(element -> {
                        rootPipeline.removeChild(element);
                        firstImplementingInnerPipeline.appendChild(element);
                    });

                    // rename parameters
                    Element parameterElement = (Element) implementsParameterExpr.evaluate(implementationElement, XPathConstants.NODE);
                    parameterElement.setAttribute("key", "implements");

                    // replace root
                    implementationElement.setAttribute("class", "core:implementing_pipeline");
                    pipelineElement.removeChild(rootOperator);
                    pipelineElement.appendChild(implementationElement);

                    // set pipeline type
                    pipelineElement.setAttribute("type", ImplementingPipeline.class.getName());

                    // restore macros as variables
                    NodeList macroSettings = (NodeList) macrosParameterExpr.evaluate(implementationElement, XPathConstants.NODESET);
                    for (int i = 0; i < macroSettings.getLength(); i++) {
                        Element macroListElement = (Element) macroSettings.item(i);
                        macroListElement.setAttribute("key", "variable_" + macroListElement.getAttribute("key"));
                        macroListElement.getParentNode().removeChild(macroListElement);
                        implementationElement.appendChild(macroListElement);
                    }

                }
            } catch (XPathExpressionException | XMLException e) {
            }
        }
    }

    @Override
    public void refreshParameters() {
        abstractPath = null;
        getParameters().setTypes(getParameterTypes());
    }
}
