/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessSetupError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.tools.*;


/**
 * An OperatorChain is an Operator that contains a nested {@link ExecutionUnit}s,which in turn
 * contain operators. This execution unit can be executed once or several times during its own
 * execution.<br/>
 *
 * @author Simon Fischer, Ingo Mierswa
 */
public abstract class OperatorChain extends Operator {

    private ExecutionUnit[] subprocesses;

    private final Observer<ExecutionUnit> delegatingObserver = new DelegatingObserver<ExecutionUnit, Operator>(this, this);

    public OperatorChain(String... subprocessNames) {
        subprocesses = new ExecutionUnit[subprocessNames.length];
        for (int i = 0; i < subprocesses.length; i++) {
            subprocesses[i] = new ExecutionUnit(this, subprocessNames[i]);
            subprocesses[i].addObserver(delegatingObserver, false);
            makeDirtyOnUpdate(subprocesses[i].getInnerSinks());
        }
    }

    /**
     * Indicates whether the GUI may offer an option to dynamically add to the number of
     * subprocesses. The default implementation returns false.
     */
    public boolean areSubprocessesExtendable() {
        return false;
    }

    public ExecutionUnit removeSubprocess(int index) {
        ExecutionUnit deleted = subprocesses[index];

        // first remove all operators
        deleted.getAllInnerOperators().forEach(operator -> operator.getPipeline().unregisterName(operator.getName()));


        ExecutionUnit[] copy = subprocesses;
        subprocesses = new ExecutionUnit[copy.length - 1];
        int j = 0;
        for (int i = 0; i < copy.length; i++) {
            if (i != index) {
                subprocesses[j++] = copy[i];
            }
        }
        deleted.removeObserver(delegatingObserver);
        fireUpdate(this);
        return deleted;
    }

    /** Creates a subprocess by making a callback to {@link #createSubprocess(int)}. */
    public ExecutionUnit addSubprocess(int index) {
        ExecutionUnit[] copy = subprocesses;
        subprocesses = new ExecutionUnit[copy.length + 1];
        int j = 0;
        for (int i = 0; i < copy.length; i++) {
            if (i == index) {
                j++;
            }
            subprocesses[j++] = copy[i];
        }
        subprocesses[index] = createSubprocess(index);
        subprocesses[index].addObserver(delegatingObserver, false);
        fireUpdate(this);
        return subprocesses[index];
    }

    protected ExecutionUnit createSubprocess(int index) {
        return new ExecutionUnit(this, "Sub-Pipeline");
    }

    /**
     * This method returns an arbitrary implementation of {@link InputPorts} for inner sink port
     * initialization. Useful for adding an arbitrary implementation (e.g. changing port creation &
     * (dis)connection behavior, optionally by customized {@link InputPort} instances) by overriding
     * this method.
     * 
     * @param portOwner
     *            The owner of the ports.
     * @return The {@link InputPorts} instance, never {@code null}.
     */
    protected InputPorts createInnerSinks(PortOwner portOwner) {
        return new SortingInputPorts(portOwner);
    }

    /**
     * This method returns an arbitrary implementation of {@link OutputPorts} for inner source port
     * initialization. Useful for adding an arbitrary implementation (e.g. changing port creation &
     * (dis)connection behavior, optionally by customized {@link OutputPort} instances) by
     * overriding this method.
     * 
     * @param portOwner
     *            The owner of the ports.
     * @return The {@link OutputPorts} instance, never {@code null}.
     */
    protected OutputPorts createInnerSources(PortOwner portOwner) {
        return new SortingOutputPorts(portOwner);
    }

    /**
     * Performs a deep clone of this operator chain. Use this method only if you are sure what you
     * are doing.
     */
    @Override
    public Operator cloneOperator(String name, boolean forParallelExecution) {
        OperatorChain clone = (OperatorChain) super.cloneOperator(name, forParallelExecution);
        if (areSubprocessesExtendable()) {
            while (clone.getNumberOfSubprocesses() < getNumberOfSubprocesses()) {
                clone.addSubprocess(clone.getNumberOfSubprocesses());
            }
        }
        for (int i = 0; i < subprocesses.length; i++) {
            clone.subprocesses[i].cloneExecutionUnitFrom(this.subprocesses[i], forParallelExecution);
        }
        return clone;
    }


    /**
     * Register this operator chain and all of its children in the given process. This might change
     * the name of the operator.
     */
    @Override
    protected void registerOperator(AbstractPipeline pipeline) {
        super.registerOperator(pipeline);
        for (ExecutionUnit subprocess : subprocesses) {
            for (Operator child : subprocess.getOperators()) {
                child.registerOperator(pipeline);
            }
        }
    }

    /** Unregisters this chain and all of its children from the given process. */
    @Override
    protected void unregisterOperator(AbstractPipeline pipeline) {
        super.unregisterOperator(pipeline);
        for (ExecutionUnit subprocess : subprocesses) {
            for (Operator child : subprocess.getOperators()) {
                child.unregisterOperator(pipeline);
            }
        }
    }

    /** Returns all operators contained in the subprocesses of this chain (non-recursive). */
    public List<Operator> getImmediateChildren() {
        List<Operator> children = new LinkedList<>();
        for (ExecutionUnit executionUnit : subprocesses) {
            children.addAll(executionUnit.getOperators());
        }
        return children;
    }

    /** Returns recursively all child operators independently if they are activated or not. */
    public List<Operator> getAllInnerOperators() {
        List<Operator> children = new LinkedList<>();
        for (ExecutionUnit executionUnit : subprocesses) {
            children.addAll(executionUnit.getAllInnerOperators());
        }
        return children;
    }

    public List<Operator> getAllInnerOperatorsAndMe() {
        List<Operator> children = getAllInnerOperators();
        children.add(this);
        return children;
    }


    /**
     * Returns the result of the super method if this operator does not have a parent. Otherwise
     * this method returns true if it is enabled and the parent is also enabled.
     */
    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    /** Invokes the super method and the method for all children. */
    @Override
    public void pipelineStarts() throws OperatorException {
        ListenerTools.informAllAndThrow(x -> super.pipelineStarts(), Arrays.asList(subprocesses), ExecutionUnit::processStarts);
    }

    /** Invokes the super method and the method for all children. */
    @Override
    public void pipelineFinished() throws OperatorException {
        ListenerTools.informAllAndThrow(x -> super.pipelineFinished(), Arrays.asList(subprocesses), ExecutionUnit::processFinished);
    }

    /**
     * Clears all sinks of all inner processes
     */
    protected void clearAllInnerSinks() {
        for (ExecutionUnit subprocess : subprocesses) {
            subprocess.getInnerSinks().clear(Port.CLEAR_DATA);
        }
    }

    @Override
    public void doWork() throws OperatorException {
        for (ExecutionUnit subprocess : subprocesses) {
            subprocess.execute();
        }
    }

    @Override
    public void freeMemory() {
        super.freeMemory();
        for (ExecutionUnit unit : subprocesses) {
            unit.freeMemory();
        }
    }

    /**
     * This method invokes the additional check method for each child. Subclasses which override
     * this method to perform a check should also invoke super.performAdditionalChecks()!
     */
    @Override
    protected void performAdditionalChecks() {
        super.performAdditionalChecks();
        for (ExecutionUnit subprocess : subprocesses) {
            for (Operator o : subprocess.getOperators()) {
                if (o.isEnabled()) {
                    o.performAdditionalChecks();
                }
            }
        }
    }

    /**
     * Will throw an exception if a non optional property has no default value and is not defined by
     * user.
     */
    @Override
    public int checkProperties() {
        int errorCount = super.checkProperties();
        for (ExecutionUnit subprocess : subprocesses) {
            for (Operator o : subprocess.getOperators()) {
                if (o.isEnabled()) {
                    errorCount += o.checkProperties();
                }
            }
        }
        return errorCount;
    }

    /**
     * Will count the number of deprecated operators. Returns the total number of deprecations.
     */
    @Override
    public int checkDeprecations() {
        int deprecationCount = super.checkDeprecations();
        for (ExecutionUnit subprocess : subprocesses) {
            for (Operator o : subprocess.getOperators()) {
                deprecationCount += o.checkDeprecations();
            }
        }
        return deprecationCount;
    }

    /**
     * Returns this OperatorChain's name and class and the process trees of the inner operators.
     */
    @Override
    protected String createProcessTree(int indent, String selfPrefix, String childPrefix, Operator markOperator, String mark) {
        StringBuilder treeBuilder = new StringBuilder(super.createProcessTree(indent, selfPrefix, childPrefix, markOperator, mark));
        for (int i = 0; i < subprocesses.length; i++) {
            List<String> processTreeList = subprocesses[i].createProcessTreeList(indent, childPrefix + "+- ",
                    childPrefix + (i < subprocesses.length - 1 ? "|  " : "   "), markOperator, mark);
            for (String entry : processTreeList) {
                treeBuilder.append(Tools.getLineSeparator());
                treeBuilder.append(entry);
            }
        }
        return treeBuilder.toString();
    }

    @Override
    public List<String> createProcessTreeList(int indent, String selfPrefix, String childPrefix, Operator markOperator, String mark) {
        List<String> treeList = super.createProcessTreeList(indent, selfPrefix, childPrefix, markOperator, mark);
        for (int i = 0; i < subprocesses.length; i++) {
            treeList.addAll(subprocesses[i].createProcessTreeList(indent, childPrefix + "+- ", childPrefix + (i < subprocesses.length - 1 ? "|  " : "   "),
                    markOperator, mark));
        }
        return treeList;
    }

    public ExecutionUnit getSubprocess(int index) {
        return subprocesses[index];
    }

    public int getNumberOfSubprocesses() {
        return subprocesses.length;
    }

    /** Returns an immutable view of all subprocesses */
    public List<ExecutionUnit> getSubprocesses() {
        return Arrays.asList(subprocesses);
    }

    @Override
    protected void collectErrors(List<ProcessSetupError> errors) {
        super.collectErrors(errors);
        for (ExecutionUnit executionUnit : subprocesses) {
            for (Operator op : executionUnit.getOperators()) {
                op.collectErrors(errors);
            }
            for (Port port : executionUnit.getInnerSinks().getAllPorts()) {
                errors.addAll(port.getErrors());
            }
            for (Port port : executionUnit.getInnerSources().getAllPorts()) {
                errors.addAll(port.getErrors());
            }
        }
    }

    @Override
    public void clear(int clearFlags) {
        super.clear(clearFlags);
        for (ExecutionUnit executionUnit : subprocesses) {
            executionUnit.clear(clearFlags);
        }
    }

    @Override
    public void notifyRenaming(String oldName, String newName) {
        Arrays.stream(subprocesses).forEach(unit -> unit.getOperators().forEach(op -> op.notifyRenaming(oldName, newName)));
        super.notifyRenaming(oldName, newName);
    }

    /**
     * {@inheritDoc}
     *
     * Also all inner operators will be notified.
     */
    @Override
    public void notifyReplacing(String oldName, Operator oldOp, String newName, Operator newOp) {
        Arrays.stream(subprocesses).forEach(unit -> unit.getOperators().forEach(op -> op.notifyReplacing(oldName, oldOp, newName, newOp)));
        super.notifyReplacing(oldName, oldOp, newName, newOp);
    }

    @Override
    protected void propagateDirtyness() {
        for (ExecutionUnit unit : subprocesses) {
            for (Operator op : unit.getOperators()) {
                op.propagateDirtyness();
            }
        }
    }

    @Override
    protected Operator lookupOperator(String operatorName) {
        Operator result = super.lookupOperator(operatorName);
        if (result != null) {
            return result;
        } else {
            for (Operator child : getAllInnerOperators()) {
                if (child.getName().equals(operatorName)) {
                    return child;
                }
            }
            return null;
        }
    }

    @Override
    public TreeVisitResult walk(TreeVisitor<ExecutionUnit, Operator> visitor) {
        for (ExecutionUnit unit : subprocesses) {
            TreeVisitResult visitResultBefore = visitor.preVisitNode(unit);
            if (visitResultBefore == TreeVisitResult.TERMINATE || visitResultBefore == TreeVisitResult.SKIP_SIBLINGS) {
                return visitResultBefore;
            }
            if (visitResultBefore == TreeVisitResult.CONTINUE) {
                for (Operator op : unit.getOperators()) {
                    TreeVisitResult walkResult = op.walk(visitor);
                    if (walkResult == TreeVisitResult.SKIP_SIBLINGS || walkResult == TreeVisitResult.SKIP_SUBTREE)
                        break;
                    if (walkResult == TreeVisitResult.TERMINATE) {
                        return walkResult;
                    }
                    // continue
                }
            }
            // we scanned/skipped all children
            // no call the post-visit function
            TreeVisitResult visitResultAfter = visitor.postVisitNode(unit);
            switch (visitResultAfter) {
                case TERMINATE, SKIP_SIBLINGS -> {
                    return visitResultAfter;
                }
                case SKIP_SUBTREE, CONTINUE -> {
                    // continue
                }
            }
        }
        return TreeVisitResult.CONTINUE;
    }

}
