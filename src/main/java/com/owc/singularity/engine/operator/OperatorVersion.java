/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;


import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.modules.Module;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.tools.VersionNumber;


/**
 * Operators can change their behaviour from one version to another. Hence, their version is stored
 * in the process XML file. If the behaviour of an operator changes from, say, version 5.0.003 to
 * 5.0.004, we can notify the user in version 5.0.004. To that end, the method
 * {@link Operator#getCompatibilityLevel()} will return 5.0.003, whereas
 * {@link Operator#getIncompatibleVersionChanges()} will return [5.0.003] (or a superset thereof) in
 * version [5.0.004], so that we can detect that the behavior changed.
 * <p>
 * 
 * <strong>Note:</strong> The version numbers always refer to the plugin the operator is loaded
 * from. If it is not loaded from a plugin, it refers to the SingularityEngine version.
 * 
 * @author Simon Fischer
 * 
 */
public class OperatorVersion extends VersionNumber {

    private static OperatorVersion SAME_AS_ENGINE;

    /**
     * Parses a version string of the form x.xx.xxx
     * 
     * @throws VersionNumberException
     *             for malformed strings.
     */
    public OperatorVersion(String versionString) {
        super(versionString);
    }

    public OperatorVersion(int major, int minor, int buildNumber) {
        super(major, minor, buildNumber);
    }

    /**
     * Creates an {@link OperatorVersion} from any other {@link VersionNumber}.
     */
    public static OperatorVersion asNewOperatorVersion(VersionNumber vn) {
        try {
            return new OperatorVersion(vn.getLongVersion());
        } catch (VersionNumberException vne) {
            return new OperatorVersion(vn.getMajorNumber(), vn.getMinorNumber(), vn.getPatchLevel());
        }
    }

    public static OperatorVersion getLatestVersion(OperatorDescription desc) {
        Module plugin = ModuleService.getModuleByKey(desc.getModule());
        if (plugin == null) {
            return current();
        } else {
            try {
                return new OperatorVersion(plugin.getVersion().toString());
            } catch (VersionNumberException vne) {
                // returning current version
                return current();
            }
        }
    }

    public static OperatorVersion current() {
        if (SAME_AS_ENGINE == null) {
            SAME_AS_ENGINE = asNewOperatorVersion(SingularityEngine.getVersion());
        }
        return SAME_AS_ENGINE;
    }
}
