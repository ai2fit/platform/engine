/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;


import java.util.Collections;
import java.util.List;

import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.operator.error.SimpleProcessSetupError;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.ports.InputPorts;
import com.owc.singularity.engine.ports.OutputPorts;
import com.owc.singularity.engine.ports.extender.*;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * This operator cannot be executed. It is merely used by a {@link XMLImporter} to create a dummy
 * operator that acts as a placeholder for an operatorChain contained in a plugin that is not
 * installed.
 *
 * @author Simon Fischer
 *
 */
@DefinesOperator(module = "core", key = "dummy_composite", group = "deprecated", name = "Unknown Composite Operator", icon = "question.png")
public class CompositeDummyOperator extends OperatorChain {

    public static final String BAD_IMPORT_ACTION = "throws";

    public static final String PARAMETER_INSTALL_EXTENSION = "install_extension";
    public static final String PARAMETER_REPLACES = "replaces";
    public static final String PARAMETER_PARAMETERS = "parameters";
    public static final String PARAMETER_LISTS = "lists";
    public static final String PARAMETER_INNER_PIPELINE_OUTPUT_PORTS = "inner_pipeline_output_ports";

    public static final String PARAMETER_INNER_PIPELINE_INPUT_PORTS = "inner_pipeline_input_ports";

    public static final String PARAMETER_INPUT_PORT_REPLACEMENTS = "input_port_replacements";
    public static final String PARAMETER_OUTPUT_PORT_REPLACEMENTS = "output_port_replacements";

    private QuickFix installFix = null;

    private final OneToManyExtender inExtender = new OneToManyExtender("through", 2d, getInputPorts(),
            new OutputPorts[] { getSubprocess(0).getInnerSources() });
    private final ManyToOneExtender outExtender = new ManyToOneExtender("through", 2d, new InputPorts[] { getSubprocess(0).getInnerSinks() }, getOutputPorts());

    public CompositeDummyOperator() {
        super("Dummy Pipeline 1");
        inExtender.start();
        outExtender.start();
    }

    @Override
    public boolean areSubprocessesExtendable() {
        return true;
    }

    @Override
    public ExecutionUnit addSubprocess(int index) {
        ExecutionUnit newProcess = super.addSubprocess(index);
        inExtender.registerOutputPorts(newProcess.getInnerSources());
        outExtender.registerInputPorts(newProcess.getInnerSinks());
        normalizeSubprocessNames();
        return newProcess;
    }

    @Override
    public ExecutionUnit removeSubprocess(int index) {
        ExecutionUnit oldProcess = super.removeSubprocess(index);
        inExtender.unregisterOutputPorts(oldProcess.getInnerSources());
        outExtender.unregisterInputPorts(oldProcess.getInnerSinks());
        normalizeSubprocessNames();
        return oldProcess;
    }

    private void normalizeSubprocessNames() {
        for (int i = 0; i < getNumberOfSubprocesses(); i++) {
            getSubprocess(i).setName("Dummy Pipeline " + (i + 1));
        }
    }

    @Override
    protected ExecutionUnit createSubprocess(int index) {
        return new ExecutionUnit(this, "Dummy Pipeline");
    }

    @Override
    protected void performAdditionalChecks() {
        super.performAdditionalChecks();
        if (installFix != null) {
            addError(new SimpleProcessSetupError(Severity.ERROR, getPortOwner(), Collections.singletonList(installFix), "dummy_operator", getReplaces()));
        } else {
            addError(new SimpleProcessSetupError(Severity.ERROR, getPortOwner(), "dummy_operator", getReplaces()));
        }
    }

    @Override
    public void doWork() throws UserError {
        throw new UserError(this, 151, getName(), getReplaces());
    }

    public String getReplaces() {
        try {
            return getParameterAsString(PARAMETER_REPLACES);
        } catch (UndefinedParameterError e) {
            return "";
        }
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeString(PARAMETER_REPLACES, "The module and key of the missing operator."));
        ParameterType type = new ParameterTypeList(PARAMETER_PARAMETERS, "List of parameters for the missing operator.",
                new ParameterTypeString("former_parameter_key", "Specifies the key of the former parameter"),
                new ParameterTypeString("former_parameter_value", "Specifies the value of the former parameter"));
        type.setPrimary(true);
        types.add(type);
        type = new ParameterTypeList(PARAMETER_LISTS, "List of parameters for the missing operator.",
                new ParameterTypeString("former_list_key", "Specifies the key of the former list"),
                new ParameterTypeString("list_values", "Specifies the values of the former list"));
        types.add(type);

        types.add(new ParameterTypeList(PARAMETER_INPUT_PORT_REPLACEMENTS, "List of replaced input ports",
                new ParameterTypeString("port", "The name of the port of this dummy operator."),
                new ParameterTypeString("original port", "The name of the original port of the replaced operator.")));
        types.add(new ParameterTypeList(PARAMETER_OUTPUT_PORT_REPLACEMENTS, "List of replaced output ports",
                new ParameterTypeString("port", "The name of the port of this dummy operator."),
                new ParameterTypeString("original port", "The name of the original port of the replaced operator.")));
        types.add(new ParameterTypeList(PARAMETER_INNER_PIPELINE_INPUT_PORTS, "List of replaced inner pipeline input ports",
                new ParameterTypeString("port", "The name of the inner pipeline."),
                new ParameterTypeTupel("port_replacement_map", "The name of the original port and its replacement",
                        new ParameterTypeString("former_key", "The name of the original port"),
                        new ParameterTypeString("replacement", "The name of the replaced port"))));
        types.add(new ParameterTypeList(PARAMETER_INNER_PIPELINE_OUTPUT_PORTS, "List of replaced inner pipeline output ports",
                new ParameterTypeString("pipeline id", "The id of the inner pipeline."),
                new ParameterTypeTupel("new_to_former_port_map", "The name of the original port and its replacement",
                        new ParameterTypeString("former_key", "The name of the original port"),
                        new ParameterTypeString("replacement", "The name of the replaced port"))));
        return types;
    }
}
