/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator;

import java.util.LinkedHashSet;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;

/**
 * @author Sebastian Land
 * 
 */
public interface CapabilityProvider {

    /**
     * The property name for &quot;Indicates if only a warning should be made if learning
     * capabilities are not fulfilled (instead of breaking the process).&quot;
     */
    public static final String PROPERTY_SINGULARITY_GENERAL_CAPABILITIES_WARN = "singularity.general.capabilities.warn";

    /**
     * Checks for Learner capabilities. Should return true if the given capability is supported.
     */
    public boolean supportsCapability(OperatorCapability capability);

    /**
     * Checks if this learner can be used for the given example set, i.e. if it has sufficient
     * capabilities.
     */
    public static void checkCapabilities(CapabilityProvider capabilityProvider, ExampleSet exampleSet, Operator operator) throws OperatorException {
        // nominal attributes
        if (containsValueType(exampleSet, ValueType.NOMINAL) && !capabilityProvider.supportsCapability(OperatorCapability.NOMINAL_ATTRIBUTES)) {
            throw new UserError(operator, 501, operator.getName(), OperatorCapability.NOMINAL_ATTRIBUTES.getDescription());
        }

        // numerical attributes
        if ((containsValueType(exampleSet, ValueType.NUMERIC)) && !capabilityProvider.supportsCapability(OperatorCapability.NUMERICAL_ATTRIBUTES)) {
            throw new UserError(operator, 501, operator.getName(), OperatorCapability.NUMERICAL_ATTRIBUTES.getDescription());
        }

        // label
        Attribute labelAttribute = exampleSet.getAttributes().getLabel();
        if (labelAttribute != null) {
            if (labelAttribute.isNominal()) {
                int labelAttributeIndex = labelAttribute.getIndex();
                LinkedHashSet<String> uniqueLabelAttributeValues = new LinkedHashSet<>();
                exampleSet.streamRows().forEach(row -> {
                    uniqueLabelAttributeValues.add(exampleSet.getNominalValue(row, labelAttributeIndex));
                });
                if (uniqueLabelAttributeValues.size() == 1) {
                    if (!(capabilityProvider.supportsCapability(OperatorCapability.ONE_CLASS_LABEL))) {
                        throw new UserError(operator, 502, operator.getName());
                    }
                } else {
                    if (uniqueLabelAttributeValues.size() == 2) {
                        if (!capabilityProvider.supportsCapability(OperatorCapability.TWO_CLASS_LABEL)) {
                            throw new UserError(operator, 501, operator.getName(), OperatorCapability.TWO_CLASS_LABEL.getDescription());
                        }
                    } else {
                        if (!capabilityProvider.supportsCapability(OperatorCapability.MULTI_CLASS_LABEL)) {
                            throw new UserError(operator, 501, operator.getName(), OperatorCapability.MULTI_CLASS_LABEL.getDescription());
                        }
                    }
                }
            } else {
                if (labelAttribute.isNumerical() && !capabilityProvider.supportsCapability(OperatorCapability.NUMERICAL_LABEL)) {
                    throw new UserError(operator, 501, operator.getName(), OperatorCapability.NUMERICAL_LABEL.getDescription());
                }
            }
        } else {
            if (!(capabilityProvider.supportsCapability(OperatorCapability.NO_LABEL))) {
                throw new UserError(operator, 501, operator.getName(), OperatorCapability.NO_LABEL.getDescription());
            }
        }
    }

    private static boolean containsValueType(ExampleSet exampleSet, ValueType valueType) {
        for (Attribute attribute : exampleSet.getAttributes()) {
            if (attribute.getValueType().equals(valueType)) {
                return true;
            }
        }
        return false;
    }
}
