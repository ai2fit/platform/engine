/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.operator.error;


import java.util.MissingResourceException;

import com.owc.singularity.engine.exception.LocalizedException;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.tools.I18N;


/**
 * Exception class whose instances are thrown by instances of the class {@link Operator} or of one
 * of its subclasses.
 * <p>
 * This exception also allows using i18n keys, but it is not obligatory to do so. Currently only the
 * .short tag will be used, but this should be changed in the future, so adding the other
 * descriptions could be wise.
 * 
 * @author Ingo Mierswa, Simon Fischer, Sebastian Land
 */
public class OperatorException extends LocalizedException {

    private static final long serialVersionUID = 3626738574540303240L;

    private final String errorIdentifier;
    private final Object[] arguments;
    private transient Operator operator;

    public OperatorException(String message) {
        this(message, null);
    }

    public OperatorException(String message, Throwable cause) {
        this(null, message, cause);
    }

    public OperatorException(Operator operator, String message, Throwable cause) {
        super(() -> message, cause);
        this.errorIdentifier = null;
        this.arguments = new Object[0];
        this.operator = operator;
    }

    public OperatorException(String errorKey, Throwable cause, Object... arguments) {
        super(() -> getErrorMessage(errorKey, arguments), cause);
        this.errorIdentifier = errorKey;
        this.arguments = arguments;
    }

    /**
     * Sets the operator which caused this error
     *
     * @param operator
     *            the operator
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Returns the operator which caused this error
     *
     * @return the operator associated with this exception
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * The errorKey used to access the error.{errorKey}.short message of the getUserErrorMessage
     * bundle
     *
     * @return The error key which was used to create this exception, or {@code null}
     */
    public String getErrorIdentifier() {
        return errorIdentifier;
    }

    /**
     * @return the i18n arguments
     */
    protected Object[] getArguments() {
        return arguments;
    }

    /**
     * Returns the short error message from the {@link I18N#getUserErrorMessage UserErrorBundle}
     *
     * @param identifier
     *            part of the error.{identifier}.short i18n key
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string or "No message." if no i18n entry exists
     */
    public static String getErrorMessage(String identifier, Object[] arguments) {
        return getErrorMessage(identifier, "short", "No message for error." + identifier + ".short", arguments);
    }

    /**
     * This returns a resource message of the internationalized error messages identified by an id.
     *
     * @param id
     *            The identifier of the error. "error." will be automatically prepended-
     * @param key
     *            The part of the error description that should be shown.
     * @param defaultValue
     *            The default if no resource bundle is available.
     */
    public static String getResourceString(String id, String key, String defaultValue) {
        try {
            return I18N.getUserErrorMessage("error." + id + "." + key);
        } catch (MissingResourceException | NullPointerException e) {
            return defaultValue;
        }
    }

    /**
     * Returns the formatted message from the
     * {@link I18N#getUserErrorMessageOrNull(String, Object...)} UserError} bundle key:
     * error.{identifier}.{type}
     *
     * @param identifier
     *            the error identifier
     * @param type
     *            the type, either "name", "short" or "long"
     * @param defaultMessage
     *            the message if no entry for the given key exists
     * @param arguments
     *            i18n arguments
     * @return the message or the defaultMessage
     */
    protected static String getErrorMessage(String identifier, String type, String defaultMessage, Object... arguments) {
        if (identifier == null)
            return defaultMessage;
        String message = I18N.getUserErrorMessageOrNull("error." + identifier + "." + type, arguments);
        return message == null ? defaultMessage : message;
    }
}
