package com.owc.singularity.engine;

public enum EngineExitMode {

    NORMAL(0), ERROR(1), RELAUNCH(2);

    private final int exitCode;

    EngineExitMode(int exitCode) {
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }
}
