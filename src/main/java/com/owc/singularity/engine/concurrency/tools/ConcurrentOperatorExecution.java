package com.owc.singularity.engine.concurrency.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionPipeline;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;

/**
 * This class contains information about an operator that has been put into the background execution
 * queue. It is used to have multiple versions of the same operator entry in the queue.
 * <p>
 * Note that this part of the API is only temporary and might be removed in future versions again.
 * </p>
 *
 * @author Sebastian Land
 */
public class ConcurrentOperatorExecution implements ConcurrentExecution {

    public static class OperatorBackgroundTask {

        public int applyCount;
        public ConcurrentExecutionPipeline pipeline;
        public Operator operator;
        private final ConcurrentPipelineExecutionState state;

        public OperatorBackgroundTask(int applyCount, ConcurrentExecutionPipeline pipeline, Operator operator) {
            this.applyCount = applyCount;
            this.pipeline = pipeline;
            this.operator = operator;
            this.state = new ConcurrentPipelineExecutionState(pipeline);
        }

        public ConcurrentPipelineExecutionState getState() {
            return state;
        }
    }

    private int totalNumberOfTasks;
    private final AbstractPipeline parentPipeline;
    private final String parentOperatorName;
    private final List<OperatorBackgroundTask> tasks = new LinkedList<>();

    public ConcurrentOperatorExecution(AbstractPipeline parentPipeline, String parentOperatorName) {
        this.parentPipeline = parentPipeline;
        this.parentOperatorName = parentOperatorName;
    }

    public void addTask(int applyCount, ConcurrentExecutionPipeline process, Operator operator) {
        totalNumberOfTasks++;
        OperatorBackgroundTask application = new OperatorBackgroundTask(applyCount, process, operator);
        synchronized (tasks) {
            tasks.add(application);
        }
    }

    public void removeTask(int applyCount) {
        Iterator<OperatorBackgroundTask> iterator = tasks.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().applyCount == applyCount) {
                iterator.remove();
                break;
            }
        }
    }

    public int getNumberOfTasks() {
        return tasks.size();
    }

    public List<OperatorBackgroundTask> getTasks() {
        synchronized (tasks) {
            return new ArrayList<>(tasks);
        }
    }

    public AbstractPipeline getParentPipeline() {
        return parentPipeline;
    }

    @Override
    public String getName() {
        return parentOperatorName + "[ " + (totalNumberOfTasks - tasks.size()) + " of " + totalNumberOfTasks + " Tasks]";
    }

}
