package com.owc.singularity.engine.concurrency.tools;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * The class provides wrappers for code blocks that get executed asynchronously. The idea is to
 * enable async code to inherit the {@link org.apache.logging.log4j.ThreadContext} of the calling
 * thread which allows related threads to share information such as execution id and pipeline path.
 * 
 * @author Hatem Hamad
 */
public class TemporalThreadContext {

    public static Runnable wrap(Runnable runnable) {
        return new RunnableWithTemporalThreadContext(runnable);
    }

    public static <V> Callable<V> wrap(Callable<V> callable) {
        return new CallableWithTemporalThreadContext<>(callable);
    }

    public static <V> Supplier<V> wrap(Supplier<V> supplier) {
        return new SupplierWithTemporalThreadContext<>(supplier);
    }

    public static <V> Consumer<V> wrap(Consumer<V> consumer) {
        return new ConsumerWithTemporalThreadContext<>(consumer);
    }
}
