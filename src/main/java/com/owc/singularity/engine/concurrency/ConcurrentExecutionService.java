/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.concurrency;


import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;

import com.owc.singularity.engine.concurrency.tools.ConcurrencyExecutionServiceListener;
import com.owc.singularity.engine.concurrency.tools.ConcurrentExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentOperatorExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.IOContainer;
import com.owc.singularity.engine.tools.container.Pair;


/**
 * Interface for the concurrent pipeline and operator execution service.
 * <p>
 * Note that this part of the API is only temporary and might be removed in future versions again.
 * </p>
 *
 * @author Sebastian Land
 */
public abstract class ConcurrentExecutionService {

    protected List<ConcurrencyExecutionServiceListener> listeners = new LinkedList<>();
    protected List<ConcurrentExecution> tasks = new LinkedList<>();
    protected Map<Pair<AbstractPipeline, String>, ConcurrentOperatorExecution> operatorTasksMap = new HashMap<>();

    /**
     * Calculates the recommended batch size for parallel operators. Use when deciding how many
     * tasks to submit to
     * {@link ConcurrentExecutionService#submitOperatorTasks(Operator, java.util.List)}
     * simultaneously.
     *
     * @return the recommended batch size.
     * @since 7.5
     * 
     */
    public abstract int getRecommendedConcurrency();

    /**
     * Subclasses have to take care, that the pipeline is actually executed in the concurrency
     * framework present. The returning object is used for managing it.
     * 
     * @param pipeline
     * @param container
     * @param variableSettings
     * @return
     * @throws UserError
     */
    public abstract ConcurrentPipelineExecution submitPipelineExecutionTask(AbstractPipeline pipeline, IOContainer container,
            Map<String, String> variableSettings) throws UserError;

    /**
     * This method executes the given pipeline in the background. This method does
     * <strong>not</strong> block.
     * </p>
     *
     * @param pipeline
     *            The pipeline to be executed in the background
     */
    public void executePipelineConcurrently(AbstractPipeline pipeline) throws UserError {
        executePipelineConcurrently(pipeline, new IOContainer(), Collections.emptyMap());
    }

    /**
     * This method executes a pipeline from the repository under certain input and variables. It can
     * be used to run entire pipelines in the background from a pipeline.
     * <p>
     * This method does <strong>not</strong> block.
     * </p>
     *
     * @param pipeline
     * @param container
     * @param variableSettings
     */
    public void executePipelineConcurrently(AbstractPipeline pipeline, IOContainer container, Map<String, String> variableSettings) throws UserError {
        ConcurrentPipelineExecution execution = submitPipelineExecutionTask(pipeline, container, variableSettings);
        tasks.add(execution);
        listeners.forEach(l -> l.pipelineAdded(execution));
    }

    /**
     * This method will prepare an operator with sub-pipelines, that should be performed in parallel
     * as specified by the given {@link Callable}. The operator will be added to an otherwise empty
     * {@link ConcurrentExecutionPipeline}, that will capture all the side effects. If
     * synchronizeSideEffects is true, these side effects will be merged back into the
     * parentPipeline.
     *
     * @param <V>
     *
     * @param parentPipeline
     *            The process which contains the parallelized operator
     * @param clonedOperator
     *            A clone of this parallelized operator
     * @param applyCount
     *            The apply count for this current parallelized version
     * @param synchronizeSideEffects
     *            Whether side effects should be merged, usually only during last iteration
     * @param task
     *            The actual sub-task of the operator to execute
     * @return
     */
    public <V> Callable<V> prepareOperatorTask(AbstractPipeline parentPipeline, Operator clonedOperator, int applyCount, boolean synchronizeSideEffects,
            Callable<V> task) {
        // wrapping single operator into process so that we can capture side effects
        final ConcurrentExecutionPipeline pipeline = new ConcurrentExecutionPipeline(parentPipeline, clonedOperator, synchronizeSideEffects);

        return new Callable<V>() {

            @Override
            public V call() throws Exception {
                try {
                    V v = task.call();

                    // synchronize
                    pipeline.synchronizeSideEffects();
                    return v;
                } catch (ProcessStoppedException e) {
                    throw e;
                } finally {
                    removeOperatorTask(parentPipeline, clonedOperator.getName(), applyCount);
                }
            }
        };

    }

    /**
     * Executes the tasks for the given operator. This can also be called with a huge number of
     * tasks, the tasks will be automatically bundled.
     *
     * @param operator
     *            the operator which executes the given tasks
     * @param tasks
     *            the tasks which should be executed
     * @return the execution result
     * @throws OperatorException
     *             if something goes wrong during the task execution
     */
    public abstract <T> List<T> submitOperatorTasks(Operator operator, List<Callable<T>> tasks) throws OperatorException;

    /**
     * Submits a single task for the given operator for execution. Will cause problems with a huge
     * number of tasks. In this case, consider collecting them and rather use
     * {@link #submitOperatorTasks(Operator, List)}, which will bundle them first.
     *
     * @param operator
     *            the operator which executes the given task
     * @param task
     *            the task which should be executed
     * @return a future which can be used to later access the result
     * @since 7.3
     */
    public abstract <T> ForkJoinTask<T> submitOperatorTask(Operator operator, Callable<T> task);

    /**
     * Waits for the results of the given {@link Future}s, returning their results upon completion.
     * <p>
     * The method blocks until all {@code Future}s have their results.
     * <p>
     * This is an advanced method which should only be used if you know what you are doing. Usage of
     * {@link #call(List)} is recommended instead.
     *
     * @param <T>
     *            the type of the values returned from the futures
     * @param operator
     *            the operator which executes the given tasks
     * @param tasks
     *            the {@link Future}s for which their results should be collected
     * @return a list containing the results of the futures
     * @throws OperatorException
     *             if something goes wrong during the task execution
     * @since 7.3
     */
    public <T> List<T> collectResults(Operator operator, List<ForkJoinTask<T>> tasks) throws OperatorException {
        List<T> results = new LinkedList<T>();
        for (ForkJoinTask<T> task : tasks) {
            try {
                results.add(task.join());
                operator.checkForStop();
            } catch (CancellationException e) {
                throw new ProcessStoppedException(operator);
            } catch (RuntimeException | Error e) {
                Throwable cause = e.getCause();
                if (cause instanceof OperatorException)
                    throw (OperatorException) cause;
                else
                    throw new OperatorException("Unexpected error during parallel computation.", e);
            }
        }
        return results;
    }

    /**
     * This method removes the task from the list of currently executed things. If all tasks are
     * removed the corresponding execution element will be removed as well.
     *
     * @param parentProcess
     * @param operatorName
     * @param applyCount
     */
    private void removeOperatorTask(AbstractPipeline parentProcess, String operatorName, int applyCount) {
        Pair<AbstractPipeline, String> identifier = new Pair<>(parentProcess, operatorName);
        ConcurrentOperatorExecution execution = operatorTasksMap.get(identifier);
        if (execution != null) {
            execution.removeTask(applyCount);
            if (execution.getNumberOfTasks() == 0) {
                tasks.remove(execution);
                operatorTasksMap.remove(identifier);
                listeners.forEach(l -> l.pipelineRemoved(execution));
            }
        }
    }

    /**
     * This returns the list of background executions.
     *
     * @return unmodifiable version of the list
     */
    public List<ConcurrentExecution> getExecutions() {
        return Collections.unmodifiableList(tasks);
    }

    /**
     * This stops the execution of a given process execution in the background.
     *
     * @param execution
     *            the execution to stop
     */
    public abstract void stopPipelineExecution(ConcurrentPipelineExecution execution);

    /**
     * This drops the execution from the manageable data structures. Please notice, that this will
     * not stop nor release any resources. To stop execution, you will need to call stopExecution
     * before.
     *
     * @param execution
     *            the not running execution to remove
     */
    public void removePipelineExecution(ConcurrentPipelineExecution execution) {
        if (!execution.getBackgroundExecutionState().isRunning()) {
            tasks.remove(execution);
            listeners.forEach(l -> l.pipelineRemoved(execution));
        }
    }

    /**
     * The provided listener will be notified if any background execution is added or removed
     *
     * @param listener
     *            This listener is being added to the listener pool
     */
    public void addListener(ConcurrencyExecutionServiceListener listener) {
        listeners.add(listener);
    }

}
