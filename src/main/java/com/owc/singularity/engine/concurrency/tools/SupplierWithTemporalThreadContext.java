package com.owc.singularity.engine.concurrency.tools;

import java.util.Map;
import java.util.function.Supplier;

import org.apache.logging.log4j.ThreadContext;

public class SupplierWithTemporalThreadContext<V> implements Supplier<V> {

    private final Map<String, String> context;
    private final Supplier<V> supplier;


    public SupplierWithTemporalThreadContext(Supplier<V> supplier) {
        this(ThreadContext.getContext(), supplier);
    }

    public SupplierWithTemporalThreadContext(Map<String, String> context, Supplier<V> supplier) {
        this.context = context;
        this.supplier = supplier;
    }

    @Override
    public V get() {
        Map<String, String> previous = ThreadContext.getContext();
        if (context == null) {
            ThreadContext.clearMap();
        } else {
            ThreadContext.putAll(context);
        }
        try {
            return supplier.get();
        } finally {
            if (previous == null) {
                ThreadContext.clearMap();
            } else {
                ThreadContext.putAll(previous);
            }
        }
    }
}
