package com.owc.singularity.engine.concurrency;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;

import org.apache.logging.log4j.ThreadContext;

import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecutionState;
import com.owc.singularity.engine.concurrency.tools.TemporalThreadContext;
import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.IOContainer;
import com.owc.singularity.repository.RepositoryPath;

/**
 * This is the default concurrent execution within the single local machine.
 * 
 * @author Sebastian Land
 *
 */
public class LocalConcurrencyExecutionService extends ConcurrentExecutionService {

    @Override
    public int getRecommendedConcurrency() {
        return ForkJoinPool.commonPool().getParallelism();
    }


    @Override
    public <T> List<T> submitOperatorTasks(Operator operator, List<Callable<T>> tasks) throws OperatorException {
        List<Callable<T>> wrappedTasks = tasks.stream().map(TemporalThreadContext::wrap).toList();
        ForkJoinPool pool = getPool();
        List<Future<T>> futures = pool.invokeAll(wrappedTasks);

        List<T> results = new LinkedList<>();
        for (Future<T> future : futures) {
            try {
                while (true) {
                    operator.checkForStop();
                    try {
                        results.add(future.get());
                        break;
                    } catch (InterruptedException e) {
                    }
                }
            } catch (ExecutionException e) {
                AbstractOperator.processThrowable(e);
            }
        }
        return results;
    }


    protected ForkJoinPool getPool() {
        ForkJoinPool pool = ForkJoinTask.getPool();
        if (pool == null)
            pool = ForkJoinPool.commonPool();
        return pool;
    }

    @Override
    public <T> ForkJoinTask<T> submitOperatorTask(Operator operator, Callable<T> task) {
        return new RecursiveTask<T>() {

            private static final long serialVersionUID = 4173946438608599473L;

            @Override
            protected T compute() {
                try {
                    operator.checkForStop();
                    return TemporalThreadContext.wrap(task).call();
                } catch (Exception e) {
                    throw new CompletionException(e);
                }
            }
        }.fork();
    }


    @Override
    public ConcurrentPipelineExecution submitPipelineExecutionTask(AbstractPipeline originalPipeline, IOContainer container,
            Map<String, String> variableSettings) throws UserError {
        // clone pipeline
        final AbstractPipeline pipeline = originalPipeline.clone();
        // avoids pausing
        pipeline.registerLifeCycleEventListener(new PipelineLifeCycleEventListener() {

            @Override
            public String getIdentifier() {
                return "Concurrent Execution";
            }

            @Override
            public boolean onBeforePipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
                return false;
            }
        });

        pipeline.setPath(originalPipeline.getPath());
        // remembering state of pipeline
        ConcurrentPipelineExecutionState pipelineState = new ConcurrentPipelineExecutionState(pipeline);

        ForkJoinTask<List<IOObject>> futureResults = ForkJoinTask.adapt(TemporalThreadContext.wrap((Callable<List<IOObject>>) () -> {
            try {
                RepositoryPath pipelinePath = pipeline.getPath();
                ThreadContext.put("process.path", pipelinePath == null ? String.valueOf(pipeline.hashCode()) : pipelinePath.toShortString(10));
                ThreadContext.put("process.executionId", UUID.randomUUID().toString().substring(0, 6));

                return pipeline.debug(true);
            } catch (OperatorException e) {
                throw new RuntimeException(e);
            } finally {
                ThreadContext.remove("process.executionId");
                ThreadContext.remove("process.path");
            }
        }));
        getPool().submit(futureResults);
        pipelineState.setResults(futureResults);
        return new ConcurrentPipelineExecution(originalPipeline, pipeline, pipelineState);
    }


    @Override
    public void stopPipelineExecution(ConcurrentPipelineExecution execution) {
        execution.getBackgroundExecutionState().setStopped();
        AbstractPipeline process = execution.getPipeline();
        if (process != null) {
            process.stop();
        }
    }
}
