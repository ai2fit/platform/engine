package com.owc.singularity.engine.concurrency;

import java.util.Iterator;

import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.operator.AnalysisRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.DevelopmentExecutionContext;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.engine.visualization.datatable.DataTableRow;
import com.owc.singularity.engine.visualization.datatable.SimpleDataTableRow;
import com.owc.singularity.repository.RepositoryPath;

/**
 * This pipeline is solely for containing a single operator that is executed in parallel. We need it
 * in order to capture side effects. However, the pipeline itself is never executed.
 */
public class ConcurrentExecutionPipeline extends AbstractPipeline {

    private AbstractPipeline parent;
    private boolean synchronizeStatelySideEffects;

    public ConcurrentExecutionPipeline(final AbstractPipeline parent, final Operator clonedOperator, boolean synchronizeStatelySideEffects) {
        super(new AnalysisRootOperator());
        this.parent = parent;

        this.synchronizeStatelySideEffects = synchronizeStatelySideEffects;

        copyVariables(parent, this);

        getRootOperator().getSubprocess(0).addOperator(clonedOperator, synchronizeStatelySideEffects);
        registerLifeCycleEventListener(new PipelineLifeCycleEventListener() {

            @Override
            public String getIdentifier() {
                return "parallel-execution";
            }

            @Override
            public boolean onBeforePipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
                getLogger().warn("Ignoring breakpoint in parallel execution...");
                return false;
            }
        });
    }

    /**
     * This method copies the variables from one pipeline to another while holding a lock on the
     * target's variable handler, so that no two threads can intermix the results.
     *
     * @param source
     * @param target
     */
    private void copyVariables(AbstractPipeline source, AbstractPipeline target) {
        synchronized (target.getVariableHandler()) {
            VariableHandler sourceHandler = source.getVariableHandler();
            VariableHandler targetHandler = target.getVariableHandler();

            Iterator<String> variableIterator = sourceHandler.getDefinedVariableNames();
            while (variableIterator.hasNext()) {
                String variableName = variableIterator.next();
                targetHandler.addVariable(variableName, sourceHandler.getVariable(variableName));
            }
        }
    }

    @Override
    public PipelineExecutionContext getExecutionContext() {
        return parent.getExecutionContext();
    }

    public void synchronizeSideEffects() {
        if (synchronizeStatelySideEffects) {
            copyVariables(this, parent);
        }
        // data tables are collected, so we always need to synchronize them
        copyDataTables(this, parent);
    }

    /**
     * This method copies the datatables or their rows if data tables are already present in the
     * target pipeline to the target pipeline. It holds a lock on the target pipeline
     *
     * @param source
     * @param target
     */
    private void copyDataTables(AbstractPipeline source, AbstractPipeline target) {
        // first create data tables if not present yet
        synchronized (target) {
            for (DataTable sourceTable : source.getDataTables()) {
                DataTable targetTable = target.getDataTable(sourceTable.getName());
                if (targetTable == null) {
                    target.addDataTable(sourceTable);
                } else {
                    // we define a bit mask for speeding up computation later
                    boolean[] isNominalColumn = new boolean[targetTable.getColumnNumber()];
                    for (int i = 0; i < targetTable.getColumnNumber(); i++) {
                        isNominalColumn[i] = targetTable.isNominal(i);
                    }
                    int rowIndex = 0;
                    for (DataTableRow sourceRow : sourceTable) {
                        double[] targetRowValues = new double[sourceRow.getNumberOfValues()];
                        DataTableRow targetRow = new SimpleDataTableRow(targetRowValues, sourceRow.getId());

                        // now set values of row correctly.
                        for (int i = 0; i < isNominalColumn.length; i++) {
                            if (isNominalColumn[i]) {
                                // for nominal values we need to remap the value
                                String value = sourceTable.getCell(rowIndex, i);
                                int nominalMapIndex = targetTable.mapString(i, value);
                                targetRowValues[i] = nominalMapIndex;
                            } else {
                                targetRowValues[i] = sourceRow.getValue(i);
                            }
                        }
                        targetTable.add(targetRow);
                        rowIndex++;
                    }
                }
                ;
            }
        }
    }


    /**
     * Things that will be directly forwarded to the parent operator
     */
    @Override
    public RepositoryPath getPath() {
        return parent.getPath();
    }

    @Override
    public Logger getLogger() {
        return parent.getLogger();
    }

    @Override
    public boolean shouldPause() {
        return parent.shouldPause();
    }

    @Override
    public boolean shouldStop() {
        return parent.shouldStop();
    }

    @Override
    public AbstractPipeline clone() {
        throw new UnsupportedOperationException();
    }

    @Override
    public PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) {
        // this won't ever be called as we directly execute the inner operator
        throw new UnsupportedOperationException();
    }
}
