package com.owc.singularity.engine.concurrency.tools;

import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.ThreadContext;

public class CallableWithTemporalThreadContext<V> implements Callable<V> {

    private final Map<String, String> context;
    private final Callable<V> callable;

    public CallableWithTemporalThreadContext(Callable<V> callable) {
        this(ThreadContext.getContext(), callable);
    }

    public CallableWithTemporalThreadContext(Map<String, String> context, Callable<V> callable) {
        this.context = context;
        this.callable = callable;
    }

    @Override
    public V call() throws Exception {
        Map<String, String> previous = ThreadContext.getContext();
        if (context == null) {
            ThreadContext.clearMap();
        } else {
            ThreadContext.putAll(context);
        }
        try {
            return callable.call();
        } finally {
            if (previous == null) {
                ThreadContext.clearMap();
            } else {
                ThreadContext.putAll(previous);
            }
        }
    }
}
