package com.owc.singularity.engine.concurrency.tools;

import java.util.Map;
import java.util.function.Consumer;

import org.apache.logging.log4j.ThreadContext;

public class ConsumerWithTemporalThreadContext<V> implements Consumer<V> {

    private final Map<String, String> context;
    private final Consumer<V> consumer;


    public ConsumerWithTemporalThreadContext(Consumer<V> consumer) {
        this(ThreadContext.getContext(), consumer);
    }

    public ConsumerWithTemporalThreadContext(Map<String, String> context, Consumer<V> consumer) {
        this.context = context;
        this.consumer = consumer;
    }

    @Override
    public void accept(V value) {
        Map<String, String> previous = ThreadContext.getContext();
        if (context == null) {
            ThreadContext.clearMap();
        } else {
            ThreadContext.putAll(context);
        }
        try {
            consumer.accept(value);
        } finally {
            if (previous == null) {
                ThreadContext.clearMap();
            } else {
                ThreadContext.putAll(previous);
            }
        }
    }
}
