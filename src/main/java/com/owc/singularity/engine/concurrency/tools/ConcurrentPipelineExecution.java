/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.concurrency.tools;


import java.sql.Date;
import java.text.DateFormat;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;


/**
 * This class contains information about a pipeline that has been put into the background execution
 * queue. It is used to have multiple versions of the same pipeline entry in the queue.
 * <p>
 * Note that this part of the API is only temporary and might be removed in future versions again.
 * </p>
 *
 * @author Sebastian Land
 * @since 7.4
 */
public class ConcurrentPipelineExecution implements ConcurrentExecution {

    private final AbstractPipeline executedPipeline;
    private final AbstractPipeline originalPipeline;
    private final ConcurrentPipelineExecutionState state;
    private final long scheduledTime;

    /**
     * Creates a new ProcessBackgroundExcecution object
     *
     * @param originalPipeline
     *            The original process
     * @param executedPipeline
     *            The executed process
     * @param processState
     *            The current state of the executed process
     */
    public ConcurrentPipelineExecution(AbstractPipeline originalPipeline, AbstractPipeline executedPipeline, ConcurrentPipelineExecutionState processState) {
        this.originalPipeline = originalPipeline;
        this.executedPipeline = executedPipeline;
        this.state = processState;
        this.scheduledTime = System.currentTimeMillis();
    }

    public RepositoryPath getPath() {
        return getOriginalPipeline().getPath();
    }

    public long getScheduledTime() {
        return scheduledTime;
    }

    public ConcurrentPipelineExecutionState getBackgroundExecutionState() {
        return state;
    }

    /**
     * Returns the executed AbstractPipeline
     *
     * <p>
     * Warning: This AbstractPipeline might not be equal to the original AbstractPipeline, use
     * {@code getOriginalProcess()} or {@code getProcessWithoutLocation()} to display the
     * AbstractPipeline to the user.
     * </p>
     *
     * @return
     */
    public AbstractPipeline getPipeline() {
        return executedPipeline;
    }

    /**
     * Returns the original pipeline
     *
     * @return
     */
    public AbstractPipeline getOriginalPipeline() {
        return originalPipeline;
    }

    @Override
    public String getName() {
        RepositoryPath path = getPath();
        return (path != null ? path.toAbsolutePath().toString() : "Unsaved pipeline") + " [" + DateFormat.getDateTimeInstance().format(new Date(scheduledTime))
                + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getPath() == null ? 0 : getPath().hashCode());
        result = prime * result + (int) (scheduledTime ^ scheduledTime >>> 32);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConcurrentPipelineExecution other = (ConcurrentPipelineExecution) obj;
        if (getPath() == null) {
            if (other.getPath() != null) {
                return false;
            }
        } else if (!getPath().equals(other.getPath())) {
            return false;
        }
        return scheduledTime == other.scheduledTime;
    }

}
