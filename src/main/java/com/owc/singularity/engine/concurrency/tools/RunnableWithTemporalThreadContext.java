package com.owc.singularity.engine.concurrency.tools;

import java.util.Map;

import org.apache.logging.log4j.ThreadContext;

public class RunnableWithTemporalThreadContext implements Runnable {

    private final Map<String, String> context;
    private final Runnable runnable;

    public RunnableWithTemporalThreadContext(Runnable runnable) {
        this(ThreadContext.getContext(), runnable);
    }

    public RunnableWithTemporalThreadContext(Map<String, String> context, Runnable runnable) {
        this.context = context;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        Map<String, String> previous = ThreadContext.getContext();
        if (context == null) {
            ThreadContext.clearMap();
        } else {
            ThreadContext.putAll(context);
        }
        try {
            runnable.run();
        } finally {
            if (previous == null) {
                ThreadContext.clearMap();
            } else {
                ThreadContext.putAll(previous);
            }
        }
    }
}
