/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.concurrency.tools;

/**
 * Interface which defines a listener which is used for background process execution.
 * <p>
 * Note that this part of the API is only temporary and might be removed in future versions again.
 * </p>
 *
 * @author Sebastian Land
 * @since 7.4
 */
public interface ConcurrencyExecutionServiceListener {

    /**
     * This Method is being called every time a ProcessBackgroundExecution is removed. The
     * respective ProcessBackgroundExecution is returned.
     */
    void pipelineRemoved(ConcurrentExecution execution);

    /**
     * This Method is being called every time a ProcessBackgroundExecution is added. The respective
     * ProcessBackgroundExecution is returned.
     */
    void pipelineAdded(ConcurrentExecution execution);

}
