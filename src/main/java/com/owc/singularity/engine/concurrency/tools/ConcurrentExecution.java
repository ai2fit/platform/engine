/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.concurrency.tools;

/**
 * A BackgroundExecution can either be a pipeline ({@link ConcurrentPipelineExecution}) or an
 * operator ({@link ConcurrentOperatorExecution}) which can be executed in the background.
 * <p>
 * Note that this part of the API is only temporary and might be removed in future versions again.
 * </p>
 *
 * @author Sebastian Land
 * @since 7.4
 */
public interface ConcurrentExecution {

    /**
     * This returns the name of the Background Execution Unit
     *
     * @return A human-readable name of the unit
     */
    String getName();

}
