package com.owc.singularity.engine;

/**
 * A set of default values shared globally
 *
 * @author Hatem Hamad
 */
public class GlobalDefaults {

    public static final String SYSTEM_ENCODING_NAME = "SYSTEM";
}
