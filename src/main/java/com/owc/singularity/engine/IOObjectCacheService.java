package com.owc.singularity.engine;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.data.exampleset.DirectMemoryStatistics;
import com.owc.singularity.engine.tools.ReferenceCache;

/**
 * This service allows to store objects that are potentially dropped when more memory is needed.
 */
public class IOObjectCacheService {

    private final static ReferenceCache<IOObject> cache = new ReferenceCache<IOObject>("ioobjects", 0.6f, DirectMemoryStatistics.getTargetSizeOfDirectMemory());

    public static ReferenceCache<IOObject>.Reference cache(IOObject object) {
        return cache.cache(object);
    }


}
