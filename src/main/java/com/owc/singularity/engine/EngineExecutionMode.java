package com.owc.singularity.engine;

/**
 * Indicates how SingularityEngine is being executed.
 * 
 * @implNote (extensions-breaker) <a href=
 *           "https://gitlab.oldworldcomputing.com/singularity/engine/-/wikis/Extensions-Breaker">read
 *           more</a>
 */
public enum EngineExecutionMode {

    /**
     * RM is running as an applet inside a browser.
     */
    APPLET(false, true, true, false),
    /**
     * RM is running inside an application server.
     */
    APPSERVER(true, false, false, false),
    /**
     * RM is executed using command line
     */
    COMMAND_LINE(true, true, false, true),
    /**
     * RM is embedded into an applet.
     */
    EMBEDDED_AS_APPLET(false, false, false, false),
    /**
     * RM is embedded into another program.
     */
    EMBEDDED_WITHOUT_UI(true, true, false, false),
    /**
     * RM is embedded into another program.
     */
    EMBEDDED_WITH_UI(false, true, false, false),
    /**
     * RM is running inside the Job Container
     **/
    JOB_CONTAINER(true, false, false, false),
    /**
     * RM is running inside the Scoring Agent
     **/
    SCORING_AGENT(true, false, false, false),
    /**
     * We are executing unit tests.
     */
    TEST(true, false, false, true),
    /**
     * RM is executed using a Graphical User Interface.
     */
    UI(false, true, true, true),
    /**
     * It is unknown how RM was invoked.
     */
    UNKNOWN(true, false, false, true),
    /**
     * RM is running within Java Web Start.
     */
    WEBSTART(false, true, true, true);

    private final boolean isHeadless;
    private final boolean canAccessFilesystem;
    private final boolean hasMainFrame;
    private final boolean loadManagedExtensions;

    EngineExecutionMode(final boolean isHeadless, final boolean canAccessFilesystem, final boolean hasMainFrame, final boolean loadManagedExtensions) {
        this.isHeadless = isHeadless;
        this.canAccessFilesystem = canAccessFilesystem;
        this.hasMainFrame = hasMainFrame;
        this.loadManagedExtensions = loadManagedExtensions;
    }

    public boolean isHeadless() {
        return isHeadless;
    }

    public boolean canAccessFilesystem() {
        return canAccessFilesystem;
    }

    public boolean hasMainFrame() {
        return hasMainFrame;
    }

    public boolean isLoadingManagedExtensions() {
        return loadManagedExtensions;
    }
}
