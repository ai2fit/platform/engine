package com.owc.singularity.engine.i18n;


import java.util.Locale;

import com.owc.singularity.engine.ResourcesProvider;
import com.owc.singularity.engine.tools.ExtensibleResourceBundle;

/**
 * A provider that is responsible for providing {@link java.util.ResourceBundle}s for the
 * {@link com.owc.singularity.engine.tools.I18N I18N Service}.
 *
 * @author Hatem Hamad
 */
public interface I18NResourcesProvider extends ResourcesProvider {

    ExtensibleResourceBundle getErrorBundle();

    ExtensibleResourceBundle getGUIBundle();

    ExtensibleResourceBundle getUserErrorMessagesBundle();

    ExtensibleResourceBundle getSettingsBundle();

    ExtensibleResourceBundle getLogMessagesBundle();

    Locale getOriginalLocale();
}
