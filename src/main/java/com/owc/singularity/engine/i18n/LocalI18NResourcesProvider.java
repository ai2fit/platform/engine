package com.owc.singularity.engine.i18n;


import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import javax.swing.JComponent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.ExtensibleResourceBundle;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Tools;

/**
 * A local implementation of the {@link I18NResourcesProvider}. This service loads the required
 * {@link ResourceBundle}s using the {@link ExtensibleResourceBundle}.
 *
 * @author Hatem Hamad
 */
public class LocalI18NResourcesProvider implements I18NResourcesProvider {

    private static final Logger log = LogManager.getLogger(LocalI18NResourcesProvider.class);
    private boolean initialized = false;
    private final Locale systemLocale;
    private ExtensibleResourceBundle userErrorBundle;
    private ExtensibleResourceBundle errorBundle;
    private ExtensibleResourceBundle guiBundle;
    private ExtensibleResourceBundle settingsBundle;
    private ExtensibleResourceBundle logMessagesBundle;

    public LocalI18NResourcesProvider() {
        systemLocale = Locale.getDefault();
    }

    @Override
    public synchronized boolean isInitialized() {
        return initialized;
    }

    @Override
    public synchronized void initialize() {
        if (isInitialized())
            return;

        PropertyService.init();

        String localeLanguage = PropertyService.getParameterValue(EngineProperties.GENERAL_LOCALE_LANGUAGE);
        Locale locale = Locale.forLanguageTag(Objects.toString(localeLanguage, ""));
        TimeZone timezoneId = Tools.getPreferredTimeZone();
        initialize(locale, timezoneId);
    }

    public synchronized void initialize(Locale locale, final TimeZone timeZone) {
        if (isInitialized())
            return;

        if (!locale.getLanguage().isEmpty()) {
            Locale.setDefault(locale);
            log.info("Set locale to {}", locale);
        } else {
            locale = Locale.getDefault();
            log.info("No locale set. Taking default system locale {}", locale);
        }
        JComponent.setDefaultLocale(locale);
        if (timeZone != null && timeZone != TimeZone.getDefault()) {
            TimeZone.setDefault(timeZone);
            log.info("Set TimeZone to {}", locale);
        } else {
            log.info("Default system TimeZone is {}", locale);
        }

        userErrorBundle = new ExtensibleResourceBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.UserErrorMessages", locale,
                I18N.class.getClassLoader(), MultipleResourcesClassPathControl.INSTANCE));
        errorBundle = new ExtensibleResourceBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.Errors", locale, I18N.class.getClassLoader(),
                MultipleResourcesClassPathControl.INSTANCE));
        guiBundle = new ExtensibleResourceBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.GUI", locale, I18N.class.getClassLoader(),
                MultipleResourcesClassPathControl.INSTANCE));
        settingsBundle = new ExtensibleResourceBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.Settings", locale,
                I18N.class.getClassLoader(), MultipleResourcesClassPathControl.INSTANCE));
        logMessagesBundle = new ExtensibleResourceBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.LogMessages", locale,
                I18N.class.getClassLoader(), MultipleResourcesClassPathControl.INSTANCE));

        ResourceBundle plotterBundle = ResourceBundle.getBundle("com.owc.singularity.resources.i18n.PlotterMessages", locale, I18N.class.getClassLoader(),
                MultipleResourcesClassPathControl.INSTANCE);

        guiBundle.addResourceBundle(plotterBundle);
        initialized = true;
    }

    /**
     * Returns the resource bundle for error messages and quick fixes.
     */
    @Override
    public ExtensibleResourceBundle getErrorBundle() {
        return errorBundle;
    }

    @Override
    public ExtensibleResourceBundle getGUIBundle() {
        return guiBundle;
    }

    @Override
    public ExtensibleResourceBundle getUserErrorMessagesBundle() {
        return userErrorBundle;
    }

    @Override
    public ExtensibleResourceBundle getSettingsBundle() {
        return settingsBundle;
    }

    @Override
    public ExtensibleResourceBundle getLogMessagesBundle() {
        return logMessagesBundle;
    }

    @Override
    public Locale getOriginalLocale() {
        return systemLocale;
    }


    private static class MultipleResourcesClassPathControl extends ResourceBundle.Control {

        private static final MultipleResourcesClassPathControl INSTANCE = new MultipleResourcesClassPathControl();

        @Override
        public List<String> getFormats(String baseName) {
            if (baseName == null) {
                throw new NullPointerException();
            }
            return FORMAT_PROPERTIES;
        }

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IOException {
            String bundleName = toBundleName(baseName, locale);
            return newBundle0(bundleName, format, loader, reload);
        }

        private ResourceBundle newBundle0(String bundleName, String format, ClassLoader loader, boolean reload) throws IOException {
            ResourceBundle bundle = null;
            if (format.equals("java.properties")) {
                final String resourceName = toResourceName0(bundleName);
                if (resourceName == null) {
                    return null;
                }

                final boolean reloadFlag = reload;


                Enumeration<URL> urlEnumeration = loader.getResources(resourceName);
                Vector<InputStream> collectedInputStreams = new Vector<>();
                while (urlEnumeration.hasMoreElements()) {
                    URL url = urlEnumeration.nextElement();
                    URLConnection connection = url.openConnection();
                    if (reloadFlag) {
                        // Disable caches to get fresh data for
                        // reloading.
                        connection.setUseCaches(false);
                    }
                    collectedInputStreams.add(connection.getInputStream());
                }
                InputStream stream = null;
                if (!collectedInputStreams.isEmpty())
                    stream = new SequenceInputStream(collectedInputStreams.elements());
                if (stream != null) {
                    try {
                        bundle = new PropertyResourceBundle(stream);
                    } finally {
                        stream.close();
                    }
                }
            } else {
                throw new IllegalArgumentException("unknown format: " + format);
            }
            return bundle;
        }

        private String toResourceName0(String bundleName) {
            // application protocol check
            if (bundleName.contains("://")) {
                return null;
            } else {
                return toResourceName(bundleName, "properties");
            }
        }
    }
}
