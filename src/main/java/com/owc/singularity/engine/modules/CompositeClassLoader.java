package com.owc.singularity.engine.modules;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

import com.owc.singularity.engine.tools.CompoundEnumeration;

/**
 * A {@link ClassLoader} that will first search a default loader for a class or a resource and if
 * not found, it will search the specified {@link ClassLoader class loaders} in-order.
 * 
 * @author Hatem Hamad
 */
public class CompositeClassLoader extends ClassLoader {

    private final ClassLoader[] loaders;
    /**
     * This will store the currently running searches. This could happen if the backup loaders uses
     * this same loader to load the class.
     */
    private final Set<String> currentlySearchingFor = new ConcurrentSkipListSet<>();

    public CompositeClassLoader(ClassLoader defaultLoader, ClassLoader... loaders) {
        super(defaultLoader);
        this.loaders = Arrays.stream(loaders).distinct().filter(Objects::nonNull).toArray(ClassLoader[]::new);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (currentlySearchingFor.contains(name))
            throw new ClassNotFoundException(name);
        currentlySearchingFor.add(name);
        try {
            for (ClassLoader loader : loaders) {
                try {
                    return loader.loadClass(name);
                } catch (ClassNotFoundException ignored) {
                }
            }
            throw new ClassNotFoundException(name);
        } finally {
            currentlySearchingFor.remove(name);
        }
    }

    @Override
    protected URL findResource(String name) {
        if (currentlySearchingFor.contains(name))
            return null;
        currentlySearchingFor.add(name);
        try {
            URL result = null;
            for (int i = 0; i < loaders.length && result == null; i++) {
                result = loaders[i].getResource(name);
            }
            return result;
        } finally {
            currentlySearchingFor.remove(name);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Enumeration<URL> findResources(String name) throws IOException {
        if (currentlySearchingFor.contains(name))
            return null;
        currentlySearchingFor.add(name);
        try {
            Enumeration<URL>[] tmp = (Enumeration<URL>[]) new Enumeration<?>[loaders.length];
            for (int i = 0; i < loaders.length; i++) {
                tmp[i] = loaders[i].getResources(name);
            }
            return new CompoundEnumeration<>(tmp);
        } finally {
            currentlySearchingFor.remove(name);
        }
    }
}
