/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.modules;


import static org.reflections.scanners.Scanners.MethodsAnnotated;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.modules.annotations.InitModule;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.I18N.SettingsType;
import com.owc.singularity.engine.tools.ResourceSource;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.tools.VersionNumber;


/**
 * <p>
 * The class for SingularityEngine plugins. This class is used to encapsulate the .jar file which
 * must be in the <code>lib/plugins</code> subdirectory of SingularityEngine. Provides methods for
 * plugin checks, operator registering, and getting information about the plugin.
 * </p>
 * <p>
 * Plugin dependencies must be defined in the form <br />
 * plugin_name1 (plugin_version1) # ... # plugin_nameM (plugin_versionM) < /br> of the manifest
 * parameter <code>Plugin-Dependencies</code>. You must define both the name and the version of the
 * desired plugins and separate them with &quot;#&quot;.
 * </p>
 *
 * @implNote (extensions-breaker) <a href=
 *           "https://gitlab.oldworldcomputing.com/singularity/engine/-/wikis/Extensions-Breaker">read
 *           more</a>
 * @author Simon Fischer, Ingo Mierswa, Nils Woehler, Adrian Wilke
 */
public class Module {

    /** The resource source for this plugin */
    private final ResourceSource resourceSource;

    /** The class loader based on the plugin file. */
    private final ModuleClassLoader classLoader;

    private final Future<Reflections> reflectionsFuture;

    private final ModuleManifest manifest;


    /**
     * Creates a new plugin based on the plugin .jar file.
     * 
     * @param availableModules,
     *            ClassLoader applicationClassLoader
     */
    Module(ModuleManifest manifest, List<Module> availableModules, ClassLoader applicationClassLoader) throws IOException {
        this.manifest = manifest;

        Set<String> requiredDependencies = Arrays.stream(manifest.moduleDependencies).map(d -> d.moduleKey).collect(Collectors.toSet());
        List<ModuleClassLoader> dependencyLoader = availableModules.stream()
                .filter(m -> requiredDependencies.contains(m.getKey()))
                .map(Module::getClassLoader)
                .collect(Collectors.toList());
        // we load them in reverse order to start with the most specific module
        Collections.reverse(dependencyLoader);

        this.classLoader = new ModuleClassLoader(this, applicationClassLoader, dependencyLoader);
        this.reflectionsFuture = CompletableFuture.supplyAsync(
                () -> new Reflections(new ConfigurationBuilder().setScanners(MethodsAnnotated).setUrls(manifest.url).addClassLoaders(getClassLoader())));
        this.resourceSource = new ResourceSource(this.classLoader);
        Tools.setResourceSourceForPlugin(getKey(), resourceSource);

    }


    /**
     * Returns the class loader of this plugin. This class loader should be used in cases where
     * Class.forName(...) should be used, e.g. for implementation finding in all classes (including
     * the core and the plugins).
     */
    public ModuleClassLoader getClassLoader() {
        return this.classLoader;
    }


    public String getKey() {
        return manifest.moduleKey;
    }

    /** Returns the name of the plugin. */
    public String getName() {
        return manifest.name;
    }

    public String getVendor() {
        return manifest.vendor;
    }

    public URL getURL() {
        return manifest.url;
    }

    /** Returns the version of this plugin. */
    public VersionNumber getVersion() {
        return manifest.version;
    }

    /** Returns the necessary engine version. */
    public VersionNumber getNecessaryEngineVersion() {
        return manifest.requiredEngineVersion;
    }


    /**
     * Returns the resource source of this plugin.
     *
     * @return the source, never {@code null}
     * @since 9.0.0
     */
    public ResourceSource getResourceSource() {
        return resourceSource;
    }

    public Reflections getReflections() {
        try {
            return reflectionsFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    void initModule() {
        // handle resources like texts, log messages, errors

        // registering parse rules
        URL resource = this.classLoader.findResource("com/owc/singularity/resources/parserules.xml");
        if (resource != null) {
            XMLImporter.importParseRules(resource, this);
        }


        // registering settings for internationalization
        I18N.registerErrorBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.Errors", Locale.getDefault(), this.classLoader));
        I18N.registerGUIBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.GUI", Locale.getDefault(), this.classLoader));
        I18N.registerUserErrorMessagesBundle(
                ResourceBundle.getBundle("com.owc.singularity.resources.i18n.UserErrorMessages", Locale.getDefault(), this.classLoader));
        I18N.registerLogMessagesBundle(ResourceBundle.getBundle("com.owc.singularity.resources.i18n.LogMessages", Locale.getDefault(), this.classLoader));
        ResourceBundle settingsResourceBundle = ResourceBundle.getBundle("com.owc.singularity.resources.i18n.Settings", Locale.getDefault(), this.classLoader);
        I18N.registerSettingsBundle(settingsResourceBundle);

        // updating settings
        for (String settingsKey : settingsResourceBundle.keySet()) {
            if (settingsKey.endsWith(SettingsType.DESCRIPTION.toString())) {

                // Extract key of ParameterType by removing suffix of description
                String parameterTypeKey = settingsKey.substring(0, settingsKey.length() - SettingsType.DESCRIPTION.toString().length());

                ParameterType parameterType = PropertyService.getParameterType(parameterTypeKey);
                if (parameterType != null) {
                    String description = I18N.getSettingsMessage(parameterTypeKey, SettingsType.DESCRIPTION);

                    // Only update description, if it is set in i18n
                    if (!description.startsWith(settingsKey)) {
                        parameterType.setDescription(description);
                    }
                }
            }
        }

        String moduleKey = getKey();
        // registering IOObjects and their names
        try (InputStream ioobjectDescriptorFileStream = this.classLoader.getResourceAsStream("com/owc/singularity/resources/ioobjects.xml")) {
            if (ioobjectDescriptorFileStream != null) {
                IOObjectService.registerReporters(toString(), ioobjectDescriptorFileStream, this.classLoader);
            } else {
                LogService.getRoot().warn("Cannot find io object descriptor 'com/owc/singularity/resources/ioobjects.xml' in module {}", moduleKey);
            }
        } catch (IOException e) {
            LogService.getRoot().warn("Cannot load io object descriptor 'com/owc/singularity/resources/ioobjects.xml' in module {}", moduleKey, e);
        }

        // Call specific init methods
        getReflections().getMethodsAnnotatedWith(InitModule.class).forEach(method -> {
            try {
                if (method.getParameterCount() == 0) {
                    method.invoke(null);
                } else
                    LogService.getRoot()
                            .warn("com.owc.singularity.module.init_module_method_illegal_signature",
                                    new Object[] { this.getName(), method.getDeclaringClass().getName() });
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                LogService.getRoot()
                        .warn("com.owc.singularity.tools.plugin.Plugin.register_operators_runtime_failed", new Object[] { this.getName(), e.getMessage() });
            }
        });
    }


    @Override
    public String toString() {
        return getName() + " [" + getKey() + ":" + getVersion() + "]";
    }
}
