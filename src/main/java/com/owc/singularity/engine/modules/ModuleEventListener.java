package com.owc.singularity.engine.modules;

/**
 * A listener interface for listeners concerned with {@link Module} registration/un-registration.
 */
public interface ModuleEventListener {

    void moduleAdded(Module module);

    void moduleRemoved(Module module);
}
