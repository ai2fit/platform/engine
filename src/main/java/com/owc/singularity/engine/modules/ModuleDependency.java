package com.owc.singularity.engine.modules;

import com.owc.singularity.tools.VersionNumber;

public class ModuleDependency {

    String moduleKey;
    VersionNumber dependencyVersion;

    public ModuleDependency(String moduleKey, VersionNumber dependencyVersion) {
        this.moduleKey = moduleKey;
        this.dependencyVersion = dependencyVersion;
    }

    public static ModuleDependency[] allOf(String dependencyList) {
        String[] dependencyStrings = dependencyList.split(",");
        ModuleDependency[] dependencies = new ModuleDependency[dependencyStrings.length];
        for (int i = 0; i < dependencyStrings.length; i++) {
            dependencies[i] = ModuleDependency.of(dependencyStrings[i]);
        }
        return dependencies;
    }

    public static ModuleDependency of(String dependency) {
        dependency = dependency.trim();
        int startVersion = dependency.indexOf("[");
        int endVersion = dependency.indexOf("]");
        if (startVersion == -1 || endVersion == -1 || endVersion < startVersion)
            throw new IllegalArgumentException("Dependency not valid: " + dependency);
        String dependentModuleKey = dependency.substring(0, startVersion);
        String dependentModuleVersion = dependency.substring(startVersion + 1, endVersion);
        return new ModuleDependency(dependentModuleKey, new VersionNumber(dependentModuleVersion));
    }

    @Override
    public String toString() {
        return "ModuleDependency{" + "moduleKey=" + moduleKey + ", dependencyVersion=" + dependencyVersion + "}";
    }
}
