package com.owc.singularity.engine.modules;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import com.owc.singularity.tools.VersionNumber;
import com.owc.singularity.tools.VersionNumber.VersionNumberException;

public class ModuleManifest {

    private static final String MANIFEST_ENTRY_SINGULARITY_VERSION = "Singularity-Version";
    private static final String MANIFEST_ENTRY_MODULE_KEY = "Module-Key";
    private static final String MANIFEST_ENTRY_MODULE_NAME = "Module-Name";
    private static final String MANIFEST_ENTRY_MODULE_DEPENDENCIES = "Module-Dependencies";
    private static final String MANIFEST_ENTRY_MODULE_VENDOR = "Implementation-Vendor";
    private static final String MANIFEST_ENTRY_MODULE_VERSION = "Implementation-Version";

    /** The name of the module. */
    final String name;

    /** The version of the plugin. */
    final VersionNumber version;

    /** The vendor of the plugin. */
    final String vendor;

    /** The SingularityEngine version which is needed for this plugin. */
    final VersionNumber requiredEngineVersion;

    final String moduleKey;

    final ModuleDependency[] moduleDependencies;

    final URL url;

    /**
     * Create a representation of module from a jar file
     * 
     * @param file
     *            the file of module jar
     * @throws IOException
     *             if an error occurs during IO operations
     * @throws IllegalArgumentException
     *             if the file has an incomplete module manifest
     */
    ModuleManifest(File file) throws IOException {
        this.url = file.toURI().toURL();
        try (JarFile jarFile = new JarFile(file)) {
            Manifest manifest = jarFile.getManifest();
            if (manifest == null)
                throw new IllegalArgumentException("Jar file does not provide a MANIFEST entry");
            Attributes manifestAttributes = manifest.getMainAttributes();
            // required attributes
            name = Objects.requireNonNull(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_NAME), MANIFEST_ENTRY_MODULE_NAME);
            version = getManifestValueAsVersion(manifestAttributes, MANIFEST_ENTRY_MODULE_VERSION, VersionNumber.Undefined);
            moduleKey = Objects.requireNonNull(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_KEY), MANIFEST_ENTRY_MODULE_KEY);
            requiredEngineVersion = getManifestValueAsVersion(manifestAttributes, MANIFEST_ENTRY_SINGULARITY_VERSION, new VersionNumber(0, 0, 0));

            // optional values
            String moduleDependencyString = getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_DEPENDENCIES);
            if (moduleDependencyString != null) {
                ModuleDependency[] moduleDependencies;
                try {
                    moduleDependencies = ModuleDependency.allOf(moduleDependencyString);
                } catch (IllegalArgumentException | VersionNumberException e) {
                    throw new IllegalArgumentException("Could not parse module dependencies from '" + moduleDependencyString + "'", e);
                }
                this.moduleDependencies = moduleDependencies;
            } else {
                moduleDependencies = new ModuleDependency[0];
            }
            vendor = Objects.requireNonNull(getManifestValue(manifestAttributes, MANIFEST_ENTRY_MODULE_VENDOR), MANIFEST_ENTRY_MODULE_VENDOR);
        } catch (NullPointerException e) {
            throw new IOException("Manifest of module " + file.getName() + " misses obligatory entry:" + e.getMessage(), e);
        }
    }

    private VersionNumber getManifestValueAsVersion(Attributes manifestAttributes, String manifestEntry, VersionNumber defaultVersion) {
        try {
            String manifestValue = getManifestValue(manifestAttributes, manifestEntry);
            if (manifestValue == null) {
                return defaultVersion;
            }
            return new VersionNumber(manifestValue);
        } catch (VersionNumberException e) {
            return null;
        }
    }

    private static String getManifestValue(Attributes attributes, String key) {
        String result = attributes.getValue(key);
        if (result != null) {
            result = result.trim();
            if (result.isEmpty())
                result = null;
        }
        return result;
    }

    @Override
    public String toString() {
        return moduleKey + "[" + version + "]@" + url.getFile();
    }

}
