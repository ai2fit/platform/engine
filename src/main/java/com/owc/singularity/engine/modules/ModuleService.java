package com.owc.singularity.engine.modules;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.EngineExecutionMode;
import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.PlatformUtilities;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.tools.VersionNumber;

public class ModuleService {

    private static final Logger log = LogManager.getLogger(ModuleService.class);
    private static CompositeClassLoader MAJOR_CLASS_LOADER = new CompositeClassLoader(ModuleService.class.getClassLoader());
    private static Future<Reflections> REFLECTIONS_FUTURE;
    /** The folder where bundled server extensions are stored at */
    private static final Set<String> additionalModuleDirs = new HashSet<>();

    private static final List<Module> modules = Collections.synchronizedList(new LinkedList<>());

    private static final List<ModuleEventListener> eventListeners = new ArrayList<>();


    /**
     * Initializes the service all plugins if {@link EngineProperties#MODULES_ENABLED} is set.
     * Plugins are searched for in the directory specified by
     * {@link EngineProperties#INIT_PLUGINS_LOCATION} or, if this is not set, in the lib/plugins
     * directory. Afterward the major classloader is created containing the application classloader
     * and all modules.
     */
    public static void init() {
        log.debug("Loading modules...");
        long modulesLoadStartTime = System.nanoTime();
        // first collect all module locations
        List<File> moduleLocations = getAllConfiguredModuleLocations();


        // find jar files from class path for bundled modules
        List<File> moduleCandidateFiles = new LinkedList<>();

        // add jars from module locations
        for (File moduleLocation : moduleLocations) {
            if (moduleLocation != null && moduleLocation.isDirectory()) {
                try {
                    try (DirectoryStream<Path> pathsStream = Files.newDirectoryStream(moduleLocation.toPath(), "*.jar")) {
                        for (Path jarFilePath : pathsStream) {
                            moduleCandidateFiles.add(jarFilePath.toFile());
                        }
                    }
                } catch (IOException e) {
                    log.atWarn().withThrowable(e).log("Ignoring unreachable modules location {}", moduleLocation);
                }
            }
        }

        // create module candidate with jar class loader
        List<ModuleManifest> moduleCandidates = new LinkedList<>();
        for (File moduleCandidateFile : moduleCandidateFiles) {
            try {
                moduleCandidates.add(new ModuleManifest(moduleCandidateFile));
            } catch (IllegalArgumentException e) {
                // not a valid module jar
                log.atWarn().withThrowable(e).log("Skipping file at {} due to invalid module manifest", moduleCandidateFile);
            } catch (IOException e) {
                log.atWarn().withThrowable(e).log("Could not read file at {}", moduleCandidateFile);
            }
        }

        // remove duplicate versions
        moduleCandidates.sort((m1, m2) -> {
            if (m1.moduleKey.equals(m2.moduleKey)) {
                // want to have the newer version at front
                return m2.version.compareTo(m1.version);
            }
            return m1.moduleKey.compareTo(m2.moduleKey);
        });
        Iterator<ModuleManifest> candidateIterator = moduleCandidates.iterator();
        String lastKey = null;
        while (candidateIterator.hasNext()) {
            ModuleManifest manifest = candidateIterator.next();
            if (manifest.moduleKey.equals(lastKey)) {
                candidateIterator.remove();
                log.warn("Ignoring earlier version {} of module {}...", lastKey, manifest.version);
            } else
                lastKey = manifest.moduleKey;
        }

        // check for dependencies
        Map<String, VersionNumber> availableDependencies = new HashMap<>();
        List<ModuleManifest> loadableModules = new LinkedList<>();

        boolean foundNewLoadableModule = true;
        while (foundNewLoadableModule && !moduleCandidates.isEmpty()) {
            foundNewLoadableModule = false;
            // now scan for all candidates
            candidateIterator = moduleCandidates.iterator();
            while (candidateIterator.hasNext()) {
                ModuleManifest manifest = candidateIterator.next();
                if (areFullfilled(manifest.moduleDependencies, availableDependencies)) {
                    candidateIterator.remove();
                    loadableModules.add(manifest);
                    availableDependencies.put(manifest.moduleKey, manifest.version);
                    foundNewLoadableModule = true;
                }

            }
        }

        // alert for non loadable modules
        for (ModuleManifest manifest : moduleCandidates) {
            log.error("Failed to load module: {} ({}) with version {} due to missing dependencies: {}", manifest.name, manifest.moduleKey, manifest.version,
                    Arrays.toString(manifest.moduleDependencies));
        }

        // loading modules
        for (ModuleManifest moduleManifest : loadableModules) {
            try {
                Module module = new Module(moduleManifest, modules, ModuleService.class.getClassLoader());
                modules.add(module);
                log.info("Loaded module: {} ({}) with version {} from {}", moduleManifest.name, moduleManifest.moduleKey, moduleManifest.version,
                        moduleManifest.url);
                eventListeners.forEach(listener -> listener.moduleAdded(module));
            } catch (IOException e) {
                log.atError()
                        .withThrowable(e)
                        .log("Failed to load module: {} ({}) with version {}", moduleManifest.name, moduleManifest.moduleKey, moduleManifest.version);
            }
        }

        // init major class loader
        MAJOR_CLASS_LOADER = new CompositeClassLoader(ModuleService.class.getClassLoader(),
                modules.stream().map(Module::getClassLoader).toArray(ClassLoader[]::new));
        REFLECTIONS_FUTURE = CompletableFuture.supplyAsync(ModuleService::createReflections);
        log.debug("Loading modules...DONE (took {})", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - modulesLoadStartTime)));
    }

    public static void initModules() {
        log.debug("Initializing modules...");
        long startTime = System.nanoTime();
        modules.stream().parallel().forEach(module -> {
            try {
                long moduleInitStartTime = System.nanoTime();
                module.initModule();
                log.trace("Initialized module: {} (took {})", module, Tools.formatDuration(Duration.ofNanos(System.nanoTime() - moduleInitStartTime)));
            } catch (Throwable e) {
                log.atError().withThrowable(e).log("Failed to initialize module: {}", module);
            }
        });
        log.debug("Initializing modules...DONE (took {})", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - startTime)));
    }

    private static boolean areFullfilled(ModuleDependency[] moduleDependencies, Map<String, VersionNumber> availableDependencies) {
        for (ModuleDependency dependency : moduleDependencies) {
            VersionNumber availableVersion = availableDependencies.get(dependency.moduleKey);
            if (availableVersion == null || !availableVersion.isAtLeast(dependency.dependencyVersion))
                return false;
        }
        return true;
    }

    private static List<File> getAllConfiguredModuleLocations() {
        List<File> moduleLocations = new LinkedList<>();

        moduleLocations.add(getUserModuleDirectory());
        moduleLocations.add(getGlobalModuleDirectory());
        // Check for additional extension directories and load extensions from there (if the
        // directory exists).
        for (String additionalModuleDir : additionalModuleDirs) {
            File moduleDirectory = new File(additionalModuleDir);
            if (moduleDirectory.isDirectory()) {
                moduleLocations.add(moduleDirectory);
            }
        }
        return moduleLocations;
    }

    private static File getGlobalModuleDirectory() {
        // Check global folder for modules and register them
        if (shouldLoadFromGlobalFolder()) {
            try {
                // Load globally installed extensions if PROPERTY_SINGULARITY_HOME is specified
                File globalModuleDir = FileSystemService.getLibraryFile("modules");
                if (globalModuleDir.isDirectory()) {
                    return globalModuleDir;
                }
            } catch (IOException e) {
                log.warn("The property ''{}'' is not set. Globally installed modules will not be loaded.", PlatformUtilities.PROPERTY_SINGULARITY_HOME);
                return null;
            }
        }
        return null;
    }


    private static File getUserModuleDirectory() {
        // Check if an extension directory is specified in the preferences and load extensions
        // from there (if it exists).
        File moduleDir = null;
        String pluginDirString = PropertyService.getParameterValue(EngineProperties.INIT_PLUGINS_LOCATION);
        if (pluginDirString != null && !pluginDirString.trim().isEmpty()) {
            moduleDir = new File(pluginDirString);
        } else if (ModuleService.shouldLoadFromUserConfigFolder()) {
            // update preferences if preferences property is empty
            // and point it to ~/.ai2fit/extensions
            moduleDir = FileSystemService.getUserConfigFile("modules");
            if (!moduleDir.isDirectory() && !moduleDir.mkdirs()) {
                log.warn("Could not create user default modules directory at {}", moduleDir);
            }
            PropertyService.setParameterValue(EngineProperties.INIT_PLUGINS_LOCATION, moduleDir.getAbsolutePath());
        }
        if (moduleDir != null && !moduleDir.isDirectory())
            return null;
        return moduleDir;
    }


    /**
     * @return {@code false} if the {@link EngineExecutionMode} of Singularity is embedded, web
     *         (server/applet) or test. {@code true} for UI, COMMAND_LINE or UNKNOWN.
     */
    private static boolean shouldLoadFromUserConfigFolder() {
        switch (SingularityEngine.getExecutionMode()) {
            case APPLET:
            case APPSERVER:
            case EMBEDDED_WITHOUT_UI:
            case EMBEDDED_AS_APPLET:
            case EMBEDDED_WITH_UI:
            case TEST:
                return false;
            case COMMAND_LINE:
            case UI:
            case UNKNOWN:
            default:
                return true;
        }
    }

    /**
     * @return {@code false} if the {@link EngineExecutionMode} of Singularity is embedded or web
     *         (server/applet). {@code true} for UI, COMMAND_LINE or UNKNOWN.
     */
    private static boolean shouldLoadFromGlobalFolder() {
        switch (SingularityEngine.getExecutionMode()) {
            case APPLET:
            case APPSERVER:
            case EMBEDDED_WITHOUT_UI:
            case EMBEDDED_AS_APPLET:
            case EMBEDDED_WITH_UI:
                return false;
            case TEST:
            case COMMAND_LINE:
            case UI:
            case UNKNOWN:
            default:
                return true;
        }
    }

    /**
     * Adds a directory to scan for Singularity extensions when initializing the Singularity
     * extensions.
     *
     * @param directory
     *            the absolute path to the directory which contains the Singularity extensions
     */
    public static void addAdditionalExtensionDir(String directory) {
        additionalModuleDirs.add(directory);
    }

    /**
     * Get the cached {@link Reflections} object that will scan the engine and all of its registered
     * {@link #getAllModules() modules}
     * 
     * @return a cached {@link Reflections} object.
     */
    public synchronized static Reflections getMajorReflections() {
        if (REFLECTIONS_FUTURE == null) {
            // safe mode, no modules were loaded
            REFLECTIONS_FUTURE = CompletableFuture.completedFuture(createReflections());
        }
        try {
            return REFLECTIONS_FUTURE.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private static Reflections createReflections() {
        return new Reflections(configureReflectionsWithMajorClassLoader(new ConfigurationBuilder()));
    }

    private static ConfigurationBuilder configureReflectionsWithMajorClassLoader(ConfigurationBuilder builder) {
        // application class loader
        builder.addUrls(ClasspathHelper.forPackage("com.owc.singularity", ClassLoader.getSystemClassLoader()));
        ClassLoader classLoader = getMajorClassLoader();
        builder.addClassLoaders(classLoader == null ? ClassLoader.getSystemClassLoader() : classLoader);
        // add all modules
        for (Module module : modules) {
            builder.addUrls(module.getURL());
        }

        return builder;
    }


    public static void registerEventListener(ModuleEventListener eventListener) {
        eventListeners.add(eventListener);
    }

    public static void unregisterEventListener(ModuleEventListener eventListener) {
        eventListeners.remove(eventListener);
    }

    /** Returns a class loader which is able to load all classes (core _and_ all plugins). */
    public static ClassLoader getMajorClassLoader() {
        return MAJOR_CLASS_LOADER;
    }

    /** Returns a sorted collection of all plugins. */
    public static Collection<Module> getAllModules() {
        return modules;
    }

    /** Returns the plugin with the given extension id. */
    public static Module getModuleByKey(String key) {
        return getModuleByKey(key, modules);
    }

    private static Module getModuleByKey(String name, Collection<Module> modules) {
        for (Module module : modules) {
            if (name.equals(module.getKey())) {
                return module;
            }
        }
        return null;
    }

    public static Module getModuleOfClass(Class<?> classInModule) {
        if (classInModule.getClassLoader() instanceof ModuleClassLoader) {
            return ((ModuleClassLoader) classInModule.getClassLoader()).getModule();
        }
        try {
            URL location = classInModule.getProtectionDomain().getCodeSource().getLocation();
            if (location == null)
                return null;
            for (Module module : getAllModules()) {
                for (URL moduleUrl : module.getClassLoader().getURLs()) {
                    if (location.equals(moduleUrl))
                        return module;
                }
            }
            return null;
        } catch (Exception ignored) {
            return null;
        }
    }

}
