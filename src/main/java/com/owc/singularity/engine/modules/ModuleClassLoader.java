/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.modules;


import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;


/**
 * The class loader for a module (extending URLClassLoader). It will first resolve classes against
 * its own file, then searching in the {@link ModuleService#getMajorClassLoader()}. This allows for
 * modules to depend on other extensions
 *
 * @author Ingo Mierswa, Michael Knopf
 */
public class ModuleClassLoader extends URLClassLoader {

    private final Module module;
    private ClassLoader[] dependentClassLoaders;
    private ClassLoader applicationClassLoader;

    /**
     * This constructor is for modules.
     *
     * @param module
     *            The module which URLs will be used for class building.
     * @param dependencyLoaders
     *            The classloaders of dependent modules
     * @param applicationClassLoader
     *            the apllication classloader itself
     */
    public ModuleClassLoader(Module module, ClassLoader applicationClassLoader, List<ModuleClassLoader> dependencyLoaders) throws IOException {
        super(new URL[] { module.getURL() }, null); // we do the parent thing
                                                    // ourselves
        this.module = module;
        this.applicationClassLoader = applicationClassLoader;
        dependentClassLoaders = dependencyLoaders.toArray(ClassLoader[]::new);
    }

    @Override
    protected synchronized Class<?> findClass(String name) throws ClassNotFoundException {
        // first search within this module itself
        try {
            return super.findClass(name);
        } catch (ClassNotFoundException ignored) {
        }

        // afterwards load within the dependent class loaders
        for (ClassLoader loader : dependentClassLoaders) {
            try {
                return loader.loadClass(name);
            } catch (ClassNotFoundException ignored) {
            }
        }

        // finally try application class loader
        return applicationClassLoader.loadClass(name);
    }

    @Override
    public URL getResource(String name) {
        // first search within this module itself
        URL url = super.getResource(name);
        if (url != null) {
            return url;
        }

        // afterwards load within the dependent class loaders
        for (ClassLoader loader : dependentClassLoaders) {
            url = loader.getResource(name);
            if (url != null) {
                return url;
            }
        }

        // finally try application class loader
        return applicationClassLoader.getResource(name);
    }

    public Module getModule() {
        return module;
    }

    @Override
    public String toString() {
        return "ModuleClassLoader (" + getModule().getName() + ")";
    }
}
