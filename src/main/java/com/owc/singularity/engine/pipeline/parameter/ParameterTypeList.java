/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.studio.gui.parameters.celleditors.value.ListValueCellEditor;


/**
 * A parameter type for parameter lists. Operators ask for the list of the specified values with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterList(String)}. Please note that
 * in principle arbitrary parameter types can be used for the list values. Internally, however, all
 * values are transformed to strings. Therefore, operators retrieving values from non-string lists
 * (for example for a parameter type category) have to transform the values themself, e.g. with the
 * following code:<br/>
 * <br/>
 *
 * <code>int index = ((ParameterTypeCategory)((ParameterTypeList)getParameters().getParameterType(PARAMETER_LIST)).getValueType()).getIndex(pair[1]);</code>
 *
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = ListValueCellEditor.class)
public class ParameterTypeList extends CombinedParameterType {

    private static final long serialVersionUID = -6101604413822993455L;

    private List<String[]> defaultList = new LinkedList<>();

    private final ParameterType valueType;
    private final ParameterType keyType;


    public ParameterTypeList(String key, String description, ParameterType keyType, ParameterType valueType) {
        this(key, description, keyType, valueType, new LinkedList<String[]>());
    }

    public ParameterTypeList(String key, String description, ParameterType keyType, ParameterType valueType, List<String[]> defaultList) {
        super(key, description, keyType, valueType);
        this.defaultList = defaultList;
        this.valueType = valueType;
        this.keyType = keyType;
    }

    public ParameterType getValueType() {
        return valueType;
    }

    public ParameterType getKeyType() {
        return keyType;
    }

    @Override
    public List<String[]> getDefaultValue() {
        return defaultList;
    }

    @SuppressWarnings("unchecked")
    @Override
    // TODO: Introduce Typing??
    public void setDefaultValue(Object defaultValue) {
        this.defaultList = (List<String[]>) defaultValue;
    }

    /** Returns false. */
    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public Element getXML(String key, String value, boolean hideDefault, Document doc) {
        Element element = doc.createElement("list");
        element.setAttribute("key", key);
        List<String[]> list = null;
        if (value != null) {
            list = transformRawString2List(value);
        } else {
            list = getDefaultValue();
        }
        if (list != null) {
            for (Object object : list) {
                Object[] entry = (Object[]) object;
                element.appendChild(valueType.getXML((String) entry[0], entry[1].toString(), false, doc));
            }
        }
        return element;
    }

    @Override
    public String getRange() {
        return "list";
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toRawString(Object value) {
        if (value instanceof String) {
            return transformList2String(transformString2List(value.toString()));
        } else {
            return transformList2String((List<String[]>) value);
        }
    }

    public static String transformList2String(List<String[]> parameterList) {
        return parameterList.stream()
                .map(vals -> Arrays.stream(vals).collect(Collectors.joining(Character.toString(Parameters.PAIR_SEPARATOR))))
                .collect(Collectors.joining(Character.toString(Parameters.RECORD_SEPARATOR)));
    }

    public static List<String[]> transformRawString2List(String listString) {
        return Arrays.stream(listString.split(Character.toString(Parameters.RECORD_SEPARATOR))).filter(record -> !record.isEmpty()).map(record -> {
            String[] pair = record.split(Character.toString(Parameters.PAIR_SEPARATOR));
            if (pair.length == 1)
                return new String[] { pair[0], "" };
            else
                return pair;
        }).filter(pair -> pair.length == 2).collect(Collectors.toList());
    }

    public static List<String[]> transformString2List(String listString) {
        return Arrays.stream(listString.split(Character.toString(Parameters.RECORD_SEPARATOR)))
                .filter(record -> !record.isEmpty())
                .map(record -> record.split(Character.toString(Parameters.PAIR_SEPARATOR)))
                .filter(pair -> pair.length == 2 && !pair[0].isEmpty() && !pair[1].isEmpty())
                .collect(Collectors.toList());
    }

    public static List<String[]> transformString2ListWithParameterError(String listString, String key) throws UndefinedParameterError {
        List<String[]> list = new LinkedList<>();
        String[] splittedString = listString.split(Character.toString(Parameters.RECORD_SEPARATOR));
        for (String record : splittedString) {
            if (record.isEmpty()) {
                continue;
            }
            String[] pair = record.split(Character.toString(Parameters.PAIR_SEPARATOR));
            if (pair.length != 2 || pair[0].isEmpty() || pair[1].isEmpty()) {
                throw new UndefinedParameterError(null, 299, key, key);
            }
            list.add(pair);
        }
        return list;
    }

    /** @return the changed value after all pairs were notified */
    @Override
    public String notifyOperatorRenaming(String oldOperatorName, String newOperatorName, String parameterValue) {
        return notifyOperatorRenamingReplacing((t, v) -> t.notifyOperatorRenaming(oldOperatorName, newOperatorName, v), parameterValue);
    }

    /** @return the changed value after all pairs were notified */
    @Override
    public String notifyOperatorReplacing(String oldName, Operator oldOp, String newName, Operator newOp, String parameterValue) {
        return notifyOperatorRenamingReplacing((t, v) -> t.notifyOperatorReplacing(oldName, oldOp, newName, newOp, v), parameterValue);
    }

    /** @since 9.3 */
    private String notifyOperatorRenamingReplacing(BiFunction<ParameterType, String, String> replacer, String parameterValue) {
        List<String[]> list = transformString2List(parameterValue);
        for (String[] pair : list) {
            pair[0] = replacer.apply(keyType, pair[0]);
            pair[1] = replacer.apply(valueType, pair[1]);
        }
        return transformList2String(list);
    }

    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) throws UndefinedParameterError {
        if (!parameterValue.contains("%{")) {
            return parameterValue;
        }
        List<String[]> list = transformString2List(parameterValue);
        List<String[]> result = new LinkedList<>();
        for (String[] entry : list) {
            result.add(new String[] { getKeyType().substituteVariables(entry[0], variableHandler),
                    getValueType().substituteVariables(entry[1], variableHandler) });
        }
        return transformList2String(result);
    }

    @Override
    public String substitutePredefinedVariables(String parameterValue, Operator operator) throws UndefinedParameterError {
        if (!parameterValue.contains("%{")) {
            return parameterValue;
        }
        List<String[]> list = transformString2List(parameterValue);
        List<String[]> result = new LinkedList<>();
        for (String[] entry : list) {
            result.add(new String[] { getKeyType().substitutePredefinedVariables(entry[0], operator),
                    getValueType().substitutePredefinedVariables(entry[1], operator) });
        }
        return transformList2String(result);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@code true} if either the key parameter or the value parameter or both are
     *         sensitive; {@code false} otherwise
     */
    @Override
    public boolean isSensitive() {
        boolean keySensitive = getKeyType() != null ? getKeyType().isSensitive() : false;
        boolean valueSensitive = getValueType() != null ? getValueType().isSensitive() : false;
        return keySensitive || valueSensitive;
    }


}
