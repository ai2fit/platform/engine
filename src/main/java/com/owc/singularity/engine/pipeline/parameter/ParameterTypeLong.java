/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;

/**
 * A parameter type for Long values. Operators ask for the Long value with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterAsLong(String)} For infinite
 * ranges Long.MIN_VALUE and Long.MAX_VALUE should be used.
 * 
 * @author Ingo Mierswa, Simon Fischer
 */
public class ParameterTypeLong extends ParameterTypeNumber {

    private static final long serialVersionUID = -7360090072467405524L;

    private long defaultValue = -1;

    private long min = Long.MIN_VALUE;

    private long max = Long.MAX_VALUE;

    private boolean noDefault = true;

    public ParameterTypeLong(String key, String description) {
        this(key, description, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public ParameterTypeLong(String key, String description, long min, long max) {
        this(key, description, min, max, -1);
        this.noDefault = true;
        setOptional(false);
    }

    public ParameterTypeLong(String key, String description, long min, long max, boolean optional) {
        this(key, description, min, max, -1);
        this.noDefault = true;
        setOptional(optional);
    }

    public ParameterTypeLong(String key, String description, long min, long max, long defaultValue) {
        super(key, description);
        this.defaultValue = defaultValue;
        this.min = min;
        this.max = max;
        this.noDefault = false;
    }


    public void setMinValue(long min) {
        this.min = min;
    }

    public void getMaxValue(long max) {
        this.max = max;
    }

    @Override
    public double getMinValue() {
        return min;
    }

    @Override
    public double getMaxValue() {
        return max;
    }

    public long getMinValuelong() {
        return min;
    }

    public long getMaxValuelong() {
        return max;
    }

    public long getDefaultlong() {
        return defaultValue;
    }

    @Override
    public Object getDefaultValue() {
        if (noDefault) {
            return null;
        } else {
            return defaultValue;
        }
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        if (defaultValue == null) {
            this.noDefault = true;
            this.defaultValue = -1;
            return;
        }
        this.noDefault = false;
        if (defaultValue instanceof Long) {
            this.defaultValue = (Long) defaultValue;
        } else {
            this.defaultValue = Long.parseLong(defaultValue.toString());
        }
    }

    /** Returns true. */
    @Override
    public boolean isNumerical() {
        return true;
    }

    @Override
    public String getRange() {
        String range = "Long; ";
        if (min == -Long.MAX_VALUE) {
            range += "-\u221E";
        } else {
            range += min;
        }
        range += "-";
        if (max == Long.MAX_VALUE) {
            range += "+\u221E";
        } else {
            range += max;
        }
        if (!noDefault) {
            range += "; default: " + getStringRepresentation(defaultValue);
        }
        return range;
    }

    public String getStringRepresentation(long defaultValue2) {
        String valueString = defaultValue2 + "";
        if (defaultValue2 == Long.MAX_VALUE) {
            valueString = "+\u221E";
        } else if (defaultValue2 == Long.MIN_VALUE) {
            valueString = "-\u221E";
        }
        return valueString;
    }

}
