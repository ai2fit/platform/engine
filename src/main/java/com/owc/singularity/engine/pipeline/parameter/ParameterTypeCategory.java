/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.DefaultPropertyValueCellEditor;


/**
 * A parameter type for categories. These are several Strings and one of these is the default value.
 * Operators ask for the index of the selected value with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterAsInt(String)}.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = DefaultPropertyValueCellEditor.class)
public class ParameterTypeCategory extends ParameterTypeSingle {

    private static final long serialVersionUID = 5747692587025691591L;

    private int defaultValue;

    private final String[] categories;

    public ParameterTypeCategory(String key, String description, String[] categories, int defaultValue) {
        super(key, description);
        this.categories = categories;
        this.defaultValue = defaultValue;
    }

    public int getDefault() {
        return defaultValue;
    }

    @Override
    public Object getDefaultValue() {
        if (defaultValue == -1 || categories.length == 0) {
            return null;
        } else {
            return categories[defaultValue];
        }
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = (Integer) defaultValue;
    }

    /** Returns false. */
    @Override
    public boolean isNumerical() {
        return false;
    }

    public String getCategory(int index) {
        return categories[index];
    }

    public int getIndex(String string) {
        for (int i = 0; i < categories.length; i++) {
            if (categories[i].equals(string)) {
                return i;
            }
        }
        // try to interpret string as number
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @Override
    public String toRawString(Object value) {
        try {
            if (value == null) {
                return null;
            }
            int index = Integer.parseInt(value.toString());
            if (index >= categories.length) {
                return "";
            }
            return super.toRawString(categories[index]);
        } catch (NumberFormatException e) {
            return super.toRawString(value);
        }
    }

    public String[] getValues() {
        return categories;
    }

    @Override
    public String getRange() {
        StringBuffer values = new StringBuffer();
        for (int i = 0; i < categories.length; i++) {
            if (i > 0) {
                values.append(", ");
            }
            values.append(categories[i]);
        }
        return values.toString() + "; default: " + categories[defaultValue];
    }

    public int getNumberOfCategories() {
        return categories.length;
    }

    /**
     * {@inheritDoc}
     *
     * @return always {@code false}
     */
    @Override
    public boolean isSensitive() {
        return false;
    }


}
