/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeList;
import com.owc.singularity.engine.tools.XMLException;


/**
 * A rule, which switches the sides of entries in a list parameter.
 * 
 * @author Sebastian Land
 */
@OperatorParseRuleAnnotation(xmlTagName = "switchListEntries")
public class SwitchListEntriesRule extends AbstractConditionedParseRule {

    private String parameter;

    public SwitchListEntriesRule(String operatorTypeName, Element element) throws XMLException {
        super(operatorTypeName, element);
        assert (element.getTagName().equals("switchListEntries"));
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element childElem) {
                if (childElem.getTagName().equals("parameter")) {
                    parameter = childElem.getTextContent();
                }
            }
        }
    }

    @Override
    protected String conditionedApply(Operator operator, String operatorTypeName, XMLPipelineParser parser) {
        if (operator.getParameters().isSpecified(parameter)) {
            String value = operator.getParameters().getParameterOrNull(parameter);
            if (value != null) {
                List<String[]> list = ParameterTypeList.transformString2List(value);
                for (String[] pair : list) {
                    String first = pair[0];
                    pair[0] = pair[1];
                    pair[1] = first;
                }
                operator.getParameters().setRawParameter(parameter, ParameterTypeList.transformList2String(list));
                return "Switched sides of the entries of <code>" + parameter + "</code> in <var>" + operator.getName() + "</var>.";
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Switch entries of List " + operatorTypeName + "." + parameter;
    }

}
