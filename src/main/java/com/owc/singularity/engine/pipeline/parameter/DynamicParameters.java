package com.owc.singularity.engine.pipeline.parameter;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * This is a subclass of parameters that supports the dynamic adding and removing of parameters.
 * This can be used when outer circumstances affect not only the visibility but the entire
 * definition of parameter types, e.g. when types depend on the values of other types.
 */
public class DynamicParameters extends Parameters {

    private static final long serialVersionUID = 8901562214971296513L;
    private Supplier<List<ParameterType>> typeSupplier;
    private Predicate<Parameters> typeUpdate;
    private boolean isChecking = false;

    public DynamicParameters(Supplier<List<ParameterType>> typeSupplier, Predicate<Parameters> typeUpdate) {
        super(typeSupplier.get());
        this.typeSupplier = typeSupplier;
        this.typeUpdate = typeUpdate;
    }

    @Override
    public Set<String> getDefinedKeys() {
        checkUpdate();
        return super.getDefinedKeys();
    }

    @Override
    public Set<String> getKeys() {
        checkUpdate();
        return super.getKeys();
    }

    @Override
    public String getParameter(String key) throws UndefinedParameterError {
        checkUpdate();
        return super.getParameter(key);
    }

    @Override
    public String getParameterAsSpecified(String key) {
        checkUpdate();
        return super.getParameterAsSpecified(key);
    }

    @Override
    public String getParameterOrNull(String key) {
        checkUpdate();
        return super.getParameterOrNull(key);
    }

    @Override
    public ParameterType getParameterType(String key) {
        checkUpdate();
        return super.getParameterType(key);
    }

    @Override
    public Collection<ParameterType> getParameterTypes() {
        checkUpdate();
        return super.getParameterTypes();
    }

    @Override
    public String getRawParameter(String key) throws UndefinedParameterError {
        checkUpdate();
        return super.getRawParameter(key);
    }

    private void checkUpdate() {
        if (!isChecking) {
            isChecking = true;
            try {
                if (typeUpdate.test(this))
                    setTypes(typeSupplier.get());
            } finally {
                isChecking = false;
            }
        }
    }

    /** Performs a deep clone on this parameters object. */
    @Override
    public Object clone() {
        Parameters clone = new DynamicParameters(typeSupplier, typeUpdate);

        clone.keyToValueMap.putAll(keyToValueMap);
        clone.keyToTypeMap.putAll(keyToTypeMap);

        return clone;
    }
}
