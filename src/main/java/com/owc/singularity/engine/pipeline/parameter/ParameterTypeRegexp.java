/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.Collection;
import java.util.function.Supplier;

import com.owc.singularity.studio.gui.parameters.celleditors.value.RegexpValueCellEditor;


/**
 * A parameter type for regular expressions.
 * 
 * @author Tobias Malbrecht
 */
@ParameterTypeAnnotation(editor = RegexpValueCellEditor.class)
public class ParameterTypeRegexp extends ParameterTypeString {

    /**
     * General parameter key for this type of parameter that indicates what to replace
     */
    public static final String PARAMETER_REPLACE_WHAT = "replace_what";

    /**
     * General parameter key for an accompanying string parameter for the parameter with key
     * {@value #PARAMETER_REPLACE_WHAT} that describes the replacement
     */
    public static final String PARAMETER_REPLACE_BY = "replace_by";

    private static final long serialVersionUID = -4177652183651031337L;

    private ParameterTypeString replacementParameter;
    private transient Supplier<Collection<String>> previewSupplier = null;

    public ParameterTypeRegexp(final String key, String description) {
        this(key, description, true);
    }

    public ParameterTypeRegexp(final String key, String description, boolean optional) {
        super(key, description, optional);
    }

    public ParameterTypeRegexp(final String key, String description, boolean optional, Supplier<Collection<String>> previewSupplier) {
        super(key, description, optional);
        this.previewSupplier = previewSupplier;
    }

    public ParameterTypeRegexp(final String key, String description, String defaultValue) {
        super(key, description, defaultValue);
    }

    public Collection<String> getPreviewList() {
        if (previewSupplier != null)
            return previewSupplier.get();
        return null;
    }

    /**
     * Set the {@link ParameterTypeString} that might be linked to the replacement field in the
     * {@link com.owc.singularity.studio.gui.parameters.RegexpParameterDialog}.
     *
     * @param replacementParameter
     *            the parameter linked to replacement; can be {@code null}
     */
    public void setReplacementParameter(ParameterTypeString replacementParameter) {
        this.replacementParameter = replacementParameter;
    }

    /**
     * Returns the {@link ParameterTypeString} linked to the replacement field in the
     * {@link com.owc.singularity.studio.gui.parameters.RegexpParameterDialog} if it was set.
     *
     * @return the parameter linked to replacement; can be {@code null}
     */
    public ParameterTypeString getReplacementParameter() {
        return replacementParameter;
    }

}
