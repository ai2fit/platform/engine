package com.owc.singularity.engine.pipeline;

import com.owc.singularity.engine.operator.Operator;

/**
 * This is here only for foward compatibility with upcoming changes to the way pipelines are
 * configured, stored and executed.
 * 
 * @deprecated This will be replaced by a more meaningful imeplementation that contains all of
 *             operator's part of DEFINING an operation within a pipeline.
 */
public record Operation(Operator operator) {

    public String getName() {
        return operator.getName();
    }

}
