/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.tools.XMLException;


/**
 * Wires all immediate children of an operator chain.
 * 
 * @author Simon Fischer
 * 
 */
@OperatorParseRuleAnnotation(xmlTagName = "wireSubprocess")
public class WireAllOperators extends AbstractOperatorParseRule {

    private final int subprocess;

    public WireAllOperators(String operatorTypeName, Element element) throws XMLException {
        super(operatorTypeName, element);
        subprocess = Integer.parseInt(XMLTools.getTagContents(element, "subprocess"));
    }

    @Override
    protected String apply(final Operator operator, String operatorTypeName, XMLPipelineParser parser) {
        parser.doAfterAutoWire(new Runnable() {

            @Override
            public void run() {
                OperatorChain chain = (OperatorChain) operator;
                ExecutionUnit unit = chain.getSubprocess(subprocess);
                for (Operator op : unit.getOperators()) {
                    unit.autoWireSingle(op, true, true);
                }
            }
        });
        return null;
    }

}
