/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.pipeline;


import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import javax.swing.event.EventListenerList;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeCategory;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.tools.*;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.engine.visualization.datatable.SimpleDataTable;
import com.owc.singularity.repository.RepositoryPath;


/**
 * The abstract superclass of all pipelines. Will be split up into flow definition, layout,
 * configuration. Execution will be handled externally and not change anything on this. So use with
 * care.
 */
public abstract class AbstractPipeline implements Cloneable {

    private static final long serialVersionUID = 8004223120087061224L;
    public static final int PROCESS_STATE_UNKNOWN = -1;
    public static final int PROCESS_STATE_STOPPED = 0;
    public static final int PROCESS_STATE_PAUSED = 1;
    public static final int PROCESS_STATE_RUNNING = 2;

    /**
     * The root operator of the process.
     */
    protected AbstractRootOperator rootOperator = null;

    /**
     * This is the operator which is currently applied.
     */
    private Operator currentOperator;

    /**
     * The pipeline might be connected to this file or repository location which is then used to
     * resolve relative file names which might be defined as parameters.
     */
    private RepositoryPath processLocation;

    /**
     * Indicates if the original pipeline file has been changed by import rules. If this happens,
     * overwriting will destroy the backward compatibility. This flag indicates that this would
     * happen during saving.
     */
    private boolean isProcessConverted = false;


    /**
     * This list contains all unknown parameter information which existed during the loading of the
     * pipeline.
     */
    private List<UnknownParameterInformation> unknownParameterInformation = new LinkedList<>();

    /**
     * The listeners for breakpoints.
     */
    private final List<BreakpointListener> breakpointListeners = Collections.synchronizedList(new LinkedList<>());

    /**
     * The listeners for logging (data tables).
     */
    private final List<LoggingListener> loggingListeners = Collections.synchronizedList(new LinkedList<>());

    private final transient List<PipelineLifeCycleEventListener> lifeCycleEventListeners = new LinkedList<>();

    /**
     * The variable handler can be used to replace (user defined) variable strings.
     */
    private final VariableHandler variableHandler = new VariableHandler(this);

    /**
     * This map holds the names of all operators in the pipeline. Operators are automatically
     * registered during adding and unregistered after removal.
     */
    private Map<String, Operator> operatorNameMap = new HashMap<>();

    /**
     * Maps names of ProcessLog operators to Objects, that these Operators use for collecting
     * statistics (objects of type {@link DataTable}).
     */
    private final Map<String, DataTable> dataTableMap = new HashMap<>();

    /**
     * Indicates the current pipeline state.
     */
    private PipelineState state = PipelineState.STOPPED;

    /**
     * Indicates whether operators should be executed always or only when dirty.
     */
    private transient ExecutionMode executionMode = ExecutionMode.ALWAYS;

    /**
     * Indicates whether we are updating metadata.
     */
    private transient DebugMode debugMode = DebugMode.DEBUG_OFF;

    private transient Logger logger;

    private DevelopmentExecutionContext developmentContext = new DevelopmentExecutionContext(this);

    /**
     * We synchronize on this object to wait and resume operation.
     */
    private final Object breakpointLock = new Object();

    /**
     * Message generated during import by {@link XMLImporter}.
     */
    private String importMessage;

    private final EventListenerList processSetupListeners = new EventListenerList();

    private PipelineExecutionContext executionContext;

    private long startTimestamp = -1;

    // -------------------
    // Constructors
    // -------------------

    protected AbstractPipeline() {

    }

    /**
     * Constructs a pipeline consisting only of a SimpleOperatorChain.
     */
    public AbstractPipeline(AbstractRootOperator root) {
        try {
            root.rename(root.getOperatorDescription().getName());
            setRootOperator(root);
        } catch (Exception e) {
            throw new RuntimeException("Cannot initialize root operator of the pipeline: " + e.getMessage(), e);
        }
        developmentContext.refreshParameterTypes();
    }

    /**
     * Clone constructor. Makes a deep clone of the operator tree and the pipeline file. The same
     * applies for the operatorNameMap. The breakpoint listeners are copied by reference and all
     * other fields are initialized like for a fresh pipeline.
     */
    protected AbstractPipeline(final AbstractPipeline other) {
        this((AbstractRootOperator) other.rootOperator.cloneOperator(other.rootOperator.getName(), false));
        this.currentOperator = null;
        if (other.processLocation != null) {
            this.processLocation = other.processLocation;
        } else {
            this.processLocation = null;
        }
    }

    protected void initLogging(Level customLevel) {
        if (customLevel != null && !customLevel.equals(LogService.LEVEL_UNKNOWN)) {
            LogService.setLoggerLevel(getLogger(), customLevel);
        }
    }

    @Override
    public abstract AbstractPipeline clone();

    protected void setState(final PipelineState state) {
        this.state = state;
    }

    /**
     * Left for references from extensions
     *
     * @return the {@link PipelineState} numerical code
     */
    public int getProcessState() {
        return this.state.getCode();
    }

    /**
     * This checks whether this pipeline is actually of the given type.
     */
    public boolean isEffectivelyInstanceOf(Class<? extends AbstractPipeline> typeClass) {
        return typeClass.isAssignableFrom(this.getClass());
    }

    public Class<? extends AbstractPipeline> getEffectiveType() {
        return this.getClass();
    }

    public PipelineState getState() {
        return this.state;
    }

    // -------------------------
    // Logging
    // -------------------------

    public Logger getLogger() {
        if (logger == null) {
            logger = LogService.getI18NLogger(getClass());
        }
        return this.logger;
    }

    // -------------------------
    // Variable Handler
    // -------------------------

    /**
     * Returns the variable handler.
     */
    public VariableHandler getVariableHandler() {
        return this.variableHandler;
    }

    /**
     * Clears all variables.
     */
    public void clearVariables() {
        this.getVariableHandler().clear();
    }


    // -------------------------
    // Data Tables (Logging)
    // -------------------------

    /**
     * Adds the given logging listener.
     */
    public void addLoggingListener(final LoggingListener loggingListener) {
        this.loggingListeners.add(loggingListener);
    }

    /**
     * Removes the given logging listener.
     */
    public void removeLoggingListener(final LoggingListener loggingListener) {
        this.loggingListeners.remove(loggingListener);
    }

    /**
     * Returns true if a data table object with the given name exists.
     */
    public boolean dataTableExists(final String name) {
        return dataTableMap.get(name) != null;
    }

    /**
     * Adds the given data table.
     */
    public void addDataTable(final DataTable table) {
        dataTableMap.put(table.getName(), table);
        synchronized (loggingListeners) {
            for (LoggingListener listener : loggingListeners) {
                listener.addDataTable(table);
            }
        }
    }

    /**
     * Clears a single data table, i.e. removes all entries.
     */
    public void clearDataTable(final String name) {
        DataTable table = getDataTable(name);
        if (table instanceof SimpleDataTable) {
            ((SimpleDataTable) table).clear();
        }
    }

    /**
     * Deletes a single data table.
     */
    public void deleteDataTable(final String name) {
        if (dataTableExists(name)) {
            DataTable table = dataTableMap.remove(name);
            synchronized (loggingListeners) {
                for (LoggingListener listener : loggingListeners) {
                    listener.removeDataTable(table);
                }
            }
        }
    }

    /**
     * Returns the data table associated with the given name. If the name was not used yet, an empty
     * DataTable object is created with the given columnNames.
     */
    public DataTable getDataTable(final String name) {
        return dataTableMap.get(name);
    }

    /**
     * Returns all data tables.
     */
    public Collection<DataTable> getDataTables() {
        return dataTableMap.values();
    }

    /**
     * Removes all data tables before running a new pipeline.
     */
    protected void clearDataTables() {
        dataTableMap.clear();
    }


    // ----------------------
    // Operator Handling
    // ----------------------

    /**
     * Sets the current root operator. This might lead to a new registering of operator names.
     */
    public void setRootOperator(final AbstractRootOperator root) {
        this.rootOperator = root;
        this.operatorNameMap.clear();
        this.rootOperator.setPipeline(this);
    }

    /**
     * Delivers the current root operator.
     */
    public AbstractRootOperator getRootOperator() {
        return rootOperator;
    }

    /**
     * Returns the operator with the given name.
     */
    public Operator getOperator(final String name) {
        return operatorNameMap.get(name);
    }

    /**
     * Returns the operator that is currently being executed.
     */
    public Operator getCurrentOperator() {
        return currentOperator;
    }

    /**
     * Returns a Collection view of all operators.
     */
    public Collection<Operator> getAllOperators() {
        List<Operator> result = rootOperator.getAllInnerOperators();
        result.add(0, rootOperator);
        return result;
    }

    /**
     * Returns a Set view of all operator names (i.e. Strings).
     */
    public Collection<String> getAllOperatorNames() {
        return getAllOperators().stream().map(Operator::getName).collect(Collectors.toList());
    }

    /**
     * Sets the operator that is currently being executed.
     */
    public void setCurrentOperator(final Operator operator) {
        this.currentOperator = operator;
    }

    // -------------------------------------
    // start, stop, resume, breakpoints
    // -------------------------------------


    /**
     * Pauses the pipeline at a breakpoint.
     */
    public void pause(final Operator operator, final List<IOObject> iocontainer, final int breakpointType) {
        pause();
        if (getState() == PipelineState.PAUSED) {
            Logger log = getLogger();
            if (breakpointType == BreakpointListener.BREAKPOINT_BEFORE)
                log.info("com.owc.singularity.engine.execution.breakpoint_before_reached", operator.getName());
            else
                log.info("com.owc.singularity.engine.execution.breakpoint_after_reached", operator.getName());
            // pause request was approved
            fireBreakpointEvent(operator, iocontainer, breakpointType);
            long pauseStart = System.nanoTime();
            while (getState() == PipelineState.PAUSED) {
                synchronized (breakpointLock) {
                    try {
                        breakpointLock.wait();
                    } catch (InterruptedException e) {
                        log.warn("Waiting for an Operator breakpoint has caused an exception.", e);
                    }
                }
            }
            if (getState() == PipelineState.RUNNING) {
                log.info("com.owc.singularity.engine.execution.pipeline_resume", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - pauseStart)));
            }
        }
    }

    /**
     * Stops the pipeline as soon as possible.
     */
    public void stop() {
        final boolean requestApproved = fireEventPipelineStopRequested();
        if (requestApproved) {
            stop(null);
        }
    }

    private void breakpointUnlock() {
        synchronized (breakpointLock) {
            breakpointLock.notifyAll();
        }
    }

    /**
     * Signal the pipeline to be paused as soon as possible.
     */
    public void pause() {
        final boolean requestApproved = fireEventPipelinePauseRequested();
        if (requestApproved) {
            setState(PipelineState.PAUSED);
            fireEventPipelinePaused();
        }
    }

    /**
     * Resumes the pipeline after it has been paused.
     */
    public void resume() {
        final boolean requestApproved = fireEventPipelineResumeRequested();
        if (requestApproved) {
            setState(PipelineState.RUNNING);
            fireEventPipelineResumed();
        }
    }

    /**
     * Returns true iff the pipeline should be stopped.
     */
    public boolean shouldStop() {
        return getState() == PipelineState.STOPPED;
    }

    /**
     * Returns true iff the pipeline should be stopped.
     */
    public boolean shouldPause() {
        return getState() == PipelineState.PAUSED;
    }


    // --------------------
    // Breakpoint Handling
    // --------------------

    /**
     * Adds a breakpoint listener.
     */
    public void addBreakpointListener(final BreakpointListener listener) {
        breakpointListeners.add(listener);
    }

    /**
     * Removes a breakpoint listener.
     */
    public void removeBreakpointListener(final BreakpointListener listener) {
        breakpointListeners.remove(listener);
    }

    /**
     * Fires the event that the pipeline was paused.
     */
    protected synchronized void fireBreakpointEvent(final Operator operator, final List<IOObject> ioContainer, final int location) {
        for (BreakpointListener listener : Collections.synchronizedList(breakpointListeners)) {
            listener.breakpointReached(this, operator, ioContainer, location);
        }
    }
    // -----------------
    // Checks
    // -----------------

    /**
     * Delivers the information about unknown parameter types which occurred during pipeline
     * creation (from streams or files).
     */
    public List<UnknownParameterInformation> getUnknownParameters() {
        return this.unknownParameterInformation;
    }

    /**
     * Clears the information about unknown parameter types which occurred during pipeline creation
     * (from streams or files).
     */
    public void clearUnknownParameters() {
        this.unknownParameterInformation.clear();
    }

    /**
     * Checks for correct number of inner operators, properties, and io.
     */
    public boolean checkProcess(final IOContainer inputContainer) {
        rootOperator.checkAll();
        return true;
    }


    protected void disableBlackListedOperators() throws UserError {
        // make sure security constraints are not violated
        // iterate over all operators in the pipeline
        for (Operator op : rootOperator.getAllInnerOperators()) {
            // we only care about enabled operators
            if (op.isEnabled()) {

                // Check if the given operator is blacklisted
                if (OperatorService.isOperatorBlacklisted(op.getOperatorDescription().getFullyQuallifiedKey())) {
                    throw new UserError(op, "operator_blacklisted");
                }

                // as a side effect mark all enabled operators as dirty
                // so it is clear which ones have already been executed
                op.makeDirty();
            }
        }
    }

    /**
     * Subclasses must implement this method to derive their actual execution developmentContext
     * from the configuration within the development environment. Configuration can be specified
     * using the
     * 
     * @param context
     *            development context
     * @return
     * @throws UndefinedParameterError
     * @throws OperatorException
     */
    public abstract PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) throws OperatorException;

    /**
     * This method needs to return the parameter types which describe the execution environment.
     * 
     * @return
     */
    public List<ParameterType> getExecutionContextParameterTypes() {
        LinkedList<ParameterType> types = new LinkedList<>();
        types.add(new ParameterTypeCategory(ExecutableRootOperator.PARAMETER_LOG_LEVEL,
                "The level of messages that are logged from the pipeline execution in Studio.", LogService.SELECTABLE_LEVEL_NAMES,
                LogService.DEFAULT_LEVEL_INDEX));

        return types;
    }

    /**
     * This starts the pipeline under the given developmentContext of the pipeline
     * 
     * @return results
     * @throws OperatorException
     *             thrown if execption occur
     */
    public final List<IOObject> debug(boolean preserveEmptyResults) throws OperatorException {
        return run(derivePipelineExecutionContext(developmentContext), preserveEmptyResults);
    };


    /**
     * This starts the pipeline under the given context
     * 
     * @param context
     *            execution developmentContext. Needs to be compatible with class requirements.
     * @return results
     * @throws OperatorException
     *             thrown if execution occur
     */
    public final List<IOObject> run(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        return execute(context, preserveEmptyResults);
    };


    /**
     * Executes this pipeline by firing events and starting the root operator. Subclasses can
     * implement pipeline specific behavior that's not actually specific to the root operator here
     * and then call the super method.
     * 
     */
    protected List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        Logger log = getLogger();
        startTimestamp = System.nanoTime();

        try {
            log.info("com.owc.singularity.engine.execution.pipeline_start", getClass().getSimpleName(), getName());
            fireEventPipelineStarting();
            disableBlackListedOperators();
            // determine log level: default
            Level logLevel = Level.WARN;

            // can be set using parameter
            String logLevelParameter = rootOperator.getParameter(ExecutableRootOperator.PARAMETER_LOG_LEVEL);
            if (logLevelParameter != null && !logLevelParameter.isBlank())
                logLevel = Level.valueOf(logLevelParameter);

            // or if set in context, take this as highest priority
            if (context.logLevel != null)
                logLevel = context.logLevel;

            initLogging(logLevel);

            setState(PipelineState.RUNNING);

            RandomGenerator.init(this);

            clearDataTables();
            clearVariables();
            if (getExecutionMode() != ExecutionMode.ONLY_DIRTY) {
                getRootOperator().clear(Port.CLEAR_DATA);
            }

            // add pipeline start variable value here already to have it available for root
            // parameters
            // can be overwritten if it is passed to the run() method via the variable map
            getVariableHandler().addVariable(VariableHandler.PIPELINE_START, VariableHandler.DATE_FORMAT.get().format(new Date(System.currentTimeMillis())));

            this.executionContext = context;
            fireEventPipelineStarted();
            rootOperator.pipelineStarts();
            List<IOObject> results = rootOperator.execute(context, preserveEmptyResults);
            rootOperator.checkForStop();
            fireEventPipelineFinished();

            finishProcess();
            return results;
        } catch (ProcessStoppedException e) {
            fireEventPipelineStopped(e);
            throw e;
        } catch (Exception e) {
            finishProcess(e);
            throw e;
        }
    }

    public String getName() {
        return getPath() != null ? getPath().toShortString(80) : "<unnamed>";
    }

    /**
     * Finishes the pipeline and cleans up everything, including GUI.
     */
    protected void finishProcess() {
        finishProcess(null);
    }

    protected void finishProcess(Exception exception) {
        stop(exception);
        tearDown();
    }

    private void stop(Exception exception) {
        Duration finishedIn = Duration.ofNanos(System.nanoTime() - startTimestamp);
        getLogger().info(exception == null ? "com.owc.singularity.engine.execution.pipeline_success" : "com.owc.singularity.engine.execution.pipeline_fail",
                getClass().getSimpleName(), getName(), Tools.formatDuration(finishedIn));
        setState(PipelineState.STOPPED);
        breakpointUnlock();
        fireEventPipelineStopped(exception);
    }

    /**
     * This method is invoked after a pipeline has finished.
     */
    private void tearDown() {
        try {
            rootOperator.pipelineFinished();
        } catch (OperatorException e) {
            getLogger().warn("Problem during finishing the pipeline: " + e.getMessage(), e);
        }

        // clean up
        clearUnknownParameters();
    }

    /**
     * Returns a &quot;name (i)&quot; if name is already in use. This new name should then be used
     * as operator name.
     */
    public String registerName(final String name, final Operator operator) {
        String newName = ProcessTools.getNewName(operatorNameMap.keySet(), name);
        operatorNameMap.put(newName, operator);
        return newName;
    }

    /**
     * This method is used for unregistering a name from the operator name map.
     */
    public void unregisterName(final String name) {
        operatorNameMap.remove(name);
    }

    public void notifyRenaming(final String oldName, final String newName) {
        rootOperator.notifyRenaming(oldName, newName);
    }

    /**
     * This method is called when the operator given by {@code oldName} (and {@code oldOp} if it is
     * not {@code null}) was replaced with the operator described by {@code newName} and
     * {@code newOp}. This will inform the {@link AbstractRootOperator} of the replacing.
     *
     * @param oldName
     *            the name of the old operator
     * @param oldOp
     *            the old operator; can be {@code null}
     * @param newName
     *            the name of the new operator
     * @param newOp
     *            the new operator; must not be {@code null}
     * @see Operator#notifyReplacing(String, Operator, String, Operator)
     */
    public void notifyReplacing(String oldName, Operator oldOp, String newName, Operator newOp) {
        rootOperator.notifyReplacing(oldName, oldOp, newName, newOp);
    }

    @Override
    public String toString() {
        if (rootOperator == null) {
            return "empty pipeline";
        } else {
            return rootOperator.getXML(true);
        }
    }

    public void addProcessSetupListener(final PipelineSetupListener listener) {
        processSetupListeners.add(PipelineSetupListener.class, listener);
    }

    public void removeProcessSetupListener(final PipelineSetupListener listener) {
        processSetupListeners.remove(PipelineSetupListener.class, listener);
    }

    public void fireDevelopmentContextChanged() {
        for (PipelineSetupListener l : processSetupListeners.getListeners(PipelineSetupListener.class)) {
            l.developmentContextChanged();
        }
    }

    public void fireOperatorAdded(final Operator operator) {
        for (PipelineSetupListener l : processSetupListeners.getListeners(PipelineSetupListener.class)) {
            l.operatorAdded(operator);
        }
    }

    public void fireOperatorChanged(final Operator operator) {
        for (PipelineSetupListener l : processSetupListeners.getListeners(PipelineSetupListener.class)) {
            l.operatorChanged(operator);
        }
    }

    public void fireOperatorRemoved(final Operator operator, final int oldIndex, final int oldIndexAmongEnabled) {
        for (PipelineSetupListener l : processSetupListeners.getListeners(PipelineSetupListener.class)) {
            l.operatorRemoved(operator, oldIndex, oldIndexAmongEnabled);
        }
    }

    public void fireExecutionOrderChanged(final ExecutionUnit unit) {
        for (PipelineSetupListener l : processSetupListeners.getListeners(PipelineSetupListener.class)) {
            l.executionOrderChanged(unit);
        }
    }

    public ExecutionMode getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(final ExecutionMode mode) {
        this.executionMode = mode;
    }

    public DebugMode getDebugMode() {
        return debugMode;
    }

    public void setDebugMode(final DebugMode mode) {
        this.debugMode = mode;
        if (mode == DebugMode.DEBUG_OFF) {
            getRootOperator().clear(Port.CLEAR_REAL_METADATA);
        }
    }

    public DevelopmentExecutionContext getDevelopmentContext() {
        return developmentContext;
    }

    public PipelineExecutionContext getExecutionContext() {
        return executionContext;
    }

    public void setImportMessage(final String importMessage) {
        this.importMessage = importMessage;
    }

    /**
     * This returns true if the pipeline has been imported and ImportRules have been applied during
     * importing. Since the backward compatibility is lost on save, one can warn by retrieving this
     * value.
     */
    public boolean isProcessConverted() {
        return isProcessConverted;
    }

    /**
     * This sets whether the process is converted.
     */
    public void setProcessConverted(final boolean isProcessConverted) {
        this.isProcessConverted = isProcessConverted;
    }

    /**
     * Returns some user readable messages generated during import by {@link XMLImporter}.
     */
    public String getImportMessage() {
        return importMessage;
    }

    // pipeline location (file/repository)

    /**
     * Returns if the pipeline has a valid save location.
     *
     * @return {@code true} iff either a file location is defined or a repository location is
     *         defined AND the repository is not read-only; {@code false} otherwise
     */
    public boolean hasSaveDestination() {
        if (processLocation == null) {
            return false;
        }
        return processLocation.isHead() && !processLocation.getFileSystem().isReadOnly();
    }

    /**
     * 
     * @param pipelinePath
     *            the path where this pipeline is stored in the repository
     */
    public void setPath(final RepositoryPath pipelinePath) {
        // keep process file version if same file, otherwise overwrite
        if (this.processLocation != null && !this.processLocation.equals(pipelinePath)) {
            this.isProcessConverted = false;
            getLogger().debug("Decoupling pipeline from location \"{}\". Pipeline is now associated with file \"{}\".", this.processLocation, pipelinePath);
        }
        this.processLocation = pipelinePath;
    }

    public RepositoryPath getPath() {
        return processLocation;
    }


    /**
     * Checks if breakpoints are present in this pipeline.
     *
     * @return {@code true} if a breakpoint is present. {@code false} otherwise
     * @author Joao Pedro Pinheiro
     */
    public boolean hasBreakpoints() {
        for (Operator op : getAllOperators()) {
            if (op.hasBreakpoint()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Removes all breakpoints from the current pipeline
     *
     * @author Joao Pedro Pinheiro
     */
    public void removeAllBreakpoints() {
        for (Operator op : getAllOperators()) {
            op.setBreakpoint(BreakpointListener.BREAKPOINT_BEFORE, false);
            op.setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, false);
        }
    }

    public void registerLifeCycleEventListener(PipelineLifeCycleEventListener listener) {
        lifeCycleEventListeners.add(listener);
    }

    public void unregisterLifeCycleEventListener(PipelineLifeCycleEventListener listener) {
        lifeCycleEventListeners.remove(listener);
    }

    private List<PipelineLifeCycleEventListener> getLifeCycleEventListeners() {
        return new ArrayList<>(lifeCycleEventListeners);
    }

    protected void fireEventPipelineStarting() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onBeforePipelineStart(this, details);
        }
    }

    protected void fireEventPipelineStarted() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onAfterPipelineStart(this, details);
        }
    }

    protected boolean fireEventPipelinePauseRequested() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            if (!listener.onBeforePipelinePause(this, details)) {
                getLogger().debug("Pipeline pause request has been denied by {}", listener);
                return false;
            }
        }
        return true;
    }

    protected void fireEventPipelinePaused() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onAfterPipelinePause(this, details);
        }
    }

    protected boolean fireEventPipelineResumeRequested() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            if (!listener.onBeforePipelineResume(this, details)) {
                getLogger().debug("Pipeline resume request has been denied by {}", listener);
                return false;
            }
        }
        return true;
    }

    protected void fireEventPipelineResumed() {
        breakpointUnlock();
        LinkedList<BreakpointListener> l;
        synchronized (breakpointListeners) {
            l = new LinkedList<>(breakpointListeners);
        }
        for (BreakpointListener listener : l) {
            listener.resume();
        }

        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onAfterPipelineResume(this, details);
        }
    }

    protected boolean fireEventPipelineStopRequested() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            if (!listener.onBeforePipelineStop(this, details)) {
                getLogger().debug("Pipeline stop request has been denied by {}", listener);
                return false;
            }
        }
        return true;
    }

    protected void fireEventPipelineStopped(Exception exception) {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis(), exception);
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onAfterPipelineStop(this, details);
        }
    }

    protected void fireEventPipelineFinished() {
        ProcessLifeCycleEventDetails details = new ProcessLifeCycleEventDetails(System.currentTimeMillis());
        for (PipelineLifeCycleEventListener listener : getLifeCycleEventListeners()) {
            listener.onAfterPipelineFinish(this, details);
        }
    }

    public TreeVisitResult walk(TreeVisitor<ExecutionUnit, Operator> visitor) {
        return getRootOperator().walk(visitor);
    }

    public boolean isAbstract() {
        for (Operator operator : getAllOperators()) {
            if (operator instanceof DefineAbstractInnerPipelineComposition) {
                return true;
            }
        }
        return false;
    }
}
