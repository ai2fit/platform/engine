/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.DefaultPropertyValueCellEditor;


/**
 * A parameter for passwords. The parameter is written with asteriks in the GUI but can be read in
 * process configuration file. Please make sure that no one but the user can read the password from
 * such a file.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = DefaultPropertyValueCellEditor.class)
public class ParameterTypePassword extends ParameterTypeString {

    private static final long serialVersionUID = 384977559199162363L;

    public ParameterTypePassword(String key, String description) {
        super(key, description, true);
    }

    @Override
    public String getRange() {
        return "password";
    }

    /**
     * This method will be invoked by the Parameters after a parameter was set and will decrypt the
     * given value.
     */
    @Override
    public String fromRawString(String value) {
        // TODO: Re-add decryption here
        return value;
    }


    @Override
    public String toRawString(Object value) {
        // TODO: Re-add encryption here
        return super.toRawString(value);
    }
}
