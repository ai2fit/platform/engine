package com.owc.singularity.engine.pipeline.io.transformer;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * This marks all activated {@link CompatibilityTransformer}. Their execution order is defined by
 * priority.
 *
 * @author Sebastian Land
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface CompatibilityTransformerAnnotation {

    int OPERATOR_INSERTION = 50;
    int PRIORITY_MODULE_REPLACEMENT = 90;
    int OPERATOR_REPLACEMENT = 95;
    int PROCESS_REPLACEMENT = 99;

    int priority() default 100;
}
