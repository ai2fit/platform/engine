/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.pipeline.parameter;


import java.awt.Color;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.function.Function;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.tools.WebServiceTools;
import com.owc.singularity.engine.tools.math.StringToMatrixConverter;


/**
 * Simple {@link ParameterHandler} that relies on a list of {@link ParameterType ParameterTypes}
 * specified by {@link #getParameterTypes()}. Subclasses only need to implement that method.
 *
 * @author Simon Fischer, Dominik Halfkann, Marco Boeck, Adrian Wilke, Jan Czogalla
 */
public abstract class AbstractParameterHandler implements ParameterHandler, Serializable {

    private static final long serialVersionUID = 4802167552195194037L;
    protected Parameters parameters;

    public AbstractParameterHandler(List<ParameterType> types) {
        parameters = new Parameters(types);
    }

    public AbstractParameterHandler() {
        parameters = new Parameters(getParameterTypes());
    }

    public AbstractParameterHandler(ParameterHandler handler) {
        parameters = new Parameters(handler.getParameterTypes());
        handler.getParameters().getKeys().forEach(key -> {
            try {
                parameters.setParameter(key, handler.getParameter(key));
            } catch (UndefinedParameterError e) {
                // can't happen
            }
        });
    }

    @Override
    public String getParameter(String key) throws UndefinedParameterError {
        return getParameters().getRawParameter(key);
    }

    @Override
    public boolean getParameterAsBoolean(String key) {
        try {
            return Boolean.parseBoolean(getParameter(key));
        } catch (UndefinedParameterError e) {
            return false;
        }
    }

    @Override
    public char getParameterAsChar(String key) throws UndefinedParameterError {
        String parameterValue = getParameter(key);
        if (parameterValue != null && !parameterValue.isEmpty()) {
            return parameterValue.charAt(0);
        }
        return 0;
    }

    @Override
    public Color getParameterAsColor(String key) throws UndefinedParameterError {
        String parameterValue = getParameter(key);
        if (parameterValue == null) {
            return Color.BLACK;
        }
        return ParameterTypeColor.string2Color(parameterValue);
    }

    @Override
    public double getParameterAsDouble(String key) throws UndefinedParameterError {
        String value = getParameter(key);
        if (value == null) {
            throw new UndefinedParameterError(key);
        }
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new UndefinedParameterError(key, "Expected real number but found '" + value + "'.");
        }
    }

    @Override
    public InputStream getParameterAsInputStream(String key) throws UndefinedParameterError, IOException {
        String urlString = getParameter(key);
        if (urlString == null) {
            return null;
        }

        try {
            URL url = new URL(urlString);
            return WebServiceTools.openStreamFromURL(url);
        } catch (MalformedURLException e) {
            // URL did not work? Try as file...
            File file = getParameterAsFile(key);
            return file != null ? new FileInputStream(file) : null;
        }
    }

    @Override
    public File getParameterAsFile(String key) throws UndefinedParameterError {
        return getParameterAsFile(key, false);
    }

    @Override
    public File getParameterAsFile(String key, boolean createMissingDirectories) throws UndefinedParameterError {
        String filename = getParameter(key);
        if (filename == null) {
            return null;
        }
        File file = new File(filename);
        return !file.exists() ? null : file;
    }

    @Override
    public int getParameterAsInt(String key) throws UndefinedParameterError {
        return (int) getParameterAsIntegerType(key, s -> Integer.valueOf(s).longValue());
    }

    @Override
    public long getParameterAsLong(String key) throws UndefinedParameterError {
        return getParameterAsIntegerType(key, Long::valueOf);
    }

    @Override
    public double[][] getParameterAsMatrix(String key) throws UndefinedParameterError {
        String matrixLine = getParameter(key);
        try {
            return StringToMatrixConverter.parseMatlabString(matrixLine);
        } catch (OperatorException e) {
            throw new UndefinedParameterError(e.getMessage());
        }
    }

    @Override
    public String getParameterAsString(String key) throws UndefinedParameterError {
        return getParameter(key);
    }

    @Override
    public String getParameterAsString(String key, boolean resolveVariables) throws UndefinedParameterError {
        if (resolveVariables)
            getParameter(key);
        return getParameters().getRawParameter(key);
    }

    @Override
    public List<String[]> getParameterList(String key) throws UndefinedParameterError {
        String value = getParameter(key);
        if (value == null) {
            throw new UndefinedParameterError(key);
        }
        return ParameterTypeList.transformString2ListWithParameterError(value, key);
    }

    @Override
    public String[] getParameterTupel(String key) throws UndefinedParameterError {
        String value = getParameter(key);
        if (value == null) {
            throw new UndefinedParameterError(key);
        }
        return ParameterTypeTupel.transformString2Tupel(value);
    }

    @Override
    public boolean isParameterSet(String key) {
        try {
            return getParameter(key) != null;
        } catch (UndefinedParameterError e) {
            return false;
        }
    }

    @Override
    public void setListParameter(String key, List<String[]> list) {
        getParameters().setRawParameter(key, ParameterTypeList.transformList2String(list));
    }

    @Override
    public void setParameter(String key, String value) {
        getParameters().setRawParameter(key, value);
    }

    @Override
    public Parameters getParameters() {
        return this.parameters;
    }

    /**
     * Parses and returns the value of the given parameter to long/int depending on the parser.
     */
    private long getParameterAsIntegerType(String key, Function<String, Long> parser) throws UndefinedParameterError {
        ParameterType type = getParameters().getParameterType(key);
        String value = getParameter(key);
        if (type instanceof ParameterTypeCategory) {
            try {
                return parser.apply(value);
            } catch (NumberFormatException e) {
                ParameterTypeCategory categoryType = (ParameterTypeCategory) type;
                return categoryType.getIndex(value);
            }
        }
        try {
            return parser.apply(value);
        } catch (NumberFormatException e) {
            throw new UndefinedParameterError(key, "Expected integer/long but found '" + value + "'.");
        }
    }
}
