package com.owc.singularity.engine.pipeline.io.transformer;

import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * A {@link ProcessReplacementTransformer} to replace all mentions of the deprecated "macro" tag
 * with its updated "variable" tag.
 */
@CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_INSERTION)
public class MacroToVariableTransformer extends ProcessReplacementTransformer {

    private final XPathExpression processExpression;

    public MacroToVariableTransformer() throws Exception {
        processExpression = getXpath().compile("//macro");
    }

    @Override
    protected XPathExpression getProcessExpression() {
        return processExpression;
    }

    @Override
    protected void applyOnProcess(Node processNode, Document document, CompatibilityTransformationLog log) throws Exception {
        document.renameNode(processNode, null, "variable");
    }
}
