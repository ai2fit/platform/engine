package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.operator.CompositeDummyOperator;
import com.owc.singularity.engine.tools.container.Pair;

/**
 * This transformer replaces all unknown operators with a dummy operator. It is executed pretty late
 * in order to give all other transformers a chance to fix issues first, before we resort in
 * replacing unknown operators. <br>
 * It will preserve the ports, parameters and the type of the operator, so that the operator can
 * later be inserted again, without changes to the original process design, once known. See
 * {@link ReknownDummyOperatorTransformer}.
 * 
 * @author Sebastian Land
 *
 */
@CompatibilityTransformerAnnotation(priority = 1000)
public class UnknownOperatorTransformer extends AbstractXPathCompatibilityTransformer {

    public UnknownOperatorTransformer() throws Exception {
        super("//operator");
    }

    @Override
    protected void apply(Node operatorNode, CompatibilityTransformationLog log, Document document) {
        try {

            Document doc = operatorNode.getOwnerDocument();
            NamedNodeMap attributes = operatorNode.getAttributes();
            String operatorName = attributes.getNamedItem("name").getNodeValue();
            Node classAttribute = attributes.getNamedItem("class");
            String formerOperatorKey = classAttribute.getNodeValue();
            if (OperatorService.getOperatorDescription(formerOperatorKey) == null) {

                XPath xpath = xpathfactory.newXPath();
                // now we search for original parameters and lists
                {
                    LinkedHashMap<String, String> parameterMap = searchParameters(operatorNode, xpath);
                    HashMap<String, List<Pair<String, String>>> listValues = searchListValues(operatorNode, xpath, operatorName);

                    // save parameters and lists in new parameter
                    Element parametersListElement = doc.createElement("list");
                    parametersListElement.setAttribute("key", CompositeDummyOperator.PARAMETER_PARAMETERS);
                    operatorNode.appendChild(parametersListElement);

                    parameterMap.forEach((parameterKey, parameterValue) -> {
                        Element parameterListElement = doc.createElement("parameter");
                        parameterListElement.setAttribute("key", parameterKey);
                        parameterListElement.setAttribute("value", parameterValue);
                        parametersListElement.appendChild(parameterListElement);
                    });

                    Element newParametersListElement = doc.createElement("list");
                    newParametersListElement.setAttribute("key", CompositeDummyOperator.PARAMETER_LISTS);

                    listValues.forEach((listKey, listValue) -> {
                        for (Pair<String, String> pair : listValue) {
                            OperatorReplacementTransformer.createParameter(doc, newParametersListElement, listKey, pair.getFirst() + ": " + pair.getSecond());
                        }
                    });
                    operatorNode.appendChild(newParametersListElement);
                }

                // after searching and recording parameters, we save the original operator class and
                // replace it
                Element parameterElement = doc.createElement("parameter");
                parameterElement.setAttribute("key", CompositeDummyOperator.PARAMETER_REPLACES);
                parameterElement.setAttribute("value", formerOperatorKey);
                operatorNode.appendChild(parameterElement);
                classAttribute.setNodeValue("core:dummy");

                // now we search for necessary replaces in ports
                {
                    LinkedHashMap<String, String> inputPortMap = searchAndReplaceDirectInputPorts(xpath, operatorName, doc);
                    LinkedHashMap<String, String> outputPortMap = searchAndReplaceDirectOutputPorts(xpath, operatorName, doc);

                    // preserve original ports in new parameter
                    Element inputPortParameterElement = doc.createElement("list");
                    inputPortParameterElement.setAttribute("key", CompositeDummyOperator.PARAMETER_INPUT_PORT_REPLACEMENTS);
                    operatorNode.appendChild(inputPortParameterElement);

                    inputPortMap.forEach((newPort, oldPort) -> {
                        Element inputPortParameterListElement = doc.createElement("parameter");
                        inputPortParameterListElement.setAttribute("key", newPort);
                        inputPortParameterListElement.setAttribute("value", oldPort);
                        inputPortParameterElement.appendChild(inputPortParameterListElement);
                    });

                    Element outputPortParameterElement = doc.createElement("list");
                    outputPortParameterElement.setAttribute("key", CompositeDummyOperator.PARAMETER_OUTPUT_PORT_REPLACEMENTS);
                    operatorNode.appendChild(outputPortParameterElement);

                    outputPortMap.forEach((newPort, oldPort) -> {
                        Element outputPortParameterListElement = doc.createElement("parameter");
                        outputPortParameterListElement.setAttribute("key", newPort);
                        outputPortParameterListElement.setAttribute("value", oldPort);
                        outputPortParameterElement.appendChild(outputPortParameterListElement);
                    });
                }

                // possible inner pipelines
                {
                    NodeList pipelineNodes = (NodeList) xpath.compile("process|pipeline").evaluate(operatorNode, XPathConstants.NODESET);
                    if (pipelineNodes.getLength() != 0) {
                        // replace dummy operator to new composite dummy operator, which has a inner
                        // pipeline
                        classAttribute.setNodeValue("core:dummy_composite");

                        // prepare parameter to put replaced connections into
                        Element inputPortParameterElement = doc.createElement("list");
                        inputPortParameterElement.setAttribute("key", CompositeDummyOperator.PARAMETER_INNER_PIPELINE_INPUT_PORTS);
                        operatorNode.appendChild(inputPortParameterElement);
                        AtomicInteger count = new AtomicInteger();
                        Element outputPortParameterElement = doc.createElement("list");
                        outputPortParameterElement.setAttribute("key", CompositeDummyOperator.PARAMETER_INNER_PIPELINE_OUTPUT_PORTS);
                        operatorNode.appendChild(outputPortParameterElement);

                        // search in pipeline nodes for connections and replace them
                        for (int i = 0; i < pipelineNodes.getLength(); i++) {
                            LinkedHashMap<String, String> outputPortMap = new LinkedHashMap<>();
                            LinkedHashMap<String, String> inputPortMap = new LinkedHashMap<>();

                            Node pipelineNode = pipelineNodes.item(i);
                            NodeList outputPortList = (NodeList) xpath.compile("connect[not(@to_op)]").evaluate(pipelineNode, XPathConstants.NODESET);
                            HashSet<String> usedOutputPortIndex = new HashSet<>();
                            for (int inIndex = 0; inIndex < outputPortList.getLength(); inIndex++) {
                                Node connectionNode = outputPortList.item(inIndex);
                                NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                                Node toPortNode = connectionAttributes.getNamedItem("to_port");
                                String portNumber = toPortNode.getNodeValue().replaceAll("[^0-9]", "");
                                portNumber = portNumber.isBlank() ? String.valueOf(1) : portNumber;
                                int possiblePortIndex = Integer.parseInt(portNumber);
                                while (usedOutputPortIndex.contains(String.valueOf(possiblePortIndex))) {
                                    possiblePortIndex++;
                                }
                                String newPort = "through " + possiblePortIndex;
                                outputPortMap.put(newPort, toPortNode.getNodeValue());
                                toPortNode.setNodeValue(newPort);
                                usedOutputPortIndex.add(String.valueOf(possiblePortIndex));
                            }

                            NodeList inputPortList = (NodeList) xpath.compile("connect[not(@from_op)]").evaluate(pipelineNode, XPathConstants.NODESET);
                            HashSet<String> usedInputPortIndex = new HashSet<>();
                            for (int inIndex = 0; inIndex < inputPortList.getLength(); inIndex++) {
                                Node connectionNode = inputPortList.item(inIndex);
                                NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                                Node fromPortNode = connectionAttributes.getNamedItem("from_port");
                                String portNumber = fromPortNode.getNodeValue().replaceAll("[^0-9]", "");
                                portNumber = portNumber.isBlank() ? String.valueOf(1) : portNumber;
                                int possiblePortIndex = Integer.parseInt(portNumber);
                                while (usedInputPortIndex.contains(String.valueOf(possiblePortIndex))) {
                                    possiblePortIndex++;
                                }
                                String newPort = "through " + possiblePortIndex;
                                inputPortMap.put(newPort, fromPortNode.getNodeValue());
                                fromPortNode.setNodeValue(newPort);
                                usedInputPortIndex.add(String.valueOf(possiblePortIndex));
                            }

                            outputPortMap.forEach((newPort, oldPort) -> {
                                Element outputPortParameterListElement = doc.createElement("parameter");
                                outputPortParameterListElement.setAttribute("key", String.valueOf(count.get()));
                                outputPortParameterListElement.setAttribute("value", newPort + "." + oldPort);
                                outputPortParameterElement.appendChild(outputPortParameterListElement);
                            });

                            inputPortMap.forEach((newPort, oldPort) -> {
                                Element outputPortParameterListElement = doc.createElement("parameter");
                                outputPortParameterListElement.setAttribute("key", String.valueOf(count.get()));
                                outputPortParameterListElement.setAttribute("value", newPort + "." + oldPort);
                                inputPortParameterElement.appendChild(outputPortParameterListElement);
                            });
                            count.getAndIncrement();
                        }
                    }

                }
                log.log(Level.WARN, "Detected unknown operator '" + operatorName + "' of type '" + formerOperatorKey
                        + "'. Replaced by Dummy Operator to preserve flow, but cannot execute pipeline until resolved.");
            }

        } catch (Exception e) {
            log.log(Level.ERROR, "Detected unknown operator but cannot preserve as dummy operator: " + e.getMessage());
        }
    }

    private static LinkedHashMap<String, String> searchAndReplaceDirectOutputPorts(XPath xpath, String operatorName, Document doc)
            throws XPathExpressionException {
        LinkedHashMap<String, String> outputPortMap = new LinkedHashMap<>();
        NodeList outputPortList = (NodeList) xpath.compile("//connect[@from_op='" + operatorName + "']").evaluate(doc, XPathConstants.NODESET);
        for (int inIndex = 0; inIndex < outputPortList.getLength(); inIndex++) {
            Node connectionNode = outputPortList.item(inIndex);
            NamedNodeMap connectionAttributes = connectionNode.getAttributes();
            Node fromPortNode = connectionAttributes.getNamedItem("from_port");
            String newPort = "through " + (inIndex + 1);
            outputPortMap.put(newPort, fromPortNode.getNodeValue());
            fromPortNode.setNodeValue(newPort);
        }
        return outputPortMap;
    }

    private static LinkedHashMap<String, String> searchAndReplaceDirectInputPorts(XPath xpath, String operatorName, Document doc)
            throws XPathExpressionException {
        LinkedHashMap<String, String> inputPortMap = new LinkedHashMap<>();
        NodeList inputPortList = (NodeList) xpath.compile("//connect[@to_op='" + operatorName + "']").evaluate(doc, XPathConstants.NODESET);
        for (int inIndex = 0; inIndex < inputPortList.getLength(); inIndex++) {
            Node connectionNode = inputPortList.item(inIndex);
            NamedNodeMap connectionAttributes = connectionNode.getAttributes();
            Node toPortNode = connectionAttributes.getNamedItem("to_port");
            String newPort = "through " + (inIndex + 1);
            inputPortMap.put(newPort, toPortNode.getNodeValue());
            toPortNode.setNodeValue(newPort);
        }
        return inputPortMap;
    }

    private static HashMap<String, List<Pair<String, String>>> searchListValues(Node operatorNode, XPath xpath, String operatorName)
            throws XPathExpressionException {
        HashMap<String, List<Pair<String, String>>> listValues = new HashMap<>();
        Object listResult = xpath.compile("list|enumeration").evaluate(operatorNode, XPathConstants.NODESET);
        NodeList listNodes = (NodeList) listResult;
        for (int i = 0; i < listNodes.getLength(); i++) {
            Node listNode = listNodes.item(i);
            if (!listNode.getParentNode().getAttributes().getNamedItem("name").getNodeValue().equals(operatorName)) {
                continue;
            }
            NamedNodeMap listNodeAttributes = listNode.getAttributes();
            Node keyAttribute = listNodeAttributes.getNamedItem("key");
            String listParameterKey = keyAttribute.getNodeValue();
            NodeList childNodes = listNode.getChildNodes();
            List<Pair<String, String>> listParametersMap = new LinkedList<>();
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node childNode = childNodes.item(j);
                NamedNodeMap childAttributes = childNode.getAttributes();
                if (childAttributes != null) {
                    Node childKeyAttribute = childAttributes.getNamedItem("key");
                    Node childValueAttribute = childAttributes.getNamedItem("value");
                    String childParameterKey = childKeyAttribute.getNodeValue();
                    // this could be a placeholder parameter
                    if (childValueAttribute != null) {
                        listParametersMap.add(new Pair<>(childParameterKey, childValueAttribute.getNodeValue()));
                    }
                }
            }
            listValues.put(listParameterKey, listParametersMap);
        }
        return listValues;
    }

    private LinkedHashMap<String, String> searchParameters(Node operatorNode, XPath xpath) throws XPathExpressionException {
        LinkedHashMap<String, String> parameterMap = new LinkedHashMap<>();
        NodeList parameterNodes = (NodeList) xpath.compile("parameter").evaluate(operatorNode, XPathConstants.NODESET);
        for (int inIndex = 0; inIndex < parameterNodes.getLength(); inIndex++) {
            Node parameterNode = parameterNodes.item(inIndex);
            NamedNodeMap connectionAttributes = parameterNode.getAttributes();
            if (!Objects.equals(connectionAttributes.getNamedItem("key").getNodeValue(), CompositeDummyOperator.PARAMETER_REPLACES))
                parameterMap.put(connectionAttributes.getNamedItem("key").getNodeValue(), connectionAttributes.getNamedItem("value").getNodeValue());
        }
        return parameterMap;
    }
}
