/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.SimpleSuggestionBoxValueCellEditor;


/**
 *
 * A parameter type providing dynamic suggestions to the user via the provided
 * {@link SuggestionProvider}.
 *
 * @author Nils Woehler
 */
@ParameterTypeAnnotation(editor = SimpleSuggestionBoxValueCellEditor.class)
public class ParameterTypeSuggestion extends ParameterTypeString {

    private static final long serialVersionUID = 1L;

    private SuggestionProvider<?> provider;


    public ParameterTypeSuggestion(String key, String description, SuggestionProvider<?> provider) {
        this(key, description, provider, null, true);
    }

    public ParameterTypeSuggestion(String key, String description, SuggestionProvider<?> provider, boolean optional) {
        this(key, description, provider, null, optional);
    }

    public ParameterTypeSuggestion(String key, String description, SuggestionProvider<?> provider, String defaultValue) {
        this(key, description, provider, defaultValue, true);
    }

    public ParameterTypeSuggestion(String key, String description, SuggestionProvider<?> provider, String defaultValue, boolean optional) {
        super(key, description, defaultValue);
        this.provider = provider;
        setOptional(optional);
    }

    public SuggestionProvider<?> getSuggestionProvider() {
        return provider;
    }

}
