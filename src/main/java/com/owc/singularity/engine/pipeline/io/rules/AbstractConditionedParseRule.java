/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.engine.pipeline.io.conditions.ParameterEqualsCondition;
import com.owc.singularity.engine.pipeline.io.conditions.ParameterUnequalsCondition;
import com.owc.singularity.engine.pipeline.io.conditions.ParseRuleCondition;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * This is the super class of all ParseRules with depend on one or more conditions.
 * 
 * @author Sebastian Land
 * 
 */
public abstract class AbstractConditionedParseRule extends AbstractOperatorParseRule {

    private final List<ParseRuleCondition> conditions = new LinkedList<ParseRuleCondition>();

    public AbstractConditionedParseRule(String operatorTypeName, Element element) throws XMLException {
        super(operatorTypeName, element);
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element childElem) {
                if (childElem.getTagName().equals("condition")) {
                    parseCondition(childElem);
                }
            }
        }
    }

    protected void parseCondition(Element childElem) {
        NodeList conditionNodes = childElem.getChildNodes();
        for (int j = 0; j < conditionNodes.getLength(); j++) {
            Node conditionNode = conditionNodes.item(j);
            if (conditionNode instanceof Element conditionElem) {
                if (conditionElem.getTagName().equals("parameter_equals")) {
                    conditions.add(new ParameterEqualsCondition((Element) conditionNode));
                } else if (conditionElem.getTagName().equals("parameter_unequals")) {
                    conditions.add(new ParameterUnequalsCondition((Element) conditionNode));
                } else {
                    LogService.getRoot()
                            .warn("com.owc.singularity.engine.pipeline.io.rules.AbstractConditionedParseRule.unknown_condition", conditionElem.getTagName());
                }
            }
        }
    }

    @Override
    protected String apply(Operator operator, String operatorTypeName, XMLPipelineParser parser) {
        for (ParseRuleCondition condition : conditions) {
            if (!condition.isSatisfied(operator)) {
                return null;
            }
        }
        return conditionedApply(operator, operatorTypeName, parser);
    }

    protected abstract String conditionedApply(Operator operator, String operatorTypeName, XMLPipelineParser parser);
}
