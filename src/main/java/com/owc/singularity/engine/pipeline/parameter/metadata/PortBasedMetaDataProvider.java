package com.owc.singularity.engine.pipeline.parameter.metadata;


import java.util.function.Consumer;
import java.util.function.Supplier;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.ports.InputPort;

public class PortBasedMetaDataProvider implements Supplier<ExampleSetMetaData> {

    private final InputPort port;
    private Consumer<ExampleSetMetaData.ExampleSetMetaDataTransformer> transformerFunction;

    public PortBasedMetaDataProvider(InputPort port) {
        this(port, exampleSetMetaDataTransformer -> {});
    }

    public PortBasedMetaDataProvider(InputPort port, Consumer<ExampleSetMetaData.ExampleSetMetaDataTransformer> transformerFunction) {
        this.port = port;
        this.transformerFunction = transformerFunction;
    }

    @Override
    public ExampleSetMetaData get() {
        if (port != null) {
            return (ExampleSetMetaData) port.getMetaData();
        }
        return null;
    }

    public InputPort getInputPort() {
        return port;
    }
}
