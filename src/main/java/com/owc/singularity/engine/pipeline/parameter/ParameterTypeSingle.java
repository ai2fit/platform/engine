/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.engine.pipeline.VariableHandler;


/**
 * An abstract superclass for single, i.e. non-list, parameters.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
public abstract class ParameterTypeSingle extends ParameterType {

    private static final long serialVersionUID = 1144201124955949715L;

    public ParameterTypeSingle(String key, String description) {
        super(key, description);
    }

    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) throws UndefinedParameterError {
        return variableHandler.resolveVariables(getKey(), parameterValue);
    }
}
