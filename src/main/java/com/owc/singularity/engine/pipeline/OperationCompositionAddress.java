package com.owc.singularity.engine.pipeline;

public record OperationCompositionAddress(Operation[] parentOperations, OperationComposition[] parentCompositions) {

    public OperationCompositionAddress(OperationAddress parentAddress, OperationComposition composition) {
        this(concat(parentAddress.compositionAddress().parentOperations, parentAddress.operation()),
                concat(parentAddress.compositionAddress().parentCompositions(), composition));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < parentCompositions.length; i++) {
            builder.append(parentOperations[i].getName());
            builder.append(":");
            builder.append(parentCompositions[i].getName());
            builder.append("/");
        }
        return builder.toString();
    }

    private static OperationComposition[] concat(OperationComposition[] parents, OperationComposition child) {
        OperationComposition[] newParents = new OperationComposition[parents.length + 1];
        System.arraycopy(parents, 0, newParents, 0, parents.length);
        newParents[parents.length] = child;
        return newParents;
    }

    private static Operation[] concat(Operation[] parents, Operation child) {
        Operation[] newParents = new Operation[parents.length + 1];
        System.arraycopy(parents, 0, newParents, 0, parents.length);
        newParents[parents.length] = child;
        return newParents;
    }


    /**
     * This checks whether the given address is within the scope of this composition address. In
     * essence it compares whether the targetOperation is a child of this address.
     * 
     * @param operation
     *            the operation to check
     * @return true if it is a child
     */
    public boolean isInScope(OperationAddress operation) {
        OperationComposition[] targetParents = operation.compositionAddress().parentCompositions();
        if (parentCompositions.length <= targetParents.length) {
            for (int i = 0; i < parentCompositions.length; i++) {
                if (parentCompositions[i] != targetParents[i])
                    return false;
            }
            return true;
        }
        return false;
    }

    /**
     * This checks whether the given scope is within the scope of this composition execution
     * address. In essence it compares whether the targetOperation is a child of this address and if
     * they are in the same iterations each.
     * 
     * @param scope
     *            the operation to check
     * @return true if it is a child
     */
    public boolean isInScope(OperationCompositionAddress scope) {
        if (parentCompositions.length <= scope.parentCompositions.length) {
            for (int i = 0; i < parentCompositions.length; i++) {
                if (parentCompositions[i] != scope.parentCompositions[i])
                    return false;
            }
            return true;
        }
        return false;
    }

}
