/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.ModelMetaData;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetHeader;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.pipeline.parameter.metadata.PortBasedMetaDataProvider;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.studio.gui.parameters.celleditors.value.AttributeValueCellEditor;
import com.owc.singularity.tools.AlphanumComparator;


/**
 * This attribute type supports the user by letting him select an attribute name from a combo box of
 * known attribute names. For long lists, auto-completion and filtering of the drop-down menu eases
 * the handling. For knowing attribute names before process execution a valid metadata
 * transformation must be performed. Otherwise, the user might type in the name, instead of
 * choosing.
 *
 * @author Sebastian Land
 */
@ParameterTypeAnnotation(editor = AttributeValueCellEditor.class)
public class ParameterTypeAttribute extends ParameterTypeString {

    private static final long serialVersionUID = -4177652183651031337L;


    // private transient InputPort inPort;
    private transient final Supplier<ExampleSetMetaData> metaDataSupplier;

    private ValueType[] allowedValueTypes;


    public ParameterTypeAttribute(final String key, String description, InputPort inPort) {
        this(key, description, inPort, false);
    }

    public ParameterTypeAttribute(final String key, String description, InputPort inPort, ValueType... valueTypes) {
        this(key, description, inPort, false, valueTypes);
    }

    public ParameterTypeAttribute(final String key, String description, final InputPort inPort, boolean optional, ValueType... valueTypes) {
        this(key, description, new PortBasedMetaDataProvider(inPort), optional, valueTypes);
    }

    public ParameterTypeAttribute(final String key, String description, Supplier<ExampleSetMetaData> metaDataSupplier, boolean optional,
            ValueType... valueTypes) {
        super(key, description, optional);
        this.metaDataSupplier = metaDataSupplier;
        if (valueTypes.length == 0) {
            allowedValueTypes = ValueType.values();
        } else {
            allowedValueTypes = valueTypes;
        }
    }

    public Vector<String> getAttributeNames() {
        return getAttributeNamesAndTypes(true).stream().map(Pair::getFirst).collect(Collectors.toCollection(Vector::new));
    }

    /**
     * Returns the attribute names and their value types, can sort if desired.
     *
     * @param sortAttributes
     *            if {@code true}, will sort alpha-numerically; if {@code false} will not sort at
     *            all
     * @return the vector of pairs between the attribute names and their value type
     */
    public List<Pair<String, ValueType>> getAttributeNamesAndTypes(boolean sortAttributes) {
        List<Pair<String, ValueType>> names = new ArrayList<>();
        List<Pair<String, ValueType>> regularNames = new ArrayList<>();
        MetaData metaData = null;
        try {
            metaData = getMetaData();
        } catch (Exception ignored) {
        }
        if (metaData == null) {
            return names;
        }
        ExampleSetMetaData emd = null;
        if (metaData instanceof ExampleSetMetaData) {
            emd = (ExampleSetMetaData) metaData;
        } else if (metaData instanceof ModelMetaData mmd) {
            ExampleSetHeader trainingSetHeader = mmd.getTrainingSetHeader();
            String[] attributeNames = IntStream.range(0, trainingSetHeader.getNumberOfAttributes())
                    .mapToObj(trainingSetHeader::getAttributeName)
                    .toArray(String[]::new);
            ValueType[] attributeValueType = IntStream.range(0, trainingSetHeader.getNumberOfAttributes())
                    .mapToObj(trainingSetHeader::getAttributeType)
                    .toArray(ValueType[]::new);
            String[] attributeRoles = IntStream.range(0, trainingSetHeader.getNumberOfAttributes())
                    .mapToObj(trainingSetHeader::getAttributeRole)
                    .toArray(String[]::new);
            emd = ExampleSetMetaData.of(attributeNames, attributeValueType, attributeRoles);
        }
        if (emd == null) {
            return names;
        }
        for (AttributeMetaData amd : emd.getAllAttributes()) {
            if (!isFilteredOut(amd, emd) && isOfAllowedType(amd.getValueType())) {
                Pair<String, ValueType> nameAndType = new Pair<>(amd.getName(), amd.getValueType());
                if (emd.getRole(amd) != null) {
                    names.add(nameAndType);
                } else {
                    regularNames.add(nameAndType);
                }
            }
        }

        if (sortAttributes && (!names.isEmpty() || !regularNames.isEmpty())) {
            AlphanumComparator alphanumComparator = new AlphanumComparator(AlphanumComparator.AlphanumCaseSensitivity.INSENSITIVE);
            names.sort((o1, o2) -> alphanumComparator.compare(o1.getFirst(), o2.getFirst()));
            regularNames.sort((o1, o2) -> alphanumComparator.compare(o1.getFirst(), o2.getFirst()));
        }

        names.addAll(regularNames);

        return names;
    }

    /**
     * Checks if the given value type is allowed for this parameter type, aka the type is identical
     * or a subtype of an allowed type.
     *
     * @param valueType
     *            the value type that should be checked
     * @return {@code true} if the given value type is allowed for this parameter type;
     *         {@code false} otherwise
     */
    public boolean isOfAllowedType(ValueType valueType) {
        if (valueType == null)
            return false;
        boolean isAllowed = false;
        for (ValueType type : allowedValueTypes) {
            isAllowed |= Objects.equals(type, valueType);
        }
        return isAllowed;
    }

    @Override
    public Object getDefaultValue() {
        return "";
    }

    /**
     * This method might be overridden by subclasses in order to select attributes which are
     * applicable
     */
    protected boolean isFilteredOut(AttributeMetaData amd, ExampleSetMetaData emd) {
        return false;
    }

    /** Returns the metadata currently available by the {@link #metaDataSupplier}. */
    public MetaData getMetaData() {
        return metaDataSupplier.get();
    }
}
