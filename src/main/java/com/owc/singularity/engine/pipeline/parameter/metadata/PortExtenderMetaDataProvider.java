package com.owc.singularity.engine.pipeline.parameter.metadata;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeAttribute;
import com.owc.singularity.engine.ports.InputPortGroup;

/**
 * This class can be used in order to select attributes from a PortExtender for a
 * {@link ParameterTypeAttribute}
 *
 * @author Sebastian Land
 *
 */
public class PortExtenderMetaDataProvider implements Supplier<ExampleSetMetaData> {

    private InputPortGroup portGroup;
    private boolean intersect;

    public PortExtenderMetaDataProvider(InputPortGroup portGroup, boolean intersect) {
        this.portGroup = portGroup;
        this.intersect = intersect;
    }

    @Override
    public ExampleSetMetaData get() {
        if (portGroup != null) {
            List<MetaData> metaDataList = portGroup.getMetaData(true);
            ExampleSetMetaData.ExampleSetMetaDataTransformer transformer = ExampleSetMetaData.emptyMetaData().transform();
            Collection<AttributeMetaData> intersectionMetaData = new ArrayList<>();
            boolean first = true;
            for (MetaData metaData : metaDataList) {
                if (metaData instanceof ExampleSetMetaData emdNewSet) {
                    if (!intersect) {
                        // then it's first
                        emdNewSet.getAllAttributes()
                                .forEach(attributeMetaData -> transformer.withDerivedAttribute(attributeMetaData.getName(), attributeMetaData.getValueType()));
                    } else {
                        if (first) {
                            intersectionMetaData.addAll(emdNewSet.getAllAttributes());
                            first = false;
                        } else {
                            intersectionMetaData.retainAll(emdNewSet.getAllAttributes());
                        }
                    }
                }
            }
            for (AttributeMetaData attributeMetaData : intersectionMetaData)
                transformer.withDerivedAttribute(attributeMetaData.getName(), attributeMetaData.getValueType());

            ExampleSetMetaData emdResultSet = transformer.transform();
            if (metaDataList.isEmpty())
                return null;
            else if (!emdResultSet.getAllAttributes().isEmpty())
                return emdResultSet;
            else
                return (ExampleSetMetaData) metaDataList.get(0);
        } else {
            return null;
        }
    }
}
