package com.owc.singularity.engine.pipeline;


import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.ImplementingRootOperator;

/**
 * This is a container for an actual implementation of a defined abstract composition within a
 * pipeline.
 */
public class CompositionImplementation {

    private final int executionUnitIndex;
    private final ImplementingRootOperator executionUnitHost;

    public CompositionImplementation(ImplementingRootOperator executionUnitHost, int executionUnitIndex) {
        super();
        this.executionUnitHost = executionUnitHost;
        this.executionUnitIndex = executionUnitIndex;
    }

    public ExecutionUnit getExecutionUnit() {
        return executionUnitHost.getSubprocess(executionUnitIndex);
    }
}
