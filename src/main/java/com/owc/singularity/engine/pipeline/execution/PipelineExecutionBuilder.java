package com.owc.singularity.engine.pipeline.execution;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.DevelopmentExecutionContext;
import com.owc.singularity.engine.pipeline.PipelineExecutionContext;
import com.owc.singularity.repository.RepositoryPath;

/**
 * This is the central api for configuring the execution of a pipeline.
 */
public class PipelineExecutionBuilder {

    private final List<PipelineExecutionListener> lifeCycleEventListeners = new LinkedList<>();
    private Map<RepositoryPath, Level> pathLogLevelMap = new HashMap<>();
    private AbstractPipeline pipeline;
    private ForkJoinPool pool = ForkJoinPool.commonPool();
    private DevelopmentExecutionContext developmentContext;
    private PipelineExecutionContext context;

    private PipelineExecutionBuilder(AbstractPipeline pipeline) {
        this.pipeline = pipeline;

    }

    /**
     * This method allows to add a {@link PipelineExecutionListener} to the execution.
     * 
     * @param listener
     *            the listener
     * @return this builder
     */
    public PipelineExecutionBuilder withExecutionListener(PipelineExecutionListener listener) {
        lifeCycleEventListeners.add(listener);
        return this;
    }

    /**
     * This method enforces the logLevel for the executed pipeline and overrides the log level as
     * specified in the pipeline parameters. Please note that other pipelines executed in by calls
     * within the pipeline will not be affected. In order to change them, use
     * {@link #withLogLevel(RepositoryPath, Level)}.
     * 
     * @param logLevel
     *            the log level for the executed pipeline
     * @return this builder
     */
    public PipelineExecutionBuilder withLogLevel(Level logLevel) {
        this.pathLogLevelMap.put(pipeline.getPath(), logLevel);
        return this;
    }

    /**
     * This method can be called if a {@link DevelopmentPipelineContext} should be taken into
     * account for this execution. Cannot be used together with
     * {@link #withExecutionContext(PipelineContext)}
     * 
     * @param context
     *            the context
     * @return this builder
     */
    public PipelineExecutionBuilder withDevelopmentContext(DevelopmentExecutionContext context) {
        if (context != null)
            throw new IllegalStateException("Development context cannot be used when context already set.");
        this.developmentContext = context;
        return this;
    }

    /**
     * This method can be called if a {@link PipelineContext} should be taken into account for this
     * execution. Cannot be used together with
     * {@link #withDevelopmentContext(DevelopmentPipelineContext)}
     * 
     * @param context
     *            the context
     * @return this builder
     */
    public PipelineExecutionBuilder withExecutionContext(PipelineExecutionContext context) {
        if (developmentContext != null)
            throw new IllegalStateException("Context cannot be used when development context already set.");
        this.context = context;
        return this;
    }


    /**
     * This method enforces the log level for pipelines specified by path. This will affect all
     * pipelines which are stored below path and for that no more specific setting is made.<br>
     * Please note that a null path will affect execution of pipelines that have no path associated.
     * 
     * @param path
     *            specifies the path
     * @param logLevel
     *            specifies the log level for all pipelines below path
     * @return this builder
     */
    public PipelineExecutionBuilder withLogLevel(RepositoryPath path, Level logLevel) {
        this.pathLogLevelMap.put(path, logLevel);
        return this;
    }

    /**
     * This can be used to specify a ForkJoinPool to use for this particular execution of the
     * pipeline. Using this, properties like number of parallel threads and their priority can be
     * determined easily. All tasks will be executed within the given pool.<br>
     * Without specifying this, the {@link ForkJoinPool#commonPool()}will be used.
     * 
     * @param pool
     *            the pool to use
     * @return this builder
     */
    public PipelineExecutionBuilder using(ForkJoinPool pool) {
        this.pool = pool;
        return this;
    }

    /**
     * This configures and starts a {@link PipelineExecution}
     * 
     * @return the execution as handle on results and for interaction
     */
    public PipelineExecution start() {
        if (developmentContext == null && context == null)
            throw new IllegalArgumentException("Either the ExecutionContext or the DevelopmentContext must be set.");
        return new PipelineExecution(pipeline.clone(), context, developmentContext, lifeCycleEventListeners, pathLogLevelMap, pool);
    }

    /**
     * This method allows you to create the builder for the given pipeline.
     * 
     * @param pipeline
     *            the pipeline to execute
     * @return the builder for the execution configuration
     */
    public static PipelineExecutionBuilder configure(AbstractPipeline pipeline) {
        return new PipelineExecutionBuilder(pipeline);
    }
}
