package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.*;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

import com.owc.singularity.engine.operator.CompositeDummyOperator;
import com.owc.singularity.engine.tools.container.Pair;

/**
 * This transformer will replace dummy operators with known operators. This happens fairly early in
 * the transformer priority, as then other, operator specific transformers can then be applied. E.g.
 * if an operator has been replaced, and comes from a newly installed extension, then in the xml dom
 * the original operator information has to be restored, before the operator specific replacement
 * transformer can be applied.<br>
 * This transformer therefore transforms ALL dummy operators back. If operators are still unknown,
 * they will again be transformed into a dummy operator by the {@link UnknownOperatorTransformer},
 * which is applied later.
 * 
 * @author Sebastian Land
 *
 */
@CompatibilityTransformerAnnotation(priority = 10)
public class ReknownDummyOperatorTransformer extends AbstractXPathCompatibilityTransformer {

    private XPath xpath = xpathfactory.newXPath();

    public ReknownDummyOperatorTransformer() throws Exception {
        super("//operator[@class='core:dummy']|//operator[@class='core:dummy_composite']");
    }

    @Override
    protected void apply(Node operatorNode, CompatibilityTransformationLog log, Document document) {
        Document doc = operatorNode.getOwnerDocument();
        NamedNodeMap attributes = operatorNode.getAttributes();
        String operatorName = attributes.getNamedItem("name").getNodeValue();
        Node classAttribute = attributes.getNamedItem("class");
        try {
            String formerOperatorKey = getParameterValue(doc, operatorName, CompositeDummyOperator.PARAMETER_REPLACES);
            if (formerOperatorKey != null) {
                classAttribute.setNodeValue(formerOperatorKey);
                removeParameter(doc, operatorName, CompositeDummyOperator.PARAMETER_REPLACES);
            }
            // now we search for necessary parameters
            {
                LinkedHashMap<String, String> parameterMap = getParameterListValue(doc, operatorName, CompositeDummyOperator.PARAMETER_PARAMETERS);
                for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
                    Element parameter = document.createElement("parameter");
                    parameter.setAttribute("key", entry.getKey());
                    parameter.setAttribute("value", entry.getValue());
                    operatorNode.appendChild(parameter);
                }
                removeParameterList(doc, operatorName, CompositeDummyOperator.PARAMETER_PARAMETERS);
            }

            // now we search for necessary replaces in ports
            {
                LinkedHashMap<String, String> inputPortMap = getParameterListValue(doc, operatorName, CompositeDummyOperator.PARAMETER_INPUT_PORT_REPLACEMENTS);
                NodeList inputPortList = (NodeList) xpath.compile("//connect[@to_op='" + operatorName + "']").evaluate(doc, XPathConstants.NODESET);
                for (int inIndex = 0; inIndex < inputPortList.getLength(); inIndex++) {
                    Node connectionNode = inputPortList.item(inIndex);
                    NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                    Node toPortNode = connectionAttributes.getNamedItem("to_port");
                    toPortNode.setNodeValue(inputPortMap.get(toPortNode.getNodeValue()));
                }
                removeParameterList(doc, operatorName, CompositeDummyOperator.PARAMETER_INPUT_PORT_REPLACEMENTS);
            }
            // output ports
            {
                LinkedHashMap<String, String> outputPortMap = getParameterListValue(doc, operatorName,
                        CompositeDummyOperator.PARAMETER_OUTPUT_PORT_REPLACEMENTS);
                NodeList outputPortList = (NodeList) xpath.compile("//connect[@from_op='" + operatorName + "']").evaluate(doc, XPathConstants.NODESET);
                for (int inIndex = 0; inIndex < outputPortList.getLength(); inIndex++) {
                    Node connectionNode = outputPortList.item(inIndex);
                    NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                    Node fromPortNode = connectionAttributes.getNamedItem("from_port");
                    fromPortNode.setNodeValue(outputPortMap.get(fromPortNode.getNodeValue()));
                }
                removeParameterList(doc, operatorName, CompositeDummyOperator.PARAMETER_OUTPUT_PORT_REPLACEMENTS);
            }

            // now we search for necessary lists, which are encoded as a list of enumerations with
            // parameter values, which hold both key and value
            {
                LinkedList<Pair<String, String>> listMap = getParameterListPairValues(doc, operatorName, CompositeDummyOperator.PARAMETER_LISTS);
                List<String> uniqueLists = listMap.stream().map(Pair::getFirst).distinct().toList();

                for (String uniqueListKey : uniqueLists) {
                    Element list = document.createElement("list");
                    list.setAttribute("key", uniqueListKey);
                    for (Pair<String, String> pair : listMap) {
                        if (Objects.equals(uniqueListKey, pair.getFirst())) {
                            int indexSeparationCharacter = pair.getSecond().indexOf(":");
                            String value = pair.getSecond().substring(indexSeparationCharacter + 1);
                            String key = pair.getSecond().substring(0, indexSeparationCharacter);
                            OperatorReplacementTransformer.createParameter(doc, list, key, value);
                        }
                    }
                    operatorNode.appendChild(list);
                }
                removeParameterList(doc, operatorName, CompositeDummyOperator.PARAMETER_LISTS);
            }

            // now we search for inner pipelines and if present we redo the respective ports
            {
                NodeList pipelineNodes = (NodeList) xpath.compile("pipeline").evaluate(operatorNode, XPathConstants.NODESET);
                if (pipelineNodes.getLength() != 0) {
                    LinkedList<Pair<String, String>> inputPortReplacements = getParameterListPairValues(doc, operatorName,
                            CompositeDummyOperator.PARAMETER_INNER_PIPELINE_INPUT_PORTS);
                    LinkedList<Pair<String, String>> outputPortReplacements = getParameterListPairValues(doc, operatorName,
                            CompositeDummyOperator.PARAMETER_INNER_PIPELINE_OUTPUT_PORTS);

                    // search in pipeline nodes for connections and replace them
                    for (int i = 0; i < pipelineNodes.getLength(); i++) {
                        Node pipelineNode = pipelineNodes.item(i);

                        int finalI = i;
                        List<Pair<String, String>> pipelineOutputPortReplacements = outputPortReplacements.stream()
                                .filter(pair -> pair.getFirst().equals(String.valueOf(finalI)))
                                .map(pair -> {
                                    int indexSeparationCharacter = pair.getSecond().indexOf(".");
                                    String value = pair.getSecond().substring(indexSeparationCharacter + 1);
                                    String key = pair.getSecond().substring(0, indexSeparationCharacter);
                                    return new Pair<>(key, value);
                                })
                                .toList();
                        NodeList outputPortList = (NodeList) xpath.compile("connect[not(@to_op)]").evaluate(pipelineNode, XPathConstants.NODESET);
                        for (int inIndex = 0; inIndex < outputPortList.getLength(); inIndex++) {
                            Node connectionNode = outputPortList.item(inIndex);
                            NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                            Node fromPortNode = connectionAttributes.getNamedItem("to_port");
                            String originalFromPortName = fromPortNode.getNodeValue();
                            for (Pair<String, String> pair : pipelineOutputPortReplacements) {
                                if (Objects.equals(originalFromPortName, pair.getFirst())) {
                                    fromPortNode.setNodeValue(pair.getSecond());
                                    break;
                                }
                            }
                        }

                        List<Pair<String, String>> pipelineInputPortReplacements = inputPortReplacements.stream()
                                .filter(pair -> pair.getFirst().equals(String.valueOf(finalI)))
                                .map(pair -> {
                                    int indexSeparationCharacter = pair.getSecond().indexOf(".");
                                    String value = pair.getSecond().substring(indexSeparationCharacter + 1);
                                    String key = pair.getSecond().substring(0, indexSeparationCharacter);
                                    return new Pair<>(key, value);
                                })
                                .toList();
                        NodeList inputPortList = (NodeList) xpath.compile("connect[not(@from_op)]").evaluate(pipelineNode, XPathConstants.NODESET);
                        for (int inIndex = 0; inIndex < inputPortList.getLength(); inIndex++) {
                            Node connectionNode = inputPortList.item(inIndex);
                            NamedNodeMap connectionAttributes = connectionNode.getAttributes();
                            Node fromPortNode = connectionAttributes.getNamedItem("from_port");
                            String originalFromPortName = fromPortNode.getNodeValue();
                            for (Pair<String, String> pair : pipelineInputPortReplacements) {
                                if (Objects.equals(originalFromPortName, pair.getFirst())) {
                                    fromPortNode.setNodeValue(pair.getSecond());
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.DEBUG, e.getMessage());
        }
    }

    private LinkedList<Pair<String, String>> getParameterListPairValues(Document doc, String operatorName, String parameterKey)
            throws XPathExpressionException {
        NodeList parameterNodes = (NodeList) xpath.compile("//operator[@name='" + operatorName + "']/list[@key='" + parameterKey + "']/parameter")
                .evaluate(doc, XPathConstants.NODESET);
        LinkedList<Pair<String, String>> listValues = new LinkedList<>();
        for (int j = 0; j < parameterNodes.getLength(); j++) {
            Node parameterNode = parameterNodes.item(j);
            listValues.add(new Pair<>(parameterNode.getAttributes().getNamedItem("key").getNodeValue(),
                    parameterNode.getAttributes().getNamedItem("value").getNodeValue()));
        }
        return listValues;
    }

    private LinkedHashMap<String, String> getParameterListValue(Document doc, String operatorName, String parameterKey) throws XPathExpressionException {
        NodeList parameterElements = (NodeList) xpath.compile("//operator[@name='" + operatorName + "']/list[@key='" + parameterKey + "']/parameter")
                .evaluate(doc, XPathConstants.NODESET);
        LinkedHashMap<String, String> listValues = new LinkedHashMap<>();
        for (int i = 0; i < parameterElements.getLength(); i++) {
            NamedNodeMap listEntryAttributes = parameterElements.item(i).getAttributes();
            listValues.put(listEntryAttributes.getNamedItem("key").getNodeValue(), listEntryAttributes.getNamedItem("value").getNodeValue());
        }
        return listValues;
    }

    private void removeParameterList(Document doc, String operatorName, String parameterKey) throws XPathExpressionException {
        NodeList parameterElements = (NodeList) xpath.compile("//operator[@name='" + operatorName + "']/list[@key='" + parameterKey + "']")
                .evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < parameterElements.getLength(); i++)
            parameterElements.item(i).getParentNode().removeChild(parameterElements.item(i));
    }

    private void removeParameter(Document doc, String operatorName, String parameterKey) throws XPathExpressionException {
        NodeList parameterElements = (NodeList) xpath.compile("//operator[@name='" + operatorName + "']/parameter[@key='" + parameterKey + "']")
                .evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < parameterElements.getLength(); i++)
            parameterElements.item(i).getParentNode().removeChild(parameterElements.item(i));
    }

    private String getParameterValue(Document doc, String operatorName, String parameterKey) throws XPathExpressionException {
        NodeList parameterElements = (NodeList) xpath.compile("//operator[@name='" + operatorName + "']/parameter[@key='" + parameterKey + "']")
                .evaluate(doc, XPathConstants.NODESET);
        if (parameterElements.getLength() == 0)
            return null;
        else
            return parameterElements.item(0).getAttributes().getNamedItem("value").getNodeValue();
    }
}
