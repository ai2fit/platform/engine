package com.owc.singularity.engine.pipeline;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.io.FileObject;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.WebserviceRootOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.tools.RandomGenerator;

public class WebservicePipeline extends AbstractPipeline {


    public WebservicePipeline() {
        super(new WebserviceRootOperator());
    }

    public WebservicePipeline(AbstractRootOperator rootOperator) {
        super(rootOperator);
        if (!(rootOperator instanceof WebserviceRootOperator))
            throw new IllegalArgumentException("Only webservice root operator permitted in webservice pipelines.");
    }

    /**
     * Clone constructor
     * 
     * @param webserviceProcess
     */
    private WebservicePipeline(WebservicePipeline webserviceProcess) {
        super(webserviceProcess);
    }

    @Override
    public WebservicePipeline clone() {
        return new WebservicePipeline(this);
    }


    @Override
    public List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyValues) throws OperatorException {
        WebservicePipelineExecutionContext myContext = (WebservicePipelineExecutionContext) context;
        fireEventPipelineStarting();
        try {
            setState(PipelineState.RUNNING);
            getLogger().debug("Initialising pipeline setup.");

            RandomGenerator.init(this);

            clearDataTables();
            clearVariables();
            if (getExecutionMode() != ExecutionMode.ONLY_DIRTY) {
                getRootOperator().clear(Port.CLEAR_DATA);
            }

            getLogger().debug("Pipeline initialised.");

            // add process start variable value here already to have it available for root
            // parameters
            // can be overwritten if it is passed to the run() method via the variable map
            getVariableHandler().addVariable(VariableHandler.PIPELINE_START, VariableHandler.DATE_FORMAT.get().format(new Date(System.currentTimeMillis())));

            // apply variables
            for (Map.Entry<String, String> entry : myContext.variableValues.entrySet()) {
                getVariableHandler().addVariable(entry.getKey(), entry.getValue());
            }

            rootOperator.pipelineStarts();
            List<IOObject> results = rootOperator.execute(context, false);
            if (results.size() != 1) {
                throw new UserError(rootOperator, "webservice.expects_single_file_object_result");
            }
            if (!(results.get(0) instanceof FileObject))
                throw new UserError(rootOperator, "webservice.expects_single_file_object_result");
            return results;
        } catch (OperatorException e) {
            finishProcess(e);
            throw e;
        }
    }

    @Override
    public PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) {
        return null;
    }

    @Override
    public List<ParameterType> getExecutionContextParameterTypes() {
        return super.getExecutionContextParameterTypes();
    }

    public static class WebservicePipelineExecutionContext extends PipelineExecutionContext {

        public List<FileObject> bodyFileObjects = Collections.emptyList();
        public Map<String, String> variableValues = Collections.emptyMap();

    }

}
