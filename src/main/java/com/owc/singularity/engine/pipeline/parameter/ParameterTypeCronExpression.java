/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.CronExpressionCellEditor;


/**
 * @author Nils Woehler
 * 
 */
@ParameterTypeAnnotation(editor = CronExpressionCellEditor.class)
public class ParameterTypeCronExpression extends ParameterTypeString {

    private static final long serialVersionUID = 1L;


    public ParameterTypeCronExpression(String key, String description, boolean optional) {
        super(key, description, optional);
    }

    public ParameterTypeCronExpression(String key, String description) {
        super(key, description, true);
    }

    public ParameterTypeCronExpression(String key, String description, String defaultValue) {
        super(key, description, defaultValue);
    }

}
