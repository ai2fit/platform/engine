package com.owc.singularity.engine.pipeline.execution;

import com.owc.singularity.engine.pipeline.Operation;
import com.owc.singularity.engine.pipeline.OperationAddress;
import com.owc.singularity.engine.pipeline.OperationComposition;

public record OperationExecutionAddress(OperationCompositionExecutionAddress compositionExecutionAddress, Operation operation) {

    public OperationComposition parentComposition() {
        if (compositionExecutionAddress.parentCompositions().length > 0)
            return compositionExecutionAddress.parentCompositions()[compositionExecutionAddress.parentCompositions().length - 1];
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(compositionExecutionAddress.toString());
        builder.append(operation.getName());
        return builder.toString();
    }

    public OperationAddress toOperationAddress() {
        return new OperationAddress(compositionExecutionAddress.toOperationCompositionAddress(), operation);
    }
}
