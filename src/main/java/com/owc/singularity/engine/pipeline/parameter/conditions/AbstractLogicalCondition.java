/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter.conditions;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.w3c.dom.Element;

import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.tools.XMLException;


/**
 * Super class for all logical {@link ParameterCondition} like {@link AndParameterCondition} and
 * {@link OrParameterCondition}.
 *
 * @author Nils Woehler
 *
 */
public abstract class AbstractLogicalCondition extends ParameterCondition {

    private static final String ELEMENT_CONDITION = "Condition";
    private static final String ATTRIBUTE_CONDITION_CLASS = "condition-class";

    private ParameterCondition[] conditions;

    public AbstractLogicalCondition(Element element) throws XMLException {
        super(element);
        // get all condition xml-elements
        Element conditionsElement = XMLTools.getChildElement(element, getXMLTag(), true);
        Collection<Element> conditionElements = XMLTools.getChildElements(conditionsElement, ELEMENT_CONDITION);
        conditions = new ParameterCondition[conditionElements.size()];

        // iterate over condition xml-elements
        int idx = 0;
        for (Element conditionElement : conditionElements) {
            // try to construct a condition object
            String className = conditionElement.getAttribute(ATTRIBUTE_CONDITION_CLASS);
            Class<?> conditionClass;
            try {
                conditionClass = Class.forName(className);
                Constructor<?> constructor = conditionClass.getConstructor(Element.class);
                conditions[idx] = (ParameterCondition) constructor.newInstance(conditionElement);
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                throw new XMLException("Illegal value for attribute " + ATTRIBUTE_CONDITION_CLASS, e);
            }
            ++idx;
        }
    }

    public AbstractLogicalCondition(ParameterHandler parameterHandler, boolean becomeMandatory, ParameterCondition... conditions) {
        super(parameterHandler, becomeMandatory);
        this.conditions = conditions;
    }

    public AbstractLogicalCondition(ParameterHandler parameterHandler, String conditionParameter, boolean becomeMandatory, ParameterCondition... conditions) {
        super(parameterHandler, conditionParameter, becomeMandatory);
        this.conditions = conditions;
    }

    @Override
    public void getDefinitionAsXML(Element element) {
        Element conditionsElement = XMLTools.addTag(element, getXMLTag());
        for (ParameterCondition parameterCondition : conditions) {
            Element conditionElement = XMLTools.addTag(conditionsElement, ELEMENT_CONDITION);
            parameterCondition.getDefinitionAsXML(conditionElement);
            conditionElement.setAttribute(ATTRIBUTE_CONDITION_CLASS, parameterCondition.getClass().getName());
        }
    }

    /**
     * @return the conditions for this logical parameter condition
     */
    protected ParameterCondition[] getConditions() {
        return conditions;
    }

    /**
     * @return the XML tag for this condition
     */
    abstract String getXMLTag();

}
