package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.Map;
import java.util.Objects;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * An implementation of the {@link ProcessReplacementTransformer} that allows to transform the
 * subprocess of a given operator. This is useful to migrate or remap inner sources and sinks.
 *
 * @author Hatem Hamad
 */
public abstract class SubprocessReplacementTransformer extends ProcessReplacementTransformer {

    private final XPathExpression processExpression;
    private final XPathExpression innerSourcesConnectionsExpr;
    private final XPathExpression innerSinksConnectionsExpr;
    private final String operatorKey;
    private final Integer subprocessIndex;
    private final Map<String, String> innerSourcesReplacementMap;
    private final Map<String, String> innerSinksReplacementMap;

    /**
     * Creates a {@link SubprocessReplacementTransformer} that will transform one specific
     * subprocess of a given operator
     * 
     * @param operatorKey
     *            the operator key
     * @param subprocessIndex
     *            the index in which the subprocess shall be changed
     * @param innerSourcesReplacementMap
     *            the mapping to use for inner sources, maps between the old port names and the new
     *            ones
     * @param innerSinksReplacementMap
     *            the mapping to use for inner sinks, maps between the old port names and the new
     *            ones
     * @throws Exception
     *             if creating the transformer causes an error
     */
    public SubprocessReplacementTransformer(String operatorKey, int subprocessIndex, Map<String, String> innerSourcesReplacementMap,
            Map<String, String> innerSinksReplacementMap) throws Exception {
        this.operatorKey = operatorKey;
        this.subprocessIndex = subprocessIndex;
        this.innerSourcesReplacementMap = innerSourcesReplacementMap;
        this.innerSinksReplacementMap = innerSinksReplacementMap;
        this.processExpression = getXpath().compile("//operator[@class='" + operatorKey + "']/pipeline[" + (subprocessIndex + 1) + "]");
        this.innerSourcesConnectionsExpr = getXpath().compile("connect[not(@from_op)]");
        this.innerSinksConnectionsExpr = getXpath().compile("connect[not(@to_op)]");
    }

    /**
     * Creates a {@link SubprocessReplacementTransformer} that will transform all subprocesses of a
     * given operator
     * 
     * @param operatorKey
     *            the operator key
     * @param innerSourcesReplacementMap
     *            the mapping to use for inner sources, maps between the old port names and the new
     *            ones
     * @param innerSinksReplacementMap
     *            the mapping to use for inner sinks, maps between the old port names and the new
     *            ones
     * @throws Exception
     *             if creating the transformer causes error
     */
    public SubprocessReplacementTransformer(String operatorKey, Map<String, String> innerSourcesReplacementMap, Map<String, String> innerSinksReplacementMap)
            throws Exception {
        this.operatorKey = operatorKey;
        this.subprocessIndex = null;
        this.innerSourcesReplacementMap = innerSourcesReplacementMap;
        this.innerSinksReplacementMap = innerSinksReplacementMap;
        processExpression = getXpath().compile("//operator[@class='" + operatorKey + "']/pipeline");
        innerSourcesConnectionsExpr = getXpath().compile("connect[not(@from_op)]");
        innerSinksConnectionsExpr = getXpath().compile("connect[not(@to_op)]");
    }

    @Override
    protected XPathExpression getProcessExpression() {
        return processExpression;
    }

    @Override
    protected void applyOnProcess(Node processNode, Document document, CompatibilityTransformationLog log) throws Exception {
        // change inner sources
        if (innerSourcesReplacementMap != null && !innerSourcesReplacementMap.isEmpty()) {
            try {
                NodeList innerSourcesNodes = (NodeList) innerSourcesConnectionsExpr.evaluate(processNode, XPathConstants.NODESET);
                for (int j = 0; j < innerSourcesNodes.getLength(); j++) {
                    Node connectionNode = innerSourcesNodes.item(j);
                    Node fromPortAttribute = connectionNode.getAttributes().getNamedItem("from_port");
                    String currentPort = fromPortAttribute.getNodeValue();
                    fromPortAttribute.setNodeValue(innerSourcesReplacementMap.getOrDefault(currentPort, currentPort));
                }
            } catch (Exception e) {
                log.log(Level.ERROR, "Sub-Pipeline transformation failed: " + e.getMessage());
            }
        }
        // change inner sinks
        if (innerSinksReplacementMap != null && !innerSinksReplacementMap.isEmpty()) {
            try {
                NodeList innerSinksNodes = (NodeList) innerSinksConnectionsExpr.evaluate(processNode, XPathConstants.NODESET);
                for (int j = 0; j < innerSinksNodes.getLength(); j++) {
                    Node connectionNode = innerSinksNodes.item(j);
                    Node toPortAttribute = connectionNode.getAttributes().getNamedItem("to_port");
                    String currentPort = toPortAttribute.getNodeValue();
                    toPortAttribute.setNodeValue(innerSinksReplacementMap.getOrDefault(currentPort, currentPort));
                }
            } catch (Exception e) {
                log.log(Level.ERROR, "Sub-Pipeline transformation failed: " + e.getMessage());
            }
        }
        String operatorName = Objects.requireNonNullElse(getOperatorName(processNode), operatorKey);
        log.log(Level.INFO, "'" + operatorName + "' Sub-Pipeline" + (subprocessIndex == null ? "es" : "[" + subprocessIndex + "]")
                + " sources and sinks are transformed successfully.");
    }

    private static String getOperatorName(Node processNode) {
        if (processNode == null || processNode.getParentNode() == null)
            return null;
        Node operatorNode = processNode.getParentNode();
        NamedNodeMap attributes = operatorNode.getAttributes();
        if (attributes == null)
            return null;
        Node nameNode = attributes.getNamedItem("name");
        if (nameNode == null)
            return null;
        return nameNode.getNodeValue();
    }
}
