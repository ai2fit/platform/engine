package com.owc.singularity.engine.pipeline.io.transformer;

import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * A {@link ProcessReplacementTransformer} to replace all mentions of the deprecated "process" tag
 * with its updated "pipeline" tag.
 */
@CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_INSERTION)
public class ProcessToPipelineTransformer extends ProcessReplacementTransformer {

    private final XPathExpression processExpression;

    public ProcessToPipelineTransformer() throws Exception {
        processExpression = getXpath().compile("//process");
    }

    @Override
    protected XPathExpression getProcessExpression() {
        return processExpression;
    }

    @Override
    protected void applyOnProcess(Node processNode, Document document, CompatibilityTransformationLog log) throws Exception {
        document.renameNode(processNode, null, "pipeline");
    }
}
