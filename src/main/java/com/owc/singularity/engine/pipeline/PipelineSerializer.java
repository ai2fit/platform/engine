package com.owc.singularity.engine.pipeline;

import java.io.*;
import java.nio.charset.StandardCharsets;

import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.repository.entry.EntryTypeAlias;
import com.owc.singularity.repository.serialization.DefaultSerializer;
import com.owc.singularity.repository.serialization.DefinesSerializer;
import com.owc.singularity.repository.serialization.Serializer;

@DefinesSerializer(supportedClass = AbstractPipeline.class)
public class PipelineSerializer implements Serializer {

    @Override
    public void serialize(Object object, OutputStream out) throws IOException {
        AbstractPipeline process = (AbstractPipeline) object;
        out.write(process.getRootOperator().getXML(false).getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public Object deserialize(InputStream in, Class<?> targetClass, ClassLoader classLoader, byte version) throws IOException {
        try {
            switch (version) {
                case 1:
                    IntermediateCompatibilityPipeline intermediate = (IntermediateCompatibilityPipeline) new DefaultSerializer().deserialize(in,
                            IntermediateCompatibilityPipeline.class, classLoader, version);
                    return intermediate.pipeline;
                case 2:
                case 3:
                    // here we will have to do some more logic as internal transformer might have
                    // caused non fitting root operator to pipeline type
                    String pipelineString = new String(in.readAllBytes(), StandardCharsets.UTF_8);
                    return XMLImporter.parse(pipelineString);
                default:
                    throw new IllegalArgumentException("Unexpected value: " + version);

            }
        } catch (XMLException | OperatorCreationException e) {
            throw new RuntimeException("Error while reading pipeline definition", e);
        }
    }


    @Override
    public byte getCurrentVersion() {
        return 3;
    }

    @EntryTypeAlias(alias = "com.owc.singularity.engine.operator.process.DebugPipeline")
    @EntryTypeAlias(alias = "com.owc.singularity.engine.process.Process")
    public static class IntermediateCompatibilityPipeline extends AnalysisPipeline implements Externalizable {

        protected AbstractPipeline pipeline;

        @Override
        public void writeExternal(ObjectOutput out) throws IOException {}

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            try {
                byte[] processData = new byte[in.readInt()];
                in.readFully(processData);
                pipeline = XMLImporter.parse(new String(processData, StandardCharsets.UTF_8));
            } catch (XMLException | OperatorCreationException e) {
                throw new IOException("Cannot read XML.", e);
            }

        }
    }
}
