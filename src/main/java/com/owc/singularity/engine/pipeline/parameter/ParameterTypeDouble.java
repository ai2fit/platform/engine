/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.DefaultPropertyValueCellEditor;


/**
 * A parameter type for double values. Operators ask for the double value with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterAsDouble(String)}. For infinite
 * ranges Double.POSITIVE_INFINITY and Double.NEGATIVE_INFINITY should be used.
 * 
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = DefaultPropertyValueCellEditor.class)
public class ParameterTypeDouble extends ParameterTypeNumber {

    private static final long serialVersionUID = 2455026868706964187L;

    private double defaultValue = Double.NaN;

    private double min = Double.NEGATIVE_INFINITY;

    private double max = Double.POSITIVE_INFINITY;

    private boolean noDefault = true;


    public ParameterTypeDouble(String key, String description, double min, double max) {
        this(key, description, min, max, Double.NaN);
        this.noDefault = true;
    }

    public ParameterTypeDouble(String key, String description, double min, double max, boolean optional) {
        this(key, description, min, max, Double.NaN);
        this.noDefault = true;
        setOptional(optional);
    }

    public ParameterTypeDouble(String key, String description, double min, double max, double defaultValue) {
        super(key, description);
        this.defaultValue = defaultValue;
        this.min = min;
        this.max = max;
    }

    @Override
    public double getMinValue() {
        return min;
    }

    @Override
    public double getMaxValue() {
        return max;
    }

    @Override
    public Object getDefaultValue() {
        if (Double.isNaN(defaultValue)) {
            return null;
        } else {
            return defaultValue;
        }
    }

    @Override
    public void setDefaultValue(Object object) {
        this.defaultValue = (Double) object;
    }

    @Override
    public void setDefaultValueAsRawString(String value) {
        try {
            this.defaultValue = Double.parseDouble(value);
        } catch (NumberFormatException e) {

        }
    }

    /** Returns true. */
    @Override
    public boolean isNumerical() {
        return true;
    }

    @Override
    public String getRange() {
        String range = "real; ";
        if (min == Double.NEGATIVE_INFINITY) {
            range += "-\u221E";
        } else {
            range += min;
        }
        range += " - ";
        if (max == Double.POSITIVE_INFINITY) {
            range += "+\u221E";
        } else {
            range += max;
        }
        if (!noDefault) {
            range += "; default: " + defaultValue;
        }
        return range;
    }

}
