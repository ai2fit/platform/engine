package com.owc.singularity.engine.pipeline;

import java.io.Serializable;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;


public class PipelineMetaData implements Serializable {

    private static final long serialVersionUID = 1253611641125945566L;

    private String pipelineTypeName;
    private boolean isAbstract;
    private boolean isImplementing;
    private String tags = null;
    private String documentation = null;

    public PipelineMetaData(AbstractPipeline pipeline) {
        super();
        this.pipelineTypeName = pipeline.getEffectiveType().getName();
        this.isAbstract = pipeline.isAbstract();
        this.isImplementing = pipeline instanceof ImplementingPipeline;
        try {
            tags = pipeline.getRootOperator().getParameterAsString(AbstractRootOperator.PARAMETER_TAGS);
        } catch (UndefinedParameterError e) {
        }
        try {
            documentation = pipeline.getRootOperator().getParameterAsString(AbstractRootOperator.PARAMETER_DESCRIPTION);
        } catch (UndefinedParameterError e) {
        }
    }

    public String getPipelineTypeName() {
        return pipelineTypeName;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public boolean isImplementing() {
        return isImplementing;
    }

    public String getDocumentation() {
        return documentation;
    }

    public String getTags() {
        return tags;
    }
}
