package com.owc.singularity.engine.pipeline.parameter;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.owc.singularity.studio.gui.parameters.celleditors.value.PropertyValueCellEditor;

/**
 * This is the annotation for all parameter types. A parameter Type must be a subclass of
 * {@link ParameterType} and define a value cell editor.
 * 
 * @author Sebastian Land
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface ParameterTypeAnnotation {

    Class<? extends PropertyValueCellEditor> editor();
}
