/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.engine.tools.XMLException;


/**
 * Rule to set parameters.
 * 
 * @author Simon Fischer
 * 
 */
@OperatorParseRuleAnnotation(xmlTagName = "setParameter")
public class SetParameterRule extends AbstractConditionedParseRule {

    private String attributeName;
    private String value;

    public SetParameterRule(String operatorTypeName, Element element) throws XMLException {
        super(operatorTypeName, element);
        assert (element.getTagName().equals("setParameter"));
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element childElem) {
                if (childElem.getTagName().equals("attribute")) {
                    attributeName = childElem.getTextContent();
                } else if (childElem.getTagName().equals("value")) {
                    value = childElem.getTextContent();
                }
            }
        }
    }

    @Override
    protected String conditionedApply(Operator operator, String operatorTypeName, XMLPipelineParser parser) {
        operator.getParameters().setRawParameter(attributeName, value);
        return "Parameter <code>" + attributeName + "</code> in <var>" + operator.getName() + "</var> (<code>" + operatorTypeName + "</code>) was set to <code>"
                + value + "</code>.";
    }

    @Override
    public String toString() {
        return "Set " + operatorTypeName + "." + attributeName + " to " + value;
    }
}
