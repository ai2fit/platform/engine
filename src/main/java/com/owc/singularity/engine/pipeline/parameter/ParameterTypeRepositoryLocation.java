/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;

import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.operator.AbstractOperator.OperationExecutionContext;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.parameters.celleditors.value.RepositoryLocationValueCellEditor;

/**
 * A parameter type for specifying a repository location.
 * 
 * @author Simon Fischer, Sebastian Land
 */
@ParameterTypeAnnotation(editor = RepositoryLocationValueCellEditor.class)
public class ParameterTypeRepositoryLocation extends ParameterTypeString {

    private static final long serialVersionUID = 1L;

    private boolean allowFolders, allowEntries, allowAbsoluteEntries, onlyExistingEntries, onlyWriteableLocations, allowVersioning;

    /**
     * Creates a new parameter type for files with the given extension. If the extension is null no
     * file filters will be used.
     */
    public ParameterTypeRepositoryLocation(String key, String description, boolean optional) {
        this(key, description, true, false, optional);
    }

    public ParameterTypeRepositoryLocation(String key, String description, boolean optional, boolean allowVersioning) {
        this(key, description, true, false, false, optional, false, false, allowVersioning);
    }

    /**
     * Creates a new parameter type for files with the given extension. If the extension is null no
     * file filters will be used.
     */
    public ParameterTypeRepositoryLocation(String key, String description, boolean allowEntries, boolean allowDirectories, boolean optional) {
        this(key, description, allowEntries, allowDirectories, false, optional, false, false, false);
    }

    public ParameterTypeRepositoryLocation(String key, String description, boolean allowEntries, boolean allowDirectories, boolean allowAbsoluteEntries,
            boolean optional, boolean onlyExistingEntries) {
        this(key, description, allowEntries, allowDirectories, allowAbsoluteEntries, optional, onlyExistingEntries, false, false);
    }

    /**
     * Creates a new parameter type for files with the given extension. If the extension is null no
     * file filters will be used.
     **/
    public ParameterTypeRepositoryLocation(String key, String description, boolean allowEntries, boolean allowDirectories, boolean allowAbsoluteEntries,
            boolean optional, boolean onlyExistingEntries, boolean onlyWriteableLocations, boolean allowVersioning) {
        super(key, description, null);

        setOptional(optional);
        setAllowEntries(allowEntries);
        setAllowFolders(allowDirectories);
        setAllowAbsoluteEntries(allowAbsoluteEntries);
        setOnlyExistingEntries(onlyExistingEntries);
        setOnlyWriteableLocations(onlyWriteableLocations);
        setAllowVersioning(allowVersioning);
    }

    /**
     * Creates a new parameter type for files with the given extension. If the extension is null no
     * file filters will be used.
     */
    public ParameterTypeRepositoryLocation(String key, String description, boolean allowEntries, boolean allowDirectories, boolean allowAbsoluteEntries,
            boolean optional) {
        this(key, description, allowEntries, allowDirectories, allowAbsoluteEntries, optional, false, false, true);
    }

    public boolean isOnlyWriteableLocations() {
        return onlyWriteableLocations;
    }

    public void setOnlyWriteableLocations(boolean onlyWriteableLocations) {
        this.onlyWriteableLocations = onlyWriteableLocations;
    }

    public boolean isAllowFolders() {
        return allowFolders;
    }

    public void setAllowFolders(boolean allowFolders) {
        this.allowFolders = allowFolders;
    }

    public boolean isAllowEntries() {
        return allowEntries;
    }

    public void setAllowEntries(boolean allowEntries) {
        this.allowEntries = allowEntries;
    }

    public void setAllowAbsoluteEntries(boolean allowAbsoluteEntries) {
        this.allowAbsoluteEntries = allowAbsoluteEntries;
    }

    public boolean isAllowAbsoluteEntries() {
        return this.allowAbsoluteEntries;
    }

    public boolean isOnlyExistingEntries() {
        return onlyExistingEntries;
    }

    public void setOnlyExistingEntries(boolean onlyExistingEntries) {
        this.onlyExistingEntries = onlyExistingEntries;
    }

    public void setAllowVersioning(boolean allowVersioning) {
        this.allowVersioning = allowVersioning;
        ;
    }

    public boolean isAllowVersioning() {
        return allowVersioning;
    }


    public static RepositoryPath getPath(String parameterAsString, Operator operator) {
        if (parameterAsString == null || operator == null || operator.getPipeline() == null)
            return null;
        RepositoryPath processLocation = operator.getPipeline().getPath();
        if (processLocation != null)
            return processLocation.getParent().resolve(parameterAsString);
        return RepositoryPath.of(parameterAsString);
    }

    public static RepositoryPath getPath(String parameterAsString, OperationExecutionContext context) {
        if (context.workingPath != null)
            return context.workingPath.getParent().resolve(parameterAsString);
        return RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(parameterAsString);
    }

    public static RepositoryPath getPath(String parameterAsString, OperationDescriptionContext context) {
        RepositoryPath path = context.getPath();
        if (path != null)
            return path.getParent().resolve(parameterAsString);
        return RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(parameterAsString);
    }
}
