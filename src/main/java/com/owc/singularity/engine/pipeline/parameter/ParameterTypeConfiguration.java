/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Objects;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.parameters.celleditors.value.ConfigurationWizardValueCellEditor;
import com.owc.singularity.studio.gui.wizards.ConfigurationListener;
import com.owc.singularity.studio.gui.wizards.ConfigurationWizardCreator;


/**
 * This parameter type will lead to a GUI element which can be used as initialization for a sort of
 * operator configuration wizard.
 *
 * @author Ingo Mierswa
 */
@ParameterTypeAnnotation(editor = ConfigurationWizardValueCellEditor.class)
public class ParameterTypeConfiguration extends ParameterType {

    private static final long serialVersionUID = -3512071671355815277L;

    public static final String PARAMETER_DEFAULT_CONFIGURATION_NAME = "configure_operator";

    private Class<? extends ConfigurationWizardCreator> wizardCreatorClass;

    private transient ConfigurationListener wizardListener;

    private Map<String, String> parameters = null;

    public Object[] wizardConstructionArguments;

    public ParameterTypeConfiguration(Class<? extends ConfigurationWizardCreator> wizardCreatorClass, ConfigurationListener wizardListener) {
        this(wizardCreatorClass, null, wizardListener);
    }

    public ParameterTypeConfiguration(Class<? extends ConfigurationWizardCreator> wizardCreatorClass, Map<String, String> parameters,
            ConfigurationListener wizardListener) {
        this(wizardCreatorClass, parameters, wizardListener, null);

    }

    public ParameterTypeConfiguration(Class<? extends ConfigurationWizardCreator> wizardCreatorClass, Map<String, String> parameters,
            ConfigurationListener wizardListener, Object[] constructorArguments) {
        super(PARAMETER_DEFAULT_CONFIGURATION_NAME, "Configure this operator by means of a Wizard.");
        this.wizardCreatorClass = wizardCreatorClass;
        this.parameters = parameters;
        this.wizardListener = wizardListener;
        this.wizardConstructionArguments = constructorArguments;

        setPrimary(true);
    }

    /**
     * Returns a new instance of the wizard creator. If anything does not work this method will
     * return null.
     */
    public ConfigurationWizardCreator getWizardCreator() {
        ConfigurationWizardCreator creator = null;
        try {
            if (wizardConstructionArguments == null) {
                creator = wizardCreatorClass.newInstance();
            } else {
                // if arguments were given: Use appropriate constructor
                Class<?>[] classes = new Class[wizardConstructionArguments.length];
                for (int i = 0; i < classes.length; i++) {
                    classes[i] = wizardConstructionArguments[i].getClass();
                }
                try {
                    for (Constructor<?> constructor : wizardCreatorClass.getConstructors()) {
                        boolean fits = true;
                        for (int i = 0; i < classes.length; i++) {
                            Class<?>[] constructorParameter = constructor.getParameterTypes();
                            if (i >= constructorParameter.length || !constructorParameter[i].isAssignableFrom(classes[i])) {
                                fits = false;
                                break;
                            }
                        }
                        if (fits) {
                            creator = (ConfigurationWizardCreator) constructor.newInstance(wizardConstructionArguments);
                            break;
                        }
                    }
                } catch (SecurityException | IllegalArgumentException | InvocationTargetException e) {
                    creator = wizardCreatorClass.newInstance();
                }
            }
            // this is ensured to be non-null
            Objects.requireNonNull(creator);
            creator.setParameters(parameters);
        } catch (InstantiationException | IllegalAccessException e) {
            LogService.getRoot()
                    .warn("com.owc.singularity.engine.pipeline.parameter.ParameterTypeConfiguration.problem_during_creation_of_wizard", e.getMessage());
        }
        return creator;
    }

    public ConfigurationListener getWizardListener() {
        return wizardListener;
    }

    /** Returns null. */
    @Override
    public Object getDefaultValue() {
        return null;
    }

    /** Does nothing. */
    @Override
    public void setDefaultValue(Object value) {}

    @Override
    public String getRange() {
        return null;
    }

    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public Element getXML(String key, String value, boolean hideDefault, Document doc) {
        return null;
    }

    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) {
        return parameterValue;
    }

}
