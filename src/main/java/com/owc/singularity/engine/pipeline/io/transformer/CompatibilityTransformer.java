package com.owc.singularity.engine.pipeline.io.transformer;

import org.w3c.dom.Document;

/**
 * This is an interface for all transformer of an imported process xml dom. Each transformer marked
 * by {@link CompatibilityTransformerAnnotation} will be applied before the actual reading takes
 * place.
 * 
 * @author Sebastian Land
 */
public interface CompatibilityTransformer {

    /**
     * This transformes the given document
     * 
     * @param document
     */
    public void apply(Document document, CompatibilityTransformationLog log);
}
