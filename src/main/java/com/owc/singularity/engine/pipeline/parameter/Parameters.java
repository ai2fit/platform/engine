/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.BiFunction;

import org.apache.commons.collections4.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.tools.AbstractObservable;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;


/**
 * This class is a collection of the parameter values of a single operator. Instances of
 * <code>Parameters</code> are created with respect to the declared list of {@link ParameterType}s
 * of an operator. If parameters are set using the {@link #setParameter(String, Object)} method and
 * the value exceeds the range, it is automatically corrected. If parameters are queried that are
 * not set, their default value is returned. <br/>
 * Upon setting the parameters, observers are notified, where the argument to the
 * {@link Observer#update(Observable, Object)} method will be the key of the changed parameter.
 * 
 * @author Ingo Mierswa, Simon Fischer
 */
public class Parameters extends AbstractObservable<String> implements Cloneable, Iterable<String>, Serializable {

    private static final long serialVersionUID = 4524151154296815685L;

    public static final char PAIR_SEPARATOR = '\u241D';

    public static final char RECORD_SEPARATOR = '\u241E';

    /** Maps parameter keys (i.e. Strings) to their value (Objects). */
    protected final Map<String, String> keyToValueMap = new LinkedHashMap<>();

    /** Maps parameter keys (i.e. Strings) to their <code>ParameterType</code>. */
    protected final Map<String, ParameterType> keyToTypeMap = new LinkedHashMap<>();

    /** Creates an empty parameters object without any parameter types. */
    public Parameters() {}

    /**
     * Constructs an instance of <code>Parameters</code> for the given list of
     * <code>ParameterTypes</code>. The list might be empty but not null.
     */
    public Parameters(List<ParameterType> parameterTypes) {
        if (parameterTypes != null)
            for (ParameterType type : parameterTypes) {
                addParameterType(type);
            }
    }

    /**
     * Returns a list of <tt>ParameterTypes</tt> describing the parameters of this operator. This
     * list will be generated during construction time of the Operator.
     */
    public Collection<ParameterType> getParameterTypes() {
        return keyToTypeMap.values();
    }

    public void addParameterType(ParameterType type) {
        keyToTypeMap.put(type.getKey(), type);
        if (!keyToValueMap.containsKey(type.getKey()) && type.getDefaultValueAsRawString() != null)
            keyToValueMap.put(type.getKey(), type.getDefaultValueAsRawString());
        fireUpdate(type.getKey());
    }

    public void removeParameterType(ParameterType type) {
        keyToTypeMap.remove(type.getKey());
        keyToValueMap.remove(type.getKey());
        fireUpdate(type.getKey());
    }

    public void setTypes(Collection<ParameterType> parameterTypes) {
        if (!CollectionUtils.isEqualCollection(parameterTypes, keyToTypeMap.values())) {
            keyToTypeMap.clear();
            for (ParameterType type : parameterTypes) {
                keyToTypeMap.put(type.getKey(), type);
                if (!keyToValueMap.containsKey(type.getKey()) && type.getDefaultValueAsRawString() != null) {
                    keyToValueMap.put(type.getKey(), type.getDefaultValueAsRawString());
                }
            }
            fireUpdate("");
        }
    }

    /** Performs a deep clone on this parameters object. */
    @Override
    public Object clone() {
        Parameters clone = new Parameters();

        clone.keyToValueMap.putAll(keyToValueMap);
        clone.keyToTypeMap.putAll(keyToTypeMap);

        return clone;
    }

    @Override
    public Iterator<String> iterator() {
        return keyToTypeMap.keySet().iterator();
    }

    /** Returns the type of the parameter with the given type. */
    public ParameterType getParameterType(String key) {
        return keyToTypeMap.get(key);
    }

    /**
     * Sets the parameter for the given key after performing a range-check. This method returns true
     * if the type was known and false if no parameter type was defined for this key.
     */
    public boolean setRawParameter(String key, String value) {
        ParameterType parameterType = keyToTypeMap.get(key);
        if (value == null) {
            keyToValueMap.remove(key);
        } else {
            keyToValueMap.put(key, value);
        }
        fireUpdate(key);
        return parameterType != null;
    }

    public boolean setParameter(String key, Object value) {
        ParameterType parameterType = keyToTypeMap.get(key);
        setRawParameter(key, parameterType == null ? String.valueOf(value) : parameterType.toRawString(value));
        return parameterType != null;
    }

    /**
     * Returns the value of the given parameter. If it was not yet set, the default value is set now
     * and a log message is issued. If the <code>ParameterType</code> does not provide a default
     * value, this may result in an error message. In subsequent calls of this method, the parameter
     * will be set. An OperatorException (UserError) will be thrown if a non-optional parameter was
     * not set.
     */
    public String getRawParameter(String key) throws UndefinedParameterError {
        if (keyToValueMap.containsKey(key)) {
            return keyToValueMap.get(key);
        } else {
            ParameterType type = keyToTypeMap.get(key);
            if (type == null) {
                return null;
            }
            Object defaultValue = type.getDefaultValue();
            if ((defaultValue == null) && !type.isOptional()) {
                throw new UndefinedParameterError(key);
            }
            if (defaultValue == null) {
                return null;
            } else {
                return type.toRawString(defaultValue);
            }
        }
    }

    public String getParameter(String key) throws UndefinedParameterError {
        String rawValue = getRawParameter(key);
        ParameterType type = keyToTypeMap.get(key);
        if (type == null)
            return rawValue;
        return type.fromRawString(rawValue);
    }

    /**
     * Returns the value of the parameter as specified by the process definition file (without
     * substituting default values etc.)
     */
    public String getParameterAsSpecified(String key) {
        return keyToValueMap.get(key);
    }

    /** As {@link #getRawParameter(String)}, but returns null rather than throwing an exception. */
    public String getParameterOrNull(String key) {
        if (keyToValueMap.containsKey(key)) {
            return keyToValueMap.get(key);
        } else {
            ParameterType type = keyToTypeMap.get(key);
            if (type == null) {
                return null;
            }
            Object value = type.getDefaultValue();
            if ((value == null) && !type.isOptional()) {
                return null;
            }
            if (value == null) {
                return null;
            } else {
                return type.toRawString(value);
            }
        }
    }

    /**
     * Returns a set view of all parameter keys defined by parameter types.
     */
    public Set<String> getKeys() {
        return keyToTypeMap.keySet();
    }

    /**
     * This method returns the keys of all defined values. This might be disjunct with the keys
     * defined in the keyToType map, because the operator might contain parameter values, which do
     * not correspond to any parameter type. This is used for example during import, when parameters
     * are renamed.
     */
    public Set<String> getDefinedKeys() {
        return keyToValueMap.keySet();
    }

    /**
     * Returns true if the given parameters are not null and are the same like this parameters.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Parameters)) {
            return false;
        } else {
            Parameters p = (Parameters) o;
            return p.keyToValueMap.equals(this.keyToValueMap);
        }
    }

    @Override
    public int hashCode() {
        return keyToValueMap.hashCode();
    }

    /** Appends the elements describing these Parameters to the given element. */
    public void appendXML(Element toElement, boolean hideDefault, Document doc) {

        // for (String key : keyToTypeMap.keySet()) {
        for (String key : keyToValueMap.keySet()) {
            String value = keyToValueMap.get(key);
            ParameterType type = keyToTypeMap.get(key);
            Element paramElement;
            if (type != null) {
                paramElement = type.getXML(key, value, hideDefault, doc);
            } else {
                paramElement = doc.createElement("parameter");
                paramElement.setAttribute("key", key);
                paramElement.setAttribute("value", value.toString());
            }
            if (paramElement != null) {
                toElement.appendChild(paramElement);
            }
        }
    }

    @Override
    public String toString() {
        return this.keyToValueMap.toString();
    }

    /** Notify all set parameters of the operator renaming */
    public void notifyRenaming(String oldName, String newName) {
        if (!Objects.equals(oldName, newName)) {
            notifyRenameReplace((t, v) -> t.notifyOperatorRenaming(oldName, newName, v));
        }
    }

    /**
     * This method is called when the operator given by {@code oldName} (and {@code oldOp} if it is
     * not {@code null}) was replaced with the operator described by {@code newName} and
     * {@code newOp}. This will inform all set {@link ParameterType parameters} of the replacing.
     *
     * @param oldName
     *            the name of the old operator
     * @param oldOp
     *            the old operator; can be {@code null}
     * @param newName
     *            the name of the new operator
     * @param newOp
     *            the new operator; must not be {@code null}
     * @see ParameterType#notifyOperatorReplacing(String, Operator, String, Operator, String)
     * @since 9.3
     */
    public void notifyReplacing(String oldName, Operator oldOp, String newName, Operator newOp) {
        notifyRenameReplace((t, v) -> t.notifyOperatorReplacing(oldName, oldOp, newName, newOp, v));
    }

    /** @since 9.3 */
    private void notifyRenameReplace(BiFunction<ParameterType, String, String> replacer) {
        for (Entry<String, String> entry : keyToValueMap.entrySet()) {
            ParameterType type = keyToTypeMap.get(entry.getKey());
            if (type != null && entry.getValue() != null) {
                entry.setValue(replacer.apply(type, entry.getValue()));
            }
        }
        keyToValueMap.values().removeIf(Objects::isNull);
    }

    /** Renames a parameter, e.g. during importing old XML process files. */
    public void renameParameter(String oldAttributeName, String newAttributeName) {
        String value = keyToValueMap.get(oldAttributeName);
        if (value != null) {
            keyToValueMap.remove(oldAttributeName);
            keyToValueMap.put(newAttributeName, value);
        }
    }

    /**
     * Returns true if the parameter is set or has a default value.
     * 
     * @see Parameters#isSpecified(String)
     */
    public boolean isSet(String parameterKey) {
        if (keyToValueMap.containsKey(parameterKey)) {
            return true;
        } else {
            // check for default if we have a type registered for this key
            ParameterType type = keyToTypeMap.get(parameterKey);
            if (type == null) {
                return false;
            }
            return type.getDefaultValue() != null;
        }
    }

    /**
     * Returns true iff the parameter value was explicitly set (as opposed to {@link #isSet(String)}
     * which also takes into account a possible default value.
     */
    public boolean isSpecified(String key) {
        return keyToValueMap.containsKey(key);
    }

    public void setParameters(Parameters parameters) {
        this.keyToValueMap.putAll(parameters.keyToValueMap);
        fireUpdate();
    }


}
