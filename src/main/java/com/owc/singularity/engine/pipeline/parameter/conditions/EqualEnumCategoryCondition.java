/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter.conditions;


import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeEnumCategory;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;


/**
 * This condition checks if a type parameter (category) has a certain value.
 * 
 * @author Sebastian Land, Ingo Mierswa
 */
public class EqualEnumCategoryCondition<E extends Enum<E>> extends ParameterCondition {


    private E[] fulfillingOptions;

    public EqualEnumCategoryCondition(ParameterHandler handler, String conditionParameter, boolean becomeMandatory, E... fullfillingTypes) {
        super(handler, conditionParameter, becomeMandatory);
        this.fulfillingOptions = fullfillingTypes;

    }

    @Override
    public boolean isConditionFullfilled() {
        try {
            E actualValue = (E) ParameterTypeEnumCategory.getParameterAsEnumValue(parameterHandler, getConditionParameter(), fulfillingOptions[0].getClass());
            for (E type : fulfillingOptions) {
                if (type.equals(actualValue))
                    return true;
            }
        } catch (UndefinedParameterError e) {
        }
        return false;
    }
}
