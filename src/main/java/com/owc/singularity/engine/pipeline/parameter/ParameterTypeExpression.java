/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.tools.expression.internal.function.process.ParameterValue;
import com.owc.singularity.studio.gui.parameters.celleditors.value.ExpressionValueCellEditor;


/**
 * This attribute type supports the user by letting him define an expression with a user interface
 * known from calculators.
 * <p>
 * For knowing attribute names before process execution a valid metadata transformation must be
 * performed. Otherwise, the user might type in the name, instead of choosing.
 *
 * @author Ingo Mierswa
 */
@ParameterTypeAnnotation(editor = ExpressionValueCellEditor.class)
public class ParameterTypeExpression extends ParameterTypeString {

    private static final long serialVersionUID = -1938925853519339382L;


    private static final String PARAMETER_VALUE_FUNCTION_NAME = ParameterValue.FUNCTION_NAME;
    private static final String RENAMING_PATTERN = PARAMETER_VALUE_FUNCTION_NAME + " *\\( *\"(%s)\" *,.*?\\)";

    private transient InputPort inPort;


    /**
     * This constructor will generate a ParameterType that does not use the {@link MetaData} of an
     * associated {@link InputPort} to verify the expressions.
     *
     * @param key
     *            the parameter key
     * @param description
     *            the parameter description
     */
    public ParameterTypeExpression(final String key, String description) {
        this(key, description, null, false);
    }

    public ParameterTypeExpression(final String key, String description, InputPort inPort) {
        this(key, description, inPort, false);
    }

    public ParameterTypeExpression(final String key, String description, final InputPort inPort, boolean optional) {
        super(key, description, optional);
        this.inPort = inPort;
    }

    @Override
    public Object getDefaultValue() {
        return "";
    }

    /**
     * Returns the input port associated with this ParameterType. This might be null!
     */
    public InputPort getInputPort() {
        return inPort;
    }


    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) {
        // return value as it is and
        // do not replace any variables in the parameter value
        return parameterValue;
    }

    @Override
    public String substitutePredefinedVariables(String parameterValue, Operator operator) {
        // return value as it is and
        // do not replace any predefined variables in the parameter value
        return parameterValue;
    }

    @Override
    public String notifyOperatorRenaming(String oldName, String newName, String value) {
        if (!value.contains(PARAMETER_VALUE_FUNCTION_NAME) || !value.contains(oldName)) {
            return value;
        }
        Pattern pattern = Pattern.compile(String.format(RENAMING_PATTERN, Pattern.quote(oldName)));
        Matcher matcher = pattern.matcher(value);
        int pos = 0;
        StringBuilder newValue = new StringBuilder();
        while (matcher.find(pos)) {
            int start = matcher.start(1);
            // constant part before the first match or after previous match
            if (start > pos) {
                newValue.append(value, pos, start);
            }
            newValue.append(newName);
            pos = matcher.end(1);
        }
        if (pos > 0 && pos < value.length()) {
            newValue.append(value.substring(pos));
        }
        return newValue.toString();
    }
}
