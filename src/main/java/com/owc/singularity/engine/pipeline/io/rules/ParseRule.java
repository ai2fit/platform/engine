/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.tools.VersionNumber;


/**
 * Rule that defines how operators are imported from earlier SingularityEngine versions.
 * 
 * @author Simon Fischer
 * 
 */
public interface ParseRule {

    /**
     * Applies the rule and possibly returns a message describing what has been modified. This takes
     * only place if the rule applies as well to the operator as to the version of the process we
     * are importing or the given processVersion is null. Null is returned if the rule did not
     * apply.
     * 
     * @param parser
     */
    public String apply(Operator operator, VersionNumber processVersion, XMLPipelineParser parser);
}
