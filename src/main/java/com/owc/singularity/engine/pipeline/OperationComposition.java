package com.owc.singularity.engine.pipeline;

import com.owc.singularity.engine.operator.ExecutionUnit;

/**
 * This is here only for forward compatibility with upcoming changes.
 * 
 * @deprecated This will be replaced in future versions with a more meaningful implementation. This
 *             is going to replace the entire ExecutionUnit's part that is for defining a pipeline.
 * 
 */
public record OperationComposition(ExecutionUnit unit) {

    public String getName() {
        return unit.getName();
    }

}
