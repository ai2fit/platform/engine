package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.*;

import javax.xml.xpath.*;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

/**
 * A {@link CompatibilityTransformer}, which replaces variable values inside the "variable" tag as
 * well as in the "parameter" tag. However, this is a very simple implementation and should be
 * expanded.
 */
public abstract class VariableReplacementTransformer implements CompatibilityTransformer {

    protected static final XPathFactory xpathfactory = XPathFactory.newInstance();

    private final String newVariableKey;
    private final String formerVariableKey;
    private final XPathExpression variableExpr;
    private final XPathExpression parameterExpr;

    protected XPath xpath;


    public VariableReplacementTransformer(String formerVariableKey, String newVariableKey) throws Exception {
        this.newVariableKey = newVariableKey;
        this.formerVariableKey = formerVariableKey;
        xpath = xpathfactory.newXPath();
        variableExpr = xpath.compile("//variable");
        parameterExpr = xpath.compile("//parameter");
    }


    @Override
    public void apply(Document document, CompatibilityTransformationLog log) {
        try {
            // change variable key defined in context
            NodeList variableNodes = (NodeList) variableExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < variableNodes.getLength(); i++) {
                Node variableNode = variableNodes.item(i);
                Node variableKeyNode = variableNode.getChildNodes().item(1).getFirstChild();
                if (variableKeyNode != null && Objects.equals(variableKeyNode.getNodeValue(), formerVariableKey))
                    variableKeyNode.setNodeValue(newVariableKey);
            }
            // change variable key in parameter occurrences
            NodeList parameterNodes = (NodeList) parameterExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < parameterNodes.getLength(); i++) {
                Node parameterNode = parameterNodes.item(i);
                NamedNodeMap attributes = parameterNode.getAttributes();
                // check and replace key occurrence
                Node keyAttribute = attributes.getNamedItem("key");
                String keyValue = keyAttribute.getNodeValue();
                if (keyValue.contains(formerVariableKey))
                    keyAttribute.setNodeValue(keyValue.replaceAll("\\b" + formerVariableKey + "\\b", newVariableKey));
                // check and replace value occurrence
                Node valueAttribute = attributes.getNamedItem("value");
                String valueValue = valueAttribute.getNodeValue();
                if (valueValue.contains(formerVariableKey))
                    valueAttribute.setNodeValue(valueValue.replaceAll("\\b" + formerVariableKey + "\\b", newVariableKey));
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Variable conversion failed: " + e.getMessage());
        }
    }
}
