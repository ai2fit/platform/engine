package com.owc.singularity.engine.pipeline.io.transformer;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is the superclass for all transformers of an imported process xml dom.
 */
public abstract class AbstractXPathCompatibilityTransformer implements CompatibilityTransformer {

    protected static final XPathFactory xpathfactory = XPathFactory.newInstance();
    private XPathExpression expr;

    protected AbstractXPathCompatibilityTransformer(String xPathExpression) throws Exception {
        XPath xpath = xpathfactory.newXPath();
        expr = xpath.compile(xPathExpression);
    }

    @Override
    public void apply(Document document, CompatibilityTransformationLog log) {
        try {
            Object result = expr.evaluate(document, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            logApplication(nodes, log);
            for (int i = 0; i < nodes.getLength(); i++) {
                apply(nodes.item(i), log, document);
            }
        } catch (Exception e) {
            log.log(Level.DEBUG, e.getMessage());
        }
    }

    /**
     * Subclasses might override in order to log that this rule is applied at all instead of listing
     * each call in the {@link #apply(Document, CompatibilityTransformationLog)} method.
     * 
     * @param nodes
     *            the nodes that this transformer works on
     * @param log
     *            the log
     */
    protected void logApplication(NodeList nodes, CompatibilityTransformationLog log) {}

    protected abstract void apply(Node item, CompatibilityTransformationLog log, Document document);
}
