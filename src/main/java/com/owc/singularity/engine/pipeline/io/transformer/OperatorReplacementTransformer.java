package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.*;
import java.util.Map.Entry;

import javax.xml.xpath.*;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

import com.owc.singularity.engine.tools.container.Pair;

/**
 * An {@link CompatibilityTransformer}, which replaces the specified operator with another. The
 * constructors allow to specify one to one replacements for parameter as well as input- and output
 * port names. Additionally, Parameter values of the original operator can be adjusted using
 * {@link #deriveParameterValues(HashMap)} and list parameter values can be adjusted using
 * {@link #deriveListValues(HashMap)}. Finally, the {@link #postProcessOperator(String, Document)}
 * allows subclasses access to the actual xml.
 */
public abstract class OperatorReplacementTransformer implements CompatibilityTransformer {

    protected static final XPathFactory xpathfactory = XPathFactory.newInstance();

    private String newOperatorKey;
    private Map<String, String> parameterReplacementMap;
    private Map<String, String> inputPortRenameReplacementMap;
    private Map<String, String> outputPortRenameReplacementMap;
    private Set<String> parameterRemovalSet;

    private XPathExpression parameterExpr;
    private XPathExpression listExpr;

    private XPathExpression operatorExpr;

    protected XPath xpath;


    public OperatorReplacementTransformer(String formerOperatorKey, String newOperatorKey, Map<String, String> parameterReplacementMap,
            Set<String> parameterRemovalSet, Map<String, String> inputPortRenameReplacementMap, Map<String, String> outputPortRenameReplacementMap)
            throws Exception {
        this.newOperatorKey = newOperatorKey;
        this.parameterReplacementMap = parameterReplacementMap;
        this.parameterRemovalSet = parameterRemovalSet;
        this.inputPortRenameReplacementMap = inputPortRenameReplacementMap;
        this.outputPortRenameReplacementMap = outputPortRenameReplacementMap;

        xpath = xpathfactory.newXPath();
        operatorExpr = xpath.compile("//operator[@class='" + formerOperatorKey + "']");
        parameterExpr = xpath.compile("parameter|list|enumeration");
        listExpr = xpath.compile("list|enumeration");
    }


    public OperatorReplacementTransformer(String formerOperatorKey, String newOperatorKey) throws Exception {
        this(formerOperatorKey, newOperatorKey, null, Set.of(), null, null);
    }


    public OperatorReplacementTransformer(String formerOperatorKey, String newOperatorKey, Map<String, String> parameterReplacementMap) throws Exception {
        this(formerOperatorKey, newOperatorKey, parameterReplacementMap, Set.of(), null, null);
    }

    public OperatorReplacementTransformer(String formerOperatorKey, String newOperatorKey, Map<String, String> parameterReplacementMap,
            Set<String> parameterRemovelSet) throws Exception {
        this(formerOperatorKey, newOperatorKey, parameterReplacementMap, parameterRemovelSet, null, null);
    }


    @Override
    public void apply(Document document, CompatibilityTransformationLog log) {
        // change operator class key
        try {
            NodeList operatorNodes = (NodeList) operatorExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < operatorNodes.getLength(); i++) {
                Node operatorNode = operatorNodes.item(i);
                NamedNodeMap attributes = operatorNode.getAttributes();
                attributes.getNamedItem("class").setNodeValue(newOperatorKey);

                String operatorName = attributes.getNamedItem("name").getNodeValue();


                // change ports
                if (inputPortRenameReplacementMap != null && !inputPortRenameReplacementMap.isEmpty()) {
                    try {
                        XPathExpression inputPortExpr = xpath.compile("//connect[@to_op='" + operatorName + "']");
                        NodeList inputPortNodes = (NodeList) inputPortExpr.evaluate(document, XPathConstants.NODESET);
                        for (int j = 0; j < inputPortNodes.getLength(); j++) {
                            Node connectionNode = inputPortNodes.item(j);
                            Node toPortAttribute = connectionNode.getAttributes().getNamedItem("to_port");
                            String currentPort = toPortAttribute.getNodeValue();
                            toPortAttribute.setNodeValue(inputPortRenameReplacementMap.getOrDefault(currentPort, currentPort));
                        }
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Operator conversion failed: " + e.getMessage());
                    }
                }
                if (outputPortRenameReplacementMap != null && !outputPortRenameReplacementMap.isEmpty()) {
                    try {
                        XPathExpression outputPortExpr = xpath.compile("//connect[@from_op='" + operatorName + "']");
                        NodeList outputPortNodes = (NodeList) outputPortExpr.evaluate(document, XPathConstants.NODESET);
                        for (int j = 0; j < outputPortNodes.getLength(); j++) {
                            Node connectionNode = outputPortNodes.item(j);
                            Node fromPortAttribute = connectionNode.getAttributes().getNamedItem("from_port");
                            String currentPort = fromPortAttribute.getNodeValue();
                            fromPortAttribute.setNodeValue(outputPortRenameReplacementMap.getOrDefault(currentPort, currentPort));
                        }
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Operator conversion failed: " + e.getMessage());
                    }
                }

                // correct parameter settings
                processParameters(operatorNode, document);
                // correct list settings
                processLists(operatorNode, document);

                // perform additional changes
                postProcessOperator(operatorName, document);

                log.log(Level.INFO, "Operator '" + operatorName + "' converted to new type '" + newOperatorKey + "'.");
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Operator conversion failed: " + e.getMessage());
        }
    }

    private void processParameters(Node operatorNode, Document document) {
        // change parameters and their values
        try {
            HashMap<String, String> parameterValues = new HashMap<>();
            Object result = parameterExpr.evaluate(operatorNode, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            for (int i = 0; i < nodes.getLength(); i++) {
                Node parameterNode = nodes.item(i);
                // replacing parameter names
                if (parameterReplacementMap != null && !parameterReplacementMap.isEmpty()) {
                    replaceParameter(parameterNode);
                }

                // storing values with replaced parameter names
                NamedNodeMap attributes = parameterNode.getAttributes();
                Node keyAttribute = attributes.getNamedItem("key");
                Node valueAttribute = attributes.getNamedItem("value");
                String parameterKey = keyAttribute.getNodeValue();
                // this could be a placeholder parameter, or list that have no value attribute
                if (valueAttribute != null) {
                    parameterValues.put(parameterKey, valueAttribute.getNodeValue());
                }

                // remove parameter if part of removal set
                if (parameterRemovalSet.contains(parameterKey)) {
                    operatorNode.removeChild(parameterNode);
                }
            }

            HashMap<String, String> derivedParameterValues = deriveParameterValues(parameterValues);
            if (derivedParameterValues != null) {
                // overwrite existing parameters
                for (int i = 0; i < nodes.getLength(); i++) {
                    Node parameterNode = nodes.item(i);
                    NamedNodeMap attributes = parameterNode.getAttributes();
                    Node keyAttribute = attributes.getNamedItem("key");
                    Node valueAttribute = attributes.getNamedItem("value");
                    if (derivedParameterValues.containsKey(keyAttribute.getNodeValue())) {
                        valueAttribute.setNodeValue(derivedParameterValues.remove(keyAttribute.getNodeValue()));
                    }
                }

                // add remaining, new parameters
                for (Entry<String, String> entry : derivedParameterValues.entrySet()) {
                    Element parameterNode = document.createElement("parameter");
                    parameterNode.setAttribute("key", entry.getKey());
                    parameterNode.setAttribute("value", entry.getValue());
                    operatorNode.appendChild(parameterNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void processLists(Node operatorNode, Document document) {
        // change lists and their parameter values
        try {
            HashMap<String, List<Pair<String, String>>> listValues = new HashMap<>();
            Object listResult = listExpr.evaluate(operatorNode, XPathConstants.NODESET);
            NodeList listNodes = (NodeList) listResult;

            // read in list values
            for (int i = 0; i < listNodes.getLength(); i++) {
                Node listNode = listNodes.item(i);
                NamedNodeMap attributes = listNode.getAttributes();
                Node keyAttribute = attributes.getNamedItem("key");
                String listParameterKey = keyAttribute.getNodeValue();
                NodeList childNodes = listNode.getChildNodes();
                List<Pair<String, String>> listParametersMap = new LinkedList<>();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node childNode = childNodes.item(j);
                    NamedNodeMap childAttributes = childNode.getAttributes();
                    if (childAttributes != null) {
                        Node childKeyAttribute = childAttributes.getNamedItem("key");
                        Node childValueAttribute = childAttributes.getNamedItem("value");
                        String childParameterKey = childKeyAttribute.getNodeValue();
                        // this could be a placeholder parameter
                        if (childValueAttribute != null) {
                            listParametersMap.add(new Pair<>(childParameterKey, childValueAttribute.getNodeValue()));
                        }
                    }
                }
                listValues.put(listParameterKey, listParametersMap);
            }

            // derive new list values
            HashMap<String, List<Pair<String, String>>> derivedListValues = deriveListValues(listValues);

            if (derivedListValues != null) {
                // overwrite existing list parameters and add new list entries
                for (int i = 0; i < listNodes.getLength(); i++) {
                    Node listNode = listNodes.item(i);
                    NamedNodeMap attributes = listNode.getAttributes();
                    Node keyAttribute = attributes.getNamedItem("key");
                    String listParameterKey = keyAttribute.getNodeValue();
                    if (derivedListValues.containsKey(listParameterKey)) {
                        NodeList childNodes = listNode.getChildNodes();
                        Iterator<Pair<String, String>> derivedValuesIterator = derivedListValues.get(listParameterKey).listIterator();
                        // overwrite existing list parameters
                        for (int j = 0; j < childNodes.getLength(); j++) {
                            Node childNode = childNodes.item(j);
                            NamedNodeMap childAttributes = childNode.getAttributes();
                            if (childAttributes != null) {
                                Node childValueAttribute = childAttributes.getNamedItem("value");
                                childValueAttribute.setNodeValue(derivedValuesIterator.next().getSecond());
                                derivedValuesIterator.remove();
                            }
                        }
                        // add remaining, new parameters
                        for (Pair<String, String> entry : derivedListValues.get(listParameterKey)) {
                            Element parameterNode = document.createElement("parameter");
                            parameterNode.setAttribute("key", entry.getFirst());
                            parameterNode.setAttribute("value", entry.getSecond());
                            listNode.appendChild(parameterNode);
                        }
                        derivedListValues.remove(listParameterKey);
                    }
                }

                // add remaining, new lists
                for (Entry<String, List<Pair<String, String>>> entry : derivedListValues.entrySet()) {
                    Element listNode = document.createElement("list");
                    listNode.setAttribute("key", entry.getKey());
                    // add remaining, new parameters
                    for (Pair<String, String> parameterEntry : entry.getValue()) {
                        Element parameterNode = document.createElement("parameter");
                        parameterNode.setAttribute("key", parameterEntry.getFirst());
                        parameterNode.setAttribute("value", parameterEntry.getSecond());
                        listNode.appendChild(parameterNode);
                    }
                    operatorNode.appendChild(listNode);
                    operatorNode.appendChild(document.createElement("text"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method allows subclasses to derive new parameter values.
     * 
     * @param parameterValues
     *            Old parameter values with replaced keys
     * @return null to deactivate or a map with new key value pairs.
     */
    protected HashMap<String, String> deriveParameterValues(HashMap<String, String> parameterValues) {
        return null;
    }

    /**
     * This method allows subclasses to derive new list values.
     *
     * @param listValues
     *            Old list values with replaced keys
     * @return null to deactivate or a map with new key value pairs.
     */
    protected HashMap<String, List<Pair<String, String>>> deriveListValues(HashMap<String, List<Pair<String, String>>> listValues) {
        return listValues;
    }


    /**
     * Subclasses can implement that method to change additional things specific to this operator.
     * 
     * @param operatorName
     *            the name of the operator to identify relevant xml pieces
     * @param document
     *            the xml document
     * @throws XPathExpressionException
     */
    protected void postProcessOperator(String operatorName, Document document) throws XPathExpressionException {}


    protected void replaceParameter(Node parameterNode) {
        NamedNodeMap attributes = parameterNode.getAttributes();
        Node keyAttribute = attributes.getNamedItem("key");
        keyAttribute.setNodeValue(parameterReplacementMap.getOrDefault(keyAttribute.getNodeValue(), keyAttribute.getNodeValue()));
    }

    /**
     * This is a helper function to generate a Connection Element, which can then be inserted to a
     * pipeline Node.
     * 
     * @param document
     * @param from_op
     * @param from_port
     * @param to_op
     * @param to_port
     * @return Connection Element
     */
    public static Element createConnection(Document document, String from_op, String from_port, String to_op, String to_port) {
        Element connection = document.createElement("connect");
        if (from_op != null)
            connection.setAttribute("from_op", from_op);
        if (from_port != null)
            connection.setAttribute("from_port", from_port);
        if (to_op != null)
            connection.setAttribute("to_op", to_op);
        if (to_port != null)
            connection.setAttribute("to_port", to_port);
        return connection;
    }

    /**
     * This is a helper function to generate an Operator Element, which can then be inserted to a
     * pipeline Node.
     * 
     * @param document
     * @param operatorClass
     * @param operatorName
     * @param activated
     * @param parameterValues
     * @return Operator Element
     */
    public static Element createOperator(Document document, String operatorClass, String operatorName, String activated, Map<String, String> parameterValues) {
        Element operator = document.createElement("operator");
        operator.setAttribute("class", operatorClass);
        operator.setAttribute("name", operatorName);
        operator.setAttribute("activated", activated);
        for (Map.Entry<String, String> entry : parameterValues.entrySet()) {
            Element parameter = document.createElement("parameter");
            parameter.setAttribute("key", entry.getKey());
            parameter.setAttribute("value", entry.getValue());
            operator.appendChild(parameter);
        }
        return operator;
    }

    public static void createParameter(Document document, Element opElement, String key, String value) {
        Element parameterNodeName = document.createElement("parameter");
        parameterNodeName.setAttribute("key", key);
        parameterNodeName.setAttribute("value", value);
        opElement.appendChild(parameterNodeName);
    }
}
