/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

import com.michaelbaranov.microba.calendar.DatePicker;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.studio.gui.parameters.celleditors.value.DateValueCellEditor;


/**
 * A parameter type for Dates. It is represented by a {@link DatePicker} element in the GUI. The
 * date is internally stored as string. To get a {@link Date} object use
 * {@link ParameterTypeDate#getParameterAsDate(String, Operator)}.
 *
 * @author Nils Woehler
 *
 */
@ParameterTypeAnnotation(editor = DateValueCellEditor.class)
public class ParameterTypeDate extends ParameterTypeSingle {

    public static final String PARAMETER_TIMEZONE = "timezone";
    public static String[] zoneIDs = getZoneIDs();
    public static int defaultTimeZone = getDefaultZoneIndex();
    public static final String SYSTEM_TIME_ZONE_NAME = "SYSTEM";
    public static int systemTimeZone = 0;


    private static final long serialVersionUID = 1L;

    private Date defaultValue = null;

    public static DateTimeFormatter DEFAULT_DATE_FOMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final ThreadLocal<SimpleDateFormat> DATE_FORMAT;

    static {
        // ThreadLocale because this is static and used by other threads
        // and DateFormats are NOT threadsafe
        DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {

            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss Z");

            @Override
            protected SimpleDateFormat initialValue() {
                return format;
            }
        };
    }


    public ParameterTypeDate(String key, String description) {
        super(key, description);
    }

    public ParameterTypeDate(String key, String description, Date defaultValue) {
        super(key, description);
        setDefaultValue(defaultValue);
    }

    public ParameterTypeDate(String key, String description, boolean optional) {
        super(key, description);
        setOptional(optional);
        setDefaultValue(null);
    }

    public ParameterTypeDate(String key, String description, Date defaultValue, boolean optional) {
        super(key, description);
        setOptional(optional);
        setDefaultValue(defaultValue);
    }

    @Override
    public String getRange() {
        return null; // no range here as showRange() returns null
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = (Date) defaultValue;
    }

    @Override
    public void setDefaultValueAsRawString(String value) {
        try {
            this.defaultValue = DATE_FORMAT.get().parse(value);
        } catch (ParseException e) {
        }
    }

    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public boolean showRange() {
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @return always {@code false}
     */
    @Override
    public boolean isSensitive() {
        return false;
    }

    @Override
    public String toRawString(Object value) {
        if (value == null) {
            return "";
        }
        String ret = null;
        if (value instanceof Date) {
            ret = DATE_FORMAT.get().format(value);
        } else {
            ret = String.valueOf(value);
        }
        return ret;
    }

    public static ParameterType getZoneParameter() {
        return new ParameterTypeCategory(PARAMETER_TIMEZONE, "The timezone in which to operate. Important for leap hours and week computation.", zoneIDs,
                defaultTimeZone);
    }

    public static ZoneId evaluateZoneParameter(Operator operator) throws UserError {
        String zoneIDName = operator.getParameterAsString(PARAMETER_TIMEZONE);
        if (zoneIDName.equals(SYSTEM_TIME_ZONE_NAME)) {
            return ZoneId.of(zoneIDs[defaultTimeZone]);
        }
        try {
            return ZoneId.of(zoneIDName);
        } catch (Exception e) {
            throw new UserError(operator, e, "unkown_zoneid", zoneIDName);
        }
    }

    public static Date getParameterAsDate(String key, Operator operator) throws UndefinedParameterError, UserError {
        String value = operator.getParameter(key);
        if (value == null || value.trim().isEmpty()) {
            throw new UndefinedParameterError(key, operator);
        }
        try {
            return ParameterTypeDate.DATE_FORMAT.get().parse(value);
        } catch (ParseException e) {
            throw new UserError(operator, "wrong_date_format", value, key.replace('_', ' '));
        }
    }

    /**
     * Can be used to check whether the current string is a valid date string for this parameter
     * type.
     */
    public static boolean isValidDate(String dateString) {
        try {
            return dateString != null && DATE_FORMAT.get().parse(dateString) != null;
        } catch (ParseException e) {
            return false;
        }
    }

    private static String[] getZoneIDs() {
        String[] zoneArray = ZoneId.getAvailableZoneIds().toArray(new String[0]);
        Arrays.sort(zoneArray);

        String[] zoneArrayWithSystem = new String[zoneArray.length + 1];
        zoneArrayWithSystem[0] = SYSTEM_TIME_ZONE_NAME;
        System.arraycopy(zoneArray, 0, zoneArrayWithSystem, 1, zoneArray.length);
        return zoneArrayWithSystem;
    }

    private static int getDefaultZoneIndex() {
        return Arrays.binarySearch(zoneIDs, ZoneId.systemDefault().toString());
    }

}
