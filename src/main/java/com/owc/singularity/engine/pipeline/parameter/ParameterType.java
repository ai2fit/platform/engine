/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.io.Serializable;
import java.util.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.pipeline.parameter.conditions.ParameterCondition;


/**
 * A ParameterType holds information about type, range, and default value of a parameter. Lists of
 * ParameterTypes are provided by operators.
 * <p>
 * Sensitive information: <br/>
 * If your parameter is expected to never contain sensitive data, e.g. the number of iterations for
 * an algorithm, overwrite {@link #isSensitive()} and and return {@code false}. This method is used
 * to determine how parameters are handled in certain places, e.g. if their value is replaced before
 * uploading the process for operator recommendations. The default implementation always returns
 * {@code true}.
 * </p>
 *
 * @author Ingo Mierswa, Simon Fischer
 * @see com.owc.singularity.engine.operator.Operator#getParameterTypes()
 */
public abstract class ParameterType implements Comparable<ParameterType>, Serializable {

    private static final long serialVersionUID = 5296461242851710130L;

    /** The key of this parameter. */
    private String key;

    /** The documentation. Used as tooltip text... */
    private String description;

    /**
     * Indicates if a parameter is a primary parameter of an operator, i.e. one that gets activated
     * with a double-click.
     */
    private boolean primary = false;

    /**
     * Indicates if this parameter is hidden and is not shown in the GUI. May be used in conjunction
     * with a configuration wizard which lets the user configure the parameter.
     */
    private boolean isHidden = false;

    /** Indicates if the range should be displayed. */
    private boolean showRange = true;

    /**
     * Indicates if this parameter is optional unless a dependency condition made it mandatory.
     */
    private boolean isOptional = true;

    /**
     * Indicates that this parameter is deprecated and remains only for compatibility reasons during
     * loading of older processes. It should neither be shown nor documented.
     */
    private boolean isDeprecated = false;

    /**
     * This collection assembles all conditions to be met to show this parameter within the gui.
     */
    private final transient Collection<ParameterCondition> conditions = new LinkedList<>();


    /** Creates a new ParameterType. */
    public ParameterType(String key, String description) {
        this.key = key;
        this.description = description;
    }

    /** Returns a human readable description of the range. */
    public abstract String getRange();

    /** Returns a value that can be used if the parameter is not set. */
    public abstract Object getDefaultValue();

    /**
     * Returns the correct string representation of the default value. If the default is undefined,
     * it returns null.
     */
    public String getDefaultValueAsRawString() {
        return toRawString(getDefaultValue());
    }

    /** Sets the default value. */
    public abstract void setDefaultValue(Object defaultValue);

    public void setDefaultValueAsRawString(String value) {
        setDefaultValue(value);
    }

    /**
     * Returns true if the values of this parameter type are numerical, i.e. might be parsed by
     * {@link Double#parseDouble(String)}. Otherwise false should be returned. This method might be
     * used by parameter logging operators.
     */
    public abstract boolean isNumerical();

    public boolean showRange() {
        return showRange;
    }

    public void setShowRange(boolean showRange) {
        this.showRange = showRange;
    }

    /**
     * This method will be invoked by the Parameters after a parameter was set. The default
     * implementation is empty but subclasses might override this method, e.g. for a decryption of
     * passwords.
     */
    public String fromRawString(String value) {
        return value;
    }

    /**
     * Returns true if this parameter is hidden or not all dependency conditions are fulfilled. Then
     * the parameter will not be shown in the GUI. The default implementation returns true which
     * should be the normal case.
     * <p>
     * Please note that this method cannot be accessed during getParameterTypes() method
     * invocations, because it relies on getting the Parameters object, which is then not created.
     */
    public boolean isHidden() {
        return isHidden || isDeprecated || !conditions.stream().allMatch(ParameterCondition::dependencyMet);
    }

    public Collection<ParameterCondition> getConditions() {
        return Collections.unmodifiableCollection(conditions);
    }

    /**
     * Sets if this parameter is hidden (value true) and will not be shown in the GUI.
     */
    public void setHidden(boolean hidden) {
        this.isHidden = hidden;
    }

    /**
     * This returns whether this parameter is deprecated.
     */
    public boolean isDeprecated() {
        return this.isDeprecated;
    }

    /**
     * This method indicates that this parameter is deprecated and isn't used anymore beside from
     * loading old process files.
     */
    public void setDeprecated() {
        this.isDeprecated = true;
    }

    /**
     * This sets if the parameter is optional or must be entered. If it is not optional, it may not
     * be an expert parameter and the expert status will be ignored!
     */
    public final ParameterType setOptional(boolean isOptional) {
        this.isOptional = isOptional;
        return this;
    }

    /** Registers the given dependency condition. */
    public ParameterType registerDependencyCondition(ParameterCondition condition) {
        this.conditions.add(condition);
        return this;
    }

    public Collection<ParameterCondition> getDependencyConditions() {
        return this.conditions;
    }

    /**
     * Returns true if this parameter is optional. The default implementation returns true. Please
     * note that this method cannot be accessed during {@link Operator#getParameterTypes()} method
     * invocations, because it relies on getting the Parameters object, which is then not created.
     *
     */
    public final boolean isOptional() {
        if (isOptional) {
            // if parameter is optional per default: check conditions
            boolean becomeMandatory = false;
            for (ParameterCondition condition : conditions) {
                if (condition.dependencyMet()) {
                    becomeMandatory |= condition.becomeMandatory();
                } else {
                    return true;
                }
            }
            return !becomeMandatory;
        }
        // otherwise it is mandatory even without dependency
        return false;
    }

    /**
     * Checks whether the parameter is configured to be optional without looking at the parameter
     * conditions. This method can be invoked during {@link Operator#getParameterTypes()} method
     * invocations as it does not check parameter conditions. It does not reflect the actual state
     * though as parameter conditions might change an optional parameter to become mandatory.
     *
     * @return whether the parameter is optional without checking the parameter conditions
     */
    public final boolean isOptionalWithoutConditions() {
        return isOptional;
    }

    /** Sets the key. */
    public void setKey(String key) {
        this.key = key;
    }

    /** Returns the key. */
    public String getKey() {
        return key;
    }

    public Element getXML(String key, String value, boolean hideDefault, Document doc) {
        Element element = doc.createElement("parameter");
        element.setAttribute("key", key);
        if (value != null) {
            String rawString = toRawString(value);
            if (rawString.equals(getDefaultValueAsRawString())) {
                if (!hideDefault) {
                    element.setAttribute("value", rawString);
                } else {
                    return null;
                }
            } else {
                element.setAttribute("value", rawString);
            }
        } else {
            if (!hideDefault && getDefaultValue() != null) {
                element.setAttribute("value", getDefaultValueAsRawString());
            } else {
                return null;
            }
        }
        return element;
    }

    /** Returns a short description. */
    public String getDescription() {
        return description;
    }

    /** Sets the short description. */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * States whether a given parameter type implementation may contain sensitive information or
     * not. Sensitive information are obvious things like passwords, files, OAuth tokens, or
     * database connections. However less obvious things like SQL queries can also contain sensitive
     * information.
     *
     * @return always {@code true}
     */
    public boolean isSensitive() {
        return true;
    }

    /**
     * Sets if this parameter type is a primary parameter of an operator, i.e. one that can be
     * opened with a double-click. If not set, defaults to {@code false}, except for
     * {@link ParameterTypeConfiguration}. If more than one parameter type of an operator is set to
     * primary, the first one returned in the parameters collection will be considered the primary
     * one.
     *
     * @param primary
     *            {@code true} if it is a primary parameter; {@code false} otherwise
     */
    public void setPrimary(final boolean primary) {
        this.primary = primary;
    }

    /**
     * Returns if this is a primary parameter of an operator, i.e. one that can be opened with a
     * double click. Defaults to {@code false}.
     *
     * @return {@code true} if it is a primary parameter; {@code false} otherwise
     */
    public boolean isPrimary() {
        return primary;
    }

    /**
     * This method gives a hook for the parameter type to react to the renaming of an operator. It
     * must return the correctly modified parameter value as string.
     *
     * @return the unmodified <em>parameterValue</em> by default
     */
    public String notifyOperatorRenaming(String oldOperatorName, String newOperatorName, String parameterValue) {
        return parameterValue;
    }

    /**
     * This method gives a hook for the parameter type to react to the replacing of an operator. It
     * must return the correctly modified parameter value as string.
     *
     * @param oldName
     *            the name of the old operator; must not be {@code null}
     * @param oldOp
     *            the old operator; can be {@code null}
     * @param newName
     *            the name of the new operator; must not be {@code null}
     * @param newOp
     *            the new operator; must not be {@code null}
     * @param parameterValue
     *            the original parameter value
     * @return the same as {@link #notifyOperatorRenaming(String, String, String)
     *         notifyOperatorRenaming} by default
     */
    public String notifyOperatorReplacing(String oldName, Operator oldOp, String newName, Operator newOp, String parameterValue) {
        return notifyOperatorRenaming(oldName, newName, parameterValue);
    }

    /** Returns a string representation of this value. */
    public String toRawString(Object value) {
        if (value == null) {
            return null;
        } else {
            return value.toString();
        }
    }

    @Override
    public String toString() {
        return key + " (" + description + ")";
    }

    @Override
    public int compareTo(ParameterType o) {
        /* ParameterTypes are compared by key. */
        return this.key.compareTo(o.key);
    }

    /**
     * This method operates on the internal string representation of parameter values and replaces
     * variable expressions of the form %{variableName}.
     */
    public abstract String substituteVariables(String parameterValue, VariableHandler variableHandler) throws UndefinedParameterError;

    /**
     * This method replaces predefined variable values. It is called right after
     * {@link #substituteVariables(String, VariableHandler)}.
     * <p>
     * Override this method in case a custom parameter type should not replace predefined variables
     * on parameter value fetching.
     *
     * @param parameterValue
     *            the parameter value which is the result of
     *            {@link #substituteVariables(String, VariableHandler)}
     * @param operator
     *            the calling operator. Must not be <code>null</code>.
     * @return the parameter string with replaced predefined variables
     * @throws UndefinedParameterError
     *             in case a predefined variable is malformed
     */
    public String substitutePredefinedVariables(String parameterValue, Operator operator) throws UndefinedParameterError {
        return operator.getPipeline().getVariableHandler().resolvePredefinedVariables(parameterValue, operator);
    }


    /**
     * Shortcut method to search through already defined {@link ParameterType}s. This method is
     * handy to use in the getParameterTypes method in an Operator
     * 
     * @param types
     *            defined ParameterTypes
     * @param key
     *            the key associated with the parameter during definition
     * @return {@code null} if not found
     */
    public static ParameterType getParameterByKey(List<ParameterType> types, String key) {
        for (ParameterType parameterType : types) {
            if (parameterType.getKey().equals(key)) {
                return parameterType;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ParameterType other = (ParameterType) obj;
        return Objects.equals(key, other.key);
    }


}
