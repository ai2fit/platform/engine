package com.owc.singularity.engine.pipeline;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.ImplementingRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.SubPipelineRootOperator;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.parameter.*;


public class ImplementingPipeline extends AbstractPipeline {


    public ImplementingPipeline() {
        super(new ImplementingRootOperator());
    }

    public ImplementingPipeline(AbstractRootOperator rootOperator) {
        super(rootOperator);
        if (!(rootOperator instanceof ImplementingRootOperator))
            throw new IllegalArgumentException("Only implementing root operator permitted in implementing piplines.");
    }


    private ImplementingPipeline(ImplementingPipeline implementingPipeline) {
        super(implementingPipeline);
    }

    @Override
    public void setRootOperator(AbstractRootOperator root) {
        super.setRootOperator(root);
        root.getParameters().addObserver(new com.owc.singularity.engine.tools.Observer<String>() {

            @Override
            public void update(com.owc.singularity.engine.tools.Observable<String> observable, String arg) {
                getDevelopmentContext().refreshParameterTypes();
            }

        }, false);
        getDevelopmentContext().refreshParameterTypes();
    }

    @Override
    public AbstractPipeline clone() {
        return new ImplementingPipeline(this);
    }

    @Override
    public boolean isEffectivelyInstanceOf(Class<? extends AbstractPipeline> typeClass) {
        if (getRootOperator() != null && getPath() != null) {
            // try to get from abstract pipeline
            ImplementingRootOperator rootOperator = (ImplementingRootOperator) getRootOperator();


            AbstractPipeline pipeline = rootOperator.getImplementedPipeline();
            if (pipeline == null)
                return false;
            return pipeline.isEffectivelyInstanceOf(typeClass);
        }
        return false;
    }

    @Override
    public Class<? extends AbstractPipeline> getEffectiveType() {
        if (getRootOperator() != null && getPath() != null) {
            // try to get from abstract pipeline
            ImplementingRootOperator rootOperator = (ImplementingRootOperator) getRootOperator();


            AbstractPipeline pipeline = rootOperator.getImplementedPipeline();
            if (pipeline != null)
                return pipeline.getEffectiveType();
        }
        return ImplementingPipeline.class;
    }


    @Override
    public PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) throws UndefinedParameterError {
        return new ImplementingPipelineExecutionContext(context, rootOperator);
    }

    @Override
    public List<ParameterType> getExecutionContextParameterTypes() {
        List<ParameterType> types = super.getExecutionContextParameterTypes();
        if (rootOperator != null && getPath() != null) {
            // try to get from abstract pipeline
            AbstractPipeline pipeline = ((ImplementingRootOperator) rootOperator).getImplementedPipeline();
            if (pipeline != null) {
                List<ParameterType> developmentExecutionContextParameterTypes = pipeline.getExecutionContextParameterTypes();
                // remove variables that have been set in this root operator
                developmentExecutionContextParameterTypes = developmentExecutionContextParameterTypes.stream().filter(type -> {
                    if (type.getKey().startsWith("variable_")) {
                        try {
                            String value = rootOperator.getParameter("set_" + type.getKey().substring(9));
                            if (value != null && !value.isBlank())
                                return !Boolean.valueOf(ParameterTypeTupel.transformString2Tupel(value)[0]);
                        } catch (UndefinedVariableError e) {
                            return false;
                        } catch (UndefinedParameterError e) {
                        }
                    }
                    return true;
                }).toList();
                types.addAll(developmentExecutionContextParameterTypes);
            }

            try {
                // types for variables
                Map<String, String> defaultVariableValue = getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                        .stream()
                        .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
                for (String[] variableTupel : getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                    if (variableTupel[0] != null && !variableTupel[0].isBlank()) {
                        String variable = variableTupel[0];
                        String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableTupel[1]);
                        VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                        String defaultValue = defaultVariableValue.get(variable);
                        boolean isOptional = defaultValue != null && !defaultValue.isBlank();
                        ParameterType parameterType = switch (type) {
                            case DATE -> (new ParameterTypeDate("variable_" + variable, typeDefaultDescription[2], isOptional));
                            case FILE_PATH -> new ParameterTypeFile("variable_" + variable, typeDefaultDescription[2], null, isOptional);
                            case INTEGER -> new ParameterTypeInt("variable_" + variable, typeDefaultDescription[2], Integer.MIN_VALUE, Integer.MAX_VALUE,
                                    isOptional);
                            case REAL -> new ParameterTypeDouble("variable_" + variable, typeDefaultDescription[2], Double.NEGATIVE_INFINITY,
                                    Double.POSITIVE_INFINITY, isOptional);
                            case REPOSITORY_PATH -> new ParameterTypeRepositoryLocation("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case STRING -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case TIMESTAMP -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                        };
                        if (isOptional) {
                            parameterType.setDefaultValueAsRawString(defaultValue);
                        }
                        types.add(parameterType);
                    }
                }
            } catch (UndefinedParameterError e) {
            }
        }
        return types;
    }

    public static class ImplementingPipelineExecutionContext extends PipelineExecutionContext {

        public DevelopmentExecutionContext developmentContext = null;
        public final HashMap<String, String> variableValues = new HashMap<>();

        public ImplementingPipelineExecutionContext() {
            super();
        }

        public ImplementingPipelineExecutionContext(DevelopmentExecutionContext context, AbstractRootOperator rootOperator) throws UndefinedParameterError {
            super(context);
            developmentContext = context;

            // taking care of variables
            Map<String, String> defaultValues = rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                    .stream()
                    .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
            for (String[] variableDefinition : rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                if (variableDefinition[0] != null && !variableDefinition[0].isBlank()) {
                    String variable = variableDefinition[0];

                    // get default first
                    String value = defaultValues.get(variable);
                    String variableTypeName = ParameterTypeTupel.transformString2Tupel(variableDefinition[1])[0];
                    // then get from dev context and possibly override
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(variableTypeName, VariableType.class);
                    String variableParameterKey = "variable_" + variable;
                    if (context.isParameterSet(variableParameterKey)) {
                        try {
                            String variableValue = context.getParameter(variableParameterKey);
                            value = switch (type) {
                                case DATE -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                case TIMESTAMP -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                default -> variableValue;
                            };

                            variableValues.put(variable, value);
                        } catch (ParseException e) {
                        }
                    }

                }
            }
        }
    }


    public void notifyBreakpointReached(AbstractPipeline process, Operator operator, List<IOObject> objects, int location) {
        setState(PipelineState.PAUSED);
        fireBreakpointEvent(getOperator(operator.getName()), objects, location);
    }
}
