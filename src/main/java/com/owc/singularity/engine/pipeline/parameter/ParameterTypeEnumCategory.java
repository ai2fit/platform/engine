package com.owc.singularity.engine.pipeline.parameter;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Iterator;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.parameters.celleditors.value.EnumCategoryValueCellEditor;

/**
 * This is a parameter type for use with enum constants. A static method exists to access the enum
 * value conveniently.
 * 
 * @author Sebastian Land
 *
 * @param <E>
 *            Enum type
 */
@ParameterTypeAnnotation(editor = EnumCategoryValueCellEditor.class)
public class ParameterTypeEnumCategory<E extends Enum<E>> extends ParameterTypeCategory {

    private static final long serialVersionUID = -6674833797362385625L;

    public ParameterTypeEnumCategory(String key, String description, Class<E> enumClass, E defaultValue) {
        super(key, description, EnumSet.allOf(enumClass).stream().map(Enum::toString).toArray(String[]::new), getIndexOf(defaultValue, enumClass));
    }

    public ParameterTypeEnumCategory(String key, String description, int defaultIndex, E... values) {
        super(key, description, Arrays.stream(values).map(Enum::toString).toArray(String[]::new), defaultIndex);
    }


    private static <E extends Enum<E>> int getIndexOf(E defaultValue, Class<E> enumClass) {
        Iterator<E> iterator = EnumSet.allOf(enumClass).iterator();
        int index = 0;
        while (!iterator.next().equals(defaultValue))
            index++;
        return index;
    }

    /**
     * This method allows to retrieve a parameter of a given {@link ParameterHandler} like an
     * {@link Operator} as enum constant.
     * 
     * @param <T>
     *            The enum type
     * @param handler
     *            the handler holding the parameter values
     * @param parameter
     *            the name of the parameter one wants to retrieve
     * @param enumClass
     *            the class of the enum
     * @return the enum value
     * @throws UndefinedParameterError
     *             thrown if the parameter does not exist in the handler
     */
    public static <T extends Enum<T>> T getParameterAsEnumValue(ParameterHandler handler, String parameter, Class<T> enumClass) throws UndefinedParameterError {
        return Enum.valueOf(enumClass, handler.getParameterAsString(parameter));
    }

    public static <T extends Enum<T>> T getParameterAsEnumValue(String parameterValue, Class<T> enumClass) {
        return Enum.valueOf(enumClass, parameterValue);
    }


}
