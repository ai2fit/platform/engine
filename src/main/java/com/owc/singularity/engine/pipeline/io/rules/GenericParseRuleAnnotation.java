package com.owc.singularity.engine.pipeline.io.rules;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This is an annotation for all {@link ParseRule}s that apply for all and any operator.
 * 
 * @author sland
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
@Inherited
public @interface GenericParseRuleAnnotation {}
