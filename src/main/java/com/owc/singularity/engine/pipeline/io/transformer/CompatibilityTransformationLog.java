package com.owc.singularity.engine.pipeline.io.transformer;

import org.apache.logging.log4j.Level;

/**
 * This is a simple logger interface with that {@link CompatibilityTransformer}s can indicate when
 * they have been applied.
 * 
 * @author Sebastian Land
 *
 */
public interface CompatibilityTransformationLog {

    /**
     * Logs the given message.
     * 
     * @param level
     * @param message
     */
    public void log(Level level, String message);
}
