package com.owc.singularity.engine.pipeline;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.operator.ExecutableRootOperator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;

/**
 * This is the most basic execution context for a pipeline. Subclasses can add further details that
 * might be necessary to their function. Sub-pipelines may specify input objects, etc.
 */
public class PipelineExecutionContext {

    public Level logLevel;

    private final Map<String, CompositionImplementation> implementationMap = new HashMap<>();

    public PipelineExecutionContext() {
        super();
    }

    public PipelineExecutionContext(DevelopmentExecutionContext context) throws UndefinedParameterError {
        if (context.isParameterSet(ExecutableRootOperator.PARAMETER_LOG_LEVEL))
            logLevel = Level.valueOf(context.getParameterAsString(ExecutableRootOperator.PARAMETER_LOG_LEVEL));
        else
            logLevel = Level.OFF;

    }

    /**
     * This method needs to return the execution unit for a given name or null if not defined
     * 
     * @param implementationName
     * @return
     * @throws UserError
     */
    public CompositionImplementation getImplementation(String implementationName) {
        return implementationMap.get(implementationName);
    }

    /**
     * This method allows to provide an implementation. It will take care that more specific
     * pipelines will override provided implementations of less specific ones.
     * 
     * @param implementationName
     * @param compositionImplementation
     */
    public void provideImplementation(String implementationName, CompositionImplementation compositionImplementation) {
        implementationMap.putIfAbsent(implementationName, compositionImplementation);
    }

}
