package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.ButtonValueCellEditor;
import com.owc.singularity.studio.gui.tools.ResourceAction;

/**
 * Parameter Type for buttons that represents a {@link ResourceAction}. This parameter provide a
 * button that is spread over the width of the parameter panel and have no label
 *
 * @author Hatem Hamad
 */
@ParameterTypeAnnotation(editor = ButtonValueCellEditor.class)
public class ParameterTypeButton extends ParameterTypeSingle {

    private static final long serialVersionUID = -7056570308460073123L;
    private final ResourceAction action;

    public ParameterTypeButton(String key, String description, ResourceAction action) {
        super(key, description);
        this.action = action;
    }

    @Override
    public String getRange() {
        return null;
    }

    @Override
    public Object getDefaultValue() {
        return null;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {

    }

    @Override
    public boolean isNumerical() {
        return false;
    }

    public ResourceAction getAction() {
        return action;
    }
}
