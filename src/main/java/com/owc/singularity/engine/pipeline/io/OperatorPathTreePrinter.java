package com.owc.singularity.engine.pipeline.io;

import java.util.*;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.OperatorPath;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.tools.TreeVisitResult;
import com.owc.singularity.engine.tools.TreeVisitor;

public class OperatorPathTreePrinter implements TreeVisitor<ExecutionUnit, Operator> {

    private final Set<ExecutionUnit> subPipelinesInPathToOperator;
    private final StringBuilder result;
    private final Operator targetOperator;
    private final int numberOfSurroundingSiblingsOnEachSide;
    private final Stack<String> prefixStack;
    private final Stack<String> childrenPrefixStack;
    private final ExecutionUnit directParentUnit;

    private boolean suppressingOutput;
    private boolean currentNodeIsInPath;

    public OperatorPathTreePrinter(OperatorPath target, int numberOfSurroundingSiblingsOnEachSide) {
        this.directParentUnit = target.getParent(0);
        this.subPipelinesInPathToOperator = new LinkedHashSet<>(target.getParents());
        this.result = new StringBuilder();
        this.targetOperator = target.getOperator();
        this.numberOfSurroundingSiblingsOnEachSide = numberOfSurroundingSiblingsOnEachSide;
        suppressingOutput = false;
        currentNodeIsInPath = false;
        prefixStack = new Stack<>();
        childrenPrefixStack = new Stack<>();
        prefixStack.add("");
        childrenPrefixStack.add("");
    }

    @Override
    public TreeVisitResult preVisitNode(ExecutionUnit executionUnit) {
        String prefix = prefixStack.peek();
        String childrenPrefix = childrenPrefixStack.peek();
        prefixStack.add(childrenPrefix + "|---");
        childrenPrefixStack.add(childrenPrefix + "|   ");
        if (isEqualTarget(executionUnit.getEnclosingOperator())) {
            prefix = prefix.replace(' ', '=').replace('-', '=').substring(0, prefix.length() - 1) + ">";
            result.append(prefix).append("+ ");
            appendOperatorInfo(executionUnit.getEnclosingOperator(), result);
            result.append('\n');
            return TreeVisitResult.SKIP_SUBTREE;
        }
        currentNodeIsInPath = subPipelinesInPathToOperator.contains(executionUnit) || executionUnit.getEnclosingOperator() instanceof AbstractRootOperator;
        if (!currentNodeIsInPath) {
            return TreeVisitResult.SKIP_SUBTREE;
        }
        suppressingOutput = false;
        result.append(prefix).append("+ ");
        if (executionUnit.getEnclosingOperator()instanceof AbstractRootOperator rootOperator) {
            AbstractPipeline pipeline = rootOperator.getPipeline();
            result.append(pipeline.getClass().getSimpleName())
                    .append('[')
                    .append(pipeline.getPath() == null ? "<unknown>" : pipeline.getPath())
                    .append(']')
                    .append(" (")
                    .append(rootOperator.getOperatorDescription().getName())
                    .append(") [")
                    .append(executionUnit.getName())
                    .append(']');
        } else {
            appendOperatorInfo(executionUnit.getEnclosingOperator(), result).append(" [").append(executionUnit.getName()).append("]:");
        }
        result.append('\n');
        if (directParentUnit.equals(executionUnit)) {
            CollectionResult collectSiblings = collectSiblings(targetOperator, numberOfSurroundingSiblingsOnEachSide);
            List<Operator> siblingsBefore = collectSiblings.siblingsBefore();
            List<Operator> siblingsAfter = collectSiblings.siblingsAfter();
            if (collectSiblings.hasRemainingOperatorsBefore()) {
                result.append(childrenPrefixStack.peek()).append("...").append('\n');
            }
            prefix = prefixStack.peek();
            // surrounding operators before target
            for (Operator operator : siblingsBefore) {
                result.append(prefix).append(" ");
                appendOperatorInfo(operator, result);
                result.append('\n');
            }
            // mark target by ===>
            result.append(prefix.replace(' ', '=').replace('-', '='), 0, prefix.length() - 1).append(">").append(" ");
            appendOperatorInfo(targetOperator, result);
            result.append('\n');

            // surrounding operators after target
            for (Operator operator : siblingsAfter) {
                result.append(prefix).append(" ");
                appendOperatorInfo(operator, result);
                result.append('\n');
            }
            if (collectSiblings.hasRemainingOperatorsAfter()) {
                result.append(childrenPrefixStack.peek()).append("...").append('\n');
            }
            return TreeVisitResult.SKIP_SIBLINGS;
        }
        return TreeVisitResult.CONTINUE;
    }

    @Override
    public TreeVisitResult visitLeaf(Operator leaf) {
        if (currentNodeIsInPath && directParentUnit != leaf.getExecutionUnit() && !suppressingOutput) {
            // the operator is within the path leading to target, but not under the
            // direct parent node
            result.append(childrenPrefixStack.peek()).append("...").append("\n");
            suppressingOutput = true;
        }
        // nothing to report
        return TreeVisitResult.CONTINUE;
    }

    @Override
    public TreeVisitResult postVisitNode(ExecutionUnit executionUnit) {
        prefixStack.pop();
        childrenPrefixStack.pop();
        if (!currentNodeIsInPath) {
            if (!suppressingOutput) {
                result.append(childrenPrefixStack.peek()).append("...").append("\n");
                suppressingOutput = true;
            }
            return TreeVisitResult.SKIP_SUBTREE;
        }
        return TreeVisitResult.CONTINUE;
    }

    private record CollectionResult(List<Operator> siblingsBefore, List<Operator> siblingsAfter, boolean hasRemainingOperatorsBefore,
            boolean hasRemainingOperatorsAfter) {
    }

    private static CollectionResult collectSiblings(Operator targetOperator, int numberOfSurroundingSiblingsOnEachSide) {
        List<Operator> siblingsBefore = new ArrayList<>(numberOfSurroundingSiblingsOnEachSide);
        List<Operator> siblingsAfter = new ArrayList<>(numberOfSurroundingSiblingsOnEachSide);
        ExecutionUnit container = targetOperator.getExecutionUnit();
        Operator operatorBefore = targetOperator;
        while (siblingsBefore.size() < numberOfSurroundingSiblingsOnEachSide) {
            Operator sourceOperator = null;
            for (InputPort inputPort : operatorBefore.getInputPorts().getAllPorts()) {
                if (inputPort.isConnected()) {
                    sourceOperator = inputPort.getSource().getPorts().getOwner().getOperator();
                    if (sourceOperator.getExecutionUnit() != container) {
                        // we are out of this unit
                        sourceOperator = null;
                    }
                    break;
                }
            }
            if (sourceOperator == null) {
                // no connections before
                operatorBefore = null;
                break;
            }
            siblingsBefore.add(0, sourceOperator);
            operatorBefore = sourceOperator;
        }

        Operator operatorAfter = targetOperator;
        while (siblingsAfter.size() < numberOfSurroundingSiblingsOnEachSide) {
            Operator destinationOperator = null;
            for (OutputPort outputPort : operatorAfter.getOutputPorts().getAllPorts()) {
                if (outputPort.isConnected()) {
                    destinationOperator = outputPort.getDestination().getPorts().getOwner().getOperator();
                    if (destinationOperator.getExecutionUnit() != container) {
                        // we are out of this unit
                        destinationOperator = null;
                    }
                    break;
                }
            }
            if (destinationOperator == null) {
                // no connections after
                operatorAfter = null;
                break;
            }
            siblingsAfter.add(destinationOperator);
            operatorAfter = destinationOperator;
        }
        return new CollectionResult(siblingsBefore, siblingsAfter, operatorBefore != null, operatorAfter != null);
    }

    public String getResult() {
        return result.toString().strip();
    }

    private boolean isEqualTarget(Operator operator) {
        return operator == targetOperator || operator.getName().equals(targetOperator.getName());
    }

    private static StringBuilder appendOperatorInfo(Operator operator, StringBuilder builder) {
        return builder.append(operator.getName())
                .append('[')
                .append(operator.getApplyCount())
                .append(']')
                .append(" (")
                .append(operator.getOperatorDescription().getName())
                .append(")");
    }
}
