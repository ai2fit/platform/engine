package com.owc.singularity.engine.pipeline;


import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.SubPipelineRootOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.wizards.AbstractConfigurationWizardCreator;
import com.owc.singularity.studio.gui.wizards.ConfigurationListener;

/**
 * A pipeline being used as part of another pipeline. It is executed in the background and hidden in
 * the sense that it transparently executes the operators as if being in the parent process
 * 
 * 
 * @author Sebastian Land
 *
 */
public class SubPipeline extends AbstractPipeline {

    public static final String PARAMETER_VARIABLES = "variables";
    public static final String PARAMETER_INPUTS = "input_objects";

    private transient AbstractPipeline parentPipeline = null;
    private transient RepositoryPath inheritedPipelinePath;
    private transient SubPipelineExecutionContext debugContext = null;

    public SubPipeline() {
        super(new SubPipelineRootOperator());
    }

    public SubPipeline(AbstractRootOperator rootOperator) {
        super(rootOperator);
        if (!(rootOperator instanceof SubPipelineRootOperator))
            throw new IllegalArgumentException("Only sub-pipeline root operator permitted in sub-piplines.");
    }

    private SubPipeline(SubPipeline analysisPipeline) {
        super(analysisPipeline);
    }

    @Override
    public AbstractPipeline clone() {
        return new SubPipeline(this);
    }

    @Override
    public boolean shouldStop() {
        if (parentPipeline != null && !isDebug())
            return super.shouldStop() || parentPipeline.shouldStop();
        return super.shouldStop();
    }

    @Override
    public boolean shouldPause() {
        if (parentPipeline != null && !isDebug())
            return parentPipeline.shouldPause();
        return super.shouldPause();
    }


    @Override
    public void pause(Operator operator, List<IOObject> iocontainer, int breakpointType) {
        if (parentPipeline == null)
            super.pause(operator, iocontainer, breakpointType);

        // otherwise ignore any breakpoints during execution
    }

    @Override
    public boolean hasSaveDestination() {
        if (parentPipeline != null && inheritedPipelinePath != null)
            return true;
        return super.hasSaveDestination();
    }

    public boolean isDebug() {
        return debugContext != null;
    }

    public void setDebugContext(SubPipelineExecutionContext context) {
        debugContext = context;
        getDevelopmentContext().refreshParameterTypes();
    }

    public void clearDebugContext() {
        this.debugContext = null;
        getDevelopmentContext().refreshParameterTypes();
    }

    @Override
    public RepositoryPath getPath() {
        if (parentPipeline != null && inheritedPipelinePath != null)
            return inheritedPipelinePath;
        return super.getPath();
    }

    public AbstractPipeline getParentPipeline() {
        return parentPipeline;
    }

    @Override
    protected List<IOObject> execute(PipelineExecutionContext context, boolean preserveEmptyResults) throws OperatorException {
        SubPipelineExecutionContext myContext = (SubPipelineExecutionContext) context;
        this.parentPipeline = myContext.parentPipeline;
        this.inheritedPipelinePath = myContext.inheritedPipelinePath;
        return super.execute(context, preserveEmptyResults);
    }


    @Override
    public PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) throws OperatorException {
        if (debugContext != null) {
            return debugContext;
        }
        return new SubPipelineExecutionContext(context, getRootOperator());
    }

    @Override
    public void setRootOperator(AbstractRootOperator root) {
        super.setRootOperator(root);
        root.getParameters().addObserver(new com.owc.singularity.engine.tools.Observer<String>() {

            @Override
            public void update(com.owc.singularity.engine.tools.Observable<String> observable, String arg) {
                getDevelopmentContext().refreshParameterTypes();
            }

        }, false);
        getDevelopmentContext().refreshParameterTypes();
    }

    @Override
    public List<ParameterType> getExecutionContextParameterTypes() {
        LinkedList<ParameterType> types = new LinkedList<>();

        if (getRootOperator() != null) {
            if (isDebug()) {
                ParameterType type = new ParameterTypeConfiguration(SubPipeline.RemoveDebugStateWizard.class, getRootOperator());
                type.setKey("clear_debug_state");
                type.setDescription("This will remove the stored debug state and the development context will become active again.");
                types.add(type);

            } else {
                try {
                    // types for input objects
                    for (String[] nameType : getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_INPUT_DEFINITIONS)) {
                        if (nameType[0] != null && !nameType[0].isBlank()) {
                            String name = nameType[0];
                            String[] typeAndDescription = ParameterTypeTupel.transformString2Tupel(nameType[1]);
                            if (typeAndDescription[1] == null || typeAndDescription[1].isBlank())
                                typeAndDescription[1] = "Select repository location to use as input object during development.";
                            types.add(new ParameterTypeRepositoryLocation("input_" + name, typeAndDescription[1], true, false, true, false, true, false, true));
                        }
                    }

                    // types for variables
                    Map<String, String> defaultVariableValue = getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                            .stream()
                            .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
                    for (String[] variableTupel : getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                        if (variableTupel[0] != null && !variableTupel[0].isBlank()) {
                            String variable = variableTupel[0];
                            String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableTupel[1]);
                            VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                            String defaultValue = defaultVariableValue.get(variable);
                            boolean isOptional = defaultValue != null && !defaultValue.isEmpty();
                            ParameterType parameterType = switch (type) {
                                case DATE -> (new ParameterTypeDate("variable_" + variable, typeDefaultDescription[2], isOptional));
                                case FILE_PATH -> new ParameterTypeFile("variable_" + variable, typeDefaultDescription[2], null, isOptional);
                                case INTEGER -> new ParameterTypeInt("variable_" + variable, typeDefaultDescription[2], Integer.MIN_VALUE, Integer.MAX_VALUE,
                                        isOptional);
                                case REAL -> new ParameterTypeDouble("variable_" + variable, typeDefaultDescription[2], Double.NEGATIVE_INFINITY,
                                        Double.POSITIVE_INFINITY, isOptional);
                                case REPOSITORY_PATH -> new ParameterTypeRepositoryLocation("variable_" + variable, typeDefaultDescription[2], isOptional);
                                case STRING -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                                case TIMESTAMP -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                            };
                            if (isOptional) {
                                parameterType.setDefaultValueAsRawString(defaultValue);
                            }
                            types.add(parameterType);
                        }

                    }
                } catch (UndefinedParameterError e) {
                }
            }
        }

        types.addAll(super.getExecutionContextParameterTypes());
        return types;
    }


    public static class SubPipelineExecutionContext extends PipelineExecutionContext {

        public AbstractPipeline parentPipeline = null;
        public List<IOObject> inputs = Collections.emptyList();
        public final Map<String, String> variableValues = new HashMap<>();
        public RepositoryPath inheritedPipelinePath = null;
        public boolean isDebug = false;

        /**
         * This can be used to create an empty context. You can set variables and inputs by
         * accessing {@link #variableValues} and {@link #inputs} directly. Defaults for variables
         * will be taken from their definition when executed, but this values here will override the
         * defaults. Variables will be evaluated against types as defined in root operator when
         * executed.
         */
        public SubPipelineExecutionContext() {
            super();
        }

        /**
         * This can be used to create a context directly with the given inputs and variable
         * settings. Defaults for variables will be taken from their definition when executed, but
         * this values here will override the defaults. Variables will be evaluated against types as
         * defined in root operator when executed.
         * 
         * @param inputs
         *            list of input objects in order of ports. Must not be null but can be empty
         *            list.
         * @param variableValues
         *            map of variable values. Must not be null but may be empty.
         */
        public SubPipelineExecutionContext(List<IOObject> inputs, Map<String, String> variableValues) {
            super();
            this.inputs = inputs;
            this.variableValues.putAll(variableValues);
        }

        public SubPipelineExecutionContext(DevelopmentExecutionContext context, AbstractRootOperator rootOperator) throws OperatorException {
            super(context);
            // taking care of variables
            Map<String, String> defaultValues = rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                    .stream()
                    .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
            for (String[] variableDefinition : rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                if (variableDefinition[0] != null && !variableDefinition[0].isBlank()) {
                    String variable = variableDefinition[0];
                    // get default first
                    String value = defaultValues.get(variable);

                    // then get from dev context and possibly override
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(ParameterTypeTupel.transformString2Tupel(variableDefinition[1])[0],
                            VariableType.class);
                    String variableParameterKey = "variable_" + variable;
                    if (context.isParameterSet(variableParameterKey)) {
                        try {
                            String variableValue = context.getParameter(variableParameterKey);
                            value = switch (type) {
                                case DATE -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                case TIMESTAMP -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                default -> variableValue;
                            };

                            variableValues.put(variable, value);
                        } catch (ParseException e) {
                        }
                    }

                }
            }

            // taking care of input definitions
            inputs = new LinkedList<>();
            for (String[] nameType : rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_INPUT_DEFINITIONS)) {
                String name = nameType[0];
                String repositoryPathString = context.getParameterAsString("input_" + name);
                if (repositoryPathString != null && !repositoryPathString.isBlank()) {
                    RepositoryPath path = ParameterTypeRepositoryLocation.getPath(repositoryPathString, rootOperator);
                    try {
                        if (path != null && path.isAbsolute())
                            inputs.add(Entries.loadData(path, IOObject.class, ModuleService.getMajorClassLoader()));
                    } catch (IOException e) {
                        throw new OperatorException("Cannot load entry " + path + " from repository as specified in development execution context");
                    }
                }
            }
        }
    }

    public static class RemoveDebugStateWizard extends AbstractConfigurationWizardCreator {

        private static final long serialVersionUID = 974194343734042471L;

        @Override
        public String getI18NKey() {
            return "subpipeline.clear_debug_context";
        }

        @Override
        public void createConfigurationWizard(ParameterType type, ConfigurationListener listener) {
            ((SubPipeline) ((Operator) listener).getPipeline()).clearDebugContext();
        }

    }
}
