/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter.conditions;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.PortProvider;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.Port;


/**
 * This condition checks whether a {@link InputPort} of a {@link Operator} is connected or not. The
 * condition is fulfilled if the {@link InputPort} is NOT connected.
 * 
 * @author Nils Woehler
 * 
 */
public class PortConnectedCondition extends ParameterCondition {

    private final PortProvider portProvider;
    private final boolean connected;

    public PortConnectedCondition(ParameterHandler handler, PortProvider portProvider, boolean becomeMandatory, boolean connected) {
        super(handler, becomeMandatory);
        this.portProvider = portProvider;
        this.connected = connected;
    }

    @Override
    public boolean isConditionFullfilled() {
        Port port = portProvider.getPort();
        if (port == null)
            return !connected;
        return port.isConnected() == connected;
    }

}
