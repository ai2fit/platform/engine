package com.owc.singularity.engine.pipeline.io.transformer;

import java.util.*;

import javax.xml.xpath.*;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

/**
 * An {@link CompatibilityTransformer}, which removes the specified operator from the pipeline.
 * However, port connections between the removed operator ports and other ports are restored after
 * the best effort approach.
 */
public abstract class OperatorRemovalTransformer implements CompatibilityTransformer {

    protected static final XPathFactory xpathfactory = XPathFactory.newInstance();

    private Map<String, String> portReplacementMap;
    private XPathExpression operatorExpr;

    protected XPath xpath;


    public OperatorRemovalTransformer(String formerOperatorKey, Map<String, String> portReplacementMap) throws Exception {
        this.portReplacementMap = portReplacementMap;
        xpath = xpathfactory.newXPath();
        operatorExpr = xpath.compile("//operator[@class='" + formerOperatorKey + "']");
    }

    @Override
    public void apply(Document document, CompatibilityTransformationLog log) {
        try {
            NodeList operatorNodes = (NodeList) operatorExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < operatorNodes.getLength(); i++) {
                Node operatorNode = operatorNodes.item(i);
                String operatorName = operatorNode.getAttributes().getNamedItem("name").getNodeValue();

                // bridge port connections
                if (portReplacementMap != null && !portReplacementMap.isEmpty()) {
                    try {
                        XPathExpression inputPortExpr = xpath.compile("//connect[@to_op='" + operatorName + "']");
                        NodeList inputPortNodes = (NodeList) inputPortExpr.evaluate(document, XPathConstants.NODESET);
                        XPathExpression outputPortExpr = xpath.compile("//connect[@from_op='" + operatorName + "']");
                        NodeList outputPortNodes = (NodeList) outputPortExpr.evaluate(document, XPathConstants.NODESET);
                        for (int j = 0; j < inputPortNodes.getLength(); j++) {
                            NamedNodeMap inputConnectionNode = inputPortNodes.item(j).getAttributes();
                            String inputPortName = inputConnectionNode.getNamedItem("to_port").getNodeValue();
                            if (portReplacementMap.containsKey(inputPortName)) {
                                String desiredMatch = portReplacementMap.get(inputPortName);
                                for (int p = 0; p < outputPortNodes.getLength(); p++) {
                                    NamedNodeMap outputConnectionNode = outputPortNodes.item(p).getAttributes();
                                    String outputPortName = outputConnectionNode.getNamedItem("from_port").getNodeValue();
                                    if (Objects.equals(desiredMatch, outputPortName)) {
                                        Node toOperatorAttribute = inputConnectionNode.getNamedItem("to_op");
                                        if (outputConnectionNode.getNamedItem("to_op") != null)
                                            toOperatorAttribute.setNodeValue(outputConnectionNode.getNamedItem("to_op").getNodeValue());
                                        else
                                            inputConnectionNode.removeNamedItem("to_op");
                                        Node toPortAttribute = inputConnectionNode.getNamedItem("to_port");
                                        toPortAttribute.setNodeValue(outputConnectionNode.getNamedItem("to_port").getNodeValue());
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.log(Level.ERROR, "Operator deletion failed: " + e.getMessage());
                    }
                }
                operatorNode.getParentNode().removeChild(operatorNode);
                log.log(Level.INFO, "Operator '" + operatorName + "' deleted.'");
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Operator deletion failed: " + e.getMessage());
        }
    }
}
