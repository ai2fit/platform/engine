package com.owc.singularity.engine.pipeline;

import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.AnalysisRootOperator;
import com.owc.singularity.engine.operator.SubPipelineRootOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.VariableHandler.VariableType;
import com.owc.singularity.engine.pipeline.parameter.*;

public class AnalysisPipeline extends AbstractPipeline {

    public AnalysisPipeline() {
        super(new AnalysisRootOperator());
    }

    protected AnalysisPipeline(AnalysisPipeline analysisPipeline) {
        super(analysisPipeline);
    }

    public AnalysisPipeline(AbstractRootOperator rootOperator) {
        super(rootOperator);
        if (!(rootOperator instanceof AnalysisRootOperator))
            throw new IllegalArgumentException("Only analysis root operator permitted in analysis pipelines.");
    }

    @Override
    public AbstractPipeline clone() {
        return new AnalysisPipeline(this);
    }

    @Override
    public PipelineExecutionContext derivePipelineExecutionContext(DevelopmentExecutionContext context) throws OperatorException {
        AnalysisPipelineExecutionContext executionContext = new AnalysisPipelineExecutionContext(context, getRootOperator());
        return executionContext;
    }

    @Override
    public void setRootOperator(AbstractRootOperator root) {
        super.setRootOperator(root);
        root.getParameters().addObserver(new com.owc.singularity.engine.tools.Observer<String>() {

            @Override
            public void update(com.owc.singularity.engine.tools.Observable<String> observable, String arg) {
                getDevelopmentContext().refreshParameterTypes();
            }

        }, false);
        getDevelopmentContext().refreshParameterTypes();
    }

    @Override
    public List<ParameterType> getExecutionContextParameterTypes() {
        LinkedList<ParameterType> types = new LinkedList<>();

        if (getRootOperator() != null) {
            try {
                // types for variables
                Map<String, String> defaultVariableValue = getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                        .stream()
                        .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
                for (String[] variableTupel : getRootOperator().getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                    if (variableTupel[0] != null && !variableTupel[0].isBlank()) {
                        String variable = variableTupel[0];
                        String[] typeDefaultDescription = ParameterTypeTupel.transformString2Tupel(variableTupel[1]);
                        VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(typeDefaultDescription[0], VariableType.class);
                        String defaultValue = defaultVariableValue.get(variable);
                        boolean isOptional = defaultValue != null && !defaultValue.isBlank();
                        ParameterType parameterType = switch (type) {
                            case DATE -> (new ParameterTypeDate("variable_" + variable, typeDefaultDescription[2], isOptional));
                            case FILE_PATH -> new ParameterTypeFile("variable_" + variable, typeDefaultDescription[2], null, isOptional);
                            case INTEGER -> new ParameterTypeInt("variable_" + variable, typeDefaultDescription[2], Integer.MIN_VALUE, Integer.MAX_VALUE,
                                    isOptional);
                            case REAL -> new ParameterTypeDouble("variable_" + variable, typeDefaultDescription[2], Double.NEGATIVE_INFINITY,
                                    Double.POSITIVE_INFINITY, isOptional);
                            case REPOSITORY_PATH -> new ParameterTypeRepositoryLocation("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case STRING -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                            case TIMESTAMP -> new ParameterTypeString("variable_" + variable, typeDefaultDescription[2], isOptional);
                        };
                        if (isOptional) {
                            parameterType.setDefaultValueAsRawString(defaultValue);
                        }
                        types.add(parameterType);
                    }

                }
            } catch (UndefinedParameterError e) {
            }
        }

        types.addAll(super.getExecutionContextParameterTypes());
        return types;
    }

    public static class AnalysisPipelineExecutionContext extends PipelineExecutionContext {

        public final Map<String, String> variableValues = new HashMap<>();

        public AnalysisPipelineExecutionContext() {
            super();
        }

        /**
         * Constructor for executing the pipeline without development context.
         * 
         * @param variables
         *            variables overriding defaults in pipeline. May not be null but may be empty.
         */
        public AnalysisPipelineExecutionContext(Map<String, String> variables) {
            super();
            variableValues.putAll(variables);
        }

        public AnalysisPipelineExecutionContext(DevelopmentExecutionContext context, AbstractRootOperator rootOperator) throws OperatorException {
            super(context);
            // taking care of variables
            Map<String, String> defaultValues = rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)
                    .stream()
                    .collect(Collectors.toMap(s -> s[0], s -> ParameterTypeTupel.transformString2Tupel(s[1])[1]));
            for (String[] variableDefinition : rootOperator.getParameterList(SubPipelineRootOperator.PARAMETER_VARIABLE_DEFINITIONS)) {
                if (variableDefinition[0] != null && !variableDefinition[0].isBlank()) {
                    String variable = variableDefinition[0];

                    // get default first
                    String value = defaultValues.get(variable);
                    String variableTypeName = ParameterTypeTupel.transformString2Tupel(variableDefinition[1])[0];
                    // then get from dev context and possibly override
                    VariableType type = ParameterTypeEnumCategory.getParameterAsEnumValue(variableTypeName, VariableType.class);
                    String variableParameterKey = "variable_" + variable;
                    if (context.isParameterSet(variableParameterKey)) {
                        try {
                            String variableValue = context.getParameter(variableParameterKey);
                            value = switch (type) {
                                case DATE -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                case TIMESTAMP -> VariableHandler.dateFormatter.format(ParameterTypeDate.DATE_FORMAT.get().parse(variableValue).toInstant());
                                default -> variableValue;
                            };

                            variableValues.put(variable, value);
                        } catch (ParseException e) {
                        }
                    }

                }
            }
        }
    }
}
