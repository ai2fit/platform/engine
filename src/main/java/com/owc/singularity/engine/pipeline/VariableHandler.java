/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.pipeline;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.pipeline.io.transformer.VariableReplacementTransformer;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.pipeline.parameter.UndefinedVariableError;
import com.owc.singularity.repository.RepositoryPath;


/**
 * This class can be used to store variables for a process. It also defines some standard variables
 * like the process path or file name.
 * 
 * @implNote (extensions-breaker) <a href=
 *           "https://gitlab.oldworldcomputing.com/singularity/engine/-/wikis/Extensions-Breaker">read
 *           more</a>
 * @author Ingo Mierswa
 */
@SuppressWarnings("deprecation")
public class VariableHandler extends Observable {

    public static DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z");
    public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd Z");

    public static enum VariableType {


        INTEGER, REAL, STRING, TIMESTAMP, DATE, REPOSITORY_PATH, FILE_PATH;

        public boolean isCompatible(String value) {
            try {
                switch (this) {
                    case FILE_PATH:
                        new File(value);
                        return true;
                    case INTEGER:
                        Integer.parseInt(value);
                        return true;
                    case REAL:
                        Double.parseDouble(value);
                        return true;
                    case REPOSITORY_PATH:
                        RepositoryPath.of(value);
                        return true;
                    case STRING:
                        return true;
                    case TIMESTAMP:
                        LocalDate.parse(value, timestampFormatter);
                        return true;
                    case DATE:
                        LocalDate.parse(value, dateFormatter);
                        return true;
                    default:
                        return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }

    public static final String PIPELINE_NAME = "pipelineName";
    public static final String PIPELINE_FILE = "pipelineFile";
    public static final String PIPELINE_PATH = "pipelinePath";
    public static final String PIPELINE_START = "pipelineStart";
    public static final String EXECUTION_COUNT = "executionCount";

    /**
     * indicates the start of a variable
     */
    public static final String VARIABLE_STRING_START = "%{";
    /**
     * indicates the end of a variable
     */
    public static final String VARIABLE_STRING_END = "}";
    /**
     * indicates the start of a string expansion parameter
     */
    public static final String STRING_EXPANSION_VARIABLE_PARAMETER_START = "[";
    /**
     * indicates the end of a string expansion parameter
     */
    public static final String STRING_EXPANSION_VARIABLE_PARAMETER_END = "]";
    /**
     * string expansion key which will be replaced with %.
     */
    public static final String STRING_EXPANSION_VARIABLE_PERCENT_SIGN = "%";

    private static final String[] MOST_USED_PREDEFINED_VARIABLES = { PIPELINE_NAME, PIPELINE_FILE, PIPELINE_PATH, EXECUTION_COUNT };

    /**
     * string expansion key which will be replaced with the current system date and time
     */
    public static final String STRING_EXPANSION_VARIABLE_TIME = "currentTimestamp";
    /** all predefined variables that do not depend on an operator */
    private static final Set<String> PREDEFINED_OPERATOR_INDEPENDENT_VARIABLES = new HashSet<>(
            Arrays.asList(PIPELINE_NAME, PIPELINE_FILE, PIPELINE_PATH, PIPELINE_START, STRING_EXPANSION_VARIABLE_TIME));

    /**
     * string expansion key which will be replaced with the name of the operator.
     */
    public static final String STRING_EXPANSION_VARIABLE_OPERATOR_NAME = "operatorName";
    /** all predefined variables that depend on an operator */
    private static final Set<String> PREDEFINED_OPERATOR_DEPENDENT_VARIABLES = new HashSet<>(
            Arrays.asList(EXECUTION_COUNT, STRING_EXPANSION_VARIABLE_OPERATOR_NAME));

    private static final String[] ALL_USER_FRIENDLY_PREDEFINED_VARIABLES = { PIPELINE_NAME, PIPELINE_FILE, PIPELINE_PATH, PIPELINE_START, EXECUTION_COUNT,
            STRING_EXPANSION_VARIABLE_OPERATOR_NAME };

    /**
     * Remaining problem is that predefined variables that are overridden by custom variables are
     * evaluated first. The result is the predefined value.
     */
    private static final String[] ALL_PREDEFINED_VARIABLES = { PIPELINE_NAME, PIPELINE_FILE, PIPELINE_PATH, PIPELINE_START, EXECUTION_COUNT,
            STRING_EXPANSION_VARIABLE_OPERATOR_NAME, STRING_EXPANSION_VARIABLE_TIME };

    /**
     * This HashSet contains the keys of legacy variables which will be replaced while string
     * expansion. CAUTION: Do NOT add any new content to this set.
     */
    private static final HashSet<String> LEGACY_STRING_EXPANSION_VARIABLE_KEYS = new HashSet<>();

    static {
        LEGACY_STRING_EXPANSION_VARIABLE_KEYS.add(STRING_EXPANSION_VARIABLE_OPERATOR_NAME);
        LEGACY_STRING_EXPANSION_VARIABLE_KEYS.add(EXECUTION_COUNT);
        LEGACY_STRING_EXPANSION_VARIABLE_KEYS.add(STRING_EXPANSION_VARIABLE_TIME);
        LEGACY_STRING_EXPANSION_VARIABLE_KEYS.add(STRING_EXPANSION_VARIABLE_PERCENT_SIGN);
    }

    // ThreadLocal because DateFormat is NOT threadsafe and creating a new DateFormat is
    // EXTREMELY expensive
    /**
     * Used for formatting the %{process_start} and current time %{t} variable
     */
    public static final ThreadLocal<DateFormat> DATE_FORMAT = ThreadLocal.withInitial(() -> {
        // clone because getDateInstance uses an internal pool which can return the same
        // instance for multiple threads
        return new SimpleDateFormat("yyyy_MM_dd-a_KK_mm_ss");
    });

    /**
     * This HashSet contains the keys of variables which will be replaced while string expansion.
     * Each variable item might have an arbitrary length.
     */
    private static final HashSet<String> STRING_EXPANSION_VARIABLE_KEYS = new HashSet<>();

    static {
        STRING_EXPANSION_VARIABLE_KEYS.add(EXECUTION_COUNT);
    }

    private final AbstractPipeline process;

    private final Map<String, String> variableMap = new HashMap<>();

    private final Object LOCK = new Object();

    public VariableHandler(AbstractPipeline process) {
        this.process = process;
    }

    public void clear() {
        setChanged();
        synchronized (LOCK) {
            variableMap.clear();
        }
        notifyObservers(this);
    }

    public Iterator<String> getDefinedVariableNames() {
        synchronized (LOCK) {
            return new HashMap<>(variableMap).keySet().iterator();
        }
    }

    /**
     * @return an array with the names of all user-friendly predefined variables available in
     *         SingularityEngine
     */
    public String[] getAllGraphicallySupportedPredefinedVariables() {
        return ALL_USER_FRIENDLY_PREDEFINED_VARIABLES;
    }

    /**
     * @return an array with the names of ALL predefined variables available in SingularityEngine
     */
    public String[] getAllPredefinedVariables() {
        return ALL_PREDEFINED_VARIABLES;
    }

    public String[] getMostUsedPredefinedVariables() {
        return MOST_USED_PREDEFINED_VARIABLES;
    }

    /**
     * Adds a variable to this VariableHandler. If a variable with this name is already present, it
     * will be overwritten.
     *
     * @param variable
     *            The name of the variable.
     * @param value
     *            The new value of the variable.
     */
    public void addVariable(String variable, String value) {
        if (variable != null && !variable.isEmpty()) {
            setChanged();
            synchronized (LOCK) {
                variableMap.put(variable, value);
            }
            notifyObservers(this);
        }
    }

    public void removeVariable(String variable) {
        setChanged();
        synchronized (LOCK) {
            variableMap.remove(variable);
        }
        notifyObservers(this);
    }

    /**
     * Checks whether a provided variable was set.
     *
     * @param variable
     *            the variable key
     * @param operator
     *            the operator that can be used to resolve the variable
     * @return <code>true</code> in case it was set, <code>false</code> otherwise
     */
    public boolean isVariableSet(String variable, Operator operator) {
        synchronized (LOCK) {
            if (variableMap.containsKey(variable) || PREDEFINED_OPERATOR_INDEPENDENT_VARIABLES.contains(variable)) {
                return true;
            }
        }
        return operator != null && PREDEFINED_OPERATOR_DEPENDENT_VARIABLES.contains(variable);

    }

    /**
     * Resolves the variables {@value #PIPELINE_NAME}, {@value #PIPELINE_FILE},
     * {@value #PIPELINE_PATH}, {@value #PIPELINE_START} and user defined variables.
     */
    public String getVariable(String variable) {
        if (PREDEFINED_OPERATOR_INDEPENDENT_VARIABLES.contains(variable)) {
            return switch (variable) {
                case PIPELINE_NAME, PIPELINE_FILE -> process.getPath() != null ? process.getPath().getFileName().toString() : "";
                case PIPELINE_PATH -> process.getPath() != null ? process.getPath().toString() : "";
                case PIPELINE_START -> variableMap.containsKey(variable) ? variableMap.get(variable)
                        : DATE_FORMAT.get().format(new Date(process.getRootOperator().getStartTime()));
                case STRING_EXPANSION_VARIABLE_TIME -> DATE_FORMAT.get().format(new Date());
                default -> null;
            };
        }
        return this.variableMap.get(variable);
    }

    /**
     * Resolves the variable.
     *
     * <p>
     * Resolves following predefined variables:
     * </p>
     * <ul>
     * <li><b>{@value #PIPELINE_NAME}</b> with the name of the process</li>
     * <li><b>{@value #PIPELINE_FILE}</b> with the file name of the process</li>
     * <li><b>{@value #PIPELINE_PATH}</b> with the path to the process</li>
     * <li><b>t</b> with the current system date and time</li>
     * </ul>
     * <p>
     * Resolves following predefined variables if operator is non-null:
     * </p>
     * <ul>
     * <li><b>n</b> or <b>operatorName</b> with the name of this operator</li>
     * <li><b>c</b> with the class of this operator</li>
     * <li><b>a</b> or <b>execution_count</b> with the number of times the operator was applied</li>
     * <li><b>b</b> with the number of times the operator was applied plus one</li>
     * </ul>
     * <p>
     * Resolves user defined variables.
     * </p>
     *
     * @param variable
     *            the variable to resolve
     * @param operator
     *            the operator to use for resolving, may be {@code null}
     * @return the variable value
     */
    public String getVariable(String variable, Operator operator) {
        if (operator != null) {
            String value = resolveUnshiftedOperatorVariables(variable, operator);
            if (value != null) {
                return value;
            }
        }
        return getVariable(variable);
    }

    @Override
    public String toString() {
        return this.variableMap.toString();
    }

    /**
     * This method replaces all Variables in a given String through their real values and returns a
     * String with replaced Variables. If the CompatibilityLevel of the RootOperator is lower than
     * 6.0.3, undefined variables will be ignored.
     *
     * @param parameterValue
     *            the whole ParameterType value String
     * @return the complete parameter value with replaced Variables
     * @throws UndefinedVariableError
     *             this error will be thrown if the CompatibilityLevel of the RootOperator is at
     *             least 6.0.3 and a variable is undefined
     */
    public String resolveVariables(String parameterKey, String parameterValue) throws UndefinedVariableError {
        int startIndex = parameterValue.indexOf(VARIABLE_STRING_START);
        if (startIndex == -1) {
            return parameterValue;
        }
        StringBuilder result = new StringBuilder();
        while (startIndex >= 0) {
            result.append(parameterValue, 0, startIndex);
            int endIndex = parameterValue.indexOf(VARIABLE_STRING_END, startIndex + 2);
            if (endIndex == -1) {
                return parameterValue;
            }
            String variableString = parameterValue.substring(startIndex + 2, endIndex);
            // check whether variableString is a predefined variable which will be resolved at
            // String expansion
            if (STRING_EXPANSION_VARIABLE_KEYS.contains(variableString)
                    || LEGACY_STRING_EXPANSION_VARIABLE_KEYS.contains(variableString.length() > 1 ? variableString.substring(0, 2) : variableString)) {
                // skip variable because it will be replaced during the string expansion
                result.append(VARIABLE_STRING_START).append(variableString).append(VARIABLE_STRING_END);
            } else {
                // resolve variable
                String variableValue = this.getVariable(variableString);
                if (variableValue != null) {
                    result.append(variableValue);
                } else {
                    throw new UndefinedVariableError(parameterKey, variableString);
                }
            }
            parameterValue = parameterValue.substring(endIndex + 1);
            startIndex = parameterValue.indexOf(VARIABLE_STRING_START);
        }
        result.append(parameterValue);
        return result.toString();
    }

    /**
     * <p>
     * Replaces following predefined variables:
     * </p>
     * <ul>
     * <li><b>%{n}</b> or <b>%{operatorName}</b> with the name of this operator</li>
     * <li><b>%{c}</b> with the class of this operator</li>
     * <li><b>%{t}</b> with the current system date and time
     * <li><b>%{a}</b> or <b>%{execution_count}</b> with the number of times the operator was
     * applied</li>
     * <li><b>%{b}</b> with the number of times the operator was applied plus one (a shortcut for
     * %{p[1]})</li>
     * <li><b>%{p[number]}</b> with the number of times the operator was applied plus number</li>
     * <li><b>%{v[OperatorName.ValueName]}</b> with the value &quot;ValueName&quot; of the operator
     * &quot;OperatorName&quot;</li>
     * <li><b>%{%}</b> with %</li>
     * </ul>
     *
     * @return The String with resolved predefined variables. Returns {@code null} in case provided
     *         parameter str is {@code null}.
     */
    public String resolvePredefinedVariables(String str, Operator operator) throws UndefinedParameterError {
        if (str == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        int totalStart = 0;
        int start;
        while ((start = str.indexOf(VARIABLE_STRING_START, totalStart)) >= 0) {
            result.append(str, totalStart, start);
            int end = str.indexOf(VARIABLE_STRING_END, start);
            if (end == -1) {
                return str;
            }
            if (end >= start) {
                String command = str.substring(start + 2, end);
                String unshiftedOperatorVariableResult = resolveUnshiftedOperatorVariables(command, operator);
                if (unshiftedOperatorVariableResult != null) {
                    result.append(unshiftedOperatorVariableResult);
                } else if (STRING_EXPANSION_VARIABLE_TIME.equals(command)) {
                    result.append(DATE_FORMAT.get().format(new Date()));
                } else if (STRING_EXPANSION_VARIABLE_PERCENT_SIGN.equals(command)) {
                    result.append('%');
                } else {
                    result.append(command);
                }
            } else {
                end = start + 2;
                result.append(VARIABLE_STRING_START);
            }
            totalStart = end + 1;
        }
        result.append(str.substring(totalStart));
        return result.toString();
    }

    /**
     * <p>
     * Resolves following predefined variables:
     * </p>
     * <ul>
     * <li><b>operatorName</b> with the name of this operator</li>
     * <li><b>execution_count</b> with the number of times the operator was applied</li>
     * </ul>
     *
     */
    private String resolveUnshiftedOperatorVariables(String command, Operator operator) {
        if (STRING_EXPANSION_VARIABLE_OPERATOR_NAME.equals(command)) {
            return operator.getName();
        } else if (EXECUTION_COUNT.equals(command)) {
            return String.valueOf(operator.getApplyCount());
        } else {
            return null;
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableProcessNameTransformer extends VariableReplacementTransformer {

        public VariableProcessNameTransformer() throws Exception {
            super("process_name", PIPELINE_NAME);
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableProcessFileTransformer extends VariableReplacementTransformer {

        public VariableProcessFileTransformer() throws Exception {
            super("process_file", PIPELINE_FILE);
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableProcessPathTransformer extends VariableReplacementTransformer {

        public VariableProcessPathTransformer() throws Exception {
            super("process_path", PIPELINE_PATH);
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableProcessStartTransformer extends VariableReplacementTransformer {

        public VariableProcessStartTransformer() throws Exception {
            super("process_start", PIPELINE_START);
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableExecutionCountTransformer extends VariableReplacementTransformer {

        public VariableExecutionCountTransformer() throws Exception {
            super("execution_count", EXECUTION_COUNT);
        }
    }

    @CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT)
    public static class VariableOperatorNameTransformer extends VariableReplacementTransformer {

        public VariableOperatorNameTransformer() throws Exception {
            super("operator_name", STRING_EXPANSION_VARIABLE_OPERATOR_NAME);
        }
    }

    // TODO Enable these importers again, after VariableReplacementTransformer is more sophisticated
    /*
     * @CompatibilityTransformerAnnotation(priority =
     * CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT) public static class
     * VariableTNameTransformer extends VariableReplacementTransformer {
     * 
     * public VariableTNameTransformer() throws Exception { super("t",
     * STRING_EXPANSION_VARIABLE_TIME); } }
     * 
     * @CompatibilityTransformerAnnotation(priority =
     * CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT) public static class
     * VariableANameTransformer extends VariableReplacementTransformer {
     * 
     * public VariableANameTransformer() throws Exception { super("a", EXECUTION_COUNT); } }
     * 
     * @CompatibilityTransformerAnnotation(priority =
     * CompatibilityTransformerAnnotation.OPERATOR_REPLACEMENT) public static class
     * VariableNNameTransformer extends VariableReplacementTransformer {
     * 
     * public VariableNNameTransformer() throws Exception { super("n",
     * STRING_EXPANSION_VARIABLE_OPERATOR_NAME); } }
     */

}
