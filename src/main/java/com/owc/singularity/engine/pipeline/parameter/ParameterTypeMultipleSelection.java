package com.owc.singularity.engine.pipeline.parameter;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.owc.singularity.studio.gui.parameters.celleditors.value.MultipleSelectionValueCellEditor;

/**
 * A Parameter for selecting multiple options.
 * <p>
 * <b>Usage:</b>
 * <ul>
 * <li>Insure to register the corresponding cell editor in your <i>PluginInit*</i> Class within the
 * <i>initPlugin</i> method: <code>
 *     ParameterPanel.registerPropertyValueCellEditor(ParameterTypeMultipleSelection.class, MultipleSelectionCellEditor.class);
 * </code></li>
 * <li>In your <i>i18n resources</i>, add the properties found under "multiple selection" section in
 * the GUIOwcExtension.properties resource</li>
 * </ul>
 *
 */
@ParameterTypeAnnotation(editor = MultipleSelectionValueCellEditor.class)
public class ParameterTypeMultipleSelection extends ParameterTypeString {

    private static final long serialVersionUID = -7342477461878983254L;

    public interface MultiSelectionOptionsChangeListener {

        /**
         * This method is called when options have been changed.
         */
        void informOptionsChanged(List<String> newOptions);
    }

    /**
     * {@link com.owc.singularity.engine.pipeline.parameter.ParameterTypeMultipleSelection}
     * parameters depend on a list of options. These options may be different each time the editor
     * component is shown. To achieve that, the parameter can use this interface to query the
     * available options.
     * <p>
     * GUI components can also add listeners to be informed upon changes.
     *
     * @author Hatem Hamad
     */
    public interface MultiSelectionOptionsProvider {

        List<String> getOptions();

        default void addOptionsChangeListener(MultiSelectionOptionsChangeListener l) {}


        default void removeOptionsChangeListener(MultiSelectionOptionsChangeListener l) {}

    }

    public static final String SEPARATOR_CHARACTER = "|";

    public static final String SEPARATOR_REGEX = "\\|";


    protected MultiSelectionOptionsProvider provider;

    public ParameterTypeMultipleSelection(String key, String description, MultiSelectionOptionsProvider provider) {
        super(key, description);
        this.provider = provider;
    }

    public ParameterTypeMultipleSelection(String key, String description, String[] options) {
        super(key, description);
        this.provider = () -> Arrays.asList(options);
    }

    public List<String> getOptions() {
        return provider.getOptions();
    }

    public String getSeparator() {
        return SEPARATOR_CHARACTER;
    }

    public String getSeparatorRegex() {
        return SEPARATOR_REGEX;
    }

    public static List<String> transformString2List(String parameterAsString) {
        if (parameterAsString == null)
            return Collections.emptyList();
        return Arrays.asList(parameterAsString.split(SEPARATOR_REGEX));
    }
}
