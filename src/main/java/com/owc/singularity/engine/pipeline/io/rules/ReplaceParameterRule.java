/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io.rules;


import org.apache.logging.log4j.Level;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.io.XMLPipelineParser;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Rule to rename parameters.
 * 
 * @author Simon Fischer
 * 
 */
@OperatorParseRuleAnnotation(xmlTagName = "replaceParameter")
public class ReplaceParameterRule extends AbstractConditionedParseRule {

    private String oldAttributeName;
    private String newAttributeName;

    public ReplaceParameterRule(String operatorTypeName, Element element) throws XMLException {
        super(operatorTypeName, element);
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element childElem) {
                if (childElem.getTagName().equals("oldparameter")) {
                    oldAttributeName = childElem.getTextContent();
                } else if (childElem.getTagName().equals("newparameter")) {
                    newAttributeName = childElem.getTextContent();
                }
            }
        }
    }

    @Override
    protected String conditionedApply(Operator operator, String operatorTypeName, XMLPipelineParser parser) {
        if (operator.getParameters().isSpecified(oldAttributeName)) {
            operator.getParameters().renameParameter(oldAttributeName, newAttributeName);
            LogService.getRoot()
                    .log(Level.INFO, "com.owc.singularity.engine.pipeline.io.rules.ReplaceParameterRule.renamed_parameter",
                            new Object[] { oldAttributeName, operator.getName(), operatorTypeName, newAttributeName });
        }
        return null;
    }

    @Override
    public String toString() {
        return "Replace " + operatorTypeName + "." + oldAttributeName + " by " + newAttributeName;
    }
}
