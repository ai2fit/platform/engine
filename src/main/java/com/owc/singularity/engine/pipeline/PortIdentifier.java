package com.owc.singularity.engine.pipeline;


/**
 * This uniquely identifies a port on an operation's input or output side. There are two types:
 * Single ports that are identified by a portIndex = 0 and members of PortExtender forming a
 * PortGroup, which are differentiated with portIndex >=1.
 */
public record PortIdentifier(String portGroupName, int portIndex) {

    public static final int SINGLE_PORT = 0;
    public static final int GROUP_PORT_FIRST_INDEX = 1;

    public PortIdentifier(String portName) {
        this(portName, SINGLE_PORT);
    }
}
