/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.*;

import org.apache.commons.lang.StringUtils;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.studio.gui.parameters.celleditors.value.AttributesValueCellEditor;


/**
 * A parameter type for selecting several attributes. This is merely a copy of the
 * {@link ParameterTypeAttribute}, since it already comes with all needed functions. But we register
 * a different CellRenderer for this class.
 * 
 * @author Tobias Malbrecht, Sebastian Land
 */
@ParameterTypeAnnotation(editor = AttributesValueCellEditor.class)
public class ParameterTypeAttributes extends ParameterTypeAttribute {

    private static final long serialVersionUID = -4177652183651031337L;

    public static final String ATTRIBUTE_SEPARATOR_CHARACTER = "|";

    public static final String ATTRIBUTE_SEPARATOR_REGEX = "\\|";


    public ParameterTypeAttributes(final String key, String description, InputPort inPort) {
        this(key, description, inPort, true, ValueType.NOMINAL, ValueType.NUMERIC, ValueType.TIMESTAMP);
    }

    public ParameterTypeAttributes(final String key, String description, InputPort inPort, ValueType... valueTypes) {
        this(key, description, inPort, true, valueTypes);
    }

    public ParameterTypeAttributes(final String key, String description, InputPort inPort, boolean optional) {
        this(key, description, inPort, optional, ValueType.NOMINAL, ValueType.NUMERIC, ValueType.TIMESTAMP);
    }

    public ParameterTypeAttributes(final String key, String description, InputPort inPort, boolean optional, ValueType... valueTypes) {
        super(key, description, inPort, optional, valueTypes);
    }

    public static Attribute[] getAttributes(ExampleSet exampleSet, ParameterHandler handler, String parameterName) throws UserError {
        String[] groupByAttributesNames = handler.getParameterAsString(parameterName).split(ParameterTypeAttributes.ATTRIBUTE_SEPARATOR_REGEX);

        // if the group-by-attributes parameter is empty, we can skip these checks
        boolean emptySelection = groupByAttributesNames.length == 1 && groupByAttributesNames[0].trim().isEmpty();
        if (emptySelection) {
            return new Attribute[0];
        } else {
            List<String> unknownAttributes = new ArrayList<>();
            Set<Attribute> groupByAttributes = new HashSet<>(groupByAttributesNames.length);

            for (String attributeName : groupByAttributesNames) {
                // the user can add empty attribute names by accident
                if (attributeName.trim().isEmpty()) {
                    continue;
                }
                Attribute attribute = exampleSet.getAttributes().get(attributeName);
                if (attribute == null) {
                    unknownAttributes.add(attributeName);
                } else {
                    groupByAttributes.add(attribute);
                }
            }

            if (!unknownAttributes.isEmpty()) {
                if (unknownAttributes.size() > 1) {
                    throw new UserError(null, "aggregate_group_by_not_found", StringUtils.join(unknownAttributes, ", "));
                } else {
                    throw new UserError(null, "aggregate_group_by_not_found_single", unknownAttributes.get(0));
                }
            } else {
                // keep the attribute ordering of the input example set (to conform with the
                // legacy code)
                Attribute[] groupAttributes = new Attribute[groupByAttributes.size()];
                Iterator<Attribute> it = exampleSet.getAttributes().allAttributes();
                int i = 0;
                while (it.hasNext()) {
                    Attribute attribute = it.next();
                    if (groupByAttributes.contains(attribute)) {
                        groupAttributes[i] = attribute;
                        i++;
                    }
                }
                return groupAttributes;
            }
        }
    }
}
