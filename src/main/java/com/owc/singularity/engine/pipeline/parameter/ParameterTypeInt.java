/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.DefaultPropertyValueCellEditor;


/**
 * A parameter type for integer values. Operators ask for the integer value with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterAsInt(String)}. For infinite
 * ranges Integer.MIN_VALUE and Integer.MAX_VALUE should be used.
 * 
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = DefaultPropertyValueCellEditor.class)
public class ParameterTypeInt extends ParameterTypeNumber {

    private static final long serialVersionUID = -7360090072467405524L;


    private int defaultValue = -1;

    private int min = Integer.MIN_VALUE;

    private int max = Integer.MAX_VALUE;

    private boolean noDefault = true;


    public ParameterTypeInt(String key, String description, int min, int max) {
        this(key, description, min, max, -1);
        this.noDefault = true;
        setOptional(false);
    }

    public ParameterTypeInt(String key, String description, int min, int max, boolean optional) {
        this(key, description, min, max, -1);
        this.noDefault = true;
        setOptional(optional);
    }

    public ParameterTypeInt(String key, String description, int min, int max, int defaultValue) {
        super(key, description);
        this.defaultValue = defaultValue;
        this.min = min;
        this.max = max;
        this.noDefault = false;
    }


    public void setMinValue(int min) {
        this.min = min;
    }

    public void getMaxValue(int max) {
        this.max = max;
    }

    @Override
    public double getMinValue() {
        return min;
    }

    @Override
    public double getMaxValue() {
        return max;
    }

    public int getMinValueInt() {
        return min;
    }

    public int getMaxValueInt() {
        return max;
    }

    public int getDefaultInt() {
        return defaultValue;
    }

    @Override
    public Object getDefaultValue() {
        if (noDefault) {
            return null;
        } else {
            return defaultValue;
        }
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        noDefault = false;
        this.defaultValue = (Integer) defaultValue;
    }

    @Override
    public void setDefaultValueAsRawString(String value) {
        try {
            this.defaultValue = Integer.parseInt(value);
            noDefault = false;
        } catch (NumberFormatException e) {

        }
    }

    /** Returns true. */
    @Override
    public boolean isNumerical() {
        return true;
    }

    @Override
    public String getRange() {
        String range = "integer; ";
        if (min == -Integer.MAX_VALUE) {
            range += "-\u221E";
        } else {
            range += min;
        }
        range += "-";
        if (max == Integer.MAX_VALUE) {
            range += "+\u221E";
        } else {
            range += max;
        }
        if (!noDefault) {
            range += "; default: " + getStringRepresentation(defaultValue);
        }
        return range;
    }

    public String getStringRepresentation(int value) {
        String valueString = value + "";
        if (value == Integer.MAX_VALUE) {
            valueString = "+\u221E";
        } else if (value == Integer.MIN_VALUE) {
            valueString = "-\u221E";
        }
        return valueString;
    }

}
