/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.DefaultPropertyValueCellEditor;


/**
 * A parameter type for boolean parameters. Operators ask for the boolean value with
 * {@link com.owc.singularity.engine.operator.Operator#getParameterAsBoolean(String)}.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
@ParameterTypeAnnotation(editor = DefaultPropertyValueCellEditor.class)
public class ParameterTypeBoolean extends ParameterTypeSingle {

    private static final long serialVersionUID = 6524969076774489545L;

    private boolean defaultValue = false;

    public ParameterTypeBoolean(String key, String description) {
        this(key, description, false);
    }

    public ParameterTypeBoolean(String key, String description, boolean defaultValue) {
        super(key, description);
        this.defaultValue = defaultValue;
    }

    public boolean getDefault() {
        return defaultValue;
    }

    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        if (defaultValue == null) {
            this.defaultValue = false;
        } else if (defaultValue instanceof Boolean) {
            this.defaultValue = (Boolean) defaultValue;
        } else {
            this.defaultValue = Boolean.parseBoolean(defaultValue.toString());
        }
    }

    /** Returns false. */
    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public String getRange() {
        return "boolean; default: " + defaultValue;
    }

    /**
     * {@inheritDoc}
     *
     * @return always {@code false}
     */
    @Override
    public boolean isSensitive() {
        return false;
    }
}
