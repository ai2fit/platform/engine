package com.owc.singularity.engine.pipeline;

public enum PipelineState {

    UNKNOWN(AbstractPipeline.PROCESS_STATE_UNKNOWN), STOPPED(AbstractPipeline.PROCESS_STATE_STOPPED), PAUSED(AbstractPipeline.PROCESS_STATE_PAUSED), RUNNING(
            AbstractPipeline.PROCESS_STATE_RUNNING);

    private final int code;

    PipelineState(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
