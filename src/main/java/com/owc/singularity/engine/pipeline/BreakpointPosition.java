package com.owc.singularity.engine.pipeline;

/**
 * This is an enumeration detailing the position of a breakpoint relative to an operator.
 */
public enum BreakpointPosition {
    BEFORE, AFTER;
}
