package com.owc.singularity.engine.pipeline.parameter.conditions;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;

/**
 * This is a {@link ParameterCondition} that shows parameters depending on their location in a
 * process.
 *
 * @author Sebastian Land
 *
 */
public class ChildOperatorParameterCondition extends ParameterCondition {

    private Class<? extends OperatorChain> parentClass;
    private Operator operator;
    private boolean isIn;
    private boolean onlyDirectChild;

    /**
     * This parameter condition is fulfilled if if the operator is inside the parentClass if isIn is
     * true or if the operator is outside the parentclass otherwise.
     *
     * @param operator
     * @param parentClass
     * @param isIn
     * @param becomeMandatory
     */
    public ChildOperatorParameterCondition(Operator operator, Class<? extends OperatorChain> parentClass, boolean isIn, boolean becomeMandatory) {
        this(operator, parentClass, isIn, becomeMandatory, false);
    }

    public ChildOperatorParameterCondition(Operator operator, Class<? extends OperatorChain> parentClass, boolean isIn, boolean becomeMandatory,
            boolean onlyDirectChild) {
        super(operator, becomeMandatory);
        this.parentClass = parentClass;
        this.operator = operator;
        this.isIn = isIn;
        this.onlyDirectChild = onlyDirectChild;
    }

    @Override
    public boolean isConditionFullfilled() {
        OperatorChain parent = operator.getParent();
        while (parent != null) {
            if (parentClass.isAssignableFrom(parent.getClass())) {
                return isIn;
            }
            if (onlyDirectChild)
                break;
            parent = parent.getParent();
        }
        return !isIn;
    }

}
