package com.owc.singularity.engine.pipeline.execution;


import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.pipeline.*;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.repository.RepositoryPath;

/**
 * This is the central class for executing a pipeline. It does provide getter methods for accessing
 * the current state of the execution and for stopping it. <br>
 * To start a new execution, you should refer to the {@link PipelineExecutionBuilder}.<br>
 * <br>
 * A {@link PipelineExecution} offers {@link PipelineExecutionListener} to keep track of the
 * execution of every single operation. This class only takes care of the execution itself. Handling
 * of object {@link ObjectManager}, parameters {@link ParameterManager} and variables
 * {@link VariableManager} are performed in different classes that partly use the listener interface
 * to be aware of the state of the execution.<br>
 * However logging is a bit different as the execution provides the logger itself, but does not make
 * use of it. But customized log messages about execution of the pipeline can be configured using a
 * {@link PipelineExecutionListener} via {@link PipelineExecutionBuilder}. <br>
 * The execution of a pipeline works by recursively splitting the pipeline into smaller tasks.
 * Execution starts with the root operator, which contains an {@link OperationComposition}. A
 * composition is broken down into the single {@link Operation}s with their dependencies defined by
 * connections between them. As many operations as possible are executed in parallel. Each single
 * operation is excuted by applying the operation's operator class. Each {@link AbstractOperator} is
 * only a singleton and called with the context. No inner states are allowed to ensure thread safety
 * as many operations are executed in parallel. <br>
 * Operations that contain {@link ComposedOperation}s can use the
 * {@link CompositionExecutionBuilder} to configure their execution depending on the inner logic.
 * This can happen either in parallel or sequentially if the results depend on each other.
 */
public class PipelineExecution {

    private static final OperationCompositionExecutionAddress ROOT_COMPOSITION_EXECUTION_ADDRESS = new OperationCompositionExecutionAddress(new Operation[0],
            new OperationComposition[0], new int[0]);
    public static final int PROCESS_STATE_UNKNOWN = -1;
    public static final int PROCESS_STATE_STOPPED = 0;
    public static final int PROCESS_STATE_PAUSED = 1;
    public static final int PROCESS_STATE_RUNNING = 2;

    /**
     * The listeners for monitoring the pipeline execution
     */
    private List<PipelineExecutionListener> pipelineExecutionListeners = new LinkedList<>();

    /**
     * Indicates the current pipeline state.
     */
    private PipelineExecutionState state = PipelineExecutionState.STOPPED;


    private PipelineExecutionContext executionContext;
    private long startTimestamp = -1;
    private long endTimestamp = -1;
    private AbstractPipeline pipelineDefinition;
    private ForkJoinPool pool;
    private Map<RepositoryPath, Level> pathLogLevelMap;
    private DevelopmentExecutionContext developmentContext;

    private List<OperatorException> exceptions = new LinkedList();
    private PipelineExecutionContext pipelineExecutionContext;
    private ForkJoinTask<Map<PortIdentifier, IOObject>> executionTask;
    private Map<PortIdentifier, IOObject> resultsMap;

    PipelineExecution(AbstractPipeline pipeline, PipelineExecutionContext context, DevelopmentExecutionContext developmentContext,
            List<PipelineExecutionListener> pipelineExecutionListeners, Map<RepositoryPath, Level> pathLogLevelMap, ForkJoinPool pool) {
        this.pipelineDefinition = pipeline;
        this.pipelineExecutionContext = context;
        this.developmentContext = developmentContext;
        this.pathLogLevelMap = pathLogLevelMap;
        this.pool = pool;
        this.pipelineExecutionListeners = new LinkedList<>(pipelineExecutionListeners);
        start();
    }

    protected void setState(final PipelineExecutionState state) {
        this.state = state;
    }


    /**
     * Terminates the execution of the pipeline as soon as possible, if it is still executing.
     */
    public void stop() {
        synchronized (state) {
            if (state == PipelineExecutionState.INITIALIZING || state == PipelineExecutionState.RUNNING) {
                state = PipelineExecutionState.TERMINATING;
                pipelineDefinition.stop();
            }
        }
    }

    /**
     * Returns true iff the pipeline should be stopped.
     */
    public boolean shouldStop() {
        return switch (state) {
            case ABORTING -> true;
            case ERROR -> true;
            case FINISHED -> true;
            case STOPPED -> true;
            case TERMINATING -> true;
            default -> false;
        };
    }


    private void fireEventPipelineInit() {
        long timestamp = System.currentTimeMillis();
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onPipelineInit(this.getPipelineDefinition(), timestamp);
        }
    }

    private void fireEventPipelineStart() {
        long timestamp = System.currentTimeMillis();
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onPipelineStart(this.getPipelineDefinition(), timestamp);
        }
    }

    private void fireEventPipelineStopped(Exception exception) {
        long timestamp = System.currentTimeMillis();

        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onPipelineStop(this.getPipelineDefinition(), timestamp);
        }
    }

    private void fireEventOperationStart(OperationExecutionAddress operationExecutionAddress, Map<PortIdentifier, IOObject> inputObjects) {
        long timestamp = System.currentTimeMillis();
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onOperationStart(this.getPipelineDefinition(), operationExecutionAddress, inputObjects, timestamp);
        }
    }

    private void fireEventOperationFinish(OperationExecutionAddress operationExecutionAddress, Map<PortIdentifier, IOObject> resultObjects) {
        long timestamp = System.currentTimeMillis();
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onOperationFinish(this.getPipelineDefinition(), operationExecutionAddress, resultObjects, timestamp);
        }
    }

    private void fireEventPipelineFailed(Exception exception) {
        long timestamp = System.currentTimeMillis();
        OperationExecutionAddress cause = null;
        if (exception instanceof OperatorException)
            cause = getOperationExecutionAddress((OperatorException) exception);
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onPipelineError(this.getPipelineDefinition(), cause, exception, timestamp);
        }
    }

    private void fireEventPipelineFinished(Map<PortIdentifier, IOObject> results) {
        long timestamp = System.currentTimeMillis();
        for (PipelineExecutionListener listener : pipelineExecutionListeners) {
            listener.onPipelineFinish(this.getPipelineDefinition(), results, timestamp);
        }
    }

    /**
     * This method simply starts the execution by executing it as a new task in the given
     * {@link ForkJoinPool}.
     */
    private void start() {
        executionTask = pool.submit(() -> execute());
    }

    /**
     * This method waits for the completion of the task. The current thread will block until then.
     * Then it either returns the results, if the pipeline finished successfully or throws the first
     * error that occurred. If the pipeline was stopped, null is returned. Please notice that there
     * can be multiple operator exceptions, not just the one thrown, due to parallel execution.
     * 
     * @return the results, if the pipeline finished successfully or, if the pipeline was stopped,
     *         null is returned.
     * @throws OperatorException
     *             if there was an error.
     */
    public Map<PortIdentifier, IOObject> waitForResults() throws OperatorException {
        waitForDone();
        if (getState() == PipelineExecutionState.FINISHED)
            return getResults();
        else if (getState() == PipelineExecutionState.ERROR)
            throw getExceptions().iterator().next();
        return null;
    }

    /**
     * This waits for completing the calculations. Completion might be achieved by either error or
     * finishing the execution. No error will be thrown and status of execution must be checked
     * afterwards.
     * 
     * @return this pipeline execution with the status codes
     */
    public PipelineExecution waitForDone() {
        while (!executionTask.isDone()) {
            try {
                executionTask.get();
            } catch (ExecutionException | InterruptedException e) {
            }
        }
        return this;
    }

    /**
     * This executes the pipeline
     * 
     * @return
     */
    private Map<PortIdentifier, IOObject> execute() {

        try {
            setState(PipelineExecutionState.INITIALIZING);
            fireEventPipelineInit();
            this.startTimestamp = System.currentTimeMillis();

            if (pipelineExecutionContext == null) {
                pipelineExecutionContext = getPipelineDefinition().derivePipelineExecutionContext(developmentContext);
            }
            // setting log level
            Level level = pathLogLevelMap.get(pipelineDefinition.getPath());
            if (level != null)
                pipelineExecutionContext.logLevel = level;

            // register listener
            pipelineDefinition.registerLifeCycleEventListener(new PipelineLifeCycleEventListener() {

                @Override
                public String getIdentifier() {
                    return null;
                }
            });
            pipelineDefinition.getRootOperator().addPipelineListener(new com.owc.singularity.engine.pipeline.PipelineExecutionListener() {

                @Override
                public void pipelineStarts(AbstractPipeline process) {
                    fireEventPipelineStart();
                }

                @Override
                public void pipelineOperatorStarted(AbstractPipeline process, Operator op) {
                    fireEventOperationStart(getOperationExecutionAddress(op), getInputObjects(op));
                }

                @Override
                public void pipelineOperatorFinished(AbstractPipeline process, Operator op) {
                    fireEventOperationFinish(getOperationExecutionAddress(op), getOutputObjects(op));
                }


                @Override
                public void pipelineFinished(AbstractPipeline process) {
                    // we ignore that and handle later
                }
            });

            // now execute
            setState(PipelineExecutionState.RUNNING);
            fireEventPipelineStart();

            List<IOObject> results = pipelineDefinition.run(pipelineExecutionContext, true);

            setState(PipelineExecutionState.FINISHED);
            this.endTimestamp = System.currentTimeMillis();


            resultsMap = getResults(results);
            fireEventPipelineFinished(getResults());
            return getResults();
        } catch (ProcessStoppedException e) {
            setState(PipelineExecutionState.STOPPED);
            this.endTimestamp = System.currentTimeMillis();
            fireEventPipelineStopped(e);
            return null;
        } catch (OperatorException e) {
            setState(PipelineExecutionState.ERROR);
            this.exceptions.add(e);

            this.endTimestamp = System.currentTimeMillis();
            fireEventPipelineFailed(e);
            return null;
        } catch (Exception e) {
            setState(PipelineExecutionState.ERROR);
            this.exceptions.add(new OperatorException("Unknown error.", e));
            e.printStackTrace();

            this.endTimestamp = System.currentTimeMillis();
            fireEventPipelineFailed(e);
            return null;
        }
    }


    private Map<PortIdentifier, IOObject> getResults(List<IOObject> results) {
        AbstractPipeline pipeline = pipelineDefinition;
        if (pipeline instanceof ImplementingPipeline)
            pipeline = ((ImplementingRootOperator) ((ImplementingPipeline) pipeline).getRootOperator()).getImplementedPipeline();

        AbstractRootOperator rootOperator = pipeline.getRootOperator();
        Iterator<InputPort> portIterator = rootOperator.getSubprocess(0).getInnerSinks().getAllPorts().iterator();
        Iterator<IOObject> resultIterator = results.iterator();
        Map<PortIdentifier, IOObject> resultMap = new LinkedHashMap<>();
        while (portIterator.hasNext() && resultIterator.hasNext())
            resultMap.put(new PortIdentifier(portIterator.next().getName()), resultIterator.next());
        return resultMap;
    }

    public static void processThrowable(Throwable e) throws OperatorException {
        if (e instanceof OperatorException)
            throw (OperatorException) e;
        else if (e instanceof OperatorRuntimeException)
            throw ((OperatorRuntimeException) e).toOperatorException();
        else if ((e instanceof RuntimeException || e instanceof ExecutionException) && e != e.getCause() && e.getCause() != null) {
            processThrowable(e.getCause());
        }
        throw new OperatorException("unknown_error", e, e.getMessage());
    }


    public AbstractPipeline getPipelineDefinition() {
        return pipelineDefinition;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public RepositoryPath getWorkingPath() {
        return pipelineDefinition.getPath();
    }


    public PipelineExecutionState getState() {
        return this.state;
    }


    /**
     * Helper Method for forward compatibility
     * 
     * @param op
     * @return
     */
    private OperationExecutionAddress getOperationExecutionAddress(Operator op) {
        Operation[] parentOperations = getParentOperations(op);
        OperationComposition[] parentCompositions = getParentCompositions(op);
        return new OperationExecutionAddress(new OperationCompositionExecutionAddress(parentOperations, parentCompositions, getIterations(parentCompositions)),
                new Operation(op));
    }

    private Operation[] getParentOperations(Operator op) {
        LinkedList<Operation> parentOperations = new LinkedList<>();
        op = op.getParent();
        while (op != null) {
            parentOperations.add(0, new Operation(op));
            op = op.getParent();
        }
        return parentOperations.toArray(Operation[]::new);
    }

    private OperationComposition[] getParentCompositions(Operator op) {
        LinkedList<OperationComposition> parentComposition = new LinkedList<>();
        ExecutionUnit parentUnit = op.getExecutionUnit();
        op = op.getParent();
        while (op != null) {
            parentComposition.add(0, new OperationComposition(parentUnit));
            parentUnit = op.getExecutionUnit();
            op = op.getParent();
        }
        return parentComposition.toArray(OperationComposition[]::new);
    }

    private int[] getIterations(OperationComposition[] comps) {
        int[] iterations = new int[comps.length];
        for (int i = 0; i < comps.length; i++) {
            iterations[i] = comps[i].unit().getEnabledOperators().get(0).getApplyCount();
        }
        return iterations;
    }

    /**
     * Helper Method for forward compatibility
     * 
     * @param op
     * @return
     */

    private Map<PortIdentifier, IOObject> getInputObjects(Operator op) {
        return op.getInputPorts()
                .getAllPorts()
                .stream()
                .collect(LinkedHashMap::new, (m, p) -> m.put(new PortIdentifier(p.getName()), p.getDataOrNull()), HashMap::putAll);
    }

    /**
     * Helper Method for forward compatibility
     * 
     * @param op
     * @return
     */

    private Map<PortIdentifier, IOObject> getOutputObjects(Operator op) {
        return op.getOutputPorts()
                .getAllPorts()
                .stream()
                .collect(LinkedHashMap::new, (m, p) -> m.put(new PortIdentifier(p.getName()), p.getDataOrNull()), HashMap::putAll);
    }

    /**
     * Helper Method for forward compatibility
     * 
     * @param op
     * @return
     */
    private OperationExecutionAddress getOperationExecutionAddress(OperatorException exception) {
        if (exception.getOperator() != null)
            return getOperationExecutionAddress(exception.getOperator());
        return null;
    }

    public List<OperatorException> getExceptions() {
        return exceptions;
    }

    public Map<PortIdentifier, IOObject> getResults() {
        return resultsMap;
    }


    /**
     * This method can be used to create a {@link PipelineExecutionBuilder} instance for the given
     * pipeline. The builder can be used to create multiple {@link PipelineExecution} by calling
     * it's {@link PipelineExecutionBuilder#start()} method, which then starts the execution with
     * the current configured settings. The builder can be reused and values can be adapted before
     * starting an execution again.
     * 
     * @param pipeline
     *            the pipeline to configure an execution for
     * @return the builder
     */
    public static PipelineExecutionBuilder configure(AbstractPipeline pipeline) {
        return PipelineExecutionBuilder.configure(pipeline);
    }
}
