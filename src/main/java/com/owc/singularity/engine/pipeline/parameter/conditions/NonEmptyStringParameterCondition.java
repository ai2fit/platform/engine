package com.owc.singularity.engine.pipeline.parameter.conditions;


import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;

public class NonEmptyStringParameterCondition extends ParameterCondition {

    public NonEmptyStringParameterCondition(ParameterHandler handler, String conditionParameter, boolean becomeMandatory) {
        super(handler, conditionParameter, becomeMandatory);
    }

    @Override
    public boolean isConditionFullfilled() {
        try {
            String value = parameterHandler.getParameterAsString(conditionParameter);
            return value != null && !value.isEmpty();
        } catch (UndefinedParameterError e) {
            return false;
        }
    }
}
