/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline;


import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.owc.singularity.engine.pipeline.parameter.AbstractParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;


/**
 * This context is just providing development phase settings. This may be e.g. variable settings or
 * body parts of webservice requests for {@link WebservicePipeline}s.
 * 
 * @author Sebastian Land
 */
public class DevelopmentExecutionContext extends AbstractParameterHandler implements Serializable {

    private static final long serialVersionUID = 1L;


    private AbstractPipeline pipeline;
    private Observer<String> observer;

    public DevelopmentExecutionContext(AbstractPipeline pipeline) {
        super(pipeline.getExecutionContextParameterTypes());
        this.pipeline = pipeline;
        observer = new Observer<String>() {

            @Override
            public void update(Observable<String> observable, String arg) {
                pipeline.fireDevelopmentContextChanged();
            }
        };
        parameters.addObserver(observer, true);
    }

    /**
     * Can be called from pipelines, whose types are only available after init.
     */
    public void refreshParameterTypes() {
        parameters.setTypes(getParameterTypes());
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        if (pipeline == null)
            return Collections.emptyList();
        return pipeline.getExecutionContextParameterTypes();
    }

    public void startImport() {
        parameters.removeObserver(observer);
    }

    /**
     * Can be called from pipelines, whose types are only available after init.
     */
    public void finalizeImport() {
        parameters.setTypes(getParameterTypes());
        parameters.addObserver(observer, true);
    }

}
