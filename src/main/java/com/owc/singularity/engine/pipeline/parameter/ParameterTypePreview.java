/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.parameters.celleditors.value.PreviewValueCellEditor;
import com.owc.singularity.studio.gui.wizards.PreviewCreator;
import com.owc.singularity.studio.gui.wizards.PreviewListener;


/**
 * This parameter type will lead to a GUI element which can be used as initialization for a results
 * preview. This might be practical especially for complex operators which often also provide a
 * configuration wizard.
 * 
 * @author Ingo Mierswa
 */
@ParameterTypeAnnotation(editor = PreviewValueCellEditor.class)
public class ParameterTypePreview extends ParameterType {

    private static final long serialVersionUID = 6538432700371374278L;

    private Class<? extends PreviewCreator> previewCreatorClass;

    private transient PreviewListener previewListener;

    public ParameterTypePreview(Class<? extends PreviewCreator> previewCreatorClass, PreviewListener previewListener) {
        this("preview", "Shows a preview for the results which will be achieved by the current configuration.", previewCreatorClass, previewListener);
    }

    public ParameterTypePreview(String parameterName, String description, Class<? extends PreviewCreator> previewCreatorClass,
            PreviewListener previewListener) {
        super(parameterName, description);
        this.previewCreatorClass = previewCreatorClass;
        this.previewListener = previewListener;
    }

    /**
     * Returns a new instance of the wizard creator. If anything does not work this method will
     * return null.
     */
    public PreviewCreator getPreviewCreator() {
        PreviewCreator creator = null;
        try {
            creator = previewCreatorClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LogService.getRoot()
                    .warn("com.owc.singularity.engine.pipeline.parameter.ParameterTypePreview.problem_during_creation_of_previewer", e.getMessage());
        }
        return creator;
    }

    public PreviewListener getPreviewListener() {
        return previewListener;
    }

    /** Returns null. */
    @Override
    public Object getDefaultValue() {
        return null;
    }

    /** Does nothing. */
    @Override
    public void setDefaultValue(Object defaultValue) {}

    /** Returns null. */
    @Override
    public String getRange() {
        return null;
    }

    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public Element getXML(String key, String value, boolean hideDefault, Document doc) {
        return null;
    }

    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) {
        return parameterValue;
    }
}
