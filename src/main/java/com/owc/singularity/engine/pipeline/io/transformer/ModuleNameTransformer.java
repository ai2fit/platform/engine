package com.owc.singularity.engine.pipeline.io.transformer;

import org.apache.logging.log4j.Level;
import org.w3c.dom.*;

/**
 * An {@link AbstractXPathCompatibilityTransformer} to replace the legacy module key with its
 * current counterpart.
 */
public abstract class ModuleNameTransformer extends AbstractXPathCompatibilityTransformer {

    private String formerModuleName;
    private String newModuleName;

    public static final String LEGACY_FLAG = "legacy";

    public ModuleNameTransformer(String formerModuleName, String newModuleName) throws Exception {
        super(getXPath(formerModuleName, newModuleName));
        this.formerModuleName = formerModuleName;
        this.newModuleName = newModuleName;
    }

    private static String getXPath(String formerModuleName, String newModuleName) {
        if (formerModuleName == null) {
            return "//operator[not(contains(@class,':'))]";
        } else {
            return "//operator[starts-with(@class,'" + formerModuleName + ":')]";
        }
    }

    @Override
    protected void apply(Node operatorNode, CompatibilityTransformationLog log, Document document) {
        NamedNodeMap attributes = operatorNode.getAttributes();
        Node classAttribute = attributes.getNamedItem("class");
        String classValue = classAttribute.getNodeValue();
        if (formerModuleName == null) {
            classAttribute.setNodeValue(newModuleName + ":" + classValue);
        } else {
            classAttribute.setNodeValue(newModuleName + ":" + classValue.substring(classValue.indexOf(":") + 1));
        }
        // set flag to identify legacy operator imports
        attributes.setNamedItem(document.createAttribute(LEGACY_FLAG));
        attributes.getNamedItem(LEGACY_FLAG).setNodeValue("true");
    }

    @Override
    protected void logApplication(NodeList nodes, CompatibilityTransformationLog log) {
        if (nodes.getLength() > 0)
            log.log(Level.INFO,
                    "Replaced " + nodes.getLength() + " operators from module '" + formerModuleName + "' by operators from module '" + newModuleName + "'");
    }
}
