package com.owc.singularity.engine.pipeline;


public record OperationAddress(OperationCompositionAddress compositionAddress, Operation operation) {

    public OperationComposition parentComposition() {
        if (compositionAddress.parentCompositions().length > 0)
            return compositionAddress.parentCompositions()[compositionAddress.parentCompositions().length - 1];
        return null;
    }
}
