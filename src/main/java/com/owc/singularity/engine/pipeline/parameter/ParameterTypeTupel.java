/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import java.util.List;
import java.util.function.BiFunction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.studio.gui.parameters.celleditors.value.ParameterTupelCellEditor;


/**
 * This is a parameter type that contains a number of subtypes. But unlike the
 * {@link ParameterTypeList} or {@link ParameterTypeEnumeration}, these subtypes are treated as one
 * value. In the gui these subtypes are shown beside each other.
 *
 * @author Sebastian Land
 */
@ParameterTypeAnnotation(editor = ParameterTupelCellEditor.class)
public class ParameterTypeTupel extends CombinedParameterType {

    private static final long serialVersionUID = 7292052301201204321L;


    private static final char ESCAPE_CHAR = '\\';
    private static final char XML_SEPERATOR_CHAR = '.';
    private static final char[] XML_SPECIAL_CHARACTERS = new char[] { XML_SEPERATOR_CHAR };
    private static final char INTERNAL_SEPERATOR_CHAR = '.'; // Parameters.PAIR_SEPARATOR; //'.';
    private static final char[] INTERNAL_SPECIAL_CHARACTERS = new char[] { INTERNAL_SEPERATOR_CHAR };

    private Object[] defaultValues = null;

    private ParameterType[] types;

    private double[] weights;

    public ParameterTypeTupel(String key, String description, ParameterType... parameterTypes) {
        super(key, description, parameterTypes);
        this.types = parameterTypes;
    }

    public ParameterTypeTupel(String key, String description, double[] editorWidths, ParameterType... parameterTypes) {
        this(key, description, parameterTypes);
        this.weights = editorWidths;
    }

    @Override
    public Object getDefaultValue() {
        if (defaultValues == null) {
            String[] defaultValues = new String[types.length];
            for (int i = 0; i < types.length; i++) {
                defaultValues[i] = types[i].getDefaultValue() == null ? "" : types[i].getDefaultValue() + "";
            }
            return ParameterTypeTupel.transformTupel2String(defaultValues);
        } else {
            String[] defStrings = new String[defaultValues.length];
            for (int i = 0; i < defaultValues.length; i++) {
                defStrings[i] = defaultValues[i] + "";
            }
            return ParameterTypeTupel.transformTupel2String(defStrings);
        }
    }

    @Override
    public String getDefaultValueAsRawString() {
        String[] defaultStrings = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            Object defaultValue = types[i].getDefaultValue();
            // if (defaultValue == null)
            // return null;
            defaultStrings[i] = types[i].toRawString(defaultValue);
        }
        return ParameterTypeTupel.transformTupel2String(defaultStrings);
    }

    @Override
    public String getRange() {
        return "tupel";
    }

    @Override
    public Element getXML(String key, String value, boolean hideDefault, Document doc) {
        Element element = doc.createElement("parameter");
        element.setAttribute("key", key);
        String[] tupel;
        if (value == null) {
            tupel = transformString2Tupel((String) getDefaultValue());
        } else {
            tupel = transformString2Tupel(value);
        }
        StringBuilder valueString = new StringBuilder();
        boolean first = true;
        for (String part : tupel) {
            if (!first) {
                valueString.append(XML_SEPERATOR_CHAR);
            } else {
                first = false;
            }
            if (part == null) {
                part = "";
            }
            valueString.append(Tools.escape(part, ESCAPE_CHAR, XML_SPECIAL_CHARACTERS));
        }
        element.setAttribute("value", valueString.toString());
        return element;
    }

    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        this.defaultValues = (Object[]) defaultValue;
    }

    public ParameterType getFirstParameterType() {
        return types[0];
    }

    public ParameterType getSecondParameterType() {
        return types[1];
    }

    public ParameterType[] getParameterTypes() {
        return types;
    }

    public static boolean isTupel(String parameterValue) {
        return parameterValue.contains(INTERNAL_SEPERATOR_CHAR + "");
    }

    public static String[] transformString2Tupel(String parameterValue) {
        if (parameterValue == null || parameterValue.isEmpty()) {
            return new String[2];
        }
        List<String> split = Tools.unescape(parameterValue, ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS, INTERNAL_SEPERATOR_CHAR);
        while (split.size() < 2) {
            split.add(null);
        }
        return split.toArray(new String[0]);
    }

    public static String transformTupel2String(String firstValue, String secondValue) {
        return Tools.escape(firstValue, ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS) + INTERNAL_SEPERATOR_CHAR
                + Tools.escape(secondValue, ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS);
    }

    public static String transformTupel2String(Pair<String, String> pair) {
        return transformTupel2String(pair.getFirst(), pair.getSecond());
    }

    public static String transformTupel2String(String[] tupel) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < tupel.length; i++) {
            if (i > 0) {
                builder.append(INTERNAL_SEPERATOR_CHAR);
            }
            if (tupel[i] != null) {
                builder.append(Tools.escape(tupel[i], ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS));
            }
        }
        return builder.toString();
    }

    /** @return the changed value after all tupel entries were notified */
    @Override
    public String notifyOperatorRenaming(String oldOperatorName, String newOperatorName, String parameterValue) {
        return notifyOperatorRenamingReplacing((t, v) -> t.notifyOperatorRenaming(oldOperatorName, newOperatorName, v), parameterValue);
    }

    /** @return the changed value after all tupel entries were notified */
    @Override
    public String notifyOperatorReplacing(String oldName, Operator oldOp, String newName, Operator newOp, String parameterValue) {
        return notifyOperatorRenamingReplacing((t, v) -> t.notifyOperatorReplacing(oldName, oldOp, newName, newOp, v), parameterValue);
    }

    private String notifyOperatorRenamingReplacing(BiFunction<ParameterType, String, String> replacer, String parameterValue) {
        String[] tupel = transformString2Tupel(parameterValue);
        for (int i = 0; i < types.length; i++) {
            tupel[i] = replacer.apply(types[i], tupel[i]);
        }
        return transformTupel2String(tupel);
    }

    @Override
    public String fromRawString(String value) {
        List<String> split = Tools.unescape(value, ESCAPE_CHAR, XML_SPECIAL_CHARACTERS, XML_SEPERATOR_CHAR);
        StringBuilder internalEncoded = new StringBuilder();
        boolean first = true;
        for (String part : split) {
            if (!first) {
                internalEncoded.append(INTERNAL_SEPERATOR_CHAR);
            } else {
                first = false;
            }
            internalEncoded.append(Tools.escape(part, ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS));
        }
        return internalEncoded.toString();
    }

    public static String escapeForInternalRepresentation(String string) {
        return Tools.escape(string, ESCAPE_CHAR, INTERNAL_SPECIAL_CHARACTERS);
    }

    @Override
    public String substituteVariables(String parameterValue, VariableHandler variableHandler) throws UndefinedParameterError {
        if (!parameterValue.contains("%{")) {
            return parameterValue;
        }
        String[] tupel = transformString2Tupel(parameterValue);
        String[] result = new String[tupel.length];
        for (int i = 0; i < tupel.length; i++) {
            result[i] = types[i].substituteVariables(tupel[i], variableHandler);
        }
        return transformTupel2String(result);
    }

    @Override
    public String substitutePredefinedVariables(String parameterValue, Operator operator) throws UndefinedParameterError {
        if (!parameterValue.contains("%{")) {
            return parameterValue;
        }
        String[] tuple = transformString2Tupel(parameterValue);
        String[] result = new String[tuple.length];
        for (int i = 0; i < tuple.length; i++) {
            result[i] = types[i].substitutePredefinedVariables(tuple[i], operator);
        }
        return transformTupel2String(result);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@code true} if any of the parameter types are sensitive; {@code false} otherwise
     */
    @Override
    public boolean isSensitive() {
        for (ParameterType type : getParameterTypes()) {
            if (type.isSensitive()) {
                return true;
            }
        }
        return false;
    }

    public double[] getWeights() {
        return weights;
    }

}
