package com.owc.singularity.engine.pipeline.execution;


/**
 * This state describes the current execution stat of a {@link PipelineExecution}.
 */
public enum PipelineExecutionState {

    INITIALIZING, RUNNING, FINISHED, TERMINATING, STOPPED, ABORTING, ERROR;

}
