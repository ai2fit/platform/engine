package com.owc.singularity.engine.pipeline.io.transformer;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A {@link CompatibilityTransformer} to use when transforming pipeline-related properties such as
 * port connections, notes and pipeline parameters. The implementing transformers should provide
 * their custom logic of selecting the process out of the xml document by implementing the
 * {@link #getProcessExpression()} method.
 *
 * @author Hatem Hamad
 */
public abstract class ProcessReplacementTransformer implements CompatibilityTransformer {

    protected static final XPathFactory xpathfactory = XPathFactory.newInstance();

    private final XPath xpath;

    public ProcessReplacementTransformer() {
        xpath = xpathfactory.newXPath();
    }

    public XPath getXpath() {
        return xpath;
    }

    protected abstract XPathExpression getProcessExpression();

    protected abstract void applyOnProcess(Node processNode, Document document, CompatibilityTransformationLog log) throws Exception;

    @Override
    public void apply(Document document, CompatibilityTransformationLog log) {
        try {
            NodeList processesNodes = (NodeList) getProcessExpression().evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < processesNodes.getLength(); i++) {
                Node processNode = processesNodes.item(i);
                applyOnProcess(processNode, document, log);
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Pipeline transformation failed: " + e.getMessage());
        }
    }
}
