/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter.conditions;


import com.owc.singularity.engine.pipeline.parameter.ParameterHandler;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeEnumCategory;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;


/**
 * This condition checks if a type parameter (category) has a certain value.
 * 
 * @author Sebastian Land, Ingo Mierswa
 */
public class EqualEnumTypeCondition<T extends Enum<T>> extends ParameterCondition {

    private T[] allowedTypes;
    private Class<T> enumClass;

    public EqualEnumTypeCondition(ParameterHandler handler, String conditionParameter, boolean becomeMandatory, Class<T> enumClass, T[] types) {
        super(handler, conditionParameter, becomeMandatory);
        this.enumClass = enumClass;
        allowedTypes = types;
    }

    @Override
    public boolean isConditionFullfilled() {
        boolean equals = false;
        T isType;
        try {
            isType = ParameterTypeEnumCategory.getParameterAsEnumValue(parameterHandler, conditionParameter, enumClass);
        } catch (UndefinedParameterError e) {
            return false;
        }
        for (T type : allowedTypes) {
            equals |= isType == type;
        }
        return equals;
    }
}
