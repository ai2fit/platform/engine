/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.pipeline.parameter;

import java.io.IOException;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ParameterError;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.connection.ConnectionParameters;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.parameters.celleditors.value.ConnectionLocationValueCellEditor;

/**
 * Parameter type for selecting a
 * {@link com.owc.singularity.repository.connection.ConnectionParameters} from the repository. Can
 * narrow down the type of the connection if necessary
 *
 * @author Jan Czogalla
 */
@ParameterTypeAnnotation(editor = ConnectionLocationValueCellEditor.class)
public class ParameterTypeConnectionLocation extends ParameterTypeRepositoryLocation {

    private static final long serialVersionUID = -12191966172771999L;
    private String[] conType;

    /**
     * Minimal constructor, accepting the location of a
     * {@link com.owc.singularity.repository.connection.ConnectionParameters} of any type.
     */
    public ParameterTypeConnectionLocation(String key, String description) {
        this(key, description, (String[]) null);
    }

    /** Constructor with restricting the allowed type of the connection. */
    public ParameterTypeConnectionLocation(String key, String description, String... conType) {
        super(key, description, true, false, true, true);
        this.conType = conType;
    }

    /** Get the allowed types of connections. Returns {@code null} if any connection is allowed. */
    public String[] getConnectionType() {
        return conType;
    }

    /**
     * Load the {@link ConnectionParameters} specified in the operator with the given key
     * 
     * @param operator
     *            the responsible operator
     * @param parameterKey
     *            the parameter key
     * @return a {@link ConnectionParameters} instance loaded from the repository or null if
     *         parameter was empty
     * @throws UserError
     *             if parameter value is an invalid connection location
     * @throws OperatorException
     *             if unable to load the connection from the repository
     */
    public static ConnectionParametersIOObject getConnectionFromParameter(Operator operator, String parameterKey) throws OperatorException {
        RepositoryPath location = operator.getParameterAsRepositoryPath(parameterKey);
        if (location == null)
            return null;
        Entry repositoryEntry = Entries.getEntry(location);
        try {
            if (!repositoryEntry.isInstanceOf(ConnectionParametersIOObject.class, ModuleService.getMajorClassLoader()))
                throw new ParameterError(operator, "connection.wrong_entry_type", parameterKey, location);
            ConnectionParametersIOObject connectionParametersIOObject = repositoryEntry.loadData(ConnectionParametersIOObject.class,
                    ModuleService.getMajorClassLoader());
            return connectionParametersIOObject;
        } catch (ClassNotFoundException | IOException e) {
            throw new OperatorException("Could not load connection from: " + location, e);
        }
    }
}
