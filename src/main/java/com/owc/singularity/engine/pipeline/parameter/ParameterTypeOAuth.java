/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.studio.gui.parameters.celleditors.value.OAuthValueCellEditor;


/**
 * A ParameterType for OAuth. Requires an {@link OAuthMechanism} for the authentication.
 * Authentication workflow is triggered by the {@link OAuthValueCellEditor}.
 * 
 * @author Marcel Michel
 *
 */
@ParameterTypeAnnotation(editor = OAuthValueCellEditor.class)
public class ParameterTypeOAuth extends ParameterTypePassword {

    private static final long serialVersionUID = -2367046707430250941L;

    protected OAuthMechanism oAuth;

    public ParameterTypeOAuth(String key, String description, OAuthMechanism oAuthMechanism) {
        super(key, description);
        oAuth = oAuthMechanism;
    }

    public ParameterTypeOAuth(String key, String description, boolean optional, OAuthMechanism oAuthMechanism) {
        this(key, description, oAuthMechanism);
        setOptional(optional);
    }


    @Override
    public boolean isNumerical() {
        return false;
    }

    @Override
    public Object getDefaultValue() {
        return null;
    }

    @Override
    public void setDefaultValue(Object defaultValue) {}

    public OAuthMechanism getOAuthMechanism() {
        return oAuth;
    };

    public void setOAuthMechanism(OAuthMechanism oAuthMechanism) {
        oAuth = oAuthMechanism;
    }
}
