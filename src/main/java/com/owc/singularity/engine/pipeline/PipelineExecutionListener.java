/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline;


import com.owc.singularity.engine.operator.Operator;


/**
 * Listens to events during the run of a pipeline.
 * 
 * @author Ingo Mierswa Exp $
 */
public interface PipelineExecutionListener {

    /** Will be invoked during pipeline start. */
    void pipelineStarts(AbstractPipeline process);

    /** Will be invoked every time another operator is started in the pipeline. */
    void pipelineOperatorStarted(AbstractPipeline process, Operator op);

    /** Will be invoked every time an operator is finished */
    void pipelineOperatorFinished(AbstractPipeline process, Operator op);

    /** Will invoked when the pipeline was successfully finished. */
    void pipelineFinished(AbstractPipeline process);

}
