/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.parameter;


import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.Port;


/**
 * This interfaces provides the possibility to retrieve Ports during runtime to check for example if
 * the {@link InputPort} is connected or not.
 * 
 * 
 * @author Nils Woehler
 * 
 */
@FunctionalInterface
public interface PortProvider {

    /** Returns the desired {@link Port}. */
    public Port getPort();

}
