/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.pipeline.io;


import static org.reflections.util.ReflectionUtilsPredicates.withClassModifier;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.owc.singularity.engine.modules.Module;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.io.rules.AbstractGenericParseRule;
import com.owc.singularity.engine.pipeline.io.rules.GenericParseRuleAnnotation;
import com.owc.singularity.engine.pipeline.io.rules.OperatorParseRuleAnnotation;
import com.owc.singularity.engine.pipeline.io.rules.ParseRule;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformer;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformerAnnotation;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.XMLException;


/**
 * Class that parses an XML DOM into an {@link Operator}.
 *
 * @author Simon Fischer
 *
 */
public class XMLImporter {


    private static final Logger log = LogManager.getLogger(XMLImporter.class);

    /**
     * Encoding in which process files are written. UTF-8 is guaranteed to exist on any JVM, see
     * javadoc of {@link Charset}.
     */
    public static final Charset PROCESS_FILE_CHARSET = StandardCharsets.UTF_8;


    private static final List<CompatibilityTransformer> TRANSFORMERS = new LinkedList<>();
    private static final HashMap<String, List<ParseRule>> OPERATOR_KEY_RULES_MAP = new HashMap<>();

    private XMLImporter() {}

    /** Reads the parse rules from parserules.xml */
    public static void init() {
        log.debug("Loading parse rules and compatibility transformers...");
        log.trace("Scanning for parse rules...");
        long startTime = System.nanoTime();
        URL rulesResource = XMLImporter.class.getResource("/com/owc/singularity/resources/parserules.xml");
        if (rulesResource != null) {
            // registering the core rules without a name prefix
            importParseRules(rulesResource, null);
        }
        // now create generic rules from annotations
        Reflections reflections = ModuleService.getMajorReflections();
        List<AbstractGenericParseRule> discoveredParseRules = reflections
                .get(Scanners.TypesAnnotated.with(GenericParseRuleAnnotation.class).asClass(reflections.getConfiguration().getClassLoaders()))
                .stream()
                .filter(withClassModifier(Modifier.ABSTRACT).negate())
                .map(ruleClass -> {
                    try {
                        Constructor<?> declaredConstructor = ruleClass.getDeclaredConstructor();
                        return (AbstractGenericParseRule) declaredConstructor.newInstance();
                    } catch (Throwable e) {
                        log.atWarn().withThrowable(e).log("Could not instantiate a parse rule of type {}", ruleClass);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .toList();
        synchronized (OPERATOR_KEY_RULES_MAP) {
            for (AbstractGenericParseRule rule : discoveredParseRules) {
                for (String applicableOperatorKey : rule.getApplicableOperatorKeys()) {
                    OPERATOR_KEY_RULES_MAP.computeIfAbsent(applicableOperatorKey, ignored -> new LinkedList<>()).add(rule);
                }
            }
        }
        log.trace("Registered {} parse rules", OPERATOR_KEY_RULES_MAP::size);
        log.trace("Scanning for parse rules...DONE");
        // init CompatibilityTransformer
        log.trace("Scanning for compatibility transformers...");
        List<CompatibilityTransformer> compatibilityTransformers = reflections
                .get(Scanners.TypesAnnotated.with(CompatibilityTransformerAnnotation.class).asClass(reflections.getConfiguration().getClassLoaders()))
                .stream()
                .filter(CompatibilityTransformer.class::isAssignableFrom)
                .filter(withClassModifier(Modifier.ABSTRACT).negate())
                .sorted(Comparator.comparingInt(o -> o.getAnnotation(CompatibilityTransformerAnnotation.class).priority()))
                .map(transformerClass -> {
                    try {
                        Constructor<?> declaredConstructor = transformerClass.getDeclaredConstructor();
                        declaredConstructor.setAccessible(true);
                        return (CompatibilityTransformer) declaredConstructor.newInstance();
                    } catch (Throwable e) {
                        log.atWarn().withThrowable(e).log("Could not instantiate a compatibility transformer of type {}", transformerClass);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .toList();
        synchronized (TRANSFORMERS) {
            TRANSFORMERS.addAll(compatibilityTransformers);
        }
        log.trace("Registered {} compatibility transformers", compatibilityTransformers::size);
        log.trace("Scanning for compatibility transformers...DONE");
        log.debug("Loading parse rules and compatibility transformers...DONE (took {})", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - startTime)));
    }

    /**
     * This method adds the parse rules from the given resource to the import rule set. The operator
     * name prefix describes the operators coming from plugins. The core operators do not have any
     * name prefix, while the plugin operators are registered using <plugin>:<operatorname>
     */
    public static void importParseRules(URL rulesResource, Module module) {
        if (rulesResource == null) {
            throw new NullPointerException("Parse rules resource must not be null.");
        }
        // use reflections to read definition file
        String operatorNamePrefix = "";
        if (module != null) {
            operatorNamePrefix = module.getKey() + ":";
        }

        String source = module == null ? "Engine" : module.toString();
        log.trace("Reading parse rules from {}", source);
        try {
            Document doc = XMLTools.createDocumentBuilder().parse(rulesResource.openStream());
            if (!doc.getDocumentElement().getTagName().equals("parserules")) {
                log.warn("XML document in '{}' does not start with <parserules>", rulesResource);
            } else {
                NodeList operatorElements = doc.getDocumentElement().getChildNodes();
                List<ParseRule> discoveredParseRules = new ArrayList<>(operatorElements.getLength());
                for (int i = 0; i < operatorElements.getLength(); i++) {
                    if (operatorElements.item(i)instanceof Element operatorElement) {
                        String operatorTypeName = operatorElement.getNodeName();

                        NodeList ruleElements = operatorElement.getChildNodes();
                        for (int j = 0; j < ruleElements.getLength(); j++) {
                            if (ruleElements.item(j) instanceof Element) {
                                ParseRule rule = constructOperatorRuleFromElement(operatorNamePrefix + operatorTypeName, (Element) ruleElements.item(j));
                                // adding rules to lists
                                discoveredParseRules.add(rule);
                                synchronized (OPERATOR_KEY_RULES_MAP) {
                                    OPERATOR_KEY_RULES_MAP.computeIfAbsent(operatorTypeName, ignored -> new LinkedList<>()).add(rule);
                                }
                            }
                        }
                    }
                }
                log.trace("Reading parse rules from {}: Found {} rules", source, discoveredParseRules.size());
            }
        } catch (Exception e) {
            log.atWarn().withThrowable(e).log("Could not instantiate parse rules from {} found in {}", rulesResource, source);
        }
    }


    public static ParseRule constructOperatorRuleFromElement(String operatorTypeName, Element element) throws XMLException {
        String tagName = element.getTagName();

        // operator specific rules
        Reflections REFLECTIONS = ModuleService.getMajorReflections();
        Optional<Class<?>> ruleClassOptional = REFLECTIONS
                .get(Scanners.TypesAnnotated.with(OperatorParseRuleAnnotation.class).asClass(REFLECTIONS.getConfiguration().getClassLoaders()))
                .stream()
                .filter(ruleClass -> ruleClass.getAnnotation(OperatorParseRuleAnnotation.class).xmlTagName().equals(tagName))
                .findAny();


        if (ruleClassOptional.isPresent()) {
            try {
                return (ParseRule) ruleClassOptional.get().getConstructor(String.class, Element.class).newInstance(operatorTypeName, element);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                    | SecurityException e) {
                throw new XMLException("Could not create rule for operator " + operatorTypeName + ". Annotated class incompatible.", e);
            }
        }
        throw new XMLException("No annotated rule found with compatible tag. Check classpath.");
    }

    public static List<CompatibilityTransformer> getCompatibilityTransformers() {
        return TRANSFORMERS;
    }

    public static HashMap<String, List<ParseRule>> getOperatorParseRules() {
        return OPERATOR_KEY_RULES_MAP;
    }

    public static AbstractPipeline parse(String xml) throws XMLException, OperatorCreationException {
        return newPipelineParser().parse(xml);
    }

    public static XMLPipelineParser newPipelineParser() {
        return new XMLPipelineParser(null, getCompatibilityTransformers(), getOperatorParseRules());
    }

    public static XMLPipelineParser newPipelineParser(ProgressListener progressListener) {
        return new XMLPipelineParser(progressListener, getCompatibilityTransformers(), getOperatorParseRules());
    }
}
