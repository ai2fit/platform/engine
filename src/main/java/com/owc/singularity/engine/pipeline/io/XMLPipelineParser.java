package com.owc.singularity.engine.pipeline.io;

import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.*;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.engine.pipeline.DevelopmentExecutionContext;
import com.owc.singularity.engine.pipeline.io.rules.ParseRule;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformationLog;
import com.owc.singularity.engine.pipeline.io.transformer.CompatibilityTransformer;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeEnumeration;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeList;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.tools.VersionNumber;

public class XMLPipelineParser {

    private static final Logger log = LogService.getI18NLogger(XMLPipelineParser.class);
    /**
     * A log marker that indicates a log message is relevant for compatibility warning. This is a
     * child of {@link LogService#MARKER_DESIGN_MODE} marker which make this useful only when in
     * pipeline design mode
     */
    private static final Marker MARKER_PARAMETER_COMPATIBILITY = MarkerManager.getMarker("PARAMETER_COMPATIBILITY").setParents(LogService.MARKER_DESIGN_MODE);
    private static final HashMap<String, List<String>> IRRELEVANT_PARAMETERS = new HashMap<String, List<String>>();

    static {
        IRRELEVANT_PARAMETERS.put("core:execute_analysis_pipeline", List.of("variable_.*"));
        IRRELEVANT_PARAMETERS.put("core:include_sub_pipeline", List.of("variable_.*"));
    }
    private final List<Runnable> jobsAfterAutoWire = new LinkedList<Runnable>();
    private final List<Runnable> jobsAfterTreeConstruction = new LinkedList<Runnable>();

    private boolean mustAutoConnect = false;

    private int total;
    private final ProgressListener progressListener;
    private final List<CompatibilityTransformer> transformers;
    private final Map<String, List<ParseRule>> parseRules;
    private int created = 0;

    private boolean operatorAsDirectChildrenDeprecatedReported = false;
    private final TreeMap<Level, List<String>> transformationMessages = new TreeMap<>(Comparator.comparingInt(Level::intLevel));
    private final CompatibilityTransformationLog transformationLog = new CompatibilityTransformationLog() {


        @Override
        public void log(Level level, String message) {
            List<String> messages = transformationMessages.getOrDefault(level, new LinkedList<>());
            messages.add(message);
            transformationMessages.put(level, messages);
        }
    };

    /** Creates a new importer that reports progress to the given listener. */
    public XMLPipelineParser(ProgressListener listener, List<CompatibilityTransformer> transformers, Map<String, List<ParseRule>> parseRules) {
        progressListener = listener;
        this.transformers = transformers;
        this.parseRules = parseRules;
    }

    public void addMessage(String msg) {
        transformationLog.log(Level.INFO, msg);
    }

    public AbstractPipeline parse(String pipelineDefinition) throws XMLException, OperatorCreationException {
        return parse(pipelineDefinition, Collections.emptyList());
    }

    public AbstractPipeline parse(String pipelineDefinition, List<UnknownParameterInformation> unknownParameters)
            throws XMLException, OperatorCreationException {
        if (progressListener != null) {
            progressListener.setCompleted(20);
        }

        Document doc;
        try {
            doc = XMLTools.parse(pipelineDefinition);
        } catch (SAXException | IOException e) {
            throw new XMLException("Cannot parse document: " + e.getMessage(), e);
        }

        for (CompatibilityTransformer transformer : transformers)
            transformer.apply(doc, transformationLog);
        Element pipelineElement = doc.getDocumentElement();

        // find version number
        VersionNumber pipelineVersion = parseVersion(pipelineElement);

        // now we create pipeline object
        String pipelineClassName = pipelineElement.getAttribute("type");
        try {
            AbstractPipeline pipeline = (AbstractPipeline) Class.forName(pipelineClassName, true, ModuleService.getMajorClassLoader())
                    .getConstructor()
                    .newInstance();
            // set importing
            pipeline.getDevelopmentContext().startImport();

            // add context
            parseDevelopmentExecutionContext(doc.getDocumentElement(), pipeline, unknownParameters);

            // create root operator
            Element rootOperatorElement = XMLTools.getChildTag(pipelineElement, "operator", false);
            AbstractRootOperator rootOperator = parseRootOperator(rootOperatorElement, pipeline, pipelineVersion, unknownParameters);

            pipeline.setRootOperator(rootOperator);

            // finalize importing
            pipeline.getDevelopmentContext().finalizeImport();

            return pipeline;
        } catch (IllegalAccessException e) {
            throw new OperatorCreationException(OperatorCreationException.ILLEGAL_ACCESS_ERROR, "(" + pipelineClassName + ")", e);
        } catch (NoSuchMethodException e) {
            throw new OperatorCreationException(OperatorCreationException.NO_CONSTRUCTOR_ERROR, "(" + pipelineClassName + ")", e);
        } catch (java.lang.reflect.InvocationTargetException e) {
            throw new OperatorCreationException(OperatorCreationException.CONSTRUCTION_ERROR, "(" + pipelineClassName + ")", e);
        } catch (Throwable t) {
            throw new OperatorCreationException(OperatorCreationException.INSTANTIATION_ERROR, "(" + pipelineClassName + ")", t);
        }
    }

    private void parseDevelopmentExecutionContext(Element pipelineElement, AbstractPipeline pipeline, List<UnknownParameterInformation> unknownParameters)
            throws XMLException, OperatorCreationException {


        // AbstractPipeline context
        Collection<Element> contextElements = XMLTools.getChildElements(pipelineElement, "context");
        switch (contextElements.size()) {
            case 0:
                break;
            case 1:
                parseContext(contextElements.iterator().next(), pipeline);
                break;
            default:
                addMessage("&lt;pipeline&gt; can have at most one &lt;context&gt; tag.");
                break;
        }

        if (hasMessage()) {
            pipeline.setImportMessage(getMessage());
        }
    }

    private AbstractRootOperator parseRootOperator(Element rootOperatorElement, AbstractPipeline pipeline, VersionNumber pipelineFileVersion,
            List<UnknownParameterInformation> unknownParameters) throws XMLException, OperatorCreationException {
        Operator rootOp = parseOperator(rootOperatorElement, pipeline, pipelineFileVersion, unknownParameters);

        for (Runnable runnable : jobsAfterTreeConstruction) {
            runnable.run();
        }

        if (mustAutoConnect) {
            if (rootOp instanceof OperatorChain) {
                try {
                    ((OperatorChain) rootOp).getSubprocess(0).autoWire(true, true);
                    addMessage("SingularityEngine pipelines define an explicit data flow. This data flow has been constructed automatically.");
                } catch (Exception e) {
                    addMessage(
                            "SingularityEngine pipelines define an explicit data flow. This data flow could not be constructed automatically due to an error: "
                                    + e.getMessage());
                    log.catching(e);
                }
            }
        }
        for (Runnable runnable : jobsAfterAutoWire) {
            runnable.run();
        }
        if (rootOp instanceof AbstractRootOperator) {
            return (AbstractRootOperator) rootOp;
        } else {
            throw new XMLException("Outermost operator must be of type 'Pipeline' (<operator class=\"Pipeline\">)");
        }
    }

    private void parseProcess(Element element, ExecutionUnit executionUnit, VersionNumber processFileVersion, AbstractPipeline process,
            List<UnknownParameterInformation> unknownParameterInformation) throws XMLException, OperatorCreationException {
        assert "pipeline".equals(element.getTagName());

        if (element.hasAttribute("expanded")) {
            String expansionString = element.getAttribute("expanded");
            if ("no".equals(expansionString) || "false".equals(expansionString)) {
                executionUnit.setExpanded(false);
            } else if ("yes".equals(expansionString) || "true".equals(expansionString)) {
                executionUnit.setExpanded(true);
            } else {
                throw new XMLException("Expansion mode `" + expansionString + "` is not defined!");
            }
        }

        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element opElement) {
                if ("operator".equals(opElement.getTagName())) {
                    parseOperator(opElement, executionUnit, processFileVersion, process, unknownParameterInformation);
                } else if ("connect".equals(opElement.getTagName())) {
                    parseConnection(opElement, executionUnit);
                } else if ("portSpacing".equals(opElement.getTagName())) {
                    // ignore, parsed by ProcessRenderer
                } else if ("description".equals(opElement.getTagName())) {
                    // ignore, parsed by GUIProcessXMLFilter
                } else if ("background".equals(opElement.getTagName())) {
                    // ignore, parsed by GUIProcessXMLFilter
                } else {
                    addMessage("<em class=\"error\">ExecutionUnit must only contain <operator> tags as children. Ignoring unknown tag <code>&lt;"
                            + opElement.getTagName() + "&gt;</code>.</em>");
                }
            }
        }

        PipelineXMLFilterRegistry.fireExecutionUnitImported(executionUnit, element);
    }

    private void parseConnection(Element connectionElement, ExecutionUnit executionUnit) throws XMLException {
        final OutputPorts outputPorts;
        if (connectionElement.hasAttribute("from_op")) {
            String fromOp = connectionElement.getAttribute("from_op");
            Operator from = executionUnit.getOperatorByName(fromOp);
            if (from == null) {
                addMessage("<em class=\"error\">Unknown operator " + fromOp + " referenced in <code>from_op</code>.</em>");
                return;
            }
            outputPorts = from.getOutputPorts();
        } else {
            outputPorts = executionUnit.getInnerSources();
        }
        String fromPort = connectionElement.getAttribute("from_port");
        OutputPort out = outputPorts.getPortByName(fromPort);
        if (out == null) {
            addMessage("<em class=\"error\">The output port <var>" + fromPort + "</var> is unknown at operator <var>" + outputPorts.getOwner().getName()
                    + "</var>.</em>");
            return;
        }

        final InputPorts inputPorts;
        if (connectionElement.hasAttribute("to_op")) {
            String toOp = connectionElement.getAttribute("to_op");
            Operator to = executionUnit.getOperatorByName(toOp);
            if (to == null) {
                addMessage("<em class=\"error\">Unknown operator " + toOp + " referenced in <code>to_op</code>.</em>");
                return;
            }
            inputPorts = to.getInputPorts();
        } else {
            inputPorts = executionUnit.getInnerSinks();
        }
        String toPort = connectionElement.getAttribute("to_port");
        InputPort in = inputPorts.getPortByName(toPort);
        if (in == null) {
            addMessage("<em class=\"error\">The input port <var>" + toPort + "</var> is unknown at operator <var>" + inputPorts.getOwner().getName()
                    + "</var>.</em>");
            return;
        }
        try {
            out.connectTo(in);
        } catch (PortException e) {
            addMessage("<em class=\"error\">Faild to connect ports: " + e.getMessage() + "</var>.</em>");
        }
    }

    public Operator parseOperator(Element opElement, AbstractPipeline pipeline, VersionNumber processVersion,
            List<UnknownParameterInformation> unknownParameterInformation) throws XMLException, OperatorCreationException {
        total = opElement.getElementsByTagName("operator").getLength();
        Operator operator = parseOperator(opElement, null, processVersion, pipeline, unknownParameterInformation);
        unlockPorts(operator);
        return operator;
    }

    private Operator parseOperator(Element opElement, ExecutionUnit addToProcess, VersionNumber processFileVersion, AbstractPipeline pipeline,
            List<UnknownParameterInformation> unknownParameterInformation) throws XMLException, OperatorCreationException {
        assert "operator".equals(opElement.getTagName());
        String className = opElement.getAttribute("class");
        String replacement = OperatorService.getReplacementForDeprecatedClass(className);

        if (replacement != null) {
            addMessage("Deprecated operator '<code>" + className + "</code>' was replaced by '<code>" + replacement + "</code>'.");
            className = replacement;
        }
        OperatorDescription opDescr = OperatorService.getOperatorDescription(className);
        if (opDescr == null)
            throw new XMLException("Unknown operator class: '" + className + "'!");

        Operator operator = OperatorService.createOperator(opDescr);

        PipelineXMLFilterRegistry.fireOperatorImported(operator, opElement);
        created++;
        if (progressListener != null && total > 0) {
            progressListener.setCompleted(100 * created / total);
        }

        operator.rename(opElement.getAttribute("name"));
        String versionString = opElement.getAttribute("compatibility");

        OperatorVersion[] incompatibleVersionChanges = operator.getIncompatibleVersionChanges();
        assert incompatibleVersionChanges != null;

        // sort to be sure to have an array of ascending order
        Arrays.sort(incompatibleVersionChanges);

        OperatorVersion opVersion = null;

        /*
         * Here we are searching if there has been any change since the last save. If no, we use the
         * latest version to save it again. If yes, use the earliest incompatible version.
         */
        OperatorVersion latestOperatorVersion = OperatorVersion.getLatestVersion(opDescr);
        if (versionString != null && !versionString.isEmpty()) {
            try {
                opVersion = new OperatorVersion(versionString);
            } catch (VersionNumber.VersionNumberException e) {
                addMessage("Failed to parse version string '" + versionString + "' for operator " + operator.getName() + ".");
                // fall back to 5.0 on malformed version string
                opVersion = new OperatorVersion(5, 0, 0);
            }
        }
        if (opVersion == null) {
            // fall back version for missing version string
            opVersion = getEnclosingVersion(pipeline);
        }

        OperatorVersion nextIncompatibility = null;
        for (int i = incompatibleVersionChanges.length - 1; i >= 0; i--) {
            if (opVersion.isAtMost(incompatibleVersionChanges[i])) {
                nextIncompatibility = incompatibleVersionChanges[i];
            } else {
                break;
            }
        }

        if (nextIncompatibility == null) {
            opVersion = latestOperatorVersion;
        } else {
            opVersion = nextIncompatibility;
        }
        operator.setCompatibilityLevel(opVersion);
        if (operator instanceof AbstractRootOperator && pipeline != null) {
            // add root operator to pipeline to enable fallback version
            pipeline.setRootOperator((AbstractRootOperator) operator);
        }

        /*
         * Check if the selected operator version does not equal the latest version (hence an
         * older/incompatible version was loaded)
         */
        if (incompatibleVersionChanges.length > 0 && opVersion.compareTo(latestOperatorVersion) != 0) {
            addMessage(
                    "Operator '" + operator.getName() + "' was created with version '" + opVersion + "'. The operator's behaviour has changed as of version '"
                            + latestOperatorVersion + "' and can be adapted to the latest version in the parameter panel.");
        }

        if (opElement.hasAttribute("breakpoints")) {
            String breakpointString = opElement.getAttribute("breakpoints");
            boolean ok = false;
            if (breakpointString.equals("both")) {
                operator.setBreakpoint(BreakpointListener.BREAKPOINT_BEFORE, true);
                operator.setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, true);
                ok = true;
            }
            for (int i = 0; i < BreakpointListener.BREAKPOINT_POS_NAME.length; i++) {
                if (breakpointString.contains(BreakpointListener.BREAKPOINT_POS_NAME[i])) {
                    operator.setBreakpoint(i, true);
                    ok = true;
                }
            }
            if (!ok) {
                throw new XMLException("Breakpoint `" + breakpointString + "` is not defined!");
            }
        }

        if (opElement.hasAttribute("activated")) {
            String activationString = opElement.getAttribute("activated");
            if (activationString.equals("no") || activationString.equals("false")) {
                operator.setEnabled(false);
            } else if (activationString.equals("yes") || activationString.equals("true")) {
                operator.setEnabled(true);
            } else {
                throw new XMLException("Activation mode `" + activationString + "` is not defined!");
            }
        }

        if (opElement.hasAttribute("expanded")) {
            String expansionString = opElement.getAttribute("expanded");
            if ("no".equals(expansionString) || "false".equals(expansionString)) {
                operator.setExpanded(false);
            } else if ("yes".equals(expansionString) || "true".equals(expansionString)) {
                operator.setExpanded(true);
            } else {
                throw new XMLException("Expansion mode `" + expansionString + "` is not defined!");
            }
        }

        if (addToProcess != null) {
            addToProcess.addOperator(operator);
        }

        // parameters and inner operators
        NodeList innerTags = opElement.getChildNodes();
        for (int i = 0; i < innerTags.getLength(); i++) {
            Node node = innerTags.item(i);
            if (node instanceof Element inner) {
                boolean knownType;
                switch (inner.getTagName().toLowerCase()) {
                    case "parameter":
                        String[] parameter = parseParameter(inner);
                        knownType = operator.getParameters().setRawParameter(parameter[0], parameter[1]);
                        if (!knownType) {
                            if (isRelevantParameter(className, parameter[0])) {
                                log.atDebug()
                                        .withMarker(MARKER_PARAMETER_COMPATIBILITY)
                                        .log("com.owc.singularity.engine.pipeline.io.XMLImporter.attribute_not_found_unknown", parameter[0], operator.getName(),
                                                operator.getOperatorDescription().getFullyQuallifiedKey());
                            }
                        }
                        break;
                    case "list": {
                        final String key = inner.getAttribute("key");
                        ParameterType type = operator.getParameters().getParameterType(key);
                        if (type == null) {

                            if (isRelevantParameter(className, key)) {
                                log.atDebug()
                                        .withMarker(MARKER_PARAMETER_COMPATIBILITY)
                                        .log("com.owc.singularity.engine.pipeline.io.XMLImporter.attribute_not_found_list", key, operator.getName(),
                                                operator.getOperatorDescription().getFullyQuallifiedKey());

                            }
                        } else {
                            if (!(type instanceof ParameterTypeList)) {
                                addMessage("The parameter '" + type.getKey() + "' is a " + type.getClass().getSimpleName() + ", but a list was expected.");
                            } else {
                                List<String[]> listDescription = parseParameterList(inner, (ParameterTypeList) type);
                                final String listString = ParameterTypeList.transformList2String(listDescription);
                                String listKey = parseParameterListKey(inner);
                                knownType = operator.getParameters().setRawParameter(listKey, listString);
                                if (!knownType) {
                                    if (isRelevantParameter(className, listKey)) {
                                        log.atDebug()
                                                .withMarker(MARKER_PARAMETER_COMPATIBILITY)
                                                .log("com.owc.singularity.engine.pipeline.io.XMLImporter.attribute_not_found_unknown",
                                                        parseParameterListKey(inner), operator.getName(),
                                                        operator.getOperatorDescription().getFullyQuallifiedKey());

                                    }
                                }
                            }
                        }
                        break;
                    }
                    case "enumeration": {
                        final String key = inner.getAttribute("key");
                        ParameterType type = operator.getParameters().getParameterType(key);
                        if (type == null) {
                            if (isRelevantParameter(className, key)) {
                                log.atDebug()
                                        .withMarker(MARKER_PARAMETER_COMPATIBILITY)
                                        .log("com.owc.singularity.engine.pipeline.io.XMLImporter.attribute_not_found_enum", key, operator.getName(),
                                                operator.getOperatorDescription().getFullyQuallifiedKey());

                            }
                        } else {
                            if (!(type instanceof ParameterTypeEnumeration)) {
                                addMessage(
                                        "The parameter '" + type.getKey() + "' is a " + type.getClass().getSimpleName() + ", but an enumeration was expected.");
                                type = null;
                            } else {
                                final List<String> parsed = parseParameterEnumeration(inner, (ParameterTypeEnumeration) type);
                                knownType = operator.getParameters().setRawParameter(key, ParameterTypeEnumeration.transformEnumeration2String(parsed));
                                if (!knownType) {
                                    if (isRelevantParameter(className, key)) {
                                        log.atDebug()
                                                .withMarker(MARKER_PARAMETER_COMPATIBILITY)
                                                .log("com.owc.singularity.engine.pipeline.io.XMLImporter.attribute_not_found_unknown", key, operator.getName(),
                                                        operator.getOperatorDescription().getFullyQuallifiedKey());

                                    }
                                }
                            }
                        }
                        break;
                    }
                    case "operator":
                    case "pipeline":
                        if (!(operator instanceof OperatorChain)) {
                            addMessage("<em class=\"error\">Operator '<class>" + operator.getOperatorDescription().getName()
                                    + "</class>' may not have children. Ignoring.");
                        }
                        // otherwise, we do the parsing later
                        break;
                    case "description":
                        // ignore, parsed by GUIProcessXMLFilter
                        break;
                    default:
                        addMessage("<em class=\"error\">Ignoring unknown inner tag for <code>&gt;operator&lt;</code>: <code>&lt;" + inner.getTagName()
                                + "&gt;</code>.");
                        break;
                }
            }
        }

        if (operator instanceof OperatorChain nop) {
            NodeList children = opElement.getChildNodes();
            int subprocessIndex = 0;
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child instanceof Element childProcessElement) {
                    if ("pipeline".equals(childProcessElement.getTagName()) || "operator".equals(childProcessElement.getTagName())) {
                        if (subprocessIndex >= nop.getNumberOfSubprocesses() && !nop.areSubprocessesExtendable()) {
                            addMessage("<em class=\"error\">Cannot add child " + childProcessElement.getAttribute("name") + "</var>.</em> Operator <code>"
                                    + nop.getOperatorDescription().getName() + "</code> has only " + nop.getNumberOfSubprocesses() + " sub-pipelines.");
                        } else {
                            if (subprocessIndex >= nop.getNumberOfSubprocesses()) {
                                // we know nop.areSubprocessesExtendable()==true now
                                nop.addSubprocess(subprocessIndex);
                            }
                            if ("pipeline".equals(childProcessElement.getTagName())) {
                                ExecutionUnit subprocess = nop.getSubprocess(subprocessIndex);
                                subprocessIndex++;
                                parseProcess(childProcessElement, subprocess, processFileVersion, pipeline, unknownParameterInformation);
                            } else if ("operator".equals(childProcessElement.getTagName())) {
                                if (!operatorAsDirectChildrenDeprecatedReported) {
                                    addMessage(
                                            "<code>&lt;operator&gt;</code> as children of <code>&lt;operator&gt</code> is deprecated syntax. From version 5.0 on, use <code>&lt;pipeline&gt;</code> as children.");
                                    operatorAsDirectChildrenDeprecatedReported = true;
                                }
                                final ExecutionUnit subprocess = nop.getSubprocess(subprocessIndex);
                                if (subprocessIndex <= nop.getNumberOfSubprocesses() - 2 || nop.areSubprocessesExtendable()) {
                                    subprocessIndex++;
                                }
                                parseOperator(childProcessElement, subprocess, processFileVersion, pipeline, unknownParameterInformation);
                                mustAutoConnect = true;
                            }
                        }
                    }
                }
            }
        }

        /**
         * Apply all parse rules that are applicable for this operator.
         */
        String key = operator.getOperatorDescription().getFullyQuallifiedKey();
        List<ParseRule> list = parseRules.get(key);
        if (list != null) {
            for (ParseRule rule : list) {
                String msg = rule.apply(operator, processFileVersion, this);
                if (msg != null) {
                    pipeline.setProcessConverted(true);
                    addMessage(msg);
                }
            }
        }
        return operator;
    }

    /**
     * Returns the best default version for an operator. Will use the root operator or current
     * SingularityEngine version (in this order). The given pipeline can be null.
     *
     * @param pipeline
     *            the pipeline
     *
     * @return the best matching enclosing version
     */
    private OperatorVersion getEnclosingVersion(AbstractPipeline pipeline) {
        if (pipeline != null) {
            Operator root = pipeline.getRootOperator();
            if (root != null && root.getCompatibilityLevel() != null) {
                return root.getCompatibilityLevel();
            }
        }
        return OperatorVersion.asNewOperatorVersion(SingularityEngine.getVersion());
    }

    private boolean isRelevantParameter(String operatorKey, String paramName) {
        if (operatorKey.equals("core:dummy") || operatorKey.equals("core:dummy_composite"))
            return false;
        List<String> possiblePattern = IRRELEVANT_PARAMETERS.get(operatorKey);
        if (possiblePattern != null) {
            for (String pattern : possiblePattern) {
                if (paramName.matches(pattern))
                    return false;
            }
        }
        return true;
    }

    private List<String> parseList(Element parent, String childName) {
        List<String> result = new LinkedList<String>();
        NodeList childNodes = parent.getElementsByTagName(childName);
        switch (childNodes.getLength()) {
            case 0 -> addMessage("Missing &lt;" + childName + "&gt; tag in context.");
            case 1 -> {
                NodeList locationNodes = ((Element) childNodes.item(0)).getElementsByTagName("location");
                for (int i = 0; i < locationNodes.getLength(); i++) {
                    result.add(locationNodes.item(i).getTextContent());
                }
            }
            default -> addMessage("&lt;context&gt; can have at most one &lt;" + childName + "&gt; tag.");
        }
        return result;
    }

    private void parseContext(Element element, AbstractPipeline pipeline) throws XMLException {
        DevelopmentExecutionContext context = pipeline.getDevelopmentContext();

        // parameters and inner operators
        NodeList innerTags = element.getChildNodes();
        for (int i = 0; i < innerTags.getLength(); i++) {
            Node node = innerTags.item(i);
            if (node instanceof Element inner) {
                switch (inner.getTagName().toLowerCase()) {
                    case "parameter":
                        String[] parameter = parseParameter(inner);
                        context.getParameters().setRawParameter(parameter[0], parameter[1]);
                        break;
                    case "list": {
                        final String key = inner.getAttribute("key");
                        ParameterType type = context.getParameters().getParameterType(key);
                        if (!(type instanceof ParameterTypeList)) {
                            addMessage("The parameter '" + type.getKey() + "' is a " + type.getClass().getSimpleName() + ", but a list was expected.");
                        } else {
                            List<String[]> listDescription = parseParameterList(inner, (ParameterTypeList) type);
                            final String listString = ParameterTypeList.transformList2String(listDescription);
                            String listKey = parseParameterListKey(inner);
                            context.getParameters().setRawParameter(listKey, listString);
                        }
                        break;
                    }
                    case "enumeration": {
                        final String key = inner.getAttribute("key");
                        ParameterType type = context.getParameters().getParameterType(key);
                        if (!(type instanceof ParameterTypeEnumeration)) {
                            addMessage("The parameter '" + type.getKey() + "' is a " + type.getClass().getSimpleName() + ", but an enumeration was expected.");
                            type = null;
                        } else {
                            final List<String> parsed = parseParameterEnumeration(inner, (ParameterTypeEnumeration) type);
                            context.getParameters().setRawParameter(key, ParameterTypeEnumeration.transformEnumeration2String(parsed));
                        }

                        break;
                    }
                }
            }
        }
    }

    private List<String[]> parseParameterList(Element list, ParameterTypeList type) throws XMLException {
        ParameterType keyType = type.getKeyType();
        ParameterType valueType = type.getValueType();

        List<String[]> values = new LinkedList<String[]>();
        NodeList children = list.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node instanceof Element inner) {
                if (inner.getTagName().equalsIgnoreCase("parameter")) {
                    String key = inner.getAttribute("key");
                    String value = inner.getAttribute("value");
                    final String transformedKey = keyType.fromRawString(key);
                    final String transformedValue = valueType.fromRawString(value);
                    values.add(new String[] { transformedKey, transformedValue });
                } else {
                    addMessage("<em class=\"error\">Ilegal inner tag for <code>&lt;list&gt;</code>: <code>&lt;" + inner.getTagName() + "&gt;</code>.</em>");
                    return Collections.<String[]> emptyList();
                }
            }
        }
        return values;
    }

    private String parseParameterListKey(Element list) {
        return list.getAttribute("key");
    }

    private List<String> parseParameterEnumeration(Element list, ParameterTypeEnumeration type) throws XMLException {
        List<String> values = new LinkedList<String>();
        NodeList children = list.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node instanceof Element inner) {
                if (inner.getTagName().equalsIgnoreCase("parameter")) {
                    values.add(type.getValueType().fromRawString(inner.getAttribute("value")));
                } else {
                    addMessage(
                            "<em class=\"error\">Ilegal inner tag for <code>&lt;enumeration&gt;</code>: <code>&lt;" + inner.getTagName() + "&gt;</code>.</em>");
                    return new LinkedList<String>();
                }
            }
        }
        return values;
    }

    private String[] parseParameter(Element parameter) {
        return new String[] { parameter.getAttribute("key"), parameter.getAttribute("value") };
    }

    private VersionNumber parseVersion(Element element) {
        if (element.hasAttribute("version")) {
            try {
                return new VersionNumber(element.getAttribute("version"));
            } catch (NumberFormatException e) {
                addMessage(
                        "<em class=\"error\">The version " + element.getAttribute("version") + " is not a legal SingularityEngine version, assuming 4.0.</em>");
                return new VersionNumber(4, 0, 0);
            }
        }
        addMessage("<em class=\"error\">Found no version information, assuming 4.0.</em>");
        return new VersionNumber(4, 0, 0);
    }

    private void unlockPorts(Operator operator) {
        operator.getInputPorts().unlockPortExtenders();
        operator.getOutputPorts().unlockPortExtenders();
        if (operator instanceof OperatorChain) {
            for (ExecutionUnit unit : ((OperatorChain) operator).getSubprocesses()) {
                unit.getInnerSinks().unlockPortExtenders();
                unit.getInnerSources().unlockPortExtenders();
                for (Operator child : unit.getOperators()) {
                    unlockPorts(child);
                }
            }
        }
    }

    public void doAfterAutoWire(Runnable runnable) {
        jobsAfterAutoWire.add(runnable);
    }

    public void doAfterTreeConstruction(Runnable runnable) {
        jobsAfterTreeConstruction.add(runnable);
    }

    private boolean hasMessage() {
        return !transformationMessages.isEmpty();
    }

    private String getMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("<html><body><h3>Importing pipeline produced the following messages:</h3><ol>");
        transformationMessages.forEach((level, list) -> {
            builder.append("<h4>");
            builder.append(level.name());
            builder.append("</h4>");
            list.forEach(s -> {
                builder.append("<li>");
                builder.append(s);
                builder.append("</li>");
            });
        });
        builder.append("</ol></body></html>");
        return builder.toString();
    }
}
