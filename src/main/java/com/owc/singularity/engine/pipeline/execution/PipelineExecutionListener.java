/*

 *

 *

 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.pipeline.execution;

import java.util.List;
import java.util.Map;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.BreakpointPosition;
import com.owc.singularity.engine.pipeline.PortIdentifier;

/**
 * The pipeline execution listener contains all listener methods to monitor the execution of a
 * pipeline and possibly interact with it. A listener can be added to an excution via the
 * {@link PipelineExecutionBuilder#withExecutionListener(PipelineExecutionListener)} method.<br>
 * Please note that the notification will not happen on the AWTEventThread!
 * 
 * @author Sebastian Land, Hatem Hamad
 */
public interface PipelineExecutionListener {

    /**
     * This is called at the very beginning of a pipeline execution.
     * 
     * @param pipeline
     *            the executed pipeline
     * @param timestamp
     *            start of the execution
     */
    default void onPipelineInit(AbstractPipeline pipeline, long timestamp) {}

    /**
     * This will be called after the execution is initialized, directly before the first operator is
     * executed.
     * 
     * @param pipeline
     *            the executed pipeline
     * @param timestamp
     *            start of the execution of the first operator
     */
    default void onPipelineStart(AbstractPipeline pipeline, long timestamp) {}


    /**
     * Called before an operator is executed. If an operator has both breakpoints before and after,
     * the sequence of listener calls is as follows:
     * <ol>
     * <li>#onBreak with position before</li>
     * <li>onBeforeOperator</li>
     * <li>onAfterOperator</li>
     * <li>{@link #onBreakpointReached(AbstractPipeline, Operation, BreakpointPosition, List, long)}
     * with position after</li>
     * </ol>
     * 
     * @param pipeline
     *            the current pipeline
     * @param operationAddress
     *            the operationAddress that is going to be executed
     * @param inputPortDataMap
     *            The map of objects delivered to the operator's input ports.
     * @param timestamp
     *            when the operator was reached.
     */
    default void onOperationStart(AbstractPipeline pipeline, OperationExecutionAddress operationAddress, Map<PortIdentifier, IOObject> inputPortDataMap,
            long timestamp) {}


    /**
     * Called after an operator was executed.
     * 
     * @param pipeline
     *            the current pipeline
     * @param operationAddress
     *            the operation that is going to be executed
     * @param outputPortDataMap
     *            The map of objects delivered to the operator's output ports.
     * @param timestamp
     *            when the operator was reached.
     */
    default void onOperationFinish(AbstractPipeline pipeline, OperationExecutionAddress operationAddress, Map<PortIdentifier, IOObject> outputPortDataMap,
            long timestamp) {}

    /**
     * Called when the pipeline is resumed from previous execution results.
     * 
     * @param pipeline
     *            the current pipeline
     * @param timestamp
     *            when the pipeline was resumed
     */
    default void onPipelineResume(AbstractPipeline pipeline, long timestamp) {}

    /**
     * Called before the pipeline is stopped via {@link PipelineExecution#stop(AbstractPipeline)}.
     * 
     * @param pipeline
     *            the current pipeline
     * @param timestamp
     *            when the pipeline was stopped
     */
    default void onPipelineStop(AbstractPipeline pipeline, long timestamp) {}

    /**
     * This is called when the pipeline execution stops because of an error
     * 
     * @param pipeline
     *            the current pipeline
     * @param cause
     *            the operator causing the problem or null if unknown
     * @param exception
     *            the exception
     * @param timestamp
     *            when the error was caused.
     */
    default void onPipelineError(AbstractPipeline pipeline, OperationExecutionAddress cause, Exception exception, long timestamp) {}

    /**
     * This is called when the pipeline has finished successfully.
     * 
     * @param pipeline
     *            the current pipeline
     * @param results
     *            the list of ioobjects with the results as connected to the result ports. May
     *            contain null for non-connected ports or ports without data. May be blank when no
     *            result port is defined.
     * @param timestamp
     *            when the pipeline was finished
     */
    default void onPipelineFinish(AbstractPipeline pipeline, Map<PortIdentifier, IOObject> results, long timestamp) {}

}
