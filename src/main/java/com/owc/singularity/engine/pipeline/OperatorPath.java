package com.owc.singularity.engine.pipeline;

import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;

public class OperatorPath {

    private final List<ExecutionUnit> parents;
    private final Operator target;

    public OperatorPath(List<ExecutionUnit> parents, Operator target) {
        this.parents = parents;
        this.target = target;
    }

    public static OperatorPath of(Operator operator) {
        List<ExecutionUnit> parents = new LinkedList<>();
        ExecutionUnit unit = operator.getExecutionUnit();
        while (unit != null) {
            parents.add(unit);
            unit = unit.getEnclosingOperator().getExecutionUnit();
        }
        return new OperatorPath(parents, operator);
    }

    public static OperatorPath fromException(OperatorException error) {
        // search for the root cause operator
        List<ExecutionUnit> parents = new LinkedList<>();
        Operator target = error.getOperator();
        if (target != null) {
            // first, we add the parent units leading to the reporting operator
            ExecutionUnit unit = target.getExecutionUnit();
            while (unit != null) {
                parents.add(unit);
                unit = unit.getEnclosingOperator().getExecutionUnit();
            }
        }
        // next, we dig deep in the operator error stacktrace
        Throwable cause = error.getCause();
        while (cause instanceof OperatorException operatorException) {
            // when we find an operator error cause, this means a deeper target
            Operator deeperTarget = operatorException.getOperator();
            if (deeperTarget != null) {
                target = deeperTarget;
                List<ExecutionUnit> deeperParents = new LinkedList<>();
                ExecutionUnit parentUnit = target.getExecutionUnit();
                while (parentUnit != null) {
                    deeperParents.add(parentUnit);
                    parentUnit = parentUnit.getEnclosingOperator().getExecutionUnit();
                }
                // and we add the parent units leading to the cause operator
                // keep in mind: the parents must be sorted from the closest to the furthest up in
                // the trace,
                // hence we add the closer parents before all others.
                parents.addAll(0, deeperParents);
            }
            cause = cause.getCause();
        }
        return new OperatorPath(parents, target);
    }

    public ExecutionUnit getParent(int level) {
        return parents.get(level);
    }

    public Operator getOperator() {
        return target;
    }

    public List<ExecutionUnit> getParents() {
        return parents;
    }
}
