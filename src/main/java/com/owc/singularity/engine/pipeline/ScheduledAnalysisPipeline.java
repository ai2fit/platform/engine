package com.owc.singularity.engine.pipeline;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.ScheduledAnalysisRootOperator;

public class ScheduledAnalysisPipeline extends AnalysisPipeline {

    public ScheduledAnalysisPipeline() {
        super(new ScheduledAnalysisRootOperator());
    }

    protected ScheduledAnalysisPipeline(ScheduledAnalysisPipeline analysisPipeline) {
        super(analysisPipeline);
    }

    public ScheduledAnalysisPipeline(AbstractRootOperator rootOperator) {
        super(rootOperator);
        if (!(rootOperator instanceof ScheduledAnalysisRootOperator))
            throw new IllegalArgumentException("Only scheduled analysis root operator permitted in scheduled analysis piplines.");
    }

    @Override
    public AbstractPipeline clone() {
        return new ScheduledAnalysisPipeline(this);
    }
}
