package com.owc.singularity.engine.pipeline.io.transformer;

import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * A {@link ProcessReplacementTransformer} to replace all mentions of the deprecated "macros" tag
 * with its updated "variables" tag.
 *
 * @author Hatem Hamad
 */
@CompatibilityTransformerAnnotation(priority = CompatibilityTransformerAnnotation.OPERATOR_INSERTION)
public class MacrosToVariablesTransformer extends ProcessReplacementTransformer {

    private final XPathExpression processExpression;

    public MacrosToVariablesTransformer() throws Exception {
        processExpression = getXpath().compile("//macros");
    }

    @Override
    protected XPathExpression getProcessExpression() {
        return processExpression;
    }

    @Override
    protected void applyOnProcess(Node processNode, Document document, CompatibilityTransformationLog log) throws Exception {
        document.renameNode(processNode, null, "variables");
    }
}
