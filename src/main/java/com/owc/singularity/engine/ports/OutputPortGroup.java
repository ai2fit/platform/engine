package com.owc.singularity.engine.ports;

import java.util.List;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;


public interface OutputPortGroup {

    void deliverToAll(IOObject data);

    void deliver(List<? extends IOObject> inputs);

    void deliverMetaData(List<MetaData> inputMD);

    boolean isAnyPortConnected();

}
