/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports;


import java.util.Collection;
import java.util.List;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * Operators in a process are connected via input and output ports. Whenever an operator generates
 * output (or metadata about output), it is delivered to the connected input port. <br/>
 * This interface defines all behavior and properties common to input and output ports. This is
 * basically names, description etc., as well as adding messages about problems in the process setup
 * and quick fixes.
 * <p>
 * Implementations should extend {@link com.owc.singularity.engine.ports.AbstractPort}.
 * 
 * @see Ports
 * 
 * @author Simon Fischer
 */
public interface Port extends Observable<Port> {

    public static final int CLEAR_META_DATA_ERRORS = 1 << 0;
    public static final int CLEAR_METADATA = 1 << 1;
    public static final int CLEAR_DATA = 1 << 2;
    public static final int CLEAR_SIMPLE_ERRORS = 1 << 3;
    public static final int CLEAR_REAL_METADATA = 1 << 4;
    public static final int CLEAR_ALL = CLEAR_META_DATA_ERRORS | CLEAR_METADATA | CLEAR_DATA | CLEAR_SIMPLE_ERRORS | CLEAR_REAL_METADATA;

    /** Clears all error types. */
    public static final int CLEAR_ALL_ERRORS = CLEAR_META_DATA_ERRORS | CLEAR_SIMPLE_ERRORS;

    /** Clears all meta data, real and inferred. */
    public static final int CLEAR_ALL_METADATA = CLEAR_METADATA | CLEAR_REAL_METADATA;

    /** A human-readable, unique (operator scope) name for the port. */
    public String getName();

    /**
     * Returns the metadata currently assigned to this port.
     * 
     * @deprecated use {@link #getMetaData(Class)} instead
     */
    @Deprecated
    public MetaData getMetaData();

    /**
     * Returns the metadata of the desired class or throws an UserError if available metadata cannot
     * be cast to the desired class. If no metadata is present at all, <code>null</code> is
     * returned.
     * 
     */
    public <T extends MetaData> T getMetaData(Class<T> desiredClass);

    /**
     * This method returns the object simply as IOObject or returns null, if not present.
     * 
     * @return the object or null
     */
    public IOObject getDataOrNull();

    /**
     * This method returns the object of the desired class or throws an UserError if no object is
     * present or cannot be cast to the desiredClass.
     *
     * @throws UserError
     *             if data is missing or of wrong class.
     */
    public <T extends IOObject> T getData(Class<T> desiredClass) throws UserError;

    /**
     * Returns the last object delivered to the connected {@link InputPort} or received from the
     * connected {@link OutputPort}
     * 
     * @throws UserError
     *             If data is not of the requested type.
     */
    public <T extends IOObject> T getDataOrNull(Class<T> desiredClass) throws UserError;


    /**
     * This method returns the object of the desired class or {@link null} if no object is present
     * or it cannot be cast or converted to the desiredClass. Never throws an exception.
     *
     * @return the data cast or converted to the desired class or {@code null}
     * @since 9.4
     */
    public default <T extends IOObject> T getDataAsOrNull(Class<T> desiredClass) {
        // default method for compatibility, overwritten by {@link AbstractPort}
        try {
            return getDataOrNull(desiredClass);
        } catch (UserError userError) {
            return null;
        }
    }

    /** Returns the set of ports to which this port belongs. */
    public Ports<? extends Port> getPorts();

    /** Gets a three letter abbreviation of the port's name. */
    public String getShortName();

    /** Returns a human-readable description of the ports pre/ and post conditions. */
    public String getDescription();

    /** Returns true if connected to another Port. */
    public boolean isConnected();

    /** Report an error in the current process setup. */
    public void addError(MetaDataError metaDataError);

    /** Returns the set of errors added since the last clear errors. */
    public Collection<MetaDataError> getErrors();

    /**
     * Clears data, metadata and errors at this port.
     * 
     * @param clearFlags
     *            disjunction of the CLEAR_XX constants.
     */
    public void clear(int clearFlags);

    /** Returns a sorted list of all quick fixes applicable for this port. */
    public List<QuickFix> collectQuickFixes();

    /** Returns the string "OperatorName.PortName". */
    public String getSpec();

    /**
     * Locks the port so port extenders do not remove the port if disconnected. unlocks it.
     */
    public void lock();

    /**
     * @see #lock()
     */
    public void unlock();

    /**
     * @see #lock()
     */
    public boolean isLocked();

    /** Releases of any hard reference to IOObjects held by this class. */
    void freeMemory();

    public double getPriority();

}
