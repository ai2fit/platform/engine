/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports;


import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.ports.exceptions.CannotConnectPortException;


/**
 * An abstract output port implementation
 *
 * @author Nils Woehler
 *
 */
public class SortedOutputPort extends AbstractPort implements OutputPort {

    private InputPort connectedTo;

    private MetaData metaData;


    SortedOutputPort(Ports<? extends Port> owner, String name, double priority) {
        super(owner, name, priority);
    }


    @Override
    public InputPort getDestination() {
        return connectedTo;
    }


    @Override
    public boolean isConnected() {
        return connectedTo != null;
    }

    @Override
    public boolean shouldAutoConnect() {
        return getPorts().getOwner().getOperator().shouldAutoConnect(this);
    }


    @Override
    public void clear(int clearFlags) {
        super.clear(clearFlags);
        if ((clearFlags & CLEAR_METADATA) > 0) {
            metaData = null;
        }
    }

    protected void assertConnected() throws PortException {
        if (this.connectedTo == null) {
            throw new PortException(this, "Not connected.");
        }
    }

    @Override
    public void connectTo(InputPort inputPort) throws PortException {
        if (this.connectedTo == inputPort) {
            return;
        }
        if (inputPort.getPorts().getOwner().getConnectionContext() != this.getPorts().getOwner().getConnectionContext()) {
            throw new PortException("Cannot connect " + getSpec() + " to " + inputPort.getSpec() + ": ports must be in the same sub-pipelines, but are in "
                    + this.getPorts().getOwner().getConnectionContext().getName() + " and " + inputPort.getPorts().getOwner().getConnectionContext().getName()
                    + ".");
        }
        boolean destConnected = inputPort.isConnected();
        boolean sourceConnected = this.isConnected();
        boolean bothConnected = destConnected && sourceConnected;
        boolean connected = destConnected || sourceConnected;
        if (connected) {
            if (bothConnected) {
                throw new CannotConnectPortException(this, inputPort, this.getDestination(), inputPort.getSource());
            } else if (sourceConnected) {
                throw new CannotConnectPortException(this, inputPort, this.getDestination());
            } else {
                throw new CannotConnectPortException(this, inputPort, inputPort.getSource());
            }
        }
        this.connectedTo = inputPort;
        ((SortedInputPort) inputPort).connect(this);
        fireUpdate(this);
    }

    @Override
    public void disconnect() throws PortException {
        assertConnected();
        ((SortedInputPort) this.connectedTo).connect(null);
        this.connectedTo.receive(null);
        this.connectedTo.receiveMD(null);
        this.connectedTo = null;
        fireUpdate(this);
    }


    @Override
    public void deliver(IOObject object) {
        // registering history of object
        if (object != null) {
            object.setSource(getPorts().getOwner().getOperator(), this);
        }

        // delivering data
        setData(object);
        if (isConnected()) {
            getDestination().receive(object);
        }
    }


    @Override
    public void deliverMD(MetaData md) {
        this.metaData = md;
        if (connectedTo != null) {
            this.connectedTo.receiveMD(md);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends MetaData> T getMetaData(Class<T> desiredClass) {
        MetaData metaData = getMetaData();
        if (metaData != null) {
            checkDesiredClass(metaData, desiredClass);
            return (T) metaData;
        }
        return null;
    }

    @Override
    public MetaData getMetaData() {
        if (metaData == null) {
            // check if we can compute real metadata
            IOObject data = getDataOrNull();
            if (data != null) {
                metaData = MetaData.forIOObject(data);
            }
        }
        // otherwise, return just regular metadata
        return metaData;
    }

    @Override
    public String getDescription() {
        return "";
    }
}
