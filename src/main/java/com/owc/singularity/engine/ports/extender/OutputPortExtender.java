/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports.extender;


import java.util.List;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.OutputPortGroup;
import com.owc.singularity.engine.ports.Ports;


/**
 * Does the same as its superclass but provides also a method to generate a meta data transformation
 * rule that copies from an input to all generated output ports.
 *
 * @author Simon Fischer
 *
 */
public class OutputPortExtender extends AbstractPortExtender<OutputPort> implements OutputPortGroup {

    public OutputPortExtender(String name, double priority, Ports<OutputPort> ports) {
        super(name, priority, ports);
    }

    @Override
    public void deliverToAll(IOObject data) {
        for (OutputPort port : getManagedPorts()) {
            port.deliver(data);
        }
    }

    @Override
    public void deliver(List<? extends IOObject> inputs) {
        int i = 0;
        for (OutputPort port : getManagedPorts()) {
            if (i < inputs.size()) {
                IOObject input = inputs.get(i);
                port.deliver(input);
            }
            i++;
        }
    }

    @Override
    public void deliverMetaData(List<MetaData> inputMD) {
        int i = 0;
        for (OutputPort port : getManagedPorts()) {
            if (i < inputMD.size()) {
                port.deliverMD(inputMD.get(i));
            }
            i++;
        }
    }

    @Override
    public boolean isAnyPortConnected() {
        for (OutputPort outputPort : getManagedPorts())
            if (outputPort.isConnected())
                return true;
        return false;
    }

    @Override
    public String getInputGroupNamePrefix() {
        return null;
    }

    @Override
    public String getOutputGroupNamePrefix() {
        return name + " ";
    }

}
