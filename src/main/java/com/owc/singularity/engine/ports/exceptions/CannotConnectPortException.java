/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports.exceptions;


import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.ports.PortException;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.ProcessLayoutXMLFilter;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawUtils;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * 
 * @author Simon Fischer, Tobias Malbrecht, Nils Woehler
 */
public class CannotConnectPortException extends PortException {

    public static final int HIDE_DELAY = 2500;

    private static final long serialVersionUID = 5242982041478562116L;

    private final OutputPort source;
    private final InputPort dest;

    public CannotConnectPortException(OutputPort source, InputPort dest, InputPort sourceDest, OutputPort destSource) {
        super("Cannot connect " + source.getSpec() + " to " + dest.getSpec());
        this.source = source;
        this.dest = dest;
    }

    public CannotConnectPortException(OutputPort source, InputPort dest, InputPort sourceDest) {
        super("Cannot connect " + source.getSpec() + " to " + dest.getSpec());
        this.source = source;
        this.dest = dest;
    }

    public CannotConnectPortException(OutputPort source, InputPort dest, OutputPort destSource) {
        super("Cannot connect " + source.getSpec() + " to " + dest.getSpec());
        this.source = source;
        this.dest = dest;
    }

    @Override
    public boolean hasRepairOptions() {
        return true;
    }

    @Override
    public void showRepairPopup(Component parent, Point popupLocation) {

        // remember initial state first
        final boolean sourceConnected = source.isConnected();
        final InputPort oldDest = source.getDestination();

        // connect ports in any case
        source.lock();
        dest.lock();
        if (sourceConnected) {
            source.disconnect();
        }
        if (dest.isConnected()) {
            dest.getSource().disconnect();
        }
        source.connectTo(dest);
        source.unlock();
        dest.unlock();

        if (sourceConnected) {
            final JPopupMenu menu = new JPopupMenu();

            // give the user the possibility to create IO multiplier instead
            Action addIOMultiplierActions = new ResourceAction("cannot_connect.option.insert_multiplier") {

                private static final long serialVersionUID = 1L;

                @Override
                public void loggedActionPerformed(ActionEvent e) {
                    final List<Port> toUnlock = new LinkedList<>();
                    try {

                        // lock old destination port
                        toUnlock.add(oldDest);
                        oldDest.lock();

                        // lock source port
                        toUnlock.add(source);
                        source.lock();

                        // lock destination port
                        toUnlock.add(dest);
                        dest.lock();

                        // disconnect source
                        source.disconnect();

                        // create IO multiplier
                        Operator multiplier = OperatorService.createOperator("core:multiply");

                        // connect source to multiplier
                        source.getPorts().getOwner().getConnectionContext().addOperator(multiplier);
                        source.connectTo(multiplier.getInputPorts().getPortByIndex(0));

                        // connect multiplier to old destination and new destination
                        multiplier.getOutputPorts().getPortByIndex(0).connectTo(oldDest);
                        multiplier.getOutputPorts().getPortByIndex(1).connectTo(dest);

                        // set position of new operator to the right of source operator
                        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
                        if (mainProcessPanel == null)
                            return;
                        Rectangle2D rect = ProcessLayoutXMLFilter.lookupOperatorRectangle(source.getPorts().getOwner().getOperator());
                        rect = new Rectangle2D.Double(rect.getX() + rect.getWidth() + 20, rect.getY(), rect.getWidth(),
                                ProcessDrawUtils.calcHeightForOperator(multiplier));
                        ProcessLayoutXMLFilter.setOperatorRectangle(multiplier, rect);
                        // fire operatorMoved to ensure multiplier is displayed correctly
                        mainProcessPanel.getProcessEditor().getModel().fireOperatorsMoved(Collections.singleton(multiplier));
                    } catch (OperatorCreationException e2) {
                        LogService.getRoot().warn("Cannot create multiplier: " + e2.getLocalizedMessage(), e2);
                        SwingTools.showSimpleErrorMessage("Could not create multiplier", "Cannot create multiplier: " + e2.getLocalizedMessage());
                    } finally {
                        for (Port port : toUnlock) {
                            port.unlock();
                        }
                    }
                }

            };

            JMenuItem menuItem = new JMenuItem(addIOMultiplierActions);
            menuItem.setToolTipText(I18N.getGUILabel("cannot_connect.click_to_branch"));

            menu.add(menuItem);

            // show popup
            menu.show(parent, (int) popupLocation.getX(), (int) popupLocation.getY());

            ActionListener hideMenuTimer = new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (menu.isVisible()) {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                menu.setVisible(false);
                            }
                        });
                    }

                }
            };

            final Timer timer = new Timer(HIDE_DELAY, hideMenuTimer);

            menuItem.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    super.mouseEntered(e);
                    timer.stop();
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    super.mouseExited(e);
                    timer.start();
                }
            });

            timer.start();
        }

    }
}
