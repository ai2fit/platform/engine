package com.owc.singularity.engine.ports;

import java.util.List;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.UserError;


public interface InputPortGroup {

    <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered) throws UserError;

    /**
     * Returns a list of data of all input ports. If ordered is true, it will also return null
     * values to preserve order. Otherwise, only non-null objects are included. If unfold is true
     * collections are added as individual objects rather than as a collection. The unfolding is
     * done recursively.
     *
     * @param ordered
     *            If true, not connected ports or ports not having received a result will still be
     *            preserved and insert a null in the list.
     * @param unfold
     *            If true, collections are added as individual objects rather than as a collection.
     * @throws UserError
     */
    <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered, boolean unfold) throws UserError;

    /**
     * Returns a list of non-null metadata of all input ports.
     */
    List<MetaData> getMetaData(boolean unfold);

}
