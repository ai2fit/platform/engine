package com.owc.singularity.engine.ports.extender;


import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.InputPorts;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.OutputPorts;
import com.owc.singularity.engine.tools.function.ObjIntFunction;

public class OneToOneExtender extends OneToManyExtender {

    public static class PortPair {

        private final InputPort inputPort;
        private final OutputPort outputPort;

        public PortPair(InputPort inputPort, OutputPort outputPort) {
            this.inputPort = inputPort;
            this.outputPort = outputPort;
        }

        public InputPort getInputPort() {
            return inputPort;
        }

        public OutputPort getOutputPort() {
            return outputPort;
        }
    }

    public OneToOneExtender(String name, double priority, InputPorts inputPorts, OutputPorts outputPorts, MetaData preconditionMetaData,
            int requiredNumberOfConnectedPorts) {
        super(name, priority, inputPorts, new OutputPorts[] { outputPorts }, preconditionMetaData, requiredNumberOfConnectedPorts);
    }

    public OneToOneExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts outputPorts,
            MetaData preconditionMetaData) {
        super(inputName, outputName, priority, inputPorts, new OutputPorts[] { outputPorts }, preconditionMetaData);
    }

    public OneToOneExtender(String name, double priority, InputPorts inputPorts, OutputPorts outputPorts, MetaData preconditionMetaData) {
        super(name, priority, inputPorts, new OutputPorts[] { outputPorts }, preconditionMetaData);
    }


    public OneToOneExtender(String name, double priority, InputPorts inputPorts, OutputPorts outputPorts) {
        super(name, priority, inputPorts, new OutputPorts[] { outputPorts });
    }

    public OneToOneExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts outputPorts) {
        super(inputName, outputName, priority, inputPorts, new OutputPorts[] { outputPorts }, null);
    }

    public OneToOneExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        super(inputName, outputName, priority, inputPorts, new OutputPorts[] { outputPorts }, preconditionProvider, requiredNumberOfConnectedPorts);

    }

    public OneToOneExtender(String name, double priority, InputPorts inputPorts, OutputPorts outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        super(name, priority, inputPorts, new OutputPorts[] { outputPorts }, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    @Override
    public OneToOneExtender start() {
        super.start();
        return this;
    }

    public List<PortPair> getManagedPairs() {
        LinkedList<PortPair> pairs = new LinkedList<>();
        for (ManyToManyPortRow row : super.getAllPortRows()) {
            pairs.add(new PortPair(row.manyInputPorts.get(0), row.manyOutputPorts.get(0)));
        }

        return pairs;
    }
}
