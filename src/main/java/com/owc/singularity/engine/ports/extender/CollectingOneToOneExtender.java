package com.owc.singularity.engine.ports.extender;


import java.util.Collections;
import java.util.function.Function;

import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.rules.MDTransformationRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.InputPorts;
import com.owc.singularity.engine.ports.OutputPorts;
import com.owc.singularity.engine.ports.Port;

/**
 * This implementation of this class will automatically clear all input ports after data is
 * collected to avoid that the same data is collected twice.
 *
 * @author Sebastian Land
 *
 */
public class CollectingOneToOneExtender extends OneToOneExtender {

    public CollectingOneToOneExtender(String name, double priority, InputPorts inPorts, OutputPorts outPorts) {
        super(name, priority, inPorts, outPorts);
    }

    @SuppressWarnings("unchecked")
    public void collect() {
        synchronized (this) {
            for (PortPair pair : getManagedPairs()) {
                IOObject data = pair.getInputPort().getDataOrNull();
                // if data is null, we don't need to insert into collection
                if (data != null) {
                    IOObject output = pair.getOutputPort().getDataOrNull();
                    if (output != null) {
                        ((IOObjectCollection<IOObject>) output).add(data);
                        pair.getOutputPort().deliver(output); // necessary to trigger updates
                    } else {
                        pair.getOutputPort().deliver(new IOObjectCollection<IOObject>(Collections.singletonList(data)));
                    }
                }
                // clear input ports to avoid collecting the same object twice
                pair.getInputPort().clear(InputPort.CLEAR_DATA);
            }
        }
    }

    /**
     * The generated rule copies all metadata from the generated input ports to all generated output
     * ports and outputs them as collection metadata.
     */
    @Override
    public MDTransformationRule makePassThroughRule() {
        return super.makeTransformationRule(CollectionMetaData::new);
    }

    @Override
    public MDTransformationRule makeTransformationRule(Function<MetaData, MetaData> transformFunction) {
        return super.makeTransformationRule(metaData -> transformFunction.apply(new CollectionMetaData(metaData)));
    }

    /**
     * Resets all output ports by delivering empty collection to all output ports for which the
     * input port is connected and clearing others.
     */
    public void reset() {
        for (PortPair pair : getManagedPairs()) {
            if (pair.getOutputPort().isConnected()) {
                if (pair.getInputPort().isConnected())
                    pair.getOutputPort().deliver(new IOObjectCollection<>());
                else
                    pair.getOutputPort().deliver(null);
            } else {
                pair.getOutputPort().clear(Port.CLEAR_DATA);
            }
        }
    }
}
