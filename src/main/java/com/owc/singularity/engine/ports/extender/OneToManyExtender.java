package com.owc.singularity.engine.ports.extender;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.metadata.preconditions.SimplePrecondition;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.tools.function.ObjIntFunction;

public class OneToManyExtender extends AbstractManyToManyExtender {

    /**
     * A row of ports that can have one input port connected to any number of output ports.
     *
     * @author Sebastian Land
     *
     */
    public static class PortRow {

        private ManyToManyPortRow parentRow;

        public PortRow(ManyToManyPortRow parentRow) {
            this.parentRow = parentRow;
        }

        public boolean isDisconnected() {
            return parentRow.isDisconnected();
        }

        public InputPort getInputPort() {
            return parentRow.manyInputPorts.get(0);
        }

        public List<OutputPort> getOutputPorts() {
            return parentRow.manyOutputPorts;
        }
    }


    public OneToManyExtender(String name, double priority, InputPorts inputPorts, OutputPorts[] multiOutputPorts) {
        this(name, priority, inputPorts, multiOutputPorts, (MetaData) null);
    }

    public OneToManyExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts[] multiOutputPorts,
            MetaData preconditionMetaData) {
        this(inputName, outputName, priority, inputPorts, multiOutputPorts, preconditionMetaData, 0);
    }

    public OneToManyExtender(String name, double priority, InputPorts inputPorts, OutputPorts[] multiOutputPorts, MetaData preconditionMetaData) {
        this(name, priority, inputPorts, multiOutputPorts, preconditionMetaData, 0);
    }

    public OneToManyExtender(String name, double priority, InputPorts inputPorts, OutputPorts[] multiOutputPorts, MetaData preconditionMetaData,
            int requiredNumberOfConnectedPorts) {
        this(name, name, priority, inputPorts, multiOutputPorts,
                (preconditionMetaData != null) ? (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts) : null,
                requiredNumberOfConnectedPorts);
    }

    public OneToManyExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts[] multiOutputPorts,
            MetaData preconditionMetaData, int requiredNumberOfConnectedPorts) {
        this(inputName, outputName, priority, inputPorts, multiOutputPorts,
                (preconditionMetaData != null) ? (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts) : null,
                requiredNumberOfConnectedPorts);
    }

    public OneToManyExtender(String name, double priority, InputPorts inputPorts, OutputPorts[] outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        this(name, name, priority, inputPorts, outputPorts, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    public OneToManyExtender(String inputName, String outputName, double priority, InputPorts inputPorts, OutputPorts[] outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        super(inputName, outputName, priority, new InputPorts[] { inputPorts }, outputPorts, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    /**
     * This method returns the data that is delivered to the input set.
     *
     * @param desiredClass
     * @return
     * @throws UserError
     */
    public <T extends IOObject> List<T> getData(Class<T> desiredClass) throws UserError {
        return super.getData(desiredClass, 0);
    }

    /**
     * Returns the data that has been forwarded to the input ports as ordered list. If a port did
     * not receive any data, the list will contain null unlike the {@link #getData(Class)} method of
     * the original PortPairExtender.
     */
    public <T extends IOObject> List<T> getDataOrNull(Class<T> desiredClass) throws UserError {
        return super.getDataOrNull(desiredClass, 0);
    }

    public void passDataThrough() {
        super.passDataThrough(0);
    }

    public List<PortRow> getPortRows() {
        return super.getAllPortRows().stream().map(PortRow::new).collect(Collectors.toList());
    }

    @Override
    public void registerOutputPorts(OutputPorts additionalOutputPorts) {
        super.registerOutputPorts(additionalOutputPorts);
    }

    @Override
    public void unregisterOutputPorts(OutputPorts outputPorts) {
        super.unregisterOutputPorts(outputPorts);
    }

    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered) throws UserError {
        if (ordered)
            return getDataOrNull(desiredClass);
        return getData(desiredClass);
    }

    @Override
    public List<MetaData> getMetaData(boolean unfold) {
        List<MetaData> results = new LinkedList<>();
        for (PortRow portRow : getPortRows()) {
            MetaData data = portRow.getInputPort().getMetaData();
            if (data != null) {
                if (unfold && data instanceof CollectionMetaData) {
                    results.add(((CollectionMetaData) data).getElementMetaDataRecursive());
                } else {
                    results.add(data);
                }
            }
        }
        return results;
    }

    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered, boolean unfold) throws UserError {
        List<T> results = new ArrayList<T>();
        for (PortRow portRow : getPortRows()) {
            IOObject data = portRow.getInputPort().getDataOrNull();
            if (data != null) {
                if (unfold && data instanceof IOObjectCollection) {
                    unfold((IOObjectCollection<?>) data, results, desiredClass, portRow.getInputPort());
                } else if (desiredClass.isInstance(data)) {
                    results.add(desiredClass.cast(data));
                } else {
                    throw new UserError(portRow.getInputPort().getPorts().getOwner().getOperator(), 156, IOObjectService.getName(data.getClass()),
                            portRow.getInputPort().getName(), IOObjectService.getName(desiredClass));
                }
            } else if (ordered) {
                results.add(null);
            }
        }
        return results;
    }

    private <T extends IOObject> void unfold(IOObjectCollection<?> collection, List<T> results, Class<T> desiredClass, Port port) throws UserError {
        for (IOObject obj : collection.getObjects()) {
            if (obj instanceof IOObjectCollection) {
                unfold((IOObjectCollection<?>) obj, results, desiredClass, port);
            } else if (desiredClass.isInstance(obj)) {
                results.add(desiredClass.cast(obj));
            } else {
                throw new UserError(port.getPorts().getOwner().getOperator(), 156, IOObjectService.getName(obj.getClass()), port.getName(),
                        IOObjectService.getName(desiredClass));
            }
        }
    }
}
