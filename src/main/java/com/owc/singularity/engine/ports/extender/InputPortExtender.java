/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports.extender;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.InputMissingMetaDataError;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.CollectionPrecondition;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.InputPortGroup;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.ports.Ports;
import com.owc.singularity.engine.tools.function.ObjIntFunction;


/**
 * Port Extender that can be used if an operator needs to receive an arbitrary number of input
 * objects. Delivered Collections might be unfolded automatically, so that the contained objects are
 * presented as if they would have been forwarded one by one to the operator. This port extender can
 * be configured to throw errors if the wrong type or to few inputs are connected.
 * 
 * @author Simon Fischer, Sebastian Land
 */
public class InputPortExtender extends AbstractPortExtender<InputPort> implements InputPortGroup {

    private ObjIntFunction<InputPort, Precondition> preconditionProvider;

    public InputPortExtender(String name, double priority, Ports<InputPort> ports) {
        this(name, priority, ports, null, 0);
    }

    public InputPortExtender(String name, double priority, Ports<InputPort> ports, MetaData desiredMetaData, boolean firstIsMandatory) {
        this(name, priority, ports, desiredMetaData, firstIsMandatory ? 1 : 0);
    }

    public InputPortExtender(String name, double priority, Ports<InputPort> ports, MetaData desiredMetaData, int numberOfMandatory) {
        this(name, priority, ports, (port, portIndex) -> makePrecondition(port, portIndex, desiredMetaData, numberOfMandatory));
        ensureMinimumNumberOfPorts(numberOfMandatory);
    }

    public InputPortExtender(String name, double priority, Ports<InputPort> ports, ObjIntFunction<InputPort, Precondition> preconditionProvider) {
        super(name, priority, ports);
        this.preconditionProvider = preconditionProvider;
    }

    @Override
    protected InputPort createPort() {
        InputPort port = super.createPort();
        if (preconditionProvider != null) {
            Precondition precondition = preconditionProvider.accept(port, runningId - 1);
            if (precondition != null) {
                port.addPrecondition(precondition);
            }
        }
        return port;
    }

    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered) throws UserError {
        return this.getData(desiredClass, ordered, false);
    }

    /**
     * Returns a list of data of all input ports. If ordered is true, it will also return null
     * values to preserve order. Otherwise, only non-null objects are included. If unfold is true
     * collections are added as individual objects rather than as a collection unless the requested
     * class is a subclass of a collection itself and the specific object is already of requested
     * type. The unfolding is done recursively.
     *
     * @param ordered
     *            If true, not connected ports or ports not having received a result will still be
     *            preserved and insert a null in the list.
     * @param unfold
     *            If true, collections are added as individual objects rather than as a collection.
     * @throws UserError
     */
    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered, boolean unfold) throws UserError {
        List<T> results = new ArrayList<T>();
        for (InputPort port : getManagedPorts()) {
            IOObject data = port.getDataOrNull();
            if (data != null) {
                if (unfold && data instanceof IOObjectCollection
                        && !(IOObjectCollection.class.isAssignableFrom(desiredClass) && desiredClass.isAssignableFrom(data.getClass()))) {
                    unfold((IOObjectCollection<?>) data, results, desiredClass, port);
                } else if (desiredClass.isInstance(data)) {
                    results.add(desiredClass.cast(data));
                } else {
                    throw new UserError(getPorts().getOwner().getOperator(), 156, IOObjectService.getName(data.getClass()), port.getName(),
                            IOObjectService.getName(desiredClass));
                }
            } else if (ordered) {
                results.add(null);
            }
        }
        return results;
    }

    private <T extends IOObject> void unfold(IOObjectCollection<?> collection, List<T> results, Class<T> desiredClass, Port port) throws UserError {
        for (IOObject obj : collection.getObjects()) {
            if (obj instanceof IOObjectCollection && !desiredClass.isAssignableFrom(desiredClass)) {
                unfold((IOObjectCollection<?>) obj, results, desiredClass, port);
            } else if (desiredClass.isInstance(obj)) {
                results.add(desiredClass.cast(obj));
            } else {
                throw new UserError(getPorts().getOwner().getOperator(), 156, IOObjectService.getName(obj.getClass()), port.getName(),
                        IOObjectService.getName(desiredClass));
            }
        }
    }

    /**
     * Returns a list of non-null metadata of all input ports.
     */
    @Override
    public List<MetaData> getMetaData(boolean unfold) {
        List<MetaData> results = new LinkedList<MetaData>();
        for (InputPort port : getManagedPorts()) {
            MetaData data = port.getMetaData();
            if (data != null) {
                if (unfold && data instanceof CollectionMetaData) {
                    results.add(((CollectionMetaData) data).getElementMetaDataRecursive());
                } else {
                    results.add(data);
                }
            }
        }
        return results;
    }

    private static Precondition makePrecondition(final InputPort singlePort, int portIndex, MetaData desiredMetaData, int numberOfMandatory) {
        if (desiredMetaData != null) {
            return new CollectionPrecondition(new Precondition() {

                @Override
                public void check(OperationDescriptionContext context, MetaData metaData) {
                    boolean isMandatory = (portIndex < numberOfMandatory);
                    // checking if some of the ports received collection
                    for (int i = portIndex; i >= 0; i--) {
                        MetaData portMetaData = singlePort.getMetaData();
                        if (portMetaData != null) {
                            isMandatory &= !portMetaData.isCompatible(new CollectionMetaData(desiredMetaData));
                        }
                    }

                    // if not: throw error
                    if (metaData == null && isMandatory) {
                        singlePort.addError(new InputMissingMetaDataError(singlePort, desiredMetaData.getObjectClass(), null));
                    }
                    if (metaData != null) {
                        if (!desiredMetaData.isCompatible(metaData)) {
                            singlePort.addError(new InputMissingMetaDataError(singlePort, desiredMetaData.getObjectClass(), metaData.getObjectClass()));
                        }
                    }
                }

                @Override
                public String getDescription() {
                    return "requires " + ((numberOfMandatory > 0) ? " at least " + numberOfMandatory + " " : "") + desiredMetaData.getDescription();
                }

                @Override
                public MetaData getExpectedMetaData() {
                    return desiredMetaData;
                }

                @Override
                public boolean isCompatible(MetaData input) {
                    return desiredMetaData.isCompatible(input);
                }

            });
        }
        return null;
    }

    @Override
    public String getInputGroupNamePrefix() {
        return name + " ";
    }

    @Override
    public String getOutputGroupNamePrefix() {
        return null;
    }

}
