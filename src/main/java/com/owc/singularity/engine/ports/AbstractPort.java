/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.ports;


import java.util.*;
import java.util.function.Predicate;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.metadata.MetaDataErrorQuickFixFilter;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.PortUserError;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.tools.AbstractObservable;
import com.owc.singularity.studio.gui.editor.quickfix.BlacklistedOperatorQuickFixFilter;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * Implemented by keeping a weak reference to the data that can be cleared at any time by the
 * garbage collector.
 * <p>
 * In addition to the week reference, this class also keeps a hard reference to the data, freeing it
 * when calling {@link #freeMemory()}.
 *
 * @author Simon Fischer
 *
 */
public abstract class AbstractPort extends AbstractObservable<Port> implements Port {


    /** Filter used to sort out blacklisted operator insertion quick fixes */
    private static final Predicate<? super QuickFix> BLACKLISTED_OPERATOR_FILTER = new BlacklistedOperatorQuickFixFilter();

    private final List<MetaDataError> errorList = new LinkedList<>();
    private final Ports<? extends Port> ports;
    private final double priority;

    private String name;

    private IOObject hardDataReference;

    private boolean locked = false;

    protected AbstractPort(Ports<? extends Port> ports, String name, double priority) {
        this.name = name;
        this.ports = ports;
        this.priority = priority;
    }

    protected final void setData(IOObject object) {
        this.hardDataReference = object;
    }

    @Override
    public IOObject getDataOrNull() {
        return hardDataReference;
    }

    @Override
    public <T extends IOObject> T getDataAsOrNull(Class<T> desiredClass) {
        try {
            return getData(desiredClass, true, true);
        } catch (UserError userError) {
            // cannot happen
            return null;
        }
    }

    @Override
    public <T extends IOObject> T getData(Class<T> desiredClass) throws UserError {
        return getData(desiredClass, false, false);
    }

    @Override
    public <T extends IOObject> T getDataOrNull(Class<T> desiredClass) throws UserError {
        return getData(desiredClass, true, false);
    }

    /**
     * This method returns the object of the desired class or throws an {@link UserError} if object
     * cannot be cast to the desiredClass. Dependening on <em>allowNull</em> either returns a
     * {@code null} value or throws a {@link UserError}
     *
     * @param desiredClass
     *            the super class of desired type of data
     * @param allowNull
     *            if {@code null} value should be returned or throw an error
     * @param nullForOther
     *            if it should return null in case the existing class cannot be casted or converted
     *            to the desired class
     * @throws UserError
     *             if an error occurs, can only happen if either {@code allowNull} or
     *             {@code nullForOther} is false
     */
    private <T extends IOObject> T getData(Class<T> desiredClass, boolean allowNull, boolean nullForOther) throws UserError {
        IOObject data = getDataOrNull();
        if (data == null) {
            if (allowNull) {
                return null;
            }
            throw new PortUserError(this, 149, getSpec() + (isConnected() ? " (connected)" : " (disconnected)"));
        } else if (desiredClass.isAssignableFrom(data.getClass())) {
            return desiredClass.cast(data);
        } else if (!nullForOther) {
            PortUserError error = new PortUserError(this, 156, IOObjectService.getName(data.getClass()), this.getName(), IOObjectService.getName(desiredClass));
            error.setExpectedType(desiredClass);
            error.setActualType(data.getClass());
            throw error;
        } else {
            return null;
        }
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getSpec();
    }

    @Override
    public Ports<? extends Port> getPorts() {
        return ports;
    }

    @Override
    public String getShortName() {
        if (name.length() > 3) {
            return name.substring(0, 3);
        } else {
            return name;
        }
    }

    /** Don't use this method. Use {@link Ports#renamePort(Port,String)}. */
    protected void setName(String newName) {
        this.name = newName;
    }

    @Override
    public void addError(MetaDataError metaDataError) {
        errorList.add(new MetaDataErrorQuickFixFilter(metaDataError, BLACKLISTED_OPERATOR_FILTER));
    }

    @Override
    public Collection<MetaDataError> getErrors() {
        return Collections.unmodifiableCollection(errorList);
    }

    @Override
    public void clear(int clearFlags) {
        if ((clearFlags & CLEAR_META_DATA_ERRORS) > 0) {
            this.errorList.clear();
        }
        if ((clearFlags & CLEAR_DATA) > 0) {
            this.hardDataReference = null;
        }
    }

    /**
     * Checks whether the desired class is assignable from provided metadata object class.
     */
    protected void checkDesiredClass(MetaData obj, Class<? extends MetaData> desiredClass) {
        if (!desiredClass.isAssignableFrom(obj.getClass())) {
            throw new ClassCastException("Cannot cast MetaData of class " + obj.getClass().getName() + " to " + desiredClass.getName());
        }
    }

    @Override
    public List<QuickFix> collectQuickFixes() {
        List<QuickFix> fixes = new LinkedList<>();
        for (MetaDataError error : getErrors()) {
            fixes.addAll(error.getQuickFixes());
        }
        Collections.sort(fixes);
        return fixes;
    }

    @Override
    public String getSpec() {
        if (getPorts() != null) {
            return getPorts().getOwner().getOperator().getName() + "." + getName();
        } else {
            return "DUMMY." + getName();
        }
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public void unlock() {
        this.locked = false;
    }

    @Override
    public void lock() {
        this.locked = true;
    }

    /** Releases of the hard reference. */
    @Override
    public void freeMemory() {
        this.hardDataReference = null;
    }

    @Override
    public double getPriority() {
        return priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ports);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractPort other = (AbstractPort) obj;
        return Objects.equals(name, other.name) && Objects.equals(ports, other.ports);
    }
}
