package com.owc.singularity.engine.ports.extender;


import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.metadata.preconditions.SimplePrecondition;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.InputPorts;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.OutputPorts;
import com.owc.singularity.engine.tools.function.ObjIntFunction;

public class ManyToOneExtender extends AbstractManyToManyExtender {

    /**
     * A row of ports that can have one input port connected to any number of output ports.
     *
     * @author Sebastian Land
     *
     */
    public static class PortRow {

        private ManyToManyPortRow parentRow;

        public PortRow(ManyToManyPortRow parentRow) {
            this.parentRow = parentRow;
        }

        public boolean isDisconnected() {
            return parentRow.isDisconnected();
        }

        public OutputPort getOutputPort() {
            return parentRow.manyOutputPorts.get(0);
        }

        public List<InputPort> getInputPorts() {
            return parentRow.manyInputPorts;
        }
    }


    public ManyToOneExtender(String name, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts) {
        this(name, priority, manyInputPorts, outputPorts, (MetaData) null);
    }

    public ManyToOneExtender(String inputName, String outputName, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts,
            MetaData preconditionMetaData) {
        this(inputName, outputName, priority, manyInputPorts, outputPorts, preconditionMetaData, 0);
    }

    public ManyToOneExtender(String name, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts, MetaData preconditionMetaData) {
        this(name, priority, manyInputPorts, outputPorts, preconditionMetaData, 0);
    }

    public ManyToOneExtender(String name, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts, MetaData preconditionMetaData,
            int requiredNumberOfConnectedPorts) {
        this(name, name, priority, manyInputPorts, outputPorts,
                (preconditionMetaData != null) ? (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts) : null,
                requiredNumberOfConnectedPorts);
    }

    public ManyToOneExtender(String inputName, String outputName, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts,
            MetaData preconditionMetaData, int requiredNumberOfConnectedPorts) {
        this(inputName, outputName, priority, manyInputPorts, outputPorts,
                (preconditionMetaData != null) ? (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts) : null,
                requiredNumberOfConnectedPorts);
    }

    public ManyToOneExtender(String name, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        this(name, name, priority, manyInputPorts, outputPorts, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    public ManyToOneExtender(String inputName, String outputName, double priority, InputPorts[] manyInputPorts, OutputPorts outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        super(inputName, outputName, priority, manyInputPorts, new OutputPorts[] { outputPorts }, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    public List<PortRow> getPortRows() {
        return super.getAllPortRows().stream().map(PortRow::new).collect(Collectors.toList());
    }

    @Override
    public void passDataThrough(int inputIndex) {
        super.passDataThrough(inputIndex);
    }

    @Override
    public void unregisterInputPorts(InputPorts inputPorts) {
        super.unregisterInputPorts(inputPorts);
    }

    @Override
    public void registerInputPorts(InputPorts additionalInputPorts) {
        super.registerInputPorts(additionalInputPorts);
    }

    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered) throws UserError {
        throw new UnsupportedOperationException("Not supported by this extender. Use specific methods instead to indicate which Inputs to address.");
    }

    @Override
    public <T extends IOObject> List<T> getData(Class<T> desiredClass, boolean ordered, boolean unfold) throws UserError {
        throw new UnsupportedOperationException("Not supported by this extender. Use specific methods instead to indicate which Inputs to address.");
    }

    @Override
    public List<MetaData> getMetaData(boolean unfold) {
        List<MetaData> results = new LinkedList<>();
        for (PortRow portRow : getPortRows()) {
            MetaData data = portRow.getInputPorts().get(0).getMetaData();
            if (data != null) {
                if (unfold && data instanceof CollectionMetaData) {
                    results.add(((CollectionMetaData) data).getElementMetaDataRecursive());
                } else {
                    results.add(data);
                }
            }
        }
        return results;
    }
}
