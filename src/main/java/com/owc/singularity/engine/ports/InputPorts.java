/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports;


import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;


/**
 * 
 * @author Simon Fischer
 * 
 */
public interface InputPorts extends Ports<InputPort> {

    /**
     * Checks all preconditions at the ports.
     * 
     * @param operationDescriptionContext
     */
    public void checkPreconditions(OperationDescriptionContext operationDescriptionContext);

    /**
     * Creates an input port with a simple precondition requiring input of type clazz.
     * 
     * @param portPriority
     *            used to order ports in descending order
     */
    public InputPort createPort(String name, double portPriority, Class<? extends IOObject> clazz);

    /**
     * Creates an input port with a simple precondition requiring input with given metadata.
     * 
     * @param portPriority
     *            used to order ports in descending order
     */
    public InputPort createPort(String name, double portPriority, MetaData metaData);

}
