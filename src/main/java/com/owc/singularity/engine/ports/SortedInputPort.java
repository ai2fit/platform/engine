/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports;


import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.SimpleMetaDataError;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;


/**
 * An abstract implementation for InputPorts. It handles metadata, metadata changes, preconditions
 * and the connection to the source output port.
 *
 * @author Nils Woehler
 *
 */
public class SortedInputPort extends AbstractPort implements InputPort {

    private final Collection<MetaDataChangeListener> metaDataChangeListeners = new LinkedList<>();

    private final Collection<Precondition> preconditions = new LinkedList<>();

    private MetaData metaData;

    private MetaData realMetaData;

    /** The port to which this port is connected. */
    private OutputPort sourceOutputPort;

    SortedInputPort(Ports<? extends Port> owner, String name, double priority) {
        super(owner, name, priority);
    }

    @Override
    public void clear(int clearFlags) {
        super.clear(clearFlags);
        if ((clearFlags & CLEAR_REAL_METADATA) > 0) {
            realMetaData = null;
            informListenersOfChange(null);
        }
        if ((clearFlags & CLEAR_METADATA) > 0) {
            this.metaData = null;
            informListenersOfChange(null);
        }
    }


    public void connect(OutputPort outputPort) {
        this.sourceOutputPort = outputPort;
        fireUpdate(this);
    }

    @Override
    public OutputPort getSource() {
        return sourceOutputPort;
    }


    @Override
    public boolean isConnected() {
        return sourceOutputPort != null;
    }


    @Override
    public void receive(IOObject object) {
        setData(object);
        this.realMetaData = null;
    }

    @Override
    public MetaData getMetaData() {
        if (realMetaData != null)
            return realMetaData.clone();
        else {
            // check if we can compute real metadata
            IOObject data = getDataOrNull();
            if (data != null) {
                this.realMetaData = MetaData.forIOObject(data);
                return realMetaData.clone();
            }
        }
        // otherwise, return just regular metadata
        if (metaData != null)
            return metaData.clone();
        return null;
    }

    @Override
    public void receiveMD(MetaData metaData) {
        this.metaData = metaData;
        informListenersOfChange(metaData);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends MetaData> T getMetaData(Class<T> desiredClass) {
        MetaData metaData = getMetaData();
        if (metaData != null) {
            checkDesiredClass(metaData, desiredClass);
            return (T) metaData;
        }
        return null;
    }

    @Override
    public void addPrecondition(Precondition precondition) {
        preconditions.add(precondition);

    }

    @Override
    public Collection<Precondition> getAllPreconditions() {
        return Collections.unmodifiableCollection(preconditions);
    }

    @Override
    public void checkPreconditions(OperationDescriptionContext operationDescriptionContext) {
        MetaData metaData = getMetaData();
        for (Precondition precondition : preconditions) {
            try {
                precondition.check(operationDescriptionContext, metaData);
            } catch (Exception e) {
                getPorts().getOwner().getOperator().getLogger().warn("Error checking preconditions at " + getSpec() + ": " + e, e);
                this.addError(new SimpleMetaDataError(Severity.WARNING, this, "exception_checking_precondition", e.toString()));
            }
        }
    }

    @Override
    public String getPreconditionDescription() {
        StringBuilder buf = new StringBuilder();
        buf.append(getName());
        buf.append(": ");
        for (Precondition precondition : preconditions) {
            buf.append(precondition.getDescription());
        }
        return buf.toString();
    }

    @Override
    public boolean isInputCompatible(MetaData input) {
        for (Precondition precondition : preconditions) {
            if (!precondition.isCompatible(input)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public synchronized void registerMetaDataChangeListener(MetaDataChangeListener listener) {
        metaDataChangeListeners.add(listener);
    }

    @Override
    public synchronized void removeMetaDataChangeListener(MetaDataChangeListener listener) {
        metaDataChangeListeners.remove(listener);
    }

    protected synchronized void informListenersOfChange(MetaData metaData) {
        for (MetaDataChangeListener listener : metaDataChangeListeners) {
            listener.informMetaDataChanged(metaData);
        }
    }


    @Override
    public String getDescription() {
        StringBuilder b = new StringBuilder();
        boolean first = true;
        for (Precondition precondition : preconditions) {
            if (!first) {
                b.append(", ");
            } else {
                first = false;
            }
            b.append(precondition.getDescription());
        }
        return b.toString();
    }


}
