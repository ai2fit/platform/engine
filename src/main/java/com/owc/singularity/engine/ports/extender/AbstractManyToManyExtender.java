package com.owc.singularity.engine.ports.extender;


import java.util.*;
import java.util.function.Function;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.metadata.preconditions.SimplePrecondition;
import com.owc.singularity.engine.metadata.rules.MDTransformationRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.*;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.engine.tools.function.ObjIntFunction;

public abstract class AbstractManyToManyExtender implements PortExtender, OutputPortGroup, InputPortGroup {

    private static final int META_DATA_SOURCE_INDEX = 0;

    /**
     * A row of ports that can have one input port connected to any number of output ports.
     *
     * @author Sebastian Land
     *
     */
    public static class ManyToManyPortRow {

        protected ArrayList<InputPort> manyInputPorts;
        protected ArrayList<OutputPort> manyOutputPorts;

        public ManyToManyPortRow(ArrayList<InputPort> manyInputPorts, ArrayList<OutputPort> manyOutputPorts) {
            this.manyInputPorts = manyInputPorts;
            this.manyOutputPorts = manyOutputPorts;

        }

        public boolean isDisconnected() {
            for (Port port : manyInputPorts) {
                if (port.isConnected() || port.isLocked()) {
                    return false;
                }
            }
            for (Port port : manyOutputPorts) {
                if (port.isConnected() || port.isLocked()) {
                    return false;
                }
            }
            return true;
        }

        public List<InputPort> getInputPorts() {
            return manyInputPorts;
        }

        public List<OutputPort> getOutputPorts() {
            return manyOutputPorts;
        }
    }

    private final String inputName;
    private final String outputName;
    private final ArrayList<Ports<InputPort>> manyInputPorts;
    private final ArrayList<Ports<OutputPort>> manyOutputPorts;
    protected int minNumberOfPorts = 0;
    protected int maxNumberOfPorts = -1;
    protected int requiredNumberOfConnectedPorts;
    protected int numberOfFreePorts = 1;

    protected final List<ManyToManyPortRow> managedRows = new LinkedList<>();

    private boolean isChanging = false;

    private final Observer<Port> portObserver = new Observer<Port>() {

        @Override
        public void update(Observable<Port> observable, Port arg) {
            updatePorts();
        }
    };
    private final Observer<Operator> operatorObserver = new Observer<Operator>() {

        @Override
        public void update(Observable<Operator> observable, Operator arg) {
            updatePorts();
        }
    };
    private ObjIntFunction<InputPort, Precondition> preconditionProvider;
    private double priority;


    public AbstractManyToManyExtender(String name, double priority, InputPorts[] inputPorts, OutputPorts[] multiOutputPorts) {
        this(name, priority, inputPorts, multiOutputPorts, (MetaData) null);
    }

    public AbstractManyToManyExtender(String inputName, String outputName, double priority, InputPorts[] inputPorts, OutputPorts[] multiOutputPorts,
            MetaData preconditionMetaData) {
        this(inputName, outputName, priority, inputPorts, multiOutputPorts, preconditionMetaData, 0);
    }

    public AbstractManyToManyExtender(String name, double priority, InputPorts[] inputPorts, OutputPorts[] multiOutputPorts, MetaData preconditionMetaData) {
        this(name, priority, inputPorts, multiOutputPorts, preconditionMetaData, 0);
    }

    public AbstractManyToManyExtender(String name, double priority, InputPorts[] inputPorts, OutputPorts[] multiOutputPorts, MetaData preconditionMetaData,
            int requiredNumberOfConnectedPorts) {
        this(name, name, priority, inputPorts, multiOutputPorts,
                (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts), requiredNumberOfConnectedPorts);
    }

    public AbstractManyToManyExtender(String inputName, String outputName, double priority, InputPorts[] inputPorts, OutputPorts[] multiOutputPorts,
            MetaData preconditionMetaData, int requiredNumberOfConnectedPorts) {
        this(inputName, outputName, priority, inputPorts, multiOutputPorts,
                (port, i) -> new SimplePrecondition(port, preconditionMetaData, i <= requiredNumberOfConnectedPorts), requiredNumberOfConnectedPorts);
    }

    public AbstractManyToManyExtender(String name, double priority, InputPorts[] inputPorts, OutputPorts[] outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        this(name, name, priority, inputPorts, outputPorts, preconditionProvider, requiredNumberOfConnectedPorts);
    }

    public AbstractManyToManyExtender(String inputName, String outputName, double priority, InputPorts[] inputPorts, OutputPorts[] outputPorts,
            ObjIntFunction<InputPort, Precondition> preconditionProvider, int requiredNumberOfConnectedPorts) {
        this.inputName = inputName;
        this.outputName = outputName;
        this.priority = priority;

        this.preconditionProvider = preconditionProvider;
        this.requiredNumberOfConnectedPorts = requiredNumberOfConnectedPorts;
        manyOutputPorts = new ArrayList<>(Arrays.asList(outputPorts));
        manyInputPorts = new ArrayList<>(Arrays.asList(inputPorts));
        for (InputPorts ports : inputPorts)
            ports.registerPortExtender(this);
        for (Ports<OutputPort> ports : outputPorts) {
            ports.registerPortExtender(this);
        }
    }

    /**
     * This method returns the data that is delivered to the input set.
     *
     * @param desiredClass
     * @return
     * @throws UserError
     */
    protected <T extends IOObject> List<T> getData(Class<T> desiredClass, int inputIndex) throws UserError {
        List<T> results = new LinkedList<>();
        for (ManyToManyPortRow row : managedRows) {
            T data = row.getInputPorts().get(inputIndex).getDataOrNull(desiredClass);
            if (data != null)
                results.add(data);
        }
        return results;
    }

    /**
     * Returns the data that has been forwarded to the input ports as ordered list. If a port did
     * not receive any data, the list will contain null unlike the
     * {@link #getData(Class, boolean)}method of the original PortPairExtender.
     */
    protected <T extends IOObject> List<T> getDataOrNull(Class<T> desiredClass, int inputIndex) throws UserError {
        List<T> results = new LinkedList<>();
        for (ManyToManyPortRow row : managedRows) {
            T data = row.getInputPorts().get(inputIndex).getDataOrNull(desiredClass);
            results.add(data);
        }
        return results;
    }

    /**
     * Returns the metadata that has been forwarded to the input ports as ordered list. If a port
     * did not receive any metadata, the list will contain null.
     */
    public List<MetaData> getMetaDataOrNull() throws UserError {
        List<MetaData> results = new LinkedList<>();
        for (ManyToManyPortRow row : managedRows) {
            results.add(row.getInputPorts().get(META_DATA_SOURCE_INDEX).getMetaData());
        }
        return results;
    }

    /**
     * Returns the metadata that has been forwarded to the input ports as ordered list. If a port
     * did not receive any metadata, the list will contain null.
     * 
     * @throws IncompatibleMDClassException
     */
    public <T extends MetaData> List<MetaData> getMetaDataOrNull(Class<T> forObjectClass) throws IncompatibleMDClassException {
        List<MetaData> results = new LinkedList<>();
        for (ManyToManyPortRow row : managedRows) {
            results.add(row.getInputPorts().get(META_DATA_SOURCE_INDEX).getMetaData(forObjectClass));
        }
        return results;
    }

    /** Creates an initial port and starts to listen. */
    public AbstractManyToManyExtender start() {
        updatePorts();
        boolean isFirst = true;
        for (Ports<InputPort> ports : manyInputPorts) {
            if (isFirst) {
                ports.getOwner().getOperator().addObserver(operatorObserver, false);
                isFirst = false;
            }
            ports.addObserver(portObserver, false);
        }
        for (Ports<OutputPort> ports : manyOutputPorts) {
            ports.addObserver(portObserver, false);
        }
        return this;
    }

    /**
     * This will deliver all objects of the list to all the available ports. It will output the
     * minimum of the size of the list or the ports.
     *
     * @param objects
     */
    @Override
    public void deliver(List<? extends IOObject> objects) {
        int i = 0;
        for (IOObject object : objects) {
            if (managedRows.size() > i) {
                if (object != null) {
                    for (OutputPort port : managedRows.get(i).manyOutputPorts) {
                        port.deliver(object);
                    }
                } else {
                    for (OutputPort port : managedRows.get(i).manyOutputPorts) {
                        port.clear(Port.CLEAR_DATA);
                    }
                }
            }
            i++;
        }
    }

    @Override
    public void deliverToAll(IOObject data) {
        for (ManyToManyPortRow managedRow : managedRows)
            for (OutputPort port : managedRow.manyOutputPorts) {
                port.deliver(data);
            }
    }

    @Override
    public void deliverMetaData(List<MetaData> objectMDs) {
        int i = 0;
        for (MetaData objectMD : objectMDs) {
            if (managedRows.size() > i) {
                if (objectMD != null) {
                    for (OutputPort port : managedRows.get(i).manyOutputPorts) {
                        port.deliverMD(objectMD);
                    }
                } else {
                    for (OutputPort port : managedRows.get(i).manyOutputPorts) {
                        port.clear(Port.CLEAR_DATA);
                    }
                }
            }
            i++;
        }

    }

    protected void passDataThrough(int inputIndex) {
        for (ManyToManyPortRow row : managedRows) {
            IOObject data = row.manyInputPorts.get(inputIndex).getDataOrNull();
            for (OutputPort out : row.manyOutputPorts)
                out.deliver(data);
        }
    }


    @Override
    public String getInputGroupNamePrefix() {
        return inputName;
    }

    @Override
    public String getOutputGroupNamePrefix() {
        return outputName;
    }

    @Override
    public void ensureMinimumNumberOfPorts(int minNumber) {
        this.minNumberOfPorts = minNumber;
        updatePorts();
    }

    protected synchronized void updatePorts() {
        if (!isChanging) {
            int minNumber = Math.max(0, this.minNumberOfPorts - 1);
            // now find last connected port or last one that is locked
            int i = 1;
            for (ManyToManyPortRow managedRow : managedRows) {
                boolean isUsed = false;
                for (InputPort port : managedRow.getInputPorts()) {
                    isUsed |= port.isConnected() || port.isLocked();
                }
                for (OutputPort port : managedRow.getOutputPorts()) {
                    isUsed |= port.isConnected() || port.isLocked();
                }
                // if the currrent row is used, we preserve it
                if (isUsed && i > minNumber)
                    minNumber = i;

                i++;
            }
            minNumber += numberOfFreePorts; // we might want to have one free port!
            if (maxNumberOfPorts > 0)
                minNumber = Math.min(minNumber, maxNumberOfPorts);

            isChanging = true;
            // now we can delete the last rows, beyond what needs to be kept
            if (managedRows.size() > minNumber) {
                ListIterator<ManyToManyPortRow> iterator = managedRows.listIterator(minNumber);
                while (iterator.hasNext()) {
                    ManyToManyPortRow row = iterator.next();
                    deletePortRow(row);
                    iterator.remove();
                }
            } else {
                for (int j = managedRows.size(); j < minNumber; j++) {
                    managedRows.add(createPortRow(j + 1));
                }
            }
            isChanging = false;
        }
    }

    private ManyToManyPortRow createPortRow(int number) {
        ArrayList<InputPort> newManyInputPorts = new ArrayList<>(manyInputPorts.size());
        for (Ports<InputPort> ports : manyInputPorts) {
            InputPort newInputPort = ports.createPort(inputName + " " + number, priority);
            if (preconditionProvider != null) {
                newInputPort.addPrecondition(preconditionProvider.accept(newInputPort, number));
            }
            newManyInputPorts.add(newInputPort);
        }
        ArrayList<OutputPort> newManyOutputPorts = new ArrayList<>(manyOutputPorts.size());

        for (Ports<OutputPort> ports : manyOutputPorts) {
            OutputPort newManyOutputPort = ports.createPort(outputName + " " + number, priority);
            newManyOutputPorts.add(newManyOutputPort);
        }
        return new ManyToManyPortRow(newManyInputPorts, newManyOutputPorts);
    }

    private void deletePortRow(ManyToManyPortRow row) {
        for (InputPort inputPort : row.manyInputPorts) {
            ((Ports<InputPort>) inputPort.getPorts()).removePort(inputPort);
        }
        for (OutputPort outputPort : row.manyOutputPorts) {
            if (outputPort.isConnected()) {
                outputPort.disconnect();
            }
            ((Ports<OutputPort>) outputPort.getPorts()).removePort(outputPort);
        }
    }

    /**
     * This returns true if the output ports of this output port are connected. This can be used if
     * it is not important whether ANY of the ports is connected but for example one specific one
     * (at the outside of an operator with a subprocess)
     *
     * @param ports
     * @return
     */
    public boolean isConnected(OutputPorts ports) {
        for (ManyToManyPortRow pair : managedRows) {
            for (OutputPort port : pair.manyOutputPorts) {
                if (port.getPorts() == ports && port.isConnected())
                    return true;
            }
        }
        return false;
    }

    /**
     * This returns true if any of the output ports is connected.
     *
     * @return
     */
    public boolean isConnected() {
        for (ManyToManyPortRow pair : managedRows) {
            for (OutputPort port : pair.manyOutputPorts) {
                if (port.isConnected())
                    return true;
            }
        }
        return false;
    }

    public MDTransformationRule makePassThroughRule() {
        return new MDTransformationRule() {

            @Override
            public void transformMD() {
                for (ManyToManyPortRow row : managedRows) {
                    MetaData metaData = row.manyInputPorts.get(META_DATA_SOURCE_INDEX).getMetaData();
                    for (OutputPort outputPort : row.manyOutputPorts) {
                        MetaData clonedMetaData = null;
                        if (metaData != null) {
                            clonedMetaData = metaData.clone();
                        }
                        outputPort.deliverMD(clonedMetaData);
                    }
                }
            }
        };
    }

    public MDTransformationRule makeTransformationRule(Function<MetaData, MetaData> transformFunction) {
        return new MDTransformationRule() {

            @Override
            public void transformMD() {
                for (ManyToManyPortRow row : managedRows) {

                    MetaData metaData = row.manyInputPorts.get(META_DATA_SOURCE_INDEX).getMetaData();
                    for (OutputPort outputPort : row.manyOutputPorts) {
                        MetaData resultMetaData = null;
                        if (metaData != null) {
                            resultMetaData = transformFunction.apply(metaData);
                        }
                        outputPort.deliverMD(resultMetaData);
                    }
                }
            }
        };
    }

    protected List<ManyToManyPortRow> getAllPortRows() {
        return managedRows;
    }

    @Override
    public boolean isAnyPortConnected() {
        for (ManyToManyPortRow row : managedRows)
            for (OutputPort port : row.manyOutputPorts)
                if (port.isConnected())
                    return true;
        return false;
    }

    protected void registerOutputPorts(OutputPorts additionalOutputPorts) {
        manyOutputPorts.add(additionalOutputPorts);
        for (int number = 0; number < managedRows.size(); number++) {
            OutputPort newInputPort = additionalOutputPorts.createPort(inputName + " " + (number + 1), priority);
            managedRows.get(number).getOutputPorts().add(newInputPort);
        }
    }

    protected void registerInputPorts(InputPorts additionalInputPorts) {
        manyInputPorts.add(additionalInputPorts);
        for (int number = 0; number < managedRows.size(); number++) {
            InputPort newInputPort = additionalInputPorts.createPort(inputName + " " + (number + 1), priority);
            if (preconditionProvider != null)
                newInputPort.addPrecondition(preconditionProvider.accept(newInputPort, number));
            managedRows.get(number).getInputPorts().add(newInputPort);
        }
    }

    protected void unregisterOutputPorts(OutputPorts outputPorts) {
        int index = manyOutputPorts.indexOf(outputPorts);
        for (ManyToManyPortRow managedRow : managedRows) {
            managedRow.getOutputPorts().remove(index);
        }
        manyOutputPorts.remove(index);
    }

    protected void unregisterInputPorts(InputPorts inputPorts) {
        int index = manyInputPorts.indexOf(inputPorts);
        for (ManyToManyPortRow managedRow : managedRows) {
            managedRow.getInputPorts().remove(index);
        }
        manyInputPorts.remove(index);
    }
}
