package com.owc.singularity.engine.ports;


import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.IOContainer;
import com.owc.singularity.engine.ports.extender.PortExtender;
import com.owc.singularity.engine.tools.AbstractObservable;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;

public class SortingOutputPorts extends AbstractObservable<Port> implements OutputPorts {

    private static final Logger log = LogManager.getLogger(SortingOutputPorts.class);
    private final List<OutputPort> portList = Collections.synchronizedList(new ArrayList<>());
    private final Map<String, OutputPort> portMap = new HashMap<>();
    private String[] portNames;
    private boolean portNamesValid = false;
    private final PortOwner owner;

    private final Observer<Port> delegatingObserver = new Observer<Port>() {

        @Override
        public void update(Observable<Port> observable, Port arg) {
            fireUpdate(arg);
        }
    };

    public SortingOutputPorts(PortOwner owner) {
        this.owner = owner;
        portNamesValid = false;
    }

    /**
     * This method adds regular ports or the first port of a port extender to the port list. It uses
     * the priority and inserts at the first position where this port is larger than the current and
     * smaller than the next port's priority.
     * 
     * @param port
     *            the port to add
     */
    private void insertSinglePort(OutputPort port) {
        boolean isAdded = false;
        ListIterator<OutputPort> iterator = portList.listIterator();
        while (iterator.hasNext()) {
            OutputPort currentPort = iterator.next();
            if (currentPort.getPriority() > port.getPriority()) {
                iterator.previous();
                iterator.add(port);
                isAdded = true;
                break;
            }
        }
        if (!isAdded) {
            portList.add(port);
        }
    }


    /**
     * This method adds a port that is part of a port group into the port list. If it's the first
     * port of the group, it is added according to priority of the port.
     * 
     * @param port
     *            port to add
     */
    private void insertGroupPort(OutputPort port) {
        String portGroupExpression = getPortGroupName(port.getName()) + " [0-9]+";

        boolean isGroup = false;
        boolean isAdded = false;
        ListIterator<OutputPort> iterator = portList.listIterator();
        while (iterator.hasNext()) {
            OutputPort existingPort = iterator.next();
            String existingPortName = existingPort.getName();
            if (existingPortName.matches(portGroupExpression)) {
                isGroup = true;
                if (!iterator.hasNext()) {
                    iterator.add(port);
                    isAdded = true;
                }
            } else {
                if (isGroup) {
                    // then it's next entry AFTER the right group
                    iterator.previous();
                    iterator.add(port);
                    isAdded = true;
                    break;
                }
            }
        }
        // make sure that we add it at all if is empty until now
        if (!isAdded) {
            insertSinglePort(port);
        }
    }

    private void addPort(OutputPort port) throws PortException {
        if (portMap.containsKey(port.getName())) {
            throw new PortException("Port name already used: " + port.getName());
        }
        assert port.getPorts() == this;
        portMap.put(port.getName(), port);
        portNamesValid = false;
        port.addObserver(delegatingObserver, false);

        if (port.getName().matches(".* [0-9]+")) {
            insertGroupPort(port);
        } else {
            insertSinglePort(port);
        }

        // finally fire update
        fireUpdate(port);
    }

    private void updatePortNames() {
        if (!portNamesValid) {
            portNames = new String[portList.size()];
            int i = 0;
            synchronized (portList) {
                for (Port port : portList) {
                    portNames[i++] = port.getName();
                }
            }
            portNamesValid = true;
        }
    }

    @Override
    public void removePort(OutputPort port) throws PortException {
        if (!portList.contains(port) || port.getPorts() != this) {
            throw new PortException("Cannot remove " + port + ".");
        } else {
            if (port.isConnected()) {
                if (port instanceof OutputPort) {
                    port.disconnect();
                } else {
                    ((InputPort) port).getSource().disconnect();
                }
            }
            portList.remove(port);
            portMap.remove(port.getName());
            port.removeObserver(delegatingObserver);
            fireUpdate();
        }
    }

    @Override
    public void removeAll() {
        // iterate to avoid concurrent modification
        while (getNumberOfPorts() != 0) {
            removePort(getPortByIndex(0));
        }
    }

    @Override
    public int getNumberOfPorts() {
        return portList.size();
    }

    @Override
    public OutputPort getPortByIndex(int index) {
        return portList.get(index);
    }

    @Override
    public OutputPort getPortByName(String name) {
        OutputPort port = portMap.get(name);
        if (port != null) {
            return port;
        } else {
            if (portExtenders != null) {
                for (PortExtender extender : portExtenders) {
                    String prefix = extender.getOutputGroupNamePrefix();
                    if (name.startsWith(prefix)) {
                        try {
                            int index = Integer.parseInt(name.substring(prefix.length()).trim());
                            extender.ensureMinimumNumberOfPorts(index); // numbering starts at 1
                            OutputPort secondTry = portMap.get(name);
                            if (secondTry == null) {
                                log.warn("Port extender '{}' did not extend to size {}", prefix, index);
                            }
                            return secondTry;
                        } catch (NumberFormatException e) {
                            log.warn("Could not extend port '{}'", prefix, e);
                            return null;
                        }
                    }
                }
            }
            // no extender found
            return null;
        }
    }

    @Override
    public String[] getPortNames() {
        updatePortNames();
        return portNames;
    }

    @Override
    public List<OutputPort> getAllPorts() {
        synchronized (portList) {
            return Collections.unmodifiableList(new ArrayList<>(portList));
        }
    }

    @Override
    public PortOwner getOwner() {
        return owner;
    }


    @Override
    public void renamePort(OutputPort port, String newName) {
        if (portMap.containsKey(newName)) {
            throw new PortException("Port name already used: " + port.getName());
        }
        portMap.remove(port.getName());
        ((SortedOutputPort) port).setName(newName);
        portMap.put(newName, port);
    }

    @Override
    public void clear(int clearFlags) {
        for (OutputPort port : getAllPorts()) {
            port.clear(clearFlags);
        }
    }

    @Override
    public IOContainer createIOContainer(boolean onlyConnected, boolean omitEmptyResults) {
        Collection<IOObject> output = new LinkedList<>();
        for (Port port : getAllPorts()) {
            if (!onlyConnected || port.isConnected()) {
                IOObject data = port.getDataOrNull();
                if (omitEmptyResults) {
                    if (data != null) {
                        output.add(data);
                    }
                } else {
                    output.add(data);
                }
            }
        }
        return new IOContainer(output);
    }

    @Override
    public IOContainer createIOContainer(boolean onlyConnected) {
        return createIOContainer(onlyConnected, true);
    }

    private List<PortExtender> portExtenders;

    @Override
    public void registerPortExtender(PortExtender extender) {
        if (portExtenders == null) {
            portExtenders = new LinkedList<>();
        }
        portExtenders.add(extender);
    }

    @Override
    public void unlockPortExtenders() {
        if (portExtenders != null) {
            for (PortExtender extender : portExtenders) {
                extender.ensureMinimumNumberOfPorts(0);
            }
        }
    }

    @Override
    public void freeMemory() {
        for (Port OutputPort : getAllPorts()) {
            OutputPort.freeMemory();
        }
    }

    @Override
    public int getNumberOfConnectedPorts() {
        int count = 0;
        for (Port port : getAllPorts()) {
            if (port.isConnected()) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        boolean first = true;
        for (String port : getPortNames()) {
            if (first) {
                first = false;
            } else {
                b.append(", ");
            }
            b.append(port);
        }
        return b.toString();
    }

    @Override
    public OutputPort createPort(String name, double portPriority) {
        OutputPort out = new SortedOutputPort(this, name, portPriority);
        addPort(out);
        return out;
    }

    @Override
    public void disconnectAll() {
        disconnectAllBut(null);
    }

    @Override
    public void disconnectAllBut(List<Operator> exceptions) {
        boolean success;
        disconnect: do {
            success = false;
            for (OutputPort port : getAllPorts()) {
                if (port.isConnected()) {
                    InputPort destination = port.getDestination();
                    boolean isException = false;
                    if (exceptions != null) {
                        Operator destOp = destination.getPorts().getOwner().getOperator();
                        if (exceptions.contains(destOp)) {
                            isException = true;
                        }
                    }
                    if (!isException) {
                        port.disconnect();
                        success = true;
                        continue disconnect;
                    }
                }
            }
        } while (success);
    }

    private String getPortGroupName(String portName) {
        int lastIndexOf = portName.lastIndexOf(" ");
        if (lastIndexOf > -1) {
            return portName.substring(0, lastIndexOf);
        }
        return portName;
    }
}
