/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.ports.extender;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.iterators.ReverseListIterator;

import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.ports.Ports;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;


/**
 * A port extender automatically creates an additional port if ports are connected. It guarantees
 * that there is always exactly one unconnected port. A port extender works by adding itself as an
 * observer of a {@link Ports} object and creating or deleting ports as necessary on any update.
 * 
 * @author Simon Fischer
 * @param <T>
 *            The type of port that is created.
 */
public abstract class AbstractPortExtender<T extends Port> implements PortExtender {

    private final Ports<T> ports;
    protected final String name;

    /** The number of ports that are guaranteed to exist. */
    protected int minNumber = 0;

    private boolean isChanging = false;

    /**
     * List of all Ports which are managed by this Class. Caution: to prevent failures in the GUI
     * mind that this list must be synchronized with the Ports-field!
     */
    protected final List<T> managedPorts = new LinkedList<>();

    protected int runningId = 0;

    private final Observer<Port> observer = new Observer<Port>() {

        @Override
        public void update(Observable<Port> observable, Port arg) {
            updatePorts();
        }
    };
    private double priority;

    /**
     * 
     * @param name
     *            The name prefix for the generated ports. An underscore and a number will be added.
     * @param ports
     *            The port to which ports are added.
     */
    public AbstractPortExtender(String name, double priority, Ports<T> ports) {
        this.priority = priority;
        this.ports = ports;
        this.name = name;
        ports.registerPortExtender(this);
    }

    /**
     * Deletes all unused (= not connected and not locked) ports, always keeping at least one free
     * port available. <br/>
     * Override only in special cases, e.g. when needing to control the number of ports depending on
     * parameters or other properties of the operator.
     */
    protected void updatePorts() {
        if (!isChanging) {
            isChanging = true;
            boolean oneDisconnectedPort = false;
            T foundDisconnected = null;
            ReverseListIterator<T> i = new ReverseListIterator<>(managedPorts);
            List<T> portsToRemove = new LinkedList<>();
            int portIndex = 0;
            while (i.hasNext()) {
                T port = i.next();
                if (!port.isConnected() && !port.isLocked()) {
                    // we only remove the last disconnected port if at least a second disconnected
                    // port is found
                    if (!oneDisconnectedPort) {
                        oneDisconnectedPort = true;
                        foundDisconnected = port;
                    } else {
                        if (portIndex >= minNumber) { // we don't remove if guaranteeing ports
                            portsToRemove.add(foundDisconnected);
                            foundDisconnected = port;
                        }
                    }
                } else {
                    break;
                }
                portIndex++;
            }
            // check whether we have to delete
            if (!portsToRemove.isEmpty()) {
                for (T port : portsToRemove) {
                    deletePort(port);
                    managedPorts.remove(port);
                }
            } else {
                // or to add
                if (!oneDisconnectedPort) {
                    managedPorts.add(createPort());
                }
                // we still might not have enought
                if ((managedPorts.size() < minNumber)) {
                    while (managedPorts.size() < minNumber) {
                        managedPorts.add(createPort());
                    }
                }
            }
            fixNames();
            isChanging = false;
        }
    }

    protected void deletePort(T port) {
        if (port instanceof OutputPort) {
            if (port.isConnected()) {
                ((OutputPort) port).disconnect();
            }
        }
        ports.removePort(port);
    }

    protected T createPort() {
        runningId++;
        return ports.createPort(name + " " + runningId, priority);
    }

    protected void fixNames() {
        runningId = 0;
        for (T port : managedPorts) {
            runningId++;
            ports.renamePort(port, name + "_tmp_" + runningId);
        }
        runningId = 0;
        for (T port : managedPorts) {
            runningId++;
            ports.renamePort(port, name + " " + runningId);
        }

    }

    /** Creates an initial port and starts to listen. */
    public void start() {
        managedPorts.add(createPort());
        fixNames();
        ports.addObserver(observer, false);
    }

    /** Returns an unmodifiable view of the ports created by this port extender. */
    public List<T> getManagedPorts() {
        return Collections.unmodifiableList(managedPorts);
    }

    @Override
    public void ensureMinimumNumberOfPorts(int minNumber) {
        this.minNumber = minNumber;
        updatePorts();
    }

    protected Ports<T> getPorts() {
        return ports;
    }
}
