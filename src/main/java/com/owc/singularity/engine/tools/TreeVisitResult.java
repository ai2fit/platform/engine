package com.owc.singularity.engine.tools;

public enum TreeVisitResult {
    /**
     * Continue. When returned from a {@link TreeVisitor#preVisitNode} method then the entries in
     * the node should also be visited.
     */
    CONTINUE,
    /**
     * Terminate.
     */
    TERMINATE,
    /**
     * Continue without visiting the entries in this node. This result is only meaningful when
     * returned from the {@link TreeVisitor#preVisitNode} method; otherwise this result type is the
     * same as returning {@link #CONTINUE}.
     */
    SKIP_SUBTREE,
    /**
     * Continue without visiting the <em>siblings</em> of this leaf or node. If returned from the
     * {@link TreeVisitor#preVisitNode} method then the entries in the node are also skipped and the
     * {@link TreeVisitor#postVisitNode} method is not invoked.
     */
    SKIP_SIBLINGS;
}
