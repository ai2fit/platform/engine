package com.owc.singularity.engine.tools.io.csv;


import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.List;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;

/**
 * Class for streaming an exampleSet as CSV.
 *
 * @author Sebastian Land
 *
 */
public class ExampleSetCSVInputStream extends InputStream {

    private final ExampleSet exampleSet;
    private final ValueType[] valueTypes;
    private final int[] attributeIndexes;
    private final String[] attributeNames;
    private byte[] currentLine;
    private int currentLinePointer = 0;
    int exampleRow = 0;
    private final DateFormat dateFormat = CSVParser.getDefaultFormat();

    public ExampleSetCSVInputStream(ExampleSet exampleSet) {
        this.exampleSet = exampleSet;
        Attribute[] attributes = exampleSet.getAttributes().streamAttributes().toArray(Attribute[]::new);
        this.valueTypes = Arrays.stream(attributes).map(Attribute::getValueType).toArray(ValueType[]::new);
        this.attributeIndexes = Arrays.stream(attributes).mapToInt(Attribute::getIndex).toArray();
        this.attributeNames = Arrays.stream(attributes).map(Attribute::getName).toArray(String[]::new);
        currentLine = CSVParser.formatHeader(attributeNames, true, '\"', '\\', ",", dateFormat).getBytes(StandardCharsets.UTF_8);
    }

    public ExampleSetCSVInputStream(ExampleSet exampleSet, List<Attribute> exportAttributes) {
        this.exampleSet = exampleSet;
        this.valueTypes = exportAttributes.stream().map(Attribute::getValueType).toArray(ValueType[]::new);
        this.attributeIndexes = exportAttributes.stream().mapToInt(Attribute::getIndex).toArray();
        this.attributeNames = exportAttributes.stream().map(Attribute::getName).toArray(String[]::new);
        currentLine = CSVParser.formatHeader(attributeNames, true, '\"', '\\', ",", dateFormat).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public int read() throws IOException {
        if (currentLinePointer == currentLine.length) {
            if (exampleRow >= exampleSet.size())
                return -1;

            currentLine = CSVParser.formatExample(exampleSet, valueTypes, attributeIndexes, exampleRow, true, '\"', '\\', ",", dateFormat)
                    .getBytes(StandardCharsets.UTF_8);
            exampleRow++;
            currentLinePointer = 0;
        }

        return currentLine[currentLinePointer++];
    }

    @Override
    public synchronized void reset() throws IOException {
        currentLinePointer = 0;
        exampleRow = 0;
        currentLine = CSVParser.formatHeader(attributeNames, true, '\"', '\\', ",", dateFormat).getBytes(StandardCharsets.UTF_8);
    }
}
