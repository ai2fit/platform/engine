/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.basic;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.function.Abstract2DoubleInputFunction;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function} for division.
 *
 * @author Gisa Schaefer
 *
 */
public class Divide extends Abstract2DoubleInputFunction {

    /**
     * Constructs a division function.
     */
    public Divide() {
        super("basic.division", 2, ValueType.NUMERIC);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType left = inputTypes[0];
        ExpressionType right = inputTypes[1];
        if ((left == ExpressionType.INTEGER || left == ExpressionType.DOUBLE) && (right == ExpressionType.INTEGER || right == ExpressionType.DOUBLE)) {
            // division has return type double even if both inputs are integers
            return ExpressionType.DOUBLE;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "numerical");
        }
    }

    @Override
    protected double compute(double value1, double value2) {
        return value1 / value2;
    }

}
