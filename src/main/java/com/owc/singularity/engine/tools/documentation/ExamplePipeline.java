/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.documentation;


import org.w3c.dom.Element;

import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * An example process with a description.
 * 
 * @author Simon Fischer
 */
public class ExamplePipeline {

    private String pipelineXML;
    private String comment;
    private final Element element;

    public ExamplePipeline(Element exampleElement) {
        this.element = exampleElement;
        if (element != null) {
            this.pipelineXML = XMLTools.getTagContents(element, "pipeline");
            this.comment = XMLTools.getTagContents(element, "comment");
        }
    }

    public void setComment(String comment) {
        this.comment = comment;
        if (element != null) {
            XMLTools.setTagContents(element, "comment", comment);
        }
    }

    public String getComment() {
        return comment;
    }

    public void setPipelineXML(String xml) {
        this.pipelineXML = xml;
        if (element != null) {
            XMLTools.setTagContents(element, "pipeline", xml);
        }
    }

    public String getPipelineXML() {
        return pipelineXML;
    }

    public AbstractPipeline getPipeline() {
        try {
            return XMLImporter.parse(getPipelineXML());
        } catch (OperatorCreationException | XMLException e) {
            LogService.getRoot().warn("com.owc.singularity.engine.tools.documentation.ExamplePipeline.parsing_example_process_error", e, e);
            return null;
        }
    }

    public Element getElement() {
        return element;
    }

}
