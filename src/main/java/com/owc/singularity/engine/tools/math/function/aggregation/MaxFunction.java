/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the maximum.
 *
 * @author Tobias Malbrecht, Ingo Mierswa
 *
 */
public class MaxFunction extends AbstractAggregationFunction {

    private String MAXIMUM_AGGREGATION_FUNCTION_NAME = "maximum";

    private double maxValue;

    public MaxFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public MaxFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return MAXIMUM_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        maxValue = Double.NEGATIVE_INFINITY;
    }

    @Override
    public void update(double value) throws UnsupportedOperationException {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        if (value > maxValue) {
            maxValue = value;
        }
    }

    @Override
    public void update(long value) throws UnsupportedOperationException {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        if (value > maxValue) {
            maxValue = value;
        }
    }

    @Override
    public double aggregateNumericValue() throws UnsupportedOperationException {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NUMERIC;
        }
        return maxValue;
    }

    @Override
    public long aggregateTimestampValue() throws UnsupportedOperationException {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_TIMESTAMP;
        }
        return (long) maxValue;
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return !ValueType.NOMINAL.equals(valueType);
    }
}
