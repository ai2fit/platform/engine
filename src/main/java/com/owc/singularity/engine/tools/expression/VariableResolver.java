/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.expression.FunctionInput.Category;


/**
 * A {@link Resolver} for variables using a {@link VariableHandler}.
 *
 * @author Gisa Schaefer
 * @since 6.5.0
 */
public class VariableResolver implements Resolver {

    private final VariableHandler variableHandler;
    private final Operator operator;

    private static final String KEY_VARIABLES = I18N.getGUIMessage("gui.dialog.function_input.variables");

    /**
     * Creates a {@link VariableResolver} that uses the given variableHandler.
     *
     * @param variableHandler
     *            the variable handler to resolve variables
     */
    public VariableResolver(VariableHandler variableHandler) {
        this(variableHandler, null);
    }

    /**
     * * Creates a {@link VariableResolver} that uses the given variableHandler and operator.
     * 
     * @param variableHandler
     *            the variable handler to resolve variables
     * @param operator
     *            the operator used for resolving operator dependent predefined variables
     */
    public VariableResolver(VariableHandler variableHandler, Operator operator) {
        this.variableHandler = variableHandler;
        this.operator = operator;
    }

    @Override
    public Collection<FunctionInput> getAllVariables() {
        List<FunctionInput> functionInputs = new ArrayList<>();

        String[] predefinedVariables = variableHandler.getAllGraphicallySupportedPredefinedVariables();
        for (String variable : predefinedVariables) {
            functionInputs.add(new FunctionInput(Category.SCOPE, KEY_VARIABLES, variable, ValueType.NOMINAL, null, false));
        }

        Iterator<String> variables = variableHandler.getDefinedVariableNames();
        while (variables.hasNext()) {
            functionInputs.add(new FunctionInput(Category.SCOPE, KEY_VARIABLES, variables.next(), ValueType.NOMINAL, null, true));
        }

        return functionInputs;
    }

    @Override
    public ExpressionType getVariableType(String variableName) {
        if (!variableHandler.isVariableSet(variableName, operator)) {
            return null;
        }
        return ExpressionType.NOMINAL;
    }

    @Override
    public String getStringValue(String variableName) {
        return variableHandler.getVariable(variableName, operator);
    }

    @Override
    public double getDoubleValue(String variableName) {
        throw new IllegalStateException("Variables are always Strings!");
    }

    @Override
    public boolean getBooleanValue(String variableName) {
        throw new IllegalStateException("Variables are always Strings!");
    }

    @Override
    public long getTimestampValue(String variableName) {
        throw new IllegalStateException("Variables are always Strings!");
    }

}
