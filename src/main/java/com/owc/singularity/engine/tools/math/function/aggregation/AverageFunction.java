/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the average.
 * 
 * @author Tobias Malbrecht, Ingo Mierswa
 * 
 */
public class AverageFunction extends AbstractAggregationFunction {

    private String AVERAGE_AGGREGATION_FUNCTION_NAME = "average";

    private double valueSum;

    private double updateCounter;

    public AverageFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public AverageFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return AVERAGE_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        valueSum = 0.0d;
        updateCounter = 0.0d;
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        valueSum += value;
        updateCounter++;
    }

    @Override
    public void update(long value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        valueSum += value;
        updateCounter++;
    }

    @Override
    public double aggregateNumericValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NUMERIC;
        }
        return valueSum / updateCounter;
    }

    @Override
    public long aggregateTimestampValue() throws UnsupportedOperationException {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_TIMESTAMP;
        }
        return (long) (valueSum / updateCounter);
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return !ValueType.NOMINAL.equals(valueType);
    }
}
