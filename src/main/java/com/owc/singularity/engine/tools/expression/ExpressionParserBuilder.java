/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.expression.internal.ConstantResolver;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionContext;
import com.owc.singularity.engine.tools.expression.internal.antlr.AntlrParser;
import com.owc.singularity.engine.tools.expression.internal.function.eval.AttributeEvaluation;
import com.owc.singularity.engine.tools.expression.internal.function.eval.Evaluation;
import com.owc.singularity.engine.tools.expression.internal.function.eval.TypeConstants;
import com.owc.singularity.engine.tools.expression.internal.function.process.ParameterValue;
import com.owc.singularity.engine.tools.expression.internal.function.statistical.Random;


/**
 * Builder for an {@link ExpressionParser}.
 *
 * @author Gisa Schaefer
 * @since 6.5.0
 */
public class ExpressionParserBuilder {

    private AbstractPipeline process;

    private final List<Function> functions = new LinkedList<>();

    private final List<Resolver> scopeResolvers = new LinkedList<>();
    private final List<Resolver> dynamicsResolvers = new LinkedList<>();
    private final List<Resolver> constantResolvers = new LinkedList<>();

    /**
     * Builds an {@link ExpressionParser} with the given data.
     *
     * @return an expression parser
     */
    public ExpressionParser build() {
        // add functions with process information
        if (process != null) {
            functions.add(new Random(process));
            functions.add(new ParameterValue(process));
        }

        Evaluation evalFunction = new Evaluation();
        functions.add(evalFunction);
        // add the attribute eval function
        AttributeEvaluation attributeEvalFunction = new AttributeEvaluation();
        functions.add(attributeEvalFunction);

        // add eval constants
        constantResolvers.add(new ConstantResolver(TypeConstants.INSTANCE.getKey(), TypeConstants.INSTANCE.getConstants()));

        ExpressionContext context = new SimpleExpressionContext(functions, scopeResolvers, dynamicsResolvers, constantResolvers, getStopChecker());
        AntlrParser parser = new AntlrParser(context);

        evalFunction.setParser(parser);
        attributeEvalFunction.setContext(context);

        return parser;
    }

    /**
     * Adds the process which enables process dependent functions.
     *
     * @param process
     *            the process to add
     * @return the builder
     */
    public ExpressionParserBuilder withProcess(AbstractPipeline process) {
        this.process = process;
        return this;
    }

    /**
     * Adds the resolver as a resolver for scope constants (%{scope_constant} in the expression).
     *
     * @param resolver
     *            the resolver to add
     * @return the builder
     */
    public ExpressionParserBuilder withScope(Resolver resolver) {
        scopeResolvers.add(resolver);
        return this;
    }

    /**
     * Adds the resolver as a resolver for dynamic variables ([variable_name] or variable_name in
     * the expression).
     *
     * @param resolver
     *            the resolver to add
     * @return the builder
     */
    public ExpressionParserBuilder withDynamics(Resolver resolver) {
        dynamicsResolvers.add(resolver);
        return this;
    }

    /**
     * Adds the given module that supplies functions and constant values.
     *
     * @param module
     *            the module to add
     * @return the builder
     */
    public ExpressionParserBuilder withModule(ExpressionParserModule module) {
        addModule(module);
        return this;
    }

    /**
     * Adds all functions of the module to the list of functions and adds a {@link ConstantResolver}
     * knowing all constants of the module to the list of constant resolver.
     *
     * @param module
     *            the module to add
     */
    private void addModule(ExpressionParserModule module) {
        List<Constant> moduleConstants = module.getConstants();
        if (moduleConstants != null && !moduleConstants.isEmpty()) {
            constantResolvers.add(new ConstantResolver(module.getKey(), moduleConstants));
        }
        List<Function> moduleFunctions = module.getFunctions();
        if (moduleFunctions != null) {
            functions.addAll(moduleFunctions);
        }
    }

    /**
     * Adds the given modules that supplies functions and constant values.
     *
     * @param modules
     *            the modules to add
     * @return the builder
     */
    public ExpressionParserBuilder withModules(List<ExpressionParserModule> modules) {
        for (ExpressionParserModule module : modules) {
            addModule(module);
        }
        return this;
    }

    /** @since 9.6.0 */
    private Callable<Void> getStopChecker() {
        if (process != null) {
            return () -> {
                process.getRootOperator().checkForStop();
                return null;
            };
        }
        return () -> null;
    }

}
