/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Abstract class for a {@link Function} that has a date, an integer and a unit as arguments and can
 * have locale and returns a date. The function can has time zone and localization strings as
 * optional parameters
 *
 * @author David Arnu
 *
 */
public abstract class AbstractDateManipulationFunction extends AbstractFunction {

    public AbstractDateManipulationFunction(String i18nKey, int numberOfArgumentsToCheck, ValueType returnType) {
        super(i18nKey, numberOfArgumentsToCheck, returnType);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        return compute(() -> null, inputEvaluators);
    }

    @Override
    public ExpressionEvaluator compute(Callable<Void> stopChecker, ExpressionEvaluator... inputEvaluators) {
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator locale = new SimpleExpressionEvaluator((String) null, ExpressionType.NOMINAL);
        ExpressionEvaluator timeZone = new SimpleExpressionEvaluator((String) null, ExpressionType.NOMINAL);
        ExpressionEvaluator date = inputEvaluators[0];
        ExpressionEvaluator value = inputEvaluators[1];
        ExpressionEvaluator unit = inputEvaluators[2];
        if (inputEvaluators.length != 3) {
            locale = inputEvaluators[3];
            timeZone = inputEvaluators[4];
        }
        return new SimpleExpressionEvaluator(type, makeDateCallable(stopChecker, date, value, unit, locale, timeZone), isResultConstant(inputEvaluators));

    }

    private TimestampSupplier makeDateCallable(Callable<Void> stopChecker, ExpressionEvaluator date, ExpressionEvaluator value, ExpressionEvaluator unit,
            ExpressionEvaluator locale, ExpressionEvaluator timeZone) {
        final TimestampSupplier funcDate;
        final NumericSupplier funcValue;
        final NominalSupplier funcUnit;
        final NominalSupplier funcLocale;
        final NominalSupplier funcTimeZone;
        AtomicBoolean allConstant = new AtomicBoolean(true);
        try {
            if (date.isConstant()) {
                long dateValue = date.getTimestampSupplier().getOrThrow();
                funcDate = () -> dateValue;
            } else {
                funcDate = date.getTimestampSupplier();
                allConstant.set(false);
            }
            if (value.isConstant()) {
                double valueValue = value.getNumericSupplier().getOrThrow();
                funcValue = () -> valueValue;
            } else {
                funcValue = value.getNumericSupplier();
                allConstant.set(false);
            }
            funcUnit = getStringCallable(unit, allConstant);
            funcLocale = getStringCallable(locale, allConstant);
            funcTimeZone = getStringCallable(timeZone, allConstant);

            TimestampSupplier callable = () -> compute(stopChecker, funcDate.getOrThrow(), funcValue.getOrThrow(), funcUnit.getOrThrow(),
                    funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
            // all constant values
            if (allConstant.get()) {
                long resultDate = callable.getOrThrow();
                return () -> resultDate;
            }
            return callable;
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for manipulating a date for a certain value on a given unit with
     * additional locale and time zone arguments.
     *
     * @param date
     *            date to manipulate
     * @param value
     *            the amount of which the date should change
     * @param unit
     *            the unit constant which should be changed
     * @param valueLocale
     *            the locale string
     * @param valueTimezone
     *            time zone string
     * @return the result of the computation.
     */
    protected abstract long compute(long date, double value, String unit, String valueLocale, String valueTimezone);

    /**
     * Computes the result for manipulating a date for a certain value on a given unit with
     * additional locale and time zone arguments.
     *
     * @param stopChecker
     *            optional callable to check for stop
     * @param date
     *            date to manipulate
     * @param value
     *            the amount of which the date should change
     * @param unit
     *            the unit constant which should be changed
     * @param valueLocale
     *            the locale string
     * @param valueTimezone
     *            time zone string
     * @return the result of the computation.
     * @since 9.6.0
     */
    protected long compute(Callable<Void> stopChecker, long date, double value, String unit, String valueLocale, String valueTimezone) {
        return compute(date, value, unit, valueLocale, valueTimezone);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes.length != 3 && inputTypes.length != 5) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), "3", "5", inputTypes.length);
        }
        ExpressionType firstType = inputTypes[0];
        ExpressionType secondType = inputTypes[1];
        ExpressionType thirdType = inputTypes[2];

        if (firstType != ExpressionType.TIMESTAMP) {
            throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "date", "first");
        }
        if (secondType != ExpressionType.DOUBLE && secondType != ExpressionType.INTEGER) {
            throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "double or integer", "second");

        }
        if (thirdType != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "third");

        }

        if (inputTypes.length == 5) {
            if (inputTypes[3] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "fourth");
            }
            if (inputTypes[4] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "fifth");
            }

        }
        return ExpressionType.TIMESTAMP;

    }

    /** @since 9.6.0 */
    private static NominalSupplier getStringCallable(ExpressionEvaluator stringEvaluator, AtomicBoolean allConstant) throws Exception {
        if (stringEvaluator.isConstant()) {
            String unitValue = stringEvaluator.getNominalSupplier().getOrThrow();
            return () -> unitValue;
        }
        allConstant.set(false);
        return stringEvaluator.getNominalSupplier();
    }
}
