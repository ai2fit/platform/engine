package com.owc.singularity.engine.tools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Stream;

import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

/**
 * This is a thread-safe helper class to map Strings to int and vice-versa.
 * 
 * @author Sebastian Land
 *
 */
public final class BiDirectionalIntStringMapping implements Serializable {

    private static final long serialVersionUID = 7112311418624037891L;

    private ArrayList<String> valueByIndex = new ArrayList<>();
    private Object2IntOpenHashMap<String> indexByValue = new Object2IntOpenHashMap<>();

    public BiDirectionalIntStringMapping() {
        indexByValue.defaultReturnValue(-1);
        valueByIndex.add(null);
    }

    public BiDirectionalIntStringMapping(Stream<String> stream) {
        this();
        if (stream.isParallel()) {
            stream.forEach(this::mapString);
        } else {
            stream.forEach(this::mapStringSync);
        }
    }

    public String mapIndex(int index) {
        return valueByIndex.get(index + 1);
    }

    public synchronized int mapString(String value) {
        if (value == null)
            return -1;
        int newIndex = indexByValue.size();
        int index = indexByValue.putIfAbsent(value, newIndex);
        if (index == -1) {
            valueByIndex.add(value);
            return newIndex;
        }
        return index;
    }

    public synchronized int get(String value) {
        if (value == null)
            return -1;
        return indexByValue.getInt(value);
    }

    public synchronized String get(int value) {
        return valueByIndex.get(value);
    }

    /**
     * This method returns all non-missing values
     * 
     * @return an array of all non-missing values
     */
    public synchronized String[] getStringSet() {
        String[] values = new String[valueByIndex.size() - 1];
        for (int i = 1; i < valueByIndex.size(); i++)
            values[i - 1] = valueByIndex.get(i);
        return values;
    }


    /**
     * This is a faster, but not threadsafe implementation
     * 
     * @param value
     *            the value to put if not present
     * @return
     */
    private int mapStringSync(String value) {
        if (value == null)
            return -1;
        int newIndex = indexByValue.size();
        int index = indexByValue.putIfAbsent(value, newIndex);
        if (index == -1) {
            valueByIndex.add(value);
            return newIndex;
        }
        return index;
    }

    public int size() {
        return valueByIndex.size() - 1;
    }
}
