/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.antlr;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.Expression;
import com.owc.singularity.engine.tools.expression.ExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.ExpressionException;
import com.owc.singularity.engine.tools.expression.ExpressionParsingException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.UnknownValue;


/**
 * A basic {@link Expression}.
 *
 * @author Gisa Schaefer
 *
 */
class SimpleExpression implements Expression {

    private ExpressionEvaluator evaluator;

    /**
     * Creates a basic expression based on the evaluator.
     *
     * @param evaluator
     *            the evaluator to use for evaluating the expression
     */
    SimpleExpression(ExpressionEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    public ExpressionType getExpressionType() {
        return evaluator.getType();
    }

    @Override
    public Object evaluate() throws ExpressionException {
        try {
            switch (evaluator.getType()) {
                case DOUBLE:
                case INTEGER:
                    return evaluator.getNumericSupplier().getOrThrow();
                case BOOLEAN:
                    return evaluator.getBooleanSupplier().getOrThrow();
                case TIMESTAMP:
                    long dateResult = evaluator.getTimestampSupplier().getOrThrow();
                    return ValueType.isMissing(dateResult) ? UnknownValue.UNKNOWN_DATE : dateResult;
                case NOMINAL:
                default:
                    String stringResult = evaluator.getNominalSupplier().getOrThrow();
                    return ValueType.isMissing(stringResult) ? UnknownValue.UNKNOWN_NOMINAL : stringResult;
            }
        } catch (ExpressionException e) {
            throw e;
        } catch (ExpressionParsingException e) {
            throw new ExpressionException(e);
        } catch (Exception e) {
            throw new ExpressionException(e.getLocalizedMessage());
        }
    }

    @Override
    public String evaluateNominal() throws ExpressionException {
        try {
            switch (evaluator.getType()) {
                case BOOLEAN:
                    Boolean result = evaluator.getBooleanSupplier().getOrThrow();
                    return result == null ? null : result.toString();
                case NOMINAL:
                    return evaluator.getNominalSupplier().getOrThrow();
                default:
                    throw new IllegalArgumentException("Cannot evaluate expression of type " + getExpressionType() + " as nominal");
            }
        } catch (ExpressionException e) {
            throw e;
        } catch (ExpressionParsingException e) {
            throw new ExpressionException(e);
        } catch (Exception e) {
            throw new ExpressionException(e.getLocalizedMessage());
        }
    }

    @Override
    public double evaluateNumeric() throws ExpressionException {
        try {
            switch (evaluator.getType()) {
                case DOUBLE:
                case INTEGER:
                    return evaluator.getNumericSupplier().getOrThrow();
                default:
                    throw new IllegalArgumentException("Cannot evaluate expression of type " + getExpressionType() + " as numerical");
            }
        } catch (ExpressionException e) {
            throw e;
        } catch (ExpressionParsingException e) {
            throw new ExpressionException(e);
        } catch (Exception e) {
            throw new ExpressionException(e.getLocalizedMessage());
        }
    }

    @Override
    public long evaluateTimestamp() throws ExpressionException {
        try {
            if (evaluator.getType() != ExpressionType.TIMESTAMP) {
                throw new IllegalArgumentException("Cannot evaluate expression of type " + getExpressionType() + " as date");
            }
            return evaluator.getTimestampSupplier().getOrThrow();
        } catch (ExpressionException e) {
            throw e;
        } catch (ExpressionParsingException e) {
            throw new ExpressionException(e);
        } catch (Exception e) {
            throw new ExpressionException(e.getLocalizedMessage());
        }
    }

    @Override
    public Boolean evaluateBoolean() throws ExpressionException {
        try {
            switch (evaluator.getType()) {
                case BOOLEAN:
                    return evaluator.getBooleanSupplier().getOrThrow();
                default:
                    throw new IllegalArgumentException("Cannot evaluate expression of type " + getExpressionType() + " as boolean");
            }
        } catch (ExpressionException e) {
            throw e;
        } catch (ExpressionParsingException e) {
            throw new ExpressionException(e);
        } catch (Exception e) {
            throw new ExpressionException(e.getLocalizedMessage());
        }
    }

}
