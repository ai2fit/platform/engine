/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.tools.documentation.OperatorGroupDocumentation;


/**
 * A group tree manages operator descriptions in a tree like manner. This is useful to present the
 * operators in groups and subgroups and eases operator selection in the GUI.
 * <p>
 * The group tree heavily depends on the associated OperatorService, since it reflects the
 * registered Operators of that Service. Each {@link OperatorService} can have multiple GroupTrees,
 * which register as listener to be able to update on new registration or unregistration events.
 * 
 * @author Ingo Mierswa, Sebastian Land
 */
public class OperatorDescriptionTreeNode extends AbstractOperatorDescriptionTreeNode {

    /** The key used for mapping I18N support. */
    private final String key;

    /** The parent of this group. This is the root node if parent is null. */
    private AbstractOperatorDescriptionTreeNode parent = null;

    private final OperatorGroupDocumentation documentation;

    /** Creates a new group tree with no operators and children. */
    OperatorDescriptionTreeNode(AbstractOperatorDescriptionTreeNode parent, String key) {
        this.parent = parent;
        this.key = key;
        this.documentation = new OperatorGroupDocumentation(key);
    }

    /** Clone constructor. */
    OperatorDescriptionTreeNode(OperatorDescriptionTreeNode other) {
        super(other);
        this.key = other.key;
        this.documentation = other.documentation;
    }

    @Override
    public OperatorDescriptionTreeNode clone() {
        return new OperatorDescriptionTreeNode(this);
    }

    /** Returns the name of this group. */
    @Override
    public String getName() {
        return getDocumentation().getName();
    }

    private OperatorGroupDocumentation getDocumentation() {
        return documentation;
    }

    /** Sets the parent of this group. */
    public void setParent(AbstractOperatorDescriptionTreeNode parent) {
        this.parent = parent;
    }

    /** Returns the parent of this group. Returns null if no parent does exist. */
    @Override
    public AbstractOperatorDescriptionTreeNode getParent() {
        return parent;
    }

    @Override
    public String getDescription() {
        return documentation.getHelp();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getIconName() {
        String groupIcon = super.getIconName();
        if (groupIcon != null) {
            return groupIcon;
        } else {
            return parent.getIconName();
        }
    }

    @Override
    public int compareTo(AbstractOperatorDescriptionTreeNode o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof OperatorDescriptionTreeNode)) {
            return false;
        }
        OperatorDescriptionTreeNode a = (OperatorDescriptionTreeNode) o;
        return this.key.equals(a.key);
    }

    @Override
    public int hashCode() {
        return this.key.hashCode();
    }

    @Override
    public String getFullyQualifiedKey() {
        String parentKey = parent.getFullyQualifiedKey();
        if (!parentKey.isEmpty()) {
            return parentKey + "." + key;
        } else {
            return key;
        }
    }
}
