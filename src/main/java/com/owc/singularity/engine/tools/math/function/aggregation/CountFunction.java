/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the number of values.
 * 
 * @author Tobias Malbrecht, Ingo Mierswa
 * 
 */
public class CountFunction extends AbstractAggregationFunction {

    private String COUNT_AGGREGATION_FUNCTION_NAME = "count";

    double totalWeightSum;

    public CountFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public CountFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return COUNT_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        totalWeightSum = 0.0d;
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        totalWeightSum++;
    }

    @Override
    public void update(long value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        totalWeightSum++;
    }

    @Override
    public void update(String value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        totalWeightSum++;
    }

    @Override
    public double aggregateNumericValue() {
        if (foundMissing && !ignoreMissings) {
            return Double.NaN;
        }
        return totalWeightSum;
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return true;
    }

    @Override
    public ValueType getValueTypeOfResult(ValueType[] inputTypes) {
        return ValueType.NUMERIC;
    }
}
