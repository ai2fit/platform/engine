package com.owc.singularity.engine.tools.expression;


import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;

public class HashModuloFunction extends AbstractFunction {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    public HashModuloFunction() {
        super("text_transformation.hash_modulo", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.NUMERIC);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) throws ExpressionParsingException {
        return new SimpleExpressionEvaluator(() -> {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            if (inputEvaluators.length < 2) {
                throw new ExpressionParsingException(
                        new IllegalArgumentException("hash_modulo requires at least two arguments, the first one the integer for calculating the modulo."));
            }

            ExpressionEvaluator moduloEvaluator = inputEvaluators[0];
            if (moduloEvaluator.getType() != ExpressionType.INTEGER) {
                throw new ExpressionParsingException(new IllegalArgumentException("hash_modulo requires at least integer as first argument."));
            }
            long modulo = (long) moduloEvaluator.getNumericSupplier().getOrThrow();

            for (int i = 1; i < inputEvaluators.length; i++) {
                ExpressionEvaluator evaluator = inputEvaluators[i];
                switch (evaluator.getType()) {
                    case BOOLEAN -> digest.update(evaluator.getBooleanSupplier().getOrThrow() ? (byte) 0 : (byte) 1);
                    case TIMESTAMP -> digest.update(longToByteArray(evaluator.getTimestampSupplier().getOrThrow()));
                    case DOUBLE -> digest.update(longToByteArray(Double.doubleToLongBits(evaluator.getNumericSupplier().getOrThrow())));
                    case INTEGER -> digest.update(longToByteArray((long) evaluator.getNumericSupplier().getOrThrow()));
                    case NOMINAL -> digest.update(evaluator.getNominalSupplier().getOrThrow().getBytes(DEFAULT_CHARSET));
                    default -> {
                    }
                }
            }
            return new BigInteger(digest.digest()).mod(BigInteger.valueOf(modulo)).doubleValue();
        }, ExpressionType.DOUBLE, false);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        return ExpressionType.NOMINAL;
    }

    public static byte[] longToByteArray(long value) {
        return new byte[] { (byte) (value >> 56), (byte) (value >> 48), (byte) (value >> 40), (byte) (value >> 32), (byte) (value >> 24), (byte) (value >> 16),
                (byte) (value >> 8), (byte) value };
    }

}
