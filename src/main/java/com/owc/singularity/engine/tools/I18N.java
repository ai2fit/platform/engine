/*

 * 

 * 

 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.i18n.I18NResourcesProvider;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeCategory;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * @author Simon Fischer, Nils Woehler, Adrian Wilke
 */
public class I18N {

    public static final String SETTINGS_TYPE_TITLE_SUFFIX = ".title";
    public static final String SETTINGS_TYPE_DESCRIPTION_SUFFIX = ".description";
    public static final String ICON_SUFFIX = ".icon";

    /**
     * Type of I18N message for the settings bundle. Provided toString() methods include a point
     * ('.') as prefix, followed by the type in lower case. E.g. '.description'.
     */
    public enum SettingsType {

        TITLE(SETTINGS_TYPE_TITLE_SUFFIX), DESCRIPTION(SETTINGS_TYPE_DESCRIPTION_SUFFIX);

        private final String type;

        SettingsType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    private static I18NResourcesProvider provider;

    private I18N() {
        // static class
    }


    public static I18NResourcesProvider getProvider() {
        if (isProviderAvailable())
            return provider;
        throw new IllegalStateException(I18N.class.getSimpleName()
                + " service is not initialized. Consider using setService(service) method with an initialized service before this call.");
    }

    /**
     * Checks if a provider is assigned and initialized
     * 
     * @return true iff provider not null and initialized
     */
    public static boolean isProviderAvailable() {
        return provider != null && provider.isInitialized();
    }

    /**
     * Assign a new {@link I18NResourcesProvider provider} for the service. The provider will also
     * get initialized if it was not already. This method will also call the
     * {@link I18NResourcesProvider#freeResources()} on the old provider if available.
     * 
     * @param newProvider
     *            the new provider to be assigned
     */
    public static void setProvider(I18NResourcesProvider newProvider) {
        if (isProviderAvailable()) {
            provider.freeResources();
        }
        provider = newProvider;
        if (provider != null && !provider.isInitialized()) {
            // not yet initialized
            provider.initialize();
        }
    }

    /** registers the properties of the given bundle on the global error bundle */
    public static void registerErrorBundle(ResourceBundle bundle) {
        registerErrorBundle(bundle, false);
    }

    /** registers the properties of the given bundle on the global gui bundle */
    public static void registerGUIBundle(ResourceBundle bundle) {
        registerGUIBundle(bundle, false);
    }

    /** registers the properties of the given bundle on the global userError bundle */
    public static void registerUserErrorMessagesBundle(ResourceBundle bundle) {
        registerUserErrorMessagesBundle(bundle, false);
    }

    /** registers the properties of the given bundle on the global userError bundle */
    public static void registerLogMessagesBundle(ResourceBundle bundle) {
        registerLogMessagesBundle(bundle, false);
    }

    /** registers the properties of the given bundle on the global settings bundle */
    public static void registerSettingsBundle(ResourceBundle bundle) {
        registerSettingsBundle(bundle, false);
    }

    /**
     * Registers the properties of the given bundle on the global error bundle
     * 
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    public static void registerErrorBundle(ResourceBundle bundle, boolean overwrite) {
        registerBundle(getProvider().getErrorBundle(), bundle, overwrite);
    }

    /**
     * Registers the properties of the given bundle on the global gui bundle
     * 
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    public static void registerGUIBundle(ResourceBundle bundle, boolean overwrite) {
        registerBundle(getProvider().getGUIBundle(), bundle, overwrite);
    }

    /**
     * Registers the properties of the given bundle on the global userError bundle
     * 
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    public static void registerUserErrorMessagesBundle(ResourceBundle bundle, boolean overwrite) {
        registerBundle(getProvider().getUserErrorMessagesBundle(), bundle, overwrite);
    }

    /**
     * Registers the properties of the given bundle on the global log messages bundle
     *
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    public static void registerLogMessagesBundle(ResourceBundle bundle, boolean overwrite) {
        registerBundle(getProvider().getLogMessagesBundle(), bundle, overwrite);
    }

    /**
     * Registers the properties of the given bundle on the global settings bundle
     * 
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    public static void registerSettingsBundle(ResourceBundle bundle, boolean overwrite) {
        registerBundle(getProvider().getSettingsBundle(), bundle, overwrite);
    }

    /**
     * Registers the given bundle in the targetBundle
     *
     * @param targetBundle
     *            the target bundle
     * @param bundle
     *            bundle to register
     * @param overwrite
     *            always false, internal api
     */
    private static void registerBundle(ExtensibleResourceBundle targetBundle, ResourceBundle bundle, boolean overwrite) {
        if (overwrite) {
            targetBundle.addResourceBundleAndOverwrite(bundle);
        } else {
            targetBundle.addResourceBundle(bundle);
        }
    }

    /**
     * Returns a message if found or the key if not found. Arguments <b>can</b> be specified which
     * will be used to format the String. In the {@link ResourceBundle} the String '{0}' (without ')
     * will be replaced by the first argument, '{1}' with the second and so on.
     * <p>
     * Catches the exception thrown by ResourceBundle in the latter case.
     * 
     * @param bundle
     *            the bundle that contains the key
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exists in the
     *         bundle
     **/
    public static String getMessage(ResourceBundle bundle, String key, Object... arguments) {
        try {
            return getMessageOptimistic(bundle, key, arguments);
        } catch (MissingResourceException e) {
            LogService.getRoot().debug(LogService.MARKER_DEVELOPMENT, "Missing I18N key: {}", key);
            return key;
        }
    }

    /**
     * Convenience method to call {@link #getMessage(ResourceBundle, String, Object...)} with return
     * value from {@link I18NResourcesProvider#getGUIBundle()} as {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exist in the bundle
     */
    public static String getGUIMessage(String key, Object... arguments) {
        return getMessage(getProvider().getGUIBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessageOrNull(ResourceBundle, String, Object...)} with
     * return value from {@link I18NResourcesProvider#getGUIBundle()} as {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or null if the key does not exist in the
     *         bundle
     */
    public static String getGUIMessageOrNull(String key, Object... arguments) {
        return getMessageOrNull(getProvider().getGUIBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessage(ResourceBundle, String, Object...)} with return
     * value from {@link I18NResourcesProvider#getErrorBundle()} as {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exist in the bundle
     */
    public static String getErrorMessage(String key, Object... arguments) {
        return getMessage(getProvider().getErrorBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessageOrNull(ResourceBundle, String, Object...)} with
     * return value from {@link I18NResourcesProvider#getErrorBundle()} as {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or null if the key does not exist in the
     *         bundle
     */
    public static String getErrorMessageOrNull(String key, Object... arguments) {
        return getMessageOrNull(getProvider().getErrorBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessage(ResourceBundle, String, Object...)} with return
     * value from {@link I18NResourcesProvider#getUserErrorMessagesBundle()} as
     * {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exist in the bundle
     */
    public static String getUserErrorMessage(String key, Object... arguments) {
        return getMessage(getProvider().getUserErrorMessagesBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessageOrNull(ResourceBundle, String, Object...)} with
     * return value from {@link I18NResourcesProvider#getUserErrorMessagesBundle()} as
     * {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or null if the key does not exist in the
     *         bundle
     */
    public static String getUserErrorMessageOrNull(String key, Object... arguments) {
        return getMessageOrNull(getProvider().getUserErrorMessagesBundle(), key, arguments);
    }

    /**
     * Convenience method to call {@link #getMessage(ResourceBundle, String, Object...)} with return
     * value from {@link I18NResourcesProvider#getSettingsBundle()} as {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exist in the bundle
     */
    public static String getSettingsMessage(String key, SettingsType type, Object... arguments) {
        return getMessage(getProvider().getSettingsBundle(), key + type, arguments);
    }

    /**
     * Convenience method to call {@link #getMessageOrNull(ResourceBundle, String, Object...)} with
     * return value from {@link I18NResourcesProvider#getSettingsBundle()} as
     * {@link ResourceBundle}.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or null if the key does not exist in the
     *         bundle
     */
    public static String getSettingsMessageOrNull(String key, SettingsType type, Object... arguments) {
        return getMessageOrNull(getProvider().getSettingsBundle(), key + type, arguments);
    }

    /**
     * Returns a message if found or <code>null</code> if not.
     *
     * Arguments <b>can</b> be specified which will be used to format the String. In the
     * {@link ResourceBundle} the String '{0}' (without ') will be replaced by the first argument,
     * '{1}' with the second and so on.
     * 
     * @param bundle
     *            the bundle that contains the key
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or null if key not exists in the bundle
     */
    public static String getMessageOrNull(ResourceBundle bundle, String key, Object... arguments) {
        try {
            return getMessageOptimistic(bundle, key, arguments);
        } catch (MissingResourceException e) {
            return null;
        }
    }

    /**
     * This will return the value of the property gui.label.-key- of the GUI bundle or the key
     * itself if unknown.
     *
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key, or the key if it does not exist in the bundle
     */
    public static String getGUILabel(String key, Object... arguments) {
        return getMessage(getProvider().getGUIBundle(), "gui.label." + key, arguments);
    }

    /**
     * Gets the original result of {@link Locale#getDefault()} (before it is exchanged with a custom
     * locale due to our language chooser).
     *
     * @return the original default locale object before it is changed due to our I18N combobox
     * @since 8.2.1
     */
    public static Locale getOriginalLocale() {
        return getProvider().getOriginalLocale();
    }

    /**
     * Returns a message if found or the key if not found. Arguments <b>can</b> be specified which
     * will be used to format the String. In the {@link ResourceBundle} the String '{0}' (without ')
     * will be replaced by the first argument, '{1}' with the second and so on.
     *
     * @param bundle
     *            the bundle that contains the key
     * @param key
     *            key – the key for the desired string
     * @param arguments
     *            optional arguments for message formatter
     * @return the formatted string for the given key
     * @throws java.util.MissingResourceException
     *             – if no object for the given key can be found
     */
    private static String getMessageOptimistic(ResourceBundle bundle, String key, Object... arguments) throws MissingResourceException {
        String message = bundle.getString(key);
        if (arguments == null || arguments.length == 0) {
            return message;
        }
        replaceThrowableWithDescription(arguments);
        return MessageFormat.format(message, arguments);
    }

    private static void replaceThrowableWithDescription(Object... arguments) {
        for (int i = 0; i < arguments.length; i++) {
            Object argument = arguments[i];
            if (argument instanceof Throwable throwable) {
                String causeTrace = Stream.iterate(throwable, Objects::nonNull, Throwable::getCause)
                        .map(Throwable::getMessage)
                        .filter(Objects::nonNull)
                        .collect(Collectors.joining(" caused by: "));
                arguments[i] = causeTrace;
            }
        }
    }

    /**
     * <p>
     * Registers a new language tag to be shown in the SingularityEngine Settings
     * </p>
     *
     * <p>
     * Important: Use underscore for the .properties files, but hyphen for the language tag!<br/>
     * Examples:
     * <table border="1">
     * <tr>
     * <th>Language Tag</th>
     * <th>Filename</th>
     * </tr>
     * <tr>
     * <td>English Fallback</td>
     * <td>MyExtGUI.properties</td>
     * </tr>
     * <tr>
     * <td>"de"</td>
     * <td>MyExtGUI_de.properties</td>
     * </tr>
     * <tr>
     * <td>"de-AT"</td>
     * <td>MyExtGUI_de_AT.properties</td>
     * </tr>
     * </table>
     * If de-AT is the selected language, first de-AT files are checked, second de, finally
     * {@link Locale#ROOT}
     *
     * @param languageTag
     *            An IETF BCP 47 language tag, i.e. "fr", "zh", "de" or "en-GB"
     * @throws IllegalArgumentException
     *             if the given {@code languageTag} is not valid
     * @throws NullPointerException
     *             if {@code languageTag} is {@code null}
     */
    public static void registerLanguage(String languageTag) {
        // Check if language key is valid
        if (Locale.forLanguageTag(languageTag).getLanguage().isEmpty()) {
            LogService.getRoot()
                    .info(LogService.MARKER_DEVELOPMENT, "Registration of language \"{}\" failed. Not a valid IETF BCP 47 language tag.", languageTag);
            throw new IllegalArgumentException(languageTag + " is not a valid IETF BCP 47 language tag.");
        }

        ParameterType type = PropertyService.getParameterType(EngineProperties.GENERAL_LOCALE_LANGUAGE);
        if (!(type instanceof ParameterTypeCategory lng)) {
            LogService.getRoot()
                    .error(LogService.MARKER_DEVELOPMENT, "Registration of language \"{}\" failed. I18N service might not be initialized.", languageTag);
            return;
        }
        if (ArrayUtils.contains(lng.getValues(), languageTag)) {
            // already registered
            return;
        }
        // Add language key to the end, not sorted
        String[] languages = (String[]) ArrayUtils.add(lng.getValues(), languageTag);
        PropertyService.registerParameter(new ParameterTypeCategory(lng.getKey(), lng.getDescription(), languages, lng.getDefault()));
    }
}
