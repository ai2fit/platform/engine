/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.ExpressionParserConstants;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function} for getting part of a date.
 *
 * @author David Arnu
 *
 */
public class DateGet extends AbstractFunction {

    public DateGet() {
        super("date.date_get", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.NUMERIC);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        ExpressionType type = getResultType(inputEvaluators);

        if (inputEvaluators.length == 2) {

            ExpressionEvaluator date = inputEvaluators[0];
            ExpressionEvaluator unit = inputEvaluators[1];
            return new SimpleExpressionEvaluator(makeDoubleCallable(date, unit, null, null), type, isResultConstant(inputEvaluators));
        } else {
            ExpressionEvaluator date = inputEvaluators[0];
            ExpressionEvaluator unit = inputEvaluators[1];
            ExpressionEvaluator locale = inputEvaluators[2];
            ExpressionEvaluator timeZone = inputEvaluators[3];
            return new SimpleExpressionEvaluator(makeDoubleCallable(date, unit, locale, timeZone), type, isResultConstant(inputEvaluators));
        }

    }

    private NumericSupplier makeDoubleCallable(ExpressionEvaluator date, ExpressionEvaluator unit, ExpressionEvaluator locale, ExpressionEvaluator timeZone) {
        final TimestampSupplier funcDate = date.getTimestampSupplier();
        final NominalSupplier funcUnit = unit.getNominalSupplier();
        final NominalSupplier funcLocale;
        final NominalSupplier funcTimeZone;

        if (locale != null) {
            funcLocale = locale.getNominalSupplier();
        } else {
            // create an dummy ExpressionEvaluator for the missing locale argument
            locale = new SimpleExpressionEvaluator("", ExpressionType.NOMINAL);
            funcLocale = () -> null;
        }
        if (timeZone != null) {
            funcTimeZone = timeZone.getNominalSupplier();
        } else {
            // create an dummy ExpressionEvaluator for the missing time zone argument
            timeZone = new SimpleExpressionEvaluator("", ExpressionType.NOMINAL);
            funcTimeZone = () -> null;
        }
        try {
            final long valueDate = date.isConstant() ? funcDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
            final String valueUnit = unit.isConstant() ? funcUnit.getOrThrow() : null;
            final String valueLocale = locale.isConstant() ? funcLocale.getOrThrow() : null;
            final String valueTimezone = timeZone.isConstant() ? funcTimeZone.getOrThrow() : null;

            // only likely cases checked:

            // all constant values
            if (date.isConstant() && unit.isConstant() && locale.isConstant() && timeZone.isConstant()) {
                final double result = compute(valueDate, valueUnit, valueLocale, valueTimezone);

                return () -> result;
                // constant date and unit is not constant
            } else if (date.isConstant() && !unit.isConstant()) {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(valueDate, funcUnit.getOrThrow(), valueLocale, valueTimezone);
                } else {
                    return () -> compute(valueDate, funcUnit.getOrThrow(), funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
            } else if (!date.isConstant() && unit.isConstant()) {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(funcDate.getOrThrow(), valueUnit, valueLocale, valueTimezone);
                } else {
                    return () -> compute(funcDate.getOrThrow(), valueUnit, funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
                // date and unit are not constant
            } else {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(funcDate.getOrThrow(), funcUnit.getOrThrow(), valueLocale, valueTimezone);
                } else {
                    return () -> compute(funcDate.getOrThrow(), funcUnit.getOrThrow(), funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     *
     * @param date
     * @param unit
     * @param valueLocale
     * @param valueTimeZone
     * @return the value of the selected unit of the given date
     */
    protected double compute(long date, String unit, String valueLocale, String valueTimeZone) {
        Locale locale;
        TimeZone zone;
        if (valueLocale == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(valueLocale);
        }
        if (valueTimeZone == null) {
            zone = TimeZone.getDefault();
        } else {
            zone = TimeZone.getTimeZone(valueTimeZone);
        }

        // for missing values as arguments, a missing value is returned
        if (ValueType.isMissing(date) || unit == null) {
            return Double.NaN;
        }

        Calendar cal = Calendar.getInstance(zone, locale);
        cal.setTimeInMillis(date);
        double result;

        switch (unit) {
            case ExpressionParserConstants.DATE_UNIT_YEAR:
                result = cal.get(Calendar.YEAR);
                break;
            case ExpressionParserConstants.DATE_UNIT_MONTH:
                result = cal.get(Calendar.MONTH);
                break;
            case ExpressionParserConstants.DATE_UNIT_WEEK:
                result = cal.get(Calendar.WEEK_OF_YEAR);
                break;

            case ExpressionParserConstants.DATE_UNIT_DAY:
                result = cal.get(Calendar.DAY_OF_MONTH);
                break;

            case ExpressionParserConstants.DATE_UNIT_HOUR:
                result = cal.get(Calendar.HOUR_OF_DAY);
                break;
            case ExpressionParserConstants.DATE_UNIT_MINUTE:
                result = cal.get(Calendar.MINUTE);
                break;
            case ExpressionParserConstants.DATE_UNIT_SECOND:
                result = cal.get(Calendar.SECOND);
                break;
            case ExpressionParserConstants.DATE_UNIT_MILLISECOND:
                result = cal.get(Calendar.MILLISECOND);
                break;
            default:
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "unit constant", "second");

        }

        return result;
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes.length != 2 && inputTypes.length != 4) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), "2", "4", inputTypes.length);
        }
        ExpressionType firstType = inputTypes[0];
        ExpressionType secondType = inputTypes[1];

        if (firstType != ExpressionType.TIMESTAMP) {
            throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "date", "first");
        }
        if (secondType != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "second");
        }

        if (inputTypes.length == 4) {
            if (inputTypes[2] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "third");
            }
            if (inputTypes[3] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "fourth");
            }
        }
        return ExpressionType.INTEGER;

    }

}
