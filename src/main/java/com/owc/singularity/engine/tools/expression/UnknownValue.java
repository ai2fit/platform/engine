/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * This is an enumeration for possible unknown values. This is used to determine, of which type an
 * returned unknown actually is.
 * 
 * @author Sebastian Land
 */
public enum UnknownValue {

    UNKNOWN_NOMINAL(ValueType.NOMINAL),
    // UNKNOWN_NUMERICAL(Ontology.NUMERICAL), Numerical Unknowns must be encoded by Double.NaN
    UNKNOWN_BOOLEAN(ValueType.NOMINAL), UNKNOWN_DATE(ValueType.TIMESTAMP);

    private ValueType valueType;

    UnknownValue(ValueType nominal) {
        this.valueType = nominal;
    }

    public ValueType getValueType() {
        return valueType;
    }
}
