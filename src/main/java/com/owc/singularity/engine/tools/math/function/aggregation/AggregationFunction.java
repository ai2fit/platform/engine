/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * An aggregation function which calculates the value for a given value array.
 * 
 * @author Tobias Malbrecht, Ingo Mierswa
 * 
 */
public interface AggregationFunction {

    /**
     * Returns the name of the aggregation function.
     */
    public String getName();

    /**
     * Consider a new value by updating counters.
     */
    public void update(double value) throws UnsupportedOperationException;

    /**
     * Consider a new value by updating counters.
     */
    public void update(String value) throws UnsupportedOperationException;

    /**
     * Consider a new value by updating counters.
     */
    public void update(long value) throws UnsupportedOperationException;

    /**
     * Returns the function value.
     */
    public double aggregateNumericValue() throws UnsupportedOperationException;

    /**
     * Returns the function value.
     */
    public String aggregateNominalValue() throws UnsupportedOperationException;

    /**
     * Returns the function value.
     */
    public long aggregateTimestampValue() throws UnsupportedOperationException;

    /**
     * Reset the counters.
     */
    public abstract void reset();


    /**
     * Returns whether this function supports attributes of the given type, where valueType is one
     * of the static value types defined in {@link ValueType}.
     */
    public boolean supportsValueType(ValueType valueType);

    /**
     * Returns the result type of this {@link AggregationFunction} when applied on data of type
     * inputType as one of the static value types defined in {@link ValueType}.
     */
    public ValueType getValueTypeOfResult(ValueType[] inputTypes);


}
