/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.time.Instant;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.FunctionInputException;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function} for calculating the difference
 * between two dates in milliseconds.
 *
 * @author David Arnu
 *
 */
public class DateDiff extends Abstract2DateInputIntegerOutputFunction {

    public DateDiff() {
        super("date.date_diff", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS);
    }

    @Override
    protected double compute(long left, long right, String valueLocale, String valueTimezone) {
        if (ValueType.isMissing(left) || ValueType.isMissing(right)) {
            return ValueType.MISSING_NUMERIC;
        } else {
            return Instant.ofEpochMilli(right).minusMillis(left).toEpochMilli();
        }
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        // locale and time zone arguments just accepted out of compatibility reasons, but are no
        // longer documented
        if (inputTypes.length != 2 && inputTypes.length != 4) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), "2", inputTypes.length);
        }
        ExpressionType firstType = inputTypes[0];
        ExpressionType secondType = inputTypes[1];
        if (firstType != ExpressionType.TIMESTAMP || secondType != ExpressionType.TIMESTAMP) {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "date");
        }
        if (inputTypes.length == 4) {
            if (inputTypes[2] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_date_diff_depricated", getFunctionName());
            }
            if (inputTypes[3] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_date_diff_depricated", getFunctionName());
            }
        }
        return ExpressionType.INTEGER;

    }

}
