/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.status.StatusLogger;


/**
 * Helper class to get platform information.
 *
 * @author Nils Woehler
 */
@SuppressWarnings({ "PMD.AvoidSynchronizedAtMethodLevel", "PMD.TooManyMethods", "PMD.GodClass" })
public final class PlatformUtilities {

    private static final String PLATFORM_PROPERTIES = "conf/platform.properties";
    private static final String PLATFORM_PROPERTY_KEY = "platform";
    private static final String GRADLE_PROPERTIES = "gradle.properties";
    private static final String VERSION_PROPERTY_KEY = "version";
    private static final String REVISION_PROPERTY_KEY = "revision";

    /**
     * The name of the SingularityEngine Studio Launcher Jar
     */
    private static final String AI2FIT_STUDIO_LAUNCHER_JAR = "ai2fit-studio-launcher.jar";

    /**
     * A regular expression that matches any version of the SingularityEngine Studio Core Jar
     */
    private static final String AI2FIT_STUDIO_CORE_REGEX = ".+ai2fit-studio-core-\\d.*\\.jar";

    /**
     * Elements potentially added to the library path by build tools.
     */
    private static final Set<String> BUILD_PATH_ELEMENTS = new HashSet<>(4);
    static {
        BUILD_PATH_ELEMENTS.add("build");
        BUILD_PATH_ELEMENTS.add("out");
        BUILD_PATH_ELEMENTS.add("classes");
        BUILD_PATH_ELEMENTS.add("ai2fit-studio-core");
        BUILD_PATH_ELEMENTS.add("ai2fit-studio-commons");
    }

    /**
     * The suffix of a path from where the tests are started via Gradle.<br/>
     * (Gradle copies created jars to '$PROJECT/target/libs')
     */
    private static final String TEST_ENVIRONMENT_SUFFIX = File.separatorChar + "target" + File.separatorChar + "libs";

    private static final Logger log = StatusLogger.getLogger();

    /** The name of the property indicating the home directory of SingularityEngine. */
    public static final String PROPERTY_SINGULARITY_HOME = "singularity.home";

    /**
     * All available release platforms.
     */
    public enum Platform {
        /**
         * Platform independent release
         */
        ANY,
        /**
         * Windows 32-bit
         */
        WIN32,
        /**
         * Windows 64-bit
         */
        WIN64,
        /**
         * Apple OS X
         */
        OSX
    }

    /**
     * Cache for current release platform.
     */
    private static Platform currentPlatform = null;

    /**
     * Cache for version of current release.
     */
    private static String currentVersion = null;

    /**
     * Cache for revision of current release.
     */
    private static String currentRevision = null;

    private static boolean isInitialized = false;

    private static final Object INIT_VERSION_LOCK = new Object();

    /**
     * Utility class constructor.
     */
    private PlatformUtilities() {
        throw new AssertionError();
    }

    /**
     * Initializes the PlatformUtilities caches
     */
    public static synchronized void initialize() {
        if (!isInitialized) {
            // silently ensure that the SingularityEngine Home system property is set
            ensureSingularityHomeSet();
            initializeReleasePlatform();
            initializeReleaseVersion();
            initializeReleaseRevision();
            isInitialized = true;
        }
    }

    /**
     * @return the platform the SingularityEngine Studio release was built for. Will return
     *         <code>null</code> in case platform was not defined.
     */
    public static synchronized Platform getReleasePlatform() {
        if (!isInitialized) {
            initialize();
        }
        return currentPlatform;
    }

    /**
     * Loads release platform from properties file.
     */
    private static void initializeReleasePlatform() {
        String platformProperty = readConfigProperty(PLATFORM_PROPERTIES, PLATFORM_PROPERTY_KEY);
        if (platformProperty == null) {
            log.info("Release platform not defined.");
        } else {
            currentPlatform = Platform.valueOf(platformProperty.toUpperCase(Locale.UK));
            log.info("Release platform: " + currentPlatform);
        }
    }

    /**
     * @return the current version of the platform release as {@link String}.
     */
    public static synchronized String getReleaseVersion() {
        if (!isInitialized) {
            initialize();
        }
        return currentVersion;
    }

    /**
     * Initializes the current version by reading the version.properties file
     */
    private static void initializeReleaseVersion() {
        synchronized (INIT_VERSION_LOCK) {
            currentVersion = readResourceProperty(VERSION_PROPERTY_KEY);
            if (currentVersion == null) {
                currentVersion = getManifestVersion();
                if (currentVersion == null) {
                    log.info("Could not read current version from resources. Looking for 'gradle.properties'...");
                    currentVersion = readConfigProperty(GRADLE_PROPERTIES, VERSION_PROPERTY_KEY);
                    if (currentVersion == null) {
                        throw new IllegalStateException("Could not initialize SingularityEngine Studio version from properties file");
                    }
                }
            }
        }
    }

    private static String getManifestVersion() {
        final String packageSpecificationVersion = PlatformUtilities.class.getPackage().getSpecificationVersion();
        if (packageSpecificationVersion != null)
            return packageSpecificationVersion;
        final Manifest manifest = getManifest();
        if (manifest == null)
            return null;
        return manifest.getMainAttributes().getValue(Attributes.Name.SPECIFICATION_VERSION);
    }

    public static Manifest getManifest() {
        Enumeration<URL> resources;
        try {
            resources = PlatformUtilities.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
        } catch (IOException ignored) {
            return null;
        }
        while (resources.hasMoreElements()) {
            try {
                Manifest manifest = new Manifest(resources.nextElement().openStream());
                // check that this is your manifest and do what you need or get the next one
                final String product = manifest.getMainAttributes().getValue(Attributes.Name.SPECIFICATION_TITLE);
                if (product != null && product.equals("Singularity Engine")) {
                    return manifest;
                }
            } catch (IOException ignored) {
            }
        }
        return null;
    }

    /**
     * @return the current version of the platform release as {@link String}.
     */
    public static synchronized String getReleaseRevision() {
        if (!isInitialized) {
            initialize();
        }
        return currentRevision;
    }

    /**
     * Initializes the current version by reading the version.properties file
     */
    private static void initializeReleaseRevision() {
        synchronized (INIT_VERSION_LOCK) {
            currentRevision = readResourceProperty(REVISION_PROPERTY_KEY);
            if (currentRevision == null) {
                log.info("Could not read current revision from resources.");
            }
        }
    }

    /**
     * @return the system property 'singularity.home'.
     */
    public static synchronized String getSingularityHome() {
        return System.getProperty(PROPERTY_SINGULARITY_HOME);
    }

    /**
     * Reads a value from a .properties file stored in the resources of this JAR.
     */
    private static String readResourceProperty(String propertyKey) {
        Properties props = new Properties();
        try (InputStream resourceAsStream = PlatformUtilities.class.getResourceAsStream("/com/owc/singularity/resources/tools/version.properties")) {
            if (resourceAsStream == null) {
                log.info("Version resource file not found at '/com/owc/singularity/resources/tools/version.properties'.");
                return null;
            } else {
                props.load(resourceAsStream);
            }
        } catch (IOException e) {
            log.warn("Error reading version properties from resources file!" + e.getLocalizedMessage());
        }
        return props.getProperty(propertyKey);
    }

    /**
     * Reads a value from a .properties file stored relative to the folder specified by the system
     * property 'singularity.home'.
     */
    private static String readConfigProperty(String relativePath, String propertyKey) {
        String home = getSingularityHome();
        if (home == null) {
            log.warn("Property 'singularity.home' not set. Cannot read property file '" + relativePath + "'");
            return null;
        }
        File propertyFile = new File(home, relativePath);
        if (propertyFile.canRead()) {
            Properties props = new Properties();
            try (InputStreamReader reader = new InputStreamReader(new FileInputStream(propertyFile), StandardCharsets.UTF_8)) {
                props.load(reader);
            } catch (IOException e) {
                log.warn("Error reading properties file! " + e.getLocalizedMessage());
            }
            return props.getProperty(propertyKey);
        } else {
            log.warn("Property file (" + propertyFile + ") not found or not readable!");
            return null;
        }
    }

    /**
     * Ensures that the environment variable 'singularity.home' is set by searching for
     * SingularityEngine Jars in classpath and build dir.
     */
    public static synchronized void ensureSingularityHomeSet() {
        if (getSingularityHome() == null) {
            log.debug("Property " + PROPERTY_SINGULARITY_HOME + " is not set. Guessing.");
            if (!searchInClassPath()) {
                try {
                    log.debug("Property " + PROPERTY_SINGULARITY_HOME + " not found via search in Classpath. Searching in build directory.");
                    searchInBuildDir();
                } catch (Throwable e) { // NOPMD
                    // important: not only URI Syntax Exception since the
                    // program must not crash
                    // in any case!!!
                    // For example: RapidNet integration as applet into
                    // Siebel would cause
                    // problem with new File(...)
                    log.error("Failed to locate 'singularity.home'! Cause: " + e.getLocalizedMessage());
                }
            }
        } else {
            log.info("{} is ''{}''.", PROPERTY_SINGULARITY_HOME, getSingularityHome());
        }
    }

    /**
     * Checks the Java Classpath for singularity studio launcher or core jars.
     *
     * @return
     */
    private static boolean searchInClassPath() {
        log.info("Searching in Java classpath for SingularityEngine Studio jars...");
        String classpath = System.getProperty("java.class.path");
        String[] pathComponents = classpath.split(File.pathSeparator);

        for (int i = 0; i < pathComponents.length; i++) {
            String path = pathComponents[i].trim();
            /*
             * Search for either 'ai2fit-studio-launcher.jar' or for
             * 'ai2fit-studio-core-x.x.xxx.jar'
             */
            if (path.matches(AI2FIT_STUDIO_CORE_REGEX) || path.endsWith(AI2FIT_STUDIO_LAUNCHER_JAR)) {
                File jar = new File(path).getAbsoluteFile();
                log.info("Trying parent directory of '" + jar + "'...");

                // Retrieve the directory the jar is placed in
                File dir = jar.getParentFile();
                if (dir == null) {
                    log.error("Failed to retrieve 'singularity.home'. Parent of jar is not a directory!");
                } else {
                    dir = retrieveHomeFromLibraryDir(dir);
                    if (dir != null && dir.isDirectory()) {
                        log.info("Gotcha! 'singularity.home' is: " + dir);

                        System.setProperty(PROPERTY_SINGULARITY_HOME, dir.getAbsolutePath());
                        return true;
                    } else {
                        log.error("Failed to retrieve 'singularity.home'. Parent of jar directory is not a directory!");
                    }
                }
            }
        }
        return false;
    }

    /**
     * Searches for 'singularity.home' assuming a development environment. Start looking for
     * 'ai2fit-studio' folder.
     */
    private static void searchInBuildDir() {
        String url = PlatformUtilities.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        if (url == null) {
            log.error("Failed to locate 'singularity.home'. Could not locate base directory of classes!");
        } else {
            try {
                url = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                // should not happen
                log.error("Failed to decode url! Path could be wrong: " + url);
            }

            // Use parent file, not the JAR itself
            File buildDir = new File(url).getParentFile();
            log.info("Trying base directory of classes (build) '" + buildDir + "' ...");

            // check if buildDir is a directory
            if (buildDir != null && buildDir.isDirectory()) {

                File home = retrieveHomeFromLibraryDir(buildDir);

                if (home != null && home.isDirectory()) {

                    // We have found rmHome
                    log.info("Gotcha! singularity.home is: " + home);
                    try {
                        System.setProperty(PROPERTY_SINGULARITY_HOME, home.getCanonicalPath());
                    } catch (IOException e) {
                        System.setProperty(PROPERTY_SINGULARITY_HOME, home.getAbsolutePath());
                    }
                } else {
                    log.error("Failed to locate 'singularity.home'! Parent dir of build directory does not exist.");
                }
            } else {
                log.error("Failed to locate 'singularity.home'! Build directory does not exists or isn't a directory.");
            }
        }
    }

    /**
     *
     * @param buildDir
     * @return
     */
    private static File retrieveHomeFromLibraryDir(File buildDir) {
        /*
         * Search for the project directory of the current build. If the search was started via
         * Gradle, we are in 'target/libs/'. If it was started via an IDE we are in 'build/' or
         * 'bin/'.
         */
        File projectDir = null;
        if (buildDir.getAbsolutePath().endsWith(TEST_ENVIRONMENT_SUFFIX)) {
            // If it was started via Gradle, go two levels up so we're in the
            // project's directory.
            log.info("Library located in 'target/libs/'. Assuming Gradle test task invocation.");
            projectDir = buildDir.getParentFile().getParentFile();
        } else {
            // Otherwise assume that the search was started via IDE or from productive environment
            // and use the library directory parent
            projectDir = buildDir.getParentFile();

        }

        // Check if we are in a development or test environment
        while (projectDir != null && BUILD_PATH_ELEMENTS.contains(projectDir.getName())) {
            projectDir = projectDir.getParentFile();
        }

        return projectDir;
    }

}
