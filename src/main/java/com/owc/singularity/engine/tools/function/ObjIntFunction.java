package com.owc.singularity.engine.tools.function;

import java.util.function.BiConsumer;

/**
 * Represents a function that accepts an object-valued and a {@code int}-valued argument, and
 * returns an object result.
 *
 * @param <T>
 *            the type of the object argument to the function
 * @param <R>
 *            the type of the object return value to the function
 *
 * @see BiConsumer
 */
@FunctionalInterface
public interface ObjIntFunction<T, R> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t
     *            the first input argument
     * @param value
     *            the second input argument
     */
    R accept(T t, int value);
}
