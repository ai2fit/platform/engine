/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.conversion;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 *
 * A {@link Function} parsing a string to a date with respect to the locale and pattern strings.
 *
 * @author Marcel Seifert
 *
 */
public class DateParseCustom extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     *
     */
    public DateParseCustom() {
        super("conversion.date_parse_custom", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.TIMESTAMP);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {

        if (inputEvaluators.length != 2 && inputEvaluators.length != 3) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), 2, 3, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        return new SimpleExpressionEvaluator(type, makeDateCallable(inputEvaluators), isResultConstant(inputEvaluators));
    }

    /**
     * Builds a Date Callable from the given string arguments
     *
     * @param inputEvaluators
     *            The input date, input pattern and optional the input locale
     * @return The resulting TimestampSupplier
     */
    protected TimestampSupplier makeDateCallable(ExpressionEvaluator... inputEvaluators) {

        final ExpressionEvaluator date = inputEvaluators[0];
        final ExpressionEvaluator pattern = inputEvaluators[1];

        final NominalSupplier funcDateString = date.getNominalSupplier();
        final NominalSupplier funcPattern = pattern.getNominalSupplier();

        try {
            final String valuePattern = pattern.isConstant() ? funcPattern.getOrThrow() : null;
            final String valueDate = date.isConstant() ? funcDateString.getOrThrow() : null;

            if (inputEvaluators.length > 2) {
                ExpressionEvaluator locale = inputEvaluators[2];
                final NominalSupplier funcLocale = locale.getNominalSupplier();
                final String valueLocale = locale.isConstant() ? funcLocale.getOrThrow() : null;

                if (pattern.isConstant()) {
                    if (date.isConstant() && locale.isConstant()) {
                        return () -> compute(valueDate, valuePattern, valueLocale);
                    } else if (date.isConstant()) {
                        return () -> compute(valueDate, valuePattern, funcLocale.getOrThrow());
                    } else if (locale.isConstant()) {
                        return () -> compute(funcDateString.getOrThrow(), valuePattern, valueLocale);
                    } else {
                        return () -> compute(funcDateString.getOrThrow(), valuePattern, funcLocale.getOrThrow());
                    }
                } else {
                    if (date.isConstant() && locale.isConstant()) {
                        return () -> compute(valueDate, funcPattern.getOrThrow(), valueLocale);
                    } else if (date.isConstant()) {
                        return () -> compute(valueDate, funcPattern.getOrThrow(), funcLocale.getOrThrow());
                    } else if (locale.isConstant()) {
                        return () -> compute(funcDateString.getOrThrow(), funcPattern.getOrThrow(), valueLocale);
                    } else {
                        return () -> compute(funcDateString.getOrThrow(), funcPattern.getOrThrow(), funcLocale.getOrThrow());
                    }
                }
            } else {
                if (date.isConstant() && pattern.isConstant()) {

                    return () -> compute(valueDate, valuePattern);
                } else if (date.isConstant()) {
                    return () -> compute(valueDate, funcPattern.getOrThrow());
                } else if (pattern.isConstant()) {
                    return () -> compute(funcDateString.getOrThrow(), valuePattern);
                } else {
                    return () -> compute(funcDateString.getOrThrow(), funcPattern.getOrThrow());
                }
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two string input values.
     *
     * @param dateString
     *            the input date string
     * @param patternString
     *            the input pattern string
     * @return the result of the computation.
     */
    protected long compute(String dateString, String patternString) {

        String defaultLocale = Locale.getDefault().getISO3Language();
        return compute(dateString, patternString, defaultLocale);
    }

    /**
     * Computes the result for three string input values.
     *
     * @param dateString
     *            the input date string
     * @param patternString
     *            the input pattern string
     * @param localeString
     *            the input locale string
     * @return the result of the computation.
     */
    protected long compute(String dateString, String patternString, String localeString) {
        if (ValueType.isMissing(dateString) || patternString == null || localeString == null) {
            return ValueType.MISSING_TIMESTAMP;
        }
        Locale locale = new Locale(localeString);
        SimpleDateFormat simpleDateFormatter;
        Date parsedDate;
        try {
            simpleDateFormatter = new SimpleDateFormat(patternString, locale);
            parsedDate = simpleDateFormatter.parse(dateString);
        } catch (IllegalArgumentException e) {
            throw new FunctionInputException("invalid_argument.custom_format", getFunctionName());
        } catch (java.text.ParseException e) {
            throw new FunctionInputException("invalid_argument.date", getFunctionName());
        }
        Calendar cal = Calendar.getInstance(locale);
        cal.setTime(parsedDate);
        return cal.getTimeInMillis();
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType date = inputTypes[0];
        ExpressionType pattern = inputTypes[1];

        if (date != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type.argument", 1, getFunctionName(), "nominal");
        } else if (pattern != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type.argument", 2, getFunctionName(), "nominal");
        } else if (inputTypes.length > 2 && inputTypes[2] != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type.argument", 3, getFunctionName(), "nominal");
        }

        return ExpressionType.TIMESTAMP;
    }

}
