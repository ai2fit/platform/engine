package com.owc.singularity.engine.tools.io.csv;


import com.owc.singularity.engine.operator.error.ProcessStoppedException;

/**
 * This is an interface that can be used by operators or other classes to give long running
 * subroutines the means to stop the execution.
 * 
 * @author Sebastian Land
 *
 */
public interface Interruptor {

    public void checkForStop() throws ProcessStoppedException;
}
