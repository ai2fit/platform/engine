package com.owc.singularity.engine.tools;

public class NoOpProgressListener implements ProgressListener {

    @Override
    public void setTotal(int total) {

    }

    @Override
    public void setCompleted(int completed) {

    }

    @Override
    public void complete() {

    }

    @Override
    public void setMessage(String message) {

    }

    @Override
    public int getCompleted() {
        return 0;
    }

    @Override
    public int getTotal() {
        return 0;
    }

    @Override
    public void progress() {
        ProgressListener.super.progress();
    }
}
