/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools.usagestats;


import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.parameter.ParameterChangeListener;


/**
 * This is the control for all telemetry of Studio. All sending and requesting online access can be
 * controlled via parameters. Changing parameters leads to changes in this class and all the places
 * that access online services need to ask their specific setting if access is allowed.
 *
 * @author Andreas Timm
 */
public enum Telemetry implements ParameterChangeListener {

    /**
     * Set ALL_TELEMETRY to true to prohibit any communication from Studio. Reacts to Parameter
     * change for the configured key to update the value.
     */
    ACCOUNT("singularity.studio.deny.account", false), ALL_TELEMETRY("singularity.studio.deny.allcommunication", false), CTA("singularity.studio.deny.cta",
            false), MARKETPLACE("singularity.studio.deny.marketplace", false), NEWS("singularity.studio.deny.news", false), USAGESTATS(
                    "singularity.studio.deny.usagestats",
                    false), WISDOM_OF_CROWDS("singularity.studio.deny.woc", false), EDUCATION("singularity.studio.deny.education", false);

    private String key;
    private boolean value;

    Telemetry(String key, boolean defaultValue) {
        this.key = key;
        value = defaultValue;
        final String parameterValue = PropertyService.getParameterValue(key);
        if (parameterValue != null) {
            value = Boolean.parseBoolean(parameterValue);
        }

        PropertyService.registerParameterChangeListener(this);
    }

    /**
     * Check if this setting is prohibited. Always checks if ALL_TELEMETRY is prohibited.
     *
     * @return true if this {@link Telemetry} or the ALL_TELEMETRY setting was denied.
     */
    public boolean isDenied() {
        return ALL_TELEMETRY.value || value;
    }


    @Override
    public void informParameterChanged(String key, String value) {
        if (this.key.equals(key)) {
            this.value = Boolean.parseBoolean(value);
        }
    }


    @Override
    public void informParameterSaved() {
        // intentionally left empty, nothing to be done here
    }


    /**
     * Getter for the key which is used to be able to change the value through the
     * {@link PropertyService}
     *
     * @return the key for this instance
     */
    public String getKey() {
        return key;
    }
}
