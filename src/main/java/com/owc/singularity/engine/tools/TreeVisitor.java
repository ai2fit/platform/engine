package com.owc.singularity.engine.tools;


public interface TreeVisitor<N, L> {

    TreeVisitResult preVisitNode(N node);

    TreeVisitResult visitLeaf(L leaf);

    TreeVisitResult postVisitNode(N node);
}
