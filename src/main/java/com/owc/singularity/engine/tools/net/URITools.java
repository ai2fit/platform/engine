package com.owc.singularity.engine.tools.net;

import java.net.URI;
import java.net.URISyntaxException;

public class URITools {

    private URITools() {
        // utility class
    }

    public static URI getWithoutQueryAndFragment(URI uri) {
        try {
            // Ignore the query and fragment parts of the input url
            return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, null);
        } catch (URISyntaxException ignored) {
            return uri;
        }
    }

    public static URI getHostUri(URI uri) {
        try {
            // Ignore the path, query and fragment parts of the input url
            return new URI(uri.getScheme(), uri.getAuthority(), null, null, null);
        } catch (URISyntaxException ignored) {
            return uri;
        }
    }

    public static URI getWithUserInfo(URI uri, String userInfo) {
        try {
            return new URI(uri.getScheme(), userInfo, uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException ignored) {
            return uri;
        }
    }
}
