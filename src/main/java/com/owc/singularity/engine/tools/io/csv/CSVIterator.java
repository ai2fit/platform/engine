package com.owc.singularity.engine.tools.io.csv;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * This class implements an iterator for CSV files. It will return the next value once completely
 * read from input stream. A null signals the end of line.
 *
 * @author Sebastian Land
 *
 */
public class CSVIterator {

    private static int lineFeed = '\n';
    private static int carriageReturn = '\r';
    private static int blank = ' ';

    private enum CSVState {
        SwitchLine, Comment, InValue, InQuotedValue, AfterQuotedValue, BeforeValue, EscapeSequence, EscapeSequenceInQuote, EndOfFile, BeforeLine
    }

    private BufferedReader bufferedReader;
    private boolean useQuotes;
    private int quote;
    private int escapeCharacter;
    private int splitCharacter;
    private int commentCharacter;

    private CSVState currentState = CSVState.BeforeLine;
    private StringBuilder buffer = null;
    private boolean isForwardedToNext = false;

    public CSVIterator(InputStream inputStream, boolean useQuotes, char quote, char escapeCharacter, char splitCharacter, char commentCharacter,
            Charset charset) {
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset));
        this.useQuotes = useQuotes;
        this.quote = quote;
        this.escapeCharacter = escapeCharacter;
        this.splitCharacter = splitCharacter;
        this.commentCharacter = commentCharacter;
    }

    public boolean hasNext() throws IOException {
        if (!isForwardedToNext) {
            advanceToNextValue();
            isForwardedToNext = true;
        }
        return !(buffer == null && currentState == CSVState.EndOfFile);
    }

    /**
     * This returns the next value or null to signal a new line
     * 
     * @return
     * @throws IOException
     */
    public String next() throws IOException {
        if (hasNext()) {
            String nextValue = null;
            if (buffer != null) {
                buffer.trimToSize();
                nextValue = buffer.toString();
            }
            isForwardedToNext = false;
            return nextValue;
        } else {
            throw new IOException("No next value");
        }
    }

    private void advanceToNextValue() throws IOException {
        // in case we are in a line switch, we first have to return a null to signal
        // that, then we read the next character.
        if (currentState == CSVState.SwitchLine) {
            buffer = null;
            currentState = CSVState.BeforeLine;
            return;
        }
        // now read until we found next value or end of file
        while (true) {
            int charCode = bufferedReader.read();
            switch (currentState) {
                case Comment:
                    if (charCode == -1) {
                        currentState = CSVState.EndOfFile;
                        return;
                    } else if (charCode == lineFeed || charCode == carriageReturn) {
                        currentState = CSVState.BeforeLine;
                        break;
                    } else {
                        break;
                    }
                case EndOfFile:
                    return;
                case EscapeSequence:
                    if (charCode == -1) {
                        throw new IOException("Unexepcted End Of File");
                    } else {
                        buffer.append((char) charCode);
                        currentState = CSVState.InValue;
                        break;
                    }
                case EscapeSequenceInQuote:
                    if (charCode == -1) {
                        throw new IOException("Unexepcted End Of File");
                    } else {
                        buffer.append((char) charCode);
                        currentState = CSVState.InQuotedValue;
                        break;
                    }
                case InQuotedValue:
                    if (charCode == -1) {
                        throw new IOException("Unexepcted End Of File");
                    } else if (charCode == quote) {
                        currentState = CSVState.AfterQuotedValue;
                        break;
                    } else if (charCode == escapeCharacter) {
                        currentState = CSVState.EscapeSequenceInQuote;
                        break;
                    } else {
                        buffer.append((char) charCode);
                        break;
                    }
                case AfterQuotedValue:
                    if (charCode == -1) {
                        currentState = CSVState.EndOfFile;
                        return;
                    } else if (charCode == splitCharacter) {
                        currentState = CSVState.BeforeValue;
                        return;
                    } else if (charCode == lineFeed || charCode == carriageReturn) {
                        // we also had an empty last value
                        currentState = CSVState.SwitchLine;
                        return;
                    } else {
                        throw new IOException("Illegal Character in CSV Sequence: Only separator allowed after quote.");
                    }
                case SwitchLine:
                    // we cannot enter that here!
                case BeforeLine:
                    if (charCode == -1) {
                        currentState = CSVState.EndOfFile;
                        return;
                    } else if (charCode == splitCharacter) {
                        // in this case we had an empty last value, eg by two ,,
                        buffer = new StringBuilder();
                        currentState = CSVState.BeforeValue;
                        return;
                    } else if (charCode == quote && useQuotes) {
                        // we start a new value
                        buffer = new StringBuilder();
                        currentState = CSVState.InQuotedValue;
                        break;
                    } else if (charCode == lineFeed || charCode == carriageReturn) {
                        // we are receiving consequtive line characters: Ignore
                        break;
                    } else if (charCode == escapeCharacter) {
                        // create new value
                        buffer = new StringBuilder();
                        currentState = CSVState.EscapeSequence;
                        break;
                    } else if (charCode == blank) {
                        // we just skip leading blanks, but it's a value nevertheless
                        buffer = new StringBuilder();
                        break;
                    } else if (charCode == commentCharacter) {
                        // we need to switch to comment and consume whatever comes until next line
                        currentState = CSVState.Comment;
                        break;
                    } else {
                        buffer = new StringBuilder();
                        currentState = CSVState.InValue;
                        buffer.append((char) charCode);
                        break;
                    }
                case BeforeValue:
                    // we always will now start a new value
                    buffer = new StringBuilder();
                    if (charCode == -1) {
                        currentState = CSVState.EndOfFile;
                        return;
                    } else if (charCode == splitCharacter) {
                        // in this case we had an empty last value, eg by two ,,
                        currentState = CSVState.BeforeValue;
                        return;
                    } else if (charCode == quote && useQuotes) {
                        // we start a new value
                        currentState = CSVState.InQuotedValue;
                        break;
                    } else if (charCode == lineFeed || charCode == carriageReturn) {
                        // we also had an empty last value
                        currentState = CSVState.SwitchLine;
                        return;
                    } else if (charCode == escapeCharacter) {
                        currentState = CSVState.EscapeSequence;
                        break;
                    } else if (charCode == blank) {
                        // we just skip leading blanks
                        break;
                    } else {
                        currentState = CSVState.InValue;
                        buffer.append((char) charCode);
                        break;
                    }
                case InValue:
                    if (charCode == -1) {
                        currentState = CSVState.EndOfFile;
                        return;
                    } else if (charCode == splitCharacter) {
                        // then we have our complete value in the buffer and return
                        currentState = CSVState.BeforeValue;
                        return;
                    } else if (charCode == lineFeed || charCode == carriageReturn) {
                        // then we also completed the current value and return
                        currentState = CSVState.SwitchLine;
                        return;
                    } else if (charCode == escapeCharacter) {
                        currentState = CSVState.EscapeSequence;
                        break;
                    } else {
                        buffer.append((char) charCode);
                        break;
                    }
            }
        }
    }

}
