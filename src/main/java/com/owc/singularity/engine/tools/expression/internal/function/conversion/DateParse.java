/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.conversion;


import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 *
 * A {@link Function} parsing a string or a number to a date.
 *
 * @author Marcel Seifert
 *
 */
public class DateParse extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     *
     */
    public DateParse() {
        super("conversion.date_parse", 1, ValueType.TIMESTAMP);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 1, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator input = inputEvaluators[0];

        return new SimpleExpressionEvaluator(type, makeDateCallable(input), isResultConstant(inputEvaluators));
    }

    /**
     * Builds a Date Callable from one String or one double input argument
     *
     * @param inputEvaluator
     *            the input
     * @return the resulting NominalSupplier
     */
    protected TimestampSupplier makeDateCallable(final ExpressionEvaluator inputEvaluator) {

        final NominalSupplier funcString = inputEvaluator.getType() == ExpressionType.NOMINAL ? inputEvaluator.getNominalSupplier() : null;

        final NumericSupplier funcDouble = inputEvaluator.getType() != ExpressionType.NOMINAL ? inputEvaluator.getNumericSupplier() : null;

        try {
            if (funcString != null) {
                if (inputEvaluator.isConstant()) {
                    final long result = compute(funcString.getOrThrow());
                    return () -> result;
                } else {
                    return () -> compute(funcString.getOrThrow());
                }
            } else {
                if (inputEvaluator.isConstant()) {
                    final long result = compute(funcDouble.getOrThrow());
                    return () -> result;
                } else {
                    return () -> compute(funcDouble.getOrThrow());
                }

            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for one input double value.
     *
     * @param value
     *            the input timestamp
     *
     * @return the result of the computation.
     */
    protected long compute(double value) {

        if (ValueType.isMissing(value)) {
            return ValueType.MISSING_TIMESTAMP;
        }

        long dateLong = (long) value;
        Date date = new Date(dateLong);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getTimeInMillis();
    }

    /**
     * Computes the result for one input string value.
     *
     * @param value
     *            the input date string
     *
     * @return the result of the computation
     */
    protected long compute(String dateString) {
        if (ValueType.isMissing(dateString)) {
            return ValueType.MISSING_TIMESTAMP;
        }
        Date date;
        try {
            // clone because getDateInstance uses an internal pool which can return the
            // same instance for multiple threads
            date = ((DateFormat) DateFormat.getDateInstance(DateFormat.SHORT).clone()).parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return date.toInstant().toEpochMilli();
        } catch (ParseException e) {
            throw new FunctionInputException("expression_parser.invalid_argument.date", getFunctionName(), dateString);
        }
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType input = inputTypes[0];
        if (input == ExpressionType.NOMINAL || input == ExpressionType.DOUBLE || input == ExpressionType.INTEGER) {
            return ExpressionType.TIMESTAMP;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type_two", getFunctionName(), "nominal", "numerical");
        }
    }

}
