/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link Function} that returns the time in milliseconds .
 *
 * @author David Arnu
 *
 */
public class DateMillis extends AbstractFunction {

    public DateMillis() {
        super("date.date_millis", 1, ValueType.NUMERIC);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 1, inputEvaluators.length);
        }
        ExpressionType resultType = getResultType(inputEvaluators);
        return new SimpleExpressionEvaluator(makeDoubleCallable(inputEvaluators[0]), resultType, isResultConstant(inputEvaluators));
    }

    private NumericSupplier makeDoubleCallable(ExpressionEvaluator date) {

        final TimestampSupplier funcDate = date.getTimestampSupplier();

        try {
            final long valueDate = date.isConstant() ? funcDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;

            if (date.isConstant()) {
                final double result = compute(valueDate);
                return () -> result;

            } else {
                return () -> compute(funcDate.getOrThrow());
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }

    }

    private double compute(long valueDate) {
        if (ValueType.isMissing(valueDate))
            return ValueType.MISSING_NUMERIC;
        return valueDate;
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes[0] != ExpressionType.TIMESTAMP) {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "date");
        }
        return ExpressionType.INTEGER;
    }

}
