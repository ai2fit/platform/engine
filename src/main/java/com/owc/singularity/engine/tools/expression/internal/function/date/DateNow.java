/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.util.Calendar;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.ExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function} that returns the current date.
 *
 * @author David Arnu
 *
 */
public class DateNow extends AbstractFunction {

    public DateNow() {
        super("date.date_now", 0, ValueType.TIMESTAMP);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length > 0) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 0, inputEvaluators.length);
        }
        ExpressionType resultType = getResultType(inputEvaluators);
        return new SimpleExpressionEvaluator(Calendar.getInstance().getTimeInMillis(), resultType);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        return ExpressionType.TIMESTAMP;
    }

    @Override
    protected boolean isConstantOnConstantInput() {
        return false;
    }
}
