/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.util.*;

import com.owc.singularity.engine.metadata.InputMissingMetaDataError;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.operator.UserData;
import com.owc.singularity.engine.operator.error.ProcessSetupError;
import com.owc.singularity.engine.operator.error.UndefinedParameterSetupError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.tools.container.Pair;


/**
 * This class contains utility methods related to
 * {@link com.owc.singularity.engine.pipeline.AbstractPipeline}es.
 *
 * @author Marco Boeck
 *
 */
public final class ProcessTools {

    /**
     * @since 9.6
     */
    private static final String KEY_PARENT_PROCESS = "parent_process";

    /**
     * @author Jan Czogalla
     * @since 9.6
     */
    private static class ParentUserData implements UserData<Object> {

        private AbstractPipeline parent;

        public ParentUserData(AbstractPipeline parent) {
            this.parent = parent;
        }

        @Override
        public UserData<Object> copyUserData(Object newParent) {
            return this;
        }

        public AbstractPipeline getParent() {
            return parent;
        }
    }

    /**
     * Private constructor which throws if called.
     */
    private ProcessTools() {
        throw new UnsupportedOperationException("Static utility class");
    }

    /**
     * Checks whether the given pipeline has at least one connected result port, i.e. if the
     * pipeline would generate results in the result perspective.
     *
     * @param pipeline
     *            the pipeline in question
     * @return {@code true} if the pipeline has at least one connected result port; {@code false} if
     *         it does not
     */
    public static boolean isProcessConnectedToResultPort(final AbstractPipeline pipeline) {
        if (pipeline == null) {
            throw new IllegalArgumentException("pipeline must not be null!");
        }

        return pipeline.getRootOperator().getSubprocess(0).getInnerSinks().getNumberOfConnectedPorts() > 0;
    }

    /**
     * Extracts the last executed operator of the {@link AbstractRootOperator} of the provided
     * pipeline.
     *
     * @param pipeline
     *            the pipeline to extract the operator from
     * @return the last executed child operator of the {@link AbstractRootOperator} or {@code null}
     *         if pipeline contains no operators
     */
    public static Operator getLastExecutedRootChild(final AbstractPipeline pipeline) {
        if (pipeline == null) {
            throw new IllegalArgumentException("pipeline must not be null!");
        }

        List<Operator> enabledOps = pipeline.getRootOperator().getSubprocess(0).getEnabledOperators();
        return enabledOps.isEmpty() ? null : enabledOps.get(enabledOps.size() - 1);
    }

    /**
     * Checks whether the given pipeline contains at least one operator with a mandatory input port
     * which is not connected. The port is then returned. If no such port can be found, returns
     * {@code null}.
     * <p>
     * This method explicitly only checks for unconnected ports because metadata alone could lead to
     * a false positive. That would prevent pipeline execution, so a false positive has to be
     * prevented under any circumstances.
     * </p>
     *
     * @param pipeline
     *            the pipeline in question
     * @return the first {@link Port} along with its error that is found if the pipeline contains at
     *         least one operator with an input port which is not connected; {@code null} otherwise
     */
    public static Pair<Port, ProcessSetupError> getPortWithoutMandatoryConnection(final AbstractPipeline pipeline) {
        if (pipeline == null) {
            throw new IllegalArgumentException("pipeline must not be null!");
        }
        return pipeline.getAllOperators()
                .stream()
                // if operator or one of its parents is disabled, we don't care
                .filter(operator -> !isSuperOperatorDisabled(operator))
                .map(ProcessTools::getMissingPortConnection)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    /**
     * Checks whether the given operator or one of its sub-operators has a mandatory input port
     * which is not connected. The port is then returned. If no such port can be found, returns
     * {@code null}.
     * <p>
     * This method explicitly only checks for unconnected ports because metadata alone could lead to
     * a false positive.
     * </p>
     *
     * @param operator
     *            the operator for which to check for unconnected mandatory ports
     * @return the first {@link Port} along with its error that is found if the operator has at
     *         least one input port which is not connected; {@code null} otherwise
     */
    public static Pair<Port, ProcessSetupError> getMissingPortConnection(Operator operator) {
        return operator.getErrorList()
                .stream()
                // the error list of an OperatorChain contains all errors of its children
                // we only want errors for the current operator however, so skip otherwise
                .filter(e -> e.getOwner().getOperator() == operator)
                // look for matching errors. We can only identify this via metadata errors
                .filter(e -> e instanceof InputMissingMetaDataError)
                // as we don't know what will be sent at runtime, we only look for unconnected
                .filter(e -> !((InputMissingMetaDataError) e).getPort().isConnected())
                .findFirst()
                .map(e -> new Pair<>(((InputMissingMetaDataError) e).getPort(), e))
                .orElse(null);
    }

    /**
     * Checks whether the given pipeline contains at least one operator with a mandatory parameter
     * which has no value and no default value. Both the operator and the parameter are then
     * returned. If no such operator can be found, returns {@code null}.
     *
     * @param pipeline
     *            the pipeline in question
     * @return the first {@link Operator} found if the pipeline contains at least one operator with
     *         a mandatory parameter which is neither set nor has a default value; {@code null}
     *         otherwise
     */
    public static Pair<Operator, ParameterType> getOperatorWithoutMandatoryParameter(final AbstractPipeline pipeline) {
        if (pipeline == null) {
            throw new IllegalArgumentException("pipeline must not be null!");
        }
        return getOperatorWithoutMandatoryParameter(pipeline.getAllOperators());
    }

    /**
     * Checks whether the given operator or one of its sub-operators has a mandatory parameter which
     * has no value and no default value. Both the operator and the parameter are then returned. If
     * no such operator can be found, returns {@code null}.
     *
     * @param operator
     *            the operator in question
     * @return the first {@link Operator} found if the operator or one of its sub-operators has a
     *         mandatory parameter which is neither set nor has a default value; {@code null}
     *         otherwise
     */
    public static Pair<Operator, ParameterType> getOperatorWithoutMandatoryParameter(final Operator operator) {
        if (operator == null) {
            throw new IllegalArgumentException("operator must not be null!");
        }

        // check the operator first
        ParameterType param = getMissingMandatoryParameter(operator);
        if (param != null) {
            return new Pair<>(operator, param);
        }

        // if it has children check them
        if (operator instanceof OperatorChain) {
            return getOperatorWithoutMandatoryParameter(((OperatorChain) operator).getAllInnerOperators());
        }

        // no operator with missing mandatory parameter found
        return null;
    }

    /**
     * Checks whether one of the given operators has a mandatory parameter which has no value and no
     * default value. Both the operator and the parameter are then returned. If no such operator can
     * be found, returns {@code null}.
     *
     * @param operators
     *            the operators in question
     * @return the first {@link Operator} found if one of the given operators has a mandatory
     *         parameter which is neither set nor has a default value; {@code null} otherwise
     * @since 9.3
     */
    private static Pair<Operator, ParameterType> getOperatorWithoutMandatoryParameter(Collection<Operator> operators) {
        return operators.stream()
                // if operator or one of its parents is disabled, we don't care
                .filter(operator -> !isSuperOperatorDisabled(operator))
                .map(op -> {
                    // check all parameter related setup errors
                    ParameterType param = getMissingMandatoryParameter(op);
                    return param == null ? null : new Pair<>(op, param);
                })
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }


    /**
     * Marks the given pipeline with its parent to allow for better connection caching
     *
     * @param pipeline
     *            the pipeline to be tagged
     * @param parentPipeline
     *            the pipeline' parent pipeline
     * @see com.owc.singularity.repository.connection.adapter.ConnectionAdapterHandler
     *      ConnectionAdapterHandler
     * @since 9.6
     */
    public static void setParentProcess(AbstractPipeline pipeline, AbstractPipeline parentPipeline) {
        if (parentPipeline == null) {
            return;
        }
        UserData<Object> parentData = parentPipeline.getRootOperator().getUserData(KEY_PARENT_PROCESS);
        if (parentData != null) {
            pipeline.getRootOperator().setUserData(KEY_PARENT_PROCESS, parentData);
            return;
        }
        pipeline.getRootOperator().setUserData(KEY_PARENT_PROCESS, new ParentUserData(parentPipeline));
    }

    /**
     * Gets the parent pipeline of the specified pipeline. Will return the argument if there is no
     * user data indicating another parent pipeline.
     *
     * @param pipeline
     *            the pipeline whose parent should be retrieved
     * @return the pipeline' parent or the pipeline itself if there is no parent
     * @since 9.6
     */
    public static AbstractPipeline getParentProcess(AbstractPipeline pipeline) {
        UserData<Object> parentData = pipeline.getRootOperator().getUserData(KEY_PARENT_PROCESS);
        if (parentData instanceof ParentUserData) {
            return ((ParentUserData) parentData).getParent();
        }
        return pipeline;
    }

    /**
     * Calculates a new name based on the already known names. Will append a number in parenthesis
     * if it is a duplicate.
     *
     * @param knownNames
     *            the collection of already known/used names; must not be {@code null}
     * @param name
     *            the name to check; must not be {@code null}
     * @return the new name; possibly the same as the input; never {@code null}
     * @since 9.3
     */
    public static String getNewName(Collection<String> knownNames, String name) {
        if (!knownNames.contains(name)) {
            return name;
        }
        String baseName = name;
        int index = baseName.lastIndexOf(" (");
        int i = 2;
        if (index >= 0 && baseName.endsWith(")")) {
            String suffix = baseName.substring(index + 2, baseName.length() - 1);
            try {
                i = Integer.parseInt(suffix) + 1;
                baseName = baseName.substring(0, index);
                if (!knownNames.contains(baseName)) {
                    return baseName;
                }
            } catch (NumberFormatException e) {
                // not a number; ignore, go with 2
            }
        }
        String newName;
        do {
            newName = baseName + " (" + i++ + ')';
        } while (knownNames.contains(newName));
        return newName;
    }

    /**
     * Calculates new names based on the already known names and returns a map of the renaming for
     * all names that actually changed.
     *
     * @param knownNames
     *            the collection of already known/used names; must not be {@code null}
     * @param names
     *            the names to check; must not be {@code null} or contain {@code null}
     * @return the new names, mapped from old to new; might be empty; never {@code null}
     * @since 9.3
     * @see #getNewName(Collection, String)
     */
    public static Map<String, String> getNewNames(Collection<String> knownNames, Collection<String> names) {
        // prevent side effects
        knownNames = new HashSet<>(knownNames);
        Map<String, String> nameMap = new LinkedHashMap<>();
        for (String name : names) {
            String newName = getNewName(knownNames, name);
            if (!name.equals(newName)) {
                nameMap.put(name, newName);
            }
            knownNames.add(newName);
        }
        return nameMap;
    }

    /**
     * Checks whether the given operator has a mandatory parameter which has no value and no default
     * value and returns the parameter. If no such parameter can be found, returns {@code null}.
     *
     * @param operator
     *            the operator in question
     * @return the first mandatory parameter which is neither set nor has a default value;
     *         {@code null} otherwise
     */
    private static ParameterType getMissingMandatoryParameter(Operator operator) {
        List<ProcessSetupError> errorList = operator.getErrorList();
        if (errorList.isEmpty()) {
            // make sure that setup errors are calculated
            operator.checkProperties();
            errorList = operator.getErrorList();
        }
        return errorList.stream()
                // the error list of an OperatorChain contains all errors of its children
                // we only want errors for the current operator however, so skip otherwise
                .filter(pse -> pse.getOwner().getOperator() == operator)
                // look for matching errors. We can identify this via undefined parameter errors
                .filter(pse -> pse instanceof UndefinedParameterSetupError)
                .map(pse -> ((UndefinedParameterSetupError) pse).getKey())
                .map(operator::getParameterType)
                .findFirst()
                .orElse(null);
    }

    /**
     * Recursively checks if the operator or one of its (grant) parents is disabled.
     *
     * @param operator
     *            the operator to check
     * @return {@code true} if the operator or one of the operators it is contained in is disabled
     */
    private static boolean isSuperOperatorDisabled(Operator operator) {
        return !operator.isEnabled() || operator.getParent() != null && isSuperOperatorDisabled(operator.getParent());
    }
}
