/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.tools.expression.*;


/**
 * Provides expression-specific information, such as the function mapping, constants and variables.
 * Stores all {@link Function}s and has access to all variables, dynamic variables and scope
 * constants that can be used during such an evaluation via their {@link Resolver}s.
 *
 * @author Gisa Schaefer
 *
 */
public class SimpleExpressionContext implements ExpressionContext {

    private final Map<String, Function> functionMap;

    private final List<Resolver> dynamicResolvers;
    private final List<Resolver> scopeResolvers;
    private final List<Resolver> constantResolvers;

    private final Callable<Void> stopChecker;

    /**
     * Creates a {@link ExpressionContext} that uses the given functions and resolvers.
     *
     * @param functions
     *            the functions to use in expressions
     * @param scopeResolvers
     *            the scope resolvers to use
     * @param dynamicResolvers
     *            the resolvers for dynamic variables to use
     * @param constantResolvers
     */
    public SimpleExpressionContext(List<Function> functions, List<Resolver> scopeResolvers, List<Resolver> dynamicResolvers, List<Resolver> constantResolvers) {
        this(functions, scopeResolvers, dynamicResolvers, constantResolvers, null);
    }

    /**
     * Creates a {@link ExpressionContext} that uses the given functions and resolvers.
     *
     * @param functions
     *            the functions to use in expressions
     * @param scopeResolvers
     *            the scope resolvers to use
     * @param dynamicResolvers
     *            the resolvers for dynamic variables to use
     * @param constantResolvers
     *            the resolvers for constants
     * @param stopChecker
     *            a callable that might throw an exception to stop a running evaluation
     * @since 9.6.0
     */
    public SimpleExpressionContext(List<Function> functions, List<Resolver> scopeResolvers, List<Resolver> dynamicResolvers, List<Resolver> constantResolvers,
            Callable<Void> stopChecker) {
        this.scopeResolvers = scopeResolvers;
        this.dynamicResolvers = dynamicResolvers;
        this.constantResolvers = constantResolvers;
        this.stopChecker = stopChecker == null ? () -> null : stopChecker;

        this.functionMap = new LinkedHashMap<>();
        for (Function function : functions) {
            // take the first function with a certain function name if there is more than one
            if (!this.functionMap.containsKey(function.getFunctionName())) {
                this.functionMap.put(function.getFunctionName(), function);
            }
        }
    }

    @Override
    public Function getFunction(String functionName) {
        return functionMap.get(functionName);
    }

    @Override
    public ExpressionEvaluator getVariable(String variableName) {
        // A variable can either be a constant coming from a {@link Resolver} for constants or a
        // dynamic variable. This is done to keep compatibility with the old parser where
        // alphanumeric strings could stand for constants or attribute values. Attribute values are
        // now a special case of dynamic variables.
        ExpressionEvaluator constant = getConstant(variableName);
        if (constant != null) {
            return constant;
        } else {
            return getDynamicVariable(variableName);
        }
    }

    /**
     * Looks for the first resolver in the resolvers list that knows the variableName.
     *
     * @param variableName
     *            the name to look for
     */
    private Resolver getResolverWithKnowledge(List<Resolver> resolvers, String variableName) {
        for (Resolver resolver : resolvers) {
            if (resolver.getVariableType(variableName) != null) {
                return resolver;
            }
        }
        return null;
    }

    /**
     * Creates a constant {@link ExpressionEvaluator} for the variableName using the resolver.
     */
    private ExpressionEvaluator getExpressionEvaluator(String variableName, Resolver resolver) {
        ExpressionType type = resolver.getVariableType(variableName);
        switch (type) {
            case DOUBLE:
            case INTEGER:
                return new SimpleExpressionEvaluator(resolver.getDoubleValue(variableName), type);
            case TIMESTAMP:
                return new SimpleExpressionEvaluator(resolver.getTimestampValue(variableName), type);
            case NOMINAL:
                return new SimpleExpressionEvaluator(resolver.getStringValue(variableName), type);
            case BOOLEAN:
                return new SimpleExpressionEvaluator(resolver.getBooleanValue(variableName), type);
            default:
                return null;
        }
    }

    @Override
    public ExpressionEvaluator getDynamicVariable(String variableName) {
        Resolver resolver = getResolverWithKnowledge(dynamicResolvers, variableName);
        if (resolver == null) {
            return null;
        }
        return getDynamicExpressionEvaluator(variableName, resolver);
    }

    /**
     * Creates an non-constant {@link ExpressionEvaluator} for the variableName using the resolver.
     */
    private ExpressionEvaluator getDynamicExpressionEvaluator(final String variableName, final Resolver resolver) {
        ExpressionType type = resolver.getVariableType(variableName);
        switch (type) {
            case DOUBLE:
            case INTEGER:
                NumericSupplier numericSupplier = () -> resolver.getDoubleValue(variableName);
                return new SimpleExpressionEvaluator(numericSupplier, type, false);
            case TIMESTAMP:
                TimestampSupplier dateCallable = () -> resolver.getTimestampValue(variableName);
                return new SimpleExpressionEvaluator(type, dateCallable, false);
            case NOMINAL:
                NominalSupplier stringCallable = () -> resolver.getStringValue(variableName);
                return new SimpleExpressionEvaluator(stringCallable, type, false);
            case BOOLEAN:
                BooleanSupplier booleanCallable = () -> resolver.getBooleanValue(variableName);
                return new SimpleExpressionEvaluator(booleanCallable, false, type);
            default:
                return null;
        }
    }

    @Override
    public ExpressionEvaluator getScopeConstant(String scopeName) {
        Resolver resolver = getResolverWithKnowledge(scopeResolvers, scopeName);
        if (resolver == null) {
            return null;
        }
        return getExpressionEvaluator(scopeName, resolver);
    }

    @Override
    public String getScopeString(String scopeName) {
        Resolver resolver = getResolverWithKnowledge(scopeResolvers, scopeName);
        if (resolver == null) {
            return null;
        }

        ExpressionType type = resolver.getVariableType(scopeName);
        switch (type) {
            case DOUBLE:
            case INTEGER:
                return String.valueOf(resolver.getDoubleValue(scopeName));
            case TIMESTAMP:
                return String.valueOf(resolver.getTimestampValue(scopeName));
            case NOMINAL:
                return resolver.getStringValue(scopeName);
            case BOOLEAN:
                return String.valueOf(resolver.getBooleanValue(scopeName));
            default:
                return null;
        }

    }

    @Override
    public List<FunctionDescription> getFunctionDescriptions() {
        List<FunctionDescription> descriptions = new ArrayList<>(functionMap.size());
        for (Function function : functionMap.values()) {
            descriptions.add(function.getFunctionDescription());
        }
        return descriptions;
    }

    @Override
    public List<FunctionInput> getFunctionInputs() {
        List<FunctionInput> allFunctionInputs = new LinkedList<>();
        for (Resolver resolver : dynamicResolvers) {
            allFunctionInputs.addAll(resolver.getAllVariables());
        }
        for (Resolver resolver : constantResolvers) {
            allFunctionInputs.addAll(resolver.getAllVariables());
        }
        for (Resolver resolver : scopeResolvers) {
            allFunctionInputs.addAll(resolver.getAllVariables());
        }
        return allFunctionInputs;
    }

    @Override
    public ExpressionEvaluator getConstant(String constantName) {
        Resolver resolver = getResolverWithKnowledge(constantResolvers, constantName);
        if (resolver != null) {
            return getExpressionEvaluator(constantName, resolver);
        } else {
            return null;
        }
    }

    /**
     * Returns the stop checker.
     */
    public Callable<Void> getStopChecker() {
        return stopChecker;
    }
}
