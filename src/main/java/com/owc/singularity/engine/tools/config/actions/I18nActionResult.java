package com.owc.singularity.engine.tools.config.actions;


import com.owc.singularity.engine.tools.I18N;

/**
 * This class implements an {@link ActionResult} with convenience constructor to use I18n configured
 * results.
 *
 * @author Burhan Yildiz
 *
 */
public class I18nActionResult implements ActionResult {

    /** Result of the Action */
    Result result = null;

    /** Message of the Action */
    String key = null;

    private Object[] args;
    private String message;

    /**
     * The key will be prefixed with "gui." at suffixed with ".message". It then searches for the
     * entry in the GUI Bundle.
     *
     * @param key
     * @param result
     */
    public I18nActionResult(String key, Result result, Object... objects) {
        this.key = key;
        this.args = objects;
        this.result = result;
    }

    @Override
    public Result getResult() {
        return result;
    }

    @Override
    public String getMessage() {
        if (message == null) {
            message = I18N.getGUIMessage("gui." + key + ".message", args);
        }
        return message;
    }

    @Override
    public Object[] getArguments() {
        return args;
    }

}
