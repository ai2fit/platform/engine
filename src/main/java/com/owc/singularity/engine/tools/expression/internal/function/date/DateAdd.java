/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.time.ZonedDateTime;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function Function} for setting a value of a
 * given date.
 *
 * @author David Arnu, Jan Czogalla
 *
 */
public class DateAdd extends AbstractDateLongManipulationFunction {

    public DateAdd() {
        super("date.date_add", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.TIMESTAMP);
    }

    /**
     * Adds the given {@code value} with the specified unit to the calendar
     */
    @Override
    protected ZonedDateTime integerManipulation(Callable<Void> stopChecker, ZonedDateTime cal, TemporalUnit unit, int amount) {
        return cal.plus(amount, unit);
    }

    /**
     * Call
     * {@link AbstractDateLongManipulationFunction#integerManipulation(Callable, ZonedDateTime, TemporalUnit, int)}
     * with {@link #MAX_DATE_VALUE} until there is only an integer rest and finally add that.
     */
    @Override
    protected ZonedDateTime longManipulation(Callable<Void> stopChecker, ZonedDateTime cal, long amount, int sign, TemporalUnit dateUnit) {
        try {
            while (amount >= MAX_DATE_VALUE) {
                cal = integerManipulation(stopChecker, cal, dateUnit, MAX_DATE_VALUE * sign);
                amount -= MAX_DATE_VALUE;
                stopChecker.call();
            }
        } catch (Exception e) {
            // stop checker forced calculation to stop
            return cal;
        }
        return integerManipulation(stopChecker, cal, dateUnit, (int) (sign * amount));
    }
}
