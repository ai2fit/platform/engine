package com.owc.singularity.engine.tools.io.csv;


import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import com.owc.singularity.engine.object.data.exampleset.*;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;

/**
 * Class executed to parse and print csv.
 *
 * @author Sebastian Land
 *
 */
public class CSVParser {

    public static final ThreadLocal<SimpleDateFormat> DEFAULT_DATE_FORMAT;

    static {
        // ThreadLocale because this is static and used by other threads
        // and DateFormats are NOT threadsafe
        DEFAULT_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {

            @Override
            protected SimpleDateFormat initialValue() {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                return format;
            }
        };
    }

    public static void formatExample(PrintStream stream, Example example, Attribute[] attributes, boolean useQuotes, char quote, char escChar, String splitChar,
            DateFormat dateFormat) {
        String currentSplitChar = "";
        // fill selected values
        for (Attribute att : attributes) {
            stream.append(currentSplitChar);
            currentSplitChar = splitChar;

            if (att.isNominal()) {
                String value = useQuotes ? quoteAndEscape(example.getNominalValue(att), quote, escChar) : example.getNominalValue(att);
                stream.append(value);
            } else if (att.isNumerical()) {
                // numerical
                double value = example.getNumericValue(att);
                if (!ValueType.isMissing(value))
                    stream.append(String.valueOf(value));
            } else {
                long timestampValue = example.getTimestampValue(att);
                if (!ValueType.isMissing(timestampValue)) {
                    // date-time
                    String date;
                    if (dateFormat != null) {
                        // for converting date from define date pattern
                        date = useQuotes ? quoteAndEscape(dateFormat.format(new Date(timestampValue)), quote, escChar)
                                : dateFormat.format(new Date(timestampValue));
                    } else {
                        date = String.valueOf(timestampValue);
                    }
                    stream.append(date);
                }
            }
        }
        stream.println();
    }

    public static String formatExample(ExampleSet exampleSet, ValueType[] attributeValueTypes, int[] attributeIndexes, int currentRow, boolean useQuotes,
            char quote, char escChar, String splitChar, DateFormat dateFormat) {
        StringBuilder builder = new StringBuilder();
        String currentSplitChar = "";
        // fill selected values
        for (int i = 0; i < attributeValueTypes.length; i++) {
            builder.append(currentSplitChar);
            currentSplitChar = splitChar;

            switch (attributeValueTypes[i]) {
                case NOMINAL -> {
                    String value = useQuotes ? quoteAndEscape(exampleSet.getNominalValue(currentRow, attributeIndexes[i]), quote, escChar)
                            : exampleSet.getNominalValue(currentRow, attributeIndexes[i]);
                    builder.append(value);
                }
                case NUMERIC -> {
                    double value = exampleSet.getNumericValue(currentRow, attributeIndexes[i]);
                    if (!ValueType.isMissing(value))
                        builder.append(value);
                }
                case TIMESTAMP -> {
                    long timestampValue = exampleSet.getTimestampValue(currentRow, attributeIndexes[i]);
                    if (!ValueType.isMissing(timestampValue)) {
                        // date-time
                        String date;
                        if (dateFormat != null) {
                            // for converting date from define date pattern
                            date = useQuotes ? quoteAndEscape(dateFormat.format(new Date(timestampValue)), quote, escChar)
                                    : dateFormat.format(new Date(timestampValue));
                        } else {
                            date = String.valueOf(timestampValue);
                        }
                        builder.append(date);
                    }
                }
            }
        }
        builder.append("\n");

        return builder.toString();
    }

    /**
     * This method will put quotes around the string and escape any quotes, escape characters and
     * line breaks within the string.
     *
     * @param nominalValue
     * @param quote
     * @param escChar
     * @return
     */
    public static String quoteAndEscape(String nominalValue, char quote, char escChar) {
        if (nominalValue == null)
            return "";
        // initializes string builder with original length * 1.125
        StringBuilder builder = new StringBuilder(nominalValue.length() + nominalValue.length() >> 3 + 4);

        builder.append(quote);
        char[] characters = nominalValue.toCharArray();
        for (char character : characters) {
            if (character == quote) {
                builder.append(escChar);
                builder.append(quote);
            } else if (character == escChar) {
                builder.append(escChar);
                builder.append(escChar);
            } else if (character == '\n') {
                builder.append(escChar);
                builder.append('n');
            } else if (character == '\r') {
                // don't do anything: \n already codes everything
            } else {
                builder.append(character);
            }
        }
        builder.append(quote);
        return builder.toString();
    }

    /**
     * This method will output the header of a csv file by writing all Properties' names
     *
     * @param attributeNames
     * @param useQuotes
     * @param quote
     * @param escapeCharacter
     * @param splitCharacter
     * @param dateFormat
     * @return
     */
    public static String formatHeader(String[] attributeNames, boolean useQuotes, char quote, char escapeCharacter, String splitCharacter,
            DateFormat dateFormat) {
        StringBuilder builder = new StringBuilder();
        String currentSplitCharacter = "";
        for (String attributeName : attributeNames) {
            builder.append(currentSplitCharacter);
            builder.append(quoteAndEscape(attributeName, quote, escapeCharacter));
            currentSplitCharacter = splitCharacter;
        }
        builder.append("\n");
        return builder.toString();
    }

    /**
     * This method will output the header of a csv file by writing all Properties' names into a
     * PrintWriter
     * 
     * @param stream
     * @param attributes
     * @param useQuotes
     * @param quote
     * @param escapeCharacter
     * @param splitCharacter
     * @param dateFormat
     */
    public static void formatHeader(PrintStream stream, Attribute[] attributes, boolean useQuotes, char quote, char escapeCharacter, String splitCharacter,
            DateFormat dateFormat) {
        String currentSplitCharacter = "";
        for (Attribute attribute : attributes) {
            stream.append(currentSplitCharacter);
            stream.append(quoteAndEscape(attribute.getName(), quote, escapeCharacter));
            currentSplitCharacter = splitCharacter;
        }
        stream.println();
    }

    public static DateFormat getDefaultFormat() {
        return DEFAULT_DATE_FORMAT.get();
    }

    /**
     * This parses a given input stream as CSV. if the given dateFormat is null, it will parse the
     * given value as milliseconds since epoch start in UTC.
     *
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws ProcessStoppedException
     */
    public static ExampleSet parse(InputStream in, String[] attributeNames, ValueType[] valueTypes, boolean hasAttributeNames, boolean useQuotes, char quote,
            char escapeCharacter, char splitCharacter, char commentCharacter, DateFormat dateFormat, Charset charset, Interruptor interruptor)
            throws IOException, ParseException, ProcessStoppedException, NumberFormatException {
        ExampleSetCreator creator = new ExampleSetCreator(attributeNames, valueTypes);
        CSVIterator iterator = new CSVIterator(in, useQuotes, quote, escapeCharacter, splitCharacter, commentCharacter, charset);

        // if has attribute names, do check
        if (hasAttributeNames) {
            for (String attributeName : attributeNames) {
                if (iterator.hasNext()) {
                    String readAttributeName = iterator.next().trim();
                    if (!attributeName.equals(readAttributeName))
                        throw new IOException("File format invalid");
                }
            }
            // now we expect a line break, signaled by null return value
            String lineBreak = iterator.next();
            if (lineBreak != null)
                throw new IOException("File format invalid");
        }

        // now we will iterate over all rows that will be delivered
        int column = 0;
        int row = 0;
        while (iterator.hasNext()) {
            String value = iterator.next();
            if (value == null) {
                // start new line
                column = 0;
                row++;
                if (row % 5000 == 0)
                    interruptor.checkForStop();
                creator.commit();
            } else {
                switch (valueTypes[column]) {
                    case NOMINAL:
                        creator.setValue(attributeNames[column], value);
                        break;
                    case NUMERIC:
                        creator.setValue(attributeNames[column], Double.parseDouble(value));
                        break;
                    case TIMESTAMP:
                        if (dateFormat != null)
                            creator.setValue(attributeNames[column], dateFormat.parse(value).toInstant().toEpochMilli());
                        else {
                            creator.setValue(attributeNames[column], Long.parseLong(value));
                        }
                }
                column++;
            }
        }

        return creator.finish();
    }

    public static Attribute[] getHeaderAttributes(ExampleSet set) {
        Attribute[] attributes = new Attribute[set.getAttributes().allSize()];
        Iterator<Attribute> iterator = set.getAttributes().allAttributes();
        int i = 0;
        while (iterator.hasNext()) {
            attributes[i++] = iterator.next();
        }
        return attributes;
    }
}
