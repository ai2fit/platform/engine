/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.process;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractArbitraryStringInputStringOutputFunction;


/**
 * Function for getting a parameter of a certain operator.
 *
 * @author Gisa Schaefer
 *
 */
public class ParameterValue extends AbstractArbitraryStringInputStringOutputFunction {

    public static final String FUNCTION_NAME = "param";
    private final AbstractPipeline process;

    /**
     * Creates a function that looks up a operator parameter in the process.
     *
     * @param process
     *            the process where to find the operator
     */
    public ParameterValue(AbstractPipeline process) {
        super("process.param", 2);
        this.process = process;
    }

    @Override
    protected void checkNumberOfInputs(int length) {
        int expectedInput = getFunctionDescription().getNumberOfArguments();
        if (length != expectedInput) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), expectedInput, length);
        }
    }

    @Override
    protected String compute(String... values) {
        try {
            Operator operator = process.getOperator(values[0]);
            if (operator == null) {
                throw new FunctionInputException("expression_parser.parameter_value_wrong_operator", getFunctionName());
            }
            return operator.getParameter(values[1]);
        } catch (UndefinedParameterError e) {
            throw new FunctionInputException("expression_parser.parameter_value_wrong_parameter", getFunctionName());
        }
    }

}
