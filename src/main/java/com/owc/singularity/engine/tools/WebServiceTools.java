/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.net.UrlFollower;
import com.owc.singularity.engine.tools.parameter.ParameterChangeListener;


/**
 * Some utility methods for web services and url connections.
 *
 * @author Simon Fischer, Marco Boeck
 */
public class WebServiceTools {

    // three minutes
    private static final int READ_TIMEOUT = (int) TimeUnit.MINUTES.toMillis(3);

    private static final int CHUNKED_SIZE = 4096 * 16;

    public static final String WEB_SERVICE_TIMEOUT = "connection.timeout";

    /** the timeout in ms used for url connections */
    public static int TIMEOUT_URL_CONNECTION;

    /** The HTTP user agent request header */
    private static final String USER_AGENT_HEADER = "User-Agent";

    /** the custom userAgent */
    private static String userAgent = StringUtils.stripToNull(PropertyService.getParameterValue(EngineProperties.NETWORK_DEFAULT_USER_AGENT));

    static {
        String timeoutStr = PropertyService.getParameterValue(WEB_SERVICE_TIMEOUT);
        if (timeoutStr != null) {
            TIMEOUT_URL_CONNECTION = Integer.parseInt(timeoutStr);
        } else {
            TIMEOUT_URL_CONNECTION = READ_TIMEOUT;
        }

        PropertyService.registerParameterChangeListener(new ParameterChangeListener() {

            @Override
            public void informParameterSaved() {}

            @Override
            public void informParameterChanged(String key, String value) {
                if (WEB_SERVICE_TIMEOUT.equals(key)) {
                    if (value != null) {
                        TIMEOUT_URL_CONNECTION = Integer.parseInt(value);
                    }
                } else if (EngineProperties.NETWORK_DEFAULT_USER_AGENT.equals(key)) {
                    userAgent = StringUtils.stripToNull(value);
                }
            }
        });
    }


    /**
     * Sets some default settings for {@link URLConnection}s, e.g. timeouts.
     */
    public static void setURLConnectionDefaults(URLConnection connection) {
        setURLConnectionDefaults(connection, false);
    }

    /**
     * Sets some default settings for {@link URLConnection}s, e.g. timeouts. Allows to specify
     * whether the connection should have chunked streaming mode enabled (works only for
     * HttpURLConnections).
     */
    public static void setURLConnectionDefaults(URLConnection connection, boolean isChunked) {
        if (connection == null) {
            throw new IllegalArgumentException("connection must not be null!");
        }

        connection.setConnectTimeout(TIMEOUT_URL_CONNECTION);
        connection.setReadTimeout(READ_TIMEOUT);
        if (connection instanceof HttpURLConnection) {
            if (isChunked) {
                ((HttpURLConnection) connection).setChunkedStreamingMode(CHUNKED_SIZE);
            }
            String userAgentCopy = userAgent;
            if (userAgentCopy != null && connection.getRequestProperties().keySet().stream().noneMatch(USER_AGENT_HEADER::equalsIgnoreCase)) {
                connection.setRequestProperty(USER_AGENT_HEADER, userAgentCopy);
            }
        }
    }

    /**
     * Opens an {@link InputStream} from the given {@link URL} and calls
     * {@link #setURLConnectionDefaults(URLConnection)} on the {@link URLConnection} .
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static InputStream openStreamFromURL(URL url) throws IOException {
        if (url == null) {
            throw new IllegalArgumentException("url must not be null!");
        }

        URLConnection connection = UrlFollower.follow(url);
        setURLConnectionDefaults(connection);
        return connection.getInputStream();
    }

    /**
     * Opens an {@link InputStream} from the given {@link URL} and sets the read and connection
     * timeout provided by timeout.
     */
    public static InputStream openStreamFromURL(URL url, int timeout) throws IOException {
        if (url == null) {
            throw new IllegalArgumentException("url must not be null!");
        }

        URLConnection connection = url.openConnection();

        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);

        return connection.getInputStream();
    }

    /**
     * Tries to create an instance of the defined entity class from a JSON String.
     *
     * @param jsonString
     *            the JSON as string
     * @param entityClass
     *            the class to parse the json string into
     * @param failOnUnknown
     *            if {@code true}, parsing will fail with a {@link JsonMappingException} if unknown
     *            properties are encountered
     * @return the parsed object
     * @throws JsonParseException
     *             see {@link ObjectMapper#readValue(String, Class)}
     * @throws JsonMappingException
     *             see {@link ObjectMapper#readValue(String, Class)}
     * @throws IOException
     *             see {@link ObjectMapper#readValue(String, Class)}
     * @since 8.1
     */
    public static <T> T parseJsonString(String jsonString, Class<T> entityClass, boolean failOnUnknown)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, failOnUnknown);
        return mapper.readValue(jsonString, entityClass);
    }
}
