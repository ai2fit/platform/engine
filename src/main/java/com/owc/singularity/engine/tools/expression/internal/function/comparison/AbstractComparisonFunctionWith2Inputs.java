/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.comparison;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Abstract class for a comparison function that has 2 numerical or nominal inputs, but both of the
 * same type
 *
 * @author Sabrina Kirstein
 *
 */
public abstract class AbstractComparisonFunctionWith2Inputs extends AbstractFunction {

    /**
     * Constructs a comparison AbstractFunction with {@link FunctionDescription} generated from the
     * arguments and the function name generated from the description.
     *
     * @param i18nKey
     *            the key for the {@link FunctionDescription}. The functionName is read from
     *            "gui.dialog.function.i18nKey.name", the helpTextName from ".help", the groupName
     *            from ".group", the description from ".description" and the function with
     *            parameters from ".parameters". If ".parameters" is not present, the ".name" is
     *            taken for the function with parameters.
     */
    public AbstractComparisonFunctionWith2Inputs(String i18nKey) {
        super(i18nKey, 2, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {

        if (inputEvaluators.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 2, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator left = inputEvaluators[0];
        ExpressionEvaluator right = inputEvaluators[1];

        return new SimpleExpressionEvaluator(makeBooleanCallable(left, right), isResultConstant(inputEvaluators), type);
    }

    /**
     * Builds a boolean callable from evaluator using {@link #compute(double, double)} or
     * {@link #compute(String, String)}, where constant child results are evaluated.
     *
     * @param left
     *            evaluator
     * @param right
     *            evaluator
     * @return the resulting boolean callable
     */
    protected BooleanSupplier makeBooleanCallable(ExpressionEvaluator left, ExpressionEvaluator right) {
        ExpressionType leftType = left.getType();
        ExpressionType rightType = right.getType();

        try {
            // if both types are numeric
            if ((leftType.equals(ExpressionType.DOUBLE) || leftType.equals(ExpressionType.INTEGER))
                    && (rightType.equals(ExpressionType.DOUBLE) || rightType.equals(ExpressionType.INTEGER))) {

                final NumericSupplier funcLeft = left.getNumericSupplier();
                final double valueLeft = left.isConstant() ? funcLeft.getOrThrow() : Double.NaN;
                final NumericSupplier funcRight = right.getNumericSupplier();
                final double valueRight = right.isConstant() ? funcRight.getOrThrow() : Double.NaN;

                if (left.isConstant() && right.isConstant()) {
                    final boolean result = compute(valueLeft, valueRight);
                    return () -> result;
                } else if (left.isConstant()) {
                    return () -> compute(valueLeft, funcRight.getOrThrow());
                } else if (right.isConstant()) {
                    return () -> compute(funcLeft.getOrThrow(), valueRight);
                } else {
                    return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
                }

                // if both types are nominal
            } else if (leftType.equals(ExpressionType.NOMINAL) && rightType.equals(ExpressionType.NOMINAL)) {

                final NominalSupplier funcLeft = left.getNominalSupplier();
                final String valueLeft = left.isConstant() ? funcLeft.getOrThrow() : null;

                final NominalSupplier funcRight = right.getNominalSupplier();
                final String valueRight = right.isConstant() ? funcRight.getOrThrow() : null;

                if (left.isConstant() && right.isConstant()) {
                    final Boolean result = compute(valueLeft, valueRight);
                    return () -> result;
                } else if (left.isConstant()) {
                    return () -> compute(valueLeft, funcRight.getOrThrow());
                } else if (right.isConstant()) {
                    return () -> compute(funcLeft.getOrThrow(), valueRight);
                } else {
                    return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
                }
            } else {
                return null;
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two double values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(double left, double right);

    /**
     * Computes the result for two String values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(String left, String right);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), "2", inputTypes.length);
        }
        ExpressionType type1 = inputTypes[0];
        ExpressionType type2 = inputTypes[1];

        // types have to be the same
        if (type1 != type2) {
            if (!((type1 == ExpressionType.INTEGER || type1 == ExpressionType.DOUBLE) && (type2 == ExpressionType.INTEGER || type2 == ExpressionType.DOUBLE))) {
                throw new FunctionInputException("expression_parser.function_needs_same_type", getFunctionName());
            }
        }

        // type has to be numerical or nominal
        for (ExpressionType inputType : inputTypes) {
            if (inputType != ExpressionType.INTEGER && inputType != ExpressionType.DOUBLE && inputType != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "nominal or numerical");
            }
        }
        // result is always boolean
        return ExpressionType.BOOLEAN;
    }
}
