package com.owc.singularity.engine.tools.config.actions;

public class ActionResultFactory {

    public static ActionResult failure(String message) {
        return failure(message, null);
    }

    public static ActionResult failure(String message, Throwable throwable) {
        return new SimpleActionResult(message, ActionResult.Result.FAILURE, throwable);
    }

    public static ActionResult success(String message) {
        return new SimpleActionResult(message, ActionResult.Result.SUCCESS);
    }

    public static ActionResult undetermined() {
        return undetermined(null);
    }

    public static ActionResult undetermined(String message) {
        return new SimpleActionResult(message, ActionResult.Result.NONE);
    }
}
