/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import java.util.HashMap;
import java.util.Objects;

import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the mode of some values.
 *
 * @author Tobias Malbrecht
 *
 */
public class ModeFunction extends AbstractAggregationFunction {

    private String MODE_AGGREGATION_FUNCTION_NAME = "mode";


    private HashMap<String, Double> valueWeightMap;

    private double maximumValueWeight;

    private String valueMaximumWeight;

    public ModeFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public ModeFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
        valueWeightMap = new HashMap<>();
        maximumValueWeight = Double.NEGATIVE_INFINITY;
        valueMaximumWeight = ValueType.MISSING_NOMINAL;
    }

    @Override
    public String getName() {
        return MODE_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        maximumValueWeight = 0;
        valueMaximumWeight = ValueType.MISSING_NOMINAL;
        if (valueWeightMap != null) {
            valueWeightMap.clear();
        }
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        String parsedValue = formatDouble(value);
        Double totalValueWeight = valueWeightMap.get(parsedValue);
        if (totalValueWeight != null) {
            totalValueWeight++;
        } else {
            totalValueWeight = 1.0;
        }
        valueWeightMap.put(parsedValue, totalValueWeight);
        if (totalValueWeight > maximumValueWeight) {
            maximumValueWeight = totalValueWeight;
            valueMaximumWeight = parsedValue;
        }
    }

    @Override
    public void update(long value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        String parsedValue = String.format("%d", value);
        Double totalValueWeight = valueWeightMap.get(parsedValue);
        if (totalValueWeight != null) {
            totalValueWeight++;
        } else {
            totalValueWeight = 1.0;
        }
        valueWeightMap.put(parsedValue, totalValueWeight);
        if (totalValueWeight > maximumValueWeight) {
            maximumValueWeight = totalValueWeight;
            valueMaximumWeight = parsedValue;
        }
    }

    @Override
    public void update(String value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        Double totalValueWeight = valueWeightMap.get(value);
        if (totalValueWeight != null) {
            totalValueWeight++;
        } else {
            totalValueWeight = 1.0;
        }
        valueWeightMap.put(value, totalValueWeight);
        if (totalValueWeight > maximumValueWeight) {
            maximumValueWeight = totalValueWeight;
            valueMaximumWeight = value;
        }
    }

    @Override
    public double aggregateNumericValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NUMERIC;
        }
        return Double.parseDouble(valueMaximumWeight);
    }

    @Override
    public long aggregateTimestampValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_TIMESTAMP;
        }
        return Long.parseLong(valueMaximumWeight);
    }

    @Override
    public String aggregateNominalValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NOMINAL;
        }
        return valueMaximumWeight;
    }

    @Override
    public ValueType getValueTypeOfResult(ValueType[] inputTypes) {
        for (int i = 1; i < inputTypes.length; i++) {
            if (!Objects.equals(inputTypes[i - 1], inputTypes[i])) {
                return ValueType.NOMINAL;
            }
        }
        return inputTypes[0];
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return true;
    }

    /**
     * This function formats integers stored as doubles as if they are integers, and otherwise
     * formats doubles with the minimum necessary precision.
     */
    public static String formatDouble(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }
}
