/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.OperatorService.OperatorServiceListener;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;


/**
 * A group tree manages operator descriptions in a tree like manner. This is useful to present the
 * operators in groups and subgroups and eases operator selection in the GUI.
 * <p>
 * The group tree heavily depends on the associated OperatorService, since it reflects the
 * registered Operators of that Service. Each {@link OperatorService} can have multiple GroupTrees,
 * which register as listener to be able to update on new registration or unregistration events.
 * <p>
 * The listening is done by the {@link OperatorDescriptionTreeRoot} class implementing the
 * {@link OperatorServiceListener} interface.
 * 
 * @author Ingo Mierswa, Sebastian Land
 */
public class OperatorDescriptionTreeRoot extends AbstractOperatorDescriptionTreeNode implements OperatorServiceListener {

    /**
     * Constructor for the OperatorService.
     */
    public OperatorDescriptionTreeRoot() {
        super();
    }

    /**
     * Clone constructor. This will keep the link to the {@link OperatorService}. Whenever a new
     * Operator is added or removed, this will be reflected in the copy as well. For detecting such
     * events, please register on
     * {@link OperatorService#addOperatorServiceListener(OperatorServiceListener)}.
     */
    protected OperatorDescriptionTreeRoot(OperatorDescriptionTreeRoot other) {
        super(other);
    }

    /** Returns a deep clone of this tree. */
    @Override
    public AbstractOperatorDescriptionTreeNode clone() {
        return new OperatorDescriptionTreeRoot(this);
    }

    /** Returns the name of this group. */
    @Override
    public String getName() {
        return "Root";
    }

    /** Returns the parent of this group. Returns null if no parent does exist. */
    @Override
    public OperatorDescriptionTreeRoot getParent() {
        return null;
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public String getKey() {
        return "";
    }

    @Override
    public String getFullyQualifiedKey() {
        return "";
    }

    /**
     * This method will be called by the {@link OperatorService}, whenever a new {@link Operator}
     * has been registered. The given bundle is used to derive subgroup names from.
     */
    @Override
    public void operatorRegistered(OperatorDescription description) {
        addOperator(description);
    }

    /**
     * This method will be called by the {@link OperatorService}, whenever an {@link Operator} has
     * been unregistered.
     */
    @Override
    public void operatorUnregistered(OperatorDescription description) {
        removeOperator(description);
    }

    public void addOperator(OperatorDescription description) {
        String groupKey = description.getGroup();
        AbstractOperatorDescriptionTreeNode group = findGroup(groupKey, true);
        group.addOperatorDescription(description);
    }

    public void removeOperator(OperatorDescription description) {
        String groupKey = description.getGroup();
        AbstractOperatorDescriptionTreeNode group = findGroup(groupKey, false);
        if (group != null) {
            group.removeOperatorDescription(description);
        }
    }

    /**
     * Finds the group for the given fully qualified name (dot separated) if existing, returning
     * null otherwise.
     * 
     * @param createIfNotFound
     *            create the group(s) if it does not exist
     */
    public AbstractOperatorDescriptionTreeNode findGroup(String fullyQualifiedGroupName, boolean createIfNotFound) {
        String[] groupKeys = fullyQualifiedGroupName.split("\\.");
        AbstractOperatorDescriptionTreeNode group = this;
        if (createIfNotFound) {
            for (int i = 0; i < groupKeys.length && group != null; i++) {
                group = group.getOrCreateSubGroup(groupKeys[i]);
            }
        } else {
            for (int i = 0; i < groupKeys.length && group != null; i++) {
                group = group.getSubGroup(groupKeys[i]);
            }
        }
        return group;
    }
}
