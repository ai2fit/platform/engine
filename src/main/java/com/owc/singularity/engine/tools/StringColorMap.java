/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.awt.Color;

import com.owc.singularity.engine.modules.Module;


/**
 * @author Tobias Malbrecht
 */
public class StringColorMap extends ParentResolvingMap<String, Color> {

    public static final Color DEFAULT_COLOR = Color.WHITE;

    @Override
    public String getParent(String child, Module provider) {
        if (child == null) {
            return null;
        }
        int dot = child.lastIndexOf('.');
        if (dot != -1) {
            return child.substring(0, dot);
        }
        return null;

    }

    @Override
    public String parseKey(String key, ClassLoader classLoader, Module provider) {
        return key;
    }

    @Override
    public Color parseValue(String value, ClassLoader classLoader, Module provider) {
        return Color.decode(value.trim());
    }

    @Override
    public Color getDefault() {
        return DEFAULT_COLOR;
    }

}
