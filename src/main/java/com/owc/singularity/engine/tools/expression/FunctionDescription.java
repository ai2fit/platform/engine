/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


import java.util.Objects;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.parameters.ExpressionParameterDialog;


/**
 * Describes a function with a name, description, number of arguments and return type.
 *
 * @author Ingo Mierswa, Sabrina Kirstein
 */
public class FunctionDescription {

    public static final int UNFIXED_NUMBER_OF_ARGUMENTS = -1;

    /** the i18n key suffix for getting the function name with parameters */
    private static final String KEY_SUFFIX_PARAMETERS = ".parameters";

    /** the i18n key suffix for getting the function description */
    private static final String KEY_SUFFIX_DESCRIPTION = ".description";

    /** the i18n key suffix for getting the help text name */
    private static final String KEY_SUFFIX_HELP = ".help";

    /** the i18n key suffix for getting the function name */
    private static final String KEY_SUFFIX_NAME = ".name";

    /** the i18n key suffix for getting the function name */
    private static final String KEY_SUFFIX_GROUP = ".group";

    /** the i18n key prefix for function descriptions */
    private static final String GUI_KEY_PREFIX = "gui.dialog.function.";

    private String displayName;

    private String helpTextName;

    /** the part which is shown as description in the {@link ExpressionParameterDialog} */
    private String functionDescription;

    /** name of the group under which this is shown in the {@link ExpressionParameterDialog} */
    private String groupName;

    private final int numberOfArguments;

    /** the part which is shown as return type in the {@link ExpressionParameterDialog} */
    private final ValueType returnType;

    /** the part which is shown as title in the {@link ExpressionParameterDialog} */
    private String functionNameWithParameters;
    private final String i18nKey;

    /**
     * Creates a {@link FunctionDescription} with number of arguments and return type as given and
     * with name, description, etc. read from the i18nKey. The functionName is read from
     * "gui.dialog.function.i18nKey.name", the helpTextName from ".help", the groupName from
     * ".group", the description from ".description" and the function with parameters from
     * ".parameters". If ".parameters" is not present, the ".name" is taken for the function with
     * parameters.
     *
     * @param i18nKey
     *            the key determining the function description
     * @param numberOfArguments
     *            the number of arguments the function takes, or -1 if this is not a fixed number
     * @param returnType
     *            the {@link ValueType} of the result of the function
     * @since 6.5.0
     */
    public FunctionDescription(String i18nKey, int numberOfArguments, ValueType returnType) {
        this.i18nKey = GUI_KEY_PREFIX + i18nKey;
        this.numberOfArguments = numberOfArguments;
        this.returnType = returnType;
    }

    /**
     * Returns the name of the function that should be displayed when inserting the function. Ends
     * with () if the function is not an operation.
     *
     * @return the function name
     */
    public String getDisplayName() {
        if (displayName == null) {
            displayName = I18N.getGUIMessage(i18nKey + KEY_SUFFIX_NAME);
        }
        return this.displayName;
    }

    /**
     * Returns the help text name that is displayed in the help text tooltip.
     *
     * @return the help text name of the function
     */
    public String getHelpTextName() {
        if (helpTextName == null) {
            helpTextName = I18N.getGUIMessage(i18nKey + KEY_SUFFIX_HELP);
        }
        return this.helpTextName;
    }

    /**
     * Returns the name of the group this function belongs to. The function is displayed under this
     * group name in the {@link ExpressionParameterDialog}.
     *
     * @return the group the function belongs to
     */
    public String getGroupName() {
        if (groupName == null) {
            groupName = I18N.getGUIMessage(i18nKey + KEY_SUFFIX_GROUP);
        }
        return this.groupName;
    }

    /**
     * Returns the description of the function.
     *
     * @return the function description
     */
    public String getDescription() {
        if (functionDescription == null) {
            functionDescription = I18N.getGUIMessage(i18nKey + KEY_SUFFIX_DESCRIPTION);
        }
        return this.functionDescription;
    }

    /**
     * Returns the number of arguments one should check for this function. Returns -1 if there is
     * more than one possible number of arguments.
     *
     * @return the number of arguments to check for this function or -1
     */
    public int getNumberOfArguments() {
        return this.numberOfArguments;
    }

    /**
     * Returns the return value type of this function.
     *
     * @see com.owc.singularity.engine.object.data.exampleset.ValueType
     */
    public ValueType getReturnType() {
        return this.returnType;
    }

    /**
     * Returns the function name with parameter type and dummy name
     */
    public String getFunctionNameWithParameters() {
        if (functionNameWithParameters == null) {
            String nameWithParameters = I18N.getGUIMessageOrNull(i18nKey + KEY_SUFFIX_PARAMETERS);
            functionNameWithParameters = nameWithParameters != null ? nameWithParameters : getDisplayName();
        }
        return this.functionNameWithParameters;
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName, displayName);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof FunctionDescription)) {
            return false;
        }
        FunctionDescription otherFunctionDescription = (FunctionDescription) other;
        return getGroupName().equals(otherFunctionDescription.getGroupName()) && getDisplayName().equals(otherFunctionDescription.getDisplayName());
    }
}
