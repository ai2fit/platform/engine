/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.time.Instant;

import com.owc.singularity.engine.object.data.exampleset.ValueType;

/**
 * A {@link com.owc.singularity.engine.tools.expression.Function} for comparing two dates.
 *
 * @author David Arnu
 *
 */
public class DateBefore extends Abstract2DateInputBooleanOutput {

    public DateBefore() {
        super("date.date_before");
    }

    /**
     * Compares two dates and returns true if the the second date is before the first
     */
    @Override
    protected Boolean compute(long left, long right) {
        if (ValueType.isMissing(left) || ValueType.isMissing(right)) {
            return null;
        }
        return Instant.ofEpochMilli(left).isBefore(Instant.ofEpochMilli(right));
    }

}
