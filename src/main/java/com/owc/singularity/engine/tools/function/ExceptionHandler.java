package com.owc.singularity.engine.tools.function;

import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.OperatorRuntimeException;

public class ExceptionHandler {

    @FunctionalInterface
    public interface ThrowingFunction<A, B> {

        public B apply(A a) throws OperatorException;
    }

    @FunctionalInterface
    public interface ThrowingIntUnaryOperator {

        int applyAsInt(int operand) throws OperatorException;
    }

    @FunctionalInterface
    public interface ThrowingIntFunction<S> {

        S apply(int operand) throws OperatorException;
    }

    @FunctionalInterface
    public interface ThrowingDoubleUnaryOperator {

        double applyAsDouble(double operand) throws OperatorException;
    }


    @FunctionalInterface
    public interface ThrowingObjIntFunction<T, R> {

        /**
         * Performs this operation on the given arguments.
         *
         * @param t
         *            the first input argument
         * @param value
         *            the second input argument
         */
        R accept(T t, int value) throws OperatorException;
    }

    @FunctionalInterface
    public interface ThrowingBiConsumer<T, R> {

        void apply(T t, R r) throws OperatorException;
    }

    public static <A, B> Function<A, B> throwing(ThrowingFunction<A, B> function) {
        return (A a) -> {
            try {
                return function.apply(a);
            } catch (OperatorException e) {
                throw new OperatorRuntimeException(e);
            }
        };

    }

    public static <S> IntFunction<S> throwing(ThrowingIntFunction<S> function) {
        return (i) -> {
            try {
                return function.apply(i);
            } catch (OperatorException e) {
                throw new OperatorRuntimeException(e);
            }
        };

    }

    public static IntUnaryOperator throwing(ThrowingIntUnaryOperator function) {
        return (i) -> {
            try {
                return function.applyAsInt(i);
            } catch (OperatorException e) {
                throw new OperatorRuntimeException(e);
            }
        };

    }

    public static DoubleUnaryOperator throwing(ThrowingDoubleUnaryOperator function) {
        return (d) -> {
            try {
                return function.applyAsDouble(d);
            } catch (OperatorException e) {
                throw new OperatorRuntimeException(e);
            }
        };

    }
}
