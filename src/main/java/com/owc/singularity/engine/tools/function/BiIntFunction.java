package com.owc.singularity.engine.tools.function;

@FunctionalInterface
public interface BiIntFunction<T> {

    public T apply(int x, int y);
}
