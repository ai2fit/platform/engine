/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.conversion;


import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 *
 * A {@link Function} parsing a string or a number to a date with respect to the locale string.
 *
 * @author Marcel Seifert
 *
 */
public class DateParseWithLocale extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     */
    public DateParseWithLocale() {
        super("conversion.date_parse_loc", 2, ValueType.TIMESTAMP);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 2, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator date = inputEvaluators[0];
        ExpressionEvaluator locale = inputEvaluators[1];

        return new SimpleExpressionEvaluator(type, makeDateCallable(date, locale), isResultConstant(inputEvaluators));
    }

    /**
     * Builds a Date Callable from one String or double input argument and one String argument
     *
     * @param date
     *            the input date
     * @param locale
     *            the input locale
     *
     * @return the resulting NominalSupplier
     */
    protected TimestampSupplier makeDateCallable(final ExpressionEvaluator date, final ExpressionEvaluator locale) {

        final NominalSupplier funcDateString = date.getType() == ExpressionType.NOMINAL ? date.getNominalSupplier() : null;
        final NumericSupplier funcDateDouble = date.getType() != ExpressionType.NOMINAL ? date.getNumericSupplier() : null;
        final NominalSupplier funcLocale = locale.getNominalSupplier();

        try {
            final String valueLocale = locale.isConstant() ? funcLocale.getOrThrow() : null;

            if (funcDateString != null) {
                final String valueDate = date.isConstant() ? funcDateString.getOrThrow() : null;
                if (date.isConstant() && locale.isConstant()) {
                    final long result = compute(valueDate, valueLocale);
                    return () -> result;
                } else if (date.isConstant()) {
                    return () -> compute(valueDate, funcLocale.getOrThrow());

                } else if (locale.isConstant()) {
                    return () -> compute(funcDateString.getOrThrow(), valueLocale);

                } else {
                    return () -> compute(funcDateString.getOrThrow(), funcLocale.getOrThrow());
                }
            } else {
                final double valueDate = date.isConstant() ? funcDateDouble.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                if (date.isConstant() && locale.isConstant()) {
                    final long result = compute(valueDate, valueLocale);
                    return () -> result;
                } else if (date.isConstant()) {
                    return () -> compute(valueDate, funcLocale.getOrThrow());

                } else if (locale.isConstant()) {
                    return () -> compute(funcDateDouble.getOrThrow(), valueLocale);

                } else {
                    return () -> compute(funcDateDouble.getOrThrow(), funcLocale.getOrThrow());
                }
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for a double input value and a locale string.
     *
     * @param value
     *            the input timestamp
     * @param localeString
     *            locale string
     * @return the result of the computation.
     */
    protected long compute(double value, String localeString) {
        if (ValueType.isMissing(value) || localeString == null) {
            return ValueType.MISSING_TIMESTAMP;
        }
        Locale locale = new Locale(localeString);
        long dateLong = (long) value;
        Date date = new Date(dateLong);
        Calendar cal = Calendar.getInstance(locale);
        cal.setTime(date);
        return cal.getTimeInMillis();
    }

    /**
     * Computes the result for a string input value and a locale string.
     *
     * @param dateString
     *            the date string
     * @param localeString
     *            the locale string
     * @return the result of the computation.
     */
    protected long compute(String dateString, String localeString) {

        if (ValueType.isMissing(dateString) || localeString == null) {
            return ValueType.MISSING_TIMESTAMP;
        }
        Locale locale = new Locale(localeString);
        Date date;
        try {
            // clone because getDateInstance uses an internal pool which can return the
            // same instance for multiple threads
            date = ((DateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale).clone()).parse(dateString);
            Calendar cal = Calendar.getInstance(locale);
            cal.setTime(date);
            return cal.getTimeInMillis();
        } catch (ParseException e) {
            throw new FunctionInputException("invalid_argument.date", getFunctionName());
        }
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType date = inputTypes[0];
        ExpressionType locale = inputTypes[1];
        if (!(date == ExpressionType.NOMINAL || date == ExpressionType.DOUBLE || date == ExpressionType.INTEGER)) {
            throw new FunctionInputException("expression_parser.function_wrong_type.argument_two", 1, getFunctionName(), "nominal", "numerical");
        } else if (locale != ExpressionType.NOMINAL) {
            throw new FunctionInputException("expression_parser.function_wrong_type.argument", 2, getFunctionName(), "nominal");
        } else {
            return ExpressionType.TIMESTAMP;
        }
    }

}
