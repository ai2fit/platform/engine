/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


/**
 * Interface for a constant used inside an expression that is parsed by an {@link ExpressionParser}.
 *
 * @author Gisa Schaefer
 * @since 6.5.0
 */
public interface Constant {

    /**
     * @return the {@link ExpressionType}
     */
    ExpressionType getType();

    /**
     * @return the name
     */
    String getName();

    /**
     * @return the string value if the constant has type {@link ExpressionType#NOMINAL}
     * @throws IllegalStateException
     *             if the type is not {@link ExpressionType#NOMINAL}
     */
    String getStringValue();

    /**
     * @return the double value if the constant has type {@link ExpressionType#DOUBLE}
     * @throws IllegalStateException
     *             if the type is not {@link ExpressionType#DOUBLE}
     */
    double getNumericValue();

    /**
     * @return the boolean value if the constant has type {@link ExpressionType#BOOLEAN}
     * @throws IllegalStateException
     *             if the type is not {@link ExpressionType#BOOLEAN}
     */
    boolean getBooleanValue();

    /**
     * @return the Date value if the constant has type {@link ExpressionType#TIMESTAMP}
     * @throws IllegalStateException
     *             if the type is not {@link ExpressionType#TIMESTAMP}
     */
    long getTimestampValue();

    /**
     * Returns the annotation of this constant, for example a description of a constant or where it
     * is used.
     *
     * @return the annotation
     */
    String getAnnotation();

    /**
     * @return is this {@link Constant} should be visible in the UI
     */
    boolean isInvisible();
}
