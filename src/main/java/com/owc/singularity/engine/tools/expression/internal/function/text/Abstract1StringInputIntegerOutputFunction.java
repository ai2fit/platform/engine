/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.text;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link AbstractFunction} which allow exactly one nominal value as input and has an integer as
 * output.
 *
 * @author Thilo Kamradt
 *
 */
public abstract class Abstract1StringInputIntegerOutputFunction extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     *
     * @param i18nKey
     *            the key for the {@link FunctionDescription}. The functionName is read from
     *            "gui.dialog.function.i18nKey.name", the helpTextName from ".help", the groupName
     *            from ".group", the description from ".description" and the function with
     *            parameters from ".parameters". If ".parameters" is not present, the ".name" is
     *            taken for the function with parameters.
     */
    public Abstract1StringInputIntegerOutputFunction(String i18nKey) {
        super(i18nKey, 1, ValueType.NUMERIC);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 1, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator eEvaluator = inputEvaluators[0];

        return new SimpleExpressionEvaluator(makeStringCallable(eEvaluator), type, isResultConstant(inputEvaluators));
    }

    /**
     * Builds a DoubleCallable from left and right using {@link #compute(String, String)}, where
     * constant child results are evaluated.
     *
     * @param left
     *            the left input
     * @param right
     *            the right input
     * @return the resulting DoubleCallable
     */
    protected NumericSupplier makeStringCallable(ExpressionEvaluator evaluator) {
        final NominalSupplier funcEvaluator = evaluator.getNominalSupplier();
        try {

            final String valueLeft = evaluator.isConstant() ? funcEvaluator.getOrThrow() : "";

            if (evaluator.isConstant()) {
                final double result = compute(valueLeft);

                return new NumericSupplier() {

                    @Override
                    public double getOrThrow() throws Exception {
                        return result;
                    }

                };
            } else {
                return new NumericSupplier() {

                    @Override
                    public double getOrThrow() throws Exception {
                        return compute(funcEvaluator.getOrThrow());
                    }
                };

            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for one input String value.
     *
     * @param value1
     * @return the result of the computation.
     */
    protected abstract double compute(String value1);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        if (inputTypes[0] == ExpressionType.NOMINAL) {
            return ExpressionType.INTEGER;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "nominal");
        }
    }
}
