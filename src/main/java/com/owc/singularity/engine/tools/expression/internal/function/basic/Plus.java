/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.basic;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.Abstract2DoubleInputFunction;


/**
 * A {@link Function} for addition.
 *
 * @author Gisa Schaefer
 *
 */
public class Plus extends Abstract2DoubleInputFunction {

    /**
     * The {@link ExpressionType}s that are allowed to be added to a ExpressionType.STRING
     */
    private static final Set<ExpressionType> STRING_ALLOWED_SET = new HashSet<ExpressionType>(
            Arrays.asList(ExpressionType.NOMINAL, ExpressionType.DOUBLE, ExpressionType.INTEGER, ExpressionType.BOOLEAN));

    /**
     * Constructs an addition function.
     */
    public Plus() {
        super("basic.addition", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, null);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 2 && inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), 1, 2, inputEvaluators.length);
        }

        ExpressionType type = getResultType(inputEvaluators);

        if (type == ExpressionType.DOUBLE || type == ExpressionType.INTEGER) {
            if (inputEvaluators.length == 1) {
                return inputEvaluators[0];
            } else {
                return new SimpleExpressionEvaluator(makeDoubleCallable(inputEvaluators[0], inputEvaluators[1]), type, isResultConstant(inputEvaluators));
            }
        } else {
            // return type is string
            return new SimpleExpressionEvaluator(makeStringCallable(inputEvaluators[0], inputEvaluators[1]), type, isResultConstant(inputEvaluators));
        }

    }

    /**
     * Constructs a String callable from left and right by distinguishing the types of left and
     * right and using the associated compute method.
     *
     * @param left
     * @param right
     * @return
     */
    private NominalSupplier makeStringCallable(ExpressionEvaluator left, ExpressionEvaluator right) {
        try {

            if (left.getType() == ExpressionType.NOMINAL && right.getType() == ExpressionType.NOMINAL) {
                final NominalSupplier funcLeft = left.getNominalSupplier();
                final NominalSupplier funcRight = right.getNominalSupplier();

                final String valueLeft = left.isConstant() ? funcLeft.getOrThrow() : null;
                final String valueRight = right.isConstant() ? funcRight.getOrThrow() : null;

                if (left.isConstant() && right.isConstant()) {
                    final String result = compute(valueLeft, valueRight);
                    return () -> result;
                } else if (left.isConstant()) {
                    return () -> compute(valueLeft, funcRight.getOrThrow());
                } else if (right.isConstant()) {
                    return () -> compute(funcLeft.getOrThrow(), valueRight);
                } else {
                    return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
                }
            } else if (left.getType() == ExpressionType.NOMINAL) {
                final NominalSupplier funcLeft = left.getNominalSupplier();
                final String valueLeft = left.isConstant() ? funcLeft.getOrThrow() : null;

                if (right.getType() == ExpressionType.DOUBLE || right.getType() == ExpressionType.INTEGER) {
                    final NumericSupplier funcRight = right.getNumericSupplier();
                    final double valueRight = right.isConstant() ? funcRight.getOrThrow() : Double.NaN;
                    final boolean isInteger = right.getType() == ExpressionType.INTEGER;

                    if (left.isConstant() && right.isConstant()) {
                        final String result = compute(valueLeft, valueRight, isInteger);
                        return () -> result;
                    } else if (left.isConstant()) {
                        return () -> compute(valueLeft, funcRight.getOrThrow(), isInteger);
                    } else if (right.isConstant()) {
                        return () -> compute(funcLeft.getOrThrow(), valueRight, isInteger);
                    } else {
                        return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow(), isInteger);
                    }
                } else { // right is boolean
                    final BooleanSupplier funcRight = right.getBooleanSupplier();
                    final Boolean valueRight = right.isConstant() ? funcRight.getOrThrow() : null;

                    if (left.isConstant() && right.isConstant()) {
                        final String result = compute(valueLeft, valueRight);
                        return () -> result;
                    } else if (left.isConstant()) {
                        return () -> compute(valueLeft, funcRight.getOrThrow());
                    } else if (right.isConstant()) {
                        return () -> compute(funcLeft.getOrThrow(), valueRight);
                    } else {
                        return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
                    }
                }
            } else { // right is STRING
                final NominalSupplier funcRight = right.getNominalSupplier();
                final String valueRight = right.isConstant() ? funcRight.getOrThrow() : null;

                if (left.getType() == ExpressionType.DOUBLE || left.getType() == ExpressionType.INTEGER) {
                    final NumericSupplier funcLeft = left.getNumericSupplier();
                    final double valueLeft = left.isConstant() ? funcLeft.getOrThrow() : Double.NaN;
                    final boolean isInteger = left.getType() == ExpressionType.INTEGER;

                    if (left.isConstant() && right.isConstant()) {
                        final String result = compute(valueLeft, valueRight, isInteger);
                        return () -> result;
                    } else if (left.isConstant()) {
                        return () -> compute(valueLeft, funcRight.getOrThrow(), isInteger);
                    } else if (right.isConstant()) {
                        return () -> compute(funcLeft.getOrThrow(), valueRight, isInteger);
                    } else {
                        return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow(), isInteger);
                    }
                } else { // left is boolean
                    final BooleanSupplier funcLeft = left.getBooleanSupplier();
                    final Boolean valueLeft = left.isConstant() ? funcLeft.getOrThrow() : null;

                    if (left.isConstant() && right.isConstant()) {
                        final String result = compute(valueLeft, valueRight);
                        return () -> result;
                    } else if (left.isConstant()) {
                        return () -> compute(valueLeft, funcRight.getOrThrow());
                    } else if (right.isConstant()) {
                        return () -> compute(funcLeft.getOrThrow(), valueRight);
                    } else {
                        return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
                    }
                }
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Adds two Strings. If both are missing, then the result is missing; if one is missing, it is
     * ignored.
     */
    private String compute(String left, String right) {
        if (left == null && right == null) {
            return null;
        }
        return (left == null ? "" : left) + (right == null ? "" : right);
    }

    /**
     * Adds a String and a double. If the double represents an integer tries to add it without
     * trailing '.0'. If both are missing, then the result is missing; if one is missing, it is
     * ignored. If the double is infinite, formats is using a symbol.
     *
     * @param left
     * @param right
     * @param isInteger
     *            whether the double represents an {@link ExpressionType#INTEGER}
     * @return
     */
    private String compute(String left, double right, boolean isInteger) {
        if (left == null && Double.isNaN(right)) {
            return null;
        }
        if (isInteger && right == (int) right) {
            return (left == null ? "" : left) + (int) right;
        } else if (Double.isInfinite(right)) {
            return (left == null ? "" : left) + Tools.formatNumber(right);
        } else {
            return (left == null ? "" : left) + (Double.isNaN(right) ? "" : right);
        }
    }

    /**
     * Adds a String and a Boolean. If both are missing, then the result is missing; if one is
     * missing, it is ignored.
     */
    private String compute(String left, Boolean right) {
        if (left == null && right == null) {
            return null;
        }
        return (left == null ? "" : left) + (right == null ? "" : right);
    }

    /**
     * Adds a Boolean and a String. If both are missing, then the result is missing; if one is
     * missing, it is ignored.
     */
    private String compute(Boolean left, String right) {
        if (left == null && right == null) {
            return null;
        }
        return (left == null ? "" : left) + (right == null ? "" : right);
    }

    /**
     * Adds a double and a String. If the double represents an integer tries to add it without
     * trailing '.0'. If both are missing, then the result is missing; if one is missing, it is
     * ignored. If the double is infinite, formats is using a symbol.
     *
     * @param left
     * @param right
     * @param isInteger
     *            whether the double represents an {@link ExpressionType#INTEGER}
     * @return
     */
    private String compute(double left, String right, boolean isInteger) {
        if (Double.isNaN(left) && right == null) {
            return null;
        }
        if (isInteger && left == (int) left) {
            return (int) left + (right == null ? "" : right);
        } else if (Double.isInfinite(left)) {
            return Tools.formatNumber(left) + (right == null ? "" : right);
        } else {
            return (Double.isNaN(left) ? "" : left) + (right == null ? "" : right);
        }
    }

    @Override
    protected double compute(double value1, double value2) {
        return value1 + value2;
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType firstType = inputTypes[0];
        if (inputTypes.length == 1) {
            if (firstType == ExpressionType.INTEGER || firstType == ExpressionType.DOUBLE) {
                return firstType;
            } else {
                throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "numerical");
            }
        } else {
            ExpressionType secondType = inputTypes[1];
            if (firstType == ExpressionType.INTEGER && secondType == ExpressionType.INTEGER) {
                return ExpressionType.INTEGER;
            } else if ((firstType == ExpressionType.INTEGER || firstType == ExpressionType.DOUBLE)
                    && (secondType == ExpressionType.INTEGER || secondType == ExpressionType.DOUBLE)) {
                return ExpressionType.DOUBLE;
            } else if ((firstType == ExpressionType.NOMINAL || secondType == ExpressionType.NOMINAL) && STRING_ALLOWED_SET.contains(firstType)
                    && STRING_ALLOWED_SET.contains(secondType)) {
                return ExpressionType.NOMINAL;
            } else {
                throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "numerical or nominal");
            }
        }

    }

}
