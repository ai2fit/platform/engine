/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.logging.LoggingHandler;


/**
 * Forwards log messages to a {@link Logger}
 * 
 * @author Simon Fischer
 */
public class WrapperLoggingHandler implements LoggingHandler {

    public static final Level[] LEVELS = { Level.ALL, Level.TRACE, Level.DEBUG,

            Level.INFO, Level.INFO, Level.WARN, Level.ERROR, Level.ERROR, Level.ERROR, Level.OFF };

    private Logger logger;

    public WrapperLoggingHandler() {}

    public WrapperLoggingHandler(Logger logger) {
        this.logger = logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void log(String message, int level) {
        logger.log(LEVELS[level], message);
    }

    @Override
    public void log(String message) {
        logger.info(message);
    }

    @Override
    public void logError(String message) {
        logger.error(message);
    }

    @Override
    public void logNote(String message) {
        logger.debug(message);
    }

    @Override
    public void logWarning(String message) {
        logger.warn(message);
    }
}
