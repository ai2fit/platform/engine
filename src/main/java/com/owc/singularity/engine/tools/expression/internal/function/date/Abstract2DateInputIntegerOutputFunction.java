/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Abstract class for a {@link Function} that has two date arguments and returns an integer. The
 * function can has time zone and locale information as optional arguments.
 *
 * @author David Arnu
 *
 */
public abstract class Abstract2DateInputIntegerOutputFunction extends AbstractFunction {

    public Abstract2DateInputIntegerOutputFunction(String i18nKey, int numberOfArgumentsToCheck) {
        super(i18nKey, numberOfArgumentsToCheck, ValueType.NUMERIC);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        ExpressionType type = getResultType(inputEvaluators);

        if (inputEvaluators.length == 2) {

            ExpressionEvaluator left = inputEvaluators[0];
            ExpressionEvaluator right = inputEvaluators[1];
            return new SimpleExpressionEvaluator(makeDoubleCallable(left, right, null, null), type, isResultConstant(inputEvaluators));
        } else {
            ExpressionEvaluator left = inputEvaluators[0];
            ExpressionEvaluator right = inputEvaluators[1];
            ExpressionEvaluator locale = inputEvaluators[2];
            ExpressionEvaluator timeZone = inputEvaluators[3];
            return new SimpleExpressionEvaluator(makeDoubleCallable(left, right, locale, timeZone), type, isResultConstant(inputEvaluators));
        }

    }

    protected NumericSupplier makeDoubleCallable(ExpressionEvaluator left, ExpressionEvaluator right, ExpressionEvaluator locale,
            ExpressionEvaluator timeZone) {

        final TimestampSupplier funcLeft = left.getTimestampSupplier();
        final TimestampSupplier funcRight = right.getTimestampSupplier();
        final NominalSupplier funcLocale;
        final NominalSupplier funcTimeZone;

        if (locale != null) {
            funcLocale = locale.getNominalSupplier();
        } else {
            // create an dummy ExpressionEvaluator for the missing locale argument
            locale = new SimpleExpressionEvaluator("", ExpressionType.NOMINAL);
            funcLocale = () -> null;
        }
        if (timeZone != null) {
            funcTimeZone = timeZone.getNominalSupplier();
        } else {
            // create an dummy ExpressionEvaluator for the missing time zone argument
            timeZone = new SimpleExpressionEvaluator("", ExpressionType.NOMINAL);
            funcTimeZone = () -> null;
        }

        try {
            final long valueLeft = left.isConstant() ? funcLeft.getOrThrow() : ValueType.MISSING_TIMESTAMP;
            final long valueRight = right.isConstant() ? funcRight.getOrThrow() : ValueType.MISSING_TIMESTAMP;
            final String valueLocale = locale.isConstant() ? funcLocale.getOrThrow() : null;
            final String valueTimezone = timeZone.isConstant() ? funcTimeZone.getOrThrow() : null;

            // only check for common combinations of constant values

            // all constant values
            if (left.isConstant() && right.isConstant() && locale.isConstant() && timeZone.isConstant()) {
                final double result = compute(valueLeft, valueRight, valueLocale, valueTimezone);

                return () -> result;
            } else if (left.isConstant() && !right.isConstant()) {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(valueLeft, funcRight.getOrThrow(), valueLocale, valueTimezone);
                } else {
                    return () -> compute(valueLeft, funcRight.getOrThrow(), funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
            } else if (!left.isConstant() && right.isConstant()) {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(funcLeft.getOrThrow(), valueRight, valueLocale, valueTimezone);
                } else {
                    return () -> compute(funcLeft.getOrThrow(), valueRight, funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
                // both dates are variable
            } else {
                // branch with constant locale and time zone data, probably both are constant or
                // both are not
                if (locale.isConstant() && timeZone.isConstant()) {
                    return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow(), valueLocale, valueTimezone);
                } else {
                    return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow(), funcLocale.getOrThrow(), funcTimeZone.getOrThrow());
                }
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two input date values with additional optional locale and time zone
     * arguments.
     *
     * @param left
     *            first date
     * @param right
     *            second date
     * @param valueLocale
     *            locale string, can be null
     * @param valueTimezone
     *            time zone string, can be null
     * @return the result of the computation.
     */
    protected abstract double compute(long left, long right, String valueLocale, String valueTimezone);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes.length != 2 && inputTypes.length != 4) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), "2", "4", inputTypes.length);
        }
        ExpressionType firstType = inputTypes[0];
        ExpressionType secondType = inputTypes[1];
        if (firstType != ExpressionType.TIMESTAMP || secondType != ExpressionType.TIMESTAMP) {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "date");
        }
        if (inputTypes.length == 4) {
            if (inputTypes[2] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "third");
            }
            if (inputTypes[3] != ExpressionType.NOMINAL) {
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "string", "fourth");
            }
        }
        return ExpressionType.INTEGER;

    }

}
