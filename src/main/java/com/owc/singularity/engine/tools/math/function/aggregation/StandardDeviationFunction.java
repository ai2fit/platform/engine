/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


/**
 * Calculates the standard deviation.
 *
 * @author Tobias Malbrecht
 *
 */
public class StandardDeviationFunction extends VarianceFunction {

    private String STANDARD_DEVIATION_AGGREGATION_FUNCTION_NAME = "standard_deviation";

    public StandardDeviationFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public StandardDeviationFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return STANDARD_DEVIATION_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public double aggregateNumericValue() {
        // return the square root of the variance
        double value = super.aggregateNumericValue();
        if (value > 0) {
            return Math.sqrt(value);
        }
        return 0d;
    }

    @Override
    public long aggregateTimestampValue() throws UnsupportedOperationException {
        // return the square root of the variance
        long value = super.aggregateTimestampValue();
        if (value > 0) {
            return (long) Math.sqrt(value);
        }
        return 0L;
    }
}
