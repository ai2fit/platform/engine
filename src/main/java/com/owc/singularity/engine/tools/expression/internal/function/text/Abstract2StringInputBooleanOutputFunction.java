/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.text;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 *
 * Abstract class for a {@link Function} that has two String arguments and returns a boolean
 * argument.
 *
 * @author David Arnu
 *
 */
public abstract class Abstract2StringInputBooleanOutputFunction extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     *
     * @param i18nKey
     *            the key for the {@link FunctionDescription}. The functionName is read from
     *            "gui.dialog.function.i18nKey.name", the helpTextName from ".help", the groupName
     *            from ".group", the description from ".description" and the function with
     *            parameters from ".parameters". If ".parameters" is not present, the ".name" is
     *            taken for the function with parameters.
     * @param numberOfArgumentsToCheck
     *            the fixed number of parameters this functions expects or -1
     */
    public Abstract2StringInputBooleanOutputFunction(String i18n) {
        super(i18n, 2, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 2, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator left = inputEvaluators[0];
        ExpressionEvaluator right = inputEvaluators[1];

        return new SimpleExpressionEvaluator(makeBooleanCallable(left, right), isResultConstant(inputEvaluators), type);
    }

    /**
     * Builds a Boolean callable from left and right using {@link #compute(String, String)}, where
     * constant child results are evaluated.
     *
     * @param left
     *            the left input
     * @param right
     *            the right input
     * @return the resulting BooleanSupplier
     */
    protected BooleanSupplier makeBooleanCallable(ExpressionEvaluator left, ExpressionEvaluator right) {
        final NominalSupplier funcLeft = left.getNominalSupplier();
        final NominalSupplier funcRight = right.getNominalSupplier();
        try {

            final String valueLeft = left.isConstant() ? funcLeft.getOrThrow() : "";
            final String valueRight = right.isConstant() ? funcRight.getOrThrow() : "";

            if (left.isConstant() && right.isConstant()) {
                final Boolean result = compute(valueLeft, valueRight);

                return () -> result;
            } else if (left.isConstant()) {
                return () -> compute(valueLeft, funcRight.getOrThrow());

            } else if (right.isConstant()) {
                return () -> compute(funcLeft.getOrThrow(), valueRight);

            } else {
                return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two input String values.
     *
     * @return the result of the computation.
     */
    protected abstract Boolean compute(String value1, String value2);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType left = inputTypes[0];
        ExpressionType right = inputTypes[1];
        if (left == ExpressionType.NOMINAL && right == ExpressionType.NOMINAL) {
            return ExpressionType.BOOLEAN;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "nominal");
        }
    }

}
