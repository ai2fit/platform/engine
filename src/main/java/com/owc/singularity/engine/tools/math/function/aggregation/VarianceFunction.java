/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the variance.
 *
 * @author Tobias Malbrecht, Ingo Mierswa
 */
public class VarianceFunction extends AbstractAggregationFunction {

    private String VARIANCE_AGGREGATION_FUNCTION_NAME = "variance";

    private double valueSum;

    private double squaredValueSum;

    private double totalWeightSum;

    private double count;

    public VarianceFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public VarianceFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return VARIANCE_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        valueSum = 0d;
        squaredValueSum = 0d;
        totalWeightSum = 0d;
        count = 0d;
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        valueSum += value;
        squaredValueSum += value * value;
        totalWeightSum++;
        count++;
    }

    @Override
    public void update(long value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        valueSum += value;
        squaredValueSum += value * value;
        totalWeightSum++;
        count++;
    }

    @Override
    public double aggregateNumericValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NUMERIC;
        }
        if (count <= 1 || totalWeightSum <= 0) {
            return 0;
        }
        return (squaredValueSum - valueSum * valueSum / totalWeightSum) / ((count - 1) / count * totalWeightSum);
    }

    @Override
    public long aggregateTimestampValue() throws UnsupportedOperationException {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_TIMESTAMP;
        }
        if (count <= 1 || totalWeightSum <= 0) {
            return 0;
        }
        return (long) ((squaredValueSum - valueSum * valueSum / totalWeightSum) / ((count - 1) / count * totalWeightSum));
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return !ValueType.NOMINAL.equals(valueType);
    }
}
