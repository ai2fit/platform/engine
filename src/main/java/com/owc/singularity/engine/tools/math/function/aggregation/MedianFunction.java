/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import java.util.Map;
import java.util.TreeMap;

import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Calculates the median of some values.
 * 
 * @author Tobias Malbrecht
 * 
 */
public class MedianFunction extends AbstractAggregationFunction {

    private String MEDIAN_AGGREGATION_FUNCTION_NAME = "median";

    private TreeMap<Double, Double> valueWeightMap = new TreeMap<>();

    private double totalWeight;

    public MedianFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public MedianFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public String getName() {
        return MEDIAN_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public void reset() {
        foundMissing = false;
        totalWeight = 0;
        if (valueWeightMap != null) {
            valueWeightMap.clear();
        }
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        Double totalValueWeight = valueWeightMap.get(value);
        if (totalValueWeight != null) {
            totalValueWeight++;
        } else {
            totalValueWeight = 1.0;
        }
        valueWeightMap.put(value, totalValueWeight);
        totalWeight++;
    }

    @Override
    public void update(long value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        Double totalValueWeight = valueWeightMap.get((double) value);
        if (totalValueWeight != null) {
            totalValueWeight++;
        } else {
            totalValueWeight = 1.0;
        }
        valueWeightMap.put((double) value, totalValueWeight);
        totalWeight++;
    }

    @Override
    public double aggregateNumericValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_NUMERIC;
        }
        double valueWeightSum = 0;
        double lastValue = ValueType.MISSING_NUMERIC;
        double lastWeight = ValueType.MISSING_NUMERIC;
        // TODO: check weighted median calculation: Middle treatment seems suspicious. Sorted arrays
        // might be much more memory efficient
        for (Map.Entry<Double, Double> entry : valueWeightMap.entrySet()) {
            if (!ValueType.isMissing(lastValue) && !ValueType.isMissing(lastWeight)) {
                double thisWeight = entry.getValue();
                return (lastValue * lastWeight + entry.getKey() * thisWeight) / (lastWeight + thisWeight);
            }
            valueWeightSum += entry.getValue();
            if (valueWeightSum > totalWeight / 2) {
                return entry.getKey();
            }
            // Now check for the case that we are EXACTLY on the middle. Then we have to average
            // with the next value
            if (valueWeightSum == totalWeight / 2) {
                lastWeight = entry.getValue();
                lastValue = entry.getKey();
            }
        }
        return ValueType.MISSING_NUMERIC;
    }

    @Override
    public long aggregateTimestampValue() {
        if (foundMissing && !ignoreMissings) {
            return ValueType.MISSING_TIMESTAMP;
        }
        double valueWeightSum = 0;
        double lastValue = ValueType.MISSING_TIMESTAMP;
        double lastWeight = ValueType.MISSING_TIMESTAMP;
        // TODO: check weighted median calculation: Middle treatment seems suspicious. Sorted arrays
        // might be much more memory efficient
        for (Map.Entry<Double, Double> entry : valueWeightMap.entrySet()) {
            if (!ValueType.isMissing(lastValue) && !ValueType.isMissing(lastWeight)) {
                double thisWeight = entry.getValue();
                return (long) ((lastValue * lastWeight + entry.getKey() * thisWeight) / (lastWeight + thisWeight));
            }
            valueWeightSum += entry.getValue();
            if (valueWeightSum > totalWeight / 2) {
                return entry.getKey().longValue();
            }
            // Now check for the case that we are EXACTLY on the middle. Then we have to average
            // with the next value
            if (valueWeightSum == totalWeight / 2) {
                lastWeight = entry.getValue();
                lastValue = entry.getKey();
            }
        }
        return ValueType.MISSING_TIMESTAMP;
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return !ValueType.NOMINAL.equals(valueType);
    }
}
