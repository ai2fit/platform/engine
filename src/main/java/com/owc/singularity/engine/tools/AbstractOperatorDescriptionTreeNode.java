/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.util.*;

import javax.swing.ImageIcon;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.OperatorService.OperatorServiceListener;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * A group tree manages operator descriptions in a tree like manner. This is useful to present the
 * operators in groups and subgroups and eases operator selection in the GUI.
 * <p>
 * The group tree heavily depends on the associated OperatorService, since it reflects the
 * registered Operators of that Service. Each {@link OperatorService} can have multiple GroupTrees,
 * which register as listener to be able to update on new registration or unregistration events.
 * <p>
 * The listening is done by the {@link OperatorDescriptionTreeRoot} class implementing the
 * {@link OperatorServiceListener} interface.
 * 
 * @author Ingo Mierswa, Sebastian Land
 */
public abstract class AbstractOperatorDescriptionTreeNode implements Comparable<AbstractOperatorDescriptionTreeNode> {

    private static final ImageIcon[] NO_ICONS = new ImageIcon[3];

    /** The list of operators in this group. */
    private final List<OperatorDescription> operators = new LinkedList<>();

    /** The subgroups of this group. */
    private final Map<String, OperatorDescriptionTreeNode> children = new LinkedHashMap<>();

    private String iconName;

    private ImageIcon[] icons;

    protected AbstractOperatorDescriptionTreeNode() {

    }

    /**
     * Clone constructor. This will keep the link to the {@link OperatorService}. Whenever a new
     * Operator is added or removed, this will be reflected in the copy as well. For detecting such
     * events, please register on
     * {@link OperatorService#addOperatorServiceListener(OperatorServiceListener)}.
     */
    protected AbstractOperatorDescriptionTreeNode(AbstractOperatorDescriptionTreeNode other) {
        this.iconName = other.iconName;
        this.icons = other.icons;
        this.operators.addAll(other.operators);
        for (AbstractOperatorDescriptionTreeNode child : other.getSubGroups()) {
            addSubGroup((OperatorDescriptionTreeNode) child.clone());
        }
    }

    /** Returns the parent of this group. Returns null if no parent does exist. */
    public abstract AbstractOperatorDescriptionTreeNode getParent();

    /** Returns or creates the subgroup with the given key. This is not the fully qualified key! */
    public AbstractOperatorDescriptionTreeNode getSubGroup(String key) {
        synchronized (children) {
            return children.get(key);
        }
    }

    /** Returns or creates the subgroup with the given name, creating it if not present. */
    public AbstractOperatorDescriptionTreeNode getOrCreateSubGroup(String key) {
        OperatorDescriptionTreeNode child = (OperatorDescriptionTreeNode) getSubGroup(key);
        if (child == null) {
            child = new OperatorDescriptionTreeNode(this, key);
            addSubGroup(child);
        }
        return child;
    }

    /** Returns a set of all children group trees. */
    public Collection<? extends AbstractOperatorDescriptionTreeNode> getSubGroups() {
        synchronized (children) {
            return children.values();
        }
    }

    /**
     * Returns the index of the given subgroup or -1 if the subgroup is not a child of this node.
     */
    public int getIndexOfSubGroup(AbstractOperatorDescriptionTreeNode child) {
        Iterator<? extends AbstractOperatorDescriptionTreeNode> i = getSubGroups().iterator();
        int index = 0;
        while (i.hasNext()) {
            AbstractOperatorDescriptionTreeNode current = i.next();
            if (current.equals(child)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    /** Returns the i-th subgroup. */
    public AbstractOperatorDescriptionTreeNode getSubGroup(int index) {
        Collection<? extends AbstractOperatorDescriptionTreeNode> allChildren = getSubGroups();
        if (index < allChildren.size()) {
            Iterator<? extends AbstractOperatorDescriptionTreeNode> i = allChildren.iterator();
            int counter = 0;
            while (i.hasNext()) {
                AbstractOperatorDescriptionTreeNode current = i.next();
                if (counter == index) {
                    return current;
                }
                counter++;
            }
        }
        return null;
    }

    /** Adds an operator to this group. */
    protected void addOperatorDescription(OperatorDescription description) {
        synchronized (operators) {
            operators.add(description);
        }
    }

    /**
     * This removes the given {@link OperatorDescription} from this GroupTree
     */
    protected void removeOperatorDescription(OperatorDescription description) {
        synchronized (operators) {
            operators.remove(description);
        }
    }

    /**
     * Returns all operator descriptions in this group or an empty list if this group does not
     * contain any operators.
     */
    public List<OperatorDescription> getOperatorDescriptions() {
        return operators;
    }

    public int getOperatorDescriptionCount() {
        return countOperators();
    }

    /**
     * This returns a deep clone of this {@link AbstractOperatorDescriptionTreeNode}.
     */
    @Override
    public abstract AbstractOperatorDescriptionTreeNode clone();

    /**
     * This returns a group description depending on internationalization
     */
    public abstract String getDescription();

    /**
     * Gets the key of this group. This is used for internationalization and does not contain the
     * parent groups. See {@link #getFullyQualifiedKey()} for this one.
     */
    public abstract String getKey();

    /**
     * Returns the fully qualified key where each group key is separated by dot. The qualified key
     * starts with the highest parent group.
     */
    public abstract String getFullyQualifiedKey();

    /**
     * Deprecated method that returns the fully qualified key. See {@link #getFullyQualifiedKey()}
     * for Details.
     */
    @Deprecated
    public String getQName() {
        return getFullyQualifiedKey();
    }

    /** Returns the name of this group. This name depends on internationalization! */
    public abstract String getName();

    public void setIconName(String icon) {
        this.iconName = icon;
        loadIcons();
    }

    public String getIconName() {
        if (this.iconName != null) {
            return this.iconName;
        } else {
            return null;
        }
    }

    public ImageIcon[] getIcons() {
        return Objects.requireNonNullElse(icons, NO_ICONS);
    }

    protected final int countOperators() {
        int count = operators.size();
        synchronized (children) {
            for (AbstractOperatorDescriptionTreeNode tree : children.values()) {
                count += tree.countOperators();
            }
        }
        return count;

    }

    /**
     * This method will sort this GroupTree according to the given comparator.
     */
    public void sort(Comparator<OperatorDescription> operatorComparator, Comparator<String> folderComparator) {
        synchronized (operators) {
            operators.sort(operatorComparator);
        }
        synchronized (children) {
            LinkedHashMap<String, OperatorDescriptionTreeNode> sortedChildren = new LinkedHashMap<>();
            children.keySet().stream().sorted(folderComparator).forEach(k -> sortedChildren.put(k, children.get(k)));
            children.clear();
            children.putAll(sortedChildren);
            for (AbstractOperatorDescriptionTreeNode child : children.values()) {
                child.sort(operatorComparator, folderComparator);
            }
        }
    }

    /** Adds a subgroup to this group. */
    private void addSubGroup(OperatorDescriptionTreeNode child) {
        synchronized (children) {
            children.put(child.getKey(), child);
        }
        child.setParent(this);
    }

    /**
     * Loads the icons if defined.
     */
    private void loadIcons() {
        if (iconName == null) {
            icons = null;
            return;
        }
        icons = new ImageIcon[3];
        icons[0] = SwingTools.createIcon("16/" + iconName);
        icons[1] = SwingTools.createIcon("24/" + iconName);
        icons[2] = SwingTools.createIcon("48/" + iconName);
    }

    @Override
    public String toString() {
        String result = getName();
        if (getParent() == null) {
            result = "Root";
        }
        int size = countOperators();
        return result + (size > 0 ? " (" + size + ")" : "");
    }

    @Override
    public int compareTo(AbstractOperatorDescriptionTreeNode o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AbstractOperatorDescriptionTreeNode)) {
            return false;
        }
        return getName().equals(((AbstractOperatorDescriptionTreeNode) o).getName());
    }

    @Override
    public int hashCode() {
        return this.getKey().hashCode();
    }

}
