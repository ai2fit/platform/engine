/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.rounding;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.Abstract2DoubleInputFunction;


/**
 *
 * Abstract class for a {@link Function} that has either one or two double arguments.
 *
 * @author David Arnu
 *
 */
public abstract class Abstract1or2DoubleInputFunction extends Abstract2DoubleInputFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     *
     * @param i18nKey
     *            the key for the {@link FunctionDescription}. The functionName is read from
     *            "gui.dialog.function.i18nKey.name", the helpTextName from ".help", the groupName
     *            from ".group", the description from ".description" and the function with
     *            parameters from ".parameters". If ".parameters" is not present, the ".name" is
     *            taken for the function with parameters.
     * @param returnType
     *            the {@link ValueType}
     */
    public Abstract1or2DoubleInputFunction(String i18nKey, ValueType returnType) {
        super(i18nKey, FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, returnType);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length == 2) {
            ExpressionType type = getResultType(inputEvaluators);

            ExpressionEvaluator left = inputEvaluators[0];
            ExpressionEvaluator right = inputEvaluators[1];

            return new SimpleExpressionEvaluator(makeDoubleCallable(left, right), type, isResultConstant(inputEvaluators));
        } else if (inputEvaluators.length == 1) {
            ExpressionType type = getResultType(inputEvaluators);
            ExpressionEvaluator input = inputEvaluators[0];
            return new SimpleExpressionEvaluator(makeDoubleCallable(input), type, isResultConstant(inputEvaluators));

        }
        throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), 1, 2, inputEvaluators.length);
    }

    /**
     * Builds a double callable from a single input {@link #compute(double)}, where constant child
     * results are evaluated.
     *
     * @param input
     *            the input
     * @return the resulting double callable
     */
    private NumericSupplier makeDoubleCallable(ExpressionEvaluator input) {
        final NumericSupplier func = input.getNumericSupplier();

        try {
            final Double value = input.isConstant() ? func.getOrThrow() : Double.NaN;
            if (input.isConstant()) {
                final double result = compute(value);
                return new NumericSupplier() {

                    @Override
                    public double getOrThrow() throws Exception {
                        return result;
                    }
                };
            } else {
                return new NumericSupplier() {

                    @Override
                    public double getOrThrow() throws Exception {
                        return compute(func.getOrThrow());
                    }
                };
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the value a single input argument
     *
     * @param value
     * @return the result of the computation
     */
    protected abstract double compute(double value);

}
