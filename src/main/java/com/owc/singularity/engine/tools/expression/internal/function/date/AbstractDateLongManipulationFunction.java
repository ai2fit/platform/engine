/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.ExpressionParserConstants;


/**
 * Abstract base for adding/setting a date
 *
 * @author Jan Czogalla, David Arnu
 * @since 9.6.0
 */
public abstract class AbstractDateLongManipulationFunction extends AbstractDateManipulationFunction {

    /**
     * maximum that can be properly processed in {@link Calendar#add(int, int)} or
     * {@link Calendar#set(int, int)} calls
     */
    static final int MAX_DATE_VALUE = Integer.MAX_VALUE - 10000;

    AbstractDateLongManipulationFunction(String i18nKey, int numberOfArgumentsToCheck, ValueType returnType) {
        super(i18nKey, numberOfArgumentsToCheck, returnType);
    }

    @Override
    protected long compute(long date, double value, String unit, String valueLocale, String valueTimezone) {
        return compute(() -> null, date, value, unit, valueLocale, valueTimezone);
    }

    @Override
    protected long compute(Callable<Void> stopChecker, long date, double value, String unit, String valueLocale, String valueTimezone) {
        ZoneId zone;
        if (valueTimezone == null) {
            zone = ZoneId.systemDefault();
        } else {
            zone = TimeZone.getTimeZone(valueTimezone).toZoneId();
        }

        // for missing values as arguments, a missing value is returned
        if (ValueType.isMissing(date) || unit == null || Double.isNaN(value) || Double.isInfinite(value)) {
            return ValueType.MISSING_TIMESTAMP;
        }

        ZonedDateTime zonedDateTime = Instant.ofEpochMilli(date).atZone(zone);
        long dateValue = (long) value;
        int sign = dateValue < 0 ? -1 : 1;
        dateValue *= sign;
        TemporalUnit dateUnit = mapUnitNameToIndex(unit);
        ZonedDateTime manipulation;
        if (dateValue <= MAX_DATE_VALUE) {
            manipulation = integerManipulation(stopChecker, zonedDateTime, dateUnit, (int) (dateValue * sign));
        } else {
            manipulation = longManipulation(stopChecker, zonedDateTime, dateValue, sign, dateUnit);
        }


        return manipulation.toInstant().toEpochMilli();
    }

    /**
     * Integer date manipulation; this sets/adds the given integer value of the specified calendar
     * field. Subclasses must implemented this method accordingly.
     */
    protected abstract ZonedDateTime integerManipulation(Callable<Void> stopChecker, ZonedDateTime cal, TemporalUnit unit, int amount);

    /**
     * Manipulate date when the absolute value parsed exceeds {@link #MAX_DATE_VALUE}. Might call
     * {@link #integerManipulation(Callable, ZonedDateTime, TemporalUnit, int)} first with a
     * modified {@code dateValue}.
     */
    protected abstract ZonedDateTime longManipulation(Callable<Void> stopChecker, ZonedDateTime cal, long amount, int sign, TemporalUnit dateUnit);

    /** Maps the name of the unit to an integer unit identifier of {@link Calendar} */
    private TemporalUnit mapUnitNameToIndex(String unit) {
        switch (unit) {
            case ExpressionParserConstants.DATE_UNIT_YEAR:
                return ChronoUnit.YEARS;
            case ExpressionParserConstants.DATE_UNIT_MONTH:
                return ChronoUnit.MONTHS;
            case ExpressionParserConstants.DATE_UNIT_WEEK:
                return ChronoUnit.WEEKS;
            case ExpressionParserConstants.DATE_UNIT_DAY:
                return ChronoUnit.DAYS;

            case ExpressionParserConstants.DATE_UNIT_HOUR:
                return ChronoUnit.HOURS;
            case ExpressionParserConstants.DATE_UNIT_MINUTE:
                return ChronoUnit.MINUTES;
            case ExpressionParserConstants.DATE_UNIT_SECOND:
                return ChronoUnit.SECONDS;
            case ExpressionParserConstants.DATE_UNIT_MILLISECOND:
                return ChronoUnit.MILLIS;
            default:
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "unit constant", "third");
        }
    }
}
