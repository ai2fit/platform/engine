/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.comparison;


import java.util.Date;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Abstract class for a equality check function that has two arbitrary inputs
 *
 * @author Sabrina Kirstein
 */
public abstract class AbstractEqualityFunctionWith2Inputs extends AbstractFunction {

    /**
     * Constructs an equality check Function with 2 parameters with {@link FunctionDescription}
     */
    public AbstractEqualityFunctionWith2Inputs(String i18nKey) {
        super(i18nKey, 2, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {

        if (inputEvaluators.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 2, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);
        ExpressionEvaluator left = inputEvaluators[0];
        ExpressionEvaluator right = inputEvaluators[1];

        return new SimpleExpressionEvaluator(makeBooleanCallable(left, right), isResultConstant(inputEvaluators), type);
    }

    /**
     * Builds a boolean callable from two {@link ExpressionEvaluator}s, where constant child results
     * are evaluated.
     *
     * @param left
     *            {@link ExpressionEvaluator}
     * @param right
     *            {@link ExpressionEvaluator}
     * @return the resulting boolean callable
     */
    protected BooleanSupplier makeBooleanCallable(ExpressionEvaluator left, ExpressionEvaluator right) {

        ExpressionType leftType = left.getType();
        ExpressionType rightType = right.getType();

        // Any type checked against any type is allowed
        try {
            switch (leftType) {
                case INTEGER:
                case DOUBLE:
                    final NumericSupplier funcLeftDouble = left.getNumericSupplier();
                    final double valueLeftDouble = left.isConstant() ? funcLeftDouble.getOrThrow() : Double.NaN;

                    switch (rightType) {
                        case INTEGER:
                        case DOUBLE:
                            final NumericSupplier funcRightDouble = right.getNumericSupplier();
                            final double valueRightDouble = right.isConstant() ? funcRightDouble.getOrThrow() : Double.NaN;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDouble, valueRightDouble);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDouble, funcRightDouble.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDouble.getOrThrow(), valueRightDouble);
                            } else {
                                return () -> compute(funcLeftDouble.getOrThrow(), funcRightDouble.getOrThrow());
                            }
                        case NOMINAL:
                            final NominalSupplier funcRightString = right.getNominalSupplier();
                            final String valueRightString = right.isConstant() ? funcRightString.getOrThrow() : null;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDouble, valueRightString);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDouble, funcRightString.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDouble.getOrThrow(), valueRightString);
                            } else {
                                return () -> compute(funcLeftDouble.getOrThrow(), funcRightString.getOrThrow());
                            }
                        case TIMESTAMP:
                            final TimestampSupplier funcRightDate = right.getTimestampSupplier();
                            final long valueRightDate = right.isConstant() ? funcRightDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDouble, valueRightDate);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDouble, funcRightDate.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDouble.getOrThrow(), valueRightDate);
                            } else {
                                return () -> compute(funcLeftDouble.getOrThrow(), funcRightDate.getOrThrow());
                            }
                        case BOOLEAN:
                            final BooleanSupplier funcRightBoolean = right.getBooleanSupplier();
                            final Boolean valueRightBoolean = right.isConstant() ? funcRightBoolean.getOrThrow() : null;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDouble, valueRightBoolean);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDouble, funcRightBoolean.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDouble.getOrThrow(), valueRightBoolean);
                            } else {
                                return () -> compute(funcLeftDouble.getOrThrow(), funcRightBoolean.getOrThrow());
                            }
                        default:
                            return null;
                    }
                case NOMINAL:
                    final NominalSupplier funcLeftString = left.getNominalSupplier();
                    final String valueLeftString = left.isConstant() ? funcLeftString.getOrThrow() : ValueType.MISSING_NOMINAL;

                    switch (rightType) {
                        case INTEGER:
                        case DOUBLE:
                            final NumericSupplier funcRightDouble = right.getNumericSupplier();
                            final double valueRightDouble = right.isConstant() ? funcRightDouble.getOrThrow() : Double.NaN;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftString, valueRightDouble);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftString, funcRightDouble.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftString.getOrThrow(), valueRightDouble);
                            } else {
                                return () -> compute(funcLeftString.getOrThrow(), funcRightDouble.getOrThrow());
                            }
                        case NOMINAL:
                            final NominalSupplier funcRightString = right.getNominalSupplier();
                            final String valueRightString = right.isConstant() ? funcRightString.getOrThrow() : ValueType.MISSING_NOMINAL;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftString, valueRightString);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftString, funcRightString.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftString.getOrThrow(), valueRightString);
                            } else {
                                return () -> compute(funcLeftString.getOrThrow(), funcRightString.getOrThrow());
                            }
                        case TIMESTAMP:
                            final TimestampSupplier funcRightDate = right.getTimestampSupplier();
                            final long valueRightDate = right.isConstant() ? funcRightDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftString, valueRightDate);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftString, funcRightDate.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftString.getOrThrow(), valueRightDate);
                            } else {
                                return () -> compute(funcLeftString.getOrThrow(), funcRightDate.getOrThrow());
                            }
                        case BOOLEAN:
                            final BooleanSupplier funcRightBoolean = right.getBooleanSupplier();
                            final Boolean valueRightBoolean = right.isConstant() ? funcRightBoolean.getOrThrow() : null;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftString, valueRightBoolean);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftString, funcRightBoolean.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftString.getOrThrow(), valueRightBoolean);
                            } else {
                                return () -> compute(funcLeftString.getOrThrow(), funcRightBoolean.getOrThrow());
                            }
                        default:
                            return null;
                    }
                case TIMESTAMP:
                    final TimestampSupplier funcLeftDate = left.getTimestampSupplier();
                    final long valueLeftDate = left.isConstant() ? funcLeftDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;

                    switch (rightType) {
                        case INTEGER:
                        case DOUBLE:
                            final NumericSupplier funcRightDouble = right.getNumericSupplier();
                            final double valueRightDouble = right.isConstant() ? funcRightDouble.getOrThrow() : Double.NaN;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDate, valueRightDouble);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDate, funcRightDouble.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDate.getOrThrow(), valueRightDouble);
                            } else {
                                return () -> compute(funcLeftDate.getOrThrow(), funcRightDouble.getOrThrow());
                            }
                        case NOMINAL:
                            final NominalSupplier funcRightString = right.getNominalSupplier();
                            final String valueRightString = right.isConstant() ? funcRightString.getOrThrow() : ValueType.MISSING_NOMINAL;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDate, valueRightString);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDate, funcRightString.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDate.getOrThrow(), valueRightString);
                            } else {
                                return () -> compute(funcLeftDate.getOrThrow(), funcRightString.getOrThrow());
                            }
                        case TIMESTAMP:
                            final TimestampSupplier funcRightDate = right.getTimestampSupplier();
                            final long valueRightDate = right.isConstant() ? funcRightDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDate, valueRightDate);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDate, funcRightDate.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDate.getOrThrow(), valueRightDate);
                            } else {
                                return () -> compute(funcLeftDate.getOrThrow(), funcRightDate.getOrThrow());
                            }
                        case BOOLEAN:
                            final BooleanSupplier funcRightBoolean = right.getBooleanSupplier();
                            final Boolean valueRightBoolean = right.isConstant() ? funcRightBoolean.getOrThrow() : null;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftDate, valueRightBoolean);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftDate, funcRightBoolean.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftDate.getOrThrow(), valueRightBoolean);
                            } else {
                                return () -> compute(funcLeftDate.getOrThrow(), funcRightBoolean.getOrThrow());
                            }
                        default:
                            return null;
                    }
                case BOOLEAN:
                    final BooleanSupplier funcLeftBoolean = left.getBooleanSupplier();
                    final Boolean valueLeftBoolean = left.isConstant() ? funcLeftBoolean.getOrThrow() : null;

                    switch (rightType) {
                        case INTEGER:
                        case DOUBLE:
                            final NumericSupplier funcRightDouble = right.getNumericSupplier();
                            final double valueRightDouble = right.isConstant() ? funcRightDouble.getOrThrow() : Double.NaN;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftBoolean, valueRightDouble);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftBoolean, funcRightDouble.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftBoolean.getOrThrow(), valueRightDouble);
                            } else {
                                return () -> compute(funcLeftBoolean.getOrThrow(), funcRightDouble.getOrThrow());
                            }
                        case NOMINAL:
                            final NominalSupplier funcRightString = right.getNominalSupplier();
                            final String valueRightString = right.isConstant() ? funcRightString.getOrThrow() : ValueType.MISSING_NOMINAL;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftBoolean, valueRightString);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftBoolean, funcRightString.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftBoolean.getOrThrow(), valueRightString);
                            } else {
                                return () -> compute(funcLeftBoolean.getOrThrow(), funcRightString.getOrThrow());
                            }
                        case TIMESTAMP:
                            final TimestampSupplier funcRightDate = right.getTimestampSupplier();
                            final long valueRightDate = right.isConstant() ? funcRightDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftBoolean, valueRightDate);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftBoolean, funcRightDate.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftBoolean.getOrThrow(), valueRightDate);
                            } else {
                                return () -> compute(funcLeftBoolean.getOrThrow(), funcRightDate.getOrThrow());
                            }
                        case BOOLEAN:
                            final BooleanSupplier funcRightBoolean = right.getBooleanSupplier();
                            final Boolean valueRightBoolean = right.isConstant() ? funcRightBoolean.getOrThrow() : null;
                            if (left.isConstant() && right.isConstant()) {
                                final Boolean result = compute(valueLeftBoolean, valueRightBoolean);
                                return () -> result;
                            } else if (left.isConstant()) {
                                return () -> compute(valueLeftBoolean, funcRightBoolean.getOrThrow());
                            } else if (right.isConstant()) {
                                return () -> compute(funcLeftBoolean.getOrThrow(), valueRightBoolean);
                            } else {
                                return () -> compute(funcLeftBoolean.getOrThrow(), funcRightBoolean.getOrThrow());
                            }
                        default:
                            return null;
                    }
                default:
                    return null;
            }

        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two double values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(double left, double right);

    /**
     * Computes the result for a double and a boolean value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(double left, Boolean right);

    /**
     * Computes the result for a boolean and a double value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(Boolean left, double right) {
        return compute(right, left);
    }

    /**
     * Computes the result for a double and a String value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(double left, String right);

    /**
     * Computes the result for a String and a double value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(String left, double right) {
        return compute(right, left);
    }

    /**
     * Computes the result for a double value and a date
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(double left, Date right);

    /**
     * Computes the result for a date value and a double value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(Date left, double right) {
        return compute(right, left);
    }

    /**
     * Computes the result for two boolean values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(Boolean left, Boolean right);

    /**
     * Computes the result for a boolean and a String value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(Boolean left, String right);

    /**
     * Computes the result for a String value and a boolean value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(String left, Boolean right) {
        return compute(right, left);
    }

    /**
     * Computes the result for a boolean value and a String value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(Boolean left, Date right);

    /**
     * Computes the result for a date value and a boolean value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(Date left, Boolean right) {
        return compute(right, left);
    }

    /**
     * Computes the result for two String values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(String left, String right);

    /**
     * Computes the result for a String value and a Date value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(String left, Date right);

    /**
     * Computes the result for a Date value and a String value
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected Boolean compute(Date left, String right) {
        return compute(right, left);
    }

    /**
     * Computes the result for two Date values.
     *
     * @param left
     *            value
     * @param right
     *            value
     * @return the result of the computation.
     */
    protected abstract Boolean compute(Date left, Date right);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        if (inputTypes.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), "2", inputTypes.length);
        }
        // result is always boolean
        return ExpressionType.BOOLEAN;
    }
}
