package com.owc.singularity.engine.tools.function;

@FunctionalInterface
public interface ObjObjIntIntConsumer<T, S> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t
     *            the first object input argument
     * @param s
     *            the second object input argument
     * @param value1
     *            the first int input argument
     * @param value2
     *            the second int input argument
     */
    void accept(T t, S s, int value1, int value2);
}
