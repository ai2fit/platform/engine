/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal;


import java.util.concurrent.Callable;

import com.owc.singularity.engine.tools.expression.*;


/**
 * {@link ExpressionEvaluator} that supplies constructors for all admissible combinations of its
 * fields. It checks if the required relationship between {@link ExpressionType} and the Callable
 * that is not {@code null} is satisfied.
 *
 * @author Gisa Schaefer
 *
 */
public class SimpleExpressionEvaluator implements ExpressionEvaluator {

    private final NominalSupplier stringCallable;
    private final TimestampSupplier timestampCallable;
    private final NumericSupplier numericSupplier;
    private final BooleanSupplier booleanSupplier;

    private final ExpressionType type;
    private final boolean isConstant;

    /**
     * Initializes the fields.
     */
    protected SimpleExpressionEvaluator(ExpressionType type, NominalSupplier stringCallable, NumericSupplier numericSupplier, BooleanSupplier booleanSupplier,
            TimestampSupplier timestampCallable, boolean isConstant) {
        this.stringCallable = stringCallable;
        this.timestampCallable = timestampCallable;
        this.numericSupplier = numericSupplier;
        this.booleanSupplier = booleanSupplier;
        this.type = type;
        this.isConstant = isConstant;
    }

    /**
     * Creates an {@link ExpressionEvaluator} with the given data where the other callables are
     * {@code null}. type must be ExpressionType.INTEGER or ExpressionType.DOUBLE.
     *
     * @param numericSupplier
     *            the callable to store
     * @param type
     *            the type of the result of the callable, must be ExpressionType.INTEGER or
     *            ExpressionType.DOUBLE
     * @param isConstant
     *            whether the result of the callable is constant
     */
    public SimpleExpressionEvaluator(NumericSupplier numericSupplier, ExpressionType type, boolean isConstant) {
        this(type, null, numericSupplier, null, null, isConstant);
        if (type != ExpressionType.DOUBLE && type != ExpressionType.INTEGER) {
            throw new IllegalArgumentException("Invalid type " + type + "for Callable");
        }
    }

    /**
     * Creates an {@link ExpressionEvaluator} with the given data where the other callables are
     * {@code null}. type must be ExpressionType.STRING.
     *
     * @param stringCallable
     *            the callable to store
     * @param type
     *            the type of the result of the callable, must be ExpressionType.STRING
     * @param isConstant
     *            whether the result of the callable is constant
     */
    public SimpleExpressionEvaluator(NominalSupplier stringCallable, ExpressionType type, boolean isConstant) {
        this(type, stringCallable, null, null, null, isConstant);
        if (type != ExpressionType.NOMINAL) {
            throw new IllegalArgumentException("Invalid type " + type + "for Callable");
        }
    }

    /**
     * Creates an {@link ExpressionEvaluator} with the given data where the other callables are
     * {@code null}. type must be ExpressionType.STRING.
     *
     * @param timestampCallable
     *            the callable to store
     * @param type
     *            the type of the result of the callable, must be ExpressionType.DATE
     * @param isConstant
     *            whether the result of the callable is constant
     */
    public SimpleExpressionEvaluator(ExpressionType type, TimestampSupplier timestampCallable, boolean isConstant) {
        this(type, null, null, null, timestampCallable, isConstant);
        if (type != ExpressionType.TIMESTAMP) {
            throw new IllegalArgumentException("Invalid type " + type + "for Callable");
        }
    }

    /**
     * Creates an {@link ExpressionEvaluator} with the given data where the other callables are
     * {@code null}. type must be ExpressionType.BOOLEAN.
     *
     * @param booleanSupplier
     *            the callable to store
     * @param type
     *            the type of the result of the callable, must be ExpressionType.BOOLEAN
     * @param isConstant
     *            whether the result of the callable is constant
     */
    public SimpleExpressionEvaluator(BooleanSupplier booleanSupplier, boolean isConstant, ExpressionType type) {
        this(type, null, null, booleanSupplier, null, isConstant);
        if (type != ExpressionType.BOOLEAN) {
            throw new IllegalArgumentException("Invalid type " + type + "for Callable");
        }
    }

    /**
     * Creates an {@link ExpressionEvaluator} with a {@link NumericSupplier} returning constantly
     * doubleValue. type must be ExpressionType.INTEGER or ExpressionType.DOUBLE.
     *
     * @param doubleValue
     *            the constant double return value
     * @param type
     *            the type of the result of the callable, must be ExpressionType.INTEGER or
     *            ExpressionType.DOUBLE
     */
    public SimpleExpressionEvaluator(double doubleValue, ExpressionType type) {
        this(makeConstantCallable(doubleValue), type, true);
    }

    /**
     * Creates an {@link ExpressionEvaluator} with a {@link NominalSupplier} returning constantly
     * stringValue. type must be ExpressionType.STRING.
     *
     * @param stringValue
     *            the constant String return value
     * @param type
     *            the type of the result of the callable, must be ExpressionType.STRING
     */
    public SimpleExpressionEvaluator(String stringValue, ExpressionType type) {
        this(makeConstantCallable(stringValue), type, true);
    }

    /**
     * Creates an {@link ExpressionEvaluator} with a {@link Callable<Boolean>} returning constantly
     * booleanValue.
     *
     * @param booleanValue
     *            the constant Boolean return value
     * @param type
     *            the type of the result of the callable, must be ExpressionType.BOOLEAN
     */
    public SimpleExpressionEvaluator(boolean booleanValue, ExpressionType type) {
        this(makeConstantCallable(booleanValue), true, type);
    }

    /**
     * Creates an {@link ExpressionEvaluator} with a {@link TimestampSupplier} returning constantly
     * timestampValue.
     *
     * @param timestampValue
     *            the constant Date return value
     * @param type
     *            type the type of the result of the callable, must be ExpressionType.DATE
     */
    public SimpleExpressionEvaluator(long timestampValue, ExpressionType type) {
        this(type, makeConstantCallable(timestampValue), true);
    }

    private static NumericSupplier makeConstantCallable(final double numericValue) {
        return () -> numericValue;
    }

    private static TimestampSupplier makeConstantCallable(final long timestampValue) {
        return () -> timestampValue;
    }

    private static NominalSupplier makeConstantCallable(final String nominalValue) {
        return () -> nominalValue;
    }

    private static BooleanSupplier makeConstantCallable(final boolean booleanValue) {
        return () -> booleanValue;
    }

    @Override
    public ExpressionType getType() {
        return type;
    }

    @Override
    public boolean isConstant() {
        return isConstant;
    }

    @Override
    public NominalSupplier getNominalSupplier() {
        return stringCallable;
    }

    @Override
    public TimestampSupplier getTimestampSupplier() {
        return timestampCallable;
    }

    @Override
    public NumericSupplier getNumericSupplier() {
        return numericSupplier;
    }

    @Override
    public BooleanSupplier getBooleanSupplier() {
        return booleanSupplier;
    }

}
