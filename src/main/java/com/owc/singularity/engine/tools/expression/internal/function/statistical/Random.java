/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.statistical;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.RandomGenerator;
import com.owc.singularity.engine.tools.expression.ExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.ExpressionParsingException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.NumericSupplier;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link Function} that delivers random numbers.
 *
 * @author Gisa Schaefer
 *
 */
public class Random extends AbstractFunction {

    private final AbstractPipeline process;

    /**
     * Creates a function that delivers random numbers using the random generator associated to the
     * process.
     *
     * @param process
     */
    public Random(AbstractPipeline process) {
        super("statistical.rand", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.NUMERIC);
        this.process = process;
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length > 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), 0, 1, inputEvaluators.length);
        }
        ExpressionType resultType = getResultType(inputEvaluators);

        NumericSupplier numericSupplier;
        if (inputEvaluators.length == 1) {
            numericSupplier = makeDoubleCallable(inputEvaluators[0]);
        } else {
            // if no seed is passed use -1 to get global random generator
            numericSupplier = makeDoubleCallable(-1);
        }
        return new SimpleExpressionEvaluator(numericSupplier, resultType, false);
    }

    /**
     * Creates a double callable. If the evaluator is constant this callable returns the next random
     * of a random generator with the given seed. If the evaluator is not constant, it creates a new
     * random generator with the changing seed for every call.
     *
     * @param evaluator
     *            the evaluator that determines the random seed
     * @return a double callable
     */
    private NumericSupplier makeDoubleCallable(final ExpressionEvaluator evaluator) {
        try {
            if (evaluator.isConstant()) {
                // if the seed is fixed create one random generator with this seed
                return makeDoubleCallable((int) evaluator.getNumericSupplier().getOrThrow());
            } else {
                // if the seed is not fixed create a new generator with this seed on every call
                return new NumericSupplier() {

                    @Override
                    public double getOrThrow() throws Exception {
                        int seed = (int) evaluator.getNumericSupplier().getOrThrow();
                        RandomGenerator randomGenerator = RandomGenerator.getRandomGenerator(process, seed);
                        return randomGenerator.nextDouble();
                    }

                };
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Creates a {@link NumericSupplier} that calls the next random of a random generator with the
     * given seed.
     *
     * @param seed
     *            the seed, if negative the seed of the process root operator is used
     * @return a double callable
     */
    private NumericSupplier makeDoubleCallable(int seed) {
        final RandomGenerator randomGenerator = RandomGenerator.getRandomGenerator(process, seed);
        return new NumericSupplier() {

            @Override
            public double getOrThrow() throws Exception {
                return randomGenerator.nextDouble();
            }

        };
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        if (inputTypes.length > 0 && inputTypes[0] != ExpressionType.INTEGER && inputTypes[0] != ExpressionType.DOUBLE) {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "double or integer");
        }
        return ExpressionType.DOUBLE;
    }

    @Override
    protected boolean isConstantOnConstantInput() {
        return false;
    }

}
