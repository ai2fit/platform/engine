/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.text;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * A {@link Function} to cut a nominal value.
 *
 * @author Thilo Kamradt
 *
 */
public class Cut extends AbstractFunction {

    /**
     * Creates a function to cut a nominal value.
     */
    public Cut() {
        super("text_transformation.cut", 3, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 3) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 3, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator text = inputEvaluators[0];
        ExpressionEvaluator startIndex = inputEvaluators[1];
        ExpressionEvaluator length = inputEvaluators[2];

        return new SimpleExpressionEvaluator(makeStringCallable(text, startIndex, length), type, isResultConstant(inputEvaluators));
    }

    /**
     * Builds a DoubleCallable from left and right using {@link #compute(String, String)}, where
     * constant child results are evaluated.
     *
     * @param left
     *            the left input
     * @param right
     *            the right input
     * @return the resulting DoubleCallable
     */
    protected NominalSupplier makeStringCallable(ExpressionEvaluator text, ExpressionEvaluator startIndex, ExpressionEvaluator length) {

        final NominalSupplier funcText = text.getNominalSupplier();
        final NumericSupplier funcIndex = startIndex.getNumericSupplier();
        final NumericSupplier funcLength = length.getNumericSupplier();
        try {

            final String valueText = text.isConstant() ? funcText.getOrThrow() : "";
            final double valueIndex = startIndex.isConstant() ? funcIndex.getOrThrow() : Double.NaN;
            final double valueLength = length.isConstant() ? funcLength.getOrThrow() : Double.NaN;

            // all three expressions are const
            if (text.isConstant() && startIndex.isConstant() && length.isConstant()) {
                final String result = compute(valueText, valueIndex, valueLength);

                return () -> result;
                // two const expressions
            } else if (!text.isConstant() && startIndex.isConstant() && length.isConstant()) {
                return () -> compute(funcText.getOrThrow(), valueIndex, valueLength);
            } else if (text.isConstant() && !startIndex.isConstant() && length.isConstant()) {
                return () -> compute(valueText, funcIndex.getOrThrow(), valueLength);
            } else if (text.isConstant() && startIndex.isConstant() && !length.isConstant()) {
                return () -> compute(valueText, valueIndex, funcLength.getOrThrow());
                // only one expression is const
            } else if (text.isConstant() && !startIndex.isConstant() && !length.isConstant()) {
                return () -> compute(valueText, funcIndex.getOrThrow(), funcLength.getOrThrow());
            } else if (!text.isConstant() && startIndex.isConstant() && !length.isConstant()) {
                return () -> compute(funcText.getOrThrow(), valueIndex, funcLength.getOrThrow());
            } else if (!text.isConstant() && !startIndex.isConstant() && length.isConstant()) {
                return () -> compute(funcText.getOrThrow(), funcIndex.getOrThrow(), valueLength);
            } else {
                // no expression is const
                return () -> compute(funcText.getOrThrow(), funcIndex.getOrThrow(), funcLength.getOrThrow());
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result.
     *
     * @param text
     * @param index
     * @param length
     * @return the result of the computation.
     */
    protected String compute(String text, double index, double length) {
        if (Double.isNaN(index)) {
            // this must be changed for compatibility with the old parser
            index = 0;
        }
        if (Double.isNaN(length)) {
            // this must be changed for compatibility with the old parser
            length = 0;
        }
        if (text == null) {
            return null;
        } else if (index < 0 || length < 0) {
            throw new FunctionInputException("expression_parser.function_non_negative", getFunctionName());
        } else if (index + length > text.length()) {
            throw new FunctionInputException("expression_parser.parameter_value_too_big", "start", "length", getFunctionName(), text);
        }
        return text.substring((int) index, (int) index + (int) length);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        if (inputTypes[0] == ExpressionType.NOMINAL && (inputTypes[1] == ExpressionType.INTEGER || inputTypes[1] == ExpressionType.DOUBLE)
                && (inputTypes[2] == ExpressionType.INTEGER || inputTypes[2] == ExpressionType.DOUBLE)) {
            return ExpressionType.NOMINAL;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "(nominal,integer,integer)");
        }
    }
}
