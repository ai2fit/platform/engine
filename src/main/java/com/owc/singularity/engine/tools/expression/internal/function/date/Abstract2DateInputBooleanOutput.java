/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Abstract class for a {@link Function} that has two date arguments and returns a boolean.
 *
 * @author David Arnu
 *
 */
public abstract class Abstract2DateInputBooleanOutput extends AbstractFunction {

    public Abstract2DateInputBooleanOutput(String i18nKey) {
        super(i18nKey, 2, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 2, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator left = inputEvaluators[0];
        ExpressionEvaluator right = inputEvaluators[1];

        return new SimpleExpressionEvaluator(makeBooleanCallable(left, right), isResultConstant(inputEvaluators), type);
    }

    /**
     * Builds a DoubleCallable from left and right using {@link #compute(long, long)}, where
     * constant child results are evaluated.
     *
     * @param left
     *            the left input
     * @param right
     *            the right input
     * @return the resulting DoubleCallable
     */
    private BooleanSupplier makeBooleanCallable(ExpressionEvaluator left, ExpressionEvaluator right) {
        final TimestampSupplier funcLeft = left.getTimestampSupplier();
        final TimestampSupplier funcRight = right.getTimestampSupplier();
        try {
            final long valueLeft = left.isConstant() ? funcLeft.getOrThrow() : ValueType.MISSING_TIMESTAMP;
            final long valueRight = right.isConstant() ? funcRight.getOrThrow() : ValueType.MISSING_TIMESTAMP;

            if (left.isConstant() && right.isConstant()) {
                final boolean result = compute(valueLeft, valueRight);
                return () -> result;
            } else if (left.isConstant() && !right.isConstant()) {
                return () -> compute(valueLeft, funcRight.getOrThrow());

            } else if (!left.isConstant() && right.isConstant()) {
                return () -> compute(funcLeft.getOrThrow(), valueRight);

            } else {
                return () -> compute(funcLeft.getOrThrow(), funcRight.getOrThrow());
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for two input date values.
     *
     * @return the result of the computation.
     */
    protected abstract Boolean compute(long left, long right);

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType left = inputTypes[0];
        ExpressionType right = inputTypes[1];
        if (left == ExpressionType.TIMESTAMP && right == ExpressionType.TIMESTAMP) {
            return ExpressionType.BOOLEAN;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "date");
        }
    }

}
