/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.date;


import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.TimeZone;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.ExpressionParserConstants;


/**
 * A {@link com.owc.singularity.engine.tools.expression.Function Function} for setting a value of a
 * given date.
 *
 * @author David Arnu, Jan Czogalla
 *
 */
public class DateSet extends AbstractDateManipulationFunction {

    public DateSet() {
        super("date.date_set", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, ValueType.TIMESTAMP);
    }

    @Override
    protected long compute(long date, double value, String unit, String valueLocale, String valueTimezone) {
        ZoneId zone;
        if (valueTimezone == null) {
            zone = ZoneId.systemDefault();
        } else {
            zone = TimeZone.getTimeZone(valueTimezone).toZoneId();
        }

        // for missing values as arguments, a missing value is returned
        if (ValueType.isMissing(date) || unit == null || Double.isNaN(value) || Double.isInfinite(value)) {
            return ValueType.MISSING_TIMESTAMP;
        }

        ZonedDateTime zonedDateTime = Instant.ofEpochMilli(date).atZone(zone);
        long dateValue = (long) value;
        int sign = dateValue < 0 ? -1 : 1;
        dateValue *= sign;
        TemporalField dateUnit = mapUnitNameToField(unit);
        ZonedDateTime manipulation = zonedDateTime.with(dateUnit, dateValue);

        return manipulation.toInstant().toEpochMilli();
    }

    /** Maps the name of the unit to an integer unit identifier of {@link Calendar} */
    private TemporalField mapUnitNameToField(String unit) {
        switch (unit) {
            case ExpressionParserConstants.DATE_UNIT_YEAR:
                return ChronoField.YEAR;
            case ExpressionParserConstants.DATE_UNIT_MONTH:
                return ChronoField.MONTH_OF_YEAR;
            case ExpressionParserConstants.DATE_UNIT_WEEK:
                return ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR;
            case ExpressionParserConstants.DATE_UNIT_DAY:
                return ChronoField.DAY_OF_MONTH;

            case ExpressionParserConstants.DATE_UNIT_HOUR:
                return ChronoField.HOUR_OF_DAY;
            case ExpressionParserConstants.DATE_UNIT_MINUTE:
                return ChronoField.MINUTE_OF_HOUR;
            case ExpressionParserConstants.DATE_UNIT_SECOND:
                return ChronoField.SECOND_OF_MINUTE;
            case ExpressionParserConstants.DATE_UNIT_MILLISECOND:
                return ChronoField.MILLI_OF_SECOND;
            default:
                throw new FunctionInputException("expression_parser.function_wrong_type_at", getFunctionName(), "unit constant", "third");
        }
    }
}
