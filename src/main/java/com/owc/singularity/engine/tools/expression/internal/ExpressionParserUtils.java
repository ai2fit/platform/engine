/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal;


import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetTransformer;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.tools.expression.*;


/**
 *
 * A collections of utility functions for the expression parser.
 *
 * @author David Arnu, Nils Woehler
 *
 */
public final class ExpressionParserUtils {

    private ExpressionParserUtils() {
        throw new UnsupportedOperationException("Static utility class");
    }

    /**
     * This will add a derivation for the given expression to the transformer.
     *
     * @param exampleSet
     *            the example set to which the generated attribute is added
     * @param name
     *            the new attribute name
     * @param expression
     *            the expression used to generate attribute values
     * @param parser
     *            the expression parser used to parse the expression argument
     * @param resolver
     *            the example resolver which is used by the parser to resolve example values
     * @param executingOperator
     *            the operator calling this method. <code>null</code> is allowed. If not null the
     *            operator will be used to check for stop
     *
     */
    public static ExampleSetTransformer addAttributeDerivation(ExampleSetTransformer transformer, ExampleSet exampleSet, String name, String expression,
            ExpressionParser parser, ExampleResolver resolver, Operator executingOperator) throws ExpressionException {

        // remove if attribute is already present
        transformer.withoutAttribute(name);

        // parse the expression
        Expression parsedExpression = parser.parse(expression);
        ValueType valueType = parsedExpression.getExpressionType().getAttributeType();
        switch (valueType) {
            case NOMINAL -> transformer.withDerivedNominalAttribute(name, example -> {
                synchronized (resolver) {
                    resolver.bind(example);
                    return parsedExpression.evaluateNominal();
                }
            });
            case NUMERIC -> transformer.withDerivedNumericAttribute(name, example -> {
                synchronized (resolver) {
                    resolver.bind(example);
                    return parsedExpression.evaluateNumeric();
                }
            });
            case TIMESTAMP -> transformer.withDerivedTimestampAttribute(name, example -> {
                synchronized (resolver) {
                    resolver.bind(example);
                    return parsedExpression.evaluateTimestamp();
                }
            });
        }

        transformer.addCleanUpOperation(resolver::unbind);

        return transformer;
    }

    /**
     * Uses the {@link ExpressionParserBuilder} to create an {@link ExpressionParser} with all
     * modules that are registered to the {@link ExpressionRegistry}.
     *
     * @param op
     *            the operator to create the {@link ExpressionParser} for. Must not be {@code null}
     * @param exampleResolver
     *            the {@link ExampleResolver} which is used to lookup example values. Might be
     *            {@code null} in case no {@link ExampleResolver} is available
     * @return the build expression parser
     */
    public static ExpressionParser createAllModulesParser(final Operator op, final ExampleResolver exampleResolver) {
        ExpressionParserBuilder builder = new ExpressionParserBuilder();

        if (op != null && op.getPipeline() != null) {
            builder.withProcess(op.getPipeline());
            builder.withScope(new VariableResolver(op.getPipeline().getVariableHandler(), op));
        }
        if (exampleResolver != null) {
            builder.withDynamics(exampleResolver);
        }

        builder.withModules(ExpressionRegistry.INSTANCE.getAll());

        return builder.build();
    }

    /**
     * Converts a {@link ExpressionException} into a {@link UserError}.
     *
     * @param op
     *            the calling operator
     * @param function
     *            the entered function
     * @param e
     *            the exception
     * @throws UserError
     *             the converted {@link UserError}
     */
    public static UserError convertToUserError(Operator op, String function, ExpressionException e) {

        // only show up to 15 characters of the function string
        String shortenedFunction = function;
        if (function.length() > 15) {
            shortenedFunction = function.substring(0, 15).concat(" (...)");
        }

        return new UserError(op, e, "expression_evaluation_failed", e.getShortMessage(), shortenedFunction);
    }

}
