/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.conversion;

import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 *
 * A {@link Function} parsing a number to a string.
 *
 * @author Marcel Seifert
 *
 */
public class NumericalToString extends AbstractFunction {

    /**
     * Constructs an AbstractFunction with {@link FunctionDescription} generated from the arguments
     * and the function name generated from the description.
     */
    public NumericalToString() {
        super("conversion.str", 1, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {
        if (inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), 1, inputEvaluators.length);
        }
        ExpressionType type = getResultType(inputEvaluators);

        ExpressionEvaluator input = inputEvaluators[0];

        return new SimpleExpressionEvaluator(makeStringCallable(input), type, isResultConstant(inputEvaluators));
    }

    /**
     * Builds a String Callable from a double input argument
     *
     * @param inputEvaluator
     *            the input
     * @return the resulting NominalSupplier
     */
    protected NominalSupplier makeStringCallable(final ExpressionEvaluator inputEvaluator) {
        final NumericSupplier func = inputEvaluator.getNumericSupplier();

        try {
            if (inputEvaluator.isConstant()) {
                final String result = compute(func.getOrThrow());
                return () -> result;
            } else {
                return () -> compute(func.getOrThrow());
            }
        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    /**
     * Computes the result for one input double value.
     *
     * @param value
     *            the double value to parse
     *
     * @return the result of the computation.
     */
    protected String compute(double value) {
        if (Double.isNaN(value)) {
            return null;
        }
        return Tools.formatIntegerIfPossible(value);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType input = inputTypes[0];
        if (input == ExpressionType.INTEGER || input == ExpressionType.DOUBLE) {
            return ExpressionType.NOMINAL;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "numerical");
        }
    }

}
