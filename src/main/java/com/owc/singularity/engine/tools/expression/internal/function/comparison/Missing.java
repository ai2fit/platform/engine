/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.comparison;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.*;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;


/**
 * Class for the MISSING function that has 1 arbitrary input
 *
 * @author Sabrina Kirstein
 */
public class Missing extends AbstractFunction {

    /**
     * Constructs a MISSING Function with 1 parameter with {@link FunctionDescription}
     */
    public Missing() {
        super("comparison.missing", 1, ValueType.NOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) {

        if (inputEvaluators.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), "1", inputEvaluators.length);
        }

        ExpressionType type = getResultType(inputEvaluators);
        ExpressionEvaluator evaluator = inputEvaluators[0];

        return new SimpleExpressionEvaluator(makeBooleanCallable(evaluator), isResultConstant(inputEvaluators), type);
    }

    /**
     * Builds a boolean callable from a {@link ExpressionEvaluator}, where constant results are
     * evaluated.
     *
     * @param evaluator
     * @return the resulting boolean callable
     */
    protected BooleanSupplier makeBooleanCallable(ExpressionEvaluator evaluator) {
        try {
            // act depending on the type of the given evaluator
            switch (evaluator.getType()) {
                case INTEGER:
                case DOUBLE:
                    final NumericSupplier funcDouble = evaluator.getNumericSupplier();
                    final double valueDouble = evaluator.isConstant() ? funcDouble.getOrThrow() : Double.NaN;
                    if (evaluator.isConstant()) {
                        final Boolean result = compute(valueDouble);
                        return () -> result;
                    } else {
                        return () -> compute(funcDouble.getOrThrow());
                    }
                case NOMINAL:
                    final NominalSupplier funcString = evaluator.getNominalSupplier();
                    final String valueString = evaluator.isConstant() ? funcString.getOrThrow() : null;
                    if (evaluator.isConstant()) {
                        final Boolean result = compute(valueString);
                        return () -> result;
                    } else {
                        return () -> compute(funcString.getOrThrow());
                    }
                case TIMESTAMP:
                    final TimestampSupplier funcDate = evaluator.getTimestampSupplier();
                    final long valueDate = evaluator.isConstant() ? funcDate.getOrThrow() : ValueType.MISSING_TIMESTAMP;
                    if (evaluator.isConstant()) {
                        final Boolean result = compute(valueDate);
                        return () -> result;
                    } else {
                        return () -> compute(funcDate.getOrThrow());
                    }
                case BOOLEAN:
                    final BooleanSupplier funcBoolean = evaluator.getBooleanSupplier();
                    final Boolean valueBoolean = evaluator.isConstant() ? funcBoolean.getOrThrow() : null;
                    if (evaluator.isConstant()) {
                        final Boolean result = compute(valueBoolean);
                        return () -> result;
                    } else {
                        return () -> compute(funcBoolean.getOrThrow());
                    }
                default:
                    return null;
            }

        } catch (ExpressionParsingException e) {
            throw e;
        } catch (Exception e) {
            throw new ExpressionParsingException(e);
        }
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {

        // has to be one argument
        if (inputTypes.length != 1) {
            throw new FunctionInputException("expression_parser.function_wrong_input", getFunctionName(), "1", inputTypes.length);
        }
        // result is always boolean
        return ExpressionType.BOOLEAN;
    }

    /**
     * Computes the result for a double value.
     *
     * @param value
     * @return the result of the computation.
     */
    protected Boolean compute(double value) {
        return Double.isNaN(value);
    }

    /**
     * Computes the result for a timestamp value.
     *
     * @param timestampValue
     * @return the result of the computation.
     */
    protected Boolean compute(long timestampValue) {
        return ValueType.isMissing(timestampValue);
    }

    /**
     * Computes the result for a boolean value.
     *
     * @param value
     * @return the result of the computation.
     */
    protected Boolean compute(Boolean value) {
        return value == null;
    }

    /**
     * Computes the result for a String value.
     *
     * @param value
     * @return the result of the computation.
     */
    protected Boolean compute(String value) {
        return ValueType.isMissing(value);
    }
}
