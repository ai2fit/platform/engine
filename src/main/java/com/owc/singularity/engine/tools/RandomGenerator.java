/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.tools;


import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeBoolean;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeInt;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.pipeline.parameter.conditions.BooleanParameterCondition;
import com.owc.singularity.engine.tools.container.StackingMap;


/**
 * The global random number generator. This should be used for all random purposes of
 * SingularityEngine to ensure that two runs of the same pipeline setup provide the same results.
 * <p>
 * A global random generator is created for each {@link AbstractPipeline} on pipeline start (see
 * {@link AbstractPipeline#prepareRun(int)}) and is available through
 * {@link #getGlobalRandomGenerator()}. Operators can use their own random generator by adding
 * corresponding parameters via {@link #getRandomGeneratorParameters(Operator)}, and the
 * {@link RandomGenerator} can then be accessed by {@link #getRandomGenerator(Operator)}. To keep
 * repeatability and consistency between runs and parallel or sequential (sub)pipeline execution,
 * the {@link RandomGenerator} of a {@link AbstractPipeline} can be {@link #stash(AbstractPipeline)
 * stashed} and {@link #restore(AbstractPipeline) restored}. The general idea is as follows.
 * <table border=1>
 * <tr>
 * <th>Sequential</th>
 * <th>Parallel</th>
 * </tr>
 * <tr>
 * <td>
 * <ol>
 * <li>Get {@link RandomGenerator} by calling stash for the current {@link AbstractPipeline}</li>
 * <li>In each iteration, init the {@link RandomGenerator} with the current {@link AbstractPipeline}
 * and a newly created seed</li>
 * <li>After all iterations are done, restore the {@link RandomGenerator} for the current
 * {@link AbstractPipeline}</li>
 * </ol>
 * </td>
 * <td>Use
 * {@link com.owc.singularity.engine.concurrency.ConcurrentExecutionService#prepareOperatorTask(AbstractPipeline, Operator, int, boolean, Callable)}
 * to take care of the RG assigning.</td>
 * </tr>
 * </table>
 *
 * @author Ralf Klinkenberg, Ingo Mierswa, Jan Czogalla
 */
public class RandomGenerator extends Random {

    private static final long serialVersionUID = 7562534107359981433L;

    public static final String PARAMETER_USE_LOCAL_RANDOM_SEED = "use_local_random_seed";

    public static final String PARAMETER_LOCAL_RANDOM_SEED = "local_random_seed";

    /** The default seed (used by the AbstractRootOperator) */
    public static final int DEFAULT_SEED = 2001;

    /** Class name of the BackgroundExecutionProcess */
    private static final String BACKGROUND_EXECUTION_PROCESS_CLASS_NAME = "com.owc.singularity.extension.concurrency.execution.BackgroundExecutionProcess";

    /** Magic seed of the AbstractRootOperator to use the current system time as seed */
    private static final int USE_SYSTEM_TIME = -1;

    /** Use this alphabet for random String creation. */
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * AbstractPipeline-global random number generator using the random number generator seed
     * specified for the root operator ({@link AbstractRootOperator}). Only present for pipelines
     * that are not background execution pipelines.
     */
    private static final ThreadLocal<RandomGenerator> GLOBAL_RANDOM_GENERATOR = new ThreadLocal<>();

    /**
     * Map of threads to their respective {@link AbstractPipeline Pipelines}. Used to determine the
     * correct random generator.
     */
    private static final ThreadLocal<WeakReference<AbstractPipeline>> THREAD_TO_PROCESS = new ThreadLocal<>();

    /**
     * Map of pipelines to their respective random generators. Mainly used for background execution
     * pipelines to manage parallel executed iterations to provide independent random generators.
     * <strong>Attention: Synchronized get and put methods to prevent race conditions!</strong>
     */
    private static final Map<AbstractPipeline, RandomGenerator> GLOBAL_RANDOM_GENERATOR_MAP = new WeakHashMap<>() {

        @Override
        public synchronized RandomGenerator get(Object key) {
            return super.get(key);
        }

        @Override
        public synchronized RandomGenerator put(AbstractPipeline key, RandomGenerator value) {
            return super.put(key, value);
        }
    };

    static {
        // default RNG in cases where the GUI needs one outside of running a pipeline (e.g. pipeline
        // validation)
        GLOBAL_RANDOM_GENERATOR_MAP.put(null, new RandomGenerator(DEFAULT_SEED));
    }

    private static final StackingMap<AbstractPipeline, RandomGenerator> GLOBAL_STASH_MAP = new StackingMap<AbstractPipeline, RandomGenerator>(
            WeakHashMap.class) {

        @Override
        public synchronized RandomGenerator push(AbstractPipeline key, RandomGenerator item) {
            return super.push(key, item);
        }

        @Override
        public synchronized RandomGenerator pop(Object key) {
            return super.pop(key);
        }

        @Override
        public synchronized RandomGenerator remove(Object key) {
            return super.remove(key);
        }

    };

    /** Initializes the random number generator without a seed. */
    private RandomGenerator() {
        super();
    }

    /** Initializes the random number generator with the given <code>seed</code> */
    public RandomGenerator(long seed) {
        super(seed);
    }

    /** Returns the global random number generator for the given context/thread. */
    public static RandomGenerator getGlobalRandomGenerator() {
        RandomGenerator rg = GLOBAL_RANDOM_GENERATOR.get();
        if (rg != null) {
            return rg;
        }
        WeakReference<AbstractPipeline> wrp = THREAD_TO_PROCESS.get();
        if (wrp != null) {
            AbstractPipeline pipeline = wrp.get();
            if (pipeline != null) {
                RandomGenerator rng = GLOBAL_RANDOM_GENERATOR_MAP.get(pipeline);
                if (rng != null) {
                    return rng;
                }
            }
        }
        return GLOBAL_RANDOM_GENERATOR_MAP.get(null);
    }

    /**
     * Returns the global random number generator if useLocalGenerator is false and a new
     * RandomGenerator with the given seed if the seed is positive or zero. This way is is possible
     * to allow for local random seeds. Operators like learners or validation operators should
     * definitely make use of such a local random generator.
     */
    public static RandomGenerator getRandomGenerator(boolean useLocalGenerator, int localSeed) {
        return useLocalGenerator ? getRandomGenerator(null, localSeed) : getGlobalRandomGenerator();
    }

    /**
     * Returns the global random number generator if the seed is negative and a new RandomGenerator
     * with the given seed if the seed is positive or zero. This way is is possible to allow for
     * local random seeds. Operators like learners or validation operators should definitely make
     * use of such a local random generator.
     *
     * @param pipeline
     *            Used to get the corresponding random generator
     * @param seed
     *            The seed to use in the RandomGenerator
     * @return new RandomGenerator if seed >=0, globalRandomGenerator otherwise
     */
    public static RandomGenerator getRandomGenerator(AbstractPipeline pipeline, int seed) {
        if (seed < 0) {
            if (pipeline == null) {
                return getGlobalRandomGenerator();
            }
            RandomGenerator rg = GLOBAL_RANDOM_GENERATOR_MAP.get(pipeline);
            return rg != null ? rg : GLOBAL_RANDOM_GENERATOR_MAP.get(null);
        } else {
            return new RandomGenerator(seed);
        }
    }

    /**
     * Instantiates the global random number generator for the given pipeline and initializes it
     * with the random number generator seed specified in the <code>global</code> section of the
     * configuration file. Should be invoked before the pipeline starts. If the pipeline is
     * {@code null}, this method does nothing.
     */
    public static void init(AbstractPipeline pipeline) {
        if (pipeline == null) {
            return;
        }
        GLOBAL_STASH_MAP.remove(pipeline);
        init(pipeline, null);
    }

    /**
     * Instantiates the global random number generator for the given pipeline and initializes it
     * with either the given {@code initSeed} if not {@code null} or the seed specified in the
     * pipeline. This allows for better control during parallel execution without touching the
     * actual parameters of the pipeline. If the pipeline is {@code null}, this method does nothing.
     *
     * @param pipeline
     *            the pipeline to create a new {@link RandomGenerator} for. Should not be
     *            {@code null}.
     * @param initSeed
     *            the initial seed for the {@link RandomGenerator}. Can be {@code null}.
     */
    public static void init(AbstractPipeline pipeline, Long initSeed) {
        if (pipeline == null) {
            return;
        }
        long seed = DEFAULT_SEED;
        if (initSeed != null) {
            seed = initSeed;
        } else {
            try {
                seed = pipeline.getRootOperator().getParameterAsInt(AbstractRootOperator.PARAMETER_RANDOM_SEED);
            } catch (UndefinedParameterError e) {
                // tries to read the general random seed
                // if no seed was specified (cannot happen) use default seed
            }
        }
        RandomGenerator rg;
        if (seed == USE_SYSTEM_TIME) {
            rg = new RandomGenerator();
        } else {
            rg = new RandomGenerator(seed);
        }
        setRandomGenerator(pipeline, rg);
    }

    /**
     * Stash the current {@link RandomGenerator} of the given {@link AbstractPipeline} to
     * {@link #restore(AbstractPipeline) restore} it later. Also returns the current generator for
     * the given pipeline using {@link #getRandomGenerator(AbstractPipeline, int)
     * getRandomGenerator(pipeline, -1}. Will stash nothing and return {@code null} if pipeline is
     * {@code null}.
     *
     * @param pipeline
     *            the pipeline to stash the {@link RandomGenerator} for
     * @return {@code null} if pipeline is {@code null}, non-{@code null} {@link RandomGenerator}
     *         otherwise.
     */
    public static RandomGenerator stash(AbstractPipeline pipeline) {
        if (pipeline == null) {
            return null;
        }
        RandomGenerator rg = getRandomGenerator(pipeline, -1);
        GLOBAL_STASH_MAP.push(pipeline, rg);
        return rg;
    }

    /**
     * Restores a {@link RandomGenerator} for the given {@link AbstractPipeline}, if it was
     * {@link #stash(AbstractPipeline) stashed} before. Will do nothing if no generator was stashed
     * or pipeline is {@code null}.
     *
     * @param pipeline
     *            the pipeline to restore the {@link RandomGenerator} for
     */
    public static void restore(AbstractPipeline pipeline) {
        RandomGenerator rg = GLOBAL_STASH_MAP.pop(pipeline);
        if (rg == null) {
            return;
        }
        setRandomGenerator(pipeline, rg);
    }

    /**
     * Associates the specified {@link RandomGenerator} with the given {@link AbstractPipeline}.
     * Also associates the pipeline with the current thread if it is executed in the background. The
     * parameter rg must not be null. Will do nothing if pipeline is {@code null}.
     *
     * @param pipeline
     *            the {@link AbstractPipeline} to associate the {@link RandomGenerator} with
     * @param rg
     *            the {@link RandomGenerator} to associate to the {@link AbstractPipeline}
     */
    private static void setRandomGenerator(AbstractPipeline pipeline, RandomGenerator rg) {
        if (pipeline == null) {
            return;
        }
        GLOBAL_RANDOM_GENERATOR_MAP.put(pipeline, rg);

        // don't have access to the class here, so reference by qualified name
        if (pipeline.getClass().getName().equals(BACKGROUND_EXECUTION_PROCESS_CLASS_NAME)) {
            // store pipeline for this thread for BEPs
            THREAD_TO_PROCESS.set(new WeakReference<>(pipeline));
        } else {
            // only the pipeline currently running in foreground on this thread can influence the
            // global random generator
            GLOBAL_RANDOM_GENERATOR.set(rg);
        }
    }

    /**
     * This method returns a list of parameters usable to conveniently provide parameters for random
     * generator use within operators
     *
     * @param operator
     *            the operator
     */
    public static List<ParameterType> getRandomGeneratorParameters(Operator operator) {
        List<ParameterType> types = new LinkedList<>();

        types.add(new ParameterTypeBoolean(PARAMETER_USE_LOCAL_RANDOM_SEED, "Indicates if a local random seed should be used.", false));

        ParameterType type = new ParameterTypeInt(PARAMETER_LOCAL_RANDOM_SEED, "Specifies the local random seed", 1, Integer.MAX_VALUE, 1992);
        type.registerDependencyCondition(new BooleanParameterCondition(operator, PARAMETER_USE_LOCAL_RANDOM_SEED, false, true));
        types.add(type);

        return types;
    }

    /**
     * This method returns the appropriate RandomGenerator for the user chosen parameter
     * combination. If the operator does not use a local random generator, use the appropriate
     * pipeline related random generator.
     *
     * @param operator
     * @throws UndefinedParameterError
     */
    public static RandomGenerator getRandomGenerator(Operator operator) throws UndefinedParameterError {
        if (operator.getParameterAsBoolean(PARAMETER_USE_LOCAL_RANDOM_SEED)) {
            return new RandomGenerator(operator.getParameterAsInt(PARAMETER_LOCAL_RANDOM_SEED));
        } else {
            return getRandomGenerator(operator.getPipeline(), -1);
        }
    }

    // ================================================================================
    /**
     * Returns the next pseudorandom, uniformly distributed <code>double</code> value between
     * <code>lowerBound</code> and <code>upperBound</code> from this random number generator's
     * sequence (exclusive of the interval endpoint values).
     *
     * @throws IllegalArgumentException
     *             if upperBound < lowerBound
     */
    public double nextDoubleInRange(double lowerBound, double upperBound) throws IllegalArgumentException {
        if (upperBound < lowerBound) {
            throw new IllegalArgumentException(
                    "RandomGenerator.nextDoubleInRange : the upper bound of the " + "random number range should be greater than the lower bound.");
        }
        return nextDouble() * (upperBound - lowerBound) + lowerBound;
    }

    /**
     * returns the next pseudorandom, uniformly distributed <code>long</code> value between
     * <code>lowerBound</code> and <code>upperBound</code> from this random number generator's
     * sequence (exclusive of the interval endpoint values).
     */
    public long nextLongInRange(long lowerBound, long upperBound) {
        if (upperBound <= lowerBound) {
            throw new IllegalArgumentException(
                    "RandomGenerator.nextLongInRange : the upper bound of the " + "random number range should be greater than the lower bound.");
        }
        return (long) (nextDouble() * (upperBound - lowerBound + 1)) + lowerBound;
    }

    /**
     * Returns the next pseudorandom, uniformly distributed <code>int</code> value between
     * <code>lowerBound</code> and <code>upperBound</code> from this random number generator's
     * sequence (lower bound inclusive, upper bound exclusive).
     */
    public int nextIntInRange(int lowerBound, int upperBound) {
        if (upperBound <= lowerBound) {
            throw new IllegalArgumentException(
                    "RandomGenerator.nextIntInRange : the upper bound of the " + "random number range should be greater than the lower bound.");
        }
        return nextInt(upperBound - lowerBound) + lowerBound;
    }

    /** Returns a random String of the given length. */
    public String nextString(int length) {
        char[] chars = new char[length];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = ALPHABET.charAt(nextInt(ALPHABET.length()));
        }
        return new String(chars);
    }

    /**
     * Returns a randomly selected integer between 0 and the length of the given array. Uses the
     * given probabilities to determine the index, all values in this array must sum up to 1.
     */
    public int randomIndex(double[] probs) {
        double r = nextDouble();
        double sum = 0.0d;
        for (int i = 0; i < probs.length; i++) {
            sum += probs[i];
            if (r < sum) {
                return i;
            }
        }
        return probs.length - 1;
    }

    /**
     * This method returns a randomly filled array of given length
     *
     * @param length
     *            the length of the returned array
     * @return the filled array
     */
    public double[] nextDoubleArray(int length) {
        double[] values = new double[length];
        for (int i = 0; i < length; i++) {
            values[i] = nextDouble();
        }
        return values;
    }

    /** Returns a random date between the given ones. */
    public Date nextDateInRange(Date start, Date end) {
        return new Date(nextLongInRange(start.getTime(), end.getTime()));
    }

    /** Returns a set of integer within the given range and given size */
    public Set<Integer> nextIntSetWithRange(int lowerBound, int upperBound, int size) {
        if (upperBound <= lowerBound) {
            throw new IllegalArgumentException(
                    "RandomGenerator.nextIntInRange : the upper bound of the " + "random number range should be greater than the lower bound.");
        }
        if (upperBound - lowerBound < size) {
            throw new IllegalArgumentException("RandomGenerator.nextIntInRange : impossible to deliver the desired set of integeres --> range is too small.");
        }
        Set<Integer> set = new HashSet<>();
        while (set.size() < size) {
            set.add(nextIntInRange(lowerBound, upperBound));
        }
        return set;
    }
}
