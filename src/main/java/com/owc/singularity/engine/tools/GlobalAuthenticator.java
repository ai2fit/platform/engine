/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools;


import java.net.*;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Global authenticator at which multiple other authenticators can register. Authentication requests
 * will be delegated subsequently until an authenticator is found.
 *
 * @author Simon Fischer, Hatem Hamad
 *
 */
public class GlobalAuthenticator extends Authenticator {

    private static GlobalAuthenticator INSTANCE;

    public static GlobalAuthenticator getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GlobalAuthenticator();
        }
        return INSTANCE;
    }

    private final List<URLAuthenticator> serverAuthenticators;
    private final List<URLAuthenticator> proxyAuthenticators;

    private GlobalAuthenticator() {
        serverAuthenticators = new LinkedList<>();
        proxyAuthenticators = new LinkedList<>();
    }

    /**
     * This method adds another Authenticator to the GlobalAuthenticator that will be enqueued in
     * the list of Authenticators that are tried for URLs that need authentication.
     */
    public synchronized void registerServerAuthenticator(URLAuthenticator authenticator) {
        serverAuthenticators.add(0, authenticator);
    }

    /**
     * This method adds another Authenticator to the GlobalAuthenticator that will be enqueued in
     * the list of Authenticators that are tried for Proxy requests for authentication.
     */
    public synchronized void registerProxyAuthenticator(URLAuthenticator authenticator) {
        proxyAuthenticators.add(0, authenticator);
    }

    @Override
    protected synchronized PasswordAuthentication getPasswordAuthentication() {
        URL url = getRequestingURL();
        RequestorType requestorType = getRequestorType();
        List<URLAuthenticator> authenticators;
        final String logKey;
        switch (requestorType) {
            case PROXY:
                logKey = "com.owc.singularity.engine.tools.GlobalAuthenticator.authentication_requested_proxy";
                authenticators = proxyAuthenticators;
                break;
            case SERVER:
                logKey = "com.owc.singularity.engine.tools.GlobalAuthenticator.authentication_requested";
                authenticators = serverAuthenticators;
                break;
            default:
                return null;
        }
        LogService.getRoot().debug(logKey, new Object[] { url, authenticators });
        for (URLAuthenticator a : authenticators) {
            PasswordAuthentication auth = a.getAuthentication(url);
            if (auth != null) {
                LogService.getRoot().trace("Authenticator {} has authenticated url {} with {}:{}", a, url, auth.getUserName(), auth.getPassword());
                return auth;
            }
        }
        return null;
    }

    public static void initialize() {
        Authenticator.setDefault(getInstance());
    }

    public interface URLAuthenticator {

        /**
         * This method returns the PasswordAuthentication if this Authenticator is registered for
         * the given URL. Otherwise, null can be returned.
         */
        PasswordAuthentication getAuthentication(URL url);
    }

}
