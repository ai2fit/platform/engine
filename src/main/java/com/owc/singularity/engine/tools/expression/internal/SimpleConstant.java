/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.Constant;
import com.owc.singularity.engine.tools.expression.ExpressionType;


/**
 * A {@link Constant} that supplies constructors for all admissible combinations of its fields.
 *
 * @author Gisa Schaefer
 *
 */
public class SimpleConstant implements Constant {

    private final ExpressionType type;
    private final String name;
    private final String stringValue;
    private final double doubleValue;
    private final boolean booleanValue;
    private final long timestampValue;
    private String annotation;
    private boolean invisible = false;

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param stringValue
     *            the string value
     */
    public SimpleConstant(String name, String stringValue) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }
        this.type = ExpressionType.NOMINAL;
        this.name = name;
        this.stringValue = stringValue;
        this.doubleValue = ValueType.MISSING_NUMERIC;
        this.booleanValue = false;
        this.timestampValue = ValueType.MISSING_TIMESTAMP;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param stringValue
     *            the string value
     * @param annotation
     *            an optional annotation
     */
    public SimpleConstant(String name, String stringValue, String annotation) {
        this(name, stringValue);
        this.annotation = annotation;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param stringValue
     *            the string value
     * @param annotation
     *            an optional annotation
     * @param invisible
     *            option to hide a constant in the UI but recognize it in the parser
     */
    public SimpleConstant(String name, String stringValue, String annotation, boolean invisible) {
        this(name, stringValue, annotation);
        this.invisible = invisible;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param doubleValue
     *            the double value
     */
    public SimpleConstant(String name, double doubleValue) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }
        this.type = ExpressionType.DOUBLE;
        this.name = name;
        this.stringValue = null;
        this.doubleValue = doubleValue;
        this.booleanValue = false;
        this.timestampValue = ValueType.MISSING_TIMESTAMP;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param doubleValue
     *            the double value
     * @param annotation
     *            an optional annotation
     */
    public SimpleConstant(String name, double doubleValue, String annotation) {
        this(name, doubleValue);
        this.annotation = annotation;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param doubleValue
     *            the double value
     * @param annotation
     *            an optional annotation
     * @param invisible
     *            option to hide a constant in the UI but recognize it in the parser
     */
    public SimpleConstant(String name, double doubleValue, String annotation, boolean invisible) {
        this(name, doubleValue, annotation);
        this.invisible = invisible;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param booleanValue
     *            the boolean value
     */
    public SimpleConstant(String name, boolean booleanValue) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }
        this.type = ExpressionType.BOOLEAN;
        this.name = name;
        this.stringValue = null;
        this.doubleValue = 0;
        this.booleanValue = booleanValue;
        this.timestampValue = ValueType.MISSING_TIMESTAMP;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param booleanValue
     *            the boolean value
     * @param annotation
     *            an optional annotation
     */
    public SimpleConstant(String name, boolean booleanValue, String annotation) {
        this(name, booleanValue);
        this.annotation = annotation;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param booleanValue
     *            the boolean value
     * @param annotation
     *            an optional annotation
     * @param invisible
     *            option to hide a constant in the UI but recognize it in the parser
     */
    public SimpleConstant(String name, boolean booleanValue, String annotation, boolean invisible) {
        this(name, booleanValue, annotation);
        this.invisible = invisible;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param timestampValue
     *            the date value
     */
    public SimpleConstant(String name, long timestampValue) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }
        this.type = ExpressionType.TIMESTAMP;
        this.name = name;
        this.stringValue = null;
        this.doubleValue = 0;
        this.booleanValue = false;
        this.timestampValue = timestampValue;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param timestampValue
     *            the date value
     * @param annotation
     *            an optional annotation
     */
    public SimpleConstant(String name, long timestampValue, String annotation) {
        this(name, timestampValue);
        this.annotation = annotation;
    }

    /**
     * Creates a Constant with the given characteristics.
     *
     * @param name
     *            the name of the constant, cannot be {@code null}
     * @param timestampValue
     *            the date value
     * @param annotation
     *            an optional annotation
     * @param invisible
     *            option to hide a constant in the UI but recognize it in the parser
     */
    public SimpleConstant(String name, long timestampValue, String annotation, boolean invisible) {
        this(name, timestampValue, annotation);
        this.invisible = invisible;
    }

    @Override
    public ExpressionType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStringValue() {
        if (type == ExpressionType.NOMINAL) {
            return stringValue;
        } else {
            throw new IllegalStateException("element is not of type String");
        }
    }

    @Override
    public double getNumericValue() {
        if (type == ExpressionType.DOUBLE) {
            return doubleValue;
        } else {
            throw new IllegalStateException("element is not of type double");
        }
    }

    @Override
    public boolean getBooleanValue() {
        if (type == ExpressionType.BOOLEAN) {
            return booleanValue;
        } else {
            throw new IllegalStateException("element is not of type boolean");
        }
    }

    @Override
    public long getTimestampValue() {
        if (type == ExpressionType.TIMESTAMP) {
            return timestampValue;
        } else {
            throw new IllegalStateException("element is not of type Date");
        }
    }

    @Override
    public String getAnnotation() {
        return annotation;
    }

    /**
     * Sets the annotation of this constant.
     *
     * @param annotation
     *            the annotation to set
     */
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @Override
    public boolean isInvisible() {
        return invisible;
    }

}
