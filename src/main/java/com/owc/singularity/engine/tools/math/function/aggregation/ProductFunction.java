/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.math.function.aggregation;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * This aggregation function calculates the product of all given values.
 * 
 * @author Sebastian Land
 * 
 */
public class ProductFunction extends AbstractAggregationFunction {

    private String PRODUCT_AGGREGATION_FUNCTION_NAME = "product";

    private double product = 1d;

    public ProductFunction() {
        this(DEFAULT_IGNORE_MISSINGS);
    }

    public ProductFunction(Boolean ignoreMissing) {
        super(ignoreMissing);
    }

    @Override
    public void reset() {
        foundMissing = false;
        product = 1d;
    }

    @Override
    public String getName() {
        return PRODUCT_AGGREGATION_FUNCTION_NAME;
    }

    @Override
    public double aggregateNumericValue() {
        return product;
    }

    @Override
    public void update(double value) {
        if (ValueType.isMissing(value)) {
            foundMissing = true;
            return;
        }
        product *= value;
    }

    @Override
    public void update(long value) {
        if (!ValueType.isMissing(value)) {
            product *= value;
        }
    }

    @Override
    public long aggregateTimestampValue() throws UnsupportedOperationException {
        return (long) product;
    }

    @Override
    public boolean supportsValueType(ValueType valueType) {
        return !ValueType.NOMINAL.equals(valueType);
    }
}
