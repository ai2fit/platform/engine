/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.logging;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.message.LocalizedMessageFactory;
import org.apache.logging.log4j.message.MessageFactory;

import com.owc.singularity.engine.tools.I18N;

public class LogService {

    // -------------------- Verbosity Level --------------------

    /** Indicates an unknown verbosity level. */
    public static final Level LEVEL_UNKNOWN = Level.forName("UNKNOWN", 1);

    /**
     * Indicates log messages concerning in- and output. Should only be used by the class Operator
     * itself and not by its subclasses.
     */
    public static final Level LEVEL_IO = Level.forName("IO", 550);

    /**
     * human-readable names of the available log levels
     */
    public static final String[] SELECTABLE_LEVEL_NAMES = Arrays.stream(Level.values())
            .sorted(Comparator.comparingInt(Level::intLevel))
            .map(Level::name)
            .toArray(String[]::new);
    /**
     * index of the default log level (INFO)
     */
    public static final int DEFAULT_LEVEL_INDEX = List.of(SELECTABLE_LEVEL_NAMES).indexOf(Level.INFO.name());

    /**
     * A log marker that indicates a log message is only useful when in pipeline design mode
     */
    public static final Marker MARKER_DESIGN_MODE = MarkerManager.getMarker("DESIGN");

    /**
     * A log marker that indicates a log message is only useful during development
     */
    public static final Marker MARKER_DEVELOPMENT = MarkerManager.getMarker("DEVELOPMENT");

    private static Logger GLOBAL_LOGGER;
    private static MessageFactory I18N_MESSAGE_FACTORY;

    private LogService() {
        // utility class
    }

    public static Logger getRoot() {
        if (GLOBAL_LOGGER == null) {
            GLOBAL_LOGGER = getI18NLogger("com.owc.singularity");
        }
        return GLOBAL_LOGGER;
    }

    public static Logger getI18NLogger(Class<?> clazz) {
        return LogManager.getLogger(clazz, getI18nMessageFactory());
    }

    public static Logger getI18NLogger(String name) {
        return LogManager.getLogger(name, getI18nMessageFactory());
    }

    public static void setLoggerLevel(Logger logger, Level level) {
        Configurator.setLevel(logger.getName(), level);
    }

    public static void setRootLevel(Level level) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), level);
    }

    public static Level getRootLevel() {
        return LogManager.getRootLogger().getLevel();
    }

    private static MessageFactory getI18nMessageFactory() {
        if (I18N_MESSAGE_FACTORY == null) {
            I18N_MESSAGE_FACTORY = new LocalizedMessageFactory(I18N.getProvider().getLogMessagesBundle());
        }
        return I18N_MESSAGE_FACTORY;
    }
}
