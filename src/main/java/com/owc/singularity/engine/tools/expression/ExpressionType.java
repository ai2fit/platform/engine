/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression;


import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Enum for the type of an {@link Expression}. Knows the {@link ValueType} associated to an
 * ExpressionType and the other way around.
 *
 * @author Gisa Schaefer
 * @since 6.5.0
 */
public enum ExpressionType {

    NOMINAL(ValueType.NOMINAL), DOUBLE(ValueType.NUMERIC), INTEGER(ValueType.NUMERIC), BOOLEAN(ValueType.NOMINAL), TIMESTAMP(ValueType.TIMESTAMP);

    private final ValueType attributeType;

    ExpressionType(ValueType attributeType) {
        this.attributeType = attributeType;
    }

    /**
     * @return the associated {@link ValueType}
     */
    public ValueType getAttributeType() {
        return attributeType;
    }

    /**
     * Returns the {@link ExpressionType} associated to the attributeType.
     *
     * @param attributeType
     *            the {@link ValueType}
     * @return the expression type associated to the attributeType
     */
    public static ExpressionType getExpressionType(ValueType attributeType) {
        switch (attributeType) {
            case TIMESTAMP:
                return TIMESTAMP;
            case NUMERIC:
                return DOUBLE;
            default:
                return NOMINAL;
        }
    }
}
