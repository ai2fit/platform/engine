/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.process;


import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.VariableHandler;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractArbitraryStringInputStringOutputFunction;


/**
 * A {@link Function} that looks for a variable value and can deliver a default value if the
 * variable does not exist.
 *
 * @author Gisa Schaefer
 *
 */
public class VariableValue extends AbstractArbitraryStringInputStringOutputFunction {

    private final VariableHandler handler;

    /**
     * Creates a function that can look up a variable value.
     *
     * @param process
     *            the process with the {@link VariableHandler} that should be used for finding the
     *            variable
     */
    public VariableValue(AbstractPipeline process) {
        super("process.variable", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS);
        handler = process.getVariableHandler();
    }

    /**
     * Creates a function that can look up a variable value.
     *
     * @param handler
     *            the {@link VariableHandler} that should be used for finding the variable
     */
    public VariableValue(VariableHandler handler) {
        super("process.variable", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS);
        this.handler = handler;
    }

    @Override
    protected void checkNumberOfInputs(int length) {
        if (length != 1 && length != 2) {
            throw new FunctionInputException("expression_parser.function_wrong_input_two", getFunctionName(), 1, 2, length);
        }
    }

    @Override
    protected String compute(String... values) {
        String variable = handler.getVariable(values[0]);
        if (values.length == 1) {
            if (variable == null) {
                throw new FunctionInputException("expression_parser.unknown_variable", values[0]);
            }
            return variable;
        } else {
            if (variable == null) {
                return values[1];
            } else {
                return variable;
            }
        }
    }

}
