/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.tools.expression.internal.function.mathematical;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionInputException;
import com.owc.singularity.engine.tools.expression.internal.function.Abstract1DoubleInputFunction;


/**
 * A {@link Function} computing the absolute value of a number.
 *
 * @author Marcel Seifert
 *
 */
public class AbsoluteValue extends Abstract1DoubleInputFunction {

    public AbsoluteValue() {
        super("mathematical.abs", ValueType.NUMERIC);
    }

    @Override
    protected double compute(double value) {
        return Math.abs(value);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        ExpressionType input = inputTypes[0];
        if (input == ExpressionType.DOUBLE) {
            return ExpressionType.DOUBLE;
        } else if (input == ExpressionType.INTEGER) {
            return ExpressionType.INTEGER;
        } else {
            throw new FunctionInputException("expression_parser.function_wrong_type", getFunctionName(), "numerical");
        }
    }
}
