package com.owc.singularity.engine;

/**
 * Represents any provider that is responsible for loading, creating, manipulation of resources.
 *
 * @author Hatem Hamad
 */
public interface ResourcesProvider {

    boolean isInitialized();

    void initialize();

    default void freeResources() {

    }
}
