/*

 *

 *

 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see http://www.gnu.org/licenses/.
 */

package com.owc.singularity.engine.event;


import com.owc.singularity.engine.pipeline.AbstractPipeline;

/**
 * @author Hatem Hamad
 */
public interface PipelineLifeCycleEventListener extends EventListener {

    default void onBeforePipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

    default void onAfterPipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

    /**
     * Called before the pipeline is paused with the ability to deny pausing.
     * 
     * @param pipeline
     *            the current pipeline
     * @param details
     *            event details
     * @return {@code false} indicates that the pipeline should not be paused, otherwise
     *         {@code true}
     */
    default boolean onBeforePipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        return true;
    }

    default void onAfterPipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

    /**
     * Called before the pipeline is resumed after it has been paused with the ability to deny
     * resuming.
     * 
     * @param pipeline
     *            the current pipeline
     * @param details
     *            event details
     * @return {@code false} indicates that the pipeline should not be resumed, otherwise
     *         {@code true}
     */
    default boolean onBeforePipelineResume(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        return true;
    }

    default void onAfterPipelineResume(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

    /**
     * Called before the pipeline is stopped with the ability to deny stopping.
     * 
     * @param pipeline
     *            the current pipeline
     * @param details
     *            event details
     * @return {@code false} indicates that the pipeline should not be stopped, otherwise
     *         {@code true}
     */
    default boolean onBeforePipelineStop(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        return true;
    }

    default void onAfterPipelineStop(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

    default void onAfterPipelineFinish(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {}

}
