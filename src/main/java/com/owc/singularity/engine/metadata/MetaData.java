/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.PipelineMetaData;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Metadata about an {@link IOObject}. Includes the specific class of the IOObject plus a map of
 * key-value pairs specifying more detailed properties. Additionally, may contain information about
 * which {@link OutputPort} originally generated this metadata. <br/>
 * 
 * Subclasses representing the metadata for a class T (in particular those defined by plugins),
 * should implement a constructor accepting a T and a boolean and be registered with
 * {@link MetaDataFactory#registerIOObjectMetaData(Class, Class)}.
 * 
 * @author Simon Fischer
 */
public class MetaData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Class<? extends IOObject> dataClass;

    public MetaData() {
        this(IOObject.class);
    }

    public MetaData(Class<? extends IOObject> dataClass) {
        this.dataClass = dataClass;
    }


    public Class<? extends IOObject> getObjectClass() {
        return dataClass;
    }

    @Override
    public MetaData clone() {
        MetaData clone;
        try {
            Constructor<? extends MetaData> declaredConstructor = this.getClass().getDeclaredConstructor();
            declaredConstructor.trySetAccessible();
            clone = declaredConstructor.newInstance();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            LogService.getRoot().error(e.getMessage());
            throw new RuntimeException("Cannot clone " + this, e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot clone " + this, e);
        }
        clone.dataClass = this.getObjectClass();

        return clone;
    }

    @Override
    public String toString() {
        return getObjectClass().getSimpleName();
    }

    public String getDescription() {
        return dataClass.getSimpleName();
    }

    /**
     * Returns true if isData is compatible with this metadata, where <code>this</code> represents
     * desired metadata and isData represents metadata that was actually delivered.
     */
    public boolean isCompatible(MetaData isData) {
        return this.dataClass.isAssignableFrom(isData.dataClass);
    }

    /**
     * This will return the metadata description of the given IOObject. If the shortened flag is
     * true, the metadata will be incomplete to avoid to generate too much data if this is supported
     * by the actual metadata implementation.
     */
    public static MetaData forIOObject(IOObject ioo) {
        return MetaDataFactory.getInstance().createMetaDataForIOObject(ioo);
    }

    public static PipelineMetaData forObject(AbstractPipeline process) {
        return new PipelineMetaData(process);
    }
}
