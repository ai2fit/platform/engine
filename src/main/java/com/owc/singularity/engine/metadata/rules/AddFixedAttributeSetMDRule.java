/*
 *  SingularityEngine
 *
 *  Copyright (C) 2001-2013 by Rapid-I and the contributors
 *
 *  Complete list of developers available at our web site:
 *
 *       http://rapid-i.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.metadata.rules;


import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.SimpleMetaDataError;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;

/**
 * This rule is for operators, which add a fixed set of attributes to an example set
 *
 * @author Sebastian Land
 */
public class AddFixedAttributeSetMDRule implements MDTransformationRule {

    private final List<Supplier<AttributeMetaData>> addedAttributeSuppliers = new LinkedList<>();
    private final InputPort inputPort;
    private final OutputPort outputPort;

    public AddFixedAttributeSetMDRule(InputPort inputPort, OutputPort outputPort, AttributeMetaData[] attributeMetadata) {
        this(inputPort, outputPort, attributeMetadata, null);
    }

    public AddFixedAttributeSetMDRule(InputPort inputPort, OutputPort outputPort, AttributeMetaData[] attributeMetadata,
            List<Supplier<AttributeMetaData>> suppliers) {
        for (AttributeMetaData amd : attributeMetadata) {
            addedAttributeSuppliers.add(new Supplier<AttributeMetaData>() {

                @Override
                public AttributeMetaData get() {
                    return amd;
                }
            });
        }
        if (suppliers != null) {
            addedAttributeSuppliers.addAll(suppliers);
        }
        this.inputPort = inputPort;
        this.outputPort = outputPort;
    }

    public AddFixedAttributeSetMDRule(InputPort inputPort, OutputPort outputPort, List<Supplier<AttributeMetaData>> supplier) {
        this(inputPort, outputPort, new AttributeMetaData[0], supplier);
    }

    @Override
    public void transformMD() {
        ExampleSetMetaData exampleSetMetaData = null;
        try {
            exampleSetMetaData = inputPort.getMetaData(ExampleSetMetaData.class);
        } catch (ClassCastException e) {
            inputPort.addError(new SimpleMetaDataError(Severity.ERROR, inputPort, "wrong_data_type"));
        }
        if (exampleSetMetaData == null) {
            exampleSetMetaData = ExampleSetMetaData.emptyMetaData();
        }
        ExampleSetMetaData.ExampleSetMetaDataTransformer transformer = exampleSetMetaData.transform();
        for (Supplier<? extends AttributeMetaData> supplier : addedAttributeSuppliers) {
            AttributeMetaData attributeMetaData = supplier.get();
            transformer.withDerivedAttribute(attributeMetaData.getName(), attributeMetaData.getValueType());
        }
        outputPort.deliverMD(modifyMetaData(transformer.transform()));
    }

    /**
     * Modifies the standard metadata before it is passed to the output. Can be used if the
     * transformation depends on parameters etc. The default implementation just returns the
     * original. Subclasses may safely modify the metadata, since a copy is used for this method.
     */
    public MetaData modifyMetaData(ExampleSetMetaData unmodifiedMetaData) {
        return unmodifiedMetaData;
    }

}
