/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import com.owc.singularity.engine.object.Model;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetHeader;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetHeader.TrainingSetRelation;
import com.owc.singularity.engine.operator.error.UserError;


/**
 * This class holds the metadata information for all models. It is the parent class of each model
 * specific implementation, which should be able to simulate all changes on the data of a model
 * application during metadata transformation. This super class remembers the metadata of the
 * trainings set and already checks compatibility of the application metadata with the
 * training-metadata.
 * 
 * @author Simon Fischer, Sebastian Land
 */
public abstract class ModelMetaData extends MetaData {

    private static final long serialVersionUID = 1L;

    protected ExampleSetHeader trainingSetHeader;
    protected TrainingSetRelation setRelation;

    public ModelMetaData(Class<? extends Model> mclass, ExampleSetHeader trainingSetHeader, TrainingSetRelation setRelation) {
        super(mclass);
        this.trainingSetHeader = trainingSetHeader;
        this.setRelation = setRelation;
    }

    public ModelMetaData(Model model) {
        super(model.getClass());
        this.trainingSetHeader = model.getTrainingHeader();
    }

    /**
     * This method simulates the application of a model. First the compatibility of the model with
     * the current example set is checked and then the effects are applied.
     */
    public final ExampleSetMetaData applyAndCheck(ExampleSetMetaData emd) {
        try {
            checkCompatibility(emd);
        } catch (UserError e) {
            return ExampleSetMetaData.emptyMetaData();
        }
        return apply(emd);
    }

    protected void checkCompatibility(ExampleSetMetaData emd) throws UserError {
        trainingSetHeader.checkMatching(new ExampleSetHeader(emd), setRelation);
        // TODO: We need an error registration mechanism
    }

    /**
     * This method must be implemented by subclasses in order to apply any changes on the metadata,
     * that would occur on application of the real model.
     */
    protected abstract ExampleSetMetaData apply(ExampleSetMetaData emd);

    @Override
    public ModelMetaData clone() {
        return this;
    }

    public ExampleSetHeader getTrainingSetHeader() {
        return trainingSetHeader;
    }

    public TrainingSetRelation getTrainingSetRelation() {
        return setRelation;
    }

}
