/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.preconditions;

import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;

/**
 * This precondition checks whether the delivered object is of a given type or a collection of the
 * given type (or a collection of such collections etc.).
 * 
 * @author Simon Fischer
 * 
 */
public class CollectionPrecondition implements Precondition {

    private final Precondition nestedPrecondition;

    public CollectionPrecondition(Precondition precondition) {
        this.nestedPrecondition = precondition;
    }

    @Override
    public void check(OperationDescriptionContext context, MetaData md) {
        if (md != null) {
            if (md instanceof CollectionMetaData) {
                check(context, ((CollectionMetaData) md).getElementMetaData());
                return;
            }
        }
        nestedPrecondition.check(context, md);
    }

    @Override
    public String getDescription() {
        return nestedPrecondition.getDescription() + " or collection of same type";
    }

    @Override
    public boolean isCompatible(MetaData input) {
        if (input instanceof CollectionMetaData) {
            return isCompatible(((CollectionMetaData) input).getElementMetaData());
        } else {
            return nestedPrecondition.isCompatible(input);
        }
    }

    @Override
    public MetaData getExpectedMetaData() {
        return new CollectionMetaData(nestedPrecondition.getExpectedMetaData());
    }

}
