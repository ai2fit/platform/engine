/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.metadata;


import java.lang.reflect.Constructor;
import java.util.LinkedHashMap;
import java.util.Map;

import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.IOObjectCollection;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.tools.DominatingClassFinder;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Factory class for creating {@link MetaData} objects for given {@link IOObject}s.
 * <p>
 * See {@link #registerIOObjectMetaData(Class, Class)} and
 * {@link #createMetaDataForIOObject(IOObject)} for a description.
 *
 * @author Nils Woehler
 *
 */
public class MetaDataFactory {

    private static final MetaDataFactory INSTANCE = new MetaDataFactory();

    public static MetaDataFactory getInstance() {
        return INSTANCE;
    }

    private MetaDataFactory() {
        // private constructor for singleton
    }

    private static final Map<Class<? extends IOObject>, Class<? extends MetaData>> ioObjectToMetaDataClass = new LinkedHashMap<Class<? extends IOObject>, Class<? extends MetaData>>();

    static {
        // register pre-defined metadata classes
        MetaDataFactory.registerIOObjectMetaData(ExampleSet.class, ExampleSetMetaData.class);
        MetaDataFactory.registerIOObjectMetaData(IOObjectCollection.class, CollectionMetaData.class);
        MetaDataFactory.registerIOObjectMetaData(ConnectionParametersIOObject.class, ConnectionParametersMetadata.class);
    }

    /**
     * Register metadata classes that will be instantiated when metadata for IO Object should be
     * created. The metadata class must have a constructor with parameters the registered IO Object
     * as first parameter and as second a boolean 'shortened' which defines whether the metadata
     * should be complete or whether less relevant facts can be omitted (in order not to clutter
     * their visual representation).
     */
    public static void registerIOObjectMetaData(Class<? extends IOObject> ioObjectClass, Class<? extends MetaData> metaDataClass) {
        try {
            metaDataClass.getConstructor(ioObjectClass);
            ioObjectToMetaDataClass.put(ioObjectClass, metaDataClass);
        } catch (Throwable e) {
            throw new IllegalArgumentException("Could not register meta data class " + metaDataClass + " for IO object " + ioObjectClass
                    + ". Check if MetaData has a constructor that accepts as single argument the provided IO object.", e);
        }
    }

    public static Class<? extends MetaData> getMetaDataClassFor(Class<? extends IOObject> ioObjectClass) {
        Class<? extends MetaData> metaDataClass = ioObjectToMetaDataClass.get(ioObjectClass);
        if (metaDataClass == null) {
            // look for next registered dominating IOObject class
            ioObjectClass = new DominatingClassFinder<IOObject>().findNextDominatingClass(ioObjectClass, ioObjectToMetaDataClass.keySet());
            // registered dominating IOObject class found
            if (ioObjectClass != null) {
                metaDataClass = ioObjectToMetaDataClass.get(ioObjectClass);
            }
        }
        return metaDataClass;
    }

    public MetaData createMetaDataForIOObject(IOObject ioo) {
        MetaData result;

        Class<? extends IOObject> ioObjectClass = ioo.getClass();
        Class<? extends MetaData> metaDataClass = getMetaDataClassFor(ioObjectClass);

        result = instantiateMetaData(metaDataClass, ioObjectClass, ioo);
        return result;
    }

    private MetaData instantiateMetaData(Class<? extends MetaData> metaDataClass, Class<? extends IOObject> compatibleIOObjectClass, IOObject ioo) {
        if (metaDataClass != null) {
            try {
                for (Constructor<?> constructor : metaDataClass.getConstructors()) {
                    if (constructor.getParameterCount() == 1) {
                        if (constructor.getParameterTypes()[0].isAssignableFrom(ioo.getClass())) {
                            return (MetaData) constructor.newInstance(ioo);
                        }
                    }
                }
            } catch (Throwable e) {
                LogService.getRoot().warn("com.owc.singularity.engine.metadata.MetaDataFactory.failed_to_create_meta_data", e);
            }
        }
        return new MetaData(ioo.getClass());
    }

}
