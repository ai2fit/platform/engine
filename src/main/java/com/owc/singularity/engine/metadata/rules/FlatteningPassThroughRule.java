/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.rules;


import java.util.List;

import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;


/**
 * Passes metadata through and recursively unfolds element metadata if CollectionMetaData is found.
 * 
 * @author Simon Fischer
 * 
 */
public class FlatteningPassThroughRule implements MDTransformationRule {

    private final List<InputPort> inputPorts;
    private final OutputPort outputPort;

    public FlatteningPassThroughRule(List<InputPort> inputPorts, OutputPort outputPort) {
        this.inputPorts = inputPorts;
        this.outputPort = outputPort;
    }

    @Override
    public void transformMD() {
        for (InputPort inputPort : inputPorts) {
            MetaData metaData = inputPort.getMetaData();
            if (metaData != null) {
                if (metaData instanceof CollectionMetaData) {
                    metaData = ((CollectionMetaData) metaData).getElementMetaDataRecursive();
                }
                if (metaData != null) {
                    metaData = metaData.clone();
                    outputPort.deliverMD(modifyMetaData(metaData));
                    return;
                }
            }
        }
        outputPort.deliverMD(null);
    }

    protected MetaData modifyMetaData(MetaData metaData) {
        return metaData;
    }
}
