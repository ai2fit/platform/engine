/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import java.util.Collections;
import java.util.List;

import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.ports.PortOwner;
import com.owc.singularity.studio.gui.editor.quickfix.DisconnectQuickFix;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * Indicates that an error occurred during an operator loop.
 * 
 * @author Simon Fischer
 * 
 */
public class OperatorLoopError implements MetaDataError {

    private OutputPort outputPort;
    private InputPort inputPort;
    private Port port;

    private final QuickFix fix;

    public OperatorLoopError(InputPort port) {
        this.port = port;
        this.inputPort = port;
        this.outputPort = port.getSource();
        fix = new DisconnectQuickFix(outputPort, inputPort);
    }

    public OperatorLoopError(OutputPort port) {
        this.port = port;
        this.outputPort = port;
        this.inputPort = port.getDestination();
        fix = new DisconnectQuickFix(outputPort, inputPort);
    }

    @Override
    public String getMessage() {
        return "This port is part of a loop.";
    }

    @Override
    public PortOwner getOwner() {
        return port.getPorts().getOwner();
    }

    @Override
    public Port getPort() {
        return port;
    }

    @Override
    public List<QuickFix> getQuickFixes() {
        return Collections.singletonList(fix);
    }

    @Override
    public Severity getSeverity() {
        return Severity.ERROR;
    }
}
