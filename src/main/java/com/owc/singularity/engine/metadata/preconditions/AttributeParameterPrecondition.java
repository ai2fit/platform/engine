/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.preconditions;


import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * This precondition can be used, if a single {@link AttributeMetaData} must be contained in the
 * example set. Three properties of the attribute might be given: Name, Type and Role. Type and Role
 * are optional. The attribute name is not given explicitly, instead a parameter name of an operator
 * is given, from which the attribute name is retrieved during runtime.
 * 
 * @author Sebastian Land
 * 
 */
public class AttributeParameterPrecondition extends AbstractPrecondition {

    private final Operator operator;
    private final String parameterName;
    private final ValueType attributeType;
    private final String attributeRole;

    /**
     * This precondition will only check the name. No Role and type checks will be performed.
     */
    public AttributeParameterPrecondition(InputPort inputPort, Operator operator, String parameterName) {
        this(inputPort, operator, parameterName, null, null);
    }

    /**
     * This precondition will not perform any role check.
     */
    public AttributeParameterPrecondition(InputPort inputPort, Operator operator, String parameterName, ValueType attributeType) {
        this(inputPort, operator, parameterName, null, attributeType);
    }

    public AttributeParameterPrecondition(InputPort inputPort, Operator operator, String parameterName, String attributeRole, ValueType attributeType) {
        super(inputPort);
        this.operator = operator;
        this.parameterName = parameterName;
        this.attributeType = attributeType;
        this.attributeRole = attributeRole;

    }

    @Override
    public void check(OperationDescriptionContext context, MetaData metaData) {
        if (metaData != null) {
            if (metaData instanceof ExampleSetMetaData emd) {
                String attributeName = getName();
                if (attributeName != null) {
                    // checking if attribute with name and type exists
                    if (emd.containsAttribute(attributeName)) {
                        AttributeMetaData amd = emd.getAttributeByName(attributeName);
                        if (amd.getValueType().equals(attributeType)) {
                            if (attributeRole != null && !attributeRole.equals(emd.getRole(amd))) {
                                createError(Severity.ERROR, "attribute_must_have_role", attributeName, attributeRole);
                            }
                        } else {
                            if (attributeType != null)
                                createError(Severity.ERROR, "attribute_has_wrong_type", attributeName, attributeType.toString());
                        }
                    } else {
                        createError(Severity.WARNING, "missing_attribute", attributeName);
                    }
                }
                makeAdditionalChecks(emd);
            }
        }
    }

    /**
     * This method returns the name of the attribute that must be contained in the metadata. It
     * might return null, if no check should be performed.
     */
    protected String getName() {
        try {
            return operator.getParameterAsString(parameterName);
        } catch (UndefinedParameterError e) {
            return null;
        }
    }


    /** Can be implemented by subclasses in order to specify quickfixes. */
    public QuickFix getQuickFix(ExampleSetMetaData emd) throws UndefinedParameterError {
        return null;
    }

    /** Can be implemented by subclasses. */
    public void makeAdditionalChecks(ExampleSetMetaData emd) {}

    @Override
    public String getDescription() {
        return "<em>expects:</em> ExampleSet";
    }

    @Override
    public boolean isCompatible(MetaData input) {
        return ExampleSet.class.isAssignableFrom(input.getObjectClass());
    }

    @Override
    public MetaData getExpectedMetaData() {
        return ExampleSetMetaData.emptyMetaData();
    }

}
