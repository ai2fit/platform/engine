/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.preconditions;


import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.metadata.rules.MDTransformationRule;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.ports.InputPort;


/**
 * A simple precondition of an {@link InputPort}. In an early design phase, preconditions used to be
 * special {@link MDTransformationRule}s rules, but having them separately attached to the input
 * ports has advantages for GUI design and auto-wiring pipelines.
 * 
 * @author Simon Fischer
 */
public interface Precondition {

    /**
     * Checks whether the precondition is satisfied, registering a {@link MetaDataError} with the
     * input port if not.
     * 
     * @param context
     *            TODO
     * @param metaData
     *            the delivered metadata. Note that this may differ from the meta data currently
     *            assigned to the input port for which this Precondition was created, e.g. for a
     *            CollectionPrecondition.
     */
    public void check(OperationDescriptionContext context, MetaData metaData);

    /** Returns a human readable description. */
    public String getDescription();

    /** Returns true if the given object is compatible with this precondition. */
    public boolean isCompatible(MetaData input);


    /** Returns the metadata required by this precondition. */
    public MetaData getExpectedMetaData();
}
