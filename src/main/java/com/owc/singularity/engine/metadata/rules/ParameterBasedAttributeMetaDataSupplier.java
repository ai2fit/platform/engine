package com.owc.singularity.engine.metadata.rules;


import java.util.function.Supplier;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;

/**
 * This class allows to provide a new attribute meta data based upon a parameter
 *
 * @author Sebastian Land
 *
 */
public class ParameterBasedAttributeMetaDataSupplier implements Supplier<AttributeMetaData> {

    private final Operator operator;
    private String parameterName;
    private ValueType attributeType;

    public ParameterBasedAttributeMetaDataSupplier(Operator operator, String parameterName, ValueType attributeType) {
        this.operator = operator;
        this.parameterName = parameterName;
        this.attributeType = attributeType;
    }

    @Override
    public AttributeMetaData get() {
        try {
            return AttributeMetaData.of(operator.getParameterAsString(parameterName), attributeType);
        } catch (UndefinedParameterError e) {
            return AttributeMetaData.of("undefined parameter for name", ValueType.NUMERIC);
        }
    }
}
