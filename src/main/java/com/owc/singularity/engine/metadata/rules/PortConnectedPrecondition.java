/*
 *  SingularityEngine
 *
 *  Copyright (C) 2001-2013 by Rapid-I and the contributors
 *
 *  Complete list of developers available at our web site:
 *
 *       http://rapid-i.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.AbstractPrecondition;
import com.owc.singularity.engine.metadata.preconditions.Precondition;
import com.owc.singularity.engine.operator.AbstractOperator.OperationDescriptionContext;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.Port;

/**
 * This precondition wraps around another precondition and performs the check only if a port is
 * connected. If not, no checks will be performed. This might be used for checking depending on
 * connected ports.
 * 
 * @author Sebastian Land
 *
 */
public class PortConnectedPrecondition extends AbstractPrecondition {

    private final Precondition condition;
    private final boolean needsConnection;
    private final Port port;

    public PortConnectedPrecondition(InputPort inputPort, Precondition condition, Port port, boolean needsCondition) {
        super(inputPort);
        this.condition = condition;
        this.port = port;
        this.needsConnection = needsCondition;
    }

    @Override
    public void check(OperationDescriptionContext context, MetaData metaData) {
        if (needsConnection == port.isConnected()) {
            condition.check(context, metaData);
        }
    }

    @Override
    public String getDescription() {
        return condition.getDescription();
    }

    @Override
    public boolean isCompatible(MetaData input) {
        return condition.isCompatible(input);
    }

    @Override
    public MetaData getExpectedMetaData() {
        return condition.getExpectedMetaData();
    }

}
