package com.owc.singularity.engine.metadata.transformer;

import java.io.Serializable;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;

public interface ExampleSetMetaDataTransformer extends Serializable {

    ExampleSetMetaData apply(ExampleSetMetaData emd);
}
