package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.ports.Port;

public class SubprocessesTransformationRule implements MDTransformationRule {

    private OperatorChain operator;

    public SubprocessesTransformationRule(OperatorChain operator) {
        this.operator = operator;
    }

    @Override
    public void transformMD() {
        for (ExecutionUnit unit : operator.getSubprocesses()) {
            for (Operator op : unit.getAllInnerOperators()) {
                op.clear(Port.CLEAR_META_DATA_ERRORS);
            }
            unit.transformMetaData();
        }
    }

}
