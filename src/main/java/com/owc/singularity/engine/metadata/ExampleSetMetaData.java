/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.Tools;


/**
 * This class stores metadata information about {@link ExampleSet}s. It contains only a signature,
 * meaning that it only stores the names, value types and roles of present attributes.
 *
 * @author Sebastian Land
 */
public class ExampleSetMetaData extends MetaData {

    private static final long serialVersionUID = 1381512437701123982L;

    private static final ExampleSetMetaData EMPTY_META_DATA = new ExampleSetMetaData(new String[0], new ValueType[0], new String[0]);

    AttributeMetaData[] attributeMetaData;

    private HashMap<String, AttributeMetaData> attributeNameMap = new HashMap<>();
    private HashMap<String, AttributeMetaData> roleAttributeMap = new HashMap<>();
    private HashMap<AttributeMetaData, String> attributeRoleMap = new HashMap<>();
    private int numberOfRegularAttributes;

    private ExampleSetMetaData(String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles) {
        this(IntStream.range(0, attributeNames.length).mapToObj(i -> AttributeMetaData.of(attributeNames[i], valueTypes[i])).toArray(AttributeMetaData[]::new),
                attributeRoles);
    }

    private ExampleSetMetaData(AttributeMetaData[] attributeMetaData, String[] attributeRoles) {
        super(ExampleSet.class);
        this.attributeMetaData = attributeMetaData;
        for (int i = 0; i < attributeMetaData.length; i++) {
            attributeNameMap.put(attributeMetaData[i].getName(), attributeMetaData[i]);
            if (attributeRoles[i] != null) {
                roleAttributeMap.put(attributeRoles[i], attributeMetaData[i]);
                attributeRoleMap.put(attributeMetaData[i], attributeRoles[i]);
            }
        }
        this.numberOfRegularAttributes = attributeMetaData.length - attributeRoleMap.size();
    }

    /**
     * Needs to be public because metadata classes need a public constructor for their associated
     * IOObjects in {@link MetaDataFactory}.
     */
    public ExampleSetMetaData(ExampleSet exampleSet) {
        this(exampleSet.getAttributes().streamAttributes().map(i -> AttributeMetaData.of(i.getName(), i.getValueType())).toArray(AttributeMetaData[]::new),
                exampleSet.getAttributes().streamAttributes().map(Attribute::getRole).toArray(String[]::new));
    }

    private ExampleSetMetaData(List<AttributeMetaData> attributeMetaData) {
        this(attributeMetaData.toArray(AttributeMetaData[]::new), new String[attributeMetaData.size()]);
    }

    public AttributeMetaData getAttributeByName(String name) {
        return attributeNameMap.get(name);
    }

    public AttributeMetaData getAttributeByRole(String role) {
        return roleAttributeMap.get(role);
    }

    public Collection<AttributeMetaData> getAllAttributes() {
        return Collections.unmodifiableCollection(Arrays.asList(attributeMetaData));
    }

    @Override
    public ExampleSetMetaData clone() {
        return this;
    }


    /**
     * This returns if an attribute with the given role exists in the example set. If the role is
     * confidence, then it checks not whether exactly the same role occurs, but if any role starts
     * with the confidence stem.
     */
    public boolean hasSpecial(String role) {
        return roleAttributeMap.containsKey(role);
    }

    public boolean containsAttribute(String name) {
        return attributeNameMap.containsKey(name);
    }

    public String getRole(AttributeMetaData amd) {
        return attributeRoleMap.get(amd);
    }

    public boolean isSpecial(AttributeMetaData amd) {
        return attributeRoleMap.containsKey(amd);
    }

    public int getNumberOfSpecialAttributes() {
        return roleAttributeMap.size();
    }

    public int getNumberOfRegularAttributes() {
        return numberOfRegularAttributes;
    }

    public ExampleSetMetaDataTransformer transform() {
        return new ExampleSetMetaDataTransformer(this);
    }


    /** Checks if the attribute sets are equal. */
    public boolean equals(ExampleSetMetaData other) {
        if (other == this) {
            return true;
        }
        if (numberOfRegularAttributes != other.numberOfRegularAttributes)
            return false;
        if (!Arrays.equals(attributeMetaData, other.attributeMetaData))
            return false;
        return (roleAttributeMap.equals(other.roleAttributeMap));
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("ExampleSetMetaData: #attributes: ")
                .append(getNumberOfRegularAttributes())
                .append("#special: ")
                .append(getNumberOfSpecialAttributes())
                .append(Tools.getLineSeparator());
        for (AttributeMetaData amd : getAllAttributes()) {
            buffer.append(amd.toString());
            if (isSpecial(amd)) {
                buffer.append("@");
                buffer.append(getRole(amd));
            }
            buffer.append(Tools.getLineSeparator());
        }
        return buffer.toString();
    }

    public String getShortDescription() {
        StringBuilder buf = new StringBuilder("Example Set");
        buf.append("<br/>");
        buf.append(getNumberOfRegularAttributes());
        buf.append(" attribute").append(getNumberOfRegularAttributes() != 1 ? "s" : "").append(": ");
        buf.append(getNumberOfSpecialAttributes());
        buf.append(" special attribute").append(getNumberOfSpecialAttributes() != 1 ? "s" : "").append(": ");
        return buf.toString();
    }


    @Override
    public String getDescription() {
        StringBuilder buf = new StringBuilder("Example Set");
        buf.append("<br/>");
        buf.append(getNumberOfRegularAttributes());
        buf.append(" attribute").append(getNumberOfRegularAttributes() != 1 ? "s" : "").append(": ");
        buf.append(getNumberOfSpecialAttributes());
        buf.append(" special attribute").append(getNumberOfSpecialAttributes() != 1 ? "s" : "").append(": ");
        buf.append("<table><thead><tr><th>Role</th><th>Name</th><th>Type</th></tr></thead><tbody>");
        for (AttributeMetaData amd : attributeMetaData) {
            buf.append("<tr><td>");
            buf.append(getRole(amd));
            buf.append("</td><td>");
            buf.append(amd.getName());
            buf.append("</td></tr>");
        }
        buf.append("</tbody></table>");
        return buf.toString();
    }

    public static final class ExampleSetMetaDataTransformer {

        private final LinkedList<AttributeMetaData> derivations = new LinkedList<>();

        private final HashMap<String, String> attributeRenamings = new HashMap<>();
        private final HashMap<String, String> attributeRoles = new HashMap<>();
        private final HashMap<String, String> attributeRolesInheritance = new HashMap<>();

        private final HashSet<String> removeAttributeNames = new HashSet<>();
        private ExampleSetMetaData sourceMetaData;

        private List<String> attributeOrder;


        public ExampleSetMetaDataTransformer(ExampleSetMetaData sourceMetaData) {
            this.sourceMetaData = sourceMetaData;
        }

        public ExampleSetMetaDataTransformer inheritRole(String sourceAttributeName, String destinationAttributeName) {
            attributeRolesInheritance.put(destinationAttributeName, sourceAttributeName);
            return this;
        }

        public ExampleSetMetaDataTransformer setRole(String attributeName, String role) {
            attributeRoles.put(attributeName, role);
            return this;
        }

        public ExampleSetMetaDataTransformer renameAttribute(String oldName, String newName) {
            attributeRenamings.put(oldName, newName);
            attributeRolesInheritance.put(newName, oldName);
            return this;
        }

        public ExampleSetMetaDataTransformer withoutAttribute(String name) {
            removeAttributeNames.add(name);
            return this;
        }

        public ExampleSetMetaDataTransformer reorderAttributes(List<String> attributeOrder) {
            this.attributeOrder = attributeOrder;
            return this;
        }

        public ExampleSetMetaDataTransformer withDerivedAttribute(String name, ValueType type) {
            derivations.add(AttributeMetaData.of(name, type));
            return this;
        }

        public ExampleSetMetaData transform() {
            Stream<AttributeMetaData> stream = Stream
                    .concat(Arrays.stream(sourceMetaData.attributeMetaData).filter(a -> !removeAttributeNames.contains(a.getName())).map(amd -> {
                        String newName = attributeRenamings.get(amd.getName());
                        if (newName != null)
                            return AttributeMetaData.of(newName, amd.getValueType());
                        return amd;
                    }), derivations.stream());
            if (attributeOrder != null)
                stream = stream.filter(a -> attributeOrder.contains(a.getName()))
                        .sorted((a1, a2) -> Integer.compare(attributeOrder.indexOf(a1.getName()), attributeOrder.indexOf(a2.getName())));
            AttributeMetaData[] attributeMetaDatas = stream.toArray(AttributeMetaData[]::new);

            String[] attributeRoles = Arrays.stream(attributeMetaDatas).map(amd -> {
                String name = amd.getName();
                String newRole = this.attributeRoles.get(name);
                if (newRole != null)
                    return newRole;
                String inheritanceFrom = attributeRolesInheritance.get(name);
                if (inheritanceFrom != null) {
                    return sourceMetaData.getRole(sourceMetaData.getAttributeByName(inheritanceFrom));
                }
                return sourceMetaData.getRole(amd);
            }).toArray(String[]::new);
            return new ExampleSetMetaData(attributeMetaDatas, attributeRoles);
        }
    }

    public static ExampleSetMetaData of(String[] attributeNames, ValueType[] valueTypes, String[] attributeRoles) {
        return new ExampleSetMetaData(attributeNames, valueTypes, attributeRoles);
    }

    public static ExampleSetMetaData of(ExampleSet exampleSet) {
        return new ExampleSetMetaData(exampleSet);
    }

    public static ExampleSetMetaData of(List<AttributeMetaData> attributeMetaData) {
        return new ExampleSetMetaData(attributeMetaData);
    }

    public static ExampleSetMetaData emptyMetaData() {
        return EMPTY_META_DATA;
    }
}
