/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.metadata.InputMissingMetaDataError;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;


/**
 * Assigns metadata received from an input port to an output port. Useful e.g. for preprocessing
 * operators. If metadata changes dynamically, can be modified by overriding
 * {@link #modifyMetaData(MetaData)}.
 * 
 * @author Simon Fischer
 */
public class PassThroughRule implements MDTransformationRule {

    private final OutputPort outputPort;

    private final InputPort inputPort;
    private final boolean optional;

    public PassThroughRule(InputPort inputPort, OutputPort outputPort, boolean mandatory) {
        super();
        this.outputPort = outputPort;
        this.inputPort = inputPort;
        this.optional = !mandatory;
    }

    @Override
    public void transformMD() {
        MetaData modified = inputPort.getMetaData();
        if (modified == null) {
            if (!optional) {
                inputPort.addError(new InputMissingMetaDataError(inputPort, null, null));
            }
            outputPort.deliverMD(null);
        } else {
            modified = modified.clone();
            outputPort.deliverMD(modifyMetaData(modified));
        }
    }

    /**
     * Modifies the received metadata before it is passed to the output. Can be used if the
     * transformation depends on parameters etc. The default implementation just returns the
     * original. Subclasses may safely modify the metadata, since a copy is used for this method.
     */
    public MetaData modifyMetaData(MetaData unmodifiedMetaData) {
        return unmodifiedMetaData;
    }

    public OutputPort getOutputPort() {
        return this.outputPort;
    }

    public InputPort getInputPort() {
        return this.inputPort;
    }

    public boolean isOptional() {
        return this.optional;
    }

}
