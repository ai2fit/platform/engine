/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.preconditions;


import java.util.Collections;
import java.util.List;

import com.owc.singularity.engine.metadata.SimpleMetaDataError;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;


/**
 * A simple precondition of an {@link InputPort} with method shortcuts to display a
 * {@link SimpleMetaDataError} to the respective {@link InputPort} if the precondition is not met.
 * 
 * @author Simon Fischer
 */
public abstract class AbstractPrecondition implements Precondition {

    private final InputPort inputPort;

    public AbstractPrecondition(InputPort inputPort) {
        this.inputPort = inputPort;
    }

    protected InputPort getInputPort() {
        return inputPort;
    }

    protected void createError(Severity severity, String i18nKey, Object... i18nArgs) {
        createError(severity, Collections.emptyList(), i18nKey, i18nArgs);
    }

    protected void createError(Severity severity, final List<? extends QuickFix> fixes, String i18nKey, Object... i18nArgs) {
        getInputPort().addError(new SimpleMetaDataError(severity, getInputPort(), fixes, i18nKey, i18nArgs));
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
