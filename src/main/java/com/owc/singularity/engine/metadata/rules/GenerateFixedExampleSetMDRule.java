/*
 *  SingularityEngine
 *
 *  Copyright (C) 2001-2013 by Rapid-I and the contributors
 *
 *  Complete list of developers available at our web site:
 *
 *       http://rapid-i.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.data.exampleset.ExampleSetCreator;
import com.owc.singularity.engine.ports.OutputPort;

/**
 * This rule is for operators, which create a completely new example set with previously known
 * MetaData. This is very likely to be used with an {@link ExampleSetCreator}
 * 
 * @author Sebastian Land
 */
public class GenerateFixedExampleSetMDRule implements MDTransformationRule {

    private ExampleSetMetaData emd;
    private OutputPort outputPort;

    public GenerateFixedExampleSetMDRule(OutputPort outPort, ExampleSetMetaData emd) {
        this.emd = emd;
        outputPort = outPort;
    }

    @Override
    public void transformMD() {
        ExampleSetMetaData clone = emd.clone();
        outputPort.deliverMD(modifyMetaData(clone));
    }

    /**
     * Modifies the standard metadata before it is passed to the output. Can be used if the
     * transformation depends on parameters etc. The default implementation just returns the
     * original. Subclasses may safely modify the metadata, since a copy is used for this method.
     */
    public MetaData modifyMetaData(ExampleSetMetaData unmodifiedMetaData) {
        return unmodifiedMetaData;
    }

}
