/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.ports.OutputPort;


/**
 * Assigns a predefined metadata object to an output port. Useful if operators newly generate
 * IOObjects. If the metadata changes dynamically, can be modified by overriding
 * {@link #modifyMetaData(MetaData)}.
 *
 * @author Simon Fischer
 */
public class GenerateNewMDRule implements MDTransformationRule {

    private OutputPort outputPort;
    private MetaData unmodifiedMetaData;

    public GenerateNewMDRule(OutputPort outputPort, Class<? extends IOObject> clazz) {
        this(outputPort, ExampleSet.class.equals(clazz) ? ExampleSetMetaData.emptyMetaData() : new MetaData(clazz));
    }

    public GenerateNewMDRule(OutputPort outputPort, MetaData unmodifiedMetaData) {
        this.outputPort = outputPort;
        this.unmodifiedMetaData = unmodifiedMetaData;
    }

    @Override
    public void transformMD() {
        MetaData clone = this.unmodifiedMetaData.clone();
        outputPort.deliverMD(modifyMetaData(clone));
    }

    /**
     * Modifies the standard metadata before it is passed to the output. Can be used if the
     * transformation depends on parameters etc. The default implementation just returns the
     * original. Subclasses may safely modify the metadata, since a copy is used for this method.
     */
    public MetaData modifyMetaData(MetaData unmodifiedMetaData) {
        return unmodifiedMetaData;
    }

    /**
     * @return a clone of the unmodified metadata object
     */
    public MetaData getUnmodifiedMetaData() {
        return unmodifiedMetaData.clone();
    }

    /**
     * @return the {@link OutputPort} the MD rule is for
     */
    public OutputPort getOutputPort() {
        return outputPort;
    }

}
