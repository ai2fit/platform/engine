/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import java.io.Serializable;
import java.util.Objects;

import com.owc.singularity.engine.object.data.exampleset.ValueType;


/**
 * Metadata about an attribute. Metadata is read only and can be changed between different parent
 * {@link ExampleSetMetaData} objects. AttributeMetaData only contains the name and the value-type
 * of an attribute. If you wish to access the role of an attribute try
 * {@link ExampleSetMetaData#getRole(AttributeMetaData)}
 *
 * @author Sebastian Land
 *
 */
public class AttributeMetaData implements Serializable {


    private static final long serialVersionUID = -2432213185368213629L;
    private final String name;
    private final ValueType type;

    private AttributeMetaData(String name, ValueType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public ValueType getValueType() {
        return type;
    }

    @Override
    public String toString() {
        return getDescription();
    }

    public String getDescription() {
        StringBuilder buf = new StringBuilder();

        buf.append(getName());
        buf.append(" (");
        buf.append(getValueType().name());
        buf.append(")");
        return buf.toString();
    }


    public boolean isNominal() {
        return type.equals(ValueType.NOMINAL);
    }

    public boolean isNumerical() {
        return type.equals(ValueType.NUMERIC);
    }

    /**
     * @return {@code true} if the type of the attribute is a {@link ValueType#TIMESTAMP} (and all
     *         it's subtypes), {@code false} otherwise
     * 
     */
    public boolean isTimestamp() {
        return type.equals(ValueType.TIMESTAMP);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AttributeMetaData other = (AttributeMetaData) obj;
        return Objects.equals(name, other.name) && type == other.type;
    }

    public static AttributeMetaData of(String name, ValueType valueType) {
        return new AttributeMetaData(name, valueType);
    }

}
