package com.owc.singularity.engine.metadata;

import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.repository.connection.ConnectionParameters;

public class ConnectionParametersMetadata extends MetaData {

    private static final long serialVersionUID = -5497928250098337731L;
    private final ConnectionParameters parameters;

    public ConnectionParametersMetadata(ConnectionParametersIOObject connectionParametersIOObject) {
        super(ConnectionParametersIOObject.class);
        this.parameters = connectionParametersIOObject.getParameters();
    }

    public ConnectionParameters getParameters() {
        return parameters;
    }

    @Override
    public MetaData clone() {
        return this;
    }
}
