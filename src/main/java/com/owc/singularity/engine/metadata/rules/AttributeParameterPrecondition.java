package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.ports.InputPort;

/**
 * This subclass fixes a problem with empty parameters that are actually optional.
 *
 * @author Sebastian Land
 *
 */
public class AttributeParameterPrecondition extends com.owc.singularity.engine.metadata.preconditions.AttributeParameterPrecondition {

    public AttributeParameterPrecondition(InputPort inputPort, Operator operator, String parameterName, ValueType attributeType) {
        super(inputPort, operator, parameterName, attributeType);
    }

    @Override
    protected String getName() {
        String name = super.getName();
        if (name.isEmpty())
            return null;
        return name;
    }
}
