package com.owc.singularity.engine.metadata.rules;


import com.owc.singularity.engine.metadata.CollectionMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;

/**
 * This class will take the metadata of a given input port, put it in a collection and return it
 * unchanged.
 * 
 * @author Sebastian Land
 *
 */
public class CollectedPassThroughMDRule implements MDTransformationRule {

    private InputPort inputPort;
    private OutputPort outputPort;

    public CollectedPassThroughMDRule(InputPort inPort, OutputPort outPort) {
        this.inputPort = inPort;
        this.outputPort = outPort;
    }


    @Override
    public void transformMD() {
        CollectionMetaData collectionMetaData = new CollectionMetaData(modifyMetaData(inputPort.getMetaData()));
        outputPort.deliverMD(collectionMetaData);
    }

    /**
     * Modifies the standard met data before it is passed to the output. Can be used if the
     * transformation depends on parameters etc. The default implementation just returns the
     * original. Subclasses may safely modify the metadata, since a copy is used for this method.
     */
    public MetaData modifyMetaData(MetaData unmodifiedMetaData) {
        return unmodifiedMetaData;
    }

}
