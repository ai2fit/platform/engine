/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.engine.metadata;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.metadata.rules.GenerateNewMDRule;
import com.owc.singularity.engine.metadata.rules.MDTransformationRule;
import com.owc.singularity.engine.metadata.rules.PassThroughRule;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.operator.error.SimpleProcessSetupError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;


/**
 * This class mirrors the behavior of an Operator's apply-method with respect to the I/O metadata.
 * This functionality is performed by a class of its own (rather than another method in Operator) to
 * keep Operator lean and since it is assumed that most metadata transformations can be handled by a
 * small set of standard rules.
 * <p>
 * The general rule is that methods of this package should not throw exceptions but rather register
 * possible errors with the ports if preconditions are not satisfied etc.
 *
 * @author Simon Fischer
 */
public class MDTransformer {

    private final LinkedList<MDTransformationRule> transformationRules = new LinkedList<>();
    private final Operator operator;

    public MDTransformer(Operator op) {
        this.operator = op;
    }

    /** Executes all rules added by {@link #addRule}. */
    public void transformMetaData() {
        for (MDTransformationRule rule : new ArrayList<>(transformationRules)) {
            try {
                rule.transformMD();
            } catch (Exception e) {
                operator.getLogger().warn("Error during meta data transformation: " + e, e);
                operator.addError(new SimpleProcessSetupError(Severity.WARNING, operator.getPortOwner(), "exception_transforming_metadata", e.toString()));
            }
        }
    }

    public void addRule(MDTransformationRule rule) {
        transformationRules.add(rule);
    }

    /** Convenience method to generate a {@link PassThroughRule}. */
    public void addPassThroughRule(InputPort input, OutputPort output) {
        addRule(new PassThroughRule(input, output, false));
    }

    /** Convenience method to generate a {@link GenerateNewMDRule}. */
    public void addGenerationRule(OutputPort output, Class<? extends IOObject> clazz) {
        addRule(new GenerateNewMDRule(output, clazz));
    }

    public void clearRules() {
        transformationRules.clear();
    }

    public void addRuleAtBeginning(MDTransformationRule mdTransformationRule) {
        transformationRules.addFirst(mdTransformationRule);
    }

    /**
     * @return an unmodifiable list of MDTransformationRules
     */
    public final List<MDTransformationRule> getRules() {
        return Collections.unmodifiableList(transformationRules);
    }

}
