package com.owc.singularity.studio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryChangeListener;
import com.owc.singularity.repository.RepositoryFileSystem;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.SwingTools;

/**
 * This class handles the history of recently accessed repository locations and persists them.
 * Currently only updated for pipeline entries.
 * 
 * @author Sebastian Land
 *
 */
public class RecentLocationsManager {

    private static LinkedList<RepositoryPath> recentFiles = new LinkedList<>();
    public static final int DEFAULT_MAX_NUMBER_OF_RECENT_FILES = 10;

    public static RepositoryChangeListener listener = new RemoveEntryRepositoryChangeListener();

    public static void addToRecentFilesAsMostRecent(final RepositoryPath location) {
        if (location != null && location.isHead()) {
            while (recentFiles.contains(location)) {
                recentFiles.remove(location);
            }
            recentFiles.addFirst(location);
            while (recentFiles.size() > getMaxNumberOfRecentFiles()) {
                recentFiles.removeLast();
            }
            saveRecentFileList();
            MainFrame.INSTANCE.updateRecentFileList();
        }
    }

    public static void replaceRecentFile(final RepositoryPath originalLocation, final RepositoryPath newLocation) {
        if (newLocation != null && newLocation.isHead() && recentFiles.contains(originalLocation)) {
            while (recentFiles.contains(originalLocation)) {
                recentFiles.set(recentFiles.lastIndexOf(originalLocation), newLocation);
            }
            saveRecentFileList();
            MainFrame.INSTANCE.updateRecentFileList();
        }
    }

    public static List<RepositoryPath> getRecentFiles() {
        return recentFiles;
    }

    public static void saveRecentFileList() {
        File file = FileSystemService.getUserConfigFile("history");
        try (FileWriter fw = new FileWriter(file); PrintWriter out = new PrintWriter(fw)) {
            for (RepositoryPath loc : recentFiles) {
                if (Files.exists(loc) && Files.isRegularFile(loc))
                    out.println(loc);
            }
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("cannot_write_history_file", e);
        }
    }

    public static void loadRecentFileList() {
        File file = FileSystemService.getUserConfigFile("history");
        if (!file.exists()) {
            return;
        }
        recentFiles.clear();
        RepositoryFileSystem fileSystem = RepositoryManager.getFileSystem();
        fileSystem.registerChangeListener(listener);
        try {
            for (String line : Files.readAllLines(file.toPath())) {
                try {
                    RepositoryPath path = RepositoryPath.of(line);
                    if (Files.exists(path) && Files.isRegularFile(path))
                        recentFiles.add(RepositoryPath.of(line));
                } catch (IllegalArgumentException e) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.unparseable_line", "history", line);
                }
            }
        } catch (IOException e) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.reading_history_file_error", e);
        }
    }

    private static int getMaxNumberOfRecentFiles() {
        String maxUndoStepsProperty = PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_GUI_RECENT_PIPELINES_SIZE);
        int resolved = DEFAULT_MAX_NUMBER_OF_RECENT_FILES;
        if (maxUndoStepsProperty != null) {
            try {
                resolved = Integer.parseInt(maxUndoStepsProperty);
            } catch (NumberFormatException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.main_frame_warning");
            }
        }
        return resolved;
    }

    public static void removeEntryFromRecentFiles(RepositoryPath location) {
        if (location != null && location.isHead()) {
            if (recentFiles.contains(location)) {
                while (recentFiles.contains(location)) {
                    recentFiles.remove(location);
                }
                saveRecentFileList();
                MainFrame.INSTANCE.updateRecentFileList();
            }
        }
    }

    private static class RemoveEntryRepositoryChangeListener implements RepositoryChangeListener {

        @Override
        public void entryDeleted(RepositoryPath path) {
            removeEntryFromRecentFiles(path);
        }

        @Override
        public void entryCreated(RepositoryPath path) {

        }

        @Override
        public void entryModified(RepositoryPath path) {

        }
    }
}
