/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.documentation;


import static com.owc.singularity.studio.documentation.OperatorDocumentationService.DEFAULT_IOOBJECT_ICON_NAME;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.IOObjectGUIService;
import com.owc.singularity.studio.gui.tools.Icons;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This class handles the conversion of the operator documentation XML to an HTML page. It's methods
 * are addressed by the xslt style sheet.
 *
 * @author Philipp Kersting, Marco Boeck, Marcel Seifert
 *
 */
@SuppressWarnings("unused")
public class OperatorDocumentationConverter {

    private static final Logger log = LogManager.getLogger(OperatorDocumentationConverter.class);
    private static final TransformerFactory XSLT_TRANSFORMER_FACTORY = TransformerFactory.newInstance();
    public static final String STYLESHEET_RESOURCE = "/com/owc/singularity/resources/documentationview.xslt";
    private static final byte[] XSLT_CONTENT;
    private static final int MAX_CATEROGIES_DISPLAYED_IN_HELP = 10;

    static final String INTEGER_LABEL = I18N.getGUILabel("attribute_type.integer");
    static final String LONG_LABEL = I18N.getGUILabel("attribute_type.long");
    static final String REAL_LABEL = I18N.getGUILabel("attribute_type.real");
    static final String SELECTION_LABEL = I18N.getGUILabel("attribute_type.selection");
    private static final String STRING_LABEL = I18N.getGUILabel("attribute_type.string");
    private static final String LIST_LABEL = I18N.getGUILabel("attribute_type.list");
    private static final String ENUMERATION_LABEL = I18N.getGUILabel("attribute_type.enumeration");
    private static final String BOOLEAN_LABEL = I18N.getGUILabel("attribute_type.boolean");
    private static final String OTHER_LABEL = I18N.getGUILabel("attribute_type.other");

    private static final HashMap<String, Operator> OPERATOR_CACHE = new HashMap<>();

    /**
     * This icon will be shown as a replacement for operators without an icon. These are usually
     * deprecated operators.
     */
    private static final String REPLACEMENT_FOR_OPERATORS_WITHOUT_ICON = "symbol_questionmark.png";

    static {
        byte[] content = new byte[0];
        try {
            content = Tools.readInputStream(OperatorDocumentationConverter.class.getResourceAsStream(STYLESHEET_RESOURCE));
        } catch (IOException e) {
            log.error(LogService.MARKER_DEVELOPMENT, "Error loading XSLT from resources {}", STYLESHEET_RESOURCE, e);
        }
        XSLT_CONTENT = content;
    }

    /**
     * Applies the documentation XSLT to the specified XML source and returns the transformed HTML
     * as {@link String}.
     *
     * @param xmlSource
     *            the source containing the documentation XML
     * @return the transformed documentation XML as HTML
     * @throws TransformerException
     *             in case the transformation goes wrong
     */
    public static String applyXSLTTransformation(Source xmlSource) throws TransformerException {
        StringWriter buffer = new StringWriter();
        Source xsltSource = new StreamSource(new ByteArrayInputStream(XSLT_CONTENT));
        Transformer trans = XSLT_TRANSFORMER_FACTORY.newTransformer(xsltSource);
        trans.transform(xmlSource, new StreamResult(buffer));
        return buffer.toString();
    }

    /**
     * Replaces underscores in the given {@link String} with a blank.
     *
     * @param string
     * @return
     */
    public static String insertBlanks(String string) {
        return string.replace('_', ' ');
    }

    /**
     * Returns the name of a type in exchange for its class' name.
     *
     * @param type
     *            the class' name as String
     * @return the short name of the class as String
     */
    @SuppressWarnings("unchecked")
    public static String getTypeNameForType(String type) {
        if (type == null || type.isEmpty()) {
            return "";
        }
        Class<? extends IOObject> objectClass = IOObjectService.getClassByClassName(type);
        if (objectClass == null) {
            // no matching class was found for type
            log.warn(LogService.MARKER_DEVELOPMENT, "No IOObject class was found for type \"{}\"", type);
            return "";
        }
        return getTypeNameForType(objectClass);
    }

    static String getTypeNameForType(Class<? extends IOObject> typeClass) {
        if (typeClass != null) {
            String typeName;
            typeName = IOObjectService.getName(typeClass);
            if (typeName != null && !typeName.isEmpty()) {
                return " (" + typeName + ")";
            } else {
                log.warn(LogService.MARKER_DEVELOPMENT, "No reportable name was found for class \"{}\"", typeClass);
                return "";
            }
        } else {
            log.warn(LogService.MARKER_DEVELOPMENT, "A null class was provided to search for its reportable name");
            return "";
        }
    }

    /**
     * Returns the path to the icon that belongs to the given operator key.
     *
     * @param operatorKey
     *            the key of the operator
     * @return the path to icon for the given operator key
     */
    public static String getIconNameForOperator(String operatorKey) {
        if (operatorKey == null) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve icon name for null operatorKey!");
            return null;
        }
        // operator keys in the documentation begin with "operator.", so remove that
        int index = operatorKey.indexOf(".");
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        OperatorDescription operatorDescription = OperatorService.getOperatorDescription(operatorKey);
        if (operatorDescription == null) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve icon name for null operator with key " + operatorKey);
            return null;
        }
        String iconName = operatorDescription.getIconName();
        if (iconName != null) {
            return SwingTools.getIconPath("24/" + iconName);
        } else {
            // Operator has no icon
            return SwingTools.getIconPath("24/" + REPLACEMENT_FOR_OPERATORS_WITHOUT_ICON);
        }
    }

    public static String getIconNameForOperatorSmall(String operatorKey) {
        if (operatorKey == null) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve icon name for null operatorKey!");
            return null;
        }
        // operator keys in the documentation begin with "operator.", so remove that
        int index = operatorKey.indexOf(".");
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        OperatorDescription operatorDescription = OperatorService.getOperatorDescription(operatorKey);
        if (operatorDescription == null) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve icon name for null operator with key " + operatorKey);
            return null;
        }
        return SwingTools.getIconPath("16/" + operatorDescription.getIconName());
    }

    public static String getOperatorNameForKey(String operatorKey) {
        OperatorDescription operatorDescription = OperatorService.getOperatorDescription(operatorKey);
        if (operatorDescription != null) {
            return operatorDescription.getName();
        } else {
            return null;
        }
    }

    public static String getModuleNameForOperator(String operatorKey) {
        OperatorDescription operatorDescription;
        int index = operatorKey.indexOf(".");
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        operatorDescription = OperatorService.getOperatorDescription(operatorKey);
        if (operatorDescription == null) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve module of a an operator without description with key {}", operatorKey);
            return null;
        }
        return operatorDescription.getModule();
    }

    /**
     * Gets the {@link ParameterType} of the given operator key and parameter name as an i18n
     * string. This is used if no type is specified in the documentation xml.
     *
     * @param operatorKey
     *            The key of the operator
     * @param parameterName
     *            The name of the parameter
     * @return An i18n string containing the type, if one can be found. Empty string else.
     */
    public static String getParameterType(String operatorKey, String parameterName) {
        Operator operator = null;
        int index = operatorKey.indexOf(".");
        // remove operator group if existent
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        operator = getOperator(operatorKey);
        if (operator != null) {
            Parameters parameters = operator.getParameters();
            if (parameters != null) {
                ParameterType type = parameters.getParameterType(parameterName);
                if (type != null) {
                    if (type instanceof ParameterTypeNumber numberType) {

                        if (numberType instanceof ParameterTypeInt) {
                            return INTEGER_LABEL;
                        } else if (numberType instanceof ParameterTypeLong) {
                            return LONG_LABEL;
                        } else if (numberType instanceof ParameterTypeDouble) {
                            return REAL_LABEL;
                        }
                    } else if (type instanceof ParameterTypeCategory || type instanceof ParameterTypeStringCategory) {
                        return SELECTION_LABEL;
                    } else if (type instanceof ParameterTypeString) {
                        return STRING_LABEL;
                    } else if (type instanceof ParameterTypeList) {
                        return LIST_LABEL;
                    } else if (type instanceof ParameterTypeEnumeration) {
                        return ENUMERATION_LABEL;
                    } else if (type instanceof ParameterTypeBoolean) {
                        return BOOLEAN_LABEL;
                    } else {
                        return OTHER_LABEL;
                    }
                }
            }
        }
        return "";

    }

    /**
     * Gets the ParameterRange of the given operator key and parameter name as a string. This is
     * used if no range is specified in the documentation xml.
     *
     * @param operatorKey
     *            The key of the operator
     * @param parameterName
     *            The name of the parameter
     * @return A string containing the range, if one can be found. Empty string else.
     */
    public static String getParameterRange(String operatorKey, String parameterName) {
        Operator operator = null;
        int index = operatorKey.indexOf(".");
        // remove operator group if existent
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        operator = getOperator(operatorKey);
        if (operator != null) {
            Parameters parameters = operator.getParameters();
            if (parameters != null) {
                ParameterType type = parameters.getParameterType(parameterName);
                if (type != null) {
                    if (type instanceof ParameterTypeNumber numberType) {
                        StringBuilder range = new StringBuilder();
                        if (numberType instanceof ParameterTypeInt) {
                            int min = ((ParameterTypeInt) numberType).getMinValueInt();
                            int max = ((ParameterTypeInt) numberType).getMaxValueInt();

                            if (min == -Integer.MAX_VALUE || min == Integer.MIN_VALUE) {
                                range.append("-\u221E");
                            } else {
                                range.append(min);
                            }
                            range.append(" - ");
                            if (max == Integer.MAX_VALUE) {
                                range.append("+\u221E");
                            } else {
                                range.append(max);
                            }
                        } else if (numberType instanceof ParameterTypeLong) {
                            long min = ((ParameterTypeLong) numberType).getMinValuelong();
                            long max = ((ParameterTypeLong) numberType).getMaxValuelong();

                            if (min == -Long.MAX_VALUE || min == Long.MIN_VALUE) {
                                range.append("-\u221E");
                            } else {
                                range.append(min);
                            }
                            range.append(" - ");
                            if (max == Long.MAX_VALUE) {
                                range.append("+\u221E");
                            } else {
                                range.append(max);
                            }
                        } else if (numberType instanceof ParameterTypeDouble) {
                            double min = numberType.getMinValue();
                            double max = numberType.getMaxValue();

                            if (min == Double.NEGATIVE_INFINITY) {
                                range.append("-\u221E");
                            } else {
                                range.append(min);
                            }
                            range.append(" - ");
                            if (max == Double.POSITIVE_INFINITY) {
                                range.append("+\u221E");
                            } else {
                                range.append(max);
                            }
                        }
                        return range.toString();
                    } else if (type instanceof ParameterTypeCategory categoryType) {
                        int number = categoryType.getNumberOfCategories();

                        StringBuilder values = new StringBuilder();
                        for (int i = 0; i < number && i < MAX_CATEROGIES_DISPLAYED_IN_HELP; i++) {
                            if (i > 0) {
                                values.append(", ");
                            }
                            values.append(categoryType.getCategory(i));
                        }
                        if (number > MAX_CATEROGIES_DISPLAYED_IN_HELP) {
                            values.append(", ...");
                        }
                        return values.toString();
                    } else if (type instanceof ParameterTypeStringCategory stringCategoryType) {
                        String[] caterogies = stringCategoryType.getValues();
                        int number = caterogies.length;

                        StringBuilder values = new StringBuilder();
                        for (int i = 0; i < number && i < MAX_CATEROGIES_DISPLAYED_IN_HELP; i++) {
                            if (i > 0) {
                                values.append(", ");
                            }
                            values.append(caterogies[i]);
                        }
                        if (number > MAX_CATEROGIES_DISPLAYED_IN_HELP) {
                            values.append(", ...");
                        }
                        return values.toString();
                    } else {
                        return type.getRange();
                    }

                }
            }
        }
        return "";
    }

    /**
     * Gets the default value of the given operator key and parameter name as a string. This is used
     * if no default value is specified in the documentation xml.
     *
     * @param operatorKey
     *            The key of the operator
     * @param parameterName
     *            The name of the parameter
     * @return A string containing the default value, if one can be found. Empty string else.
     */
    public static String getParameterDefault(String operatorKey, String parameterName) {
        Operator operator = null;
        int index = operatorKey.indexOf(".");
        // remove operator group if existent
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }
        operator = getOperator(operatorKey);
        if (operator != null) {
            Parameters parameters = operator.getParameters();
            if (parameters != null) {
                ParameterType type = parameters.getParameterType(parameterName);
                if (type != null) {
                    if (type.getDefaultValueAsRawString() != null && !type.getDefaultValueAsRawString().trim().isEmpty()) {
                        return type.getDefaultValueAsRawString();
                    }
                }
            }
        }
        return "";
    }

    private static Operator getOperator(String operatorKey) {
        synchronized (OPERATOR_CACHE) {
            if (!OPERATOR_CACHE.containsKey(operatorKey)) {
                OperatorDescription description = OperatorService.getOperatorDescription(operatorKey);
                if (description == null) {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Tried to retrieve documentation information of a an operator without description with key {}",
                            operatorKey);
                    return null;
                }
                Operator operator = null;
                try {
                    operator = OperatorService.createOperator(description);
                } catch (OperatorCreationException e) {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Could not create operator instance for operator {}", description, e);
                }
                OPERATOR_CACHE.put(operatorKey, operator);
                return operator;
            }
            return OPERATOR_CACHE.get(operatorKey);
        }
    }

    /**
     * Returns if the given parameter is optional.
     *
     * @param operatorKey
     *            The key of the operator
     * @param parameterName
     *            The name of the parameter
     * @return {@code true} if the parameter is optional. {@code false} if the parameter is not
     *         optional or if no parameter exists for the given name and operator.
     */
    public static boolean isParameterOptional(String operatorKey, String parameterName) {
        Operator operator = null;
        int index = operatorKey.indexOf(".");
        if (index != -1) {
            operatorKey = operatorKey.substring(index + 1);
        }


        operator = getOperator(operatorKey);
        if (operator != null) {
            Parameters parameters = operator.getParameters();
            if (parameters != null) {
                ParameterType type = parameters.getParameterType(parameterName);
                if (type != null) {
                    return type.isOptional();
                }
            }
        }
        return false;
    }

    /**
     * Searches for a class with the given name and returns the path of the resource. Used for the
     * images of the ports' data types.
     *
     * @param type
     *            the class' name as String
     * @return the path of the resource of the corresponding icon.
     */
    public static String getIconNameForType(String type) {
        Class<? extends IOObject> clazz = IOObjectService.getClassByClassName(type);
        if (clazz == null)
            return SwingTools.getIconPath("24/" + DEFAULT_IOOBJECT_ICON_NAME);
        Icons icons = IOObjectGUIService.getIcons(clazz);
        if (icons == null)
            return SwingTools.getIconPath("24/" + DEFAULT_IOOBJECT_ICON_NAME);
        return icons.getIconName();
    }
}
