/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.documentation;


import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.WebServiceTools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This class loads the operator's help texts. Operator help texts are located in resources under
 * {@link #DOCUMENTATION_PACKAGE}. There each module has its own subpackage in that all
 * operator-keys have their own file. A file can be either .xml in a specific format or generic
 * html.<br>
 * XML format will be prefered over HTML format.
 *
 * @author Sebastian Land
 *
 */
public class OperatorDocumentationService {

    private static final Logger log = LogManager.getLogger(OperatorDocumentationService.class);
    private static final String DOCUMENTATION_PACKAGE = "com/owc/singularity/resources/documentation/";
    public static final String DEFAULT_IOOBJECT_ICON_NAME = "question_blue.png";
    /**
     * The documentation cache. It is used to cache documentations after reading them for the first
     * time.
     */
    private static final Map<OperatorDescription, OperatorDocumentation> OPERATOR_DOCUMENTATION_CACHE = new ConcurrentHashMap<>(100);

    public static void init() {
        OperatorService.getOperatorKeys()
                .parallelStream()
                .map(OperatorService::getOperatorDescription)
                .forEach(description -> OPERATOR_DOCUMENTATION_CACHE.put(description, OperatorDocumentationService.getDocumentationFromXML(description)));
    }

    public static List<String> getTags(OperatorDescription operatorDescription) {
        return getDocumentation(operatorDescription).tags;
    }

    /**
     * Gets the operator documentation as an HTML string.
     * <p>
     * The documentation sources will be used in the following order: 1. Documentation Cache 2.
     * Single-XML documentation format (for each operator) 3. Single-HTML file
     * <p>
     * Previously uncached documentations will be cached after reading for a better response time.
     * If something goes wrong, a log message will be triggered.
     *
     * @param operatorDescription
     *            The operator from that to get the documentation. Must not be null.
     * @return HTML string of the operator documentation
     */
    public static String getHtmlDescription(OperatorDescription operatorDescription) {
        return getDocumentation(operatorDescription).html;
    }

    private static OperatorDocumentation getDocumentation(OperatorDescription operatorDescription) {
        // first try xml format
        return OPERATOR_DOCUMENTATION_CACHE.computeIfAbsent(operatorDescription, OperatorDocumentationService::getDocumentationFromXML);
    }

    private static OperatorDocumentation getDocumentationFromXML(OperatorDescription operatorDescription) {
        URL resourceURL = getResourcePathOfXMLDoc(operatorDescription);
        if (resourceURL != null) {
            try (InputStream xmlStream = WebServiceTools.openStreamFromURL(resourceURL)) {
                Source xmlSource = new StreamSource(xmlStream);
                String html = OperatorDocumentationConverter.applyXSLTTransformation(xmlSource);
                if (html != null) {
                    html = html.replace("xmlns:rmdoc=\"com.owc.singularity.studio.gui.OperatorDocumentationBrowser\"", " ");
                    Pattern tagPattern = Pattern.compile("(?:<span.*?class=\"tags\".*?>)(.*?)(?:<\\/span>)");
                    Matcher pageMatcher = tagPattern.matcher(html);
                    List<String> tags = pageMatcher.results().map(matcher -> matcher.group(1)).collect(Collectors.toList());
                    return new OperatorDocumentation(html, tags);
                }
            } catch (Exception e) {
                log.atWarn()
                        .withThrowable(e)
                        .withMarker(LogService.MARKER_DEVELOPMENT)
                        .log("Failed to load XML documentation from {}, using fallback...", resourceURL);
            }
        } else {
            log.warn(LogService.MARKER_DEVELOPMENT, "The documentation of the following operator was not found: {}", operatorDescription);
        }
        return new OperatorDocumentation(getErrorText(operatorDescription), Collections.emptyList());
    }


    /**
     * Generates a HTML string which contains an error text for the case that an operator
     * documentation was not found.
     *
     * @param operatorDesc
     *            The operator which documentation not was found
     * @return An error HTML which says, that no documentation for the operator was found
     */
    private static String getErrorText(OperatorDescription operatorDesc) {
        String opName;
        if (operatorDesc != null) {
            opName = operatorDesc.getName();
        } else {
            opName = "unnamed";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"en\" xml:lang=\"en\"><head><table cellpadding=0 cellspacing=0><tr><td><img src=\"");
        builder.append(SwingTools.getIconPath("48/bug_error.png"));
        builder.append("\"/></td><td width=\"5\"></td><td>");
        builder.append(I18N.getErrorMessage("documentation.could_not_find", opName));
        builder.append("</td></tr></table></head></html>");
        return builder.toString();
    }


    /**
     * Returns the resource path to the operator_name.xml of the given operator
     *
     * @param opDesc
     *            the operatorDescription
     * @return the resource path
     */
    public static URL getResourcePathOfXMLDoc(OperatorDescription opDesc) {
        String documentationRoot = DOCUMENTATION_PACKAGE + opDesc.getModule() + "/";
        String key = opDesc.getKey();

        String opDescXMLResourcePath = documentationRoot + key + ".xml";
        return ModuleService.getMajorClassLoader().getResource(opDescXMLResourcePath);
    }

    private record OperatorDocumentation(String html, List<String> tags) {

    }

}
