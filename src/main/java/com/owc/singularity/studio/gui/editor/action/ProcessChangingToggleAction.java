package com.owc.singularity.studio.gui.editor.action;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.ToggleAction;

public abstract class ProcessChangingToggleAction extends ToggleAction {

    {
        setCondition(Condition.PROCESS_IS_READ_ONLY, ConditionReaction.DISALLOWED);
    }

    public ProcessChangingToggleAction(boolean smallIcon, String key, Object... args) {
        super(smallIcon, key, args);
    }

    public ProcessChangingToggleAction(boolean smallIcon, String key, IconType iconType, Object... args) {
        super(smallIcon, key, iconType, args);
    }

    public boolean isFocusedProcessReadOnly() {
        return MainFrame.INSTANCE.getMainProcessPanel() != null && MainFrame.INSTANCE.getMainProcessPanel().isReadOnly();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled && isFocusedProcessReadOnly()) {
            if (super.enabled) {
                super.setEnabled(false);
            }
            return;
        }
        super.setEnabled(enabled);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && !isFocusedProcessReadOnly();
    }
}
