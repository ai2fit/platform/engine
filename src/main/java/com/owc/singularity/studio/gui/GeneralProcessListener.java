/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui;


import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.PipelineExecutionListener;


/**
 * This listener must be created after the GUI is set up. It will then register automatically with
 * every newly created or opened process.
 * 
 * @author Simon Fischer
 * 
 */
public abstract class GeneralProcessListener implements PipelineExecutionListener {

    private AbstractPipeline process;

    public GeneralProcessListener() {
        register();
    }

    private void register() {
        if (!SingularityEngine.getExecutionMode().isHeadless()) {
            MainFrame.INSTANCE.addFocusedProcessChangedListener((oldPanel, currentlyShownProcess) -> {
                if (GeneralProcessListener.this.process != null) {
                    GeneralProcessListener.this.process.getRootOperator().removePipelineListener(GeneralProcessListener.this);
                }
                GeneralProcessListener.this.process = currentlyShownProcess != null ? currentlyShownProcess.getProcess() : null;
                if (GeneralProcessListener.this.process != null) {
                    GeneralProcessListener.this.process.getRootOperator().addPipelineListener(GeneralProcessListener.this);
                }
            });
        }
    }

}
