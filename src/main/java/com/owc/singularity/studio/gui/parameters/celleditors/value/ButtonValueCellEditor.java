package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.awt.Component;
import java.awt.Insets;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeButton;

/**
 * Defines the cell editor for the {@link ParameterTypeButton}
 *
 * @author Hatem Hamad
 */
public class ButtonValueCellEditor extends AbstractCellEditor implements PropertyValueCellEditor {

    private static final long serialVersionUID = -4046860164465118975L;

    private final JButton button;

    public ButtonValueCellEditor(ParameterTypeButton parameter) {
        button = new JButton(parameter.getAction());
        button.setMargin(new Insets(0, 0, 0, 0));
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    @Override
    public void setOperator(Operator operator) {}

    @Override
    public boolean useEditorAsRenderer() {
        return true;
    }

    @Override
    public boolean rendersLabel() {
        return true;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return button;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return getTableCellEditorComponent(table, value, isSelected, row, column);
    }
}
