/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.studio.gui.MainFrame;


/**
 * Start the corresponding action.
 * 
 * @author Tobias Malbrecht
 */
public class ToggleAllBreakpointsItem extends ProcessChangingToggleAction {

    private static final long serialVersionUID = 1727841552148351670L;


    public ToggleAllBreakpointsItem() {
        super(true, "toggle_all_breakpoints");
    }

    @Override
    public void actionToggled(ActionEvent e) {
        if (MainFrame.INSTANCE.getMainProcessPanel() == null)
            return;
        Operator rootOp = MainFrame.INSTANCE.getMainProcessPanel().getActions().getRootOperator();
        if (rootOp != null) {
            for (Operator op : ((OperatorChain) rootOp).getAllInnerOperators()) {
                op.setBreakpoint(BreakpointListener.BREAKPOINT_BEFORE, false);
                op.setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, isSelected());
            }
        }
    }
}
