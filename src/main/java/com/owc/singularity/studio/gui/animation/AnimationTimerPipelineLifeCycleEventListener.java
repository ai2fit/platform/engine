/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.animation;


import java.awt.event.ActionListener;

import javax.swing.Timer;

import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.studio.gui.MainFrame;


/**
 * A {@link PipelineLifeCycleEventListener} that periodically checks for redraws for animations if
 * the process is running.
 *
 * @author Gisa Schaefer
 * @since 7.1.0
 */
class AnimationTimerPipelineLifeCycleEventListener implements PipelineLifeCycleEventListener {

    @Override
    public String getIdentifier() {
        return "pipeline-animation-timer";
    }

    /**
     * The interval for redrawing the animations (in milliseconds).
     */
    private static final int REPAINT_INTERVAL = 60;

    private final Timer timer;

    AnimationTimerPipelineLifeCycleEventListener() {
        ActionListener timerListener = e -> {
            if (ProcessAnimationManager.INSTANCE.isRepaintRequired()) {
                MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().getModel().fireMiscChanged();
            }
        };
        timer = new Timer(REPAINT_INTERVAL, timerListener);
    }

    @Override
    public void onAfterPipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        startTimer();
    }

    @Override
    public void onAfterPipelineResume(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        startTimer();
    }

    @Override
    public void onAfterPipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
        startTimer();
    }

    /**
     * Starts the timer.
     */
    void startTimer() {
        timer.start();
    }

    /**
     * Stops the timer.
     */
    void stopTimer() {
        timer.stop();
    }
}
