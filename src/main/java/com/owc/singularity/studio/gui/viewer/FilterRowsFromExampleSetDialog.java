/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.viewer;


import static com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.Serial;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

import javax.swing.*;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExpressionFilter;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeExpression;
import com.owc.singularity.studio.gui.parameters.ExpressionParameterDialog;
import com.owc.singularity.studio.gui.tools.Icons;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceActionAdapter;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * A dialog for selecting the displayed rows for an
 * {@link com.owc.singularity.engine.object.data.exampleset.ExampleSet}. Used by the
 * {@link ExampleSetViewer} to let the user filter the displayed rows. This dialog supports both
 * selecting a range of examples (from -> to) and filtering examples by their values.
 *
 * @author Alexander Mahler
 */
public class FilterRowsFromExampleSetDialog extends ButtonDialog {

    // exclude custom_filters as an option because it requires an operator to function
    public static String[] KNOWN_CONDITION_NAMES_MINUS_CUSTOM_FILTER = Arrays.stream(KNOWN_CONDITION_NAMES)
            .filter(value -> !Objects.equals(value, "custom_filters"))
            .toArray(String[]::new);
    private final JTextField startRowField = new JTextField(10);
    private final JTextField endRowField = new JTextField(10);
    private final JCheckBox invertRangeSelectionCheckbox = new JCheckBox(new ResourceActionAdapter("data_view.invert_range_selection"));
    private final JTextField expressionField = new JTextField(10);
    private final JTextField valueFilterField = new JTextField(10);
    JComboBox<String> additionalFilterOptions = new JComboBox<>(KNOWN_CONDITION_NAMES_MINUS_CUSTOM_FILTER);
    private final JCheckBox invertRowFilterCheckbox = new JCheckBox(new ResourceActionAdapter("data_view.invert_filter"));
    private final JPanel filterPanel = new JPanel(new GridBagLayout());

    private final ExampleSetViewer exampleSetViewer;

    public FilterRowsFromExampleSetDialog(ExampleSetViewer exampleSetViewer, FilterableExampleSetTableModel.RowInformation rowInformation,
            String previousRegexValue, String previousSpecificValue) {
        super(exampleSetViewer != null ? SwingUtilities.getWindowAncestor(exampleSetViewer) : null, "filter_exampleset_rows", ModalityType.DOCUMENT_MODAL);
        this.exampleSetViewer = exampleSetViewer;
        filterPanel.setPreferredSize(new Dimension(150, 200));

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(4, 0, 4, 0);
        c.gridwidth = 1;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        JLabel labelStartRow = new ResourceLabel("data_view.start_Row");
        filterPanel.add(labelStartRow, c);

        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridx = 1;
        c.weightx = 1;
        c.gridy = 0;
        filterPanel.add(startRowField, c);
        labelStartRow.setLabelFor(startRowField);
        startRowField.setText(String.valueOf(rowInformation.startIndex + 1));

        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        JLabel labelEndRow = new ResourceLabel("data_view.end_Row");
        filterPanel.add(labelEndRow, c);

        c.gridx = 1;
        c.weightx = 1;
        filterPanel.add(endRowField, c);
        labelEndRow.setLabelFor(endRowField);
        endRowField.setText(String.valueOf(rowInformation.endIndex + 1));

        c.gridy = 2;
        filterPanel.add(invertRangeSelectionCheckbox, c);
        invertRangeSelectionCheckbox.setSelected(rowInformation.invertRangeSelection);
        c.gridy = 3;
        c.gridx = 0;
        c.weightx = 1;
        JLabel labelConditionFilter = new ResourceLabel("data_view.condition_filter_class");
        filterPanel.add(labelConditionFilter, c);

        c.gridx = 1;
        filterPanel.add(additionalFilterOptions, c);
        additionalFilterOptions.addItemListener(i -> toggleDependentFields(i.getItem()));
        if (rowInformation.additionalRowFilter != null) {
            String filterName = rowInformation.additionalRowFilter.getClass().getName();
            for (int i = 0; i < KNOWN_CONDITION_FILTERS.length; i++) {
                if (KNOWN_CONDITION_FILTERS[i].equals(filterName)) {
                    additionalFilterOptions.setSelectedItem(KNOWN_CONDITION_NAMES[i]);
                }
            }
        } else
            additionalFilterOptions.setSelectedIndex(CONDITION_ALL);
        toggleDependentFields(additionalFilterOptions.getSelectedItem());
        c.gridy = 4;
        filterPanel.add(expressionField, c);

        GridBagConstraints cc = new GridBagConstraints();
        cc.gridwidth = GridBagConstraints.REMAINDER;
        cc.weightx = 1;
        cc.weighty = 1;
        cc.anchor = GridBagConstraints.EAST;
        expressionField.setLayout(new GridBagLayout());
        JButton expressionDialogButton = new JButton(Icons.fromName("calculator.png").getIcon16x16());
        expressionField.add(expressionDialogButton, cc);
        expressionField.setText(previousRegexValue);
        expressionDialogButton.addActionListener(i -> {
            String initial = expressionField.getText();
            ExpressionParameterDialog dialog = new ExpressionParameterDialog(new ParameterTypeExpression("", ""), null, initial,
                    ExampleSetMetaData.of(exampleSetViewer.getOriginalExampleSet()));
            dialog.setVisible(true);
            if (dialog.isOk()) {
                expressionField.setText(dialog.getExpression());
            }
        });
        filterPanel.add(valueFilterField, c);

        valueFilterField.setText(previousSpecificValue);
        c.gridy = 5;
        invertRowFilterCheckbox.setSelected(rowInformation.invertRowFilter);
        filterPanel.add(invertRowFilterCheckbox, c);

        Collection<AbstractButton> buttons = new LinkedList<>();
        final JButton filterRowButton = new JButton(new ResourceAction("select_rows") {

            @Serial
            private static final long serialVersionUID = -87900679932752344L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                applyRowSelection();
            }
        });
        buttons.add(filterRowButton);
        final JButton resetRowFilterButton = new JButton(new ResourceAction("reset_row_filter") {

            @Serial
            private static final long serialVersionUID = -8716424597241669547L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                resetRowSelection();
            }
        });
        buttons.add(resetRowFilterButton);
        buttons.add(makeCloseButton());
        layoutDefault(filterPanel, buttons);
        getRootPane().setDefaultButton(filterRowButton);
    }

    /**
     * Toggles the visibility of the expression and additional value input field dependent on the
     * currently selected condition.
     */
    private void toggleDependentFields(Object item) {
        if (KNOWN_CONDITION_NAMES[CONDITION_ATTRIBUTE_VALUE_FILTER].equals(item)) {
            valueFilterField.setVisible(true);
            expressionField.setVisible(false);
        } else if (KNOWN_CONDITION_NAMES[CONDITION_EXPRESSION].equals(item)) {
            expressionField.setVisible(true);
            valueFilterField.setVisible(false);
        } else {
            valueFilterField.setVisible(false);
            expressionField.setVisible(false);
        }
        filterPanel.validate();
    }

    /**
     * Applies the current parameter selection.
     */
    private void applyRowSelection() {
        String startIndex = startRowField.getText().trim();
        if (startIndex.isEmpty()) {
            return;
        }
        String endIndex = endRowField.getText().trim();
        if (endIndex.isEmpty()) {
            return;
        }
        boolean invertRangeSelection = invertRangeSelectionCheckbox.isSelected();
        int startRow;
        int endRow;
        try {
            startRow = Integer.parseInt(startIndex);
            endRow = Integer.parseInt(endIndex);
        } catch (NumberFormatException e) {
            return;
        }
        endRow--;
        startRow--;
        if (startRow < 0) {
            startRowField.setText("1");
            startRow = 0;
        }
        if (endRow > exampleSetViewer.getOriginalNumberOfRows()) {
            endRow = exampleSetViewer.getOriginalNumberOfRows();
            endRowField.setText(String.valueOf(exampleSetViewer.getOriginalNumberOfRows() + 1));
        }
        int additionalFilterIndex = additionalFilterOptions.getSelectedIndex();
        String expression = expressionField.getText();
        String filteredValue = valueFilterField.getText();

        if (additionalFilterIndex == 0)
            exampleSetViewer.updateSelectedExamples(startRow, endRow, invertRangeSelection, expression, filteredValue);
        else {
            ExampleSetRowFilter condition;
            String className = (String) additionalFilterOptions.getSelectedItem();
            try {
                if (ExampleSetRowFilter.KNOWN_CONDITION_NAMES[CONDITION_EXPRESSION].equals(className)) {
                    // special handling for expression, has different
                    if (expression == null || expression.isEmpty()) {
                        return;
                    }
                    condition = new ExpressionFilter(expression, null);
                } else if (ExampleSetRowFilter.KNOWN_CONDITION_NAMES[CONDITION_ATTRIBUTE_VALUE_FILTER].equals(className)) {
                    if (filteredValue == null || filteredValue.isEmpty()) {
                        return;
                    }
                    condition = ExampleSetRowFilter.createCondition(className, filteredValue);
                } else {
                    condition = ExampleSetRowFilter.createCondition(className);
                }
            } catch (OperatorException e) {
                return;
            }
            boolean invertRowFilter = invertRowFilterCheckbox.isSelected();

            exampleSetViewer.updateSelectedExamples(startRow, endRow, invertRangeSelection, condition, invertRowFilter, expression, filteredValue);
        }
    }

    /**
     * Resets all dialog parameters to their default values applies the row selection
     */
    private void resetRowSelection() {
        startRowField.setText("1");
        int originalSize = exampleSetViewer.getOriginalNumberOfRows();
        endRowField.setText(String.valueOf(originalSize + 1));
        invertRangeSelectionCheckbox.setSelected(false);
        additionalFilterOptions.setSelectedIndex(0);
        invertRowFilterCheckbox.setSelected(false);
        expressionField.setText("");
        valueFilterField.setText("");
        applyRowSelection();
    }
}
