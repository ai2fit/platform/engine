package com.owc.singularity.studio.gui.repository.mount;

import java.util.Map;

import com.owc.singularity.repository.PlaceholderRepositoryMount;
import com.owc.singularity.studio.gui.Form;

public class PlaceholderMountConfigurationUIProvider implements MountConfigurationUIProvider {

    @Override
    public boolean supportsCreateUI() {
        return false;
    }

    @Override
    public Form<Map<String, String>> getCreateUI(String mountType) {
        throw new UnsupportedOperationException("There is no create UI for Placeholder mounts");
    }

    @Override
    public boolean supportsEditUI() {
        return true;
    }

    @Override
    public Form<Map<String, String>> getEditUI(String mountType, Map<String, String> options) {
        String templateType = PlaceholderRepositoryMount.getTemplateType(options);
        Map<String, String> templateOptions = PlaceholderRepositoryMount.getTemplateOptions(options);
        return RepositoryUIManager.getMountEditForm(templateType, templateOptions);
    }

    @Override
    public boolean isMountTypeSupported(String mountType) {
        return PlaceholderRepositoryMount.TYPE_NAME.equals(mountType);
    }
}
