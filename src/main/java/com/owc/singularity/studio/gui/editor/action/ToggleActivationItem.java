/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.event.ActionEvent;
import java.util.*;

import javax.swing.JMenuItem;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa, Tobias Malbrecht
 */
public class ToggleActivationItem extends ProcessChangingToggleAction {

    private static final long serialVersionUID = -7020868989225535479L;

    {
        setCondition(Condition.ROOT_SELECTED, ConditionReaction.DISALLOWED);
        setCondition(Condition.OPERATOR_SELECTED, ConditionReaction.MANDATORY);
    }

    public ToggleActivationItem() {
        super(true, "enable_operator");
    }

    @Override
    public void actionToggled(ActionEvent e) {
        if (MainFrame.INSTANCE.getMainProcessPanel() == null || !isEnabled())
            return;
        ProcessActions processActions = MainFrame.INSTANCE.getMainProcessPanel().getActions();
        List<Operator> selectedOperators = processActions.getSelectedOperators();
        if (selectedOperators.isEmpty()) {
            return;
        }
        boolean targetState = getTargetState(selectedOperators);

        if (!targetState) {
            final String disableBehavior = PropertyService.getParameterValue(StudioProperties.PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR);
            if ("bridged".equals(disableBehavior)) {
                // we need to find ports inside and outside and connect
                HashMap<MetaData, List<OutputPort>> insideConnections = new HashMap<>();
                HashMap<MetaData, List<InputPort>> outsideConnections = new HashMap<>();
                for (Operator operator : selectedOperators) {
                    for (InputPort inputPort : operator.getInputPorts().getAllPorts()) {
                        if (inputPort.isConnected() && !selectedOperators.contains(inputPort.getSource().getPorts().getOwner().getOperator())) {
                            MetaData inputMetaData = inputPort.getMetaData();
                            insideConnections.computeIfAbsent(inputMetaData, m -> new LinkedList<>()).add(inputPort.getSource());
                            inputPort.getSource().lock();
                        }
                    }
                    for (OutputPort outputPort : operator.getOutputPorts().getAllPorts()) {
                        if (outputPort.isConnected() && !selectedOperators.contains(outputPort.getDestination().getPorts().getOwner().getOperator())) {
                            MetaData inputMetaData = outputPort.getMetaData();
                            outsideConnections.computeIfAbsent(inputMetaData, m -> new LinkedList<>()).add(outputPort.getDestination());
                            outputPort.getDestination().lock();
                        }
                    }
                }

                // we remove all ports to the outside
                for (Operator operator : selectedOperators) {
                    operator.getInputPorts().disconnectAllBut(selectedOperators);
                    operator.getOutputPorts().disconnectAllBut(selectedOperators);
                }
                // we restore inside outside connections by bridging
                for (Map.Entry<MetaData, List<OutputPort>> entry : insideConnections.entrySet()) {
                    List<InputPort> possibleTargets = outsideConnections.get(entry.getKey());
                    if (possibleTargets != null) {
                        Iterator<OutputPort> sourceIterator = entry.getValue().iterator();
                        Iterator<InputPort> targetIterator = possibleTargets.iterator();
                        while (sourceIterator.hasNext() && targetIterator.hasNext()) {
                            sourceIterator.next().connectTo(targetIterator.next());
                        }
                        // unlock ports again
                        possibleTargets.forEach(in -> in.unlock());
                    }
                    entry.getValue().forEach(out -> out.unlock());
                }
            } else {
                // we remove all ports to the outside
                for (Operator operator : selectedOperators) {
                    operator.getInputPorts().disconnectAllBut(selectedOperators);
                    operator.getOutputPorts().disconnectAllBut(selectedOperators);
                }
            }
        }

        for (Operator op : selectedOperators) {
            op.setEnabled(targetState);
        }
    }

    public JMenuItem createMultipleActivationItem(Collection<Operator> selectedOperators) {
        boolean targetState = getTargetState(selectedOperators);
        String actionKey = targetState ? "enable_operator_multiple" : "disable_operator_multiple";
        return new JMenuItem(new ResourceAction(actionKey, selectedOperators.size()) {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                actionToggled(e);
            }
        });
    }

    private boolean getTargetState(Collection<Operator> selectedOperators) {
        if (selectedOperators == null || selectedOperators.isEmpty()) {
            return false;
        }
        for (Operator op : selectedOperators) {
            if (op.isEnabled()) {
                return false;
            }
        }
        return true;
    }
}
