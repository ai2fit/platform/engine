/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.tree;


import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.export.PrintableComponent;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * @author Tobias Malbrecht
 */
public class OperatorTreeDockable extends JPanel implements Dockable, PrintableComponent {

    private static final long serialVersionUID = 1L;

    private final OperatorTree operatorTree;

    public OperatorTreeDockable() {
        operatorTree = new OperatorTree();

        setLayout(new BorderLayout());

        JScrollPane scrollPane = new ExtendedJScrollPane(operatorTree);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(scrollPane, BorderLayout.CENTER);
    }

    private final DockKey DOCK_KEY = new ResourceDockKey("operator_tree");

    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    private Component component;

    @Override
    public Component getComponent() {
        if (component == null) {
            component = this;
        }
        return component;
    }

    @Override
    public Component getExportComponent() {
        return this;
    }

    @Override
    public String getExportIconName() {
        return I18N.getGUIMessage("gui.dockkey.operator_tree.icon");
    }

    @Override
    public String getExportName() {
        return I18N.getGUIMessage("gui.dockkey.operator_tree.name");
    }

    @Override
    public String getIdentifier() {
        AbstractPipeline process = MainFrame.INSTANCE.getMainProcessPanel().getProcess();
        if (process != null) {
            RepositoryPath processLocation = process.getPath();
            if (processLocation != null) {
                return processLocation.toString();
            }
        }
        return null;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }


    public OperatorTree getOperatorTree() {
        return operatorTree;
    }
}
