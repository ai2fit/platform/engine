/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.connection;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.connection.ConnectionDefinitionRegistry;
import com.owc.singularity.repository.connection.ConnectionParameterDefinition;
import com.owc.singularity.repository.connection.ConnectionParameters;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.connection.actions.SaveConnectionAction;
import com.owc.singularity.studio.gui.look.icons.IconFactory;
import com.owc.singularity.studio.gui.tools.*;


/**
 * Dialog when a new connection should be created.
 *
 * @author Marco Boeck
 * @since 9.3.0
 */
public class ConnectionCreateDialog extends JDialog {

    private static final long serialVersionUID = 2229442876428540265L;

    private enum Status {
        NO_STATUS,

        INFO,

        WORKING,

        WARNING
    }

    private static final String I18N_KEY = "connection.create_new_connection";
    private static final ImageIcon WARNING_ICON = SwingTools.createIcon("16/" + I18N.getGUILabel("connection.warning.icon"));
    private static final ImageIcon INFORMATION_ICON = SwingTools.createIcon("16/" + I18N.getGUILabel("connection.information.icon"));
    private static final ImageIcon WORKING_ICON = SwingTools.createIcon("16/" + I18N.getGUILabel("connection.working.icon"));

    private final JTextField nameField;
    private final JComboBox<String> typeBox;
    private final JLabel nameErrorLabel;
    private final JLabel typeErrorLabel;
    private final JLabel repositoryErrorLabel;
    private final JLabel statusIcon;
    private final JTextArea statusLabel;
    private final JButton nextButton;
    private final AtomicBoolean cancelled;

    /**
     * Creates a new dialog instance that might be used to create a new Connection Object inside the
     * given directory.
     *
     * @param parent
     *            the parent, can be {@code null}
     * @param targetDirectory
     *            the path to a directory where the new Connection object should be stored; must not
     *            be {@code null}
     */
    public ConnectionCreateDialog(Window parent, final RepositoryPath targetDirectory) {
        super(parent, I18N.getGUIMessage("gui.dialog." + I18N_KEY + ".title"), Dialog.ModalityType.APPLICATION_MODAL);

        this.cancelled = new AtomicBoolean(false);

        JPanel mainPanel = new JPanel(new BorderLayout());

        JPanel configPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();


        // info label
        JPanel infoPanel = new JPanel(new GridBagLayout());
        gbc.insets = new Insets(20, 20, 5, 20);
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        infoPanel.add(new JLabel(I18N.getGUILabel("connection.create_new.info.label")), gbc);
        mainPanel.add(infoPanel, BorderLayout.NORTH);

        gbc.weightx = 0;
        gbc.gridx = 0;
        gbc.gridy = 0;
        // connection type chooser
        gbc.gridy += 1;
        gbc.insets = new Insets(20, 20, 5, 20);
        configPanel.add(new JLabel(I18N.getGUILabel("connection.create_new.type.label")), gbc);

        Vector<String> types = new Vector<>(ConnectionDefinitionRegistry.getInstance().getAllTypes());
        types.sort(Comparator.comparing(ConnectionI18N::getTypeName));
        typeBox = new JComboBox<>(types);
        typeBox.setRenderer(new ConnectionListCellRenderer());
        gbc.gridx += 1;
        configPanel.add(typeBox, gbc);

        typeErrorLabel = new JLabel(IconFactory.getEmptyIcon16x16());
        typeErrorLabel.setHorizontalAlignment(SwingConstants.LEFT);
        typeErrorLabel.setIconTextGap(10);
        gbc.gridy += 1;
        gbc.insets = new Insets(0, 20, 5, 20);
        configPanel.add(typeErrorLabel, gbc);


        repositoryErrorLabel = new JLabel(IconFactory.getEmptyIcon16x16());
        repositoryErrorLabel.setHorizontalAlignment(SwingConstants.LEFT);
        repositoryErrorLabel.setIconTextGap(10);
        gbc.gridy += 1;
        gbc.insets = new Insets(0, 20, 5, 20);
        configPanel.add(repositoryErrorLabel, gbc);

        // name field
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.insets = new Insets(0, 20, 5, 20);
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        configPanel.add(new JLabel(I18N.getGUILabel("connection.create_new.name.label")), gbc);

        nameField = new JTextField(25);
        nameField.setToolTipText(I18N.getGUILabel("connection.create_new.name.tip"));
        nameField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                nextButton.setEnabled(!validateFieldsAndReturnError());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                nextButton.setEnabled(!validateFieldsAndReturnError());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                nextButton.setEnabled(!validateFieldsAndReturnError());
            }
        });
        SwingTools.setPrompt(I18N.getGUILabel("connection.create_new.name.prompt"), nameField);
        gbc.gridx += 1;
        configPanel.add(nameField, gbc);

        nameErrorLabel = new JLabel(IconFactory.getEmptyIcon16x16());
        nameErrorLabel.setHorizontalAlignment(SwingConstants.LEFT);
        nameErrorLabel.setIconTextGap(10);
        gbc.gridy += 1;
        gbc.insets = new Insets(0, 20, 5, 20);
        configPanel.add(nameErrorLabel, gbc);

        // spacer
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        configPanel.add(new JLabel(), gbc);

        // button panel at bottom
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbcb = new GridBagConstraints();
        gbcb.gridx = 0;
        gbcb.gridy = 1;
        gbcb.weightx = 1;
        gbcb.fill = GridBagConstraints.HORIZONTAL;
        gbcb.insets = new Insets(10, 20, 10, 10);

        // Status
        GridBagConstraints gbcFullWidth = new GridBagConstraints();
        gbcFullWidth.fill = GridBagConstraints.BOTH;
        gbcFullWidth.weightx = 1;
        gbcFullWidth.gridwidth = GridBagConstraints.REMAINDER;
        gbcFullWidth.insets = (Insets) gbcb.insets.clone();
        gbcFullWidth.insets.bottom = 0;
        JPanel warningPanel = new JPanel(new GridBagLayout());

        statusIcon = new JLabel(IconFactory.getEmptyIcon16x16());
        statusLabel = new JTextArea(2, 20);
        statusLabel.setMinimumSize(new Dimension(20, 40));
        statusLabel.setBackground(null);
        statusLabel.setLineWrap(true);
        statusLabel.setWrapStyleWord(true);
        statusLabel.setBorder(BorderFactory.createEmptyBorder());
        statusLabel.setEditable(false);
        GridBagConstraints gbcStatus = new GridBagConstraints();
        gbcStatus.anchor = GridBagConstraints.NORTH;
        warningPanel.add(statusIcon, gbcStatus);
        JScrollPane scrollPane = new ExtendedJScrollPane(statusLabel);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.setPreferredSize(new Dimension(1, 48));
        gbcStatus.fill = GridBagConstraints.HORIZONTAL;
        gbcStatus.insets.left = 10;
        gbcStatus.weightx = 1.0;
        warningPanel.add(scrollPane, gbcStatus);
        buttonPanel.add(warningPanel, gbcFullWidth);
        buttonPanel.add(new JLabel(), gbcb);

        final ResourceAction nextAction = new ResourceAction("connection.create_new.create") {

            private static final long serialVersionUID = 1221270171727198622L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                if (validateFieldsAndReturnError()) {
                    return;
                }

                nextButton.setEnabled(false);
                updateStatus(Status.WORKING, I18N.getGUILabel("connection.create_new.status.working"));

                String name = nameField.getText();
                String type = String.valueOf(typeBox.getSelectedItem());

                new ProgressThread(SaveConnectionAction.PROGRESS_THREAD_ID_PREFIX, false, name) {

                    @Override
                    public void run() {
                        try {
                            RepositoryPath location = targetDirectory.resolve(name);


                            try {
                                if (Files.exists(location)) {
                                    SwingTools.invokeLater(() -> {
                                        nextButton.setEnabled(true);
                                        nameErrorLabel.setText(I18N.getGUILabel("connection.create_new.error.name_duplicate"));
                                        nameErrorLabel.setIcon(INFORMATION_ICON);
                                        updateStatus(Status.NO_STATUS, null);
                                    });
                                    return;
                                }

                                // check if user cancelled dialog in the meantime
                                if (ConnectionCreateDialog.this.cancelled.get()) {
                                    return;
                                }

                                // check if user cancelled dialog in the meantime
                                if (ConnectionCreateDialog.this.cancelled.get()) {
                                    return;
                                }
                                ConnectionParameterDefinition<?> connectionParameterDefinition = ConnectionDefinitionRegistry.getInstance()
                                        .getDefinitionOfType(type);
                                ConnectionParameters parameters = connectionParameterDefinition.create(name, null, null, null);
                                ConnectionParametersIOObject connectionParametersIOObject = new ConnectionParametersIOObject(parameters);
                                Entries.write(location, MetaData.forIOObject(connectionParametersIOObject), connectionParametersIOObject);
                                SwingTools.invokeLater(() -> {
                                    ConnectionCreateDialog.this.dispose();
                                    ConnectionEditDialog connectionEditDialog = new ConnectionEditDialog(parent, location, parameters, true);
                                    // open up the first relevant tab if possible
                                    connectionEditDialog.showTab(1);

                                    connectionEditDialog.setVisible(true);
                                    // scroll to connection location
                                    MainFrame.INSTANCE.getRepositoryBrowser().getRepositoryTree().expandAndSelectIfExists(location);

                                });
                            } catch (Exception e) {
                                LogService.getRoot().error("com.owc.singularity.studio.gui.connection.ConnectionCreationDialog.creation_failed", e);
                                SwingTools.invokeLater(() -> {
                                    nextButton.setEnabled(true);
                                    updateStatus(Status.WARNING, I18N.getGUILabel("connection.create_new.status.failed", e.getMessage()));
                                });
                            }
                        } catch (IllegalArgumentException e1) {
                            // should not happen as it is validated before, but you never know
                            SwingTools.invokeLater(() -> {
                                nextButton.setEnabled(true);
                                nameErrorLabel.setText(e1.getMessage());
                                nameErrorLabel.setIcon(WARNING_ICON);
                                updateStatus(Status.NO_STATUS, null);
                            });
                            return;
                        }
                    }
                }.start();
            }
        };
        gbcb.weightx = 0;
        gbcb.insets = new Insets(10, 0, 10, 10);
        gbcb.gridx += 1;
        nextButton = new JButton(nextAction);
        buttonPanel.add(nextButton, gbcb);

        final ResourceAction cancelAction = new ResourceAction("connection.create_new.cancel") {

            private static final long serialVersionUID = 2706198398278997200L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                cancelled.set(true);
                dispose();
            }
        };
        gbcb.gridx += 1;
        buttonPanel.add(new JButton(cancelAction), gbcb);

        mainPanel.add(configPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // close dialog with ESC
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "CLOSE");
        getRootPane().getActionMap().put("CLOSE", cancelAction);
        // next press with ENTER
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "NEXT");
        getRootPane().getActionMap().put("NEXT", nextAction);

        setContentPane(mainPanel);
        setSize(new Dimension(600, 400));
        setLocationRelativeTo(getOwner());

        ActionStatisticsCollector.getInstance().log(ActionStatisticsCollector.TYPE_DIALOG, I18N_KEY, "open");
    }

    /**
     * Updates the status message field. Make sure to call on the EDT.
     *
     * @param status
     *            the status, changes the displayed icon. If {@code Status#NO_STATUS}, no icon will
     *            be displayed.
     * @param message
     *            the message to display, can be {@code null}
     */
    private void updateStatus(Status status, String message) {
        switch (status) {
            case NO_STATUS:
                statusIcon.setIcon(IconFactory.getEmptyIcon16x16());
                break;
            case INFO:
                statusIcon.setIcon(INFORMATION_ICON);
                break;
            case WORKING:
                statusIcon.setIcon(WORKING_ICON);
                break;
            case WARNING:
                statusIcon.setIcon(WARNING_ICON);
                break;
        }
        statusLabel.setText(message);
        statusLabel.setToolTipText(message);
        statusIcon.revalidate();
    }

    /**
     * Validates the input fields and highlights potential errors.
     *
     * @return {@code true} if at least one error is contained in the config; {@code false}
     *         otherwise
     */
    private boolean validateFieldsAndReturnError() {
        boolean error = false;
        String name = nameField.getText();
        if (name == null || name.trim().isEmpty()) {
            nameErrorLabel.setText(I18N.getGUILabel("connection.create_new.error.name_empty"));
            nameErrorLabel.setIcon(INFORMATION_ICON);
            error = true;
        } else if (!RepositoryGuiTools.isNameValid(name)) {
            nameErrorLabel.setText(
                    I18N.getGUIMessage("gui.dialog.repository_location.location_invalid_char.label", RepositoryGuiTools.getIllegalCharacterInName(name)));
            nameErrorLabel.setIcon(WARNING_ICON);
            error = true;
        } else {
            nameErrorLabel.setText("");
            nameErrorLabel.setIcon(IconFactory.getEmptyIcon16x16());
        }
        String type = (String) typeBox.getSelectedItem();
        if (type == null) {
            typeErrorLabel.setText(I18N.getGUILabel("connection.create_new.error.type_empty"));
            typeErrorLabel.setIcon(INFORMATION_ICON);
            error = true;
        } else {
            typeErrorLabel.setText("");
            typeErrorLabel.setIcon(IconFactory.getEmptyIcon16x16());
        }

        return error;
    }
}
