package com.owc.singularity.studio.gui.editor.action;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;

public abstract class ProcessChangingAction extends ResourceAction {

    {
        setCondition(Condition.PROCESS_IS_READ_ONLY, ConditionReaction.DISALLOWED);
    }

    public ProcessChangingAction(String i18nKey, Object... i18nArgs) {
        super(i18nKey, i18nArgs);
    }


    public ProcessChangingAction(boolean smallIcon, String i18nKey, Object... i18nArgs) {
        super(smallIcon, i18nKey, i18nArgs);
    }


    public boolean isFocusedProcessReadOnly() {
        return MainFrame.INSTANCE.getMainProcessPanel() != null && MainFrame.INSTANCE.getMainProcessPanel().isReadOnly();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled && isFocusedProcessReadOnly()) {
            if (super.enabled) {
                super.setEnabled(false);
            }
            return;
        }
        super.setEnabled(enabled);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && !isFocusedProcessReadOnly();
    }
}
