package com.owc.singularity.studio.gui.repository.model;

import javax.swing.tree.DefaultTreeModel;

import com.owc.singularity.repository.RepositoryPath;

public abstract class AbstractRepositoryTreeModel extends DefaultTreeModel {

    public AbstractRepositoryTreeModel(RepositoryTreeNode root) {
        super(root);
    }

    public AbstractRepositoryTreeModel(RepositoryTreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
    }

    public abstract boolean containsNodeFor(RepositoryPath path);

    public boolean containsNodeFor(RepositoryTreeNode node) {
        return containsNodeFor(node.getRepositoryPath());
    }

    public abstract RepositoryTreeNode getNodeFor(RepositoryPath path);

    public RepositoryTreeNode getNodeFor(RepositoryTreeNode node) {
        return getNodeFor(node.getRepositoryPath());
    }

    public abstract int addNode(RepositoryTreeNode parent, RepositoryTreeNode node);

    public abstract void removeNode(RepositoryTreeNode node);

    @Override
    public RepositoryTreeNode getRoot() {
        return (RepositoryTreeNode) super.getRoot();
    }
}
