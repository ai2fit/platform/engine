/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.metadata;


import java.awt.Component;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.studio.gui.repository.RepositoryTreeNodeToolTipProvider;


/**
 * Provides a custom renderer added by the {@link RepositoryTreeNodeToolTipProvider} for subclasses
 * of {@link MetaData}.
 * 
 * @author Simon Fischer, Gabor Makrai
 * 
 */
public interface MetaDataRendererFactory {

    Class<? extends MetaData> getSupportedClass();

    Component createRenderer(MetaData metaData);
}
