package com.owc.singularity.studio.gui.vcs;

import java.awt.Color;
import java.awt.Component;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MountTreeDifference;
import com.owc.singularity.studio.gui.IOObjectGUIService;
import com.owc.singularity.studio.gui.repository.RepositoryTreeCellRenderer;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.vcs.model.*;
import com.owc.singularity.tools.AlphanumComparator;

public class MountTreeDiffTree extends JTree {

    public static final Color COLOR_LEFT_CHANGES = new Color(0, 255, 50, 30);
    public static final Color COLOR_RIGHT_CHANGES = new Color(0, 200, 255, 30);
    public static final Color COLOR_CONFLICT_CHANGES = new Color(255, 100, 50, 30);
    public static final Color COLOR_ADDITIONAL_CHANGES = new Color(161, 161, 161, 30);
    public static final Color COLOR_RESOLVED_CONFLICT_CHANGES = new Color(232, 222, 116, 30);

    private final Collection<RepositoryPath> conflictPaths;
    private final Collection<RepositoryPath> uncommittedPaths;
    private final Map<RepositoryPath, String> resolvedConflictPaths = new HashMap<>();
    private final EntryVersion leftVersion, rightVersion;
    private final MountTreeDiffTreeModel model;
    private final TreeLeafsComparator treeNodeSortComparator = new TreeLeafsComparator(
            Comparator.comparing(AbstractMountTreeDiffTreeNode::getName, new AlphanumComparator(AlphanumComparator.AlphanumCaseSensitivity.INSENSITIVE)));

    public MountTreeDiffTree(RepositoryPath mountPath, MountTreeDifference difference, Collection<RepositoryPath> uncommittedPaths) {
        super();
        this.leftVersion = difference.getLeft();
        this.rightVersion = difference.getRight();
        this.conflictPaths = new HashSet<>(difference.getConflictedPaths());
        this.uncommittedPaths = new HashSet<>(uncommittedPaths);
        this.model = new MountTreeDiffTreeModel(mountPath, difference, uncommittedPaths, treeNodeSortComparator);
        initUI();
    }

    public MountTreeDiffTree(RepositoryPath mountPath, EntryVersion firstVersion, EntryVersion secondVersion, Collection<RepositoryPath> conflictPaths,
            Collection<RepositoryPath> uncommittedPaths) {
        super();
        this.leftVersion = firstVersion;
        this.rightVersion = secondVersion;
        this.conflictPaths = new HashSet<>(conflictPaths);
        this.uncommittedPaths = new HashSet<>(uncommittedPaths);
        this.model = new MountTreeDiffTreeModel(mountPath, leftVersion, rightVersion, conflictPaths, uncommittedPaths, treeNodeSortComparator);
        initUI();
    }

    private void initUI() {
        model.addTreeModelListener(new ExpandAllTreeModelListener());
        setModel(model);
        setCellRenderer(new MountTreeDifferenceTreeCellRenderer(leftVersion, rightVersion, resolvedConflictPaths::get));
        setRootVisible(false);
        setShowsRootHandles(true);
        ToolTipManager.sharedInstance().registerComponent(this);
        updateTree();
    }

    public void setConflicts(Collection<RepositoryPath> conflicts) {
        if (this.conflictPaths.size() != conflicts.size() || !this.conflictPaths.containsAll(conflicts) || !conflicts.containsAll(this.conflictPaths)) {
            // conflicts have changed
            this.conflictPaths.clear();
            this.conflictPaths.addAll(conflicts);
            model.setConflicts(this.conflictPaths);
            update();
        }
    }

    public void setUncommitted(Collection<RepositoryPath> uncommittedPaths) {
        if (this.uncommittedPaths.size() != uncommittedPaths.size() || !this.uncommittedPaths.containsAll(uncommittedPaths)
                || !uncommittedPaths.containsAll(this.uncommittedPaths)) {
            // uncommitted have changed
            this.uncommittedPaths.clear();
            this.uncommittedPaths.addAll(uncommittedPaths);
            model.setUncommittedChanges(this.uncommittedPaths);
            update();
        }
    }

    public void addResolvedConflict(RepositoryPath conflictPath, String decisionDescription) {
        String oldDecisionDescription = resolvedConflictPaths.put(conflictPath, decisionDescription);
        if (oldDecisionDescription == null || !oldDecisionDescription.equals(decisionDescription)) {
            model.fireNodeChanged(conflictPath);
        }
    }

    public void removeResolvedConflict(RepositoryPath conflictPath) {
        String oldDecisionDescription = resolvedConflictPaths.remove(conflictPath);
        if (oldDecisionDescription != null) {
            model.fireNodeChanged(conflictPath);
        }
    }

    private void updateTree() {
        update();
        repaint();
    }

    private void update() {
        model.scanTreeAsync();
    }

    @Override
    public String convertValueToText(Object node, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (node instanceof AbstractMountTreeDiffTreeNode) {
            String name = ((AbstractMountTreeDiffTreeNode) node).getName();
            EntryVersion sourceVersion = ((AbstractMountTreeDiffTreeNode) node).getSourceVersion();
            if (node instanceof MountTreeDiffMergeConflictTreeNode) {
                MountTreeDiffMergeConflictTreeNode mergeConflictTreeNode = (MountTreeDiffMergeConflictTreeNode) node;
                String decisionDescription = resolvedConflictPaths.get(mergeConflictTreeNode.getRepositoryPath().toUnversionedPath());
                if (decisionDescription != null) {
                    return "<html>" + mergeConflictTreeNode.getName() + " <small style=\"color:gray\">(" + decisionDescription + ")</small></html>";
                } else {
                    return "<html>" + name + " <small style=\"color:gray\">Ours | Theirs</small></html>";
                }
            } else if (sourceVersion == null) {
                return "<html>" + name + " <small style=\"color:gray\">(additional)</small></html>";
            }
            return "<html>" + name + " <small style=\"color:gray\">" + (sourceVersion.equals(leftVersion) ? "Ours" : "Theirs") + "</small></html>";
        }
        return super.convertValueToText(node, selected, expanded, leaf, row, hasFocus);
    }


    public AbstractMountTreeDiffTreeNode getSelectedChange() {
        Object pathComponent = getLastSelectedPathComponent();
        if (pathComponent instanceof AbstractMountTreeDiffTreeNode) {
            return ((AbstractMountTreeDiffTreeNode) pathComponent);
        }
        return null;
    }

    private static class TreeLeafsComparator implements Comparator<TreeNode> {

        private final Comparator<AbstractMountTreeDiffTreeNode> leafComparator;

        private TreeLeafsComparator(Comparator<AbstractMountTreeDiffTreeNode> leafComparator) {
            this.leafComparator = leafComparator;
        }

        @Override
        public int compare(TreeNode first, TreeNode second) {
            if (isFile(first) && isDirectory(second)) {
                // first is a file, second is folder
                return 1;
            }
            if (isDirectory(first) && isFile(second)) {
                // first is a folder, second is file
                return -1;
            }
            if (isFile(first)) {
                // comparing files
                return leafComparator.compare((AbstractMountTreeDiffTreeNode) first, (AbstractMountTreeDiffTreeNode) second);
            } else {
                // comparing folders
                return first.toString().compareTo(second.toString());
            }
        }

        private static boolean isDirectory(TreeNode node) {
            return !(node.isLeaf() && !node.getAllowsChildren());
        }

        private static boolean isFile(TreeNode node) {
            return node instanceof AbstractMountTreeDiffTreeNode && node.isLeaf();
        }
    }


    public static class MountTreeDifferenceTreeCellRenderer extends DefaultTreeCellRenderer {

        private static final ImageIcon ICON_CONFLICTED = SwingTools.createIcon("24/error.png", IconSize.MEDIUM);
        private static final ImageIcon ICON_ADDED = SwingTools.createIcon("24/add.png", IconSize.MEDIUM);
        private static final ImageIcon ICON_MODIFIED = SwingTools.createIcon("24/pencil.png", IconSize.MEDIUM);
        private static final ImageIcon ICON_DELETED = SwingTools.createIcon("24/garbage_can.png", IconSize.MEDIUM);

        private static final Map<Class<?>, Icon> ICON_CACHE_CONFLICTED = new HashMap<>();
        private static final Map<Class<?>, Icon> ICON_CACHE_ADDED = new HashMap<>();
        private static final Map<Class<?>, Icon> ICON_CACHE_MODIFIED = new HashMap<>();
        private static final Map<Class<?>, Icon> ICON_CACHE_DELETED = new HashMap<>();
        private final Map<RepositoryPath, Icon> iconCache = new HashMap<>();

        private final Color defaultBackgroundNonSelectionColor;
        private final EntryVersion leftVersion;
        private final EntryVersion rightVersion;
        private final Function<RepositoryPath, String> decisionSupplier;

        public MountTreeDifferenceTreeCellRenderer(EntryVersion leftVersion, EntryVersion rightVersion, Function<RepositoryPath, String> decisionSupplier) {
            super();
            this.leftVersion = leftVersion;
            this.rightVersion = rightVersion;
            this.decisionSupplier = decisionSupplier;
            defaultBackgroundNonSelectionColor = getBackgroundNonSelectionColor();
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            Component cellRendererComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            if (leaf) {
                if (value instanceof AbstractMountTreeDiffFileTreeNode) {
                    Icon icon = null;
                    AbstractMountTreeDiffFileTreeNode changesTreeNode = (AbstractMountTreeDiffFileTreeNode) value;
                    EntryVersion sourceVersion = changesTreeNode.getSourceVersion();
                    if (sourceVersion == null) {
                        setBackgroundNonSelectionColor(COLOR_ADDITIONAL_CHANGES);
                        setToolTipText("Uncommitted change");
                    } else if (sourceVersion.equals(leftVersion)) {
                        setBackgroundNonSelectionColor(COLOR_LEFT_CHANGES);
                        setToolTipText("Outgoing change from our version");
                    } else if (sourceVersion.equals(rightVersion)) {
                        setBackgroundNonSelectionColor(COLOR_RIGHT_CHANGES);
                        setToolTipText("Incoming change from their version");
                    }
                    Class<?> contentClass = changesTreeNode.getType();
                    if (value instanceof MountTreeDiffMergeConflictTreeNode) {
                        icon = ICON_CACHE_CONFLICTED.computeIfAbsent(contentClass, MountTreeDifferenceTreeCellRenderer::createConflictedImageIconForClass);
                        String decisionDescription = decisionSupplier.apply(changesTreeNode.getRepositoryPath().toUnversionedPath());
                        if (decisionDescription != null) {
                            setBackgroundNonSelectionColor(COLOR_RESOLVED_CONFLICT_CHANGES);
                            setToolTipText("Outgoing change that resolve conflict by " + decisionDescription);
                        } else {
                            setBackgroundNonSelectionColor(COLOR_CONFLICT_CHANGES);
                            setToolTipText("Incoming change from their version that caused a conflict with ours");
                        }
                    } else if (value instanceof MountTreeDiffCreationTreeNode) {
                        icon = ICON_CACHE_ADDED.computeIfAbsent(contentClass, MountTreeDifferenceTreeCellRenderer::createAddedImageIconForClass);
                    } else if (value instanceof MountTreeDiffModificationTreeNode) {
                        icon = ICON_CACHE_MODIFIED.computeIfAbsent(contentClass, MountTreeDifferenceTreeCellRenderer::createModifiedImageIconForClass);
                    } else if (value instanceof MountTreeDiffDeletionTreeNode) {
                        icon = ICON_CACHE_DELETED.computeIfAbsent(contentClass, MountTreeDifferenceTreeCellRenderer::createDeletedImageIconForClass);
                    }
                    if (icon != null) {
                        setIcon(icon);
                    }
                }
            } else {
                if (value instanceof AbstractMountTreeDiffDirectoryTreeNode) {
                    EntryVersion sourceVersion = ((AbstractMountTreeDiffDirectoryTreeNode) value).getSourceVersion();
                    if (sourceVersion.equals(leftVersion)) {
                        setBackgroundNonSelectionColor(COLOR_LEFT_CHANGES);
                        setToolTipText("Outgoing change from our version");
                    } else if (sourceVersion.equals(rightVersion)) {
                        setBackgroundNonSelectionColor(COLOR_RIGHT_CHANGES);
                        setToolTipText("Incoming change from their version");
                    }
                    if (value instanceof MountTreeDiffDirectoryCreationTreeNode) {
                        setIcon(SwingTools.addIconOverlay(getIcon(), ICON_ADDED));
                    } else if (value instanceof MountTreeDiffDirectoryDeletionTreeNode) {
                        setIcon(SwingTools.addIconOverlay(getIcon(), ICON_DELETED));
                    }
                }
                setToolTipText(null);
                setBackgroundNonSelectionColor(defaultBackgroundNonSelectionColor);
            }
            return cellRendererComponent;
        }

        private static Icon createConflictedImageIconForClass(Class<?> content) {
            return SwingTools.addIconOverlay(getIconOfType(content), ICON_CONFLICTED);
        }

        private static Icon createAddedImageIconForClass(Class<?> content) {
            return SwingTools.addIconOverlay(getIconOfType(content), ICON_ADDED);
        }

        private static Icon createModifiedImageIconForClass(Class<?> content) {
            return SwingTools.addIconOverlay(getIconOfType(content), ICON_MODIFIED);
        }

        private static Icon createDeletedImageIconForClass(Class<?> content) {
            return SwingTools.addIconOverlay(getIconOfType(content), ICON_DELETED);
        }

        private static ImageIcon getIconOfType(Class<?> contentClass) {
            if (IOObject.class.isAssignableFrom(contentClass)) {
                return IOObjectGUIService.getIcon((Class<? extends IOObject>) contentClass);
            } else if (AbstractPipeline.class.isAssignableFrom(contentClass)) {
                return RepositoryTreeCellRenderer.ICON_PIPELINE;
                // RepositoryTreeCellRenderer.derivePipelineIcon((Class<? extends AbstractPipeline>)
                // contentClass);
            } else if (Path.class.isAssignableFrom(contentClass)) {
                return RepositoryTreeCellRenderer.ICON_FOLDER_OPEN;
            } else {
                return RepositoryTreeCellRenderer.ICON_UNKNOWN;
            }
        }
    }

    private class ExpandAllTreeModelListener implements TreeModelListener {

        @Override
        public void treeNodesChanged(TreeModelEvent e) {

        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            SwingUtilities.invokeLater(() -> {
                expandPath(e.getTreePath());
            });
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {

        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
            SwingUtilities.invokeLater(() -> {
                for (int i = 0; i < getRowCount(); i++) {
                    expandRow(i);
                }
            });
        }
    }

}
