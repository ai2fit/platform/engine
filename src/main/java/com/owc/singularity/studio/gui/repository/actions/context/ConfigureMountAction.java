/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.*;
import com.owc.singularity.studio.gui.repository.OperationProgressMonitorAdapter;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNodeInformation;
import com.owc.singularity.studio.gui.repository.mount.ConfigureRepositoryMountDialog;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This action configures the selected repository.
 *
 * @author Simon Fischer
 *
 */
public class ConfigureMountAction extends AbstractRepositoryContextAction<ConfigureMountAction.ActionConfiguration> {

    private static final long serialVersionUID = 1L;

    public ConfigureMountAction(RepositoryTree tree) {
        super(tree, true, false, null, false, false, "configure_mount");
    }


    public boolean enable() {
        List<RepositoryTreeNode> entries = tree.getSelectedEntries();
        if (entries.size() != 1) {
            setEnabled(false);
            return false;
        } else {
            RepositoryTreeNode node = entries.iterator().next();
            RepositoryTreeNodeInformation nodeInformation = node.getInformation();
            RepositoryMount mount = nodeInformation.getMount();
            if (mount != null && nodeInformation.getRepositoryPath().equals(mount.getMountPath())) {
                setEnabled(true);
                return true;
            } else {
                setEnabled(false);
                return false;
            }
        }
    }

    @Override
    protected ActionConfiguration configureAction(List<RepositoryTreeNode> entries) {
        RepositoryPath path = entries.get(0).getRepositoryPath();
        try {
            RepositoryMount mount = path.getFileSystem().getMount(path);
            ActionConfiguration config = new ActionConfiguration();
            config.path = mount.getMountPath();
            if (mount instanceof PlaceholderRepositoryMount) {
                // handle placeholder as their original mount
                config.type = ((PlaceholderRepositoryMount) mount).getTemplateType();
                config.options = ((PlaceholderRepositoryMount) mount).getTemplateOptions();
            } else {
                config.type = mount.getType();
                config.options = mount.getOptions();
            }
            ConfigureRepositoryMountDialog dialog = new ConfigureRepositoryMountDialog(config.options, config.type);
            dialog.setVisible(true);
            if (!dialog.wasConfirmed()) {
                // cancel action by returning null
                return null;
            }
            config.options = dialog.getOptions();
            return config;
        } catch (NoSuchFileException e) {
            SwingTools.showSimpleErrorMessage("cannot_configure_mount", e, path.toString(), e.getMessage());
            return null;
        }
    }

    @Override
    public void executeAction(RepositoryPath repositoryPath, ActionConfiguration config, ProgressListener progressListener) {
        String mountPoint = config.path.toString();
        RepositoryManager.RepositoryMountConfiguration mountConfig = new RepositoryManager.RepositoryMountConfiguration(config.type, config.options);
        try (OperationProgressMonitorAdapter ignored = new OperationProgressMonitorAdapter(progressListener)) {
            RepositoryManager.removeMount(mountPoint, false);
            RepositoryManager.addMount(mountPoint, mountConfig);
            // persist the configuration directly to the configured path
            Path repositoryConfigurationFilePath = Path.of(PropertyService.getParameterValue(EngineProperties.GENERAL_REPOSITORY_CONFIG_FILE,
                    FileSystemService.getUserConfigFile(EngineProperties.DEFAULT_GENERAL_REPOSITORY_CONFIG_FILE).toString()));
            try (OutputStream outputStream = Files.newOutputStream(repositoryConfigurationFilePath)) {
                RepositoryManager.writeConfiguration(outputStream);
            } catch (IOException e) {
                LogService.getRoot()
                        .error("com.owc.singularity.repository.configuration_persistence_error",
                                new Object[] { repositoryConfigurationFilePath, e.getMessage(), e });
                SwingTools.showSimpleErrorMessage("cannot_save_repository_configuration", e, repositoryConfigurationFilePath, e.getMessage());
            }
        } catch (IOException e) {
            try {
                RepositoryManager.addMountPlaceholder(mountPoint, mountConfig, e);
            } catch (IOException ex) {
                SwingTools.showSimpleErrorMessage("cannot_configure_mount", e);
            }
            SwingTools.showSimpleErrorMessage("cannot_configure_mount", e);
        }
    }

    static class ActionConfiguration {

        String type;
        RepositoryPath path;
        Map<String, String> options;
    }
}
