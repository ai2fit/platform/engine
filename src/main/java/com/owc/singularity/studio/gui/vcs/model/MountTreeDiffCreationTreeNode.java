package com.owc.singularity.studio.gui.vcs.model;


import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffCreationTreeNode extends AbstractMountTreeDiffFileTreeNode {


    public MountTreeDiffCreationTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }
}
