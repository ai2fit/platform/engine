/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.perspective;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceMenu;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * A {@link ResourceMenu} which displays the all {@link Perspective}s of the
 * {@link PerspectiveController}. Uses the {@link PerspectiveController.PerspectiveModel} to fetch
 * updates, like adding of new perspectives or perspective changes.
 *
 * @author Marcel Michel
 * @since 7.0.0
 */
public class PerspectiveMenu extends ResourceMenu {

    private static final long serialVersionUID = 1L;

    private final PerspectiveController perspectiveController;

    private final Map<String, JMenuItem> perspectiveMap = new HashMap<>();

    private ButtonGroup workspaceMenuGroup = new ButtonGroup();

    private String perspectiveName;

    /**
     * Creates a new {@link PerspectiveMenu} and uses the {@link PerspectiveController} to get the
     * registrered {@link Perspective}s.
     *
     * @param perspectiveController
     *            the controller which should be used the fetch the perspectives
     */
    public PerspectiveMenu(PerspectiveController perspectiveController) {
        super("perspectives");
        this.perspectiveController = perspectiveController;
        this.perspectiveController.addPerspectiveChangeListener(new PerspectiveChangeListener() {

            @Override
            public void onPerspectiveChange(Perspective from, Perspective to) {
                perspectiveName = to.getName();
                if (perspectiveMap.containsKey(perspectiveName)) {
                    SwingTools.invokeLater(() -> {
                        JMenuItem item = perspectiveMap.get(perspectiveName);
                        if (item != null) {
                            item.setSelected(true);
                        }
                    });
                }
            }

            @Override
            public void onPerspectiveAdd(Perspective newPerspective) {
                updatePerspectives(perspectiveController.getAllPerspectives());
            }

            @Override
            public void onPerspectiveRemove(Perspective removedPerspective) {
                updatePerspectives(perspectiveController.getAllPerspectives());
            }
        });
        updatePerspectives(this.perspectiveController.getAllPerspectives());
    }

    private void updatePerspectives(List<Perspective> perspectives) {
        removeAll();
        perspectiveMap.clear();
        workspaceMenuGroup = new ButtonGroup();
        for (Perspective p : perspectives) {
            ResourceAction action = perspectiveController.createPerspectiveAction(p);

            JMenuItem menuItem = new JRadioButtonMenuItem(action);
            add(menuItem);
            perspectiveMap.put(p.getName(), menuItem);
            workspaceMenuGroup.add(menuItem);
        }
        if (perspectiveMap.containsKey(perspectiveName)) {
            perspectiveMap.get(perspectiveName).setSelected(true);
        }
    }

}
