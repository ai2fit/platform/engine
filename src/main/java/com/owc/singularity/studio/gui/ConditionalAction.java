/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui;


import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.*;

import javax.swing.Icon;
import javax.swing.SwingUtilities;


/**
 * An action that must be enabled/disabled depending on certain conditions. These conditions can be
 * mandatory, disallowed, or irrelevant. All ConditionalActions created are added to a collection
 * and there status is automatically checked if the condition premises might have changed.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
public abstract class ConditionalAction extends LoggedAbstractAction {

    /**
     *
     */
    private static final long serialVersionUID = -3581066203343247846L;

    private static final List<WeakReference<ConditionalAction>> ALL_ACTIONS = new LinkedList<>();

    private final EnumMap<Condition, ConditionReaction> conditions = new EnumMap<>(Condition.class);

    private boolean isDisabledDueToFocusLost;

    public ConditionalAction(String name) {
        this(name, null);
    }

    public ConditionalAction(String name, Icon icon) {
        super(name, icon);
        conditions.put(Condition.EDIT_IN_PROGRESS, ConditionReaction.DISALLOWED);
        if (SwingUtilities.isEventDispatchThread()) {
            ALL_ACTIONS.add(new WeakReference<>(this));
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    ALL_ACTIONS.add(new WeakReference<>(ConditionalAction.this));
                }
            });
        }
    }

    /**
     * @param condition
     *            one out of OPERATOR_SELECTED, OPERATOR_CHAIN_SELECTED, ROOT_SELECTED,
     *            CLIPBOARD_FILLED, and PROCESS_RUNNING
     * @param state
     *            one out of DISALLOWED, DONT_CARE, and MANDATORY
     */
    public void setCondition(Condition condition, ConditionReaction state) {
        conditions.put(condition, state);
    }

    /** Updates all actions. */
    public static void updateAll(EnumMap<Condition, Boolean> states) {
        Iterator<WeakReference<ConditionalAction>> i = ALL_ACTIONS.iterator();
        while (i.hasNext()) {
            WeakReference<ConditionalAction> ref = i.next();
            ConditionalAction c = ref.get();
            if (c == null) {
                i.remove();
            } else {
                c.update(states);
            }
        }
    }

    /**
     * Updates an action given the set of states that can be true or false. States refer to
     * OPERATOR_SELECTED... An action is enabled iff for all states the condition is MANDATORY and
     * state is true or DISALLOWED and state is false. If for all states the condition is DONT_CARE,
     * the enabling status of the action is not touched.
     */
    protected void update(EnumMap<Condition, Boolean> states) {
        // if this action is disabled due to a focus loss never change its enabled state here
        if (isDisabledDueToFocusLost) {
            return;
        }

        boolean ok = true;
        boolean ignore = true;
        for (Map.Entry<Condition, ConditionReaction> conditionEntry : conditions.entrySet()) {
            Condition condition = conditionEntry.getKey();
            ConditionReaction reaction = conditionEntry.getValue();
            if (reaction == ConditionReaction.DONT_CARE) {
                continue;
            }
            boolean conditionState = states.get(condition);
            ignore = false;
            if (((reaction == ConditionReaction.MANDATORY) && !conditionState) || ((reaction == ConditionReaction.DISALLOWED) && conditionState)) {
                ok = false;
                break;
            }
        }
        if (!ignore) {
            setEnabled(ok);
        }
    }

    public boolean isDisabledDueToFocusLost() {
        return isDisabledDueToFocusLost;
    }

    /**
     * If set to <code>true</code>, will not enable itself to condition changes.
     *
     * @param isDisabledDueToFocusLost
     */
    public void setDisabledDueToFocusLost(boolean isDisabledDueToFocusLost) {
        this.isDisabledDueToFocusLost = isDisabledDueToFocusLost;
    }

    @Override
    protected ConditionalAction clone() throws CloneNotSupportedException {
        ConditionalAction clone = (ConditionalAction) super.clone();
        ALL_ACTIONS.add(new WeakReference<>(clone));
        return clone;
    }

    /**
     * The possible states.
     */
    public enum ConditionReaction implements Serializable {
        DISALLOWED, DONT_CARE, MANDATORY
    }

    public enum Condition implements Serializable {
        OPERATOR_SELECTED, OPERATOR_CHAIN_SELECTED, ROOT_SELECTED, SIBLINGS_EXIST, PROCESS_STOPPED, PROCESS_PAUSED, PROCESS_RUNNING, PARENT_ENABLED, EDIT_IN_PROGRESS, PROCESS_SAVED, PROCESS_RENDERER_IS_VISIBLE, PROCESS_RENDERER_HAS_UNDO_STEPS, PROCESS_RENDERER_HAS_REDO_STEPS, PROCESS_HAS_BREAKPOINTS, PROCESS_HAS_REPOSITORY_LOCATION, PROCESS_IS_READ_ONLY,
    }
}
