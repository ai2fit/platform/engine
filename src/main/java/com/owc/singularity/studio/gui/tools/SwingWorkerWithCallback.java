package com.owc.singularity.studio.gui.tools;

import java.util.function.Consumer;

import javax.swing.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class SwingWorkerWithCallback<T, V> extends SwingWorker<T, V> {

    private final Logger log = LogManager.getLogger(getClass());
    private final Consumer<T> callback;

    public SwingWorkerWithCallback(Consumer<T> callback) {
        this.callback = callback;
    }

    @Override
    protected void done() {
        if (callback == null)
            return;
        try {
            callback.accept(get());
        } catch (InterruptedException ignored) {
            log.trace("Worker done method was interrupted");
        } catch (Throwable e) {
            log.catching(e);
        }
    }
}
