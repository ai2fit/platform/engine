/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.renderer;


import java.awt.Component;

import com.owc.singularity.engine.object.Reportable;


/**
 * This is the abstract renderer superclass for all renderers which should simply use a Java
 * component. Basically, this class only exists to allow for dirty hacks and should not be used in
 * general.
 * 
 * @author Ingo Mierswa
 * @deprecated deprecated with 9.2.0, will be removed in a future release!
 */
@Deprecated
public class DefaultComponentRenderer extends AbstractRenderer {

    private String name;

    private Component component;

    public DefaultComponentRenderer(String name, Component component) {
        this.name = name;
        this.component = component;
    }

    @Override
    public Reportable createReportable(Object renderable, int desiredWidth, int desiredHeight) {
        return new DefaultComponentRenderable(component);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Component getVisualizationComponent(Object renderable) {
        return component;
    }
}
