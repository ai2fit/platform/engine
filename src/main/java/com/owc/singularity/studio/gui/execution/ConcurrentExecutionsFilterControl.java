package com.owc.singularity.studio.gui.execution;

import javax.swing.DefaultListModel;
import javax.swing.SwingUtilities;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.ConcurrentExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentOperatorExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecutionState;

public class ConcurrentExecutionsFilterControl {

    public static DefaultListModel<ConcurrentExecution> filteredList = new DefaultListModel<>();
    public static boolean SHOW_RUNNING_EXECUTIONS = true, SHOW_FINISHED_EXECUTIONS = true, SHOW_PENDING_EXECUTIONS = true;

    public static void applyFilter() {
        filteredList.removeAllElements();

        for (ConcurrentExecution execution : ConcurrentExecutionServiceProvider.getService().getExecutions()) {
            addFilteredElement(execution);
        }

    }

    public static void addFilteredElement(final ConcurrentExecution execution) {
        if (filteredList.contains(execution))
            return;
        boolean processAdded = false;
        if (execution instanceof ConcurrentPipelineExecution) {
            ConcurrentPipelineExecution process = (ConcurrentPipelineExecution) execution;
            ConcurrentPipelineExecutionState state = process.getBackgroundExecutionState();
            if (state != null) {
                if (SHOW_FINISHED_EXECUTIONS) {
                    // A process is finished if it has started and stoped/ended
                    if (state.isStarted() && (state.isEnded() || state.isStopped())) {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                filteredList.addElement(execution);

                            }
                        });

                        processAdded = true;
                    }

                }

                if (!processAdded && SHOW_PENDING_EXECUTIONS) {
                    // A process is pending if it is not yet started and is not being stopped yet
                    if (!state.isStarted() && !state.isEnded() && !state.isStopped()) {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                filteredList.addElement(execution);

                            }
                        });
                        processAdded = true;
                    }

                }
                if (!processAdded && SHOW_RUNNING_EXECUTIONS) {
                    // A process is running if it has started and is not yet stopped
                    if (state.isStarted() && !state.isEnded() && !state.isStopped()) {
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                filteredList.addElement(execution);

                            }
                        });
                    }
                }
            }

        }

        if (!processAdded && execution instanceof ConcurrentOperatorExecution) {
            // A OperatorConcurrentExecution is considered as a running process
            if (SHOW_RUNNING_EXECUTIONS) {
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        filteredList.addElement(execution);

                    }
                });
                processAdded = true;
            }
        }
    }

}
