/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.editor;


import java.awt.*;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.event.EventListenerList;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.ports.Ports;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.engine.tools.SystemInfoUtilities.OperatingSystem;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.StoreInRepositoryAction;
import com.owc.singularity.studio.gui.actions.export.PrintableComponent;
import com.owc.singularity.studio.gui.dnd.AbstractPatchedTransferHandler;
import com.owc.singularity.studio.gui.dnd.DragListener;
import com.owc.singularity.studio.gui.dnd.OperatorTransferHandler;
import com.owc.singularity.studio.gui.editor.action.ProcessActions;
import com.owc.singularity.studio.gui.editor.action.ProcessChangingAction;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessUserInteractionListener;
import com.owc.singularity.studio.gui.editor.pipeline.ExtensionButton;
import com.owc.singularity.studio.gui.editor.pipeline.PanningManager;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessOverviewDockable;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.AnnotationsVisualizer;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.event.AnnotationEventHook;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.OperatorAnnotation;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotation;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotations;
import com.owc.singularity.studio.gui.editor.pipeline.background.ProcessBackgroundImageVisualizer;
import com.owc.singularity.studio.gui.editor.pipeline.draw.OperatorDrawDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawUtils;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessRenderer;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessEditorModelEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererAnnotationEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererModelEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererOperatorEvent;
import com.owc.singularity.studio.gui.editor.pipeline.view.ProcessRendererEventDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.view.ProcessRendererEventDecorator.KeyEventType;
import com.owc.singularity.studio.gui.editor.pipeline.view.ProcessRendererEventDecorator.MouseEventType;
import com.owc.singularity.studio.gui.editor.pipeline.view.RenderPhase;
import com.owc.singularity.studio.gui.editor.pipeline.view.actions.DeleteSelectedConnectionAction;
import com.owc.singularity.studio.gui.editor.pipeline.view.actions.SelectAllAction;
import com.owc.singularity.studio.gui.editor.pipeline.view.components.ProcessRendererTooltipProvider;
import com.owc.singularity.studio.gui.editor.quickfix.QuickFix;
import com.owc.singularity.studio.gui.parameters.celleditors.value.PropertyValueCellEditor;
import com.owc.singularity.studio.gui.tools.PrintingTools;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceMenu;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow.TooltipLocation;


/**
 * This class displays a SingularityEngine process and allows user interaction with it.
 * <p>
 * Actual Java2D drawing is delegated to the {@link ProcessRenderer} and its registered
 * {@link ProcessDrawDecorator}s. To decorate the process drawing, call
 * {@link #addDrawDecorator(ProcessDrawDecorator, RenderPhase)} and register custom decorators.
 * </p>
 * <p>
 * To provide hooks into the event handling, i.e. to allow the user to interact with your
 * decorations, event decorators can be registered via
 * {@link #addEventDecorator(ProcessRendererEventDecorator, RenderPhase)}.
 * </p>
 * <p>
 * To simply hook into default popup menus, register a listener via
 * {@link #addProcessInteractionListener(ProcessUserInteractionListener)}.
 * </p>
 *
 * @author Simon Fischer, Marco Boeck, Jan Czogalla
 * @since 6.4.0
 *
 */
public class ProcessEditorPanel extends JPanel implements PrintableComponent {

    private static final long serialVersionUID = 1L;

    private static final int RENAME_FIELD_HEIGHT = 21;

    /** the backing model */
    private final ProcessRendererModel model;


    /** the workflow annotations handler instance */
    private final AnnotationsVisualizer annotationsVisualizer;

    /** the background image handler */
    private final ProcessBackgroundImageVisualizer backgroundImageHandler;

    private final ProcessActions processEditorActions;
    private final ProcessEditorPanelTransferHandler transferHandler;
    /** the text field used for renaming an operator */
    private JTextField renameField;

    /** responsible for default process renderer view interaction */
    private final transient ProcessEditorMouseHandler interactionMouseHandler;

    /** the mouse handler for the entire process renderer */
    private final transient MouseAdapter mouseHandler = new MouseEventProcessor();

    /** the key handler for the entire process renderer */

    private final transient KeyAdapter keyHandler = new KeyEventProcessor();

    /** the drag and drop listener */
    private final transient DragListener dragListener = new DragListener() {

        @Override
        public void dragStarted(Transferable t) {
            if (isReadOnly())
                return;

            // check if transferable can be imported
            if (!userInteractionInterceptor.canImportTransferable(t)) {
                return;
            }

            model.setDragStarted(true);
            model.fireMiscChanged();
        }

        @Override
        public void dragEnded() {
            if (isReadOnly())
                return;

            model.setDragStarted(false);
            model.checkForNewUndoStep();
            model.fireMiscChanged();
        }
    };

    private final ResourceAction selectAllAction;
    private static final ResourceAction DELETE_SELECTED_CONNECTION_ACTION = new DeleteSelectedConnectionAction();

    /** the list of extension buttons for subprocesses (add/remove subprocess) */
    private final List<ExtensionButton> subprocessExtensionButtons;

    /** the list of event decorators */
    private final Map<RenderPhase, CopyOnWriteArrayList<ProcessRendererEventDecorator>> decorators;

    /** the controller for this view */
    private final ProcessEditorUserInteractionInterceptor userInteractionInterceptor;

    /** the drawer responsible for 2D drawing the process for this view */
    private final ProcessRenderer drawer;

    /** the drawer responsible for 2D drawing the process for the overview */
    private final ProcessRenderer drawerOverview;


    /** event listener for the editor */
    private final EventListenerList eventListeners;
    private boolean readOnly;

    public ProcessEditorPanel() {
        this.model = new ProcessRendererModel();
        this.eventListeners = new EventListenerList();
        // prepare event decorators for each phase
        this.decorators = new EnumMap<>(RenderPhase.class);
        for (RenderPhase phase : RenderPhase.eventOrder()) {
            decorators.put(phase, new CopyOnWriteArrayList<>());
        }
        this.drawer = new ProcessRenderer(model, true);
        this.drawerOverview = new ProcessRenderer(model, false);

        this.userInteractionInterceptor = new ProcessEditorUserInteractionInterceptor(this, model);
        this.annotationsVisualizer = new AnnotationsVisualizer(this);
        this.backgroundImageHandler = new ProcessBackgroundImageVisualizer(this);
        this.processEditorActions = new ProcessActions(this);

        interactionMouseHandler = new ProcessEditorMouseHandler(this, model, userInteractionInterceptor);

        // init list of subprocess extension buttons (add/remove subprocess)
        subprocessExtensionButtons = new LinkedList<>();

        // listen to ProcessRendererModel events
        addProcessEditorEventListener(processEditorActions);
        addProcessInteractionListener(processEditorActions);

        ModelEventProcessor modelEventProcessor = new ModelEventProcessor();
        model.registerEditorModelEventListener(modelEventProcessor);
        model.registerEditorEventListener(modelEventProcessor);

        // add GUI actions
        selectAllAction = new SelectAllAction(this);

        // register transfer handler and drop target
        transferHandler = new ProcessEditorPanelTransferHandler(this, model, userInteractionInterceptor);
        setTransferHandler(transferHandler);
        ProcessEditorPanelDropTarget dropTarget;
        try {
            dropTarget = new ProcessEditorPanelDropTarget(this, AbstractPatchedTransferHandler.getDropTargetListener());
            setDropTarget(dropTarget);
            model.setDropTargetSet(true);
        } catch (Exception e) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.flow.processrendering.view.ProcessRendererView.drop_target_failed", e.getMessage());
        }

        // add some actions to the action map of this component
        ProcessActions.TOGGLE_BREAKPOINT[BreakpointListener.BREAKPOINT_AFTER].addToActionMap(this, WHEN_FOCUSED);
        ProcessActions.TOGGLE_ACTIVATION_ITEM.addToActionMap(this, WHEN_FOCUSED);
        selectAllAction.addToActionMap(this, WHEN_FOCUSED);
        OperatorTransferHandler.addToActionMap(this);
        DELETE_SELECTED_CONNECTION_ACTION.addToActionMap(WHEN_FOCUSED, false, false, true, "delete", this);

        // add tooltips
        new ToolTipWindow(new ProcessRendererTooltipProvider(model), this, TooltipLocation.RIGHT);

        // add panning support to allow operators to extend and move the displayed process area when
        // dragged to the side/bottom
        new PanningManager(this);

        init();
    }

    public void updateProcess(AbstractPipeline process) {
        getModel().setProcess(process, false);
        getModel().fireProcessEdited();
    }

    public void setProcess(AbstractPipeline process) {
        getModel().setProcess(process, true);
    }

    public void resize() {
        userInteractionInterceptor.autoFit();
    }

    /**
     * Selects and shows a single given operator. Will switch the displayed chain either to the
     * parent or the selected chain, depending on the provided flag. This can be used to easily
     * update the view. Convenience method.
     *
     * @param currentlySelected
     * @param showParent
     * @since 7.5
     */
    public void selectAndShowOperator(Operator currentlySelected, boolean showParent) {
        AbstractPipeline process = model.getProcess();
        if (currentlySelected == null) {
            if (process == null)
                return;
            currentlySelected = process.getRootOperator();
        }
        OperatorChain parent = currentlySelected.getParent();
        // if this is not a chain, it can not be displayed as such!
        showParent |= !(currentlySelected instanceof OperatorChain);
        // root chain has no parent
        showParent &= parent != null;
        OperatorChain dispChain = showParent ? parent : (OperatorChain) currentlySelected;
        model.setDisplayedChainAndFire(dispChain);
        selectOperators(Collections.singletonList(currentlySelected));
    }

    public void selectOperators(List<Operator> operators) {
        for (Operator op : operators) {
            AbstractPipeline selectedProcess = op.getPipeline();
            if (selectedProcess == null) {
                SwingTools.showVerySimpleErrorMessage("op_deleted", op.getName());
                return;
            }
        }
        model.clearOperatorSelection();
        model.addOperatorsToSelection(operators);
        model.fireOperatorSelectionChanged(operators);
    }

    @Override
    public void paintComponent(final Graphics graphics) {
        super.paintComponent(graphics);
        if (model.isDragStarted() || model.getConnectingPortSource() != null) {
            ((Graphics2D) graphics).setRenderingHints(ProcessRenderer.LOW_QUALITY_HINTS);
        } else {
            ((Graphics2D) graphics).setRenderingHints(ProcessRenderer.HI_QUALITY_HINTS);
        }
        Graphics2D g2 = (Graphics2D) graphics.create();
        getProcessRenderer().draw(g2, false);
        g2.dispose();
    }

    @Override
    public void printComponent(final Graphics graphics) {
        ((Graphics2D) graphics).setRenderingHints(ProcessRenderer.HI_QUALITY_HINTS);
        getProcessRenderer().draw((Graphics2D) graphics.create(), true);
    }

    /**
     * Return the index of the process under the given {@link Point2D view point}.
     *
     * @param p
     *            the point
     * @return the index or -1 if no process is under the point
     * @see #getProcessIndexOfOperator(Operator)
     */
    public int getProcessIndexUnder(final Point2D p) {
        if (p == null) {
            return -1;
        }

        if (p.getY() < 0 || p.getY() > userInteractionInterceptor.getTotalHeight()) {
            return -1;
        }
        int xOffset = 0;
        for (int i = 0; i < model.getExecutionUnits().size(); i++) {
            int relativeX = (int) p.getX() - xOffset;
            if (relativeX >= 0 && relativeX <= model.getProcessWidth(model.getProcess(i))) {
                return i;
            }
            xOffset += ProcessRenderer.WALL_WIDTH * 2 + model.getProcessWidth(model.getProcess(i));
        }
        return -1;
    }

    /**
     * Converts a {@link Point view point} to a point relative to the specified process.
     *
     * @param p
     *            the original point
     * @param processIndex
     *            the index of the process
     * @return the relative point or {@code null} if no process exists for the specified index
     * @see ProcessEditorPanel#fromProcessSpace(Point, int)
     */
    public Point toProcessSpace(final Point p, final int processIndex) {
        if (processIndex == -1 || processIndex >= model.getExecutionUnits().size()) {
            return null;
        }
        int xOffset = getXOffset(processIndex);
        double zoomFactor = model.getZoomFactor();
        return new Point((int) ((p.getX() - xOffset) * (1 / zoomFactor)), (int) (p.getY() * (1 / zoomFactor)));
    }

    /**
     * Returns the index of the process of the given {@link Operator}.
     *
     * @param op
     *            the operator
     * @return the index or -1 if the process is not currently shown
     * @since 7.5
     * @see #getProcessIndexUnder(Point2D)
     */
    public int getProcessIndexOfOperator(Operator op) {
        if (op == null) {
            return -1;
        }
        ExecutionUnit eu = op.getExecutionUnit();
        return model.getProcessIndex(eu);
    }

    /**
     * Converts a {@link Point} from the process to a point relative to the current view.
     *
     * @param p
     *            the process point
     * @param processIndex
     *            the index of the process
     * @return the view point or {@code null} if no process exists for the specified index
     * @since 7.5
     * @see #toProcessSpace(Point, int)
     */
    public Point fromProcessSpace(final Point p, final int processIndex) {
        if (processIndex == -1 || processIndex >= model.getExecutionUnits().size()) {
            return null;
        }
        int xOffset = getXOffset(processIndex);
        double zoomFactor = model.getZoomFactor();
        return new Point((int) (p.x * zoomFactor) + xOffset, (int) (p.y * zoomFactor));
    }

    /**
     * Calculates the (absolute/view) x offset for the given process index. The index must be
     * between 0 (inclusive) and the number of processes currently in the model (exclusive).
     *
     * @param processIndex
     *            the index of the process
     * @return the x offset before the specified process
     * @since 7.5
     * @see #toProcessSpace(Point, int)
     * @see #fromProcessSpace(Point, int)
     */
    private int getXOffset(final int processIndex) {
        List<ExecutionUnit> processes = model.getExecutionUnits();
        int xOffset = processIndex * ProcessRenderer.WALL_WIDTH * 2;
        for (int i = 0; i < processIndex; i++) {
            xOffset += model.getProcessWidth(processes.get(i));
        }
        return xOffset;
    }

    /**
     * Call when the process has been updated, i.e. an operator has been added. Will update operator
     * locations and then repaint.
     */
    public void processUpdated() {
        Operator hoveredOp = model.getHoveringOperator();
        boolean hoveredOperatorFound = hoveredOp == null;
        List<Operator> movedOperators = new LinkedList<>();
        List<Operator> portChangedOperators = new LinkedList<>();

        // make sure location of every operator is set and potentially reset hovered op
        for (ExecutionUnit unit : model.getExecutionUnits()) {
            // check if all operators have positions, if not, set them now
            movedOperators = userInteractionInterceptor.ensureOperatorsHaveLocation(unit);

            for (Operator op : unit.getOperators()) {
                // check if number of ports has changed for any operators
                // otherwise we would not know that we need to check process size and repaint
                Integer formerNumber = model.getNumberOfPorts(op);
                Integer newNumber = op.getInputPorts().getNumberOfPorts() + op.getOutputPorts().getNumberOfPorts();
                if (!newNumber.equals(formerNumber)) {
                    portChangedOperators.add(op);
                    model.setNumberOfPorts(op, newNumber);
                }

                // if hovered operator has not yet been found, see if current one is it
                if (!hoveredOperatorFound && hoveredOp != null && hoveredOp.equals(op)) {
                    hoveredOperatorFound = true;
                }
            }
        }

        for (ExecutionUnit unit : model.getExecutionUnits()) {
            // check if number of ports has changed for any processes
            // otherwise we would not know that we need to check process size and repaint
            Operator op = unit.getEnclosingOperator();
            Integer formerNumber = model.getNumberOfPorts(op);
            Integer newNumber = op.getInputPorts().getNumberOfPorts() + op.getOutputPorts().getNumberOfPorts();
            if (!newNumber.equals(formerNumber)) {
                portChangedOperators.add(op);
                model.setNumberOfPorts(op, newNumber);
            }
        }

        // reset hovered operator if not in any process anymore
        if (!hoveredOperatorFound) {
            setHoveringOperator(null);
        }

        if (!movedOperators.isEmpty()) {
            model.fireOperatorsMoved(movedOperators);
        } else if (!portChangedOperators.isEmpty()) {
            model.firePortsChanged(portChangedOperators);
        }
    }

    /**
     * Adds a listener that will be informed when the user right-clicks an operator or a port.
     *
     * @param l
     *            the listener
     */
    public void addProcessInteractionListener(final ProcessUserInteractionListener l) {
        eventListeners.add(ProcessUserInteractionListener.class, l);
    }

    /**
     * @see #addProcessInteractionListener(ProcessUserInteractionListener)
     */
    public void removeProcessInteractionListener(final ProcessUserInteractionListener l) {
        eventListeners.remove(ProcessUserInteractionListener.class, l);
    }

    @Override
    public Component getExportComponent() {
        return this;
    }

    @Override
    public String getExportIconName() {
        return I18N.getGUIMessage("gui.dockkey.process_panel.icon");
    }

    @Override
    public String getExportName() {
        return I18N.getGUIMessage("gui.dockkey.process_panel.name");
    }

    @Override
    public String getIdentifier() {
        AbstractPipeline process = getProcess();
        if (process != null) {
            RepositoryPath processLocation = process.getPath();
            if (processLocation != null) {
                return processLocation.toString();
            }
        }
        return null;
    }

    /**
     * Returns the {@link DragListener} for the process renderer.
     *
     * @return the listener, never {@code null}
     */
    public DragListener getDragListener() {
        return dragListener;
    }

    /**
     * Returns the {@link ProcessRenderer} which is responsible for drawing the process(es) in the
     * {@link ProcessEditorPanel}.
     *
     * @return the drawer instance, never {@code null}
     */
    public ProcessRenderer getProcessRenderer() {
        return drawer;
    }

    /**
     * Returns the {@link ProcessRenderer} which is responsible for drawing the process(es) in the
     * {@link ProcessOverviewDockable}.
     *
     * @return the drawer instance, never {@code null}
     */
    public ProcessRenderer getOverviewPanelDrawer() {
        return drawerOverview;
    }

    /**
     * Returns the {@link ProcessRendererModel} which is backing the GUI.
     *
     * @return the model instance, never {@code null}
     */
    public ProcessRendererModel getModel() {
        return model;
    }

    /**
     * Adds the given renderer decorator for the specified render phase.
     * <p>
     * To add a {@link ProcessDrawDecorator}, call {@link #getProcessRenderer()} and
     * {@link ProcessRenderer#addDecorator(ProcessDrawDecorator, RenderPhase)} on it.
     * </p>
     *
     * @param decorator
     *            the decorator instance to add
     * @param phase
     *            the phase during which the decorator should be notified of events. If multiple
     *            decorators want to handle events during the same phase, they are called in the
     *            order they were registered. If any of the decorators in the chain consume the
     *            event, the remaining decorators will <strong>not</strong> be notified!
     */
    public void addEventDecorator(ProcessRendererEventDecorator decorator, RenderPhase phase) {
        if (decorator == null) {
            throw new IllegalArgumentException("decorator must not be null!");
        }
        if (phase == null) {
            throw new IllegalArgumentException("phase must not be null!");
        }

        decorators.get(phase).add(decorator);
    }

    /**
     * Removes the given decorator for the specified render phase. If the decorator has already been
     * removed, does nothing.
     * <p>
     * To remove a {@link ProcessDrawDecorator}, call {@link #getProcessRenderer()} and
     * {@link ProcessRenderer#removeDecorator(ProcessDrawDecorator, RenderPhase)} on it.
     * </p>
     *
     * @param decorator
     *            the decorator instance to remove
     * @param phase
     *            the phase from which the decorator should be removed
     */
    public void removeEventDecorator(ProcessRendererEventDecorator decorator, RenderPhase phase) {
        if (decorator == null) {
            throw new IllegalArgumentException("decorator must not be null!");
        }
        if (phase == null) {
            throw new IllegalArgumentException("phase must not be null!");
        }

        decorators.get(phase).remove(decorator);
    }

    /**
     * Does the same as {@link ProcessRenderer#addDecorator(ProcessDrawDecorator, RenderPhase)}.
     *
     * @param decorator
     *            the draw decorator
     * @param phase
     *            the specified phase in which to draw
     */
    public void addDrawDecorator(ProcessDrawDecorator decorator, RenderPhase phase) {
        getProcessRenderer().addDecorator(decorator, phase);
    }

    /**
     * Does the same as {@link ProcessRenderer#removeDecorator(ProcessDrawDecorator, RenderPhase)}.
     *
     * @param decorator
     *            the draw decorator to add
     * @param phase
     *            the specified phase for which the decorator was registered
     */
    public void removeDrawDecorator(ProcessDrawDecorator decorator, RenderPhase phase) {
        getProcessRenderer().removeDecorator(decorator, phase);
    }

    /**
     * Does the same as {@link ProcessRenderer#addDecorator(OperatorDrawDecorator)}.
     *
     * @param decorator
     *            the operator draw decorator
     */
    public void addDrawDecorator(OperatorDrawDecorator decorator) {
        getProcessRenderer().addDecorator(decorator);
    }

    /**
     * Does the same as {@link ProcessRenderer#removeDecorator(OperatorDrawDecorator)}.
     *
     * @param decorator
     *            the operator draw decorator to remove
     */
    public void removeDrawDecorator(OperatorDrawDecorator decorator) {
        getProcessRenderer().removeDecorator(decorator);
    }

    /**
     * Opens a rename textfield at the location of the specified operator.
     *
     * @param op
     *            the operator to be renamed
     */
    public void rename(final Operator op) {
        if (op == null) {
            throw new IllegalArgumentException("op must not be null!");
        }

        int processIndex = getModel().getProcessIndex(op.getExecutionUnit());
        if (processIndex == -1) {
            String name = SwingTools.showInputDialog("rename_operator", op.getName());
            if (name != null && name.length() > 0) {
                op.rename(name);
            }
            return;
        }
        renameField = new JTextField(10);
        Rectangle2D rect = ProcessLayoutXMLFilter.lookupOperatorRectangle(op);
        float fontSize = (float) (ProcessRenderer.OPERATOR_FONT.getSize() * model.getZoomFactor());
        Font nameFont = ProcessRenderer.OPERATOR_FONT.deriveFont(fontSize);
        int width;
        width = (int) nameFont.getStringBounds(op.getName(), ((Graphics2D) getGraphics()).getFontRenderContext()).getWidth();
        width = (int) Math.max(width, ProcessRenderer.OPERATOR_WIDTH * model.getZoomFactor());
        double xOffset = (ProcessRenderer.OPERATOR_WIDTH * model.getZoomFactor() - width) / 2;
        double yOffset = (ProcessRenderer.HEADER_HEIGHT * model.getZoomFactor() - RENAME_FIELD_HEIGHT) / 2;
        renameField.setHorizontalAlignment(SwingConstants.CENTER);
        renameField.setText(op.getName());
        renameField.selectAll();

        int x = (int) (rect.getX() * model.getZoomFactor());
        int y = (int) (rect.getY() * model.getZoomFactor());
        Point p = ProcessDrawUtils.convertToAbsoluteProcessPoint(new Point(x, y), processIndex, model);

        int padding = 7;
        renameField.setBounds((int) (p.getX() + xOffset - padding), (int) (p.getY() + yOffset - 1), width + padding * 2, RENAME_FIELD_HEIGHT);
        renameField.setFont(nameFont);
        renameField.setBorder(null);
        add(renameField);
        renameField.requestFocusInWindow();
        // accepting changes on enter and focus lost
        Runnable renamer = () -> {
            if (renameField == null) {
                return;
            }
            String name = renameField.getText().trim();
            if (name.length() > 0) {
                op.rename(name);
            }
            cancelRenaming();
        };
        renameField.addActionListener(e -> renamer.run());
        renameField.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(final FocusEvent e) {
                // right-click menu
                if (e.isTemporary()) {
                    return;
                }
                renamer.run();
            }
        });
        // ignore changes on escape
        renameField.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(final KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    cancelRenaming();
                }
            }
        });

        repaint();
    }

    /**
     * Updates location and size of the extension buttons.
     */
    void updateExtensionButtons() {
        for (ExtensionButton button : subprocessExtensionButtons) {
            int subprocessIndex = button.getSubprocessIndex();
            int buttonSize = button.getWidth();
            int gap = 2 * ProcessRenderer.WALL_WIDTH;
            if (subprocessIndex >= 0) {
                Point location = ProcessDrawUtils.convertToAbsoluteProcessPoint(new Point(0, 0), subprocessIndex, model);
                int height = (int) model.getProcessHeight(model.getProcess(subprocessIndex));
                int width = (int) model.getProcessWidth(model.getProcess(subprocessIndex));
                button.setBounds(location.x + width - buttonSize - gap - (button.isAdd() ? 0 : buttonSize), location.y + height - gap - buttonSize, buttonSize,
                        buttonSize);
            } else {
                Point location = ProcessDrawUtils.convertToAbsoluteProcessPoint(new Point(0, 0), 0, model);
                int height = (int) model.getProcessHeight(model.getProcess(0));
                button.setBounds(location.x + gap, location.y + height - gap - buttonSize, buttonSize, buttonSize);
            }
        }
    }

    /**
     * Shows a popup menu if preconditions are fulfilled.
     *
     * @param e
     *            the mouse event potentially triggering the popup menu
     * @return {@code true} if a popup menu was displayed; {@code false} otherwise
     */
    boolean showPopupMenu(final MouseEvent e) {
        if (model.getConnectingPortSource() != null) {
            return false;
        }
        if (getProcessIndexUnder(e.getPoint()) == -1) {
            return false;
        }
        JPopupMenu menu = new JPopupMenu();

        // port or not port, that is the question
        final Port hoveringPort = model.getHoveringPort();
        OperatorChain displayedChain = model.getDisplayedChain();
        if (hoveringPort != null) {
            // add port actions
            final IOObject data = getModel().getCachedResultOfPort(hoveringPort);
            if (data != null) {
                JMenuItem showResult = new JMenuItem(new ResourceAction(true, "show_port_data", IOObjectService.getName(data.getClass())) {

                    private static final long serialVersionUID = -6557085878445788274L;

                    @Override
                    public void loggedActionPerformed(final ActionEvent e) {
                        MainFrame.INSTANCE.getResultDisplay().showResult(data, SwingTools.createIcon("16/mouse_pointer.png"));
                    }

                });
                menu.add(showResult);
                AbstractPipeline pipeline = model.getProcess();
                menu.add(new StoreInRepositoryAction(data, pipeline == null || pipeline.getPath() == null ? null : pipeline.getPath().getParent()));
                menu.addSeparator();
            }


            final List<ResourceAction> portActions = OperatorPortActionRegistry.INSTANCE.getPortActions(hoveringPort, data);
            for (ResourceAction action : portActions) {
                menu.add(action);
            }
            if (!portActions.isEmpty()) {
                menu.addSeparator();
            }

            List<QuickFix> fixes = hoveringPort.collectQuickFixes();
            if (!fixes.isEmpty()) {
                JMenu fixMenu = new ResourceMenu("quick_fixes");
                for (QuickFix fix : fixes) {
                    fixMenu.add(fix.getAction());
                }
                menu.add(fixMenu);
            }
            if (hoveringPort.isConnected()) {
                menu.add(new ResourceAction(true, "disconnect") {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public void loggedActionPerformed(final ActionEvent e) {
                        if (hoveringPort.isConnected()) {
                            if (hoveringPort instanceof OutputPort) {
                                ((OutputPort) hoveringPort).disconnect();
                            } else {
                                ((InputPort) hoveringPort).getSource().disconnect();
                            }
                        }
                    }
                });
            }

            Ports<? extends Port> ports = hoveringPort.getPorts();
            ExecutionUnit subprocess = displayedChain.getSubprocess(0);
            if (menu.getSubElements().length > 0) {
                firePortMenuWillOpen(menu, hoveringPort);
            }
        } else if (model.getHoveringOperator() == null && model.getHoveringConnectionSource() != null) {
            // right-clicked a connection spline
            menu.add(new ProcessChangingAction(true, "delete_connection") {

                private static final long serialVersionUID = 1L;

                @Override
                public void loggedActionPerformed(final ActionEvent e) {
                    disconnectHoveredConnection(model);
                }
            });
        } else {
            // add workflow annotation and background image actions
            int index = model.getHoveringProcessIndex();
            Action[] annotationActions = new Action[4];
            final Operator hoveredOp = model.getHoveringOperator();

            // reset zoom action if clicking on background and zoom is set
            if (hoveredOp == null && model.getZoomFactor() != 1.0) {
                menu.add(new ResourceAction(true, "processrenderer.reset_zoom") {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public void loggedActionPerformed(ActionEvent e) {
                        resetZoom();
                    }
                });
                menu.addSeparator();
            }

            if (index != -1) {
                ExecutionUnit process = model.getProcess(index);
                Point point = toProcessSpace(e.getPoint(), index);
                if (hoveredOp == null) {
                    annotationActions[0] = getAnnotationsVisualizer().makeAddProcessAnnotationAction(process, point);
                    annotationActions[1] = getAnnotationsVisualizer().getToggleAnnotationsAction();
                    annotationActions[2] = getBackgroundImageHandler().makeSetBackgroundImageAction(process);
                    if (model.getBackgroundImage(process) != null) {
                        annotationActions[3] = getBackgroundImageHandler().makeRemoveBackgroundImageAction(process);
                    }
                } else {
                    WorkflowAnnotations annotations = model.getOperatorAnnotations(hoveredOp);
                    if (annotations == null || annotations.isEmpty()) {
                        annotationActions[0] = getAnnotationsVisualizer().makeAddOperatorAnnotationAction(hoveredOp);
                    } else {
                        annotationActions[0] = getAnnotationsVisualizer().makeDetachOperatorAnnotationAction(hoveredOp);
                    }
                }
            }

            // add operator actions
            processEditorActions.addToOperatorPopupMenu(menu, annotationActions);

            // if not hovering on operator, add process panel actions
            if (hoveredOp == null) {
                menu.addSeparator();

                JMenu layoutMenu = new ResourceMenu("process_layout");

                layoutMenu.add(new ProcessChangingAction("arrange_operators") {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public void loggedActionPerformed(final ActionEvent ae) {
                        int index = getProcessIndexUnder(e.getPoint());
                        if (index == -1) {
                            for (ExecutionUnit u : model.getExecutionUnits()) {
                                userInteractionInterceptor.autoArrange(u);
                            }

                        } else {
                            userInteractionInterceptor.autoArrange(model.getProcess(index));
                        }
                    }
                });
                layoutMenu.add(ProcessActions.AUTO_FIT_ACTION);
                menu.add(layoutMenu);

                menu.addSeparator();
                String name = "Pipeline";
                if (displayedChain.getPipeline().getPath() != null) {
                    name = displayedChain.getPipeline().getPath().toShortString(150);
                }
                menu.add(PrintingTools.makeExportPrintMenu(this, name));
                if (menu.getSubElements().length > 0) {
                    fireOperatorMenuWillOpen(menu, displayedChain);
                }
            } else {
                boolean first = true;
                for (OutputPort port : hoveredOp.getOutputPorts().getAllPorts()) {
                    if (!port.isConnected())
                        continue;
                    final IOObject data = getModel().getCachedResultOfPort(port);
                    if (data == null)
                        continue;
                    if (first) {
                        menu.addSeparator();
                        first = false;
                    }
                    JMenuItem showResult = new JMenuItem(new ResourceAction(true, "show_port_data", port.getName()) {

                        private static final long serialVersionUID = -6557085878445788274L;

                        @Override
                        public void loggedActionPerformed(final ActionEvent e) {
                            MainFrame.INSTANCE.getResultDisplay().showResult(data, SwingTools.createIcon("16/mouse_pointer.png"));
                        }
                    });
                    menu.add(showResult);

                }
                if (menu.getSubElements().length > 0) {
                    fireOperatorMenuWillOpen(menu, hoveredOp);
                }
            }
        }

        // show popup
        if (menu.getSubElements().length > 0) {
            menu.show(this, e.getX(), e.getY());
            return true;
        }
        return false;
    }

    /**
     * Disconnects the hovered connection.
     *
     * @param model
     *            the {@link ProcessRendererModel}
     * @since 8.2
     */
    static void disconnectHoveredConnection(ProcessRendererModel model) {
        OutputPort port = model.getHoveringConnectionSource();
        if (port == null || !port.isConnected()) {
            return;
        }
        port.disconnect();
        model.setHoveringConnectionSource(null);
        if (port.equals(model.getSelectedConnectionSource())) {
            model.setSelectedConnectionSource(null);
        }
        model.fireMiscChanged();
    }

    /**
     * Updates the currently displayed cursor depending on hover state.
     */
    void updateCursor() {
        if (model.isHoveringOperatorName()) {
            setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        } else if (model.getHoveringOperator() != null || model.getHoveringPort() != null) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } else {
            setCursor(Cursor.getDefaultCursor());
        }
    }

    /**
     * Sets the hovering operator and updates the operator name rollout and the cursor.
     *
     * @param hoveringOperator
     *            the operator or {@code null}
     */
    void setHoveringOperator(final Operator hoveringOperator) {
        model.setHoveringOperator(hoveringOperator);
        updateCursor();
        model.fireMiscChanged();
    }

    /**
     * Removes the rename textfield and resets the focus to the view.
     */
    private void cancelRenaming() {
        if (renameField != null) {
            remove(renameField);
            renameField = null;
            // this makes sure that pressing F2 afterwards works
            // otherwise nothing is focused until the next click
            ProcessEditorPanel.this.requestFocusInWindow();
            repaint();
        }
    }

    /**
     * Inits event listeners and GUI dimensions.
     */
    private void init() {
        addMouseMotionListener(mouseHandler);
        addMouseListener(mouseHandler);
        addMouseWheelListener(mouseHandler);
        addKeyListener(keyHandler);

        // for absolute positioning of tipPane
        setLayout(null);

        setPreferredSize(new Dimension(1000, 440));
        setMinimumSize(new Dimension(100, 100));
        setMaximumSize(new Dimension(2000, 2000));
    }

    /**
     * Sets up the buttons which can be used to add/remove subprocesses of the currently displayed
     * operator chain.
     */
    private void setupExtensionButtons() {
        for (ExtensionButton button : subprocessExtensionButtons) {
            remove(button);
        }
        subprocessExtensionButtons.clear();
        if (model.getDisplayedChain().areSubprocessesExtendable()) {
            for (int index = 0; index < model.getExecutionUnits().size(); index++) {
                double width = model.getProcessWidth(model.getProcess(index)) + 1;
                Point loc = ProcessDrawUtils.convertToAbsoluteProcessPoint(new Point(0, 0), index, model);

                if (index == 0) {
                    ExtensionButton addButton2 = new ExtensionButton(model, model.getDisplayedChain(), -1, true);
                    addButton2.setBounds((int) (loc.getX() - addButton2.getPreferredSize().getWidth() + 1), (int) (loc.getY() - 1),
                            (int) addButton2.getPreferredSize().getWidth(), (int) addButton2.getPreferredSize().getHeight());
                    subprocessExtensionButtons.add(addButton2);
                    add(addButton2);
                }

                ExtensionButton addButton = new ExtensionButton(model, model.getDisplayedChain(), index, true);
                addButton.setBounds((int) (loc.getX() + width), (int) (loc.getY() - 1), (int) addButton.getPreferredSize().getWidth(),
                        (int) addButton.getPreferredSize().getHeight());
                subprocessExtensionButtons.add(addButton);
                add(addButton);

                if (model.getExecutionUnits().size() > 1) {
                    ExtensionButton deleteButton = new ExtensionButton(model, model.getDisplayedChain(), index, false);
                    deleteButton.setBounds((int) (loc.getX() + width), (int) (loc.getY() + addButton.getHeight() - 1),
                            (int) deleteButton.getPreferredSize().getWidth(), (int) deleteButton.getPreferredSize().getHeight());
                    subprocessExtensionButtons.add(deleteButton);
                    add(deleteButton);
                }
            }
        }
    }

    /**
     * Update preferred size of this {@link JComponent} and updates the subprocess extension buttons
     * as well.
     */
    private void updateComponentSize() {
        Dimension newSize = new Dimension((int) userInteractionInterceptor.getTotalWidth(), (int) userInteractionInterceptor.getTotalHeight());
        updateExtensionButtons();
        if (!newSize.equals(getPreferredSize())) {
            setPreferredSize(newSize);
            revalidate();
        }
    }

    /**
     * Notifies listeners that the operator context menu will be opened.
     *
     * @param m
     *            the menu instance
     * @param op
     *            the operator for which the menu will open
     */
    private void fireOperatorMenuWillOpen(final JPopupMenu m, final Operator op) {
        ProcessUserInteractionListener[] copy = eventListeners.getListeners(ProcessUserInteractionListener.class);
        for (ProcessUserInteractionListener l : copy) {
            l.onBeforeOperatorContextMenuOpen(m, op);
        }
    }

    /**
     * Notifies listeners that the port context menu will be opened.
     *
     * @param m
     *            the menu instance
     * @param port
     *            the port for which the menu will open
     */
    private void firePortMenuWillOpen(final JPopupMenu m, final Port port) {
        ProcessUserInteractionListener[] copy = eventListeners.getListeners(ProcessUserInteractionListener.class);
        for (ProcessUserInteractionListener l : copy) {
            l.onBeforePortContextMenuOpen(m, port);
        }
    }

    private void fireOperatorSelectionChange(Collection<Operator> operators) {
        for (ProcessUserInteractionListener editor : eventListeners.getListeners(ProcessUserInteractionListener.class)) {
            editor.onOperatorSelectionChange(new LinkedList<>(operators));
        }
    }

    /**
     * Notifies listeners that an operator has moved.
     *
     * @param op
     *            the operator that moved
     */
    private void fireOperatorMoved(final Operator op) {
        ProcessUserInteractionListener[] copy = eventListeners.getListeners(ProcessUserInteractionListener.class);
        for (ProcessUserInteractionListener l : copy) {
            l.onOperatorMoved(op);
        }
    }

    /**
     * Notifies listeners that the displayed operator chain has changed.
     *
     * @param op
     *            the new displayed chain
     */
    private void fireDisplayedChainChanged(final OperatorChain op) {
        ProcessUserInteractionListener[] copy = eventListeners.getListeners(ProcessUserInteractionListener.class);
        for (ProcessUserInteractionListener l : copy) {
            l.onDisplayedChainChanged(op);
        }
    }

    /**
     * Lets all registered {@link ProcessRendererEventDecorator}s process the mouse event for the
     * given {@link RenderPhase}. If the event is consumed by any decorator, processing will stop.
     *
     * @param type
     *            the type of the mouse event
     * @param phase
     *            the event phase we are in
     * @param e
     *            the event itself
     * @param hoverIndex
     * @return {@code true} if the event was consumed; {@code false} otherwise
     */
    private boolean processPhaseListenerMouseEvent(final MouseEventType type, RenderPhase phase, final MouseEvent e, int hoverIndex) {
        ExecutionUnit hoveredProcess = null;
        if (hoverIndex >= 0 && hoverIndex < model.getExecutionUnits().size()) {
            hoveredProcess = model.getProcess(hoverIndex);
        }

        for (ProcessRendererEventDecorator decorator : decorators.get(phase)) {
            try {
                decorator.processMouseEvent(hoveredProcess, model, type, e);
            } catch (RuntimeException e1) {
                // catch everything here
                LogService.getRoot().warn("com.owc.singularity.studio.gui.flow.processrendering.view.ProcessRendererView.decorator_error", e1);
            }

            // if the decorator consumed the event, it no longer makes sense to use it.
            if (e.isConsumed()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Lets all registered {@link ProcessRendererEventDecorator}s process the key event for the
     * given {@link RenderPhase}. If the event is consumed by any decorator, processing will stop.
     *
     * @param type
     *            the type of the key event
     * @param e
     *            the event itself
     * @param phase
     *            the event phase we are in
     * @return {@code true} if the event was consumed; {@code false} otherwise
     */
    private boolean processPhaseListenerKeyEvent(final KeyEventType type, final KeyEvent e, RenderPhase phase) {
        int hoverIndex = model.getHoveringProcessIndex();
        ExecutionUnit hoveredProcess = null;
        if (hoverIndex >= 0 && hoverIndex < model.getExecutionUnits().size()) {
            hoveredProcess = model.getProcess(hoverIndex);
        }

        for (ProcessRendererEventDecorator decorater : decorators.get(phase)) {
            try {
                decorater.processKeyEvent(hoveredProcess, type, e);
            } catch (RuntimeException e1) {
                // catch everything here
                LogService.getRoot().warn("com.owc.singularity.studio.gui.flow.processrendering.view.ProcessRendererView.decorator_error", e1);
            }
            // if the decorator consumed the event, it no longer makes sense to use it.
            if (e.isConsumed()) {
                return true;
            }
        }
        return false;
    }

    public void undo() {
        Exception e = getModel().undo();
        if (e != null) {
            SwingTools.showSimpleErrorMessage("while_changing_process", e);
        }
    }

    public void redo() {
        Exception e = getModel().redo();
        if (e != null) {
            SwingTools.showSimpleErrorMessage("while_changing_process", e);
        }
    }

    public void setZoom(double percentage) {
        if (getModel().setZoomFactor(percentage)) {
            fireProcessZoomChanged();
        }
    }

    /**
     * Tries to zoom into the process. If the largest zoom factor has already been reached, does
     * nothing.
     */
    public void zoomIn() {
        if (getModel().zoomIn()) {
            fireProcessZoomChanged();
        }
    }

    /**
     * Tries to zoom out of the process. If the smallest zoom factor has already been reached, does
     * nothing.
     */
    public void zoomOut() {
        if (getModel().zoomOut()) {
            fireProcessZoomChanged();
        }
    }

    public void resetZoom() {
        if (getModel().resetZoom()) {
            fireProcessZoomChanged();
        }
    }

    /**
     * Fire when the process zoom level has changed.
     */
    private void fireProcessZoomChanged() {
        ProcessUserInteractionListener[] listeners = eventListeners.getListeners(ProcessUserInteractionListener.class);
        if (listeners.length > 0) {
            ZoomState zoomState = getModel().getZoomState();
            for (ProcessUserInteractionListener interactionListener : listeners) {
                interactionListener.onZoomStateChange(zoomState);
            }
        }
    }

    public AnnotationsVisualizer getAnnotationsVisualizer() {
        return annotationsVisualizer;
    }

    public ProcessBackgroundImageVisualizer getBackgroundImageHandler() {
        return backgroundImageHandler;
    }

    public ProcessActions getActions() {
        return processEditorActions;
    }

    public void addProcessEditorEventListener(ProcessEditorEventListener listener) {
        getModel().registerEditorEventListener(listener);
    }

    public void removeProcessEditorEventListener(ProcessEditorEventListener listener) {
        getModel().removeEditorEventListener(listener);
    }

    public AbstractPipeline getProcess() {
        return getModel().getProcess();
    }

    public boolean isEdited() {
        return getModel().hasChanged();
    }

    public List<Operator> getSelectedOperators() {
        return getModel().getSelectedOperators();
    }

    public Operator getSelectedOperator() {
        List<Operator> selectedOperators = getModel().getSelectedOperators();
        if (selectedOperators == null || selectedOperators.isEmpty())
            return null;
        return selectedOperators.iterator().next();
    }

    public ZoomState getZoomState() {
        return getModel().getZoomState();
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        getProcessRenderer().setReadOnly(readOnly);
        transferHandler.setReadOnly(readOnly);
        annotationsVisualizer.setReadOnly(readOnly);
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void autoArrange() {
        userInteractionInterceptor.autoArrange(getModel().getExecutionUnits());
    }

    public void autoFit() {
        userInteractionInterceptor.autoFit();
    }

    public void rename() {
        Operator selectedOperator = processEditorActions.getFirstSelectedOperator();
        if (selectedOperator == null)
            return;
        rename(selectedOperator);
    }

    private class MouseEventProcessor extends MouseAdapter {

        @Override
        public void mouseMoved(final MouseEvent e) {
            // no matter what, update mouse locations first
            Point mousePosition = e.getPoint();
            model.setCurrentMousePosition(mousePosition);
            int hoveringProcessIndex = getProcessIndexUnder(mousePosition);
            model.setHoveringProcessIndex(hoveringProcessIndex);
            if (model.getHoveringProcessIndex() != -1) {
                model.setMousePositionRelativeToProcess(toProcessSpace(mousePosition, hoveringProcessIndex));
            }

            for (RenderPhase phase : RenderPhase.eventOrder()) {
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_MOVED, phase, e, hoveringProcessIndex)) {
                    return;
                }
                if (phase == RenderPhase.OPERATOR_ADDITIONS) {
                    // foreground, overlay and operator addition listeners can be notified before
                    // operator hovering
                    interactionMouseHandler.mouseMoved(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
            }
        }

        @Override
        public void mouseDragged(final MouseEvent e) {
            // no matter what, update mouse locations first
            Point mousePosition = e.getPoint();
            model.setCurrentMousePosition(mousePosition);
            int hoveringProcessIndex = getProcessIndexUnder(mousePosition);
            model.setHoveringProcessIndex(hoveringProcessIndex);
            if (model.getHoveringProcessIndex() != -1) {
                model.setMousePositionRelativeToProcess(toProcessSpace(mousePosition, hoveringProcessIndex));
            }

            for (RenderPhase phase : RenderPhase.eventOrder()) {
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_DRAGGED, phase, e, hoveringProcessIndex)) {
                    return;
                }
                if (phase == RenderPhase.OPERATOR_ADDITIONS) {
                    // foreground, overlay and operator addition listeners can be notified before
                    // operator hovering
                    interactionMouseHandler.mouseDragged(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
            }
        }

        @Override
        public void mousePressed(final MouseEvent e) {
            // whatever we pressed the mouse on, remove the rename field
            if (renameField != null) {
                remove(renameField);
            }

            requestFocusInWindow();

            int hoveringProcessIndex = getProcessIndexUnder(e.getPoint());
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_PRESSED, phase, e, hoveringProcessIndex)) {
                    return;
                }
                if (phase == RenderPhase.OPERATOR_ADDITIONS) {
                    // foreground, overlay and operator addition listeners can be notified before
                    // operator hovering
                    interactionMouseHandler.mousePressed(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
                if (phase == RenderPhase.ANNOTATIONS) {
                    interactionMouseHandler.mousePressedBackground(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
            }
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            model.setDragStarted(false);
            int hoveringProcessIndex = getProcessIndexUnder(e.getPoint());
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_RELEASED, phase, e, hoveringProcessIndex)) {
                    return;
                }
                if (phase == RenderPhase.OPERATOR_ADDITIONS) {
                    // foreground, overlay and operator addition listeners can be notified before
                    // operator hovering
                    interactionMouseHandler.mouseReleased(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
                if (phase == RenderPhase.ANNOTATIONS) {
                    interactionMouseHandler.mouseReleasedBackground(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
            }
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
            int hoveringProcessIndex = getProcessIndexUnder(e.getPoint());
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_CLICKED, phase, e, hoveringProcessIndex)) {
                    return;
                }
                if (phase == RenderPhase.OPERATOR_ADDITIONS) {
                    // foreground, overlay and operator addition listeners can be notified before
                    // operator hovering
                    interactionMouseHandler.mouseClicked(e);
                    if (e.isConsumed()) {
                        return;
                    }
                }
            }
        }

        @Override
        public void mouseEntered(final MouseEvent e) {
            int hoveringProcessIndex = getProcessIndexUnder(e.getPoint());

            // first come, first served, no limit to specific phases
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                // abort if event was consumed
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_ENTERED, phase, e, hoveringProcessIndex)) {
                    return;
                }
            }
        }

        @Override
        public void mouseExited(final MouseEvent e) {
            if (!SwingTools.isMouseEventExitedToChildComponents(ProcessEditorPanel.this, e)) {
                model.setCurrentMousePosition(null);
            }

            // always reset status text
            userInteractionInterceptor.clearStatus();
            int hoveringProcessIndex = getProcessIndexUnder(e.getPoint());
            // first come, first served, no limit to specific phases
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                // abort if event was consumed
                if (processPhaseListenerMouseEvent(MouseEventType.MOUSE_EXITED, phase, e, hoveringProcessIndex)) {
                    return;
                }
            }
        }
    }

    private class KeyEventProcessor extends KeyAdapter {

        @Override
        public void keyPressed(final KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_UP:
                case KeyEvent.VK_DOWN:
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.FOREGROUND)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OVERLAY)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OPERATOR_ADDITIONS)) {
                        return;
                    }

                    // operator phase event, no more decorator processing afterwards
                    userInteractionInterceptor.selectInDirection(e);
                    e.consume();
                    break;
                case KeyEvent.VK_ESCAPE:
                    // remove currently dragged connection. Afterwards: first come, first served
                    boolean abortedConnection = false;
                    if (model.getConnectingPortSource() != null) {
                        model.setConnectingPortSource(null);
                        model.fireMiscChanged();
                        abortedConnection = true;
                    }

                    // process render phases that come before the OPERATORS phase, abort in case the
                    // event was consumed
                    for (RenderPhase phase : new RenderPhase[] { RenderPhase.FOREGROUND, RenderPhase.OVERLAY, RenderPhase.OPERATOR_ADDITIONS }) {
                        if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, phase)) {
                            return;
                        }
                    }

                    // OPERATORS phase event in case we are in a nested operator (consume event and
                    // abort)
                    if (!abortedConnection && model.getDisplayedChain().getRoot() != model.getDisplayedChain()) {
                        OperatorChain parent = model.getDisplayedChain().getParent();
                        if (parent != null) {
                            model.setDisplayedChainAndFire(parent);
                        }
                        e.consume();
                        return;
                    }

                    // remaining render phases, abort in case the event was consumed
                    for (RenderPhase phase : new RenderPhase[] { RenderPhase.CONNECTIONS, RenderPhase.OPERATOR_BACKGROUND, RenderPhase.OPERATOR_ANNOTATIONS,
                            RenderPhase.ANNOTATIONS, RenderPhase.BACKGROUND }) {
                        if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, phase)) {
                            return;
                        }
                    }

                    break;
                case KeyEvent.VK_ENTER:
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.FOREGROUND)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OVERLAY)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OPERATOR_ADDITIONS)) {
                        return;
                    }

                    // operator phase event, no more decorator processing afterwards
                    if (!model.getSelectedOperators().isEmpty()) {
                        Operator selected = model.getSelectedOperators().get(0);
                        if (selected instanceof OperatorChain && e.getModifiersEx() != InputEvent.ALT_DOWN_MASK) {
                            // dive into operator chain, unless user has pressed ALT key. ALT +
                            // ENTER = activate primary parameter
                            ActionStatisticsCollector.getInstance().logOperatorDoubleClick(selected, ActionStatisticsCollector.OPERATOR_ACTION_OPEN);
                            model.setDisplayedChainAndFire((OperatorChain) selected);
                        } else {
                            // look for a primary parameter, and activate it if found
                            ParameterType primaryParameter = selected.getPrimaryParameter();
                            ActionStatisticsCollector.getInstance()
                                    .logOperatorDoubleClick(selected, ActionStatisticsCollector.OPERATOR_ACTION_PRIMARY_PARAMETER);
                            if (primaryParameter != null) {
                                PropertyValueCellEditor editor = MainFrame.INSTANCE.getOperatorPropertyPanel().getEditorForKey(primaryParameter.getKey());
                                if (editor != null) {
                                    editor.activate();
                                }
                            }
                        }
                    }
                    e.consume();
                    break;
                case KeyEvent.VK_BACK_SPACE:
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.FOREGROUND)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OVERLAY)) {
                        return;
                    }
                    if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, RenderPhase.OPERATOR_ADDITIONS)) {
                        return;
                    }

                    // OS X: Trigger deletion action.
                    // Other: Navigate up (same as ESC).
                    if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
                        DELETE_SELECTED_CONNECTION_ACTION.actionPerformed(null);
                    } else if (model.getDisplayedChain().getRoot() != model.getDisplayedChain()) {
                        OperatorChain parent = model.getDisplayedChain().getParent();
                        if (parent != null) {
                            model.setDisplayedChainAndFire(parent);
                        }
                    }
                    // operator phase event, no more decorator processing afterwards
                    e.consume();

                    break;
                default:
                    // first come, first served, no limit to specific phases
                    for (RenderPhase phase : RenderPhase.eventOrder()) {
                        // abort if event was consumed
                        if (processPhaseListenerKeyEvent(KeyEventType.KEY_PRESSED, e, phase)) {
                            return;
                        }
                    }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (isReadOnly())
                return;
            // first come, first served
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                // abort if event was consumed
                if (processPhaseListenerKeyEvent(KeyEventType.KEY_RELEASED, e, phase)) {
                    return;
                }
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (isReadOnly())
                return;
            // first come, first served
            for (RenderPhase phase : RenderPhase.eventOrder()) {
                // abort if event was consumed
                if (processPhaseListenerKeyEvent(KeyEventType.KEY_TYPED, e, phase)) {
                    return;
                }
            }
        }
    }

    private class ModelEventProcessor implements ProcessEditorModelEventListener, ProcessEditorEventListener {

        @Override
        public void modelChanged(ProcessRendererModelEvent e) {
            switch (e.getEventType()) {
                case DISPLAYED_CHAIN_CHANGED:
                    userInteractionInterceptor.processDisplayedChainChanged();

                    // notify registered listeners
                    fireDisplayedChainChanged((OperatorChain) e.getArgs()[0]);
                    break;
                case DISPLAYED_EXECUTION_UNITS_CHANGED:
                    userInteractionInterceptor.setInitialSizes();
                    setupExtensionButtons();
                    break;
                case PROCESS_SIZE_CHANGED:
                    cancelRenaming();
                    SwingUtilities.invokeLater(() -> {
                        updateComponentSize();
                        repaint();
                    });
                    break;
                case MISC_CHANGED:
                    repaint();
                    break;
                default:
                    break;
            }

        }

        @Override
        public void operatorsChanged(ProcessRendererOperatorEvent e, Collection<Operator> operators) {
            switch (e.getEventType()) {
                case SELECTED_OPERATORS_CHANGED:
                    Operator firstOp = !operators.isEmpty() ? operators.iterator().next() : null;
                    if (firstOp != null) {
                        // only switch displayed chain if not in selected chain and selected op
                        // is not visible
                        OperatorChain displayChain = (OperatorChain) (firstOp instanceof AbstractRootOperator ? firstOp
                                : model.getDisplayedChain() == firstOp ? firstOp : firstOp.getParent());
                        if (displayChain != null && model.getDisplayedChain() != displayChain) {
                            model.setDisplayedChainAndFire(displayChain);
                            return;
                        }
                    }
                    repaint();
                    fireOperatorSelectionChange(operators);
                    break;
                case OPERATORS_MOVED:
                    Set<Operator> opsToMove = new LinkedHashSet<>(operators);
                    boolean wasResized = false;
                    boolean shouldMoveOperators = Boolean
                            .parseBoolean(PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_GUI_MOVE_CONNECTED_OPERATORS));
                    Set<WorkflowAnnotation> movedAnnotations = new HashSet<>();
                    while (!opsToMove.isEmpty()) {
                        Iterator<Operator> iterator = opsToMove.iterator();
                        Operator op = iterator.next();
                        iterator.remove();
                        ExecutionUnit executionUnit = op.getExecutionUnit();

                        if (shouldMoveOperators && !Tools.isOperatorInCircle(op, -1)) {

                            List<Operator> leftConnectedOperators = getDirectlyConnectedPorts(op.getInputPorts(), InputPort::getSource, executionUnit,
                                    Collections.emptyList());
                            List<Operator> rightConnectedOperators = getDirectlyConnectedPorts(op.getOutputPorts(), OutputPort::getDestination, executionUnit,
                                    opsToMove);

                            final Rectangle2D operatorRect = ProcessLayoutXMLFilter.lookupOperatorRectangle(op);
                            final Rectangle2D oldOperatorRect = (Rectangle2D) operatorRect.clone();

                            // check it does not collide with other operators and move it to the
                            // right if necessary
                            leftConnectedOperators.stream()
                                    .map(ProcessLayoutXMLFilter::lookupOperatorRectangle)
                                    .filter(r -> r != null && isOverlapping(r, operatorRect))
                                    .mapToDouble(r -> r.getX() + ProcessRenderer.GRID_AUTOARRANGE_WIDTH - 1)
                                    .filter(x -> operatorRect.getX() < x)
                                    .max()
                                    .ifPresent(x -> {
                                        double diff = operatorRect.getX() - x;
                                        operatorRect.setRect(x, operatorRect.getY(), operatorRect.getWidth(), operatorRect.getHeight());
                                        model.setOperatorRect(op, operatorRect);
                                        final WorkflowAnnotations operatorAnnotations = model.getOperatorAnnotations(op);
                                        if (operatorAnnotations != null && !operatorAnnotations.isEmpty()) {
                                            List<WorkflowAnnotation> annotationsEventOrder = operatorAnnotations.getAnnotationsEventOrder();
                                            annotationsEventOrder.stream()
                                                    .filter(anno -> anno instanceof OperatorAnnotation)
                                                    .map(WorkflowAnnotation::getLocation)
                                                    .forEach(r -> r.setRect(r.getX() - diff, r.getY(), r.getWidth(), r.getHeight()));
                                            movedAnnotations.addAll(annotationsEventOrder);
                                        }
                                    });

                            // check all connected operators to the right also
                            // only if this operator really moved either manually or by this
                            // feature
                            if (!oldOperatorRect.equals(operatorRect) || operators.contains(op)) {
                                opsToMove.addAll(rightConnectedOperators);
                            }
                        }
                        wasResized |= userInteractionInterceptor.ensureProcessSizeFits(executionUnit, ProcessLayoutXMLFilter.lookupOperatorRectangle(op));
                        // notify registered listeners
                        fireOperatorMoved(op);
                    }

                    if (!movedAnnotations.isEmpty()) {
                        model.fireAnnotationsMoved(movedAnnotations);
                    }

                    // need to repaint if process was not resized
                    if (!wasResized) {
                        repaint();
                    }
                    break;
                case PORTS_CHANGED:
                    for (Operator op : operators) {
                        // trigger calculation of new op height
                        userInteractionInterceptor.processPortsChanged(op);
                    }
                    // we need to reposition the annotations AFTER the relevant changes to the
                    // operators
                    List<WorkflowAnnotation> movedAnnos = AnnotationEventHook.positionOperatorAnnotations(operators, model);
                    model.fireAnnotationsMoved(movedAnnos);
                    repaint();
                    break;
                default:
                    break;
            }
        }

        /** @since 9.1 */
        private <P extends Port> List<Operator> getDirectlyConnectedPorts(Ports<P> ports, Function<P, Port> opposite, ExecutionUnit executionUnit,
                Collection<Operator> exclude) {
            // get all connected ports, find their operator, make sure it is an operator on the
            // same process level and is not already moved
            return ports.getAllPorts()
                    .stream()
                    .filter(Port::isConnected)
                    .map(port -> opposite.apply(port).getPorts().getOwner().getOperator())
                    .filter(co -> executionUnit == co.getExecutionUnit() && !exclude.contains(co))
                    .distinct()
                    .collect(Collectors.toList());
        }

        /** @since 9.2.1 */
        private boolean isOverlapping(Rectangle2D a, Rectangle2D b) {
            return isOverlappingTop(a, b) || isOverlappingTop(b, a);
        }

        /** @since 9.2.1 */
        private boolean isOverlappingTop(Rectangle2D top, Rectangle2D bottom) {
            return top.getY() <= bottom.getY() && bottom.getY() <= top.getMaxY();
        }

        @Override
        public void annotationsChanged(ProcessRendererAnnotationEvent e, Collection<WorkflowAnnotation> annotations) {
            switch (e.getEventType()) {
                case SELECTED_ANNOTATION_CHANGED:
                case MISC_CHANGED:
                    repaint();
                    break;
                case ANNOTATIONS_MOVED:
                    for (WorkflowAnnotation anno : annotations) {
                        boolean wasResized = userInteractionInterceptor.ensureProcessSizeFits(anno.getProcess(), anno.getLocation());

                        // need to repaint if process was not resized
                        if (!wasResized) {
                            repaint();
                        }
                    }
                    break;
                default:
                    break;

            }

        }

        @Override
        public void onProcessEdit(AbstractPipeline process) {
            SwingUtilities.invokeLater(ProcessEditorPanel.this::repaint);
        }
    }
}
