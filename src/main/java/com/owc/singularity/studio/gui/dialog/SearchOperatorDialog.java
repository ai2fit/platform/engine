/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.dialog;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.Serial;
import java.util.*;
import java.util.List;

import javax.swing.*;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceActionAdapter;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * The search dialog for searching operator names of the current pipeline panel. This dialog is very
 * similar to the {@link SearchDialog}, which is used to search log messages.
 *
 * @author Alexander Mahler
 */
public class SearchOperatorDialog extends ButtonDialog {

    private final JTextField patternField = new JTextField(20);

    private final JCheckBox caseSensitive = new JCheckBox(new ResourceActionAdapter("case_sensitive"));

    private final JRadioButton forwardRadioButton = new JRadioButton(new ResourceActionAdapter("search_forward"));

    private final JRadioButton backwardRadioButton = new JRadioButton(new ResourceActionAdapter("search_backward"));

    public SearchOperatorDialog(Component owner) {
        super(owner != null ? SwingUtilities.getWindowAncestor(owner) : null, "search_operator", ModalityType.MODELESS);
        JPanel searchPanel = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(4, 0, 4, 0);
        c.gridwidth = 1;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 0;
        JLabel label = new ResourceLabel("search_what");
        searchPanel.add(label, c);

        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridx = 1;
        c.gridy = 0;
        searchPanel.add(patternField, c);
        label.setLabelFor(patternField);

        Collection<AbstractButton> buttons = new LinkedList<>();
        final JButton search = new JButton(new ResourceAction("start_search") {

            @Serial
            private static final long serialVersionUID = -1869521694209126702L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                search();
            }
        });
        buttons.add(search);

        GridLayout settingsPanelLayout = new GridLayout(1, 2);
        settingsPanelLayout.setVgap(0);
        settingsPanelLayout.setHgap(0);
        JPanel settingsPanel = new JPanel(settingsPanelLayout);
        GridLayout directionPanelLayout = new GridLayout(2, 1);
        directionPanelLayout.setVgap(0);
        directionPanelLayout.setHgap(0);
        JPanel directionPanel = new JPanel(directionPanelLayout);
        directionPanel.setBorder(BorderFactory.createTitledBorder("Direction"));
        ButtonGroup directionGroup = new ButtonGroup();
        directionGroup.add(forwardRadioButton);
        directionGroup.add(backwardRadioButton);
        forwardRadioButton.setSelected(true);
        directionPanel.add(forwardRadioButton);
        directionPanel.add(backwardRadioButton);
        settingsPanel.add(directionPanel);

        JPanel optionsPanel = new JPanel(new GridLayout(2, 1));
        optionsPanel.setBorder(BorderFactory.createTitledBorder("Options"));
        optionsPanel.add(caseSensitive);
        settingsPanel.add(optionsPanel);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridx = 0;
        c.gridy = 2;
        searchPanel.add(settingsPanel, c);

        buttons.add(makeCloseButton());
        layoutDefault(searchPanel, buttons);
        getRootPane().setDefaultButton(search);
    }

    private void search() {
        String pattern = patternField.getText().trim();
        if (pattern.isEmpty()) {
            return;
        }
        ProcessEditorPanel editorPanel = MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor();
        Collection<String> operatorNames = editorPanel.getProcess().getAllOperatorNames();
        List<String> sortedOperatorNames = operatorNames.stream().sorted().toList();
        Operator selectedOperator = editorPanel.getSelectedOperator();
        int startPos;
        if (selectedOperator == null) {
            startPos = 0;
        } else {
            String selectedOperatorName = selectedOperator.getName();
            if (operatorNames.contains(selectedOperatorName)) {
                startPos = sortedOperatorNames.indexOf(selectedOperatorName) + 1;
            } else {
                startPos = 0;
            }
        }

        if (forwardRadioButton.isSelected()) {
            String result = search(startPos, pattern, sortedOperatorNames);
            if (result == null) {
                noMoreHits();
            } else {
                editorPanel.selectAndShowOperator(editorPanel.getProcess().getOperator(result), true);
            }
        } else {
            String lastResult = null;
            int pos = 0;
            while (true) {
                String result = search(pos, pattern, sortedOperatorNames);
                if (result != null) {
                    int indexOfResult = sortedOperatorNames.indexOf(result);
                    if (indexOfResult < startPos - 1) {
                        pos = indexOfResult + 1;
                        lastResult = result;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            if (lastResult == null) {
                noMoreHits();
            } else {
                editorPanel.selectAndShowOperator(editorPanel.getProcess().getOperator(lastResult), true);
            }
        }
    }

    private String search(int start, String pattern, List<String> sortedOperatorNames) {
        if (!caseSensitive.isSelected()) {
            pattern = pattern.toLowerCase();
        }
        for (int i = start; i < sortedOperatorNames.size(); i++) {
            String operatorName = caseSensitive.isSelected() ? sortedOperatorNames.get(i) : sortedOperatorNames.get(i).toLowerCase();
            if (operatorName.contains(pattern)) {
                return sortedOperatorNames.get(i);
            }
        }
        return null;
    }

    private void noMoreHits() {
        String restartAt = backwardRadioButton.isSelected() ? "end" : "beginning";
        switch (SwingTools.showConfirmDialog(SearchOperatorDialog.this, "editor.search_replace.no_more_hits", ConfirmDialog.YES_NO_OPTION, restartAt)) {
            case ConfirmDialog.YES_OPTION:
                ProcessEditorPanel editorPanel = MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor();
                List<Operator> operators = editorPanel.getProcess().getAllOperators().stream().sorted(Comparator.comparing(Operator::getName)).toList();
                if (!backwardRadioButton.isSelected()) {
                    editorPanel.selectAndShowOperator(operators.get(0), false);
                } else {
                    editorPanel.selectAndShowOperator(operators.get(operators.size() - 1), false);
                }
                search();
                break;
            case ConfirmDialog.NO_OPTION:
            default:
        }
    }
}
