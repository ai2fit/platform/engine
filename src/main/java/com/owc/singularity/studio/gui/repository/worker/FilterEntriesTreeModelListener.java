package com.owc.singularity.studio.gui.repository.worker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.repository.model.AbstractRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;

/**
 * A model listener that will listen to changes on the model data and will reflect the structure
 * changes accordingly but only of entries that were accepted by the view filter.
 */
public class FilterEntriesTreeModelListener implements TreeModelListener {

    private static final Logger log = LogManager.getLogger(FilterEntriesTreeModelListener.class);
    private final AbstractRepositoryTreeModel treeFilteredModel;
    private final Predicate<RepositoryTreeNode> filter;

    public FilterEntriesTreeModelListener(AbstractRepositoryTreeModel treeFilteredModel, Predicate<RepositoryTreeNode> filter) {
        this.treeFilteredModel = treeFilteredModel;
        this.filter = filter;
    }

    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        TreePath treePath = e.getTreePath();
        if (treePath == null)
            return;
        RepositoryTreeNode parentNode = (RepositoryTreeNode) treePath.getLastPathComponent();
        if (e.getChildren() == null) {
            // this means root has changes
            RepositoryTreeNode modelNodeForModifiedNode = treeFilteredModel.getNodeFor(parentNode);
            treeFilteredModel.nodesChanged(modelNodeForModifiedNode, null);
        } else {
            if (!filter.test(parentNode)) {
                // the model do not consider the parent node
                // no need to change anything
                return;
            }
            RepositoryTreeNode[] modifiedChildren = getChildren(e);
            List<RepositoryTreeNode> children = new ArrayList<>(modifiedChildren.length);
            RepositoryTreeNode modelNodeForParentNode = treeFilteredModel.getNodeFor(parentNode);
            for (RepositoryTreeNode originalModifiedNode : modifiedChildren) {
                RepositoryTreeNode modelNodeForModifiedNode = treeFilteredModel.getNodeFor(originalModifiedNode);
                if (modelNodeForModifiedNode != null) {
                    children.add(modelNodeForModifiedNode);
                }
            }
            new TreeChange.Modification(modelNodeForParentNode, children.toArray(RepositoryTreeNode[]::new)).applyOn(treeFilteredModel);
        }
    }

    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        TreePath treePath = e.getTreePath();
        if (treePath == null)
            return;
        RepositoryTreeNode parentNode = (RepositoryTreeNode) treePath.getLastPathComponent();
        if (!filter.test(parentNode))
            return;
        List<TreeChange> changes = new LinkedList<>();
        RepositoryTreeNode modelNodeForParent = treeFilteredModel.getNodeFor(parentNode);
        if (modelNodeForParent == null) {
            // we should add this new entry
            Stack<RepositoryTreeNode> unknownParents = new Stack<>();
            unknownParents.push(parentNode);
            RepositoryTreeNode knownParent = null;
            while (!unknownParents.isEmpty()) {
                RepositoryTreeNode originalParentNode = unknownParents.pop();
                knownParent = treeFilteredModel.getNodeFor(originalParentNode);
                if (knownParent != null) {
                    // we found a known parent in the path
                    // add all unknown ancestors
                    while (!unknownParents.isEmpty()) {
                        RepositoryTreeNode originalAncestor = unknownParents.pop();
                        RepositoryTreeNode clonedAncestor = originalAncestor.clone();
                        treeFilteredModel.addNode(knownParent, clonedAncestor);
                        changes.add(new TreeChange.Insertion(knownParent, new RepositoryTreeNode[] { clonedAncestor }));
                        knownParent = clonedAncestor;
                    }
                } else {
                    if (originalParentNode.getParent() == null) {
                        // we reached out for the root path, and we could not find
                        // any common ancestor
                        log.warn(LogService.MARKER_DEVELOPMENT, "{} could not find any common ancestor for path {}", getClass().getSimpleName(),
                                parentNode.getRepositoryPath());
                        return;
                    }
                    // try to go up a level by trying out the parent node
                    unknownParents.push(originalParentNode);
                    unknownParents.push(originalParentNode.getParent());
                }
            }
            modelNodeForParent = knownParent;
        }
        RepositoryTreeNode[] newNodes = getChildren(e);
        List<RepositoryTreeNode> newChildren = new ArrayList<>(newNodes.length);
        for (RepositoryTreeNode newNode : newNodes) {
            if (treeFilteredModel.containsNodeFor(newNode)) {
                // already registered, this operation is redundant
                log.trace(LogService.MARKER_DEVELOPMENT, "Cannot register new entry at ''{}''. The entry is already registered.", newNode.getRepositoryPath());
                continue;
            }
            if (!filter.test(newNode)) {
                // node is filtered out
                continue;
            }
            RepositoryTreeNode modelNodeForChild = newNode.clone();
            newChildren.add(modelNodeForChild);
            treeFilteredModel.addNode(modelNodeForParent, modelNodeForChild);
        }
        if (newChildren.isEmpty())
            return;
        // we find the indices out only after adding all children
        // since items could be added with the same index as older items
        changes.add(new TreeChange.Insertion(modelNodeForParent, newChildren.toArray(RepositoryTreeNode[]::new)));
        for (TreeChange change : changes.stream().collect(TreeChange.combining())) {
            change.applyOn(treeFilteredModel);
        }
    }

    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        TreePath treePath = e.getTreePath();
        if (treePath == null)
            return;
        RepositoryTreeNode parentNode = (RepositoryTreeNode) treePath.getLastPathComponent();
        RepositoryTreeNode[] removedChildren = getChildren(e);
        RepositoryTreeNode modelNodeForParent = treeFilteredModel.getNodeFor(parentNode);
        if (!filter.test(parentNode)) {
            // check whether their parent was accepted before the deletion
            if (modelNodeForParent != null) {
                // after deletion of entry, parent is also filtered out
                // we remove the parent and do not bother with the rest
                int index = modelNodeForParent.getParent().getIndex(modelNodeForParent);
                treeFilteredModel.removeNode(modelNodeForParent);
                new TreeChange.Removal(modelNodeForParent.getParent(), new RepositoryTreeNode[] { modelNodeForParent }, new int[] { index })
                        .applyOn(treeFilteredModel);
                return;
            }
            // the model do not consider the parent node
            // no need to change anything
            return;
        }
        List<RepositoryTreeNode> modelChildrenMarkedForRemoval = new ArrayList<>(removedChildren.length);
        for (RepositoryTreeNode removedNode : removedChildren) {
            RepositoryTreeNode modelNodeForRemovedNode = treeFilteredModel.getNodeFor(removedNode);
            if (modelNodeForRemovedNode != null) {
                modelChildrenMarkedForRemoval.add(modelNodeForRemovedNode);
            }
        }
        if (modelChildrenMarkedForRemoval.isEmpty()) {
            return;
        }
        int[] indices = modelChildrenMarkedForRemoval.stream().mapToInt(modelNodeForParent::getIndex).sorted().toArray();
        for (RepositoryTreeNode node : modelChildrenMarkedForRemoval) {
            treeFilteredModel.removeNode(node);
        }
        new TreeChange.Removal(modelNodeForParent, modelChildrenMarkedForRemoval.toArray(RepositoryTreeNode[]::new), indices).applyOn(treeFilteredModel);
    }

    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        TreePath treePath = e.getTreePath();
        if (treePath == null)
            return;
        RepositoryTreeNode node = (RepositoryTreeNode) treePath.getLastPathComponent();
        if (!filter.test(node))
            return;

        RepositoryTreeNode modelNode = treeFilteredModel.getNodeFor(node);
        if (modelNode == null)
            return;
        throw new UnsupportedOperationException();
        /*
         * RepositoryTreeModelWatchWorker.EntryDeletionWorkItem deletionWorkItem = new
         * RepositoryTreeModelWatchWorker.EntryDeletionWorkItem(modelNode.getParent(), new
         * RepositoryTreeNode[] { modelNode }); List<TreeChange> changes = new
         * LinkedList<>(deletionWorkItem.getChanges(treeFilteredModel, filter));
         * RepositoryTreeModelWatchWorker.EntryCreationWorkItem creationWorkItem = new
         * RepositoryTreeModelWatchWorker.EntryCreationWorkItem(modelNode.getParent(), new
         * RepositoryTreeNode[] { node });
         * changes.addAll(creationWorkItem.getChanges(treeFilteredModel, filter)); int childCount =
         * node.getChildCount(); if (childCount > 0) { RepositoryTreeNode[] children = new
         * RepositoryTreeNode[childCount]; for (int i = 0; i < childCount; i++) { children[i] =
         * (RepositoryTreeNode) node.getChildAt(i); }
         * RepositoryTreeModelWatchWorker.EntryCreationWorkItem childrenCreationWorkItem = new
         * RepositoryTreeModelWatchWorker.EntryCreationWorkItem(node, children);
         * changes.addAll(childrenCreationWorkItem.getChanges(treeFilteredModel, filter)); } return
         * changes;
         */
    }

    @SuppressWarnings("SuspiciousSystemArraycopy")
    private RepositoryTreeNode[] getChildren(TreeModelEvent e) {
        Object[] original = e.getChildren();
        RepositoryTreeNode[] copy = new RepositoryTreeNode[original.length];
        System.arraycopy(original, 0, copy, 0, original.length);
        return copy;
    }

}
