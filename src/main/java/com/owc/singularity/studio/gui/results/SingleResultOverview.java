/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.results;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import com.owc.singularity.engine.IOObjectCacheService;
import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.object.Renderable;
import com.owc.singularity.engine.object.Reportable;
import com.owc.singularity.engine.object.Reporter;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.ReferenceCache;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawUtils;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.look.RapidLookAndFeel;
import com.owc.singularity.studio.gui.tools.ExtendedHTMLJEditorPane;
import com.owc.singularity.studio.gui.tools.MultiSwingWorker;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Displays an overview of a single IOObject. Does not remember the IOObject itself.
 *
 * @author Simon Fischer, Marco Boeck
 *
 */
public class SingleResultOverview extends JPanel {

    private static final int MAX_RESULT_STRING_LENGTH = 2048;

    private static final long serialVersionUID = 1L;

    private final JLabel title;
    private final Component main;

    private final ReferenceCache<IOObject>.Reference ioObject;

    static final int MIN_HEIGHT = 300;
    static final int MIN_WIDTH = 300;

    private final Action OPEN_DATA = new ResourceAction("resulthistory.open_data") {

        private static final long serialVersionUID = 1L;

        @Override
        public void loggedActionPerformed(ActionEvent e) {
            IOObject referenced = ioObject.get();
            if (referenced != null) {
                MainFrame.INSTANCE.getResultDisplay().showResult(referenced, SwingTools.createIcon("16/folder.png"));
            }
        }
    };

    private BufferedImage img;

    public SingleResultOverview(IOObject result, AbstractPipeline process, int resultIndex) {
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.anchor = GridBagConstraints.NORTHWEST;

        MetaData metaData = MetaData.forIOObject(result);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5),
                new RapidBorder(ProcessDrawUtils.getColorFor(metaData), RapidLookAndFeel.CORNER_DEFAULT_RADIUS, 25)));
        setBackground(Colors.WHITE);

        this.ioObject = IOObjectCacheService.cache(result);

        String name = IOObjectService.getName(result.getClass());
        main = makeTextRenderer(result);

        StringBuilder b = new StringBuilder();
        b.append("<html><strong>").append(name);
        b.append("</strong>");
        if (result.getSource() != null) {
            b.append(" (").append(result.getSource()).append(")");
        }
        b.append("</html>");
        title = new JLabel(b.toString());
        add(title, gbc);

        main.setPreferredSize(new Dimension(MIN_WIDTH - 10, MIN_HEIGHT - 65));
        gbc.gridy += 1;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(main, gbc);

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showContextMenu(e.getPoint());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showContextMenu(e.getPoint());
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showContextMenu(e.getPoint());
                }
            }

            private void showContextMenu(Point point) {
                JPopupMenu menu = new JPopupMenu();
                boolean empty = true;

                if (ioObject != null && ioObject.get() != null) {
                    menu.add(OPEN_DATA);
                    empty = false;
                }
                if (!empty) {
                    menu.show(SingleResultOverview.this, (int) point.getX(), (int) point.getY());
                }
            }
        });

        addAncestorListener(new AncestorListener() {

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                // not needed
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
                // not needed
            }

            @Override
            public void ancestorAdded(AncestorEvent event) {
                // update image (correct size is now known)
                updatePreviewImage();
            }
        });

    }

    /**
     * Create a text renderer for this result.
     *
     * @param result
     * @return
     */
    private Component makeTextRenderer(IOObject result) {
        String resultString = result.toResultString();
        if (resultString.length() > MAX_RESULT_STRING_LENGTH) {
            resultString = resultString.substring(0, MAX_RESULT_STRING_LENGTH);
        }
        return makeMainLabel("<html><pre>" + resultString + "</pre></html>");
    }

    /**
     * Creates the main text representation of this result.
     *
     * @param text
     * @return
     */
    private Component makeMainLabel(String text) {
        JEditorPane label = new ExtendedHTMLJEditorPane("text/html", text);
        StyleSheet css = ((HTMLEditorKit) label.getEditorKit()).getStyleSheet();
        css.addRule("body {font-family:Sans;font-size:11pt}");
        css.addRule("h3 {margin:0; padding:0}");
        css.addRule("h4 {margin-bottom:0; margin-top:1ex; padding:0}");
        css.addRule("p  {margin-top:0; margin-bottom:1ex; padding:0}");
        css.addRule("ul {margin-top:0; margin-bottom:1ex; list-style-image: url(" + Tools.getResource("icons/help/circle.png") + ")}");
        css.addRule("ul li {padding-bottom: 2px}");
        css.addRule("li.outPorts {padding-bottom: 0px}");
        css.addRule("ul li ul {margin-top:0; margin-bottom:1ex; list-style-image: url(" + Tools.getResource("icons/help/line.png") + ")");
        css.addRule("li ul li {padding-bottom:0}");

        label.setEditable(false);
        label.setBackground(Colors.WHITE);

        JScrollPane pane = new JScrollPane(label);
        pane.setBackground(Colors.WHITE);
        pane.setBorder(null);
        return pane;
    }

    /**
     * Updates the preview renderable image in a {@link MultiSwingWorker}.
     */
    private void updatePreviewImage() {
        final IOObject result = ioObject != null ? ioObject.get() : null;
        if (result != null) {
            String name = IOObjectService.getName(result.getClass());
            final List<Reporter> reporters = IOObjectService.getReporters(name);
            if (reporters.isEmpty()) {
                return;
            }

            MultiSwingWorker<Void, Void> sw = new MultiSwingWorker<Void, Void>() {

                @Override
                protected Void doInBackground() throws Exception {
                    int width = Math.max(getSize().width, MIN_WIDTH);
                    int height = Math.max(getSize().height, MIN_HEIGHT);
                    for (Reporter renderer : reporters) {
                        Reportable reportable = renderer.createReportable(result, 800, 600);
                        if (reportable instanceof Renderable renderable) {
                            renderable.prepareRendering();
                            int preferredWidth = renderable.getRenderWidth(800);
                            int preferredHeight = renderable.getRenderHeight(600);

                            img = new BufferedImage(preferredWidth, preferredHeight, BufferedImage.TYPE_INT_RGB);
                            Graphics2D graphics = (Graphics2D) img.getGraphics();
                            graphics.setColor(Colors.WHITE);
                            graphics.fillRect(0, 0, 5000, 3000);
                            double scale = Math.min((double) width / (double) preferredWidth, (double) height / (double) preferredHeight);
                            graphics.scale(scale, scale);
                            renderable.render(graphics, preferredWidth, preferredHeight);

                            graphics.dispose();
                            break;
                        }
                    }

                    return null;
                }

                @Override
                public void done() {
                    main.repaint();
                }
            };
            sw.start();
        }
    }
}
