/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui;


import java.util.List;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.OperatorPath;
import com.owc.singularity.engine.pipeline.PipelineState;
import com.owc.singularity.engine.pipeline.io.OperatorPathTreePrinter;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.tools.ProcessGUITools;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * A Thread for running an process in the SingularityGUI. This thread is necessary in order to keep
 * the GUI running (and working). If you want to perform an process in its own thread from your own
 * program simply use a Java Thread peforming the method process.run() in its run()-method.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
public class LocalPipelineExecutor {

    private static final Logger log = LogService.getI18NLogger(LocalPipelineExecutor.class);
    private AbstractPipeline pipeline;
    private final Consumer<List<IOObject>> callback;
    private ForkJoinTask<?> processTask;

    public LocalPipelineExecutor(final AbstractPipeline pipeline, Consumer<List<IOObject>> callback) {
        this.pipeline = pipeline;
        this.callback = callback;
    }

    public void run() {
        processTask = ForkJoinPool.commonPool().submit(new RecursiveAction() {

            private static final long serialVersionUID = 1L;

            @Override
            protected void compute() {
                try {
                    RepositoryPath processPath = pipeline.getPath();
                    ThreadContext.put("process.path", processPath == null ? String.valueOf(pipeline.hashCode()) : processPath.toShortString(10));
                    ThreadContext.put("process.executionId", UUID.randomUUID().toString().substring(0, 6));
                    List<IOObject> results = pipeline.debug(false);
                    beep("success");
                    callback.accept(results);
                } catch (ProcessStoppedException e) {
                    // here the process ended method is not called ! let the thread finish the
                    // current operator and send no events to the main frame...
                    // also no beep...
                    Operator stoppedOperator = e.getOperator();
                    if (stoppedOperator != null) {
                        log.info("com.owc.singularity.engine.execution.pipeline_stop", pipeline::getName,
                                () -> formatPipelineToTreePathOf(OperatorPath.of(stoppedOperator), pipeline));
                    } else {
                        log.info("com.owc.singularity.engine.execution.pipeline_stop_unknown_operator", pipeline::getName);
                    }
                    callback.accept(null);
                } catch (Throwable e) {
                    beep("error");
                    String debugProperty = PropertyService.getParameterValue(EngineProperties.GENERAL_DEBUGMODE);
                    boolean debugMode = Tools.booleanValue(debugProperty, false);

                    if (e instanceof OutOfMemoryError) {
                        // out of memory, give memory hint
                        SwingTools.showVerySimpleErrorMessage("proc_failed_out_of_mem");
                    } else if (e instanceof OperatorException operatorException && operatorException.getOperator() != null) {
                        // no bug? Show nice error screen (user error infos)
                        ProcessGUITools.displayBubbleForUserError(operatorException);
                    } else {
                        if (debugMode) {
                            SwingTools.showFinalErrorMessage("process_failed_simple", e, true, new Object[] {});
                        } else {
                            // perform process check. No bug report if errors...
                            if (e instanceof NullPointerException || e instanceof ArrayIndexOutOfBoundsException) {
                                log.error(e.toString(), e);
                                SwingTools.showVerySimpleErrorMessage("proc_failed_without_obv_reason");
                            } else {
                                SwingTools.showSimpleErrorMessage("process_failed_simple", e, true, new Object[] {});
                            }
                        }
                    }
                    OperatorPath operatorPath = null;
                    if (e instanceof OperatorException operatorException) {
                        operatorPath = OperatorPath.fromException(operatorException);
                    } else {
                        Operator currentOperator = pipeline.getCurrentOperator();
                        if (currentOperator != null) {
                            operatorPath = OperatorPath.of(currentOperator);
                        }
                    }
                    if (operatorPath != null) {
                        log.atError()
                                .withThrowable(e)
                                .log("com.owc.singularity.engine.execution.pipeline_error", pipeline.getName(),
                                        formatPipelineToTreePathOf(operatorPath, pipeline));
                    } else {
                        log.atError().withThrowable(e).log("com.owc.singularity.engine.execution.pipeline_error_unknown_operator", pipeline.getName());
                    }
                    callback.accept(null);
                } finally {
                    ThreadContext.remove("process.executionId");
                    ThreadContext.remove("process.path");
                    if (pipeline.getState() != PipelineState.STOPPED) {
                        pipeline.stop();
                    }
                    pipeline = null;
                }
            }
        });
    }

    private static String formatPipelineToTreePathOf(OperatorPath path, AbstractPipeline pipeline) {
        OperatorPathTreePrinter pathPrinter = new OperatorPathTreePrinter(path, 3);
        pipeline.walk(pathPrinter);
        return pathPrinter.getResult();
    }

    public static void beep(final String reason) {
        if (Tools.booleanValue(PropertyService.getParameterValue("singularity.gui.beep." + reason), false)) {
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    public void stopProcess() {
        if (pipeline != null) {
            processTask.cancel(true);
            this.pipeline.stop();
            callback.accept(null);
        }
    }

    public void pauseProcess() {
        if (pipeline != null) {
            this.pipeline.pause();
        }
    }

    public AbstractPipeline getPipeline() {
        return pipeline;
    }

    @Override
    public String toString() {
        return "PipelineThread (" + pipeline.getPath() + ")";
    }

    public boolean isAlive() {
        if (processTask != null)
            return !processTask.isDone();
        return false;
    }
}
