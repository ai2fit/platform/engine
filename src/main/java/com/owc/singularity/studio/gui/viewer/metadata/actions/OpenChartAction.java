/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.viewer.metadata.actions;


import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationModel;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationSettings;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.components.ButtonBarCardPanel;
import com.owc.singularity.studio.gui.viewer.metadata.AttributeStatisticsPanel;
import com.owc.singularity.studio.gui.viewer.metadata.model.AbstractAttributeStatisticsModel;
import com.owc.singularity.studio.gui.viewer.metadata.model.DateTimeAttributeStatisticsModel;
import com.owc.singularity.studio.gui.viewer.metadata.model.NominalAttributeStatisticsModel;
import com.owc.singularity.studio.gui.viewer.metadata.model.NumericalAttributeStatisticsModel;


/**
 * This action is only to be used by the {@link AttributePopupMenu} or as a button inside a
 * {@link AttributeStatisticsPanel}.
 *
 * @author Marco Boeck, Michael Knopf, Nils Woehler
 *
 */
public class OpenChartAction extends ResourceAction {

    private static final String VISUALIZATIONS_CLASS_NAME = "com.owc.singularity.studio.gui.plotter.PlotterPanel";
    private static final String SHOW_AGGREGATED_COLUMN_METHOD_NAME = "showAggregatedColumnChart";
    private static final String GET_PLOTTER_SETTINGS = "getPlotterSettings";


    /**
     * Creates a new {@link OpenChartAction} instance.
     */
    public OpenChartAction() {
        super(true, "meta_data_stats.open_chart");
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {

        // look up the panel invoking the pop up invoking the action
        AttributeStatisticsPanel asp = null;

        // the action should only be invoked by AttributePopupMenus
        Container parent = ((JComponent) e.getSource()).getParent();
        if ((parent instanceof AttributePopupMenu)) {
            asp = ((AttributePopupMenu) parent).getAttributeStatisticsPanel();
        } else {
            asp = (AttributeStatisticsPanel) SwingUtilities.getAncestorOfClass(AttributeStatisticsPanel.class, parent);
            if (asp == null) {
                // we are not inside a AttributesStatisticPanel
                return;
            }
        }

        ButtonBarCardPanel cardPanel = (ButtonBarCardPanel) SwingUtilities.getAncestorOfClass(ButtonBarCardPanel.class, asp);
        AbstractAttributeStatisticsModel model = asp.getModel();

        // select the visualizations view
        cardPanel.selectCard("plot_view");

        // get the opened visualization
        JPanel outerPanel = (JPanel) cardPanel.getShownComponent();
        for (Component innerComp : outerPanel.getComponents()) {
            if (innerComp != null && innerComp.getClass().getName().equals(VISUALIZATIONS_CLASS_NAME)) {
                // adjust settings
                String attributeName = model.getAttribute().getName();
                int exampleCount = model.getNumberExamples();
                try {
                    if (model instanceof NominalAttributeStatisticsModel) {
                        Method showHistogramChart = innerComp.getClass().getDeclaredMethod(GET_PLOTTER_SETTINGS);
                        showHistogramChart.setAccessible(true);
                        PlotterConfigurationModel configurationModel = (PlotterConfigurationModel) showHistogramChart.invoke(innerComp);
                        configurationModel.setPlotter(PlotterConfigurationModel.BAR_CHART);
                        configurationModel.setParameterAsString(PlotterConfigurationSettings.GROUP_BY_COLUMN, attributeName);
                    } else if (model instanceof NumericalAttributeStatisticsModel || model instanceof DateTimeAttributeStatisticsModel) {
                        Method showHistogramChart = innerComp.getClass().getDeclaredMethod(GET_PLOTTER_SETTINGS);
                        showHistogramChart.setAccessible(true);
                        PlotterConfigurationModel configurationModel = (PlotterConfigurationModel) showHistogramChart.invoke(innerComp);
                        configurationModel.setPlotter(PlotterConfigurationModel.HISTOGRAM_PLOT);
                        configurationModel.setParameterAsString(PlotterConfigurationSettings.AXIS_PLOT_COLUMNS, attributeName);
                        configurationModel.setParameterAsInt(PlotterConfigurationSettings.NUMBER_OF_BINS, Math.min(exampleCount, 10));
                    }
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.viewer.metadata.actions.OpenChartAction.cannot_show_visualization", e1);
                }
                break;
            }
        }
    }
}
