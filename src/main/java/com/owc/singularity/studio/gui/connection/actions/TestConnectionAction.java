/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.connection.actions;


import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.tools.config.actions.ActionResult;
import com.owc.singularity.engine.tools.config.actions.ActionResultFactory;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.connection.ConnectionParameterDefinition;
import com.owc.singularity.repository.connection.ConnectionParameters;
import com.owc.singularity.studio.gui.connection.ConnectionI18N;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ProgressThreadStoppedException;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * The action that triggers when a {@link ConnectionParameters} is tested in the connection UI.
 *
 * @author Jonas Wilms-Pfau
 * @since 9.3.0
 */
public class TestConnectionAction extends ResourceAction {

    private static final ImageIcon CANCEL_ICON = SwingTools.createIcon("24/" + ConnectionI18N.getConnectionGUIMessage("test.cancel.icon"));
    private static final String CANCEL_TESTING = ConnectionI18N.getConnectionGUILabel("test.cancel_testing");

    private final ConnectionParameterDefinition<?> definition;
    private final transient Supplier<Parameters> parameterSupplier;
    private final transient Consumer<ActionResult> setTestResult;
    /** Boolean indicating if it's testing or not */
    private final AtomicBoolean isTesting = new AtomicBoolean(false);
    /** Current call count */
    private final AtomicInteger callCount = new AtomicInteger(0);

    private final String name;
    private final transient Icon icon;

    private transient ProgressThread testThread;


    /**
     * Creates a new test connection action
     *
     * @param parametersSupplier
     *            supplier for a connection to test
     * @param setResult
     *            consumer that accepts the test results
     */
    public TestConnectionAction(ConnectionParameterDefinition<?> definition, Supplier<Parameters> parametersSupplier, Consumer<ActionResult> setResult) {
        super(false, "test_connection");
        this.definition = definition;
        this.parameterSupplier = parametersSupplier;
        this.setTestResult = setResult;
        this.name = (String) getValue(NAME);
        this.icon = (Icon) getValue(LARGE_ICON_KEY);
    }

    @Override
    protected void loggedActionPerformed(ActionEvent e) {
        final int currentCallCount = callCount.incrementAndGet();
        final ProgressThread currentTestThread = testThread;
        if (isTesting.compareAndSet(true, false)) {
            resetIcons();
            if (currentTestThread != null) {
                currentTestThread.cancel();
            }
            testThread = null;
            setTestResult.accept(null);
            return;
        } else if (!isTesting.compareAndSet(false, true)) {
            return;
        }

        SwingTools.invokeAndWait(() -> {
            setTestResult.accept(ActionResultFactory.undetermined("test.running"));
            putValue(NAME, CANCEL_TESTING);
            putValue(LARGE_ICON_KEY, CANCEL_ICON);
            firePropertyChange("icons", null, null);
        });

        testThread = new ProgressThread("test_connection") {

            @Override
            public void run() {
                ActionResult testResult = null;
                try {
                    final Parameters connection = parameterSupplier.get();
                    checkCancelled();
                    Set<String> keys = connection.getKeys();
                    Map<String, String> parametersMap = new HashMap<>(keys.size());
                    for (String key : keys) {
                        parametersMap.put(key, connection.getRawParameter(key));
                    }
                    testResult = getTestResult(definition, parametersMap);
                    checkCancelled();
                } catch (ProgressThreadStoppedException ptse) {
                    testResult = null;
                    throw ptse;
                } catch (Throwable t) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.connection.actions.TestConnectionAction.testing_failed", t);
                    testResult = ActionResultFactory.failure("test.unexpected_error", t);
                } finally {
                    if (currentCallCount == callCount.get() && isTesting.compareAndSet(true, false)) {
                        resetIcons();
                        Optional.ofNullable(testResult).ifPresent(setTestResult);
                    }
                }
            }
        };
        testThread.start();
    }

    /**
     * Tests the given connection
     *
     * @param connectionParameters
     *            the connection to test
     * @return the test result
     */
    public static ActionResult getTestResult(ConnectionParameterDefinition<?> definition, Map<String, String> connectionParameters) {
        if (connectionParameters == null) {
            return ActionResultFactory.undetermined();
        }

        try {
            ConnectionParameters connectionToBeTested = definition.create("Test Connection of " + definition, connectionParameters, "", "");
            Callable<ActionResult> testAction = connectionToBeTested.getTestAction();
            return testAction.call();
        } catch (Exception e) {
            return ActionResultFactory.failure(e.getMessage(), e);
        }
    }

    /**
     * Resets the icons to the regular icons
     */
    private void resetIcons() {
        SwingTools.invokeAndWait(() -> {
            putValue(NAME, name);
            putValue(LARGE_ICON_KEY, icon);
            firePropertyChange("icons", null, null);
        });
    }
}
