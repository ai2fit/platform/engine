/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.tools;


import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.text.MessageFormat;
import java.util.EnumMap;
import java.util.function.Consumer;

import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.engine.tools.SystemInfoUtilities.OperatingSystem;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.ConditionalAction;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;


/**
 * This will create an action, whose settings are take from a .properties file being part of the GUI
 * Resource bundles of SingularityEngine. These might be accessed using the I18N class.
 *
 * A resource action needs a key specifier, which will be used to build the complete keys of the
 * form: gui.action.<specifier>.label = Which will be the caption gui.action.<specifier>.icon = The
 * icon of this action. For examples used in menus or buttons gui.action.<specifier>.acc = The
 * accelerator key used for menu entries gui.action.<specifier>.tip = Which will be the tool tip
 * gui.action.<specifier>.mne = Which will give you access to the mnemonics key. Please make it the
 * same case as in the label
 *
 * @author Simon Fischer, Sebastian Land
 */
public abstract class ResourceAction extends ConditionalAction {

    private static final long serialVersionUID = -3699425760142415331L;
    private static final Logger log = LogManager.getLogger(ResourceAction.class);

    private final String key;

    private final String iconName;

    private final IconType iconType;

    /**
     * Specifies the style of the icon.
     */
    public enum IconType {
        /** standard, multi-colored icons for 24x24 and higher, grey for 16x16 */
        NORMAL,

        /** flat, single-colored icons */
        FLAT,

        /** monochrome icons for 16x16 */
        MONO
    };

    /**
     * Creates a new {@link ResourceAction} with the standard {@link IconType}. With an icon size
     * for buttons.
     *
     * @param i18nKey
     * @param i18nArgs
     */
    public ResourceAction(String i18nKey, Object... i18nArgs) {
        this(false, i18nKey, i18nArgs);
    }

    /**
     * Creates a new {@link ResourceAction} with the standard {@link IconType}.
     *
     * @param smallIcon
     * @param i18nKey
     * @param i18nArgs
     */
    public ResourceAction(boolean smallIcon, String i18nKey, Object... i18nArgs) {
        this(smallIcon, i18nKey, IconType.NORMAL, i18nArgs);
    }

    /**
     * Creates a new {@link ResourceAction} with the specified {@link IconType}.
     *
     * @param smallIcon
     * @param i18nKey
     * @param iconType
     * @param i18nArgs
     */
    public ResourceAction(boolean smallIcon, String i18nKey, IconType iconType, Object... i18nArgs) {
        this(smallIcon ? IconSize.SMALL : IconSize.MEDIUM, IconSize.SMALL, i18nKey, iconType, i18nArgs);
    }

    /**
     * Creates a new {@link ResourceAction} with the specified {@link IconType}.
     *
     * @param iconSize
     * @param i18nKey
     * @param iconType
     * @param i18nArgs
     */
    public ResourceAction(IconSize buttonSize, IconSize menuSize, String i18nKey, IconType iconType, Object... i18nArgs) {
        super(i18nArgs == null || i18nArgs.length == 0 ? getMessage(i18nKey + ".label") : MessageFormat.format(getMessage(i18nKey + ".label"), i18nArgs));
        putValue(ACTION_COMMAND_KEY, i18nKey);
        this.key = i18nKey;
        this.iconType = iconType;
        String mne = getMessageOrNull(i18nKey + ".mne");
        if (mne != null && mne.length() > 0) {
            String name = (String) getValue(NAME);
            if (name != null && name.length() > 0 && name.indexOf(mne.charAt(0)) == -1 && name.indexOf(Character.toLowerCase(mne.charAt(0))) == -1) {
                log.warn(LogService.MARKER_DEVELOPMENT, "Mnemonic key {} not found for action {} ({}).", new Object[] { mne, i18nKey, name });
            }
            mne = mne.toUpperCase();
            putValue(MNEMONIC_KEY, (int) mne.charAt(0));
        }
        String acc = getMessageOrNull(i18nKey + ".acc");
        KeyStroke accStroke = null;
        if (acc != null) {
            if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
                acc = acc.replace("ctrl", "meta");
                acc = acc.replace("control", "meta");
            }
            accStroke = KeyStroke.getKeyStroke(acc);
            putValue(ACCELERATOR_KEY, accStroke);
        }
        String tip = getMessageOrNull(i18nKey + ".tip");
        if (tip != null) {
            if (accStroke != null) {
                tip += " (" + formatKeyStroke(accStroke) + ")";
            }
            putValue(SHORT_DESCRIPTION, i18nArgs == null || i18nArgs.length == 0 ? tip : MessageFormat.format(tip, i18nArgs));
        }
        this.iconName = getMessageOrNull(i18nKey + ".icon");
        if (getIconName() != null && !getIconName().trim().isEmpty()) {
            putValue(LARGE_ICON_KEY, SwingTools.createIcon(buttonSize.getSize() + "/" + getIconName(), buttonSize, iconType == IconType.MONO));
            putValue(SMALL_ICON, SwingTools.createIcon(menuSize.getSize() + "/" + getIconName(), menuSize, iconType == IconType.MONO));
        }
        putValue("rm_id", i18nKey);
    }

    /**
     * Adds the action to the input and action map of the components.
     *
     * @param condition
     *            one out of {@link JComponent#WHEN_FOCUSED}, ...
     * @param clone
     * @param disableOnFocusLost
     *            if <code>true</code>, will disable the action on FocusLost event and enable it
     *            again on FocusGained (if conditions of superclass are met).
     * @param components
     *            the {@link JComponent}s to register this action to
     */
    public void addToActionMap(int condition, boolean clone, boolean disableOnFocusLost, boolean initiallyDisabled, String actionKey,
            JComponent... components) {
        final ResourceAction instance;
        if (clone) {
            try {
                instance = this.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        } else {
            instance = this;
        }
        for (JComponent comp : components) {
            if (comp == null) {
                throw new IllegalArgumentException("components must not be null!");
            }

            KeyStroke keyStroke = (KeyStroke) instance.getValue(ACCELERATOR_KEY);
            if (keyStroke != null) {
                actionKey = actionKey == null ? instance.key : actionKey;
                comp.getInputMap(condition).put(keyStroke, actionKey);
                comp.getActionMap().put(actionKey, instance);
            } else {
                log.warn(LogService.MARKER_DEVELOPMENT, "Cannot add action {} to input map: no accelerator defined", instance.key);
            }
            if (disableOnFocusLost) {
                comp.addFocusListener(new FocusListener() {

                    @Override
                    public void focusLost(FocusEvent e) {
                        if (!e.isTemporary()) {
                            // focus lost here means disable it no matter the conditions
                            instance.setEnabled(false);
                            instance.setDisabledDueToFocusLost(true);
                        }
                    }

                    @Override
                    public void focusGained(FocusEvent e) {
                        if (!e.isTemporary()) {
                            // focus gained here means enable it if conditions are fulfilled
                            instance.setDisabledDueToFocusLost(false);
                            ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
                            if (mainProcessPanel == null)
                                return;
                            EnumMap<Condition, Boolean> currentStates = mainProcessPanel.getActions().getCurrentStates();
                            instance.update(currentStates);
                        }
                    }
                });
                if (initiallyDisabled) {
                    if (SwingUtilities.isEventDispatchThread()) {
                        instance.setEnabled(false);
                    } else {
                        SwingUtilities.invokeLater(() -> instance.setEnabled(false));
                    }
                }
            }
        }
    }

    /**
     * Adds the action to the input and action map of the component.
     *
     * @param condition
     *            one out of WHEN_IN_FOCUES, ...
     */
    public void addToActionMap(JComponent component, int condition) {
        addToActionMap(condition, false, false, false, null, component);
    }

    /**
     * This returns the i18n key of this action.
     */
    public String getKey() {
        return key;
    }

    public String getIconName() {
        return iconName;
    }

    public IconType getIconType() {
        return iconType;
    }

    @Override
    protected ResourceAction clone() throws CloneNotSupportedException {
        return (ResourceAction) super.clone();
    }

    private static String getMessage(String key) {
        return I18N.getGUIMessage("gui.action." + key);
    }

    private static String getMessageOrNull(String key) {
        return I18N.getGUIMessageOrNull("gui.action." + key);
    }

    /**
     * Converts {@link KeyStroke} to a human-readable string.
     *
     * @param keyStroke
     *            the keystroke
     * @return a human-readable string like 'Ctrl+E'
     */
    private static String formatKeyStroke(KeyStroke keyStroke) {
        StringBuilder builder = new StringBuilder();
        String modifierString = KeyEvent.getKeyModifiersText(keyStroke.getModifiers());
        String keyString = KeyEvent.getKeyText(keyStroke.getKeyCode());

        if (modifierString != null && !modifierString.trim().isEmpty()) {
            builder.append(modifierString);
            builder.append("+");
        }

        builder.append(keyString);

        return builder.toString();
    }

    public static ResourceAction of(String name, Consumer<ActionEvent> action) {
        return new DefaultResourceAction(action, name);
    }

    public static ResourceAction of(String name, boolean smallIcon, Consumer<ActionEvent> action, Object... i18nArgs) {
        return new ResourceAction(smallIcon, name, i18nArgs) {

            @Override
            protected void loggedActionPerformed(ActionEvent e) {
                action.accept(e);
            }
        };
    }
}
