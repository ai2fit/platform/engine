/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui;


import java.awt.Dimension;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.parameters.ParameterPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * A visualizer which shows the attribute values of an example. This is the most simple visualizer
 * which should work in all cases.
 *
 * @author Ingo Mierswa, Tobias Malbrecht
 */
public class ExampleVisualizer {

    private final ExampleSet exampleSet;

    private final Attribute idAttribute;

    public ExampleVisualizer(ExampleSet exampleSet) {
        this.exampleSet = exampleSet;
        idAttribute = exampleSet.getAttributes().getId();
    }

    public void startVisualization(final int rowId) {
        JComponent main;
        int dialogSize = ButtonDialog.MESSAGE;

        main = makeMainVisualizationComponent(exampleSet, rowId);
        dialogSize = ButtonDialog.NARROW;


        String idValue;
        if (idAttribute != null) {
            idValue = exampleSet.getNominalValue(rowId, idAttribute.getIndex());
        } else {
            idValue = rowId + "";
        }
        JDialog dialog = ButtonDialog.create("example_visualizer_dialog").setI18nArguments(idValue).setContent(main, dialogSize).withCloseButton().build();
        dialog.setVisible(true);
    }

    private String getFormattedValue(ExampleSet exampleSet, Attribute attribute, int row) {
        switch (attribute.getValueType()) {
            case NUMERIC:
                double value = exampleSet.getNumericValue(row, attribute.getIndex());
                if (ValueType.isMissing(value)) {
                    return "?";
                }
                return Tools.formatIntegerIfPossible(value);
            case TIMESTAMP:
                long timestamp = exampleSet.getTimestampValue(row, attribute.getIndex());
                if (ValueType.isMissing(timestamp)) {
                    return "?";
                }
                return Tools.formatDateAsUserSpecified(timestamp);
            case NOMINAL:
                String string = exampleSet.getNominalValue(row, attribute.getIndex());
                if (ValueType.isMissing(string)) {
                    return "?";
                }
                return string;
        }
        return "";
    }

    protected JComponent makeMainVisualizationComponent(ExampleSet exampleSet, int row) {
        JComponent main;
        String[] columnNames = new String[] { "Attribute", "Value" };
        String[][] data = new String[this.exampleSet.getAttributes().allSize()][2];
        Iterator<Attribute> a = this.exampleSet.getAttributes().allAttributes();
        int counter = 0;
        while (a.hasNext()) {
            Attribute attribute = a.next();
            data[counter][0] = attribute.getName();
            data[counter][1] = getFormattedValue(exampleSet, attribute, row);
            counter++;
        }
        ExtendedJTable table = new ExtendedJTable();
        table.setDefaultEditor(Object.class, null);
        TableModel tableModel = new DefaultTableModel(data, columnNames);
        table.setRowHighlighting(true);
        table.setRowHeight(ParameterPanel.VALUE_CELL_EDITOR_HEIGHT);
        table.setModel(tableModel);
        main = new ExtendedJScrollPane(table);
        main.setBorder(null);
        int tableHeight = (int) (table.getPreferredSize().getHeight() + table.getTableHeader().getPreferredSize().getHeight() + 5); // 5
                                                                                                                                    // for
                                                                                                                                    // border
        if (tableHeight < main.getPreferredSize().getHeight()) {
            main.setPreferredSize(new Dimension((int) main.getPreferredSize().getWidth(), tableHeight));
        }
        return main;
    };
}
