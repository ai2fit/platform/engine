/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.net.URITools;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.security.UserCredential;
import com.owc.singularity.studio.gui.security.Wallet;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * Dialog asking for username and passwords. Answers may be cached (if chosen by user).
 *
 * @author Simon Fischer
 *
 */
public class PasswordDialog extends ButtonDialog {

    /**
     * Determines if the PasswordDialog should be suppressed
     */
    private static final AtomicInteger suppressionCount = new AtomicInteger(0);

    /**
     * Checks if a suppression exists
     *
     * @return true if one or more suppressions exist
     */
    private static boolean isSuppressed() {
        return suppressionCount.get() > 0;
    }

    /**
     * Prevents the dialog from being shown to the user
     * <p>
     * Warning: You must remove the suppression from the PasswordDialog after you are done!
     * </p>
     */
    public static void addSuppression() {
        suppressionCount.incrementAndGet();
    }

    /**
     * Removes the suppression from the PasswordDialog, must be called after addSuppression
     */
    public static void removeSuppression() {
        suppressionCount.decrementAndGet();
    }

    private static final long serialVersionUID = 1L;

    private final JTextField usernameField = new JTextField(20);
    private final JPasswordField passwordField = new JPasswordField(20);
    private final JCheckBox rememberBox = new JCheckBox(new ResourceActionAdapter("authentication.remember"));
    private final JLabel enforcedIcon = new ResourceLabel("preferences.setting_enforced");

    private PasswordDialog(Window owner, String i18nKey, URI uri, String knownUsername, char[] knownPassword, Object... args) {
        super(owner, i18nKey, ModalityType.APPLICATION_MODAL, args);
        setModal(true);
        if (knownUsername != null && !knownUsername.isBlank()) {
            usernameField.setText(knownUsername);
        }
        if (knownPassword != null && knownPassword.length > 0) {
            passwordField.setText(new String(knownPassword));
            rememberBox.setSelected(true);
        }

        JPanel main = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.insets = new Insets(4, 4, 4, 4);

        JLabel label = new ResourceLabel("authentication.username", uri);
        label.setLabelFor(usernameField);
        c.gridwidth = GridBagConstraints.RELATIVE;
        main.add(label, c);
        c.gridwidth = GridBagConstraints.REMAINDER;
        main.add(usernameField, c);

        label = new ResourceLabel("authentication.password", uri);
        label.setLabelFor(passwordField);
        c.gridwidth = GridBagConstraints.RELATIVE;
        main.add(label, c);
        c.gridwidth = GridBagConstraints.REMAINDER;
        main.add(passwordField, c);

        JPanel rememberPanel = new JPanel();
        rememberPanel.add(rememberBox);
        if (Boolean.parseBoolean(PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_DISALLOW_REMEMBER_PASSWORD))) {
            rememberPanel.add(enforcedIcon);
            rememberBox.setSelected(false);
            rememberBox.setEnabled(false);
        }

        main.add(rememberPanel, c);

        layoutDefault(main, makeOkButton(), makeCancelButton());

    }

    public PasswordAuthentication makeAuthentication() {
        return new PasswordAuthentication(usernameField.getText(), passwordField.getPassword());
    }

    /**
     * @param username
     *            the username to accompany the given url. Used to differentiate multiple entries
     *            for the same url.
     * @param i18nKey
     *            The i18nKey has to look like every dialog i18n: gui.dialog.KEY.title etc.
     * @param args
     *            Will be used when i18nKey is set. If i18nKey is <code>null</code> it will be not
     *            be used
     **/
    public static PasswordAuthentication getPasswordAuthentication(URI uri, String username, boolean forceRefresh, boolean hideDialogIfPasswordKnown,
            String i18nKey, Object... args) {
        // clean up the uri by removing user info, query and fragments.
        URI cleanedUri = URITools.getWithUserInfo(URITools.getWithoutQueryAndFragment(uri), null);
        // First check whether the credentials are stored within the Wallet
        final UserCredential authentication = Wallet.getInstance().getEntry(cleanedUri, username);

        // return immediately if known and desired
        if (hideDialogIfPasswordKnown && !forceRefresh && authentication != null) {
            if (authentication.getPassword().length == 0) {
                return null;
            }
            LogService.getRoot().debug("com.owc.singularity.studio.gui.tools.PasswordDialog.reusing_cached_password", cleanedUri);
            return new PasswordAuthentication(authentication.getUsername(), authentication.getPassword());
        }

        // If password was not stored within the wallet or a refresh is forced, check whether we are
        // in headless mode
        if (SingularityEngine.getExecutionMode().isHeadless()) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.tools.PasswordDialog.no_query_password_in_batch_mode", cleanedUri);
            return null;
        }
        // Throw away all requests before the UI is ready
        if (ApplicationFrame.getApplicationFrame() == null) {
            return null;
        }

        // If the PasswordDialog is suppressed for reasons don't show it
        if (PasswordDialog.isSuppressed()) {
            return null;
        }

        // only set to default authentication key if provided key not available
        if (i18nKey == null || i18nKeyNotAvailable(i18nKey)) {
            i18nKey = "authentication";
            args = new Object[] { cleanedUri };
        }

        final Object[] pdArgs = args;
        final String passwordI18N = i18nKey;

        // create PasswordDialog on EDT
        final PasswordDialog pd = SwingTools.invokeAndWaitWithResult(() -> {
            PasswordDialog pd1 = new PasswordDialog(ApplicationFrame.getApplicationFrame(), passwordI18N, cleanedUri,
                    authentication != null ? authentication.getUsername() : username, authentication != null ? authentication.getPassword() : new char[0],
                    pdArgs);
            // Onboarding is already a APPLICATION_MODAL
            pd1.setModalityType(ModalityType.TOOLKIT_MODAL);
            pd1.setVisible(true);
            return pd1;
        });
        if (!pd.wasConfirmed()) {
            return null;
        }
        PasswordAuthentication result = pd.makeAuthentication();
        if (pd.rememberBox.isSelected()) {
            UserCredential userCredential = new UserCredential(cleanedUri, result.getUserName(), result.getPassword());
            Wallet.getInstance().addEntry(cleanedUri, userCredential);
            Wallet.getInstance().saveCache();
        }
        return result;
    }

    /**
     * Tests the availability of a given i18n key. More specifically tests if the key
     * <p>
     * {@code gui.dialog.<i18nKey>.title} can be found.
     *
     * @param i18nKey
     *            the i18n key to test for dialog purposes
     * @return whether the key is available
     * @since 9.0.2
     */
    private static boolean i18nKeyNotAvailable(String i18nKey) {
        String fullKey = "gui.dialog." + i18nKey + ".title";
        return fullKey.equals(I18N.getGUIMessage(fullKey));
    }

}
