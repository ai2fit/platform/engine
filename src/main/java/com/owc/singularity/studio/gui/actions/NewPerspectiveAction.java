/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.perspective.Perspective;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.InputDialog;


/**
 *
 * @author Simon Fischer
 */
public class NewPerspectiveAction extends ResourceAction {

    private static final long serialVersionUID = 5526646387968616318L;

    private static class NewPerspectiveDialog extends InputDialog {

        private static final long serialVersionUID = -7106546247629834518L;

        private final PerspectiveController perspectiveController;

        private boolean ok = false;

        private NewPerspectiveDialog(PerspectiveController perspectiveController) {
            super(ApplicationFrame.getApplicationFrame(), "new_perspective");
            this.perspectiveController = perspectiveController;
        }

        public boolean isOk() {
            return ok;
        }

        @Override
        protected void ok() {
            if (perspectiveController.isValidPerspectiveName(getInputText())) {
                ok = true;
                dispose();
            } else {
                SwingTools.showVerySimpleErrorMessage(this, "invalid_perspective_name");
            }
        }
    }


    public NewPerspectiveAction() {
        super("new_perspective");
    }

    /**
     * @deprecated use {@link #NewPerspectiveAction()} instead
     */
    @Deprecated
    public NewPerspectiveAction(MainFrame mainFrame) {
        this();
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        PerspectiveController perspectiveController = MainFrame.INSTANCE.getPerspectiveController();
        NewPerspectiveDialog dialog = new NewPerspectiveDialog(perspectiveController);
        dialog.setVisible(true);
        if (dialog.isOk()) {
            Perspective perspective = perspectiveController.getOrCreatePerspective(dialog.getInputText(), true);
            perspectiveController.showPerspective(perspective);
        }
    }
}
