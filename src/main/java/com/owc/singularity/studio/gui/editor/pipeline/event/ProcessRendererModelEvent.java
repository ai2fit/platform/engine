/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.event;


import com.owc.singularity.studio.gui.editor.ProcessRendererModel;


/**
 * An event for the {@link ProcessRendererModel}.
 *
 * @author Marco Boeck, Jan Czogalla
 * @since 6.4.0
 *
 */
public final class ProcessRendererModelEvent {

    /**
     * Defines different kind of {@link ProcessRendererModelEvent}s.
     *
     */
    public enum ModelEvent {
        /**
         * fired when the displayed processes have changed. First argument is the containing
         * operator chain
         */
        DISPLAYED_EXECUTION_UNITS_CHANGED,

        /**
         * Fired when the displayed operator chain has changed. First argument is the current
         * displayed operator chain
         */
        DISPLAYED_CHAIN_CHANGED,

        /** fired when a process size has changed */
        PROCESS_SIZE_CHANGED,

        /** fired when something minor changes which only requires a repaint */
        MISC_CHANGED,

        /** fired before the displayed operator chain will change */
        DISPLAYED_CHAIN_WILL_CHANGE;
    }

    private final ModelEvent type;
    private final Object[] args;

    /**
     * Creates a new {@link ProcessRendererModelEvent} instance for the specified {@link ModelEvent}
     * .
     *
     * @param type
     *            the event type
     */
    public ProcessRendererModelEvent(final ModelEvent type, Object... args) {
        this.type = type;
        this.args = args;
    }

    /**
     * Returns the {@link ModelEvent}.
     *
     * @return the type of the event
     */
    public ModelEvent getEventType() {
        return type;
    }

    public Object[] getArgs() {
        return args;
    }
}
