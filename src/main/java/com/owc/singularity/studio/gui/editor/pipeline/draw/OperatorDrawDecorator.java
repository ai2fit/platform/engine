/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.draw;


import java.awt.Graphics2D;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;


/**
 * Implementations of this interface can be registered to decorate operators during the process
 * renderer drawing process.
 * <p>
 * <strong>Attention:</strong> Drawing should work on a headless server, so implementations should
 * take extra care to not make any components which do not support headless mode.
 * </p>
 *
 * @author Marco Boeck
 * @since 6.4.0
 * @see ProcessRenderer#addOperatorDecorator(OperatorDrawDecorator)
 *
 */
public interface OperatorDrawDecorator {

    /**
     * Draws the decoration directly after the operator itself was drawn. This method is called when
     * {@link ProcessEditorPanel#paintComponent(java.awt.Graphics)} was called.
     *
     * @param operator
     *            the operator which can be decorated.
     * @param g2
     *            the graphics context to draw upon. Coordinates start at (0,0) aka the top left
     *            corner and extend to {@link ProcessRendererModel#getProcessSize(ExecutionUnit)}
     * @param model
     *            the model backing the process rendering
     */
    public void draw(final Operator operator, final Graphics2D g2, final ProcessRendererModel model);

    /**
     * Prints the decoration directly after the operator itself was drawn. This method is called
     * when {@link ProcessEditorPanel#printComponent(java.awt.Graphics)} was called.
     *
     * @param operator
     *            the operator which can be decorated.
     * @param g2
     *            the graphics context to draw upon. Coordinates start at (0,0) aka the top left
     *            corner and extend to {@link ProcessRendererModel#getProcessSize(ExecutionUnit)}
     * @param model
     *            the model backing the process rendering
     */
    public void print(final Operator operator, final Graphics2D g2, final ProcessRendererModel model);
}
