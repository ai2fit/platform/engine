/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.dnd;


import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.editor.pipeline.tree.OperatorTree;


/**
 * Provides a transferable wrapper for Operators in order to drag-n-drop them in the
 * AbstractPipeline-Tree.
 * 
 * @see OperatorTree
 * @author Helge Homburg, Michael Knopf, Adrian Wilke
 */
public class TransferableOperator implements Transferable {

    public static final DataFlavor LOCAL_TRANSFERRED_OPERATORS_FLAVOR = new DataFlavor(
            DataFlavor.javaJVMLocalObjectMimeType + ";class=" + Operator.class.getName(), "SingularityEngine operator");

    public static final DataFlavor LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR = TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR;

    public static final DataFlavor LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR = TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR;

    private static final DataFlavor[] DATA_FLAVORS = { TransferableOperator.LOCAL_TRANSFERRED_OPERATORS_FLAVOR, DataFlavor.stringFlavor };

    private final List<Operator> originalReferences;

    private final List<Operator> copiedOperator;

    public TransferableOperator(Operator[] operators) {
        // references to original operators (required to delete operators in case of a cut and paste
        // event)
        this.originalReferences = Arrays.asList(operators);
        // cloning the operators ensures that further editing (e.g., in the process view) does not
        // affect the copied elements
        this.copiedOperator = Tools.cloneOperators(originalReferences);
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (flavor.equals(LOCAL_TRANSFERRED_OPERATORS_FLAVOR)) {
            return new Operator[][] { Tools.cloneOperators(copiedOperator).toArray(Operator[]::new), this.originalReferences.toArray(new Operator[0]) };
        }
        if (flavor.equals(DataFlavor.stringFlavor)) {
            StringBuilder b = new StringBuilder();
            for (Operator op : this.copiedOperator) {
                b.append(op.getXML(false));
            }
            return b.toString();
        }
        throw new UnsupportedFlavorException(flavor);
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return Arrays.asList(DATA_FLAVORS).contains(flavor);
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return DATA_FLAVORS;
    }

    /**
     * @return a list of to the original cloned operators.
     */
    protected List<Operator> getOriginalOperators() {
        return this.originalReferences;
    }

}
