/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.operatormenu;


import java.awt.Color;

import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.OperatorService.OperatorServiceListener;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.tools.AbstractOperatorDescriptionTreeNode;
import com.owc.singularity.studio.gui.editor.operators.OperatorGroupTreeModel;
import com.owc.singularity.studio.gui.tools.Icons;
import com.owc.singularity.studio.gui.tools.ResourceMenu;


/**
 * This is the abstract superclass for all menu containing operators. An operator menu can be used
 * for selecting new operators (see {@link InsertOperatorMenu}) or replacing operators (see
 * {@link ReplaceOperatorMenu}).
 * 
 * @author Simon Fischer, Ingo Mierswa
 */
public abstract class OperatorMenu extends ResourceMenu implements OperatorServiceListener {

    private static final long serialVersionUID = 2685621612717488446L;

    private final boolean onlySubprocesses;

    protected OperatorMenu(String key, boolean onlySubprocesses) {
        super(key);
        this.onlySubprocesses = onlySubprocesses;

        addAllOperatorsToMenu();

        OperatorService.addOperatorServiceListener(this);
    }

    public void addMenu(AbstractOperatorDescriptionTreeNode group, JMenu menu, boolean onlyChains) {
        for (AbstractOperatorDescriptionTreeNode subGroup : group.getSubGroups()) {
            OperatorGroupMenu subMenu = new OperatorGroupMenu(subGroup.getName());
            addMenu(subGroup, subMenu, onlyChains);
            if (subMenu.getItemCount() > 0) {
                menu.add(subMenu);
            }
        }
        for (OperatorDescription description : group.getOperatorDescriptions()) {
            if ((!onlyChains) || OperatorChain.class.isAssignableFrom(description.getOperatorClass())) {
                JMenuItem item;
                Icon icon = Icons.fromName(description.getIconName()).getSmallIcon();
                if (icon == null) {
                    item = new JMenuItem(description.getName());
                } else {
                    item = new JMenuItem(description.getName(), icon);
                }
                item.addActionListener(e -> performAction(description));

                if (description.isDeprecated()) {
                    item.setForeground(Color.LIGHT_GRAY);
                }

                menu.add(item);
            }
        }
    }

    public abstract void performAction(OperatorDescription description);

    @Override
    public void operatorRegistered(OperatorDescription description) {
        removeAll();

        // then add everything back
        addAllOperatorsToMenu();
    }

    @Override
    public void operatorUnregistered(OperatorDescription description) {
        removeAll();

        // then add everything back
        addAllOperatorsToMenu();
    }

    private void addAllOperatorsToMenu() {
        addMenu(OperatorGroupTreeModel.getFilledCompleteTree(), this, onlySubprocesses);
    }
}
