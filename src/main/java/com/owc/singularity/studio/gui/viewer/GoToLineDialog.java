package com.owc.singularity.studio.gui.viewer;

import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.*;

import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * A dialog for shifting the view port for the user in the {@link ExampleSetViewer}.
 * 
 * @author Alexander Mahler
 */
public class GoToLineDialog extends ButtonDialog {

    private final JTextField rowField;
    private final ExampleSetViewer exampleSetViewer;

    public GoToLineDialog(ExampleSetViewer exampleSetViewer) {
        super(exampleSetViewer != null ? SwingUtilities.getWindowAncestor(exampleSetViewer) : null, "go_to_line", ModalityType.DOCUMENT_MODAL);
        this.exampleSetViewer = exampleSetViewer;
        final JPanel filterPanel = new JPanel(new GridBagLayout());
        filterPanel.setPreferredSize(new Dimension(100, 100));

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(4, 0, 4, 0);
        c.gridwidth = 1;
        c.weightx = 0.2;
        c.gridx = 0;
        c.gridy = 0;
        JLabel labelStartRow = new ResourceLabel("data_view.jump_to_Row");
        filterPanel.add(labelStartRow, c);

        c.gridx = 1;
        c.weightx = 1;
        rowField = new JTextField();
        labelStartRow.setLabelFor(rowField);
        filterPanel.add(rowField, c);

        Collection<AbstractButton> buttons = new LinkedList<>();
        JButton okButton = makeOkButton();
        buttons.add(okButton);
        buttons.add(makeCloseButton());
        layoutDefault(filterPanel, buttons);
        getRootPane().setDefaultButton(okButton);

    }

    @Override
    protected void ok() {
        String text = rowField.getText();
        int row;
        try {
            row = Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return;
        }
        if (row > exampleSetViewer.getRowNumber())
            row = exampleSetViewer.getRowNumber();
        exampleSetViewer.scrollToRow(row - 1);
        dispose();
    }
}
