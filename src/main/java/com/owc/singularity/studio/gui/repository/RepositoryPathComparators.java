/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository;


import java.time.Instant;
import java.util.Comparator;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.tools.AlphanumComparator;
import com.owc.singularity.tools.AlphanumComparator.AlphanumCaseSensitivity;


/**
 * This class contains utility methods to compare {@link Repository}s, {@link RepositoryPath}s and
 * {@link Folder}s.
 *
 * @author Denis Schernov, Marcel Seifert
 *
 * @since 7.3
 */
public final class RepositoryPathComparators {

    private static final AlphanumComparator ALPHANUMERIC_COMPARATOR = new AlphanumComparator(AlphanumCaseSensitivity.INSENSITIVE);

    /**
     * Private constructor which throws if called.
     */
    private RepositoryPathComparators() {
        throw new AssertionError("Utility class must not be instantiated!");
    }

    /**
     * Compares a {@link RepositoryPath} to another by comparing their names as {@link String} using
     * alphanumeric comparison.
     *
     */
    public static final Comparator<RepositoryTreeNode> ENTRY_COMPARATOR = (entry1, entry2) -> {
        if (entry1 == entry2) {
            // same reference
            return 0;
        }
        Integer nullComparison = compareForNull(entry1, entry2);
        if (nullComparison != null) {
            return nullComparison;
        }
        boolean isDirectory1 = entry1.getInformation().isDirectory();
        boolean isDirectory2 = entry2.getInformation().isDirectory();
        if (isDirectory1 != isDirectory2) {
            if (isDirectory1)
                return -1;
            else
                return 1;
        }
        return compareAlphanumerically(entry1, entry2);
    };

    /**
     * Compares an {@link RepositoryPath} to another by comparing their last modified dates
     * {@link String} descending from new to old.
     */
    public static final Comparator<RepositoryTreeNode> ENTRY_COMPARATOR_LAST_MODIFIED = (entry1, entry2) -> {
        if (entry1 == entry2) {
            // same reference
            return 0;
        }
        Integer nullComparison = compareForNull(entry1, entry2);
        if (nullComparison != null) {
            return nullComparison;
        }
        boolean isDirectory1 = entry1.getInformation().isDirectory();
        boolean isDirectory2 = entry2.getInformation().isDirectory();
        if (isDirectory1 && isDirectory2) {
            return compareAlphanumerically(entry1, entry2);
        } else {
            if (isDirectory1)
                return -1;
            else if (isDirectory2)
                return 1;
        }

        Instant time1 = entry1.getInformation().getLastModifiedTimestamp();
        Instant time2 = entry2.getInformation().getLastModifiedTimestamp();
        nullComparison = compareForNull(time1, time2);
        if (nullComparison != null) {
            return nullComparison;
        }
        int timeResult = time1.compareTo(time2);
        if (timeResult == 0)
            return compareAlphanumerically(entry1, entry2);
        return timeResult;
    };


    /** @since 9.0 */
    private static Integer compareForNull(Object entry1, Object entry2) {
        boolean entry1IsNull = entry1 == null;
        boolean entry2IsNull = entry2 == null;
        if (entry1IsNull) {
            return entry2IsNull ? 0 : -1;
        }
        if (entry2IsNull) {
            return 1;
        }
        return null;
    }

    private static int compareAlphanumerically(RepositoryTreeNode node1, RepositoryTreeNode node2) {
        return ALPHANUMERIC_COMPARATOR.compare(node1.getRepositoryPathStringWithoutVersion(), node2.getRepositoryPathStringWithoutVersion());
    }
}
