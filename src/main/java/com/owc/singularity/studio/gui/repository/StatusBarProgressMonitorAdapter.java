package com.owc.singularity.studio.gui.repository;

import com.owc.singularity.repository.OperationProgressMonitor;
import com.owc.singularity.studio.gui.tools.StatusBar;

public class StatusBarProgressMonitorAdapter implements OperationProgressMonitor {

    private final StatusBar statusBar;
    private String title;
    private int total;
    private int completed = 0;

    public StatusBarProgressMonitorAdapter(StatusBar statusBar) {
        this.statusBar = statusBar;
    }

    @Override
    public void start(int numberOfTasks) {

    }

    @Override
    public void taskStarts(String title, int totalWork) {
        this.title = title == null || title.isBlank() ? "Processing files..." : title;
        this.total = totalWork;
        updateStatusBar(0);
    }

    private void updateStatusBar(int progress) {
        statusBar.setProgress(this.title, this.completed += progress, this.total);
    }

    @Override
    public void update(int completed) {
        updateStatusBar(completed);
    }

    @Override
    public void taskEnds() {
        statusBar.setProgress("", this.total, this.total);
        this.completed = 0;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }
}
