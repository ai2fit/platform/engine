/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.RecentLocationsManager;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.event.OnCreateRepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * This action stores the current process at the selected entry.
 * 
 * @author Simon Fischer
 */
public class StoreProcessAction extends AbstractRepositoryContextAction<RepositoryPath> {

    private static final long serialVersionUID = 1L;

    public StoreProcessAction(RepositoryTree tree) {
        super(tree, true, true, AbstractPipeline.class, false, true, "repository_store_process");
    }


    @Override
    protected RepositoryPath configureAction(List<RepositoryTreeNode> entries) {
        // we support only single entries
        RepositoryTreeNode entry = entries.get(0);
        RepositoryPath path = entry.getRepositoryPath();
        if (Files.isDirectory(path)) {
            String currentName = null;
            AbstractPipeline process = MainFrame.INSTANCE.getMainProcessPanel().getProcess();
            if (process.getPath() != null) {
                currentName = process.getPath().getFileName().toString();
            }

            final String name = SwingTools.showRepositoryEntryInputDialog("store_process", currentName);
            if (name != null) {
                if (name.isEmpty()) {
                    SwingTools.showVerySimpleErrorMessage("please_enter_non_empty_name");
                    return null;
                }
                try {
                    // check if folder already contains entry with said name
                    RepositoryPath target = path.resolve(name);
                    if (Files.exists(target)) {
                        Entry existingEntry = Entries.getEntry(target);
                        if (!(existingEntry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader()))) {
                            // existing entry is not a ProcessEntry, cannot overwrite
                            SwingTools.showVerySimpleErrorMessage("repository_entry_already_exists", name);
                            return null;
                        }
                    }
                    return target;
                } catch (IOException | ClassNotFoundException e1) {
                    SwingTools.showSimpleErrorMessage("cannot_store_process_in_repository", e1, name, e1.getMessage());
                    return null;
                }
            } else {
                return null;
            }
        } else {
            if (SwingTools.showConfirmDialog("overwrite", ConfirmDialog.YES_NO_OPTION, path.toString()) == ConfirmDialog.YES_OPTION) {
                return path;
            }
            return null;
        }
    }

    @Override
    public void executeAction(RepositoryPath actionPath, RepositoryPath path, ProgressListener progressListener) {
        ProgressThread storeProgressThread = new ProgressThread("store_process") {

            @Override
            public void run() {
                getProgressListener().setTotal(100);
                getProgressListener().setCompleted(10);
                try {
                    tree.invokeAfterTreeEvent(new OnCreateRepositoryTreeEventTrigger(path), ignored -> tree.expandAndSelectIfExists(path));
                    MainFrame.INSTANCE.getMainProcessPanel().saveProcessAt(path);
                    RecentLocationsManager.addToRecentFilesAsMostRecent(path);
                } catch (Exception e) {
                    SwingTools.showSimpleErrorMessage("cannot_store_process_in_repository", e, path, e.getMessage());
                } finally {
                    getProgressListener().setCompleted(100);
                    getProgressListener().complete();
                }
            }
        };
        storeProgressThread.start();
    }
}
