/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.repository.actions.context;


import java.util.List;
import java.util.Set;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.connection.ConnectionDefinitionRegistry;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.connection.ConnectionCreateDialog;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Action to create a new {@link com.owc.singularity.repository.connection.ConnectionParameters
 * Connection}.
 *
 * @author Marco Boeck
 * @since 9.3.0
 */
public class CreateConnectionAction extends AbstractRepositoryContextAction<RepositoryPath> {

    private static final long serialVersionUID = 2640807260982650855L;


    public CreateConnectionAction(RepositoryTree tree) {
        super(tree, true, false, null, false, true, "repository_create_connection");
    }

    @Override
    protected RepositoryPath configureAction(List<RepositoryTreeNode> entries) {
        Set<String> allTypes = ConnectionDefinitionRegistry.getInstance().getAllTypes();
        if (allTypes.isEmpty()) {
            // no connection types were defined, do not show dialog
            SwingTools.showMessageDialog("connection.no_connection_types");
            return null;
        }
        RepositoryPath path = entries.get(0).getRepositoryPath();
        ConnectionCreateDialog createDialog = new ConnectionCreateDialog(ApplicationFrame.getApplicationFrame(), path);

        createDialog.setVisible(true);
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, RepositoryPath config, ProgressListener progressListener) {
        // not necessary
    }
}
