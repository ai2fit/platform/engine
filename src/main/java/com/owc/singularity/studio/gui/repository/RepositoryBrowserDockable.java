/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.dnd.AbstractPatchedTransferHandler;
import com.owc.singularity.studio.gui.dnd.DragListener;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.repository.actions.ShowProcessInRepositoryAction;
import com.owc.singularity.studio.gui.repository.actions.SortByAction;
import com.owc.singularity.studio.gui.repository.actions.SortByLastModifiedAction;
import com.owc.singularity.studio.gui.repository.actions.SortByNameAction;
import com.owc.singularity.studio.gui.repository.actions.context.AbstractRepositoryContextAction;
import com.owc.singularity.studio.gui.repository.actions.context.CreateDirectoryAction;
import com.owc.singularity.studio.gui.repository.actions.context.DefaultRepositoryTreeContextActionFactory;
import com.owc.singularity.studio.gui.repository.actions.context.MountSpecificRepositoryTreeContextActionFactory;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNodeInformation;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ExtendedJToolBar;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.owc.singularity.studio.gui.tools.components.DropDownPopupButton;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * A component to browse through repositories.
 *
 * @author Simon Fischer
 */
public class RepositoryBrowserDockable extends JPanel implements Dockable {

    private static final long serialVersionUID = 1L;

    private static final Action SORT_REPOSITORY_ACTION = new ResourceAction(true, "repository_sort_submenu") {

        private static final long serialVersionUID = 1L;

        @Override
        public void loggedActionPerformed(ActionEvent e) {}
    };

    private final RepositoryTree tree;


    /**
     * @param dragListener
     *            registers a dragListener at the repository tree transferhandler. The listener is
     *            informed when a drag starts and a drag ends.
     */
    public RepositoryBrowserDockable(DragListener dragListener) {
        tree = new RepositoryTree();
        tree.addContextActionFactory(DefaultRepositoryTreeContextActionFactory.getInstance());
        for (RepositoryMount repositoryMount : RepositoryManager.getFileSystem().getMountingPoints().values()) {
            if (repositoryMount.getActions().isEmpty())
                continue;
            tree.addContextActionFactory(new MountSpecificRepositoryTreeContextActionFactory(repositoryMount));
        }
        // register a watch for mount changes
        RepositoryManager.addRepositoryConfigurationChangeListener(mountingPoints -> {
            for (RepositoryMount repositoryMount : mountingPoints.values()) {
                if (repositoryMount.getActions().isEmpty())
                    continue;
                tree.addContextActionFactory(new MountSpecificRepositoryTreeContextActionFactory(repositoryMount));
            }
        });
        if (dragListener != null) {
            ((AbstractPatchedTransferHandler) tree.getTransferHandler()).addDragListener(dragListener);
        }
        tree.addRepositorySelectionListener(e -> {

            if (e.getTreeNode() == tree.getModel().getRoot())
                return;
            RepositoryTreeNodeInformation viewModel = e.getTreeNode().getInformation();

            // skip folder double-clicks
            if (viewModel.isDirectory()) {
                return;
            }
            OpenEntryAction.open(Entries.getEntry(viewModel.getRepositoryPath()));
        });

        setLayout(new BorderLayout());


        final JPopupMenu sortActionsMenu = new JPopupMenu();
        // must be after the treeModel, since they require it to be initialized
        List<SortByAction> sortActions = new ArrayList<>(2);
        sortActions.add(new SortByNameAction(tree));
        sortActions.add(new SortByLastModifiedAction(tree));
        tree.addRepositorySortingMethodListener(selectedMethod -> {
            for (SortByAction sortAction : sortActions) {
                sortAction.setSelected(selectedMethod.equals(sortAction.getMethod()));
            }
        });
        for (SortByAction sortAction : sortActions) {
            sortActionsMenu.add(sortAction.createMenuItem());
        }
        DropDownPopupButton sortActionButton = new DropDownPopupButton("gui.action.sort_repository_actions", () -> sortActionsMenu);
        sortActionButton.setMaximumSize(new Dimension(39, 28));

        JToolBar toolBar = new ExtendedJToolBar();
        CreateDirectoryAction createDirectoryAction = new CreateDirectoryAction(tree);
        tree.getSelectionModel().addTreeSelectionListener(new EnableActionOnSelectionChange(createDirectoryAction));
        toolBar.add(createDirectoryAction);
        toolBar.add(new ShowProcessInRepositoryAction(tree));
        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(sortActionButton);


        add(toolBar, BorderLayout.NORTH);
        JScrollPane scrollPane = new ExtendedJScrollPane(tree);
        scrollPane.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Colors.TEXTFIELD_BORDER));
        add(scrollPane, BorderLayout.CENTER);
    }

    private record EnableActionOnSelectionChange(AbstractRepositoryContextAction createDirectoryAction) implements TreeSelectionListener {

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            createDirectoryAction.enable();
        }
    }

    /**
     * Returns the {@link RepositoryTree} managed by this browser.
     *
     * @return the repository tree
     * @since 7.0.0
     */
    public RepositoryTree getRepositoryTree() {
        return tree;
    }

    public static final String REPOSITORY_BROWSER_DOCK_KEY = "repository_browser";
    private final DockKey DOCK_KEY = new ResourceDockKey(REPOSITORY_BROWSER_DOCK_KEY);

    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    /**
     * @param storedRepositoryLocation
     *            the repository location that should be displayed in the browser
     */
    public void expandToRepositoryLocation(RepositoryPath storedRepositoryLocation) {
        tree.expandAndSelectIfExists(storedRepositoryLocation);
    }
}
