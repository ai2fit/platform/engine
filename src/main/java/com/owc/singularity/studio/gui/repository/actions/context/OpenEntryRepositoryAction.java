/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.util.List;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * This action opens the selected entry.
 * 
 * @author Tobias Malbrecht, Sebastian Land
 */
public class OpenEntryRepositoryAction extends AbstractRepositoryContextAction<Object> {

    private static final long serialVersionUID = 1L;

    public OpenEntryRepositoryAction(RepositoryTree tree) {
        super(tree, false, true, Object.class, true, false, "open_repository_entry");
    }

    @Override
    protected Object configureAction(List<RepositoryTreeNode> entries) {
        // we can do the opening in the awt thread
        entries.stream().map(RepositoryTreeNode::getEntry).forEach(OpenEntryAction::open);
        return null; // avoids entry specific execution
    }

    @Override
    public void executeAction(RepositoryPath path, Object config, ProgressListener progressListener) {}

}
