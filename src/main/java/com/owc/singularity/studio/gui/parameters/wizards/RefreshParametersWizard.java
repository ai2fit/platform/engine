package com.owc.singularity.studio.gui.parameters.wizards;

import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.studio.gui.wizards.AbstractConfigurationWizardCreator;
import com.owc.singularity.studio.gui.wizards.ConfigurationListener;


public class RefreshParametersWizard extends AbstractConfigurationWizardCreator {

    public static interface ParameterRefresher extends ConfigurationListener {

        public void refreshParameters();
    }

    private static final long serialVersionUID = -535437197367761680L;

    @Override
    public String getI18NKey() {
        return "refresh_parameters";
    }

    @Override
    public void createConfigurationWizard(ParameterType type, ConfigurationListener listener) {
        ((ParameterRefresher) listener).refreshParameters();
    }

}
