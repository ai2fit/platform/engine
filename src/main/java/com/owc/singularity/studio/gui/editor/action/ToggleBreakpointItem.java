/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.studio.gui.MainFrame;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa, Tobias Malbrecht
 */
public class ToggleBreakpointItem extends ProcessChangingToggleAction {

    private static final long serialVersionUID = 1727841552148351670L;

    private final int position;

    {
        setCondition(Condition.OPERATOR_SELECTED, ConditionReaction.MANDATORY);
        setCondition(Condition.ROOT_SELECTED, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    public ToggleBreakpointItem(final int position) {
        super(true, "breakpoint_" + BreakpointListener.BREAKPOINT_POS_NAME[position]);
        this.position = position;
    }

    @Override
    public void actionToggled(ActionEvent e) {
        Operator op = MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().getSelectedOperator();
        // don't allow breakpoints for AbstractRootOperator
        if (op != null && !(op instanceof AbstractRootOperator)) {
            op.setBreakpoint(position, !op.hasBreakpoint(position));
        }
        // compatibility clause to revoke breakpoints set before the fix above
        if (op instanceof AbstractRootOperator && op.hasBreakpoint(position)) {
            op.setBreakpoint(position, false);
        }

        // TODO: toggle (rather set to common state!) all operators would be nice
        // for (Operator op : this.actions.getSelectedOperators()) {
        // op.setBreakpoint(position, !op.hasBreakpoint(position));
        // }
    }
}
