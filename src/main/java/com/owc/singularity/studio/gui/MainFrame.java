/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.EngineExitMode;
import com.owc.singularity.engine.event.AbstractEventListener;
import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.UnknownParameterInformation;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.ProcessSetupError;
import com.owc.singularity.engine.operator.error.ProcessStoppedException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.*;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.tools.ProcessTools;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.engine.tools.SystemInfoUtilities.OperatingSystem;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.RecentLocationsManager;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.WorkspaceSessionManager;
import com.owc.singularity.studio.gui.actions.*;
import com.owc.singularity.studio.gui.actions.export.ShowPrintAndExportDialogAction;
import com.owc.singularity.studio.gui.animation.OperatorAnimationProcessListener;
import com.owc.singularity.studio.gui.dialog.UnknownParametersInfoDialog;
import com.owc.singularity.studio.gui.documentation.OperatorDocumentationDockable;
import com.owc.singularity.studio.gui.editor.*;
import com.owc.singularity.studio.gui.editor.action.InfoOperatorAction;
import com.owc.singularity.studio.gui.editor.action.ProcessActions;
import com.owc.singularity.studio.gui.editor.action.ToggleBreakpointItem;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessStorageListener;
import com.owc.singularity.studio.gui.editor.operators.OperatorsDockable;
import com.owc.singularity.studio.gui.editor.pipeline.ErrorTable;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessOverviewDockable;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotation;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessEditorModelEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererAnnotationEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererModelEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererOperatorEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererOperatorEvent.OperatorEvent;
import com.owc.singularity.studio.gui.editor.pipeline.tree.OperatorTree;
import com.owc.singularity.studio.gui.editor.pipeline.tree.OperatorTreeDockable;
import com.owc.singularity.studio.gui.execution.ConcurrentExecutionPipelinesView;
import com.owc.singularity.studio.gui.logging.LogViewPanel;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.osx.OSXAdapter;
import com.owc.singularity.studio.gui.osx.OSXQuitListener;
import com.owc.singularity.studio.gui.parameters.OperatorParameterPanel;
import com.owc.singularity.studio.gui.perspective.PerspectiveChangeListener;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.perspective.PerspectiveMenu;
import com.owc.singularity.studio.gui.repository.RepositoryBrowserDockable;
import com.owc.singularity.studio.gui.repository.RepositoryTreeCellRenderer;
import com.owc.singularity.studio.gui.results.ResultDisplay;
import com.owc.singularity.studio.gui.results.ResultDisplayTools;
import com.owc.singularity.studio.gui.security.PasswordManager;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.bubble.BubbleWindow;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;
import com.owc.singularity.studio.gui.tools.dialogs.DecisionRememberingConfirmDialog;
import com.owc.singularity.studio.gui.vcs.conflict.MergeConflictResolvingView;
import com.vlsolutions.swing.docking.DockGroup;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockingContext;
import com.vlsolutions.swing.docking.DockingDesktop;
import com.vlsolutions.swing.toolbars.ToolBarContainer;


/**
 * The main component class of the SingularityEngine GUI. The class holds a lot of Actions that can
 * be used for the toolbar and for the menu bar. MainFrame has methods for handling the process
 * (saving, opening, creating new). It keeps track of the state of the process and enables/disables
 * buttons. It must be notified whenever the process changes and propagates this event to its
 * children. Most of the code is enclosed within the Actions.
 *
 * @author Ingo Mierswa, Simon Fischer, Sebastian Land, Marius Helf, Jan Czogalla
 */
public class MainFrame extends ApplicationFrame implements WindowListener {

    private final InfoOperatorAction INFO_OPERATOR_ACTION = new InfoOperatorAction() {

        private static final long serialVersionUID = 6758272768665592429L;

        @Override
        protected Operator getOperator() {
            ProcessPanel processPanel = getMainProcessPanel();
            if (processPanel == null)
                return null;
            return processPanel.getProcessEditor().getSelectedOperator();
        }
    };

    public static void create() {
        // setting static accessor
        INSTANCE = new MainFrame();

        // instantiate animation listener: Will register itself
        new OperatorAnimationProcessListener();
    }

    /**
     * This listener takes care of changes relevant to the {@link MainFrame} itself, such as the
     * {@link LocalPipelineExecutor} and saving, loading or setting a process.
     *
     * @since 7.5
     * @author Jan Czogalla
     */
    private class MainProcessListener
            implements ProcessEditorEventListener, ProcessStorageListener, FocusProcessChangeEventListener, ProcessEditorModelEventListener {


        @Override
        public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
            if (oldPanel != null) {
                AbstractPipeline oldProcess = oldPanel.getProcess();
                // remove our listeners
                oldProcess.unregisterLifeCycleEventListener(globalProcessLifeCycleEventListener);
                oldProcess.removeBreakpointListener(breakpointListener);
            }

            AbstractPipeline currentProcess = null;
            if (currentlyFocusedPanel != null) {
                currentProcess = currentlyFocusedPanel.getProcess();
                processExecutor = null;
                // register our listeners to monitor the process
                currentProcess.registerLifeCycleEventListener(globalProcessLifeCycleEventListener);
                currentProcess.addBreakpointListener(breakpointListener);

                setTitle(currentProcess);
                currentlyFocusedPanel.getActions().enableActions();
            } else {
                setTitle((AbstractPipeline) null);
            }
            getStatusBar().clearSpecialText();
            if (currentProcess != null && !currentProcess.hasSaveDestination())
                SAVE_ACTION.setEnabled(true);
            else
                SAVE_ACTION.setEnabled(isChanged());

            // user might have changed another pipeline and we should revalidate this one if it
            // calls the other one.
            if (VALIDATE_AUTOMATICALLY_ACTION.isSelected()) {
                // not running at this point!
                validateProcess(true);
            }
        }

        @Override
        public void onProcessEdit(AbstractPipeline process) {
            setTitle(process);
            boolean changed = isChanged();
            SAVE_ACTION.setEnabled(changed);
            UNDO_ACTION.setEnabled(changed);
            validateProcess(false);
        }

        @Override
        public void onStore(AbstractPipeline process, RepositoryPath storedAt) {
            SwingUtilities.invokeLater(() -> {
                SAVE_ACTION.setEnabled(false);
                setTitle(process);
            });
            RecentLocationsManager.addToRecentFilesAsMostRecent(storedAt);
        }

        @Override
        public void onLoad(AbstractPipeline process, RepositoryPath loadedFrom) {
            if (process.getImportMessage() != null && process.getImportMessage().contains("error")) {
                SwingTools.showLongMessage("import_message", process.getImportMessage());
            }

            SAVE_ACTION.setEnabled(false);
            setTitle(process);

            RecentLocationsManager.addToRecentFilesAsMostRecent(loadedFrom);

            List<UnknownParameterInformation> unknownParameters;
            synchronized (process) {
                unknownParameters = process.getUnknownParameters();
            }

            // show unsupported parameters info?
            if (unknownParameters != null && unknownParameters.size() > 0) {
                final UnknownParametersInfoDialog unknownParametersInfoDialog = new UnknownParametersInfoDialog(MainFrame.this, unknownParameters);
                if (SwingUtilities.isEventDispatchThread()) {
                    unknownParametersInfoDialog.setVisible(true);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(() -> unknownParametersInfoDialog.setVisible(true));
                    } catch (Exception e) {
                        LogService.getRoot().warn("Error opening the unknown parameter dialog: " + e, e);
                    }
                }
            }

            if (VALIDATE_AUTOMATICALLY_ACTION.isSelected()) {
                // not running at this point!
                validateProcess(true);
            }
        }

        @Override
        public void modelChanged(ProcessRendererModelEvent e) {}

        @Override
        public void operatorsChanged(ProcessRendererOperatorEvent e, Collection<Operator> operators) {
            if (operators.size() > 0 && e.getEventType() != OperatorEvent.SELECTED_OPERATORS_CHANGED) {
                AbstractPipeline pipeline = operators.iterator().next().getPipeline();
                setTitle(pipeline);
                boolean changed = isChanged();
                SAVE_ACTION.setEnabled(changed);
                UNDO_ACTION.setEnabled(changed);

                if (e.getEventType() == OperatorEvent.PORTS_CHANGED)
                    validateProcess(false);
            }
        }

        @Override
        public void annotationsChanged(ProcessRendererAnnotationEvent e, Collection<WorkflowAnnotation> annotations) {}
    }

    private static final long serialVersionUID = 1L;

    private static final int MAX_LOCATION_TITLE_LENGTH = 150;

    private static String getFrameTitle() {
        return SingularityEngine.getApplicationTitle() + " " + SingularityEngine.getLongVersion();
    }

    // --------------------------------------------------------------------------------

    public static MainFrame INSTANCE;

    public static final int EDIT_MODE = 0;
    public static final int RESULTS_MODE = 1;

    public final transient Action AUTO_WIRE = new AutoWireAction();

    public final transient Action NEW_ANALYSIS_ACTION = new NewPipelineAction("pipeline.new.analysis", AnalysisPipeline::new, this);
    public final transient Action NEW_SCHEDULED_ANALYSIS_ACTION = new NewPipelineAction("pipeline.new.scheduled_analysis", ScheduledAnalysisPipeline::new,
            this);
    public final transient Action NEW_SUB_ACTION = new NewPipelineAction("pipeline.new.sub", SubPipeline::new, this);
    public final transient Action NEW_WEBSERVICE_ACTION = new NewPipelineAction("pipeline.new.webservice", WebservicePipeline::new, this);
    public final transient Action NEW_IMPEMENTATION_ACTION = new NewPipelineAction("pipeline.new.implementation", ImplementingPipeline::new, this);
    public final transient Action OPEN_ACTION = new OpenEntryAction();
    public final transient SaveAction SAVE_ACTION = new SaveAction();
    public final transient Action SAVE_AS_ACTION = new SaveAsAction();
    public final transient ToggleAction PROPAGATE_REAL_METADATA_ACTION = new PropagateRealMetaDataAction();

    // ---------- Export as Image/Print actions -----------------
    public final transient Action EXPORT_ACTION = new ShowPrintAndExportDialogAction(false);

    public final transient Action EXIT_ACTION = new ExitAction();

    public final transient RunAction RUN_ACTION = new RunAction();
    public final transient Action PAUSE_ACTION = new PauseAction();
    public final transient Action STOP_ACTION = new StopAction();
    public final transient Action VALIDATE_ACTION = new ValidateProcessAction();
    public final transient ToggleAction VALIDATE_AUTOMATICALLY_ACTION = new ValidateAutomaticallyAction();

    private transient JButton runRemoteToolbarButton;

    public final transient Action NEW_PERSPECTIVE_ACTION = new NewPerspectiveAction();
    public final transient Action RESTORE_PERSPECTIVE_ACTION = new RestoreDefaultPerspectiveAction();
    public final transient Action SETTINGS_ACTION = new SettingsAction();
    public final transient Action UNDO_ACTION = new UndoAction();
    public final transient Action REDO_ACTION = new RedoAction();
    public final transient Action SEARCH_OPERATOR_ACTION = new SearchOperatorAction();


    public final transient Action FEEDBACK_ACTION = new FeedbackAction();
    public final transient Action BROWSE_SUPPORT_ACTION = new BrowseAction("toolbar_resources.support", URI.create("https://support.oldworldcomputing.com"));
    public final transient Action ABOUT_ACTION = new AboutAction();

    // --------------------------------------------------------------------------------

    // DOCKING

    public static final DockGroup DOCK_GROUP_ROOT = new DockGroup("root");
    public static final DockGroup DOCK_GROUP_RESULTS = new DockGroup("results");

    private final DockingContext dockingContext = new DockingContext();
    private final DockingDesktop dockingDesktop = new DockingDesktop("mainDesktop", dockingContext);

    private final MainProcessListener mainProcessEventListener = new MainProcessListener();

    private final OperatorDocumentationDockable operatorDocumentationBrowser = new OperatorDocumentationDockable();
    private final OperatorTreeDockable operatorTreeDockable = new OperatorTreeDockable();
    private final ErrorTable errorTable = new ErrorTable();
    private final OperatorParameterPanel operatorPropertyPanel = new OperatorParameterPanel(this);
    private final MergeConflictResolvingView mergeConflictResolvingView = new MergeConflictResolvingView();
    private final ProcessXMLPanel processXMLPanel = new ProcessXMLPanel();
    private final DevelopmentExecutionContextPanel developmentExecutionContextPanel = new DevelopmentExecutionContextPanel();
    private final PipelineParametersPanel pipelineParametersPanel = new PipelineParametersPanel();
    private final ProcessTabViewDockable processTabViewDockable = new ProcessTabViewDockable();

    private final OperatorsDockable operatorBrowser = new OperatorsDockable(processTabViewDockable.getDragListener());
    private final RepositoryBrowserDockable repositoryBrowser = new RepositoryBrowserDockable(processTabViewDockable.getDragListener());
    private final VariableViewer variableViewer = new VariableViewer();
    /** the overview panel */
    private final ProcessOverviewDockable processOverviewDockable = new ProcessOverviewDockable();

    private final ResultDisplay resultDisplay = ResultDisplayTools.makeResultDisplay();
    private final LogViewPanel logViewer = new LogViewPanel();
    private final SystemMonitor systemMonitor = new SystemMonitor();

    private final PerspectiveController perspectiveController = new PerspectiveController(dockingContext);

    /** the bubble which displays a warning that no result ports are connected */
    private BubbleWindow noResultConnectionBubble;
    /** the bubble which displays a warning that a port must receive input but is not connected */
    private BubbleWindow missingInputBubble;
    /**
     * the bubble which displays a warning that a parameter must be set as he has no default value
     */
    private BubbleWindow missingParameterBubble;

    private final JMenuBar menuBar;

    private final MainToolBar toolBar;

    private final JMenu fileMenu;

    private final JMenu editMenu;

    private final JMenu processMenu;

    private final JMenu settingsMenu;

    private final JMenu viewMenu;

    private final JMenu helpMenu;

    private final JMenu extensionsMenu;

    private DockableMenu dockableMenu;

    private final JMenu recentFilesMenu = new ResourceMenu("recent_files");

    private Insets menuBarInsets = new Insets(0, 0, 0, 5);
    private ProcessPanel mainProcessPanel;

    /**
     * The host name of the system. Might be empty (no host name will be shown) and will be
     * initialized in the first call of {@link #setTitle(AbstractPipeline)}.
     */
    private String hostname = null;

    private transient LocalPipelineExecutor processExecutor;

    private final MetaDataUpdateQueue metaDataUpdateQueue = new MetaDataUpdateQueue();

    // --------------------------------------------------------------------------------
    // LISTENERS And OBSERVERS
    private final EventListenerList eventListenerList = new EventListenerList();
    private final List<Callable<Boolean>> closeHooks = new ArrayList<>();

    public void validateProcess(final boolean force) {
        if (force || getProcessState() != PipelineState.RUNNING) {
            metaDataUpdateQueue.validate(getProcess(), force || VALIDATE_AUTOMATICALLY_ACTION.isSelected());
        }
    }

    private static class StatsCollectorPipelineLifeCycleEventListener extends AbstractEventListener implements PipelineLifeCycleEventListener {

        public StatsCollectorPipelineLifeCycleEventListener() {
            super("mainframe.stats");
        }

        @Override
        public void onBeforePipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            ActionStatisticsCollector.getInstance().logExecutionStarted(pipeline);
        }

        @Override
        public void onAfterPipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            ActionStatisticsCollector.getInstance().logExecution(pipeline);
        }

        @Override
        public void onAfterPipelineStop(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            if (details.hasArgs()) {
                final Object firstArg = details.getArgs()[0];
                if (firstArg instanceof ProcessStoppedException stoppedException) {
                    Operator op = stoppedException.getOperator();
                    ActionStatisticsCollector.getInstance().log(op, ActionStatisticsCollector.OPERATOR_EVENT_STOPPED);
                } else if (firstArg instanceof UserError) {
                    ActionStatisticsCollector.getInstance().log(pipeline.getCurrentOperator(), ActionStatisticsCollector.OPERATOR_EVENT_FAILURE);
                    ActionStatisticsCollector.getInstance().log(((UserError) firstArg).getOperator(), ActionStatisticsCollector.OPERATOR_EVENT_USER_ERROR);
                } else if (firstArg instanceof OperatorException) {
                    ActionStatisticsCollector.getInstance().log(pipeline.getCurrentOperator(), ActionStatisticsCollector.OPERATOR_EVENT_FAILURE);
                    ActionStatisticsCollector.getInstance().log(pipeline.getCurrentOperator(), ActionStatisticsCollector.OPERATOR_EVENT_OPERATOR_EXCEPTION);
                } else if (firstArg instanceof Exception) {
                    ActionStatisticsCollector.getInstance().logExecutionException(pipeline, (Exception) firstArg);
                }
            }
            ActionStatisticsCollector.getInstance().logExecutionFinished(pipeline);
        }

        @Override
        public void onAfterPipelineFinish(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            ActionStatisticsCollector.getInstance().logExecutionSuccess(pipeline);
        }
    }

    private transient final StatsCollectorPipelineLifeCycleEventListener globalProcessLifeCycleEventListener = new StatsCollectorPipelineLifeCycleEventListener();
    private transient final BreakpointListener breakpointListener = new BreakpointListener() {

        @Override
        public void breakpointReached(final AbstractPipeline process, final Operator operator, final List<IOObject> objects, final int location) {
            if (process.equals(getProcess())) {
                RUN_ACTION.setState(process.getState());
                LocalPipelineExecutor.beep("breakpoint");
                MainFrame.this.toFront();
                getMainProcessPanel().getProcessEditor().selectAndShowOperator(operator, true);
                resultDisplay.showData(objects, "Breakpoint in " + operator.getName() + ", application " + operator.getApplyCount(),
                        SwingTools.createIcon("16/breakpoint.png"));
                validateProcess(true);
            }
        }

        /** Since the mainframe triggers the resume itself this method does nothing. */
        @Override
        public void resume() {
            RUN_ACTION.setState(getProcessState());
        }
    };

    private JToolBar buttonToolbar;

    // --------------------------------------------------------------------------------

    /** Creates a new main frame containing the SingularityEngine GUI. */
    private MainFrame() {
        super(getFrameTitle());

        // creating instance
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(this);
        if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
            // load dock icon from resources and adapt UI via OSXAdapter class
            try {
                OSXQuitListener quitListener = new OSXQuitListener() {

                    @Override
                    public void quit() {
                        MainFrame.this.exit(false);
                    }
                };
                OSXAdapter.adaptUI(this, SETTINGS_ACTION, new AboutAction(), quitListener);
            } catch (Throwable t) {
                // catch everything - in case the OSX adapter is called without being on a OS X
                // system
                // or the Java classes have been removed from OS X JRE it will just log an error
                // instead of breaking the program start-up
                LogService.getRoot().warn("com.owc.singularity.studio.gui.MainFrame.could_not_adapt_OSX_look_and_feel", t);
            }
        }

        SwingTools.setFrameIcon(this);

        // load perspectives now because otherwise the WSDesktop class does not know the nodes and
        // won't restore the user customized perspective
        perspectiveController.loadAll();

        dockingDesktop.registerDockable(repositoryBrowser);
        dockingDesktop.registerDockable(operatorTreeDockable);
        dockingDesktop.registerDockable(operatorPropertyPanel);
        dockingDesktop.registerDockable(processTabViewDockable);
        dockingDesktop.registerDockable(mergeConflictResolvingView);
        dockingDesktop.registerDockable(processXMLPanel);
        dockingDesktop.registerDockable(operatorBrowser);
        dockingDesktop.registerDockable(errorTable);
        dockingDesktop.registerDockable(resultDisplay);
        dockingDesktop.registerDockable(getLogViewer());
        dockingDesktop.registerDockable(systemMonitor);
        dockingDesktop.registerDockable(operatorDocumentationBrowser);
        dockingDesktop.registerDockable(developmentExecutionContextPanel);
        dockingDesktop.registerDockable(pipelineParametersPanel);
        dockingDesktop.registerDockable(processOverviewDockable);
        dockingDesktop.registerDockable(variableViewer);
        dockingDesktop.registerDockable(new ConcurrentExecutionPipelinesView());
        // Test

        ToolBarContainer toolBarContainer = ToolBarContainer.createDefaultContainer(true, true, true, true);
        toolBarContainer.setBorder(BorderFactory.createEmptyBorder(6, 3, 0, 3));
        toolBarContainer.setOpaque(true);
        toolBarContainer.setBackground(Colors.WINDOW_BACKGROUND);
        getContentPane().add(toolBarContainer, BorderLayout.CENTER);
        toolBarContainer.add(dockingDesktop, BorderLayout.CENTER);

        systemMonitor.startMonitorThread();

        addFocusedProcessChangedListener(operatorPropertyPanel);
        addFocusedProcessChangedListener(operatorDocumentationBrowser);
        addFocusedProcessChangedListener(mainProcessEventListener);
        addFocusedProcessChangedListener(variableViewer);
        addFocusedProcessChangedListener(developmentExecutionContextPanel);
        addFocusedProcessChangedListener(pipelineParametersPanel);
        addFocusedProcessChangedListener(processXMLPanel);
        addFocusedProcessChangedListener(processOverviewDockable);
        addFocusedProcessChangedListener(getOperatorTreePanel());
        addFocusedProcessChangedListener(resultDisplay);
        addFocusedProcessChangedListener(getStatusBar());
        processTabViewDockable.addTabChangeListener(e -> setMainProcessPanel(processTabViewDockable.getMainProcessPanel()));
        resultDisplay.init(this);

        // menu bar
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        fileMenu = new ResourceMenu("file");
        fileMenu.setMargin(menuBarInsets);
        JMenu newMenu = new ResourceMenu("pipeline.new");

        newMenu.add(NEW_ANALYSIS_ACTION);
        newMenu.add(NEW_SCHEDULED_ANALYSIS_ACTION);
        newMenu.add(NEW_SUB_ACTION);
        newMenu.add(NEW_WEBSERVICE_ACTION);
        newMenu.add(NEW_IMPEMENTATION_ACTION);
        fileMenu.add(newMenu);
        fileMenu.add(OPEN_ACTION);
        updateRecentFileList();
        fileMenu.add(recentFilesMenu);
        fileMenu.addSeparator();
        fileMenu.add(SAVE_ACTION);
        fileMenu.add(SAVE_AS_ACTION);
        fileMenu.addSeparator();
        menuBar.add(fileMenu);


        // edit menu
        editMenu = new ResourceMenu("edit");
        editMenu.addMenuListener(new MenuListener() {

            @Override
            public void menuSelected(MenuEvent e) {
                ProcessPanel panel = getMainProcessPanel();
                if (panel == null)
                    return;
                panel.getActions().enableActions();
            }

            @Override
            public void menuDeselected(MenuEvent e) {

            }

            @Override
            public void menuCanceled(MenuEvent e) {

            }
        });
        editMenu.setMargin(menuBarInsets);
        menuBar.add(editMenu);


        // process menu
        processMenu = new ResourceMenu("process");
        processMenu.setMargin(menuBarInsets);
        menuBar.add(processMenu);


        // view menu
        viewMenu = new ResourceMenu("view");
        viewMenu.setMargin(menuBarInsets);
        viewMenu.add(new PerspectiveMenu(perspectiveController));
        viewMenu.add(NEW_PERSPECTIVE_ACTION);
        viewMenu.add(dockableMenu = new DockableMenu(dockingContext));
        viewMenu.addMenuListener(new MenuListener() {

            @Override
            public void menuSelected(MenuEvent e) {
                dockableMenu.fill();
            }

            @Override
            public void menuDeselected(MenuEvent e) {
                // ignore
            }

            @Override
            public void menuCanceled(MenuEvent e) {
                // ignore
            }
        });
        viewMenu.add(RESTORE_PERSPECTIVE_ACTION);


        menuBar.add(viewMenu);

        // create settings menu (will be added in finishInitialization())
        settingsMenu = new ResourceMenu("settings");
        settingsMenu.setMargin(menuBarInsets);

        // help menu
        helpMenu = new ResourceMenu("help");
        helpMenu.add(ProcessActions.INFO_OPERATOR_ACTION);
        helpMenu.addSeparator();
        helpMenu.add(FEEDBACK_ACTION);
        helpMenu.add(BROWSE_SUPPORT_ACTION);
        helpMenu.addSeparator();
        helpMenu.add(ABOUT_ACTION);

        // extensions menu
        extensionsMenu = new ResourceMenu("extensions");

        // main tool bar
        toolBar = new MainToolBar(this);

        getStatusBar().setBackground(Colors.WINDOW_BACKGROUND);
        getContentPane().add(getToolBar(), BorderLayout.NORTH);
        getContentPane().add(getStatusBar(), BorderLayout.SOUTH);
        getStatusBar().startClockThread();

        // check all ConditionalActions on perspective switch
        // try to request focus for the process renderer so actions are enabled after
        // perspective switch and ProcessRenderer is visible
        PerspectiveChangeListener perspectiveChangeListener = (from, to) -> {

            SwingTools.invokeLater(() -> {
                ProcessPanel panel = getMainProcessPanel();
                if (panel == null)
                    return;
                // check all ConditionalActions on perspective switch
                panel.getActions().enableActions();
                // try to request focus for the process renderer so actions are enabled after
                // perspective switch and ProcessRenderer is visible
                if (panel.getProcessEditor().isShowing()) {
                    panel.getProcessEditor().requestFocusInWindow();
                }
            });
        };
        perspectiveController.addPerspectiveChangeListener(perspectiveChangeListener);
        pack();
        metaDataUpdateQueue.start();
        addCloseHook(this::checkBeforeClose);
        addCloseHook(this::persistWorkspace);
    }

    public void setMainProcessPanel(ProcessPanel processPanel) {
        if (mainProcessPanel == null || getProcessState() == PipelineState.STOPPED) {
            if (!Objects.equals(mainProcessPanel, processPanel)) {
                ProcessPanel from = null;
                ProcessPanel to = null;
                if (mainProcessPanel != null) {
                    removeProcessListeners(mainProcessPanel);
                    from = mainProcessPanel;
                }
                mainProcessPanel = processPanel;
                if (mainProcessPanel != null) {
                    addProcessListeners(mainProcessPanel);
                    to = mainProcessPanel;
                }
                setFocusedProcessPanel(processPanel);
                fireProcessSelectionChangedEvent(from, to);
            }
        }
    }

    /**
     * Adds all relevant {@link ProcessEditorEventListener ProcessEditors}. Adds the
     * {@link MainProcessListener} as first.
     */
    private void addProcessListeners(ProcessPanel mainProcessPanel) {
        ProcessEditorPanel processEditorPanel = mainProcessPanel.getProcessEditor();
        mainProcessPanel.addProcessStorageListener(mainProcessEventListener);
        processEditorPanel.addProcessEditorEventListener(mainProcessEventListener);
        processEditorPanel.getModel().registerEditorModelEventListener(mainProcessEventListener);

        processEditorPanel.addProcessEditorEventListener(processXMLPanel);
        processEditorPanel.addProcessInteractionListener(processXMLPanel);

        processEditorPanel.addProcessEditorEventListener(developmentExecutionContextPanel);
        processEditorPanel.addProcessEditorEventListener(pipelineParametersPanel);
        processEditorPanel.addProcessEditorEventListener(operatorPropertyPanel);
        processEditorPanel.addProcessInteractionListener(operatorPropertyPanel);

        processEditorPanel.addProcessInteractionListener(operatorTreeDockable.getOperatorTree());

        processEditorPanel.addProcessInteractionListener(operatorDocumentationBrowser);

        processEditorPanel.addProcessInteractionListener(errorTable);
    }

    /**
     * Adds all relevant {@link ProcessEditorEventListener ProcessEditors}. Adds the
     * {@link MainProcessListener} as first.
     */
    private void removeProcessListeners(ProcessPanel mainProcessPanel) {
        ProcessEditorPanel processEditorPanel = mainProcessPanel.getProcessEditor();
        mainProcessPanel.removeProcessStorageListener(mainProcessEventListener);
        processEditorPanel.removeProcessEditorEventListener(mainProcessEventListener);

        processEditorPanel.removeProcessEditorEventListener(processXMLPanel);
        processEditorPanel.removeProcessInteractionListener(processXMLPanel);

        processEditorPanel.removeProcessEditorEventListener(developmentExecutionContextPanel);
        processEditorPanel.removeProcessEditorEventListener(pipelineParametersPanel);
        processEditorPanel.removeProcessEditorEventListener(operatorPropertyPanel);
        processEditorPanel.removeProcessInteractionListener(operatorPropertyPanel);

        processEditorPanel.removeProcessInteractionListener(operatorTreeDockable.getOperatorTree());

        processEditorPanel.removeProcessInteractionListener(operatorDocumentationBrowser);

        processEditorPanel.removeProcessInteractionListener(errorTable);
    }

    /**
     * Finishes the MainFrame initialization. Should be called after all extension have been
     * initialized.
     */
    public void finishInitialization() {
        INFO_OPERATOR_ACTION.addToActionMap(JComponent.WHEN_FOCUSED, true, true, true, null, getOperatorTreePanel());
        ProcessActions.TOGGLE_ACTIVATION_ITEM.addToActionMap(JComponent.WHEN_FOCUSED, true, true, true, null, getOperatorTreePanel());
        ProcessActions.RENAME_OPERATOR_ACTION.addToActionMap(JComponent.WHEN_FOCUSED, true, true, true, null, getOperatorTreePanel());
        // not added for ProcessRenderer because there the DELETE_SELECTED_CONNECTION action is
        // active
        ProcessActions.DELETE_OPERATOR_ACTION.addToActionMap(JComponent.WHEN_FOCUSED, true, true, true, null, getOperatorTreePanel());
        ProcessActions.TOGGLE_ALL_BREAKPOINTS.addToActionMap(JComponent.WHEN_FOCUSED, true, true, true, null, getOperatorTreePanel());
        // edit menu
        editMenu.add(UNDO_ACTION);
        editMenu.add(REDO_ACTION);
        editMenu.addSeparator();
        editMenu.add(ProcessActions.TOGGLE_ACTIVATION_ITEM.createMenuItem());
        editMenu.add(ProcessActions.RENAME_OPERATOR_ACTION);
        editMenu.add(SEARCH_OPERATOR_ACTION);
        editMenu.addSeparator();
        editMenu.add(ProcessActions.CUT_ACTION);
        editMenu.add(ProcessActions.COPY_ACTION);
        editMenu.add(ProcessActions.PASTE_ACTION);
        editMenu.add(ProcessActions.DELETE_ACTION);
        // editMenu.add(actions.MAKE_DIRTY_ACTION);

        // process menu
        processMenu.add(RUN_ACTION);
        processMenu.add(PAUSE_ACTION);
        processMenu.add(STOP_ACTION);
        processMenu.addSeparator();
        ResourceMenu switchMenu = new ResourceMenu("switch_type");
        switchMenu.add(new SwitchPipelineTypeAction("analysis", AnalysisPipeline.class));
        switchMenu.add(new SwitchPipelineTypeAction("scheduled_analysis", ScheduledAnalysisPipeline.class));
        switchMenu.add(new SwitchPipelineTypeAction("sub", SubPipeline.class));
        switchMenu.add(new SwitchPipelineTypeAction("webservice", WebservicePipeline.class));
        switchMenu.add(new SwitchPipelineTypeAction("implementing", ImplementingPipeline.class));
        processMenu.add(switchMenu);
        processMenu.addSeparator();
        for (ToggleBreakpointItem item : ProcessActions.TOGGLE_BREAKPOINT) {
            processMenu.add(item.createMenuItem());
        }
        processMenu.add(ProcessActions.TOGGLE_ALL_BREAKPOINTS.createMenuItem());
        processMenu.add(ProcessActions.REMOVE_ALL_BREAKPOINTS);
        processMenu.addSeparator();
        processMenu.add(PROPAGATE_REAL_METADATA_ACTION.createMenuItem());
        processMenu.add(VALIDATE_ACTION);
        processMenu.add(VALIDATE_AUTOMATICALLY_ACTION.createMenuItem());
        processMenu.addSeparator();

        processMenu.add(AUTO_WIRE);
        JMenu layoutMenu = new ResourceMenu("process_layout");
        layoutMenu.add(ProcessActions.ARRANGE_OPERATORS_ACTION);
        layoutMenu.add(ProcessActions.AUTO_FIT_ACTION);
        processMenu.add(layoutMenu);

        // add export and exit as last file menu actions
        fileMenu.addSeparator();
        fileMenu.add(EXPORT_ACTION);
        fileMenu.addSeparator();
        fileMenu.add(EXIT_ACTION);


        // Password Manager
        settingsMenu.add(PasswordManager.OPEN_WINDOW);
        if (SystemInfoUtilities.getOperatingSystem() != OperatingSystem.OSX || !OSXAdapter.isAdapted()) {
            settingsMenu.add(SETTINGS_ACTION);
        }

        // add settings menu as second last menu or third last if there are entries in the help menu
        menuBar.add(settingsMenu);

        if (extensionsMenu.getItemCount() > 0) {
            // add extensions menu as last menu or second last if there are entries in the help
            // menu
            extensionsMenu.setMargin(menuBarInsets);
            menuBar.add(extensionsMenu);
        }

        // Add Help menu as last entry if it is not empty
        if (helpMenu.getItemCount() > 0) {
            helpMenu.setMargin(menuBarInsets);
            menuBar.add(helpMenu);
        }
    }

    public OperatorParameterPanel getOperatorPropertyPanel() {
        return operatorPropertyPanel;
    }

    public OperatorsDockable getNewOperatorEditor() {
        return operatorBrowser;
    }

    public OperatorTree getOperatorTreePanel() {
        return operatorTreeDockable.getOperatorTree();
    }

    public ResultDisplay getResultDisplay() {
        return resultDisplay;
    }

    /**
     * @return the toolbar button for running processes on the Server
     */
    public JButton getRunRemoteToolbarButton() {
        return runRemoteToolbarButton;
    }

    public PipelineState getProcessState() {
        AbstractPipeline process = getProcess();
        if (process == null) {
            return PipelineState.UNKNOWN;
        } else {
            return process.getState();
        }
    }

    public final AbstractPipeline getProcess() {
        ProcessPanel processPanel = getMainProcessPanel();
        return processPanel != null ? processPanel.getProcess() : null;
    }

    // ====================================================
    // M A I N A C T I O N S
    // ===================================================

    /**
     * Runs or resumes the current process.
     *
     * @param precheckBeforeExecution
     *            if {@code true} and the process is started, checks for potential errors first and
     *            prevents execution unless the user has disabled the pre-run check
     */
    public void runProcess(boolean precheckBeforeExecution) {
        ProcessPanel processPanel = getMainProcessPanel();
        if (processPanel == null)
            return;
        AbstractPipeline process = processPanel.getProcess();
        if (process.getState() == PipelineState.STOPPED) {
            // Run
            if (processPanel.isEdited() || process.getPath() == null) {
                if (DecisionRememberingConfirmDialog.confirmAction("save_before_run", StudioProperties.PROPERTY_SINGULARITY_GUI_SAVE_BEFORE_RUN)) {
                    SaveAction.saveAsync(process);
                }
            }

            // don't run process if showstoppers are present
            // this only returns true if the user did not disable the strict process check in the
            // preferences
            if (precheckBeforeExecution && doesProcessContainShowstoppers(processPanel)) {
                return;
            }

            // we need to prevent the user from switching to another process
            processTabViewDockable.lockTab(processPanel);
            processTabViewDockable.setTabCloseCondition(panel -> {
                if (panel != processPanel) {
                    return true;
                }
                if (isProcessRunning()) {
                    // trying to close the running process
                    int closeRunningProcess = SwingTools.showConfirmDialog("close_running_process", ConfirmDialog.YES_NO_OPTION);
                    if (closeRunningProcess == ConfirmDialog.NO_OPTION)
                        return false;
                    stopProcess();
                }
                return true;
            });
            processExecutor = new LocalPipelineExecutor(process, results -> {
                processTabViewDockable.unlock();
                processTabViewDockable.setTabCloseCondition(null);
                processEnded(results);
            });
            try {
                processExecutor.run();
            } catch (Exception t) {
                processTabViewDockable.unlock();
                processTabViewDockable.setTabCloseCondition(null);
                SwingTools.showSimpleErrorMessage("cannot_start_process", t);
            }
        } else {
            process.resume();
        }
    }

    /**
     * Can be used to stop the currently running process. Please note that the ProcessThread will
     * still be running in the background until the current operator is finished.
     */
    public void stopProcess() {
        if (processExecutor == null)
            return;
        AbstractPipeline process = processExecutor.getPipeline();
        if (process != null && process.getState() != PipelineState.STOPPED && processExecutor.isAlive()) {
            getStatusBar().setSpecialText("Pipeline stopped. Completing current operator.");
            processExecutor.stopProcess();
        }
    }

    public void pauseProcess() {
        if (getProcessState() == PipelineState.RUNNING) {
            getStatusBar().setSpecialText("Pipeline paused. Completing current operator.");
            if (processExecutor != null) {
                processExecutor.pauseProcess();
            }
        }
    }

    /** Will be invoked from the process thread after the process was successfully ended. */
    private void processEnded(final List<IOObject> results) {
        if (results != null) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    MainFrame.this.toFront();
                    resultDisplay.showData(results, "Pipeline results", SwingTools.createIcon("16/check.png"));
                    validateProcess(true);
                }
            });
        }
    }

    /** Returns true if the process has changed since the last save. */
    public boolean isChanged() {
        ProcessPanel processPanel = getMainProcessPanel();
        return processPanel != null && processPanel.isEdited();
    }

    /**
     * Sets the window title (SingularityEngine + filename + an asterisk if process was modified.
     */
    public void setTitle(AbstractPipeline process) {
        if (hostname == null) {
            try {
                hostname = " @ " + InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                hostname = "";
            }
        }

        if (process != null) {
            RepositoryPath loc = process.getPath();
            String locString;
            if (loc != null) {
                locString = loc.isHead() ? loc.toShortString(MAX_LOCATION_TITLE_LENGTH)
                        : loc.toShortString(MAX_LOCATION_TITLE_LENGTH) + " #" + loc.getVersionId().substring(0, 8);
            } else {
                locString = "<new pipeline";
            }
            setTitle(locString + (isChanged() ? "*" : "") + (loc == null ? ">" : "") + " \u2013 " + getFrameTitle() + hostname);
        } else {
            setTitle(getFrameTitle() + hostname);
        }
    }

    // //////////////////// File menu actions ////////////////////

    private void exit(final boolean relaunch) {
        stopProcess();
        dispose();
        SingularityEngine.stop(relaunch ? EngineExitMode.RELAUNCH : EngineExitMode.NORMAL);
    }

    private boolean checkBeforeClose() {
        for (ProcessPanel panel : processTabViewDockable.getProcessPanels()) {
            if (panel.isEdited()) {
                AbstractPipeline process = panel.getProcess();
                if (process != null) {
                    RepositoryPath loc = process.getPath();
                    String locName;
                    if (loc != null) {
                        locName = " to " + loc;
                    } else {
                        locName = "";
                    }
                    processTabViewDockable.select(panel);
                    switch (SwingTools.showConfirmDialog("save", ConfirmDialog.YES_NO_CANCEL_OPTION, panel.getName(), locName)) {
                        case ConfirmDialog.YES_OPTION:
                            SaveAction.save(process);
                            if (isChanged()) {
                                return false;
                            }
                            break;
                        case ConfirmDialog.NO_OPTION:
                            break;
                        case ConfirmDialog.CANCEL_OPTION:
                        default:
                            return false;
                    }
                }
            } else {
                // ask for special confirmation before exiting SingularityEngine while a process is
                // running!
                if (getProcessState() == PipelineState.RUNNING || getProcessState() == PipelineState.PAUSED) {
                    if (SwingTools.showConfirmDialog("exit_despite_running_process", ConfirmDialog.YES_NO_OPTION) == ConfirmDialog.NO_OPTION) {
                        return false;
                    }
                } else {
                    int answer = ConfirmDialog.showConfirmDialog(ApplicationFrame.getApplicationFrame(), "exit", ConfirmDialog.YES_NO_OPTION,
                            StudioProperties.PROPERTY_CONFIRM_EXIT, ConfirmDialog.YES_OPTION);
                    if (answer != ConfirmDialog.YES_OPTION) {
                        return false;
                    }
                }
            }
        }
        StudioProperties.saveGUIProperties();
        return true;
    }

    private boolean persistWorkspace() {
        return WorkspaceSessionManager.saveCurrentSession(processTabViewDockable.getProcessPanels());
    }

    /** Updates the list of recently used files. */
    public void updateRecentFileList() {
        recentFilesMenu.removeAll();
        ClassLoader classLoader = ModuleService.getMajorClassLoader();
        List<RepositoryPath> recentFiles = RecentLocationsManager.getRecentFiles();
        ImageIcon icon;
        for (final RepositoryPath recentLocation : recentFiles) {
            Entry entry = new Entry(recentLocation);
            Class<?> entryClass;
            try {
                entryClass = entry.getContentClass(classLoader);
                if (AbstractPipeline.class.isAssignableFrom(entryClass)) {
                    PipelineMetaData metaPipeline = entry.loadMetaData(PipelineMetaData.class, ModuleService.getMajorClassLoader());
                    icon = RepositoryTreeCellRenderer.getPipelineIconFromMetadata(metaPipeline);
                } else {
                    icon = Icons.fromName("pipeline.png").getIcon16x16();
                }
            } catch (IOException | ClassNotFoundException e) {
                icon = Icons.fromName("pipeline.png").getIcon16x16();
            }
            // whitespaces to create a gap between icon and text as #setIconTextGap(int) sets a gap
            // to both sides of the icon...
            JMenuItem menuItem = new JMenuItem("   " + recentLocation.toShortString(75));
            menuItem.setIcon(icon);
            menuItem.addActionListener(e -> OpenEntryAction.showProcess(Entries.getEntry(recentLocation)));
            menuItem.addMouseListener(new ExpandRecentFilePathOnHoverMouseEvent(recentLocation));
            recentFilesMenu.add(menuItem);
        }
    }


    private class ExpandRecentFilePathOnHoverMouseEvent extends MouseAdapter {

        RepositoryPath recentLocation;

        ExpandRecentFilePathOnHoverMouseEvent(RepositoryPath recentLocation) {
            this.recentLocation = recentLocation;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((JMenuItem) e.getSource()).setText("   " + recentLocation.toString());
            recentFilesMenu.getPopupMenu().pack();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((JMenuItem) e.getSource()).setText("   " + recentLocation.toShortString(75));
            recentFilesMenu.getPopupMenu().pack();
        }
    }

    public void addFocusedProcessChangedListener(FocusProcessChangeEventListener listener) {
        eventListenerList.add(FocusProcessChangeEventListener.class, listener);
    }

    public void removeProcessSelectionListener(FocusProcessChangeEventListener listener) {
        eventListenerList.remove(FocusProcessChangeEventListener.class, listener);
    }

    public void addCloseHook(Callable<Boolean> hook) {
        closeHooks.add(hook);
    }

    public void close() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void windowOpened(final WindowEvent e) {}

    @Override
    public void windowClosing(final WindowEvent e) {
        for (Callable<Boolean> closeHook : closeHooks) {
            try {
                Boolean shouldContinue = closeHook.call();
                if (!shouldContinue) {
                    return;
                }
            } catch (Exception ex) {
                LogService.getRoot().warn("Error running close hook: " + closeHook, e);
            }
        }
        dispose();
    }

    @Override
    public void windowClosed(final WindowEvent e) {
        exit(false);
    }

    @Override
    public void windowIconified(final WindowEvent e) {}

    @Override
    public void windowDeiconified(final WindowEvent e) {}

    @Override
    public void windowActivated(final WindowEvent e) {}

    @Override
    public void windowDeactivated(final WindowEvent e) {}

    /**
     * This methods provide plugins the possibility to modify the menus
     */
    public void removeMenu(final int index) {
        menuBar.remove(menuBar.getMenu(index));
    }

    public void removeMenuItem(final int menuIndex, final int itemIndex) {
        menuBar.getMenu(menuIndex).remove(itemIndex);
    }

    public void addMenuItem(final int menuIndex, final int itemIndex, final JMenuItem item) {
        menuBar.getMenu(menuIndex).add(item, itemIndex);
    }

    public void addMenu(int menuIndex, final JMenu menu) {
        menu.setMargin(menuBarInsets);
        if (menuIndex < -1 || menuIndex >= menuBar.getComponentCount()) {
            menuIndex = -1;
        }
        menuBar.add(menu, menuIndex);
    }

    public void addMenuSeparator(final int menuIndex) {
        menuBar.getMenu(menuIndex).addSeparator();
    }

    // / LISTENERS

    public DockingDesktop getDockingDesktop() {
        return dockingDesktop;
    }

    public DockingContext getDockingContext() {
        return dockingContext;
    }

    public PerspectiveController getPerspectiveController() {
        return perspectiveController;
    }

    public ProcessPanel openProcess(AbstractPipeline pipeline) {
        return openProcess(pipeline, false);
    }

    public ProcessPanel openProcess(Entry processEntry) {
        try {
            AbstractPipeline process = processEntry.loadData(AbstractPipeline.class, ModuleService.getMajorClassLoader());
            process.setPath(processEntry.getPath());
            return openProcess(process, !processEntry.getPath().isHead());
        } catch (IOException ex) {
            return handleBrokenProcessXML(processEntry, ex);
        }
    }

    public ProcessPanel openProcess(AbstractPipeline pipeline, boolean readOnly) {
        return openProcess(pipeline, readOnly, false);
    }

    public ProcessPanel replaceProcess(AbstractPipeline oldPipeline, AbstractPipeline newPipeline) {
        ProcessPanel processPanel = processTabViewDockable.findPanelWith(oldPipeline);
        if (processPanel != null) {
            // then it is already opened
            setFocusedProcessPanel(processPanel);
            processPanel.setProcess(newPipeline);
            processPanel.getProcessEditor().getModel().processWasChanged();
            processPanel.getProcessEditor().getModel().fireProcessEdited();
            // we have to focus again to set right pipeline in focus
            setMainProcessPanel(processPanel);
            return processPanel;
        }
        return openProcess(newPipeline, false, true);
    }

    public ProcessPanel openProcess(AbstractPipeline pipeline, boolean readOnly, boolean force) {
        // switch to design perspective if not already on it
        if (!perspectiveController.getSelectedPerspective().getName().equals(PerspectiveController.DESIGN)) {
            perspectiveController.showPerspective(PerspectiveController.DESIGN);
        }
        RepositoryPath pipelinePath = pipeline.getPath();
        if (pipelinePath != null) {
            RecentLocationsManager.addToRecentFilesAsMostRecent(pipelinePath);
            // then we have a storage location and must check for reloading
            ProcessPanel processPanel = processTabViewDockable.findPanelWith(pipelinePath);
            if (processPanel != null) {
                // then it is already opened
                setMainProcessPanel(processPanel);
                if (processPanel.isEdited() && !force) {
                    // ask whether really reloading and discard changes
                    int option = ConfirmDialog.showConfirmDialog(MainFrame.INSTANCE, "reload_pipeline", ConfirmDialog.YES_NO_OPTION, null, -1);
                    if (option == ConfirmDialog.NO_OPTION) {
                        // if not, don't do anything except focussing
                        setMainProcessPanel(processPanel);
                        return processPanel;
                    }
                }
                processPanel.setProcess(pipeline);
                processPanel.getProcessEditor().getModel().processWasChanged();

                // we have to focus again to set right pipeline in focus
                setMainProcessPanel(processPanel);
                return processPanel;
            }
        }
        // create a new tab: as either closed or no existing found
        ProcessPanel processPanel = processTabViewDockable.createPanel(pipeline, readOnly);
        setMainProcessPanel(processPanel);
        return processPanel;
    }

    /**
     * Move the focus to the given {@link ProcessPanel panel}
     * 
     * @param processPanel
     *            the previously {@link #openProcess(AbstractPipeline) opened} panel
     * @return {@code true} iff the panel still opened and the focus changed successfully
     */
    public boolean setFocusedProcessPanel(ProcessPanel processPanel) {
        return processTabViewDockable.select(processPanel);
    }

    public boolean closeProcessPanel(ProcessPanel processPanel, boolean force) {
        return processTabViewDockable.close(processPanel, force);
    }

    private boolean isProcessRunning() {
        return processExecutor != null && processExecutor.isAlive();
    }


    public ProcessPanel handleBrokenProcessXML(final Entry processEntry, final Exception e) {
        SwingTools.showSimpleErrorMessage("while_loading", e, processEntry.getPath().toString(), e.getMessage());
        perspectiveController.showPerspective(PerspectiveController.DESIGN);
        return openProcess(new AnalysisPipeline(), false);
    }

    public OperatorDocumentationDockable getOperatorDocViewer() {
        return operatorDocumentationBrowser;
    }

    public ProcessPanel getMainProcessPanel() {
        return mainProcessPanel;
    }

    public void registerDockable(final Dockable dockable) {
        dockingDesktop.registerDockable(dockable);
    }

    public DevelopmentExecutionContextPanel getDevelopmentExecutionContextPanel() {
        return developmentExecutionContextPanel;
    }

    public PipelineParametersPanel getPipelineParametersPanel() {
        return pipelineParametersPanel;
    }

    public VariableViewer getVariableViewer() {
        return variableViewer;
    }

    public RepositoryBrowserDockable getRepositoryBrowser() {
        return repositoryBrowser;
    }

    public Component getXMLEditor() {
        return processXMLPanel;
    }

    public MergeConflictResolvingView getMergeConflictResolvingPanel() {
        return mergeConflictResolvingView;
    }

    /**
     * This returns the file menu to change menu entries
     */
    public JMenu getFileMenu() {
        return fileMenu;
    }

    /**
     * This returns the settings menu to change menu entries
     *
     * @return the settings menu
     */
    public JMenu getSettingsMenu() {
        return settingsMenu;
    }

    /**
     * This returns the edit menu to change menu entries
     */
    public JMenu getEditMenu() {
        return editMenu;
    }

    /**
     * This returns the process menu to change menu entries
     */

    public JMenu getProcessMenu() {
        return processMenu;
    }

    /**
     * This returns the help menu to change menu entries
     */
    public JMenu getHelpMenu() {
        return helpMenu;
    }

    /**
     * This returns the extensions menu to change menu entries
     *
     * @since 7.0.0
     */
    public JMenu getExtensionsMenu() {
        return extensionsMenu;
    }

    public DockableMenu getDockableMenu() {
        return dockableMenu;
    }

    /**
     *
     * @return the toolbar containing e.g. process run buttons
     */
    public JToolBar getButtonToolbar() {
        return buttonToolbar;
    }

    /**
     * Checks the current process for potential problems. If a problem is deemed big enough (e.g. an
     * operator that requires input but is not connected), returns {@code true}. If no showstoppers
     * are found, returns {@code false}. This method also alerts the user about the problems so
     * after it returns, nothing else needs to be done.
     *
     * @return {@code true} if the process contains a problem which should prevent process
     *         execution; {@code false} otherwise
     */
    private boolean doesProcessContainShowstoppers(ProcessPanel processPanel) {
        // prevent two bubbles on top of each other
        processPanel.getOperatorWarningHandler().killWarningBubble();

        // if any operator has a mandatory parameter with no value and no default value. As it
        // cannot predict execution behavior (e.g. Branch operators), this may turn up problems
        // which would not occur during process execution
        AbstractPipeline process = processPanel.getProcess();
        Pair<Operator, ParameterType> missingParamPair = ProcessTools.getOperatorWithoutMandatoryParameter(process);
        if (missingParamPair != null) {
            // if there is already one of these, kill
            if (missingParameterBubble != null) {
                missingParameterBubble.killBubble(true);
            }

            missingParameterBubble = ProcessGUITools.displayPrecheckMissingMandatoryParameterWarning(missingParamPair.getFirst(), missingParamPair.getSecond());
            return true;
        }

        // if any port needs data but is not connected. As it cannot predict execution behavior
        // (e.g. Branch operators), this may turn up problems which would not occur during
        // process execution
        Pair<Port, ProcessSetupError> missingInputPort = ProcessTools.getPortWithoutMandatoryConnection(process);
        if (missingInputPort != null) {
            // if there is already one of these, kill
            if (missingInputBubble != null) {
                missingInputBubble.killBubble(true);
            }

            missingInputBubble = ProcessGUITools.displayPrecheckInputPortDisconnectedWarning(missingInputPort);
            return true;
        }

        // if there is already one of these, kill
        if (noResultConnectionBubble != null) {
            noResultConnectionBubble.killBubble(true);
        }

        // no showstopper
        return false;
    }

    /**
     * Resets the location of the MainFrame process without reloading it. Use only if the currently
     * displayed process is stored in repository, and got renamed or moved.
     *
     * @param repositoryLocation
     *            The new process location.
     */
    public static void resetProcessLocation(RepositoryPath repositoryLocation) {
        INSTANCE.getProcess().setPath(repositoryLocation);
        INSTANCE.setTitle(INSTANCE.getProcess());
        RecentLocationsManager.addToRecentFilesAsMostRecent(repositoryLocation);
    }

    /**
     * Checks whether the repository location belongs to the currently loaded process.
     *
     * @param location
     *            Repository location to check.
     * @return true if the process belonging to this entry is currently presented on the GUI, false
     *         otherwise.
     */
    public static boolean isMainFrameProcessLocation(RepositoryPath location) {
        if (INSTANCE != null && INSTANCE.getProcess() != null) {
            RepositoryPath currentProcessLocation = INSTANCE.getProcess().getPath();
            if (location.equals(currentProcessLocation)) {
                return true;
            }
        }
        return false;
    }

    private void fireProcessSelectionChangedEvent(ProcessPanel from, ProcessPanel to) {
        for (FocusProcessChangeEventListener listener : eventListenerList.getListeners(FocusProcessChangeEventListener.class)) {
            listener.onFocusedProcessChange(from, to);
        }
    }

    public MainToolBar getToolBar() {
        return toolBar;
    }

    public LogViewPanel getLogViewer() {
        return logViewer;
    }


}
