/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.dnd;


import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.StringReader;
import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.AnalysisPipeline;
import com.owc.singularity.engine.pipeline.SubPipeline;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeRepositoryLocation;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.tools.ProcessTools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotation;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Transfer handler that supports dragging and dropping operators and workflow annotations.
 *
 * @author Simon Fischer, Marius Helf, Michael Knopf, Marco Boeck
 *
 */
public abstract class ReceivingOperatorTransferHandler extends OperatorTransferHandler {

    private static final long serialVersionUID = 5355397064093668659L;

    private final List<DataFlavor> acceptableFlavors = new LinkedList<>();

    public ReceivingOperatorTransferHandler() {
        acceptableFlavors.add(TransferableOperator.LOCAL_TRANSFERRED_OPERATORS_FLAVOR);
        acceptableFlavors.add(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR);
        acceptableFlavors.add(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR);
        acceptableFlavors.add(TransferableAnnotation.LOCAL_OPERATOR_ANNOTATION_FLAVOR);
        acceptableFlavors.add(TransferableAnnotation.LOCAL_PROCESS_ANNOTATION_FLAVOR);
        acceptableFlavors.add(DataFlavor.javaFileListFlavor);
        acceptableFlavors.add(DataFlavor.stringFlavor);
    }

    /**
     * Drops the operator at the given location. The location may be null, indicating that this is a
     * paste.
     */
    protected abstract boolean dropNow(List<Operator> newOperators, Point loc);

    protected abstract boolean dropNow(WorkflowAnnotation anno, Point loc);

    protected abstract boolean dropNow(String processXML);

    protected abstract void markDropOver(Point dropPoint);

    protected abstract boolean isDropLocationOk(List<Operator> operator, Point loc);

    protected abstract void dropEnds();

    protected abstract AbstractPipeline getProcess();

    // Drop Support
    @Override
    public boolean canImport(TransferSupport ts) {
        for (DataFlavor flavor : acceptableFlavors) {
            if (ts.isDataFlavorSupported(flavor)) {
                if (ts.isDrop()) {
                    markDropOver(ts.getDropLocation().getDropPoint());
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean importData(TransferSupport ts) {
        if (!canImport(ts)) {
            return false;
        }
        DataFlavor acceptedFlavor = null;
        for (DataFlavor flavor : acceptableFlavors) {
            if (ts.isDataFlavorSupported(flavor)) {
                acceptedFlavor = flavor;
                break;
            }
        }
        if (acceptedFlavor == null) {
            dropEnds();
            return false; // cannot happen
        }

        Object transferData;
        Transferable transferable = ts.getTransferable();
        try {
            transferData = transferable.getTransferData(acceptedFlavor);
        } catch (Exception e1) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.error_while_accepting_drop", e1);
            dropEnds();
            return false;
        }
        List<Operator> newOperators;
        if (acceptedFlavor.equals(TransferableOperator.LOCAL_TRANSFERRED_OPERATORS_FLAVOR)) {
            // This is an operator
            Operator[] clonedOperators, originalOperators;
            if (transferData instanceof Operator[][]) {
                clonedOperators = ((Operator[][]) transferData)[0];
                originalOperators = ((Operator[][]) transferData)[1];
            } else {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.expected_operator", acceptedFlavor);
                dropEnds();
                return false;
            }

            AbstractPipeline newProcess = getProcess();
            for (int i = 0; i < originalOperators.length; i++) {
                Operator originalOperator = originalOperators[i];
                AbstractPipeline oldProcess = originalOperator.getPipeline();
                if (oldProcess != null && oldProcess != newProcess) {
                    // make sure relative paths are transferred
                    Parameters clonedParameters = clonedOperators[i].getParameters();
                    for (String definedKey : clonedParameters.getDefinedKeys()) {
                        if (originalOperator.getParameterType(definedKey) instanceof ParameterTypeRepositoryLocation) {
                            try {
                                RepositoryPath originalRepositoryPath = originalOperator.getParameterAsRepositoryPath(definedKey);
                                if (newProcess != null && newProcess.getPath() != null && oldProcess.getPath() != null
                                        && !originalRepositoryPath.isAbsolute()) {
                                    // make paths relative to the new process
                                    originalRepositoryPath = newProcess.getPath().getParent().relativize(oldProcess.getPath().resolve(originalRepositoryPath));
                                }
                                clonedParameters.setRawParameter(definedKey, originalRepositoryPath.toString());
                            } catch (Exception ignored) {
                            }
                        }
                    }
                }
            }
            newOperators = Arrays.asList(clonedOperators);
        } else if (acceptedFlavor.equals(DataFlavor.stringFlavor)) {
            if (!(transferData instanceof String)) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.expected_string", acceptedFlavor);
                dropEnds();
                return false;
            }
            try {
                AbstractPipeline process = XMLImporter.parse(((String) transferData).trim());
                newOperators = process.getRootOperator().getSubprocess(0).getOperators();
            } catch (Exception e) {
                try {
                    Document document = XMLTools.createDocumentBuilder().parse(new InputSource(new StringReader((String) transferData)));
                    NodeList opElements = document.getDocumentElement().getChildNodes();
                    Operator newOp = null;
                    for (int i = 0; i < opElements.getLength(); i++) {
                        Node child = opElements.item(i);
                        if (child instanceof Element) {
                            Element elem = (Element) child;
                            if ("operator".equals(elem.getTagName())) {
                                newOp = XMLImporter.newPipelineParser().parseOperator(elem, null, SingularityEngine.getVersion(), new LinkedList<>());
                                break;
                            }
                        }
                    }
                    if (newOp == null) {
                        LogService.getRoot()
                                .warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.parsing_operator_from_clipboard_error",
                                        transferData);
                        dropEnds();
                        return false;
                    }
                    newOperators = Collections.singletonList(newOp);
                } catch (SAXParseException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.processeditor.XMLEditor.failed_to_parse_process");
                    dropEnds();
                    return false;
                } catch (Exception e1) {
                    LogService.getRoot()
                            .warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.parsing_operator_from_clipboard_error_exception", e1,
                                    transferData, e1);
                    dropEnds();
                    return false;
                }
            }
        } else if (acceptedFlavor.equals(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR)) {
            if (!(transferData instanceof RepositoryPath)) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.expected_repositorylocation", acceptedFlavor);
                dropEnds();
                return false;
            }
            RepositoryPath repositoryLocation = (RepositoryPath) transferData;
            Operator newOp = createOperator(repositoryLocation);
            if (newOp == null) {
                dropEnds();
                return false;
            }
            newOperators = Collections.singletonList(newOp);
        } else if (acceptedFlavor.equals(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR)) {
            Stream<RepositoryPath> repoLocations;
            if (transferData instanceof TransferableRepositoryEntries) {
                repoLocations = ((TransferableRepositoryEntries) transferData).getLocations().stream();
            } else if (transferData instanceof RepositoryPath[]) {
                repoLocations = Arrays.stream((RepositoryPath[]) transferData);
            } else {
                LogService.getRoot()
                        .warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.expected_repositorylocationlist", acceptedFlavor);
                dropEnds();
                return false;
            }
            newOperators = repoLocations.map(this::createOperator).filter(Objects::nonNull).collect(Collectors.toList());
            if (newOperators.isEmpty()) {
                dropEnds();
                return false;
            }
        } else if (acceptedFlavor.equals(TransferableAnnotation.LOCAL_OPERATOR_ANNOTATION_FLAVOR)
                || acceptedFlavor.equals(TransferableAnnotation.LOCAL_PROCESS_ANNOTATION_FLAVOR)) {
            newOperators = new LinkedList<>();
        } else if (acceptedFlavor.equals(DataFlavor.javaFileListFlavor)) {
            Stream<RepositoryPath> repoLocations;
            if (transferData instanceof List) {
                repoLocations = ((List) transferData).stream().map(entry -> {
                    if (entry instanceof File)
                        return RepositoryPath.of(((File) entry).getAbsolutePath());
                    return null;
                });
            } else {
                LogService.getRoot()
                        .warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.expected_repositorylocationlist", acceptedFlavor);
                dropEnds();
                return false;
            }
            newOperators = repoLocations.map(this::createOperator).filter(Objects::nonNull).collect(Collectors.toList());
            if (newOperators.isEmpty()) {
                dropEnds();
                return false;
            }
        } else {
            // cannot happen
            dropEnds();
            return false;
        }

        if (ts.isDrop()) {
            // drop
            Point loc = ts.getDropLocation().getDropPoint();
            boolean dropLocationOk = !ts.isDrop() || isDropLocationOk(newOperators, loc);
            if (!dropLocationOk) {
                dropEnds();
                return false;
            }
            if (ts.getDropAction() == MOVE) {
                for (Operator operator : newOperators) {
                    operator.removeAndKeepConnections(newOperators);
                }
            }

            boolean result = false;
            try {
                result = dropNow(newOperators, ts.isDrop() ? loc : null);
            } catch (RuntimeException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.error_in_drop", e, e);
                SwingTools.showVerySimpleErrorMessage("error_in_paste", e.getMessage(), e.getMessage());
            }
            dropEnds();
            return result;
        } else {
            // paste
            BooleanSupplier dropNow;
            if (acceptedFlavor.equals(DataFlavor.stringFlavor)) {
                dropNow = () -> dropNow(String.valueOf(transferData));
            } else if (acceptedFlavor.equals(TransferableAnnotation.LOCAL_PROCESS_ANNOTATION_FLAVOR)
                    || acceptedFlavor.equals(TransferableAnnotation.LOCAL_OPERATOR_ANNOTATION_FLAVOR)) {
                dropNow = () -> dropNow((WorkflowAnnotation) transferData, null);
            } else {
                List<Operator> droppedOperators = newOperators;

                // find all operators that will be renamed and notify the cloned operators about
                // that
                Collection<String> allOperatorNames = getProcess().getAllOperatorNames();
                List<String> oldNames = findAllNames(newOperators);
                Map<String, String> nameMap = ProcessTools.getNewNames(allOperatorNames, oldNames);
                if (!nameMap.isEmpty()) {
                    droppedOperators.forEach(op -> nameMap.forEach(op::notifyRenaming));
                }

                dropNow = () -> dropNow(droppedOperators, null);
            }
            boolean result = false;
            try {
                result = dropNow.getAsBoolean();
            } catch (RuntimeException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.error_in_paste", e, e);
                SwingTools.showVerySimpleErrorMessage("error_in_paste", e.getMessage(), e.getMessage());
            }
            dropEnds();
            return result;
        }
    }

    /**
     * Finds the names of all operators, including inner operators
     *
     * @since 9.3
     */
    private List<String> findAllNames(List<Operator> operators) {
        return operators.stream()
                .flatMap(op -> op instanceof OperatorChain ? ((OperatorChain) op).getAllInnerOperatorsAndMe().stream().map(Operator::getName)
                        : Stream.of(op.getName()))
                .collect(Collectors.toList());
    }

    /**
     * Creates the operator to import from the given repository location.
     *
     * @param repositoryLocation
     *            the location which should be imported
     * @return the operator or {@code null}
     */
    private Operator createOperator(RepositoryPath repositoryLocation) {
        try {
            Entry entry = Entries.getEntry(repositoryLocation);
            RepositoryPath processPath = getProcess().getPath();
            String resolvedLocation = (processPath != null) ? processPath.getParent().relativize(repositoryLocation).toString() : repositoryLocation.toString();
            if (resolvedLocation.endsWith(".csv")) {
                // create Retrieve operator
                try {
                    Operator readCSVOperator = OperatorService.createOperator("core:read_csv_file");
                    readCSVOperator.setParameter("csv_file", resolvedLocation);
                    readCSVOperator.rename("Read CSV " + repositoryLocation.getFileName().toString());
                    return readCSVOperator;
                } catch (OperatorCreationException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.creating_repositorysource_error", e1, e1);
                    return null;
                }
            } else if (resolvedLocation.endsWith(".xlsx") || resolvedLocation.endsWith(".xls")) {
                // create Retrieve operator
                try {
                    Operator readExcelOperator = OperatorService.createOperator("core:read_excel");
                    readExcelOperator.setParameter("excel_file", resolvedLocation);
                    readExcelOperator.rename("Read Excel " + repositoryLocation.getFileName().toString());
                    return readExcelOperator;
                } catch (OperatorCreationException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.creating_repositorysource_error", e1, e1);
                    return null;
                }
            } else if (entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
                // create Execute AbstractPipeline operator
                try {
                    AbstractPipeline pipeline = entry.loadData(AbstractPipeline.class, ModuleService.getMajorClassLoader());
                    pipeline.setPath(entry.getPath());
                    Operator embedder;
                    if (pipeline.isEffectivelyInstanceOf(AnalysisPipeline.class)) {
                        embedder = OperatorService.createOperator("core:execute_analysis_pipeline");
                        embedder.setParameter("pipeline_location", resolvedLocation);
                        embedder.rename("Execute " + repositoryLocation.getFileName().toString());
                        return embedder;
                    } else if (pipeline.isEffectivelyInstanceOf(SubPipeline.class)) {
                        embedder = OperatorService.createOperator("core:include_sub_pipeline");
                        embedder.setParameter("pipeline_location", resolvedLocation);
                        embedder.rename("Execute " + repositoryLocation.getFileName().toString());
                        return embedder;
                    }
                    return null;
                } catch (OperatorCreationException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.creating_repositorysource_error", e1, e1);
                    return null;
                }
            } else if (entry.isInstanceOf(IOObject.class, ModuleService.getMajorClassLoader())) {
                // create Retrieve operator
                try {
                    Operator retrieveOperator = OperatorService.createOperator("core:retrieve");
                    retrieveOperator.setParameter("repository_entry", resolvedLocation);
                    retrieveOperator.rename("Retrieve " + repositoryLocation.getFileName().toString());
                    return retrieveOperator;
                } catch (OperatorCreationException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.dnd.ReceivingOperatorTransferHandler.creating_repositorysource_error", e1, e1);
                    return null;
                }
            }
        } catch (Exception e) {
            // no valid entry
        }
        return null;
    }
}
