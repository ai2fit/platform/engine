package com.owc.singularity.studio.gui.vcs.model;

import java.io.IOException;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.EntryVersion;

public abstract class AbstractMountTreeDiffFileTreeNode extends AbstractMountTreeDiffTreeNode {

    private Class<?> type;

    protected AbstractMountTreeDiffFileTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }

    @Override
    public Class<?> getType() {
        if (type == null) {
            try {
                type = Entries.getEntry(getRepositoryPath()).getContentClass(ModuleService.getMajorClassLoader());
            } catch (IOException | ClassNotFoundException e) {
                type = Object.class;
            }
        }
        return type;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public boolean getAllowsChildren() {
        return false;
    }
}
