package com.owc.singularity.studio.gui.execution;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.*;
import com.owc.singularity.engine.concurrency.tools.ConcurrentOperatorExecution.OperatorBackgroundTask;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.AbstractIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.actions.ToggleAction;
import com.owc.singularity.studio.gui.dnd.TransferableOperator;
import com.owc.singularity.studio.gui.execution.action.*;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.components.DropDownButton;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class ConcurrentExecutionPipelinesView extends JPanel implements Dockable {

    private ConcurrentExecution selectedPipelineExecution = null;
    private JSplitPane infoPanel;
    private JList<ConcurrentExecution> backgroundExecutionsView = new JList<ConcurrentExecution>();
    private DefaultMutableTreeNode root = new DefaultMutableTreeNode("SubTasks");
    JPanel executionInfoPanel;
    JTree subtaskPanel = new JTree(root);
    JList<ProcessExecutionStackEntry> stackPanel = new JList<ProcessExecutionStackEntry>();
    JList<DataTable> logPanel = new JList<DataTable>();
    JList<Object> resultPanel = new JList<Object>();

    /*
     * toggle action to show the background processes
     */
    private transient final ToggleAction SHOW_RUNNING_EXECUTIONS_ACTION = new ToggleAction(true, "toolkit.show_running_executions") {

        private static final long serialVersionUID = 1L;

        {
            setSelected(true);
            actionToggled(null);
        }

        @Override
        public void actionToggled(ActionEvent e) {
            ConcurrentExecutionsFilterControl.SHOW_RUNNING_EXECUTIONS = isSelected();
            revalidateFilter();

        }

    };
    /*
     * toggle action to show currently running processes
     */
    private transient final ToggleAction SHOW_PENDING_EXECUTIONS_ACTION = new ToggleAction(true, "toolkit.show_pending_executions") {

        private static final long serialVersionUID = 1L;

        {
            setSelected(true);
            actionToggled(null);
        }

        @Override
        public void actionToggled(ActionEvent e) {
            ConcurrentExecutionsFilterControl.SHOW_PENDING_EXECUTIONS = isSelected();
            revalidateFilter();

        }

    };

    /*
     * toggle action to show finished processes
     */
    private transient final ToggleAction SHOW_FINISHED_EXECUTIONS_ACTION = new ToggleAction(true, "toolkit.show_finished_executions") {

        private static final long serialVersionUID = 1L;

        {
            setSelected(true);
            actionToggled(null);
        }

        @Override
        public void actionToggled(ActionEvent e) {
            ConcurrentExecutionsFilterControl.SHOW_FINISHED_EXECUTIONS = isSelected();
            revalidateFilter();
        }
    };

    /*
     * button action to select a process that is supposed to be imported
     */
    private transient final Action CHOSE_PROCESS_ACTION = new ResourceAction(true, "toolkit.import_process") {

        /**
         *
         **/
        private static final long serialVersionUID = 1L;

        {
            setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent arg0) {

            try {
                OpenExecutionProcessAction.open();
            } catch (final UserError e) {
                showUserError(e);

            }

        }

    };
    /*
     * Control Button to start the selected AbstractPipeline
     */
    private transient final Action REMOVE_PROCESS_ACTION = new ToggleAction(true, "toolkit.remove_process") {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            setEnabled(false);
        }

        @Override
        public void actionToggled(ActionEvent e) {
            {
                setEnabled(false);
            }

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    if (selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
                        ConcurrentPipelineExecution process = (ConcurrentPipelineExecution) selectedPipelineExecution;
                        ConcurrentExecutionServiceProvider.getService().removePipelineExecution(process);
                    }
                }
            });

        }

    };

    /*
     * Control Button to stop the current AbstractPipeline
     */
    private transient final Action STOP_PROCESS_ACTION = new ToggleAction(true, "toolkit.stop_process") {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            setEnabled(false);
        }

        @Override
        public void actionToggled(ActionEvent e) {
            if (selectedPipelineExecution != null && selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
                ConcurrentExecutionServiceProvider.getService().stopPipelineExecution((ConcurrentPipelineExecution) selectedPipelineExecution);
                setEnabled(false);

            }

        }

    };

    /*
     * ============================THIS IS WHERE THE GUI CLASS ACTUALLY STARTS
     * ==============================
     */
    private static final long serialVersionUID = 1L;

    private ResourceDockKey resourceDockKey = new ResourceDockKey("toolkit.background_processes");

    private ScheduledExecutorService updateExecutor = Executors.newSingleThreadScheduledExecutor();

    public ConcurrentExecutionPipelinesView() {

        setLayout(new BorderLayout());
        backgroundExecutionsView.setModel(ConcurrentExecutionsFilterControl.filteredList);
        // ListCellRenderer
        ConcurrentExecutionListCellRenderer renderer = new ConcurrentExecutionListCellRenderer();
        ConcurrentExecutionListCellRenderer rendererInfo = new ConcurrentExecutionListCellRenderer();
        createPopUpForJList(backgroundExecutionsView);
        // Update selectedProcess if selection changes
        backgroundExecutionsView.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                setSelectedPipelineExecution(backgroundExecutionsView.getSelectedValue());

            }
        });
        backgroundExecutionsView.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        backgroundExecutionsView.setCellRenderer(renderer);
        backgroundExecutionsView.setOpaque(false);
        // tabs can share the renderer with the backgroundExecutionView
        resultPanel.setCellRenderer(rendererInfo);
        logPanel.setCellRenderer(rendererInfo);
        stackPanel.setCellRenderer(rendererInfo);

        /*
         * Adding a popUp for the tabPanels. Since the tabs only contain results, we can limit the
         * popup options to result and concurrency level.
         */
        // resultTab
        resultPanel.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                super.mouseReleased(e);
                if (e.getClickCount() == 2) {
                    Object value = resultPanel.getSelectedValue();
                    if (value instanceof IOObject || value instanceof DataTable) {
                        Action openResultAction = new OpenExecutionResultAction(value);
                        openResultAction.actionPerformed(null);
                    }

                }
                if (e.isPopupTrigger()) {
                    resultPanel.setSelectedIndex(resultPanel.locationToIndex(e.getPoint()));
                    Object value = resultPanel.getSelectedValue();
                    final JPopupMenu menu = new JPopupMenu();
                    Action openResultAction = new OpenExecutionResultAction(value);
                    menu.add(openResultAction);

                    menu.addSeparator();

                    // entries for generic things
                    SetParallelityAction setParallelityAction = new SetParallelityAction();
                    menu.add(setParallelityAction);

                    // enabling actions that are relevant to current position

                    openResultAction.setEnabled(value instanceof IOObject || value instanceof DataTable);

                    // show menu
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            menu.show(resultPanel, e.getX(), e.getY());

                        }
                    });
                }
            }
        });
        // log tab
        logPanel.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                super.mouseReleased(e);
                if (e.getClickCount() == 2) {
                    Object value = logPanel.getSelectedValue();
                    if (value instanceof AbstractIOObject || value instanceof DataTable) {
                        Action openResultAction = new OpenExecutionResultAction(value);
                        openResultAction.actionPerformed(null);
                    }

                }
                if (e.isPopupTrigger()) {
                    logPanel.setSelectedIndex(logPanel.locationToIndex(e.getPoint()));
                    Object value = logPanel.getSelectedValue();
                    final JPopupMenu menu = new JPopupMenu();
                    Action openResultAction = new OpenExecutionResultAction(value);
                    menu.add(openResultAction);

                    menu.addSeparator();

                    // entries for generic things
                    SetParallelityAction setParallelityAction = new SetParallelityAction();
                    menu.add(setParallelityAction);

                    // enabling actions that are relevant to current position

                    openResultAction.setEnabled(value instanceof AbstractIOObject || value instanceof DataTable);

                    // show menu
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            menu.show(logPanel, e.getX(), e.getY());

                        }
                    });

                }
            }
        });// End Tab popups

        // Setting up subtasks as a JTree
        subtaskPanel.setCellRenderer(new ConcurrentExecutionTreeCellRenderer());
        subtaskPanel.setRootVisible(false);
        subtaskPanel.setShowsRootHandles(true);

        // adding this view to the backgroundExecutionService listeners
        ConcurrentExecutionServiceProvider.getService().addListener(new ConcurrencyExecutionServiceListener() {

            @Override
            public void pipelineRemoved(ConcurrentExecution execution) {
                removeBackgroundPipelineView(execution);

            }

            @Override
            public void pipelineAdded(ConcurrentExecution execution) {
                addBackgroundPipelineView(execution);

            }
        });

        /*
         * setup process panel. The processPanelContainer lists all the currently running background
         * executions
         */
        ExtendedJScrollPane processPanelContainer = new ExtendedJScrollPane();
        processPanelContainer.setViewportView(backgroundExecutionsView);
        /*
         * setup drowdownButton for filter Options
         */
        DropDownButton filterMenu = new DropDownButton(new ResourceActionAdapter(true, "toolkit.filter_menu")) {

            private static final long serialVersionUID = 1L;

            @Override
            protected JPopupMenu getPopupMenu() {
                JPopupMenu popupMenu = new JPopupMenu();

                popupMenu.add(SHOW_PENDING_EXECUTIONS_ACTION.createToggleButton());
                popupMenu.add(SHOW_RUNNING_EXECUTIONS_ACTION.createToggleButton());
                popupMenu.add(SHOW_FINISHED_EXECUTIONS_ACTION.createToggleButton());

                return popupMenu;
            }
        };

        /*
         * setup toolbar that will contain buttons to perform filter operations on the background
         * processes
         */
        JToolBar toolBar = new ExtendedJToolBar();
        // toolBar.add(SHOW_PENDING_EXECUTIONS_ACTION.createToggleButton());
        // toolBar.add(SHOW_RUNNING_EXECUTIONS_ACTION.createToggleButton());
        // toolBar.add(SHOW_FINISHED_EXECUTIONS_ACTION.createToggleButton());

        filterMenu.setUsePopupActionOnMainButton();
        toolBar.add(CHOSE_PROCESS_ACTION);
        toolBar.add(STOP_PROCESS_ACTION);
        toolBar.add(REMOVE_PROCESS_ACTION);
        toolBar.addSeparator();
        toolBar.add(new RemoveFinishedExecutionsAction());
        filterMenu.addToToolBar(toolBar);

        // setup left container. The left container contains the current processes and allows filter
        // options on them through buttons on the top
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());
        leftPanel.add(processPanelContainer, BorderLayout.CENTER);
        add(toolBar, BorderLayout.NORTH);

        // Execution Panel
        executionInfoPanel = new JPanel();
        executionInfoPanel.setOpaque(false);

        executionInfoPanel.setLayout(new BoxLayout(executionInfoPanel, BoxLayout.Y_AXIS));
        executionInfoPanel.setAlignmentX(LEFT_ALIGNMENT);
        ExtendedJScrollPane scrollableView = new ExtendedJScrollPane();
        scrollableView.setViewportView(executionInfoPanel);
        executionInfoPanel.setBorder(null);
        infoPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollableView, logPanel);
        infoPanel.setResizeWeight(0.8F);
        infoPanel.setBorder(null);

        // subtasks/logs and resultpanel have a white background and and are opaque
        subtaskPanel.setOpaque(false);
        subtaskPanel.setAlignmentX(LEFT_ALIGNMENT);
        subtaskPanel.setBorder(null);
        logPanel.setOpaque(false);
        logPanel.setAlignmentX(LEFT_ALIGNMENT);
        logPanel.setBorder(null);
        resultPanel.setOpaque(false);
        resultPanel.setAlignmentX(LEFT_ALIGNMENT);
        resultPanel.setBorder(null);
        stackPanel.setOpaque(false);
        stackPanel.setAlignmentX(LEFT_ALIGNMENT);
        stackPanel.setBorder(null);

        // setup right side container
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());
        rightPanel.add(infoPanel, BorderLayout.CENTER);

        // creating the resultview
        JSplitPane resultPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);
        resultPanel.setResizeWeight(0.5F);

        resultPanel.setBorder(null);

        // add to this component

        add(resultPanel, BorderLayout.CENTER);

        // register as drop target
        new DropTarget(this, new DropTargetAdapter() {

            @Override
            public void drop(DropTargetDropEvent dtde) {
                if (dtde.isDataFlavorSupported(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR)) {
                    try {
                        RepositoryPath repositoryLocation = (RepositoryPath) dtde.getTransferable()
                                .getTransferData(TransferableOperator.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR);
                        try {
                            AbstractPipeline process = Entries.loadData(repositoryLocation, AbstractPipeline.class, ModuleService.getMajorClassLoader());
                            ConcurrentExecutionServiceProvider.getService().executePipelineConcurrently(process);
                        } catch (final UserError e) {
                            showUserError(e);
                        } catch (IOException e) {
                        }

                    } catch (UnsupportedFlavorException | ClassCastException e) {
                    } catch (IOException e) {
                    }
                }
            }
        });

        // create an update task
        updateExecutor.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {

                        @Override
                        public void run() {
                            backgroundExecutionsView.updateUI();
                            updateBackgroundPipelineInformation();
                        }
                    });
                } catch (InvocationTargetException e) {
                    // can't happen: lines above do not throw exception
                } catch (InterruptedException e) {
                    // we can ignore that
                }
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }

    /**
     * Needed from singularity to generate the gui.
     */
    @Override
    public Component getComponent() {
        return this;
    }

    /**
     * necessary method to make this component dockable
     */
    @Override
    public DockKey getDockKey() {
        return resourceDockKey;
    }

    /**
     * This method adds the provided execution to the ConcurrentExecution list and updates the
     * respective view.
     *
     * @param targetPanel
     * @param process
     */
    public void addBackgroundPipelineView(final ConcurrentExecution execution) {

        if (isPipeline(execution)) {
            ConcurrentExecutionsFilterControl.addFilteredElement(execution);

        }
    }

    /**
     * This Method removes a ProcessView from the processPanel.
     *
     * @param execution
     */
    public void removeBackgroundPipelineView(final ConcurrentExecution execution) {
        // If the Execution does not exist, we can not remove it from the view, since we never have
        // added it in the first place.

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (!(ConcurrentExecutionsFilterControl.filteredList.removeElement(execution))) {
                    return;
                } else {
                    if (selectedPipelineExecution == execution)
                        setSelectedPipelineExecution(null);
                }
                revalidateFilter();
            }
        });

    }

    /**
     * This Method writes informations about the selectedProcess into the respective JScrollPane.
     * The Type SUB_TASK_TAB adds the subtasks of the selected process into the respective tab. The
     * Type LOG_TAB adds the log values of the selected process into the respective tab. The Type
     * RESULT_TAB adds the results of the selected process into the respective tab.
     */
    private void updateBackgroundPipelineInformation() {

        executionInfoPanel.removeAll();
        // updating logs in any case
        updateLogs();
        if (selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
            ConcurrentPipelineExecution process = (ConcurrentPipelineExecution) selectedPipelineExecution;
            ConcurrentPipelineExecutionState state = process.getBackgroundExecutionState();
            if (!state.isEnded() && !state.isStopped()) {
                STOP_PROCESS_ACTION.setEnabled(true);
                REMOVE_PROCESS_ACTION.setEnabled(false);
                updateStackView();
                executionInfoPanel.add(stackPanel);
            } else {
                // We can only stop a process if it is running
                STOP_PROCESS_ACTION.setEnabled(false);
                REMOVE_PROCESS_ACTION.setEnabled(true);
                updateResults();
                executionInfoPanel.add(resultPanel);
            }
        } else if (selectedPipelineExecution instanceof ConcurrentOperatorExecution) {
            // OperatorConcurrentExecutions only have subtasks, so update them and put them
            // into the result view
            updateSubTasks();
            executionInfoPanel.add(subtaskPanel);
            // OperatorConcurrentExecutions will be automaticly stopped/removed
            REMOVE_PROCESS_ACTION.setEnabled(false);
            STOP_PROCESS_ACTION.setEnabled(false);
        }

        infoPanel.revalidate();
        infoPanel.repaint();
    }

    /**
     * ProcessBackgroundExectuin and OperatorConcurrentExecutions are handled as processes.
     * Everything else is not a process according to this Method.
     *
     * @param value
     * @return
     */
    private boolean isPipeline(Object value) {
        if (value instanceof ConcurrentPipelineExecution)
            return true;
        else if (value instanceof ConcurrentOperatorExecution)
            return true;
        else
            return false;
    }

    /**
     * This Method sets the current selectedProcess as the provided execution
     *
     * @param execution
     */
    public void setSelectedPipelineExecution(ConcurrentExecution execution) {
        selectedPipelineExecution = execution;
        selectedPipelineExecutionChanged();
    }

    /**
     * Every time a new background process is selected, we need to check if the sub_task_tab is
     * supposed to be shown or not.
     */
    private void selectedPipelineExecutionChanged() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                updateBackgroundPipelineInformation();

            }
        });
    }

    /**
     * This method updates the logs tab with the log results of the selected process in a safe way.
     */
    private void updateLogs() {
        boolean clearPanel = false;
        if (selectedPipelineExecution == null) {
            clearPanel = true;
        }

        if (!clearPanel && selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
            ConcurrentPipelineExecution processExecution = (ConcurrentPipelineExecution) selectedPipelineExecution;
            final ConcurrentPipelineExecutionState state = processExecution.getBackgroundExecutionState();
            if (state != null) {
                final List<DataTable> data = state.getProcessLogs();
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        if (data != null) {
                            logPanel.setListData(data.toArray(new DataTable[0]));
                        } else {
                            logPanel.setListData(new DataTable[0]);
                        }

                    }

                });
                return;
            } else {
                clearPanel = true;
            }

        } else {
            clearPanel = true;
        }

        if (clearPanel) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    logPanel.setListData(new DataTable[0]);
                }

            });
        }
    }

    /**
     * This method lists the results of the selectedProcess into the resultTab. Only
     * proessConcurrentExecutions can deliver results.
     *
     * @param tasks
     */
    private void updateResults() {

        // no result possible if no process is selected, hence we only need to clear the result
        // panel
        if (selectedPipelineExecution == null) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    resultPanel.setListData(new Object[0]);

                }
            });
            return;
        }
        // The selected pro
        ConcurrentPipelineExecution processExecution = null;
        ConcurrentOperatorExecution operatorExecution = null;
        if (selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
            processExecution = (ConcurrentPipelineExecution) selectedPipelineExecution;
        } else if (selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
            operatorExecution = (ConcurrentOperatorExecution) selectedPipelineExecution;
        }
        if (operatorExecution == null && processExecution == null) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    resultPanel.setListData(new Object[0]);
                }
            });
            return;
        }
        // generate results if processConcurrentExecution is selected
        if (processExecution != null) {
            // Extracting the results
            final List<IOObject> results = processExecution.getBackgroundExecutionState().getResults();
            final Throwable e = processExecution.getBackgroundExecutionState().getException();
            if (results == null) {
                if (e != null) {
                    SwingUtilities.invokeLater(new Runnable() {

                        // Error found, hence put it into the result table
                        @Override
                        public void run() {
                            resultPanel.setListData(new Object[] { e });
                        }
                    });
                } else {
                    SwingUtilities.invokeLater(new Runnable() {

                        // No results and no error, hence clear the panel
                        @Override
                        public void run() {
                            resultPanel.setListData(new Object[0]);
                        }
                    });
                }
                return;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    resultPanel.setListData(results.toArray(new Object[0]));
                }
            });
        }
    }

    /**
     * This Method shows a JTree with the subtasks of the selected process
     */
    private void updateSubTasks() {
        root.removeAllChildren();

        if (selectedPipelineExecution != null && selectedPipelineExecution instanceof ConcurrentOperatorExecution) {
            ConcurrentOperatorExecution operatorExecution = (ConcurrentOperatorExecution) selectedPipelineExecution;
            List<OperatorBackgroundTask> tasks = operatorExecution.getTasks();
            if (tasks != null) {
                for (OperatorBackgroundTask task : tasks) {
                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(task);

                    for (ProcessExecutionStackEntry stackEntry : task.getState().getStack())
                        newNode.add(new DefaultMutableTreeNode(stackEntry));
                    root.add(newNode);
                    subtaskPanel.expandPath(new TreePath(newNode.getPath()));

                }

            }

        }
        // subtaskPanel.setModel(new DefaultTreeModel(root));
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                subtaskPanel.updateUI();
            }
        });

    }

    private void updateStackView() {
        if (selectedPipelineExecution != null && selectedPipelineExecution instanceof ConcurrentPipelineExecution) {
            ConcurrentPipelineExecution process = (ConcurrentPipelineExecution) selectedPipelineExecution;
            ConcurrentPipelineExecutionState state = process.getBackgroundExecutionState();
            if (state != null) {
                final List<ProcessExecutionStackEntry> stack = state.getStack();
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        if (stack != null) {
                            stackPanel.setListData(stack.toArray(new ProcessExecutionStackEntry[0]));
                        }

                    }
                });

            }
        }
    }

    /**
     * This method creates a PopUpview for a Jlist<ConcurrentExecution>. Popups can only be created
     * for contents of the class processConcurrentExecution and operatorConcurrentExecution. The
     * Popup allows the User to stop- /clear- /open- Processes and set the concurrency level. For
     * OperatorConcurrentExecutions you can only set the concurrency level.
     *
     * @param view
     */
    private void createPopUpForJList(JList<ConcurrentExecution> view) {
        view.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(final MouseEvent e) {
                super.mouseReleased(e);
                if (e.isPopupTrigger()) {
                    backgroundExecutionsView.setSelectedIndex(backgroundExecutionsView.locationToIndex(e.getPoint()));
                    ConcurrentExecution value = backgroundExecutionsView.getSelectedValue();
                    if (value instanceof ConcurrentPipelineExecution || value instanceof ConcurrentOperatorExecution)
                        setSelectedPipelineExecution(value);

                    // getting information from click

                    ConcurrentPipelineExecution execution = null;
                    ConcurrentPipelineExecutionState executionState = null;
                    if (value instanceof ConcurrentPipelineExecution) {

                        execution = (ConcurrentPipelineExecution) value;
                        executionState = execution.getBackgroundExecutionState();

                    }

                    // creating menu

                    final JPopupMenu menu = new JPopupMenu();

                    // add generic entries about the process
                    Action stopAction = new StopExecutionAction(execution);
                    menu.add(stopAction);
                    Action removeAction = new RemoveExecutionAction(execution);
                    menu.add(removeAction);
                    Action openProcessAction = new OpenExecutionProcessAction(execution);
                    menu.add(openProcessAction);

                    // menu.addSeparator();

                    // // entries for results
                    // Action openResultAction = new OpenExecutionResultAction(value);
                    // menu.add(openResultAction);

                    menu.addSeparator();

                    // entries for generic things
                    SetParallelityAction setParallelityAction = new SetParallelityAction();
                    menu.add(setParallelityAction);

                    // enabling actions that are relevant to current position
                    stopAction.setEnabled(execution != null && executionState.isStarted() && !(executionState.isEnded() || executionState.isStopped()));
                    removeAction.setEnabled(execution != null && (executionState.isEnded() || executionState.isStopped()));
                    openProcessAction.setEnabled(execution != null);
                    // openResultAction.setEnabled(value instanceof AbstractIOObject || value
                    // instanceof
                    // DataTable);

                    // show menu
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            menu.show(backgroundExecutionsView, e.getX(), e.getY());

                        }
                    });

                }
            }
        });
    }

    private void revalidateFilter() {
        ConcurrentExecutionsFilterControl.applyFilter();

    }

    protected void showUserError(final UserError e) {
        ConfirmDialog confirmDialog = new ConfirmDialog(null, "toolkit.background_execution_view.user_error", ConfirmDialog.OK_OPTION, false) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            protected String getInfoText() {
                return e.getMessage();
            };
        };
        confirmDialog.setVisible(true);
    }

}
