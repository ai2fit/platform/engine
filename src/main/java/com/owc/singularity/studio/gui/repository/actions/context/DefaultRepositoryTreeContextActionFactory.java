package com.owc.singularity.studio.gui.repository.actions.context;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.function.Predicate;

import javax.swing.*;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.actions.conditions.RepositoryActionCondition;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;

public class DefaultRepositoryTreeContextActionFactory implements RepositoryTreeContextActionFactory {

    private static DefaultRepositoryTreeContextActionFactory INSTANCE;

    private record RepositoryActionRecord(Class<? extends AbstractRepositoryContextAction<?>> actionClass, Predicate<List<RepositoryTreeNode>> condition,
            boolean hasSeparatorBefore, boolean hasSeparatorAfter, boolean supportsMultiSelection) {

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            RepositoryActionRecord that = (RepositoryActionRecord) o;
            return Objects.equals(actionClass, that.actionClass) && Objects.equals(condition, that.condition);
        }

        @Override
        public int hashCode() {
            return Objects.hash(actionClass, condition);
        }
    }

    public static DefaultRepositoryTreeContextActionFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DefaultRepositoryTreeContextActionFactory();
        }
        return INSTANCE;
    }

    private final Set<RepositoryActionRecord> registeredActions = new LinkedHashSet<>();

    /**
     * This method returns a list of actions shown in the context menu if the given
     * {@link RepositoryActionCondition} is true. Contains {@code null} elements for each separator.
     * This method is called by each {@link RepositoryTree} instance during construction time and
     * creates instances via reflection of all registered acionts. See
     * {@link #addRepositoryAction(Class, Predicate, boolean, boolean, boolean)} to add actions.
     */
    @Override
    public List<Action> createActionsFor(RepositoryTree tree, List<RepositoryTreeNode> selectedNodes) {
        List<Action> listOfActions = new ArrayList<>();
        boolean lastWasSeparator = true;

        for (RepositoryActionRecord actionRecord : registeredActions) {
            try {
                if (actionRecord.condition().test(selectedNodes)) {
                    if (!lastWasSeparator && actionRecord.hasSeparatorBefore()) {
                        // add null element which means a separator will be added in the menu
                        listOfActions.add(null);
                    }
                    Constructor<? extends AbstractRepositoryContextAction<?>> constructor = actionRecord.actionClass().getConstructor(RepositoryTree.class);
                    AbstractRepositoryContextAction<?> createdAction = constructor.newInstance(tree);
                    if (!createdAction.enable()) {
                        // action is disabled, skip it
                        continue;
                    }
                    listOfActions.add(createdAction);
                    if (actionRecord.hasSeparatorAfter()) {
                        listOfActions.add(null);
                    }
                    lastWasSeparator = actionRecord.hasSeparatorAfter();
                }
            } catch (Exception e) {
                LogService.getRoot()
                        .error("com.owc.singularity.studio.gui.repository.RepositoryTree.creating_repository_action_error", actionRecord.actionClass());
            }
        }
        return listOfActions;
    }

    /**
     * Adds the given {@link AbstractRepositoryContextAction} extending class to the popup menu
     * actions at the given index.
     * <p>
     * The class <b>MUST</b> have one public constructor taking only a RepositoryTree. </br>
     * Example: public MyNewRepoAction(RepositoryTree tree) { ... } </br>
     * Otherwise creating the action via reflection will fail.
     *
     * @param actionClass
     *            the class extending {@link AbstractRepositoryContextAction}
     * @param condition
     *            the {@link RepositoryActionCondition} which determines on which selected entries
     *            the action will be visible.
     * @param hasSeparatorBefore
     *            if true, a separator will be added before the action
     * @param hasSeparatorAfter
     *            if true, a separator will be added after the action
     * @param multiSelection
     *            if true, the action will be enabled when the user selects multiple entries
     */
    public void addRepositoryAction(Class<? extends AbstractRepositoryContextAction<?>> actionClass, Predicate<List<RepositoryTreeNode>> condition,
            boolean hasSeparatorBefore, boolean hasSeparatorAfter, boolean multiSelection) {
        if (actionClass == null || condition == null) {
            throw new IllegalArgumentException("actionClass and condition must not be null!");
        }

        RepositoryActionRecord newEntry = new RepositoryActionRecord(actionClass, condition, hasSeparatorBefore, hasSeparatorAfter, multiSelection);
        synchronized (registeredActions) {
            registeredActions.add(newEntry);
        }
    }

    /**
     * Removes the given action from the popup menu actions.
     *
     * @param actionClass
     *            the class of the {@link AbstractRepositoryContextAction} to remove
     */
    public void removeRepositoryAction(Class<? extends AbstractRepositoryContextAction<?>> actionClass) {
        synchronized (registeredActions) {
            registeredActions.removeIf(repositoryActionRecord -> repositoryActionRecord.actionClass().equals(actionClass));
        }
    }
}
