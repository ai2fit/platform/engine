package com.owc.singularity.studio.gui.vcs.conflict;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryChangeListener;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;

public class MergeConflictResolvingController implements ConflictResolvingComponent.ConflictResolvedEventListener {

    private final EventListenerList eventListenerList = new EventListenerList();
    private final RegisterAdditionalPathsOnMountChange mountChangeListener;

    private final Map<RepositoryPath, ConflictResolvingComponent> conflictResolvingPanelMap = new HashMap<>();

    private final CountDownLatch waitForResultLatch = new CountDownLatch(1);
    private final RepositoryMount mount;
    private final List<MergeConflict> remainingConflicts;
    private final Collection<RepositoryPath> additionalPaths;
    private final EntryVersion ourVersion;
    private final EntryVersion theirVersion;
    private final Collection<MergeConflict> conflicts;
    private Boolean result;
    private String mergeMessage;

    public MergeConflictResolvingController(EntryVersion ourVersion, EntryVersion theirVersion, Collection<MergeConflict> conflicts) throws IOException {
        this.conflicts = conflicts;
        this.remainingConflicts = new ArrayList<>(conflicts);
        this.ourVersion = ourVersion;
        this.theirVersion = theirVersion;
        mount = RepositoryManager.getFileSystem().getMount(remainingConflicts.get(0).getConflictRepositoryPath());

        ClassLoader classLoader = ModuleService.getMajorClassLoader();
        for (MergeConflict conflict : conflicts) {
            RepositoryPath conflictRepositoryPath = conflict.getConflictRepositoryPath();
            Entry ourEntry = Entries.getEntry(RepositoryPath.of(conflictRepositoryPath, conflict.getOurVersionId()));
            Entry theirEntry = Entries.getEntry(RepositoryPath.of(conflictRepositoryPath, conflict.getTheirVersionId()));
            ConflictResolvingComponent resolvingComponent;
            boolean oursExists = Files.exists(ourEntry.getPath());
            boolean theirsExists = Files.exists(theirEntry.getPath());
            if (oursExists && theirsExists && !ourEntry.getContentType().equals(theirEntry.getContentType())) {
                resolvingComponent = new TypeMismatchConflictResolvingComponent(conflict, ourEntry.getContentType(), theirEntry.getContentType());
            } else {
                try {
                    if ((oursExists && ourEntry.isInstanceOf(AbstractPipeline.class, classLoader))
                            || (theirsExists && theirEntry.isInstanceOf(AbstractPipeline.class, classLoader))) {
                        AbstractPipeline ourProcess = oursExists ? ourEntry.loadData(AbstractPipeline.class, classLoader)
                                : (AbstractPipeline) ourEntry.getContentClass(classLoader).getConstructor().newInstance();
                        AbstractPipeline theirProcess = theirsExists ? theirEntry.loadData(AbstractPipeline.class, classLoader)
                                : (AbstractPipeline) theirEntry.getContentClass(classLoader).getConstructor().newInstance();
                        resolvingComponent = new ProcessConflictResolvingComponent(conflict, ourProcess, theirProcess);
                    } else {
                        resolvingComponent = new SimpleConflictResolvingComponent(conflict);
                    }
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException("Entry content type is unknown " + ourEntry.getContentType());
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                        | SecurityException e) {
                    throw new RuntimeException("Pipeline could not be created.", e);

                }
            }
            resolvingComponent.addResolveListener(this);
            conflictResolvingPanelMap.put(conflictRepositoryPath, resolvingComponent);
        }
        additionalPaths = new HashSet<>();
        mountChangeListener = new RegisterAdditionalPathsOnMountChange();
        mount.registerChangeListener(mountChangeListener);
    }

    public EntryVersion getOurVersion() {
        return ourVersion;
    }

    public EntryVersion getTheirVersion() {
        return theirVersion;
    }

    public void abort() {
        for (ConflictResolvingComponent resolvingPanel : conflictResolvingPanelMap.values()) {
            resolvingPanel.removeResolveListener(this);
            resolvingPanel.dispose();
        }
        setResult(false);
    }

    public void merge() {
        if (!isDone()) {
            SwingTools.showVerySimpleErrorMessage("vcs.merge_not_done",
                    remainingConflicts.stream().map(conflict -> conflict.getConflictRepositoryPath().toString(false)).collect(Collectors.toList()));
            return;
        }
        setResult(true);
    }

    private void setResult(boolean result) {
        this.result = result;
        mount.unregisterChangeListener(mountChangeListener);
        waitForResultLatch.countDown();
    }

    public void setMergeMessage(String mergeMessage) {
        this.mergeMessage = mergeMessage;
        fireStateChanged();
    }

    public boolean isDone() {
        return getMergeMessage() != null && conflictResolvingPanelMap.values().stream().allMatch(ConflictResolvingComponent::isResolved);
    }

    public boolean getResult() {
        return this.result;
    }


    public Collection<MergeConflict> getRemainingConflicts() {
        return remainingConflicts;
    }

    public Collection<RepositoryPath> getAdditionalPaths() {
        return additionalPaths;
    }

    public void addAdditionalPath(RepositoryPath path) {
        additionalPaths.add(path.toUnversionedPath());
        fireStateChanged();
    }

    public void removeAdditionalPath(RepositoryPath path) {
        additionalPaths.remove(path.toUnversionedPath());
        fireStateChanged();
    }

    public String getMergeMessage() {
        return mergeMessage;
    }

    public MergeConflictDecision getDecisionFor(RepositoryPath path) {
        return conflictResolvingPanelMap.get(path.toUnversionedPath()).getDecision();
    }

    public void addChangeListener(ChangeListener listener) {
        eventListenerList.add(ChangeListener.class, listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        eventListenerList.remove(ChangeListener.class, listener);
    }

    public ConflictResolvingComponent getResolvingComponentFor(RepositoryPath conflictPath) {
        if (isRepositoryPathAConflict(conflictPath)) {
            return conflictResolvingPanelMap.get(conflictPath.toUnversionedPath());
        }
        return null;
    }

    private boolean isRepositoryPathAConflict(RepositoryPath path) {
        for (MergeConflict conflict : conflicts) {
            if (conflict.getConflictRepositoryPath().equalsIgnoreVersion(path))
                return true;
        }
        return false;
    }

    public boolean isRepositoryPathResolved(RepositoryPath path) {
        for (MergeConflict conflict : remainingConflicts) {
            if (conflict.getConflictRepositoryPath().equalsIgnoreVersion(path))
                return false;
        }
        return true;
    }

    public boolean waitForUserDecision() {
        try {
            waitForResultLatch.await();
            return getResult();
        } catch (InterruptedException e) {
            return false;
        }
    }

    @Override
    public void onResolve(ConflictResolvingComponent self) {
        MergeConflict conflict = self.getConflict();
        remainingConflicts.remove(conflict);
        self.dispose();
        fireStateChanged();
    }

    private void fireStateChanged() {
        ChangeListener[] listeners = eventListenerList.getListeners(ChangeListener.class);
        for (ChangeListener listener : listeners) {
            listener.stateChanged(new ChangeEvent(this));
        }
    }

    public RepositoryPath getMountPath() {
        return mount.getMountPath();
    }

    private class RegisterAdditionalPathsOnMountChange implements RepositoryChangeListener {

        private boolean confirmAddingPathToMerge(RepositoryPath path) {
            ConfirmDialog dialog = new ConfirmDialog(MainFrame.INSTANCE, "vcs.dialog.add_to_merge_files", ConfirmDialog.YES_NO_OPTION, false,
                    path.getFileName().toString(), path.toString(false));
            dialog.setVisible(true);
            return dialog.getReturnOption() == ConfirmDialog.YES_OPTION;
        }

        @Override
        public void entryDeleted(RepositoryPath path) {
            process(path);
        }

        @Override
        public void entryCreated(RepositoryPath path) {
            process(path);
        }

        @Override
        public void entryModified(RepositoryPath path) {
            process(path);
        }

        private void process(RepositoryPath path) {
            if (getAdditionalPaths().contains(path.toUnversionedPath()) || isRepositoryPathAConflict(path)) {
                // already in the merge changes
                return;
            }
            SwingUtilities.invokeLater(() -> {
                if (confirmAddingPathToMerge(path)) {
                    addAdditionalPath(path);
                }
            });
        }
    }
}
