/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.dialogs;


import java.awt.*;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.*;

import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.studio.gui.tools.RepositoryEntryTextField;
import com.owc.singularity.studio.gui.tools.ValidatableTextField;


/**
 * Shows an input dialog which uses the {@link RepositoryEntryTextField}.
 *
 * @author Marco Boeck
 */
public class SingleFieldInputDialog extends ButtonDialog implements Observer<Boolean> {

    private static final long serialVersionUID = -5825873580778775409L;

    private final JButton okButton;
    private final JButton cancelButton;

    private final ValidatableTextField textField;

    /**
     * Display an input dialog for a repository entry.
     *
     * @param owner
     *            the owner of the input dialog
     * @param key
     *            the i18n key
     * @param validationLogic
     *            the Logic to be validated
     * @param validationMessageProvider
     *            the Messages associated with the validation Logic
     * @param text
     *            the text to display
     * @param i18nKey
     *            the i18nKey necessary for the message displayed left o the text field
     * @param arguments
     *            optional i18n arguments
     */
    public SingleFieldInputDialog(Window owner, String key, Predicate<String> validationLogic, Function<String, String> validationMessageProvider, String text,
            String i18nKey, Object... arguments) {
        this(owner, key, null, validationLogic, validationMessageProvider, text, i18nKey, arguments);
    }

    /**
     * Display an input dialog for a repository entry.
     *
     * @param owner
     *            the owner of the input dialog
     * @param key
     *            the i18n key
     * @param messageComponent
     *            an optional component to display within the dialog
     * @param validationLogic
     *            the Logic to be validated
     * @param validationMessageProvider
     *            the Messages associated with the validation Logic
     * @param text
     *            the text to display
     * @param i18nKey
     *            the i18nKey necessary for the message displayed left o the text field
     * @param arguments
     *            optional i18n arguments
     */
    public SingleFieldInputDialog(Window owner, String key, JComponent messageComponent, Predicate<String> validationLogic,
            Function<String, String> validationMessageProvider, String text, String i18nKey, Object... arguments) {
        super(owner, "input." + key, ModalityType.APPLICATION_MODAL, arguments);
        this.okButton = makeOkButton();
        this.cancelButton = makeCancelButton();
        this.textField = new ValidatableTextField(validationLogic, validationMessageProvider, i18nKey);
        textField.addObserver(this, true);
        textField.setText(text);
        if (messageComponent == null) {
            layoutDefault(textField, okButton, cancelButton);
        } else {
            JPanel panel = new JPanel(new BorderLayout());
            panel.add(messageComponent, BorderLayout.CENTER);
            panel.add(textField, BorderLayout.SOUTH);
            layoutDefault(panel, okButton, cancelButton);
        }
        textField.requestFocusInWindow();
    }

    public String getInputText() {
        return textField.getText();
    }

    @Override
    public void update(Observable<Boolean> observable, Boolean arg) {
        okButton.setEnabled(arg);
    }
}
