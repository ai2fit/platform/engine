package com.owc.singularity.studio.gui.tools;

import java.util.function.Supplier;

public class LazySupplier<V> implements Supplier<V> {

    private final Object executionLock = new Object();
    private final Supplier<V> originalSupplier;
    private V computed = null;
    private volatile boolean evaluated = false;

    public LazySupplier(Supplier<V> originalSupplier) {
        this.originalSupplier = originalSupplier;
    }

    @Override
    public V get() {
        return evaluated ? computed : evaluate();
    }

    private synchronized V evaluate() {
        if (evaluated)
            return computed;
        try {
            computed = originalSupplier.get();
        } catch (Exception e) {
            computed = null;
        }
        evaluated = true;
        return computed;
    }

    public synchronized void reset() {
        this.computed = null;
        this.evaluated = false;
    }

    public boolean isEvaluated() {
        return evaluated;
    }
}
