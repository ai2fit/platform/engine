package com.owc.singularity.studio.gui.vcs.conflict;

import java.awt.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collection;

import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.repository.vcs.MergeConflictResolver;
import com.owc.singularity.repository.vcs.MergeConflictSolution;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.perspective.Perspective;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;

public class InteractiveMergeConflictResolver implements MergeConflictResolver {

    private boolean aborted = false;
    private Exception error = null;

    public InteractiveMergeConflictResolver() {

    }

    @Override
    public MergeConflictSolution resolve(EntryVersion ourVersion, EntryVersion theirVersion, Collection<MergeConflict> conflicts)
            throws AbortOperationException {
        // start by telling the user what is going on
        SwingTools.showMessageDialog("vcs.merge_conflict_explanation", "You have a merge conflict between your local changes and changes from the remote");
        PerspectiveController perspectiveController = MainFrame.INSTANCE.getPerspectiveController();
        Perspective perspectiveBeforeOperation = perspectiveController.getSelectedPerspective();
        MergeConflictResolvingView mergeConflictResolvingView = MainFrame.INSTANCE.getMergeConflictResolvingPanel();
        // switch to vcs perspective
        Perspective perspective = perspectiveController.getOrCreatePerspective(PerspectiveController.VCS, false);
        perspectiveController.showPerspective(perspective);
        try {
            MergeConflictResolvingController mergeConflictResolvingController = new MergeConflictResolvingController(ourVersion, theirVersion, conflicts);

            Component panel = mergeConflictResolvingView.showViewFor(mergeConflictResolvingController);
            mergeConflictResolvingView.getComponent().setVisible(true);
            panel.requestFocusInWindow();
            boolean resolve = mergeConflictResolvingController.waitForUserDecision();
            mergeConflictResolvingView.removeView(panel);
            if (!resolve) {
                throw new AbortOperationException();
            }
            String mergeCommitMessage = mergeConflictResolvingController.getMergeMessage();
            for (MergeConflict conflict : conflicts) {
                MergeConflictDecision decision = mergeConflictResolvingController.getDecisionFor(conflict.getConflictRepositoryPath());
                if (!Files.exists(decision.getResultRepositoryPath())) {
                    // the decision is a version where the file has been deleted,
                    // so we delete the conflict file
                    Files.delete(conflict.getConflictRepositoryPath());
                } else {
                    if (decision.getResultRepositoryPath().equals(conflict.getConflictRepositoryPath())) {
                        // changes were made directly to the conflicting file, no need to change
                        // anything
                        continue;
                    }
                    try (InputStream result = decision.getInputStream()) {
                        try (OutputStream outputStream = Files.newOutputStream(conflict.getConflictRepositoryPath())) {
                            result.transferTo(outputStream);
                        }
                    }
                }
            }
            return new MergeConflictSolution(mergeCommitMessage, mergeConflictResolvingController.getAdditionalPaths());
        } catch (Exception e) {
            if (e instanceof AbortOperationException) {
                aborted = true;
                throw (AbortOperationException) e;
            }
            error = e;
            throw new AbortOperationException();
        } finally {
            perspectiveController.showPerspective(perspectiveBeforeOperation);
        }
    }

    public boolean isAborted() {
        return aborted;
    }

    public boolean isError() {
        return error != null;
    }

    public Exception getError() {
        return error;
    }
}
