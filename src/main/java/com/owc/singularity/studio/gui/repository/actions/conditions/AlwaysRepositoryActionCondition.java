package com.owc.singularity.studio.gui.repository.actions.conditions;

import java.util.List;

import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


public class AlwaysRepositoryActionCondition implements RepositoryActionCondition {

    @Override
    public boolean test(List<RepositoryTreeNode> entryList) {
        return true;
    }

}
