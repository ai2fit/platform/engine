/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.repository;


import java.awt.Component;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import com.owc.singularity.engine.EngineProperties;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.metadata.ConnectionParametersMetadata;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.PipelineMetaData;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.repository.PlaceholderRepositoryMount;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.git.GitRepositoryMount;
import com.owc.singularity.repository.git.UncommittedEntryVersion;
import com.owc.singularity.studio.gui.IOObjectGUIService;
import com.owc.singularity.studio.gui.connection.ConnectionI18N;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNodeInformation;
import com.owc.singularity.studio.gui.repository.worker.BackgroundLoader;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.Icons;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * @author Simon Fischer
 */
public class RepositoryTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final String DASH = " &ndash; ";

    private static final long serialVersionUID = 1L;

    public static final ImageIcon ICON_FOLDER_OPEN = SwingTools.createIcon("24/folder_open.png", IconSize.SMALL);
    public static final ImageIcon ICON_FOLDER_CLOSED = SwingTools.createIcon("24/folder.png", IconSize.SMALL);
    public static final ImageIcon ICON_MOUNT_GIT_FOLDER_OPEN = SwingTools.createIcon("24/folder_into.png", IconSize.SMALL);
    public static final ImageIcon ICON_MOUNT_FILE_FOLDER_OPEN = SwingTools.createIcon("24/folder_document.png", IconSize.SMALL);
    public static final ImageIcon ICON_FOLDER_LOADING = SwingTools.createIcon("24/hourglass.png", IconSize.SMALL);

    public static final ImageIcon ICON_PIPELINE = SwingTools.createIcon("24/pipeline.png", IconSize.SMALL);
    public static final ImageIcon ICON_UNKNOWN = SwingTools.createIcon("24/question.png", IconSize.SMALL);
    public static final ImageIcon ICON_MOUNT_PLACEHOLDER = SwingTools.createIcon("24/link_broken.png", IconSize.SMALL);
    public static final ImageIcon ICON_REPOSITORY = SwingTools.createIcon("24/server_network.png", IconSize.SMALL);
    public static final ImageIcon ICON_ERROR = SwingTools.createIcon("24/error.png", IconSize.SMALL);

    public static final Map<RepositoryPath, Pair<Instant, ImageIcon>> PIPELINE_ICON_CACHE = new ConcurrentHashMap<>();
    public static final Map<RepositoryPath, Pair<Instant, Icon>> CONNECTION_ICON_CACHE = new ConcurrentHashMap<>();


    private final DateTimeFormatter dateFormatter;

    private static final Border ENTRY_BORDER = BorderFactory.createEmptyBorder(1, 0, 1, 0);

    public RepositoryTreeCellRenderer() {
        String userSpecifiedDateTimeFormat = PropertyService.getParameterValue(EngineProperties.GENERAL_LOCALE_DATE_FORMAT);
        if (userSpecifiedDateTimeFormat == null || userSpecifiedDateTimeFormat.isBlank()) {
            dateFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withZone(ZoneId.systemDefault());
        } else {
            dateFormatter = DateTimeFormatter.ofPattern(userSpecifiedDateTimeFormat, Locale.getDefault()).withZone(ZoneId.systemDefault());
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        RepositoryTreeNode entry = (RepositoryTreeNode) value;
        setText(deriveText(tree, entry));
        setIcon(deriveIcon(tree, entry, expanded));
        setBorder(ENTRY_BORDER);
        return this;
    }

    private String deriveText(final JTree tree, final RepositoryTreeNode node) {
        RepositoryTreeNodeInformation model = node.getInformation();
        StringBuilder labelText = new StringBuilder();
        if (model.isDirectory()) {
            labelText.append("<html>");
            if (model.getRepositoryPath().getNameCount() > 0) {
                labelText.append(adjustRepositoryEntryName(model.getFileName()));
            } else {
                // root
                labelText.append(model.getRepositoryPath().getFileSystem().getRepositoryName());
            }
            labelText.append("</html>");
        } else {
            labelText.append("<html>").append(adjustRepositoryEntryName(model.getFileName()));
            // version information
            StringBuilder versionBuilder = new StringBuilder();
            EntryVersion version = BackgroundLoader.loadAndReplace(null, model::readVersion, ignored -> updateNode(tree, node), model.hasVersion());
            if (version == null || version instanceof UncommittedEntryVersion) {
                // dirty file or
                // the file has not been committed yet
                versionBuilder.append("<it>*</it>");
                Instant lastChangeTimestamp = model.getLastModifiedTimestamp();
                versionBuilder.append(DASH);
                versionBuilder.append(dateFormatter.format(lastChangeTimestamp));
            } else {
                String author = version.getAuthor();
                if (author != null) {
                    versionBuilder.append(author);
                }
                Instant timestamp = version.getCommitTimestamp();
                if (timestamp == null) {
                    // a version with no commit timestamp
                    // at least we show the last modification timestamp
                    timestamp = model.getLastModifiedTimestamp();
                }
                if (timestamp != null) {
                    versionBuilder.append(DASH);
                    versionBuilder.append(dateFormatter.format(timestamp));
                }
            }
            versionBuilder.append(DASH);
            versionBuilder.append(Tools.formatBytes(model.getFileSize()));
            labelText.append(" <small style=\"color:gray\">(").append(versionBuilder).append(")</small>");
            labelText.append("</html>");
        }
        return labelText.toString();
    }

    @SuppressWarnings("unchecked")
    private Icon deriveIcon(final JTree tree, final RepositoryTreeNode node, boolean isExpanded) {
        RepositoryTreeNodeInformation entry = node.getInformation();
        if (entry.getRepositoryPath().getNameCount() == 0) {
            // root path
            return ICON_REPOSITORY;
        }
        if (entry.isDirectory()) {
            if (node.isLoading())
                return ICON_FOLDER_LOADING;
            RepositoryMount mount = entry.getMount();
            if (mount instanceof PlaceholderRepositoryMount) {
                return ICON_MOUNT_PLACEHOLDER;
            } else if (mount instanceof GitRepositoryMount) {
                if (entry.getMount().getMountPath().equals(entry.getRepositoryPath())) {
                    return ICON_MOUNT_GIT_FOLDER_OPEN;
                }
                if (isExpanded)
                    return ICON_FOLDER_OPEN;
                return ICON_FOLDER_CLOSED;
            } else {
                if (entry.getMount().getMountPath().equals(entry.getRepositoryPath())) {
                    return ICON_MOUNT_FILE_FOLDER_OPEN;
                }
                if (isExpanded)
                    return ICON_FOLDER_OPEN;
                return ICON_FOLDER_CLOSED;
            }
        } else {
            if (!entry.hasContentClass()) {
                BackgroundLoader.loadAsync(entry::readContentClass, ignored -> updateNode(tree, node));
                return ICON_FOLDER_LOADING;
            }
            Class<?> contentClass = entry.readContentClass();
            if (contentClass == RepositoryTreeNodeInformation.UNKNOWN_CONTENT_CLASS) {
                return ICON_ERROR;
            }
            if (ConnectionParametersIOObject.class.isAssignableFrom(contentClass)) {
                return deriveConnectionIcon(entry);
            } else if (IOObject.class.isAssignableFrom(contentClass)) {
                return IOObjectGUIService.getIcon((Class<? extends IOObject>) contentClass);
            } else if (AbstractPipeline.class.isAssignableFrom(contentClass)) {
                return derivePipelineIcon(entry);
            }
            return ICON_UNKNOWN;
        }
    }

    private static void updateNode(JTree tree, RepositoryTreeNode node) {
        ((DefaultTreeModel) tree.getModel()).nodeChanged(node);
    }

    public static Icon deriveConnectionIcon(RepositoryTreeNodeInformation entry) {
        Pair<Instant, Icon> pair = CONNECTION_ICON_CACHE.get(entry.getRepositoryPath());
        if (pair == null || !pair.getFirst().equals(entry.getLastModifiedTimestamp())) {
            // we have to reconsider
            ConnectionParametersMetadata connectionMetaData = null;
            try {
                connectionMetaData = entry.getEntry().loadMetaData(ConnectionParametersMetadata.class, ModuleService.getMajorClassLoader());
                Icon icon = SwingTools.addIconOverlay(SwingTools.createIcon("16/plug.png", IconSize.SMALL), getConnectionIconFromMetadata(connectionMetaData),
                        0.6);
                pair = new Pair<>(entry.getLastModifiedTimestamp(), icon);
                CONNECTION_ICON_CACHE.put(entry.getRepositoryPath(), pair);
                return icon;
            } catch (IOException e) {
                // happens on old savings, we handle below.
            }
            if (connectionMetaData == null) {
                ImageIcon icon = SwingTools.createIcon("16/plug.png", IconSize.SMALL);
                pair = new Pair<>(entry.getLastModifiedTimestamp(), icon);
                CONNECTION_ICON_CACHE.put(entry.getRepositoryPath(), pair);
                return icon;
            }
        }
        return pair.getSecond();
    }

    public static ImageIcon derivePipelineIcon(RepositoryTreeNodeInformation entry) {
        Pair<Instant, ImageIcon> pair = PIPELINE_ICON_CACHE.get(entry.getRepositoryPath());
        if (pair == null || !pair.getFirst().equals(entry.getLastModifiedTimestamp())) {
            // we have to reconsider
            PipelineMetaData metaPipeline = null;
            try {
                metaPipeline = entry.getEntry().loadMetaData(PipelineMetaData.class, ModuleService.getMajorClassLoader());
                ImageIcon icon = getPipelineIconFromMetadata(metaPipeline);
                pair = new Pair<>(entry.getLastModifiedTimestamp(), icon);
                PIPELINE_ICON_CACHE.put(entry.getRepositoryPath(), pair);
            } catch (IOException e) {
                // happens on old savings, we handle below.
            }
            if (metaPipeline == null) {
                ImageIcon icon = Icons.fromName("pipeline.png").getSmallIcon();
                pair = new Pair<>(entry.getLastModifiedTimestamp(), icon);
                PIPELINE_ICON_CACHE.put(entry.getRepositoryPath(), pair);
            }
        }
        return pair.getSecond();
    }

    public static ImageIcon getPipelineIconFromMetadata(PipelineMetaData metaPipeline) {
        if (metaPipeline != null) {
            return getPipelineIconFromMetadata(metaPipeline.getPipelineTypeName(), metaPipeline.isImplementing(), metaPipeline.isAbstract());
        }
        return Icons.fromName("pipeline.png").getSmallIcon();
    }

    public static ImageIcon getPipelineIconFromMetadata(String typeName, boolean isImplementing, boolean isAbstract) {
        StringBuilder type = new StringBuilder();
        if (isImplementing) {
            type.append("implementing_");
        } else if (isAbstract) {
            type.append("abstract_");
        }
        type.append("pipeline");
        if (typeName.endsWith("ScheduledAnalysisPipeline")) {
            type.append("_scheduled");
        } else if (typeName.endsWith("AnalysisPipeline")) {
            type.append("_spreadsheet_chart");
        } else if (typeName.endsWith("WebservicePipeline")) {
            type.append("_webservice");
        } else if (typeName.endsWith("SubPipeline")) {
            type.append("_piece");
        }
        type.append(".png");

        return Icons.fromName(type.toString()).getSmallIcon();
    }

    private static ImageIcon getConnectionIconFromMetadata(ConnectionParametersMetadata parametersMetadata) {
        if (parametersMetadata != null) {
            return Icons.fromName(ConnectionI18N.getConnectionIconName(parametersMetadata.getParameters().getTypeId())).getSmallIcon();
        }
        return null;
    }

    /**
     * This method highlights consecutive whitespaces inside a repository items name with a html
     * underline
     * 
     * @param original
     *            The repository item name to be checked
     * @return The highlighted repository item name
     */
    private String adjustRepositoryEntryName(String original) {
        if (original.contains("  ")) {
            StringBuilder b = new StringBuilder(original.length());
            for (int i = 0; i < original.length(); i++) {
                char c = original.charAt(i);
                if (Character.isWhitespace(c) && i > 0) {
                    boolean whitespaceMinus1 = Character.isWhitespace(original.charAt(i - 1));
                    boolean whitespacePlus1 = Character.isWhitespace(original.charAt(i + 1));
                    if (whitespaceMinus1 || whitespacePlus1) {
                        if (!whitespaceMinus1) {
                            b.append("&nbsp;<u style= \"color:red;\">");
                        } else if (!whitespacePlus1) {
                            b.append("&nbsp;" + "</u>");
                        } else {
                            b.append(c);
                        }
                    } else {
                        b.append(c);
                    }
                } else {
                    b.append(c);
                }
            }
            return b.toString();
        } else {
            return original;
        }
    }
}
