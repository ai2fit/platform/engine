/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.repository.actions.context;

import java.io.IOException;
import java.util.List;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.connection.ConnectionEditDialog;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Edit Connection Action
 *
 * @author Jonas Wilms-Pfau
 * @since 9.3.0
 */
public class EditConnectionAction extends AbstractRepositoryContextAction<Object> {

    private static final long serialVersionUID = 836060161321369660L;
    private static final String PROGRESS_THREAD_KEY = "download_connection_from_repository";

    public EditConnectionAction(RepositoryTree tree) {
        super(tree, false, true, ConnectionParametersIOObject.class, false, true, "repository_edit_connection");
    }

    /**
     * Opens the connection edit dialog. Note that the retrieval happens async, so this may take a
     * while before the dialog will appear. A
     * {@link com.owc.singularity.studio.gui.tools.ProgressThread} will be visible while the
     * retrieval is in progress.
     *
     * @param path
     *            the path to the entry containing a {@link ConnectionParametersIOObject}
     * @param openInEditMode
     *            {@code true} if the connection should be opened in edit mode
     */
    public static void openConnection(RepositoryPath path, boolean openInEditMode) {
        final ProgressThread downloadProgressThread = new ProgressThread(PROGRESS_THREAD_KEY, false, path.toString()) {

            @Override
            public void run() {
                Entry connectionEntry = Entries.getEntry(path);
                try {
                    ConnectionParametersIOObject connectionIOObject = connectionEntry.loadData(ConnectionParametersIOObject.class,
                            ModuleService.getMajorClassLoader());
                    SwingTools.invokeLater(() -> new ConnectionEditDialog(path, connectionIOObject.getParameters(), true).setVisible(true));
                } catch (IOException e) {
                    SwingTools.showSimpleErrorMessage("connection_read_error", e, e.getMessage());
                }
            }

            @Override
            public String getID() {
                return super.getID() + path.toString();
            }
        };
        downloadProgressThread.start();
    }

    @Override
    protected Object configureAction(List<RepositoryTreeNode> entries) {
        return new Object(); // we don't have something to configure
    }

    @Override
    public void executeAction(RepositoryPath path, Object config, ProgressListener progressListener) {
        Entry connectionEntry = Entries.getEntry(path);

        try {
            ConnectionParametersIOObject connectionIOObject = connectionEntry.loadData(ConnectionParametersIOObject.class, ModuleService.getMajorClassLoader());
            SwingTools.invokeLater(() -> new ConnectionEditDialog(path, connectionIOObject.getParameters(), true).setVisible(true));
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("connection_read_error", e, e.getMessage());
        }
    }
}
