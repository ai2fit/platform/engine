/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeAttribute;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.studio.gui.tools.autocomplete.AutoCompleteComboBoxAddition;


/**
 * Autocompletion combo box that observes an input port so it can update itself whenever the meta
 * data changes.
 *
 * @author Simon Fischer, Sebastian Land
 *
 */
public class AttributeComboBox extends JComboBox<String> {

    private static final long serialVersionUID = 1L;

    static class AttributeComboBoxModel extends DefaultComboBoxModel<String> {

        private static final long serialVersionUID = 1L;

        private ParameterTypeAttribute attributeType;
        private List<Pair<String, ValueType>> attributes = new ArrayList<>();

        AttributeComboBoxModel(ParameterTypeAttribute attributeType) {
            this.attributeType = attributeType;
            MetaData metaData = attributeType.getMetaData();
            if (metaData != null) {
                attributes = attributeType.getAttributeNamesAndTypes(true);
            }
        }

        @Override
        public int getSize() {
            return attributes.size();
        }

        @Override
        public String getElementAt(int index) {
            return attributes.get(index).getFirst();
        }


        /**
         * @return the attribute <> value type pairs
         */
        List<Pair<String, ValueType>> getAttributePairs() {
            return attributes;
        }
    }

    private AttributeComboBoxModel model;

    public AttributeComboBox(ParameterTypeAttribute type) {
        super(new AttributeComboBoxModel(type));
        model = (AttributeComboBoxModel) getModel();
        AutoCompleteComboBoxAddition autoCompleteCBA = new AutoCompleteComboBoxAddition(this);
        autoCompleteCBA.setCaseSensitive(false);
    }
}
