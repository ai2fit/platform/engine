package com.owc.singularity.studio.gui.editor.event;

import java.util.EventListener;

import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;

public interface FocusProcessChangeEventListener extends EventListener {

    void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel);
}
