/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Abstract superclass of actions that are executed on Entries of the repository file system. The
 * type T will be resolved against the actually stored class in the entries. Automatically
 * enables/disables itself depending on matching type.
 *
 * @author Simon Fischer, Adrian Wilke
 */
public abstract class AbstractRepositoryContextAction<T> extends ResourceAction {

    /** the tree to which the action belongs to */
    protected final transient RepositoryTree tree;

    /** the required selection type for the action to show/enable */
    private final Class<?> requiredEntryType;

    /** if write access to the repository is needed for this action */
    private final boolean needsWriteAccess;

    private static final long serialVersionUID = -6415235351430454776L;

    private final boolean enabledOnDirectories;

    private final boolean enabledOnFiles;

    private final boolean enableOnMultiSelection;

    /**
     * This is the constructor to be called by subclasses if they want to be only enabled on paths
     * pointing to entries of specific classes. Boolean flags can be used to decide, whether this
     * action is enabled on directories and/or entries. If on entries, the required type can be
     * given, or null passed for any type.
     * 
     */
    public AbstractRepositoryContextAction(RepositoryTree tree, boolean enabledOnDirectories, boolean enabledOnFiles, Class<?> requiredEntryType,
            boolean enableOnMultiSelection, boolean needsWriteAccess, String i18nKey) {
        super(true, i18nKey);
        this.tree = tree;
        if (!enabledOnFiles && !enabledOnDirectories) {
            throw new IllegalArgumentException("Invalid context action since it supports neither files nor folders");
        }
        this.enabledOnDirectories = enabledOnDirectories;
        this.enabledOnFiles = enabledOnFiles;
        this.requiredEntryType = requiredEntryType;
        this.enableOnMultiSelection = enableOnMultiSelection;
        this.needsWriteAccess = needsWriteAccess;
        setEnabled(false);
    }

    @Override
    protected void update(EnumMap<Condition, Boolean> states) {
        // we have our own mechanism to enable/disable actions,
        // so ignore ConditionalAction mechanism
    }

    /**
     * Enables action, if every entry exists and can be written if needed.
     */
    public boolean enable() {

        // Do not treat entries, whose are already included in selected folders
        List<RepositoryTreeNode> entries = removeIntersectedEntries(tree.getSelectedEntries());
        if (entries.size() > 1 && !enableOnMultiSelection) {
            setEnabled(false);
            return false;
        } else {
            boolean enable = true;
            for (RepositoryTreeNode entry : entries) {
                enable = isEntrySupported(entry);
                if (!enable)
                    break;
            }
            if (entries.isEmpty()) {
                enable = false;
            }
            setEnabled(enable);
            return enable;
        }
    }

    private boolean isEntrySupported(RepositoryTreeNode entry) {
        if (entry == null) {
            return false;
        }
        boolean isDirectory = entry.getInformation().isDirectory();
        if (isDirectory) {
            return enabledOnDirectories;
        } else {
            if (enabledOnFiles) {
                try {
                    if (requiredEntryType != null && !entry.getEntry().isInstanceOf(requiredEntryType, ModuleService.getMajorClassLoader())) {
                        // this refers to entries of a specific type
                        return false;
                    }
                } catch (ClassNotFoundException | IOException e) {
                    // unknown type -> unsupported
                    return false;
                }
            } else {
                return false;
            }
        }
        return !needsWriteAccess || entry.getInformation().isWritable();
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        final List<RepositoryTreeNode> remainingEntries = removeIntersectedEntries(tree.getSelectedEntries());

        T config = configureAction(remainingEntries);
        if (config != null) {
            ProgressThread progressThread = new ProgressThread(getKey(), false,
                    remainingEntries.stream().map(RepositoryTreeNode::getRepositoryPathStringWithoutVersion).toArray()) {

                @Override
                public void run() {
                    int progressListenerCompleted = 0;
                    // Initialize progress listener
                    getProgressListener().setTotal(remainingEntries.size());
                    getProgressListener().setCompleted(0);
                    try {
                        for (RepositoryTreeNode entry : remainingEntries) {
                            if (isEntrySupported(entry)) {
                                executeAction(entry.getRepositoryPath(), config, getProgressListener());
                                progressListenerCompleted++;
                                getProgressListener().setCompleted(progressListenerCompleted);
                            }
                        }
                    } finally {
                        getProgressListener().complete();
                    }
                }
            };
            progressThread.start();
        }
    }

    /**
     * This method is to be implemented to configure the actual action. It will be called once with
     * the full set of selected entries. A rename would like to ask for a new name. It is executed
     * on the AWTEventQueue and returns a configuration file or null if the user aborted.
     * 
     * @param entries
     *            the selected entries
     * @return the configuration object
     */
    protected abstract T configureAction(List<RepositoryTreeNode> entries);

    /**
     * This method is to be implemented by subclasses. It will be called for each selected
     * {@link RepositoryPath} once and it is guaranteed that this entry matching the requirements.
     * It is executed within a background thread for avoiding blocking the AWT Queue.
     *
     * @param path
     *            the path for the actual action
     * @param config
     *            the config of type T
     * @param progressListener
     *            a nullable listener which can relay feedback on the progress to the user
     */
    public abstract void executeAction(RepositoryPath path, T config, ProgressListener progressListener);

    /**
     * Removes entries from list, which are already included in parent entries. Works inplace, so
     * the given list is modified
     * <p>
     * Example: [/1/2/3, /1, /1/2] becomes [/1]
     */
    private static List<RepositoryTreeNode> removeIntersectedEntries(List<RepositoryTreeNode> entries) {
        entries.sort(Comparator.comparing(RepositoryTreeNode::getRepositoryPath));
        Iterator<RepositoryTreeNode> iterator = entries.iterator();
        RepositoryPath currentReference = null;
        while (iterator.hasNext()) {
            if (currentReference == null)
                currentReference = iterator.next().getRepositoryPath();
            else {
                RepositoryPath other = iterator.next().getRepositoryPath();
                if (other.startsWith(currentReference))
                    iterator.remove();
                else
                    currentReference = other;
            }
        }

        return entries;
    }
}
