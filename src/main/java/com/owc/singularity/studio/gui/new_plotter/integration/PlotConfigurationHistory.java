/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.new_plotter.integration;


import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.studio.gui.new_plotter.configuration.DataTableColumn;
import com.owc.singularity.studio.gui.new_plotter.configuration.DataTableColumn.PlotColumnValueType;
import com.owc.singularity.studio.gui.new_plotter.configuration.DefaultDimensionConfig;
import com.owc.singularity.studio.gui.new_plotter.configuration.PlotConfiguration;
import com.owc.singularity.studio.gui.new_plotter.configuration.ValueSource;
import com.owc.singularity.studio.gui.new_plotter.configuration.ValueSource.SeriesUsageType;
import com.owc.singularity.studio.gui.new_plotter.data.DataTableColumnIndex;
import com.owc.singularity.studio.gui.new_plotter.gui.AbstractConfigurationPanel.DatasetTransformationType;


/**
 * This class holds informations about plotter settings in the processing history since the
 * SingularityEngine startup. They might be used for pre-initilizing the plotter with settings from
 * the past processing history.
 * 
 * @author Sebastian Land, Marius Helf
 */

public final class PlotConfigurationHistory {

    private static final Map<Set<String>, Map<DatasetTransformationType, PlotConfiguration>> settingsHistory = new LinkedHashMap<Set<String>, Map<DatasetTransformationType, PlotConfiguration>>();

    /**
     * Private ctor - static only class.
     */
    private PlotConfigurationHistory() {};

    public static Map<DatasetTransformationType, PlotConfiguration> getPlotConfigurationMap(DataTable dataTable) {
        // search for compatible data set
        String[] columnNames = dataTable.getColumnNames();
        Set<String> columnNameSet = Arrays.stream(columnNames).collect(Collectors.toSet());

        Optional<Set<String>> optionalConfig = settingsHistory.entrySet()
                .stream()
                .filter(e -> isPlotConfigurationCompatible(e.getValue().get(DatasetTransformationType.ORIGINAL), dataTable))
                .map(Entry::getKey)
                .findAny();

        Map<DatasetTransformationType, PlotConfiguration> plotConfigMap = new HashMap<DatasetTransformationType, PlotConfiguration>();
        if (optionalConfig.isPresent()) {
            // we use that and clone the contents from before
            settingsHistory.get(optionalConfig.get()).entrySet().forEach(e -> plotConfigMap.put(e.getKey(), e.getValue().clone()));
        } else {
            PlotConfiguration plotConfiguration = new PlotConfiguration(new DataTableColumn(dataTable, -1));
            plotConfigMap.put(DatasetTransformationType.ORIGINAL, plotConfiguration);

            PlotConfiguration metaPlotConfiguration = new PlotConfiguration(new DataTableColumn(null, PlotColumnValueType.INVALID));
            plotConfigMap.put(DatasetTransformationType.DE_PIVOTED, metaPlotConfiguration);
        }
        settingsHistory.put(columnNameSet, plotConfigMap);
        return plotConfigMap;
    }

    /**
     * Returns <code>true</code> iff all columns used in <code>plotConfiguration</code> are also
     * present in <code>dataTable</code> and have compatible value types.
     */
    private static boolean isPlotConfigurationCompatible(PlotConfiguration plotConfiguration, DataTable dataTable) {
        // check if columns from valueSources are present and compatible in dataTable
        for (ValueSource valueSource : plotConfiguration.getAllValueSources()) {
            for (SeriesUsageType usageType : valueSource.getDefinedUsageTypes()) {
                DataTableColumn column = valueSource.getDataTableColumn(usageType);
                if (column != null) {
                    DataTableColumnIndex columnIdx = new DataTableColumnIndex(column, dataTable);
                    if (columnIdx.getIndex() < 0) {
                        return false;
                    }
                }
            }
        }

        // check if columns from defaultDimensionConfigs are present and compatible in dataTable
        for (DefaultDimensionConfig defaultDimensionConfig : plotConfiguration.getDefaultDimensionConfigs().values()) {
            DataTableColumn column = defaultDimensionConfig.getDataTableColumn();
            DataTableColumnIndex columnIdx = new DataTableColumnIndex(column, dataTable);
            if (columnIdx.getIndex() < 0) {
                return false;
            }
        }

        // check if columns from domainConfigManager is present and compatible in dataTable
        DataTableColumn column = plotConfiguration.getDomainConfigManager().getDataTableColumn();
        DataTableColumnIndex columnIdx = new DataTableColumnIndex(column, dataTable);
        if (columnIdx.getIndex() < 0) {
            return false;
        }

        return true;
    }
}
