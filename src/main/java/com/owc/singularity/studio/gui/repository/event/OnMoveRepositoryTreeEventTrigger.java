package com.owc.singularity.studio.gui.repository.event;

import com.owc.singularity.repository.RepositoryPath;

/**
 * A trigger that fires when a Move event is detected
 */
public class OnMoveRepositoryTreeEventTrigger extends TimeoutRepositoryTreeEventTrigger {

    private final RepositoryPath sourcePath;
    private final RepositoryPath targetPath;
    // a matching insert and remove events must come before we fire
    private int eventsCount = 2;

    public OnMoveRepositoryTreeEventTrigger(RepositoryPath sourcePath, RepositoryPath targetPath) {
        this(sourcePath, targetPath, 5_000);
    }

    public OnMoveRepositoryTreeEventTrigger(RepositoryPath sourcePath, RepositoryPath targetPath, long timeoutMillis) {
        super(timeoutMillis);
        this.sourcePath = sourcePath;
        this.targetPath = targetPath;
    }

    @Override
    public boolean shouldFire(RepositoryTreeUpdateType event, RepositoryPath... affectedPaths) {
        switch (event) {
            case Update -> {
                return false;
            }
            case Insert -> {
                for (RepositoryPath path : affectedPaths) {
                    if (targetPath.equals(path)) {
                        eventsCount--;
                        break;
                    }
                }
            }
            case Remove -> {
                for (RepositoryPath path : affectedPaths) {
                    if (sourcePath.equals(path)) {
                        eventsCount--;
                        break;
                    }
                }
            }
        }
        return eventsCount == 0;
    }

    @Override
    public boolean shouldBeRemoved() {
        return eventsCount <= 0 || super.shouldBeRemoved();
    }
}
