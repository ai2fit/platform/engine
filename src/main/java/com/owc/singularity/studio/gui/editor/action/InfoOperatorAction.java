/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.dialog.OperatorInfoScreen;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa
 */
public abstract class InfoOperatorAction extends ResourceAction {

    private static final long serialVersionUID = 1764142570608930118L;

    public InfoOperatorAction() {
        super(true, "operator_info");
        setCondition(Condition.OPERATOR_SELECTED, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        Operator selectedOperator = getOperator();
        if (selectedOperator != null) {
            OperatorInfoScreen infoScreen = new OperatorInfoScreen(selectedOperator);
            infoScreen.setVisible(true);
        }
    }

    protected abstract Operator getOperator();
}
