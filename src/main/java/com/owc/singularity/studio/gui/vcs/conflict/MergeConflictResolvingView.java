package com.owc.singularity.studio.gui.vcs.conflict;

import java.awt.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.tree.TreeNode;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;
import com.owc.singularity.studio.gui.vcs.MountTreeDiffPanel;
import com.owc.singularity.studio.gui.vcs.MountTreeDiffTree;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;
import com.owc.singularity.studio.gui.vcs.conflict.model.SimpleMergeConflictDecision;
import com.owc.singularity.studio.gui.vcs.model.AbstractMountTreeDiffFileTreeNode;
import com.owc.singularity.studio.gui.vcs.model.AbstractMountTreeDiffTreeNode;
import com.owc.singularity.studio.gui.vcs.model.MountTreeDiffMergeConflictTreeNode;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class MergeConflictResolvingView implements Dockable {

    public static final String DOCKABLE_KEY = "vcs.conflict_resolver";
    private final DockKey key;
    private final JPanel container;
    private final Map<MergeConflictResolvingController, JPanel> resolvers;

    public MergeConflictResolvingView() {
        container = new JPanel(new CardLayout());
        resolvers = new HashMap<>();
        key = new ResourceDockKey(DOCKABLE_KEY);
        key.setDockGroup(PerspectiveController.DOCK_GROUP_VCS);
    }

    public Component showViewFor(MergeConflictResolvingController resolvingController) {
        CardLayout cards = (CardLayout) container.getLayout();
        JPanel component = resolvers.get(resolvingController);
        if (component == null) {
            component = createView(resolvingController);
            resolvers.put(resolvingController, component);
            container.add(component, component.toString());
        }
        JPanel finalComponent = component;
        SwingUtilities.invokeLater(() -> {
            cards.show(container, finalComponent.toString());
            container.setVisible(true);
        });
        return component;
    }

    public void removeView(Component resolverPanel) {
        container.remove(resolverPanel);
    }

    private JPanel createView(MergeConflictResolvingController resolvingController) {
        RepositoryPath mountPath = resolvingController.getMountPath();
        EntryVersion ourVersion = resolvingController.getOurVersion();
        EntryVersion theirVersion = resolvingController.getTheirVersion();
        Set<RepositoryPath> conflictPaths = resolvingController.getRemainingConflicts()
                .stream()
                .map(MergeConflict::getConflictRepositoryPath)
                .collect(Collectors.toSet());
        MountTreeDiffPanel mountTreeDiffPanel = new MountTreeDiffPanel(mountPath, ourVersion, theirVersion, conflictPaths,
                resolvingController.getAdditionalPaths());
        MountTreeDiffTree differenceTree = mountTreeDiffPanel.getTree();
        Predicate<TreeNode> resolvedConflictsPredicate = MountTreeDiffPanel.SHOW_FOR_CONFLICT_NODES
                .and(treeNode -> resolvingController.isRepositoryPathResolved(((MountTreeDiffMergeConflictTreeNode) treeNode).getRepositoryPath()));
        mountTreeDiffPanel.addPopupMenuItem(resolvedConflictsPredicate, new JMenuItem(ResourceAction.of("vcs.merge.show_resolved", event -> {
            RepositoryPath resolvedConflictPath = differenceTree.getSelectedChange().getRepositoryPath();
            MergeConflictDecision decision = resolvingController.getDecisionFor(resolvedConflictPath);
            RepositoryPath repositoryPath = decision.getResultRepositoryPath();
            if (Files.exists(repositoryPath)) {
                OpenEntryAction.open(Entries.getEntry(repositoryPath));
            } else {
                SwingTools.showVerySimpleErrorMessage("vcs.compare.path_not_found_in_version", repositoryPath.getFileName().toString(),
                        repositoryPath.toString(false), decision.getDescription());
            }
        })));
        mountTreeDiffPanel.addPopupMenuItem(MountTreeDiffPanel.SHOW_FOR_CONFLICT_NODES, new JMenuItem(ResourceAction.of("vcs.merge.resolve_manually", event -> {
            RepositoryPath conflict = differenceTree.getSelectedChange().getRepositoryPath();
            ConflictResolvingComponent resolvingComponent = resolvingController.getResolvingComponentFor(conflict);
            if (resolvingComponent == null) {
                SwingTools.showVerySimpleErrorMessage("vcs.merge.no_conflict", conflict.toString(false));
            } else {
                resolvingComponent.show();
            }
        })), new JMenuItem(ResourceAction.of("vcs.merge.use_our_version", event -> {
            RepositoryPath conflictPath = differenceTree.getSelectedChange().getRepositoryPath();
            ConflictResolvingComponent resolvingComponent = resolvingController.getResolvingComponentFor(conflictPath);
            if (resolvingComponent == null) {
                SwingTools.showVerySimpleErrorMessage("vcs.merge.no_conflict", conflictPath.toString(false));
            } else {
                resolvingComponent.setDecision(new SimpleMergeConflictDecision(resolvingComponent.getConflict(), true));
            }
        })), new JMenuItem(ResourceAction.of("vcs.merge.use_their_version", event -> {
            RepositoryPath conflictPath = differenceTree.getSelectedChange().getRepositoryPath();
            ConflictResolvingComponent resolvingComponent = resolvingController.getResolvingComponentFor(conflictPath);
            if (resolvingComponent == null) {
                SwingTools.showVerySimpleErrorMessage("vcs.merge.no_conflict", conflictPath.toString(false));
            } else {
                resolvingComponent.setDecision(new SimpleMergeConflictDecision(resolvingComponent.getConflict(), false));
            }
        })));

        Predicate<TreeNode> forOurChanges = (TreeNode node) -> {
            if (node instanceof AbstractMountTreeDiffFileTreeNode) {
                return ((AbstractMountTreeDiffFileTreeNode) node).getSourceVersion().equals(ourVersion);
            }
            return false;
        };
        Predicate<TreeNode> forTheirChanges = (TreeNode node) -> {
            if (node instanceof AbstractMountTreeDiffFileTreeNode) {
                return ((AbstractMountTreeDiffFileTreeNode) node).getSourceVersion().equals(theirVersion);
            }
            return false;
        };
        JMenuItem openOurVersionMenuItem = new JMenuItem(ResourceAction.of("vcs.compare.open_our_version", event -> {
            RepositoryPath repositoryPath = RepositoryPath.of(differenceTree.getSelectedChange().getRepositoryPath(), ourVersion.getId());
            if (Files.exists(repositoryPath)) {
                OpenEntryAction.open(Entries.getEntry(repositoryPath));
            } else {
                SwingTools.showVerySimpleErrorMessage("vcs.compare.path_not_found_in_version", repositoryPath.getFileName().toString(),
                        repositoryPath.toString(false), ourVersion);
            }
        }));
        JMenuItem openTheirVersionMenuItem = new JMenuItem(ResourceAction.of("vcs.compare.open_their_version", event -> {
            RepositoryPath repositoryPath = RepositoryPath.of(differenceTree.getSelectedChange().getRepositoryPath(), theirVersion.getId());
            if (Files.exists(repositoryPath)) {
                OpenEntryAction.open(Entries.getEntry(repositoryPath));
            } else {
                SwingTools.showVerySimpleErrorMessage("vcs.compare.path_not_found_in_version", repositoryPath.getFileName().toString(),
                        repositoryPath.toString(false), theirVersion);
            }
        }));
        mountTreeDiffPanel.addPopupMenuItem(forOurChanges, openOurVersionMenuItem, openTheirVersionMenuItem);
        mountTreeDiffPanel.addPopupMenuItem(forTheirChanges, openTheirVersionMenuItem, openOurVersionMenuItem);

        mountTreeDiffPanel.addPopupMenuItem(MountTreeDiffPanel.SHOW_FOR_UNCOMMITTED_NODES,
                new JMenuItem(ResourceAction.of("vcs.merge.remove_additional", event -> {
                    AbstractMountTreeDiffTreeNode additionalChange = differenceTree.getSelectedChange();
                    resolvingController.removeAdditionalPath(additionalChange.getRepositoryPath());
                })));

        JPanel containingPane = new JPanel(new BorderLayout());
        containingPane.add(mountTreeDiffPanel, BorderLayout.CENTER);
        ValidatableTextField messageTextField = new ValidatableTextField(value -> value != null && !value.isBlank(),
                value -> "Merge commit message should not be empty", "commit_message");
        JButton mergeButton = new JButton(ResourceAction.of("vcs.continue_merge", e -> resolvingController.merge()));
        JButton cancelButton = new JButton(ResourceAction.of("vcs.cancel_merge", e -> resolvingController.abort()));
        mergeButton.setEnabled(false);
        JLabel conflictsLabel = new JLabel(
                "<html><font color='#FF64321E'><i>(" + resolvingController.getRemainingConflicts().size() + " unresolved conflicts)</i></font></html>");
        resolvingController.addChangeListener(e -> {
            mergeButton.setEnabled(resolvingController.isDone());
            SwingUtilities.invokeLater(() -> {
                differenceTree.setUncommitted(resolvingController.getAdditionalPaths());
                Set<RepositoryPath> remainingConflictPaths = resolvingController.getRemainingConflicts()
                        .stream()
                        .map(MergeConflict::getConflictRepositoryPath)
                        .collect(Collectors.toSet());
                for (RepositoryPath conflictPath : conflictPaths) {
                    if (resolvingController.isRepositoryPathResolved(conflictPath)) {
                        MergeConflictDecision decisionFor = resolvingController.getDecisionFor(conflictPath);
                        differenceTree.addResolvedConflict(conflictPath, decisionFor.getDescription());
                    }
                }
                if (remainingConflictPaths.isEmpty()) {
                    conflictsLabel.setText("<html><i>(all conflicts are resolved)</i></html>");
                } else {
                    conflictsLabel.setText("<html><font color='#FF64321E'><i>(" + remainingConflictPaths.size() + " unresolved conflicts)</i></font></html>");
                }
            });
        });
        messageTextField.addObserver((observable, messageValid) -> {
            if (messageValid) {
                resolvingController.setMergeMessage(messageTextField.getText());
            } else {
                resolvingController.setMergeMessage(null);
            }
        }, true);

        JPanel controlPanel = new JPanel(new BorderLayout());
        controlPanel.add(messageTextField, BorderLayout.CENTER);
        controlPanel.add(ButtonDialog.makeButtonPanel(conflictsLabel, mergeButton, cancelButton), BorderLayout.SOUTH);
        containingPane.add(controlPanel, BorderLayout.SOUTH);

        return containingPane;
    }

    @Override
    public DockKey getDockKey() {
        return key;
    }

    @Override
    public Component getComponent() {
        return container;
    }
}
