package com.owc.singularity.studio.gui.vcs;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.ocpsoft.prettytime.PrettyTime;

import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MountTreeDifference;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.vcs.model.*;

public class MountTreeDiffPanel extends JPanel {

    private static final PrettyTime timeFormatter = new PrettyTime();
    public static final Predicate<TreeNode> SHOW_FOR_ALL_NODES = (TreeNode ignored) -> true;
    public static final Predicate<TreeNode> SHOW_FOR_ALL_DIFFERENCES = (TreeNode node) -> node instanceof AbstractMountTreeDiffFileTreeNode;
    public static final Predicate<TreeNode> SHOW_FOR_CONFLICT_NODES = (TreeNode node) -> node instanceof MountTreeDiffMergeConflictTreeNode;
    public static final Predicate<TreeNode> SHOW_FOR_UNCOMMITTED_NODES = (TreeNode node) -> {
        if (node instanceof AbstractMountTreeDiffTreeNode) {
            return ((AbstractMountTreeDiffTreeNode) node).getSourceVersion() == null;
        }
        return false;
    };

    private final MountTreeDiffTree tree;

    private final Map<Predicate<TreeNode>, List<JMenuItem>> popupMenuItems;
    private final RepositoryPath mountPath;
    private final EntryVersion leftVersion, rightVersion;

    public MountTreeDiffPanel(RepositoryPath mountPath, MountTreeDifference difference, Collection<RepositoryPath> uncommittedPaths) {
        super();
        this.mountPath = mountPath;
        this.leftVersion = difference.getLeft();
        this.rightVersion = difference.getRight();
        this.tree = new MountTreeDiffTree(mountPath, difference, uncommittedPaths);
        this.popupMenuItems = new LinkedHashMap<>();
        initUI();
    }

    public MountTreeDiffPanel(RepositoryPath mountPath, EntryVersion firstVersion, EntryVersion secondVersion, Collection<RepositoryPath> conflictPaths,
            Collection<RepositoryPath> uncommittedPaths) {
        super();
        this.mountPath = mountPath;
        this.leftVersion = firstVersion;
        this.rightVersion = secondVersion;
        this.tree = new MountTreeDiffTree(mountPath, firstVersion, secondVersion, conflictPaths, uncommittedPaths);
        this.popupMenuItems = new LinkedHashMap<>();
        initUI();
    }

    private void initUI() {
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addMouseListener(new CustomMouseAdapter(tree));
        setLayout(new BorderLayout());
        JLabel infoLabel = new ResourceLabel("vcs.update.comparing_two_versions", RepositoryManager.DEFAULT_FILESYSTEM_ROOT.relativize(mountPath).toString(),
                "<font color='#008F1D'>" + formatEntryVersion(leftVersion) + "</font> <b>(Ours)</b>",
                "<font color='#0080A3'>" + formatEntryVersion(rightVersion) + "</font> <b>(Theirs)</b>");
        add(infoLabel, BorderLayout.NORTH);
        ExtendedJScrollPane treeScrollPane = new ExtendedJScrollPane(tree);
        add(treeScrollPane, BorderLayout.CENTER);
    }

    private static String formatEntryVersion(EntryVersion version) {
        return "(" + version.getShortId() + " - " + version.getAuthor() + " - " + timeFormatter.format(version.getCommitTimestamp()) + ")";
    }

    public MountTreeDiffTree getTree() {
        return tree;
    }

    private JPopupMenu createRepositoryContextMenuPopup(TreeNode treeNode) {
        JPopupMenu menu = new JPopupMenu();
        for (Predicate<TreeNode> predicate : popupMenuItems.keySet()) {
            if (predicate.test(treeNode)) {
                for (JMenuItem menuItem : popupMenuItems.get(predicate)) {
                    menu.add(menuItem);
                }
            }
        }
        return menu;
    }

    public void addPopupMenuItem(Predicate<TreeNode> showFor, JMenuItem... items) {
        if (showFor == null) {
            showFor = SHOW_FOR_ALL_NODES;
        }
        List<JMenuItem> menuItems = popupMenuItems.computeIfAbsent(showFor, ignored -> new LinkedList<>());
        menuItems.addAll(Arrays.asList(items));
    }

    private class CustomMouseAdapter extends MouseAdapter {

        private final MountTreeDiffTree tree;

        public CustomMouseAdapter(MountTreeDiffTree tree) {
            this.tree = tree;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());
            if (treePath == null) {
                tree.clearSelection();
                return;
            }
            if (e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
                tree.setSelectionPath(treePath);
                Object lastPathComponent = treePath.getLastPathComponent();
                if (lastPathComponent instanceof TreeNode) {
                    TreeNode node = (TreeNode) lastPathComponent;
                    Optional<Map.Entry<Predicate<TreeNode>, List<JMenuItem>>> firstMenu = popupMenuItems.entrySet()
                            .stream()
                            .filter(entry -> entry.getKey().test(node))
                            .findFirst();
                    if (firstMenu.isEmpty())
                        return;
                    // by double click run the first menu item action
                    List<JMenuItem> action = firstMenu.get().getValue();
                    if (action == null || action.isEmpty())
                        return;
                    JMenuItem menuItem = action.iterator().next();
                    menuItem.doClick();
                    e.consume();
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                TreePath treePath = tree.getPathForLocation(e.getX(), e.getY());
                if (treePath == null) {
                    tree.clearSelection();
                    return;
                }
                tree.setSelectionPath(treePath);
                Object lastPathComponent = treePath.getLastPathComponent();
                if (lastPathComponent instanceof TreeNode) {
                    JPopupMenu popup = createRepositoryContextMenuPopup((TreeNode) lastPathComponent);
                    popup.show(tree, e.getX(), e.getY());
                    e.consume();
                }
            }
        }
    }
}
