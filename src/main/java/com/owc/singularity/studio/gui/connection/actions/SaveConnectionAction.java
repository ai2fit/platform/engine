/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.connection.actions;


import java.awt.event.ActionEvent;
import java.io.IOException;

import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.connection.ConnectionParameterDefinition;
import com.owc.singularity.repository.connection.ConnectionParameters;
import com.owc.singularity.repository.connection.ConnectionParametersException;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.connection.ConnectionEditDialog;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * Action for saving a connection
 *
 * @author Jonas Wilms-Pfau
 * @since 9.3
 */
public class SaveConnectionAction extends ResourceAction {

    private static final long serialVersionUID = 763971075711127990L;

    /**
     * The progress thread id consists of {@value #PROGRESS_THREAD_ID_PREFIX} followed by the
     * connection location.
     */
    public static final String PROGRESS_THREAD_ID_PREFIX = "saving_connection";

    private final ConnectionEditDialog parent;
    private final ConnectionParameterDefinition<?> definition;
    private final transient RepositoryPath location;


    public SaveConnectionAction(String i18nKey, ConnectionEditDialog parent, ConnectionParameterDefinition<?> definition, RepositoryPath location) {
        super(i18nKey);
        this.parent = parent;
        this.definition = definition;
        this.location = location;
    }

    @Override
    protected void loggedActionPerformed(ActionEvent e) {
        if (!parent.assertAllRequiredParametersAreSet()) {
            final boolean continueSaving = SwingTools.invokeAndWaitWithResult(() -> {
                ConfirmDialog dialog = new ConfirmDialog(parent, "save_invalid_connection", ConfirmDialog.OK_CANCEL_OPTION, false, "connection.save_anyway",
                        "connection.back_to_editing", null, null, new Object[] { "One or more required parameters are not set" });
                dialog.setVisible(true);
                return dialog.wasConfirmed();
            });
            if (!continueSaving)
                return;
        }
        ConnectionParameters result = null;
        try {
            result = parent.extractConnectionParametersFromCurrentParameters();
        } catch (ConnectionParametersException e1) {
            // TODO how to display error?
        }
        final ConnectionParameters connection = result;
        if (connection == null) {
            return;
        }
        parent.setVisible(false);
        ProgressThread progressThread = new ProgressThread(PROGRESS_THREAD_ID_PREFIX, false, location.toString()) {

            @Override
            public void run() {
                ConnectionParametersIOObject ioobject = new ConnectionParametersIOObject(connection);
                try {
                    Entries.write(location, MetaData.forIOObject(ioobject), ioobject);
                    SwingTools.invokeLater(parent::dispose);
                } catch (IOException e) {
                    SwingTools.invokeLater(() -> parent.setVisible(true));
                    SwingTools.showSimpleErrorMessage(parent, "saving_connection_failed", e, connection, location, e.getMessage());
                }
            }

            @Override
            public String getID() {
                return super.getID() + location;
            }
        };
        progressThread.setIndeterminate(true);
        progressThread.start();
    }

}
