/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.perspective;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.plaf.LayerUI;

import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * The perspectives panel bar has a gray background and displays the perspectives as toggle buttons.
 * 
 * @author Nils Woehler
 * 
 */
public class PerspectivesPanelBar extends JPanel {

    private static final long serialVersionUID = 1L;

    private static class PerspectivesLayerUI extends LayerUI<JPanel> {

        private static final long serialVersionUID = 1L;

        private final JPanel contentPanel;

        public PerspectivesLayerUI(JPanel contentPanel) {
            this.contentPanel = contentPanel;
        }

        @Override
        public void paint(Graphics g, JComponent c) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setColor(getBackgroundColor());
            Rectangle rec = contentPanel.getBounds();
            g2.fillRoundRect((int) rec.getX(), (int) rec.getY(), rec.width + 15, rec.height, 25, 25);
            g2.setColor(SwingTools.SINGULARITY_ORANGE);
            g2.drawRoundRect((int) rec.getX(), (int) rec.getY(), rec.width + 15, rec.height - 1, 25, 25);
            g2.dispose();
            super.paint(g, c);
        }

    }

    private static Color getBackgroundColor() {
        Color lightGray = SwingTools.SINGULARITY_LIGHT_GRAY;
        return new Color(lightGray.getRed(), lightGray.getGreen(), lightGray.getBlue(), 50);
    }
}
