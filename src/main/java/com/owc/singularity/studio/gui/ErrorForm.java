package com.owc.singularity.studio.gui;

import java.awt.*;

import javax.swing.*;

import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.StackTraceList;
import com.owc.singularity.studio.gui.tools.components.FixedWidthEditorPane;

public class ErrorForm<K> implements Form<K> {

    private final String message;
    private final Throwable throwable;
    private final JPanel component;

    public ErrorForm(String message, Throwable throwable) {
        this.message = message;
        this.throwable = throwable;
        component = new JPanel(new BorderLayout(0, 20));
        // component.setBorder(BorderFactory.createEmptyBorder(12, 16, 16, 4));
        int width = 420;
        FixedWidthEditorPane infoTextLabel = new FixedWidthEditorPane(width, message);
        // set the background as for infoPanel such that infoTextLabel looks like a JLabel
        infoTextLabel.setBackground(component.getBackground());
        component.add(infoTextLabel, BorderLayout.NORTH);

        StackTraceList stl = new StackTraceList(throwable);
        JScrollPane detailPane = new ExtendedJScrollPane(stl);
        // detailPane.setPreferredSize(new Dimension(component.getWidth(), 200));
        detailPane.setBorder(null);
        component.add(detailPane, BorderLayout.CENTER);
    }

    @Override
    public K getData() {
        return null;
    }

    @Override
    public boolean isReadyToSubmit() {
        return false;
    }

    @Override
    public void addInputChangeListener(InputChangeListener listener) {

    }

    @Override
    public void removeInputChangeListener(InputChangeListener listener) {

    }

    @Override
    public JComponent getComponent() {
        return component;
    }
}
