package com.owc.singularity.studio.gui.vcs.conflict.model;

import com.owc.singularity.engine.pipeline.AbstractPipeline;

public class CompareProcessesModel {

    private final AbstractPipeline left;
    private final AbstractPipeline right;

    public CompareProcessesModel(AbstractPipeline left, AbstractPipeline right) {
        this.left = left;
        this.right = right;
    }

    public AbstractPipeline getLeftProcess() {
        return left;
    }

    public AbstractPipeline getRightProcess() {
        return right;
    }
}
