package com.owc.singularity.studio.gui.repository.actions.context;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.*;
import com.owc.singularity.repository.exception.CannotUpdateMountException;
import com.owc.singularity.repository.vcs.MountTreeDifference;
import com.owc.singularity.studio.gui.repository.OperationProgressMonitorAdapter;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNodeInformation;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.vcs.MountTreeDiffTree;
import com.owc.singularity.studio.gui.vcs.conflict.InteractiveMergeConflictResolver;

public class UpdateMountAction extends AbstractRepositoryContextAction<Object> {

    private static final Collector<CharSequence, ?, String> pathJoiningCollector = Collectors.joining("\n- ", "- ", "");
    private static boolean inAction = false;

    public UpdateMountAction(RepositoryTree tree) {
        super(tree, true, false, null, false, true, "vcs.update_mount");
    }

    public boolean enable() {
        if (inAction) {
            setEnabled(false);
            return false;
        }
        List<RepositoryTreeNode> entries = tree.getSelectedEntries();
        if (entries.size() != 1) {
            setEnabled(false);
            return false;
        } else {
            RepositoryTreeNode node = entries.iterator().next();
            RepositoryTreeNodeInformation nodeInformation = node.getInformation();
            if (nodeInformation.getMount().hasRemote() && nodeInformation.getRepositoryPath().equals(nodeInformation.getMount().getMountPath())) {
                setEnabled(true);
                return true;
            } else {
                setEnabled(false);
                return false;
            }
        }
    }

    @Override
    protected Object configureAction(List<RepositoryTreeNode> entries) {
        return new Object();
    }

    @Override
    public void executeAction(RepositoryPath path, Object config, ProgressListener progressListener) {
        inAction = true;
        try {
            RepositoryFileSystem fileSystem = RepositoryManager.getFileSystem();
            RepositoryMount mount = fileSystem.getMount(path);
            if (!mount.hasRemote())
                throw new RuntimeException("The mount does not have a remote and does not support update operation");
            InteractiveMergeConflictResolver conflictResolver = new InteractiveMergeConflictResolver();
            try (OperationProgressMonitorAdapter ignored = new OperationProgressMonitorAdapter(fileSystem, progressListener)) {

                MountTreeDifference difference = mount.update(conflictResolver);
                if (conflictResolver.isAborted()) {
                    return;
                }
                if (conflictResolver.isError()) {
                    SwingTools.showSimpleErrorMessage("vcs.merge.fail", conflictResolver.getError());
                    return;
                }
                if (difference.getBase() != null && difference.getLeft().equals(difference.getRight())) {
                    // nothing changed
                    SwingTools.showMessageDialog("vcs.update.success_no_change");
                    return;
                }
                MountTreeDiffTree mountTreeDiffTree = new MountTreeDiffTree(mount.getMountPath(), difference, Collections.emptyList());
                ExtendedJScrollPane treeScrollPane = new ExtendedJScrollPane(mountTreeDiffTree);
                SwingTools.showMessageDialog("vcs.update.success", treeScrollPane, difference.getLeft(), difference.getRight());
            }
        } catch (CannotUpdateMountException expected) {
            SwingTools.showVerySimpleErrorMessage("vcs.update.fail", expected.getMessage(),
                    expected.getRelatedRepositoryPaths().stream().map(fail -> pathToString(path, fail)).collect(pathJoiningCollector));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            inAction = false;
        }
    }

    private static String pathToString(RepositoryPath mountPath, RepositoryPath repositoryPath) {
        return mountPath.relativize(repositoryPath).toString(false);
    }
}
