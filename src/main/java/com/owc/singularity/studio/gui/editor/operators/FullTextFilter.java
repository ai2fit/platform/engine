package com.owc.singularity.studio.gui.editor.operators;

public class FullTextFilter implements OperatorFilter {

    private String contains;


    public FullTextFilter(String contains) {
        this.contains = contains;
    }

    @Override
    public boolean matches(String string) {
        return string.contains(contains);
    }

}
