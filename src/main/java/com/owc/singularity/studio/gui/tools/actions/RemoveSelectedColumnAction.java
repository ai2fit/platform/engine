package com.owc.singularity.studio.gui.tools.actions;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.Serial;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.table.TableModel;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.studio.gui.LoggedAbstractAction;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.viewer.ExampleSetViewerTable;


/**
 * This action removed the selected columns from the view.
 *
 * @author ALexander Mahler
 */
public class RemoveSelectedColumnAction extends LoggedAbstractAction {

    @Serial
    private static final long serialVersionUID = 5408896556834440633L;
    private static final String ICON_NAME = "delete.png";

    private final ExampleSetViewerTable table;

    public RemoveSelectedColumnAction(ExampleSetViewerTable table, IconSize size) {
        super("Remove Columns", SwingTools.createIcon(size.getSize() + "/" + ICON_NAME));
        this.table = table;
        putValue(SHORT_DESCRIPTION, "Remove the selected columns from view");
        putValue(MNEMONIC_KEY, KeyEvent.VK_D);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        List<Attribute> selectedAttributes = this.table.getSelectedAttributes();
        TableModel model = this.table.getModel();
        int[] selectedColumns = this.table.getSelectedColumns();
        if (selectedColumns.length != 0 && model != null) {
            Set<String> unselectedNames = new HashSet<>();
            for (int index : selectedColumns) {
                unselectedNames.add(model.getColumnName(index));
            }
            this.table.setSelectedAttributes(selectedAttributes.stream().filter(attribute -> !unselectedNames.contains(attribute.getName())).toList());
        }
    }

}
