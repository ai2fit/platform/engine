package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionService;
import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.dialogs.InputDialog;

public class SetParallelityAction extends ResourceAction {

    private static final long serialVersionUID = 1L;

    public SetParallelityAction() {
        super("toolkit.set_parallelity");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConcurrentExecutionService service = ConcurrentExecutionServiceProvider.getService();

        InputDialog dialog = new InputDialog("set_parallelity", "" + service.getRecommendedConcurrency());
        dialog.setVisible(true);

        if (dialog.wasConfirmed()) {
            try {
                int newBaseParallelity = Integer.parseInt(dialog.getInputText());
                // Local
                // BackgroundExecutionService.setBaseParallelity(newBaseParallelity);
            } catch (NumberFormatException ex) {
            }
        }
    }

}
