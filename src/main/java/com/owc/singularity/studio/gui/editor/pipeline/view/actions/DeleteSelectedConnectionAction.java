/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.view.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;
import com.owc.singularity.studio.gui.editor.action.ProcessActions;
import com.owc.singularity.studio.gui.editor.action.ProcessChangingAction;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;


/**
 * Deletes either a selected connection or {@link Operator}.
 *
 * @author Simon Fischer
 * @since 6.4.0
 *
 */
public class DeleteSelectedConnectionAction extends ProcessChangingAction {

    private static final long serialVersionUID = 1L;

    public DeleteSelectedConnectionAction() {
        super("delete_selected_connection");
    }

    @Override
    public void loggedActionPerformed(final ActionEvent e) {
        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
        if (mainProcessPanel == null)
            return;
        ProcessRendererModel model = mainProcessPanel.getProcessEditor().getModel();
        if (model.getSelectedConnectionSource() != null) {
            if (model.getSelectedConnectionSource().isConnected()) {
                model.getSelectedConnectionSource().disconnect();
            }
            model.setSelectedConnectionSource(null);
            model.fireMiscChanged();
        } else {
            // try to delete selected annotation
            if (model.getSelectedOperators().size() == 1 && model.getSelectedOperators().get(0).equals(model.getDisplayedChain())) {
                if (mainProcessPanel.getAnnotationsVisualizer().isActive()) {
                    mainProcessPanel.getAnnotationsVisualizer().deleteSelected();
                }
            }
            for (Operator selectedOperator : model.getSelectedOperators()) {
                // don't delete if we have selected surrounding operator in subprocess so the whole
                // subprocess would get deleted
                if (selectedOperator.equals(model.getDisplayedChain())) {
                    return;
                }
            }
            ProcessActions.DELETE_OPERATOR_ACTION.actionPerformed(e);
            model.setConnectingPortSource(null);
            model.fireMiscChanged();
        }
    }
}
