package com.owc.singularity.studio.gui.repository.event;

public enum RepositoryTreeUpdateType {
    Insert, Remove, Update
}
