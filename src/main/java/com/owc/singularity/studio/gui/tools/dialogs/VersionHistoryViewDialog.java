/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.dialogs;


import java.awt.*;

import javax.swing.*;

import com.owc.singularity.studio.gui.tools.Icons;


/**
 *
 * @author Tobias Malbrecht
 */
public class VersionHistoryViewDialog extends ButtonDialog {

    /**
     * Displays a dialog with the given result component.
     *
     * @param owner
     *            the owner where the dialog is shown in
     * @param i18nKey
     *            the i18n key
     * @param results
     *            the result component to display
     * @param i18nArgs
     *            the i18n arguments
     */
    public VersionHistoryViewDialog(Window owner, String i18nKey, JComponent results, ModalityType modalityType, Object... i18nArgs) {
        super(owner, "versions." + i18nKey, modalityType, i18nArgs);
        results.setBorder(createBorder());
        layoutDefault(results, ButtonDialog.LARGE, makeCloseButton());
    }

    @Override
    protected Icon getInfoIcon() {
        return Icons.fromName("history2.png").getIcon48x48();
    }
}
