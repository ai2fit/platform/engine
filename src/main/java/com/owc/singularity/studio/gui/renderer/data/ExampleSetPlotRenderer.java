/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.renderer.data;


import java.util.LinkedHashMap;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.engine.visualization.datatable.ExampleSetDataTable;
import com.owc.singularity.studio.gui.plotter.Plotter;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationModel;
import com.owc.singularity.studio.gui.renderer.AbstractDataTablePlotterRenderer;


/**
 * A renderer for the plot view of example sets.
 *
 * @author Ingo Mierswa
 * @deprecated since 9.2.0
 */
@Deprecated
public class ExampleSetPlotRenderer extends AbstractDataTablePlotterRenderer {

    @Override
    public LinkedHashMap<String, Class<? extends Plotter>> getPlotterSelection() {
        return PlotterConfigurationModel.DATA_SET_PLOTTER_SELECTION;
    }

    /**
     * This method is used to create a {@link DataTable} from this example set. The default
     * implementation returns an instance of {@link ExampleSetDataTable}. The given IOContainer is
     * used to check if there are compatible attribute weights which would used as column weights of
     * the returned table. Subclasses might want to override this method in order to allow for other
     * data tables.
     */
    @Override
    public DataTable getDataTable(Object renderable) {
        ExampleSet exampleSet = (ExampleSet) renderable;

        return new ExampleSetDataTable(exampleSet, false);
    }
}
