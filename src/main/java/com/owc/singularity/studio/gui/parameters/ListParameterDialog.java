/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters;


import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeList;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * A Dialog displaying a {@link ListParameterTable}. This can be used to add new values to the
 * parameter list or change current values. Removal of values is also supported.
 *
 * @see com.owc.singularity.studio.gui.parameters.ListParameterTable
 * @author Ingo Mierswa, Simon Fischer, Tobias Malbrecht, Nils Woehler, Marius Helf
 */
public class ListParameterDialog extends ParameterDialog {

    private static final long serialVersionUID = 1876607848416333390L;

    private boolean ok = false;

    private final ListParameterTable2 listPropertyTable;

    private final List<String[]> parameterList;

    public ListParameterDialog(final ParameterTypeList type, List<String[]> parameterList, Operator operator) {
        super(type, "list");
        this.parameterList = parameterList;
        listPropertyTable = new ListParameterTable2(type, parameterList, operator);
        if (listPropertyTable.isEmpty()) {
            listPropertyTable.addRow();
        }
        JScrollPane scrollPane = new ExtendedJScrollPane(listPropertyTable);
        scrollPane.setBorder(null);
        layoutDefault(scrollPane, NORMAL, new JButton(new ResourceAction("list.add_row") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                listPropertyTable.addRow();
            }
        }), new JButton(new ResourceAction("list.remove_row") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                listPropertyTable.removeSelected();
            }
        }), makeOkButton("list_property_dialog_apply"), makeCancelButton());

        listPropertyTable.requestFocusForLastEditableCell();
    }

    @Override
    protected void ok() {
        ok = true;
        listPropertyTable.stopEditing();

        // this displays a warning message if list entries contain empty values
        /*
         * List<String[]> list = ((ListTableModel) listPropertyTable.getModel()).getParameterList();
         * 
         * if (checkTableEntries(list)) { listPropertyTable.storeParameterList(parameterList);
         * dispose(); } else {
         * 
         * ConfirmDialog dialog = new ConfirmDialog(this, "empty_list_entry",
         * ConfirmDialog.OK_CANCEL_OPTION, false, "empty_list_entry_continue",
         * "empty_list_entry_dismiss", null, null, new Object[0]); dialog.setVisible(true); int
         * answer = dialog.getReturnOption(); if (answer == ConfirmDialog.CANCEL_OPTION) {
         * listPropertyTable.storeParameterList(parameterList); dispose(); } else { // do nothing
         * and just close dialog window }
         * 
         * }
         */
        listPropertyTable.storeParameterList(parameterList);
        dispose();
    }

    @Override
    protected void cancel() {
        ok = false;
        dispose();
    }

    @Override
    public boolean isOk() {
        return ok;
    }

    private boolean checkTableEntries(List<String[]> parameterList) {

        for (String[] names : parameterList) {
            // check if there is no attribute name given but an expression
            if (names[0].isEmpty() || !names[1].isEmpty()) {
                return false;
            }

        }

        return true;
    }
}
