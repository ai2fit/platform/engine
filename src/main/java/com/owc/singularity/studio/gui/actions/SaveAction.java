/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.SwingUtilities;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.RecentLocationsManager;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.DecisionRememberingConfirmDialog;


/**
 * Start the corresponding action.
 *
 * @author Ingo Mierswa, Marco Boeck
 */
public class SaveAction extends ResourceAction {

    private static final long serialVersionUID = -2226200404990114956L;

    /** key of the progress thread to save */
    public static final String SAVE_PROGRESS_KEY = "save_action";

    public SaveAction() {
        super("save");
        // this action is managed manually rather than over conditions
        setCondition(Condition.EDIT_IN_PROGRESS, ConditionReaction.DONT_CARE);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        saveAsync(MainFrame.INSTANCE.getMainProcessPanel().getProcess());
    }

    /**
     * Saves the specified process to its {@link RepositoryPath}. If it has none, will open the
     * SaveAs dialog. <br/>
     * <strong>Note:</strong> This call executes in the calling thread. In other words, this would
     * block the GUI if called from the EDT!
     *
     * @param process
     *            the {@link AbstractPipeline} to save
     * @return true on success, false on failure
     */
    public static boolean save(final AbstractPipeline process) {
        return save(process, false);
    }

    /**
     * Saves the specified process to its {@link RepositoryPath}. If it has none, will open the
     * SaveAs dialog. <br/>
     * <strong>Note:</strong> This call executes in the calling thread. In other words, this would
     * block the GUI if called from the EDT!
     *
     * @param process
     *            the {@link AbstractPipeline} to save
     * @param refreshProcessMetaData
     *            if {@code true}, the meta data of the process will be recalculated after
     *            successful save
     * @return true on success, false on failure
     * @since 8.2
     */
    public static boolean save(final AbstractPipeline process, final boolean refreshProcessMetaData) {

        if (process.hasSaveDestination()) {
            if (confirmOverwriteWithNewVersion(process)) {
                // user wants to save
                // disable save action
                MainFrame.INSTANCE.SAVE_ACTION.setEnabled(false);
                boolean successful = true;
                try {
                    MainFrame.INSTANCE.getMainProcessPanel().saveProcessAt(process.getPath());
                    // check if process has really been saved or user has pressed
                    // cancel in saveAs dialog
                    if (process.hasSaveDestination()) {

                        // after successful save, if desired, force recalculation of meta data
                        if (refreshProcessMetaData) {
                            process.getRootOperator().transformMetaData();
                        }
                    }
                } catch (IOException e) {
                    successful = false;
                    SwingTools.showSimpleErrorMessage("cannot_save_process", e, process.getPath(), e.getMessage());
                    // something went wrong, enable save action again
                    MainFrame.INSTANCE.SAVE_ACTION.setEnabled(true);
                } catch (Exception e) {
                    successful = false;
                    // something went wrong, enable save action again
                    MainFrame.INSTANCE.SAVE_ACTION.setEnabled(true);
                }
                return successful;
            } else {
                return false;
            }
        } else {
            // SaveAsAction.saveAs cannot be null since async=false
            // noinspection DataFlowIssue
            return SaveAsAction.saveAs(process, false);
        }
    }

    /**
     * Saves the specified process to its {@link RepositoryPath}. If it has none, will open the
     * SaveAs dialog. <br/>
     * <strong>Note:</strong> This call executes in a {@link ProgressThread} with the key
     * {@link #SAVE_PROGRESS_KEY}. In other words, this method can return immediately!
     *
     * @param process
     *            the {@link AbstractPipeline} to save
     */
    public static void saveAsync(final AbstractPipeline process) {
        saveAsync(process, false);
    }

    /**
     * Saves the specified process to its {@link RepositoryPath}. If it has none, will open the
     * SaveAs dialog. <br/>
     * <strong>Note:</strong> This call executes in a {@link ProgressThread} with the key
     * {@link #SAVE_PROGRESS_KEY}. In other words, this method can return immediately!
     *
     * @param process
     *            the {@link AbstractPipeline} to save
     * @param refreshProcessMetaData
     *            if {@code true}, the meta data of the process will be recalculated after
     *            successful save
     * @since 8.2
     */
    public static void saveAsync(final AbstractPipeline process, final boolean refreshProcessMetaData) {
        if (process.hasSaveDestination()) {
            if (!confirmOverwriteWithNewVersion(process)) {
                return;
            }
            // user wants to save
            // disable save action
            MainFrame.INSTANCE.SAVE_ACTION.setEnabled(false);
            // save in progressThread to execute asynchronously
            ProgressThread saveThread = new ProgressThread(SAVE_PROGRESS_KEY) {

                @Override
                public void run() {
                    try {
                        MainFrame.INSTANCE.getMainProcessPanel().saveProcessAt(process.getPath());

                        SwingUtilities.invokeLater(() -> {
                            RecentLocationsManager.addToRecentFilesAsMostRecent(process.getPath());
                        });

                        // after successful save, if desired, force recalculation of meta data
                        if (refreshProcessMetaData && process.hasSaveDestination()) {
                            process.getRootOperator().transformMetaData();
                        }
                    } catch (IOException ex) {
                        SwingTools.showSimpleErrorMessage("cannot_save_process", ex, process.getPath(), ex.getMessage());
                        // something went wrong, enable save action again
                        MainFrame.INSTANCE.SAVE_ACTION.setEnabled(true);
                    } catch (Exception e) {
                        // something went wrong, enable save action again
                        MainFrame.INSTANCE.SAVE_ACTION.setEnabled(true);
                    }
                }
            };
            // just in case always depent on ourself so we cannot execute multiple saves at the
            // same time
            saveThread.addDependency(SAVE_PROGRESS_KEY);
            saveThread.setIndeterminate(true);
            saveThread.setCancelable(false);
            saveThread.start();
        } else {
            SaveAsAction.saveAs(process);
        }
    }

    private static boolean confirmOverwriteWithNewVersion(AbstractPipeline process) {
        return !process.isProcessConverted()
                || DecisionRememberingConfirmDialog.confirmAction("save_over_with_new_version", "singularity.gui.saveover_new_version");
    }
}
