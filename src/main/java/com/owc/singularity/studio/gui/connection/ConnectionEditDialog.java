/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.connection;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.*;

import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.config.actions.ActionResult;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.connection.*;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.connection.actions.CancelEditingAction;
import com.owc.singularity.studio.gui.connection.actions.OpenEditConnectionAction;
import com.owc.singularity.studio.gui.connection.actions.SaveConnectionAction;
import com.owc.singularity.studio.gui.connection.components.ConnectionInformationPanel;
import com.owc.singularity.studio.gui.connection.components.TestConnectionPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ExtendedJTabbedPane;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.config.ConnectionParametersPanel;


public class ConnectionEditDialog extends JDialog {

    private static final Dimension DEFAULT_SIZE = new Dimension(790, 500);

    private final transient ConnectionParameters originalConnectionParameters;
    private final transient ConnectionParametersPanel connectionParametersPanel;
    private final transient ConnectionInformationPanel connectionInformationPanel;

    private final boolean editMode;
    private final transient TestConnectionPanel testConnectionPanel;

    private final boolean isTypeKnown;
    private final ConnectionParameterDefinition<ConnectionParameters> definition;

    // the mainGUI will be the ConnectionGUI implementation
    private final JTabbedPane mainGUI;

    /**
     * Opens the dialog in view or edit mode
     *
     * @param connectionParameters
     *            the connection holder
     * @param openInEditMode
     *            {@code true} for edit mode, {@code false} for view mode
     */
    public ConnectionEditDialog(RepositoryPath location, ConnectionParameters connectionParameters, boolean openInEditMode) {
        this(ApplicationFrame.getApplicationFrame(), location, connectionParameters, openInEditMode);
    }

    /**
     * Opens the dialog in view or edit mode
     *
     * @param owner
     *            the owner
     * @param connectionParameters
     *            the connection holder
     * @param openInEditMode
     *            {@code true} for edit mode, {@code false} for view mode
     */
    public ConnectionEditDialog(Window owner, RepositoryPath location, ConnectionParameters connectionParameters, boolean openInEditMode) {
        super(owner, getTitle(connectionParameters, openInEditMode), ModalityType.APPLICATION_MODAL);
        this.definition = ConnectionDefinitionRegistry.getInstance().getDefinitionOfParameters(connectionParameters);
        this.isTypeKnown = definition != null;
        this.editMode = openInEditMode && isTypeKnown;
        this.originalConnectionParameters = connectionParameters;
        this.setLayout(new GridBagLayout());
        setMinimumSize(DEFAULT_SIZE);
        setPreferredSize(DEFAULT_SIZE);


        GridBagConstraints gbc = new GridBagConstraints();
        // ----------BIG PANEL------------
        // Set injected Fields
        // [Test connection] [Create] [Cancel]
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(10, 10, 0, 10);
        mainGUI = new ExtendedJTabbedPane();
        connectionInformationPanel = new ConnectionInformationPanel(location, connectionParameters, editMode);
        SwingTools.setEnabledRecursive(connectionInformationPanel, editMode);
        mainGUI.addTab("Information", new ExtendedJScrollPane(connectionInformationPanel));
        this.connectionParametersPanel = new ConnectionParametersPanel(this,
                new ParameterDefinitionParameterHandler(definition).fillWith(connectionParameters).getParameters());
        SwingTools.setEnabledRecursive(connectionParametersPanel, editMode);
        mainGUI.addTab("Parameters", new ExtendedJScrollPane(connectionParametersPanel));
        mainGUI.setSelectedIndex(editMode ? 1 : 0);
        add(mainGUI, gbc);

        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weighty = 0;
        gbc.gridwidth = 1;
        gbc.insets = new Insets(18, 10, 10, 74);
        testConnectionPanel = new TestConnectionPanel(definition, connectionParametersPanel::getParameters, this::processTestResult);
        add(testConnectionPanel, gbc);

        gbc.insets = new Insets(0, 0, 10, 10);
        // Give save full with to make alignable to the east
        gbc.weightx = 0;
        gbc.anchor = GridBagConstraints.SOUTHEAST;
        if (!openInEditMode) {
            JButton editButton = new JButton(new OpenEditConnectionAction(this, location, connectionParameters));
            editButton.setEnabled(isTypeKnown);
            add(editButton, gbc);
        } else if (editMode) {
            add(new JButton(new SaveConnectionAction("save_connection", this, definition, location)), gbc);
        }
        gbc.insets = new Insets(0, 0, 10, 10);
        CancelEditingAction cancelAction = new CancelEditingAction(this, editMode ? "cancel_connection_edit" : "close_connection_edit",
                () -> editMode && currentParametersDifferFromOriginalConnectionParameters());
        add(new JButton(cancelAction), gbc);

        // check changes on close
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                cancelAction.actionPerformed(null);
            }

        });
        // close dialog with ESC
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "CLOSE");
        getRootPane().getActionMap().put("CLOSE", cancelAction);

        setLocationRelativeTo(getOwner());
        SwingUtilities.invokeLater(() -> {
            pack();
            revalidate();
            repaint();
        });
    }

    public ConnectionParametersPanel getConnectionParametersPanel() {
        return connectionParametersPanel;
    }

    public ConnectionInformationPanel getConnectionInformationPanel() {
        return connectionInformationPanel;
    }

    public ConnectionParameters getOriginalConnectionParameters() {
        return originalConnectionParameters;
    }

    private boolean currentParametersDifferFromOriginalConnectionParameters() {
        try {
            return !getOriginalConnectionParameters().equals(extractConnectionParametersFromCurrentParameters());
        } catch (ConnectionParametersException e) {
            return true;
        }
    }

    public ConnectionParameters extractConnectionParametersFromCurrentParameters() throws ConnectionParametersException {
        Parameters parameters = getConnectionParametersPanel().getParameters();
        Set<String> keys = definition.getParameterTypes().stream().map(ParameterType::getKey).collect(Collectors.toSet());
        Map<String, String> parametersMap = new HashMap<>(keys.size());
        for (String key : keys) {
            parametersMap.put(key, parameters.getParameterAsSpecified(key));
        }
        return definition.create(getOriginalConnectionParameters().getName(), parametersMap, getConnectionInformationPanel().getConnectionDescription(),
                getConnectionInformationPanel().getConnectionContactInformation());
    }


    /**
     * Verifies that all required parameters are set, displays a warning for parameters if necessary
     *
     * @return {@code true} if all required parameters are set
     */
    public boolean assertAllRequiredParametersAreSet() {
        List<ParameterType> types = definition.getParameterTypes();
        Parameters parameters = getConnectionParametersPanel().getParameters();
        boolean result = true;
        for (ParameterType type : types) {
            try {
                parameters.getParameter(type.getKey());
            } catch (UndefinedParameterError e) {
                getConnectionParametersPanel().addWarningToParameterForKey(type.getKey());
                result = false;
            }
        }
        return result;
    }

    /**
     * Show a specific tab, to be found via the title. The title is already present resolved from
     * I18N so the result from {@link ConnectionI18N#getConnectionGUILabel(String, Object...)} is
     * required here. Fails quietly.
     *
     * @param name
     *            value of the I18N key for connection GUI label
     */
    public void showTab(String name) {
        if (name == null || mainGUI == null) {
            return;
        }

        JTabbedPane tabPane = mainGUI;
        for (int i = 0; i < tabPane.getTabCount(); i++) {
            if (name.equals(tabPane.getTitleAt(i))) {
                tabPane.setSelectedIndex(i);
                break;
            }
        }
    }

    /**
     * Show a specific tab, to be found via the index. Fails quietly.
     *
     * @param index
     *            the index of the tab to be selected
     */
    public void showTab(int index) {
        JTabbedPane tabPane = mainGUI;
        if (index < 0 || index >= tabPane.getTabCount()) {
            return;
        }
        tabPane.setSelectedIndex(index);
    }

    /**
     * Get the name of the currently displayed tab or {@code null}
     *
     * @return the localized title of the currently displayed tab
     */
    public String getCurrentTabTitle() {
        if (mainGUI != null) {
            return mainGUI.getTitleAt(mainGUI.getSelectedIndex());
        }
        return null;
    }

    /**
     * @return {@code true} if the dialog is in edit mode and the connection is editable
     */
    protected boolean isEditable() {
        return editMode && isTypeKnown;
    }

    private void processTestResult(ActionResult testResult) {
        testConnectionPanel.setTestResult(testResult);
    }

    /**
     * Returns the title for the given connection information
     *
     * @param holder
     *            the connection information
     * @param editMode
     *            if the dialog is opened in edit mode
     * @return the dialog title
     */
    private static String getTitle(ConnectionParameters holder, boolean editMode) {
        final String i18n;
        if (editMode)
            i18n = "edit_connection";
        else
            i18n = "view_connection";
        return ConnectionI18N.getConnectionGUILabel(i18n, holder.getName());
    }

}
