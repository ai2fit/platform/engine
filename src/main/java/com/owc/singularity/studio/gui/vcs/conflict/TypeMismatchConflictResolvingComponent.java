package com.owc.singularity.studio.gui.vcs.conflict;

import com.owc.singularity.repository.entry.EntryType;
import com.owc.singularity.repository.vcs.MergeConflict;

public class TypeMismatchConflictResolvingComponent extends SimpleConflictResolvingComponent {

    public TypeMismatchConflictResolvingComponent(MergeConflict conflict, EntryType ourType, EntryType theirType) {
        super(conflict, "vcs.merge.mismatch_merge", conflict.getConflictRepositoryPath().getFileName().toString(),
                conflict.getConflictRepositoryPath().toString(false), ourType.getTypeName(), conflict.getOurVersionId().substring(0, 8),
                theirType.getTypeName(), conflict.getTheirVersionId().substring(0, 8));
    }
}
