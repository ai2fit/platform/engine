/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.Logger;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.dialogs.AboutBox;


/**
 *
 * @author Simon Fischer
 */
public class AboutAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private static Logger log = LogService.getI18NLogger(AboutAction.class);

    public AboutAction() {
        super("about");
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        BufferedImage logoImage = null;
        URL url = Tools.getResource(ModuleService.getMajorClassLoader(), "splash/logo.png");

        if (url != null) {
            try {
                logoImage = ImageIO.read(url);
            } catch (IOException ex) {
                log.warn("com.owc.singularity.studio.gui.tools.SplashScreen.loading_images_for_splash_screen_error");
            }
        }
        new AboutBox(MainFrame.INSTANCE, SingularityEngine.getLongVersion(), logoImage).setVisible(true);
    }

}
