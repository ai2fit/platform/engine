/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.viewer;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.owc.singularity.engine.object.data.exampleset.*;


/**
 * The model for the {@link com.owc.singularity.studio.gui.viewer.ExampleSetViewer}.
 * 
 * @author Ingo Mierswa
 */
public class ExampleSetTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -3057324874942971672L;

    protected Attribute[] regularAttributes;

    protected Attribute[] specialAttributes;

    protected ExampleSet exampleSet;

    /**
     * This {@link Comparator} compares {@link Attribute}s according to their Role. It can be used
     * to sort the (special) Attributes of an {@link ExampleSet}.
     * <p>
     * It enforces the following order for Attributes: ID -> Label -> Prediction -> Confidence ->
     * Cluster -> Weight -> Other Special Attributes -> Other Regular Attributes
     */
    public static final Comparator<Attribute> SPECIAL_ATTRIBUTES_ROLE_COMPARATOR = new Comparator<>() {

        private final List<String> priorityList = Arrays.asList(Attributes.ID_NAME, Attributes.LABEL_NAME, Attributes.PREDICTION_NAME,
                Attributes.CONFIDENCE_NAME, Attributes.CLUSTER_NAME, Attributes.WEIGHT_NAME);

        @Override
        public int compare(Attribute a1, Attribute a2) {
            // the lower the priority, the earlier the attribute is being sorted
            // special attributes should come before regular attributes
            int priorityAttribute1 = a1.isSpecial() ? 1000 : 2000;
            int priorityAttribute2 = a2.isSpecial() ? 1000 : 2000;

            // if the attribute role is in the priority list, use special priority
            if (a1.isSpecial() && priorityList.contains(a1.getRole())) {
                priorityAttribute1 = priorityList.indexOf(a1.getRole());
            }
            if (a2.isSpecial() && priorityList.contains(a2.getRole())) {
                priorityAttribute2 = priorityList.indexOf(a2.getRole());
            }

            // special priority for roles that start with "confidence_"
            if (a1.isSpecial() && a1.getRole().startsWith(Attributes.CONFIDENCE_NAME + "_")) {
                priorityAttribute1 = priorityList.indexOf(Attributes.CONFIDENCE_NAME);
            }
            if (a2.isSpecial() && a2.getRole().startsWith(Attributes.CONFIDENCE_NAME + "_")) {
                priorityAttribute2 = priorityList.indexOf(Attributes.CONFIDENCE_NAME);
            }

            return priorityAttribute1 - priorityAttribute2;
        }

    };

    public ExampleSetTableModel(ExampleSet exampleSet) {
        this.exampleSet = exampleSet;
        this.regularAttributes = exampleSet.getAttributes().streamAttributes().filter(a -> !a.isSpecial()).toArray(Attribute[]::new);
        this.specialAttributes = exampleSet.getAttributes()
                .streamAttributes()
                .filter(Attribute::isSpecial)
                .sorted(SPECIAL_ATTRIBUTES_ROLE_COMPARATOR)
                .toArray(Attribute[]::new);
    }


    @Override
    public Class<?> getColumnClass(int column) {
        Class<?> type;
        if (column == 0) {
            type = Integer.class;
        } else {
            type = switch (getColumnAttribute(column).getValueType()) {
                case NOMINAL -> String.class;
                case NUMERIC -> Double.class;
                case TIMESTAMP -> Date.class;
            };
        }
        return type;
    }

    @Override
    public int getRowCount() {
        return getExampleSet().size();
    }

    /**
     * Returns the sum of the number of special attributes, the number of regular attributes and 1
     * for the row no. column.
     */
    @Override
    public int getColumnCount() {
        return this.specialAttributes.length + this.regularAttributes.length + 1;
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (column == 0) {
            return row + 1;
        } else {
            ExampleSet set = getExampleSet();
            if (set != null && row <= set.size()) {
                return getValueAsObject(set, row, getColumnAttribute(column));
            } else {
                return null;
            }
        }
    }

    @Override
    public String getColumnName(int column) {
        if (column < 0) {
            return "";
        }
        if (column == 0) {
            return "Row No.";
        } else {
            Attribute columnAttribute = getColumnAttribute(column);
            return columnAttribute.getName();
        }
    }

    /**
     * Returns the {@link Attribute} for the given column or <code>null</code> if this is no column
     * denoting an {@link Attribute} in the {@link ExampleSet}.
     * 
     * @param column
     * @return
     */
    public Attribute getColumnAttribute(int column) {
        if (column == 0) {
            return null;
        }
        int col = column - 1;
        Attribute attribute;
        if (col < specialAttributes.length) {
            attribute = specialAttributes[col];
        } else {
            attribute = regularAttributes[col - specialAttributes.length];
        }

        return attribute;
    }

    protected Object getValueAsObject(ExampleSet exampleSet, int row, Attribute attribute) {
        if (attribute == null)
            return null;
        switch (attribute.getValueType()) {
            case NOMINAL:
                return exampleSet.getNominalValue(row, attribute.getIndex());
            case NUMERIC:
                double numericValue = exampleSet.getNumericValue(row, attribute.getIndex());
                if (ValueType.isMissing(numericValue))
                    return null;
                return numericValue;
            case TIMESTAMP:
                long timestampValue = exampleSet.getTimestampValue(row, attribute.getIndex());
                if (ValueType.isMissing(timestampValue))
                    return null;
                return new Date(timestampValue);
        }
        return null;
    }

    public ExampleSet getExampleSet() {
        return exampleSet;
    }

}
