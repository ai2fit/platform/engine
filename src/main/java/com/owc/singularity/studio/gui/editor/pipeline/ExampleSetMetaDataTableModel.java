/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline;


import java.awt.Component;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.tools.AttributeGuiTools;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;


/**
 * This is a table model for the example set meta data.
 *
 * @author Simon Fischer
 */
public class ExampleSetMetaDataTableModel implements TableModel {

    private final List<AttributeMetaData> attributes;
    private final ExampleSetMetaData exampleSetMetaData;

    private static final String[] COLUMN_NAMES = { "Name", "Type", "Role" };
    private static final int NAME_COLUMN = 0;
    private static final int TYPE_COLUMN = 1;
    private static final int ROLE_COLUMN = 2;


    public ExampleSetMetaDataTableModel(ExampleSetMetaData emd) {
        super();
        this.exampleSetMetaData = emd;
        this.attributes = new LinkedList<>(emd.getAllAttributes());
    }

    /** Table is immutable. We ignore all listeners. */
    @Override
    public void addTableModelListener(TableModelListener l) {}

    @Override
    public void removeTableModelListener(TableModelListener l) {}

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public int getRowCount() {
        return attributes.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        AttributeMetaData amd = attributes.get(rowIndex);
        switch (columnIndex) {
            case NAME_COLUMN -> {
                return amd.getName();
            }
            case TYPE_COLUMN -> {
                return amd.getValueType().toString();
            }
            case ROLE_COLUMN -> {
                return exampleSetMetaData.getRole(amd);
            }
            default -> {
                return null;
            }
        }

    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Table is read only.");
    }

    public static Component makeTableForToolTip(ExampleSetMetaData emd) {
        ExtendedJTable table = new ExtendedJTable(new ExampleSetMetaDataTableModel(emd), true, true, true, false, false);
        table.getColumnModel().getColumn(TYPE_COLUMN).setCellRenderer(new DefaultTableCellRenderer() {

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (tableCellRendererComponent instanceof JLabel) {
                    JLabel renderer = (JLabel) tableCellRendererComponent;
                    ValueType type = null;
                    try {
                        type = ValueType.valueOf(String.valueOf(value));
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                    if (type != null) {
                        Icon icon;
                        if (type.equals(ValueType.NUMERIC)) {
                            icon = AttributeGuiTools.NUMERICAL_COLUMN_ICON;
                        } else if (type.equals(ValueType.NOMINAL)) {
                            icon = AttributeGuiTools.NOMINAL_COLUMN_ICON;
                        } else if (type.equals(ValueType.TIMESTAMP)) {
                            icon = AttributeGuiTools.DATE_COLUMN_ICON;
                        } else {
                            // attribute value type
                            icon = AttributeGuiTools.UNKNOWN_COLUMN_ICON;
                        }
                        renderer.setIcon(icon);
                    }
                }

                return tableCellRendererComponent;
            }

        });
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBorder(null);
        scrollPane.setPreferredSize(new Dimension(300, 200));
        scrollPane.setBackground(Colors.WHITE);
        scrollPane.getViewport().setBackground(Colors.WHITE);
        return scrollPane;
    }
}
