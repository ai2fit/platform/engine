package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.studio.gui.tools.ResourceAction;

public class StopExecutionAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private ConcurrentPipelineExecution execution;


    public StopExecutionAction(ConcurrentPipelineExecution execution) {
        super("toolkit.stop_execution");
        this.execution = execution;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConcurrentExecutionServiceProvider.getService().stopPipelineExecution(execution);
    }

}
