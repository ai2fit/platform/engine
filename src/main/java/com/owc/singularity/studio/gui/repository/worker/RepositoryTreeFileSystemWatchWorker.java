package com.owc.singularity.studio.gui.repository.worker;

import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryChangeListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.model.AbstractRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;

/**
 * A {@link SwingWorker} that watches for
 * {@link com.owc.singularity.repository.RepositoryFileSystem} changes and notify the
 * {@link AbstractRepositoryTreeModel tree model}.
 *
 * @author Hatem Hamad
 */
public class RepositoryTreeFileSystemWatchWorker extends SwingWorker<Void, TreeChange> implements RepositoryChangeListener {

    private static final Logger log = LogManager.getLogger(RepositoryTreeFileSystemWatchWorker.class.getName());
    private static final Tracer tracer = GlobalOpenTelemetry.getTracer("studio.repository");
    private final AbstractRepositoryTreeModel treeModel;
    private final BlockingQueue<WorkItem> workQueue = new LinkedBlockingQueue<>();

    public RepositoryTreeFileSystemWatchWorker(AbstractRepositoryTreeModel repositoryTreeModel) {
        this.treeModel = repositoryTreeModel;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Thread.currentThread().setName(Thread.currentThread().getName() + "/Watching for change in FileSystem");
        try {
            while (!isCancelled()) {
                WorkItem workItem = workQueue.take();
                TreeChange[] changes = workItem.getChanges(treeModel);
                if (changes != null && changes.length > 0)
                    publish(changes);
            }
        } catch (InterruptedException expected) {
            // when canceled while waiting on take()
        }
        return null;
    }

    @Override
    protected void process(List<TreeChange> chunks) {
        // runs on UI thread
        Span uiSpan = tracer.spanBuilder("edt-change-processing").setAttribute("chunks", chunks.size()).startSpan();
        Collection<TreeChange> accumulatedTreeChanges = chunks.stream().collect(TreeChange.combining());
        uiSpan.setAttribute("accumulatedChanges", accumulatedTreeChanges.size());
        for (TreeChange change : accumulatedTreeChanges) {
            synchronized (treeModel) {
                change.applyOn(treeModel);
            }
        }
        uiSpan.end();
    }


    @Override
    public void entryCreated(RepositoryPath path) {
        if (!path.isHead())
            return;
        addWorkItem(new EntryCreationWorkItem(path));
    }

    @Override
    public void entryModified(RepositoryPath path) {
        if (!path.isHead())
            return;
        addWorkItem(new EntryModificationWorkItem(path));
    }

    @Override
    public void entryDeleted(RepositoryPath path) {
        if (!path.isHead())
            return;
        addWorkItem(new EntryDeletionWorkItem(path));
    }

    private void addWorkItem(WorkItem item) {
        // noinspection ResultOfMethodCallIgnored
        workQueue.offer(item);
    }

    /**
     * Describes tasks that {@link RepositoryTreeFileSystemWatchWorker worker} will run in
     * background thread. These tasks will calculate {@link TreeChange changes} to be applied later
     * to the UI
     */
    private static abstract class WorkItem {

        private final RepositoryPath path;

        private WorkItem(RepositoryPath path) {
            this.path = path;
        }

        protected RepositoryPath getPath() {
            return path;
        }

        public abstract TreeChange[] getChanges(AbstractRepositoryTreeModel repositoryTreeModel);
    }

    private static class EntryCreationWorkItem extends WorkItem {

        private EntryCreationWorkItem(RepositoryPath path) {
            super(path);
        }

        @Override
        public TreeChange[] getChanges(AbstractRepositoryTreeModel repositoryTreeModel) {
            RepositoryPath path = getPath();
            if (repositoryTreeModel.containsNodeFor(path)) {
                // already registered, this operation is redundant
                log.atDebug().withMarker(LogService.MARKER_DEVELOPMENT).log("Cannot register new entry at '{}'. The entry is already registered.", path);
                return new TreeChange[0];
            }
            RepositoryTreeNode parentNode = repositoryTreeModel.getNodeFor(path.getParent());
            List<TreeChange> changes = new LinkedList<>();
            if (parentNode == null) {
                return new TreeChange[0];
            }
            RepositoryTreeNode newEntry = new RepositoryTreeNode(path);
            repositoryTreeModel.addNode(parentNode, newEntry);
            changes.add(new TreeChange.Insertion(parentNode, new RepositoryTreeNode[] { newEntry }));
            return changes.toArray(TreeChange[]::new);
        }
    }

    private static class EntryModificationWorkItem extends WorkItem {

        public EntryModificationWorkItem(RepositoryPath path) {
            super(path);
        }

        @Override
        public TreeChange[] getChanges(AbstractRepositoryTreeModel repositoryTreeModel) {
            RepositoryPath path = getPath();
            RepositoryTreeNode modifiedNode = repositoryTreeModel.getNodeFor(path);
            if (modifiedNode == null) {
                if (!Files.exists(path)) {
                    return new TreeChange[0];
                }
                // a possible side effect of querying Files API is that the
                // file may get loaded and therefore be already added
                modifiedNode = repositoryTreeModel.getNodeFor(path);
                if (modifiedNode == null) {
                    // file exists only virtually
                    log.atDebug().withMarker(LogService.MARKER_DEVELOPMENT).log("Modifying an entry at '{}' which was not registered.", path);
                    // try to handle it as a creation
                    return new EntryCreationWorkItem(path).getChanges(repositoryTreeModel);
                }
            }
            RepositoryTreeNode parentNode = modifiedNode.getParent();
            if (parentNode == null) {
                // this is the root node
                return new TreeChange[] { new TreeChange.Modification(modifiedNode, null) };
            }
            modifiedNode.reloadInformation();
            return new TreeChange[] { new TreeChange.Modification(parentNode, new RepositoryTreeNode[] { modifiedNode }) };
        }
    }

    private static class EntryDeletionWorkItem extends WorkItem {

        public EntryDeletionWorkItem(RepositoryPath path) {
            super(path);
        }

        @Override
        public TreeChange[] getChanges(AbstractRepositoryTreeModel repositoryTreeModel) {
            RepositoryPath path = getPath();
            RepositoryTreeNode removedNode = repositoryTreeModel.getNodeFor(path);
            if (removedNode == null) {
                // removing an entry at path which was not registered or already deleted
                return new TreeChange[0];
            }
            RepositoryTreeNode parentNode = removedNode.getParent();
            if (parentNode == null) {
                log.debug("Removing an entry at '" + path + "' which parent is not registered.");
                return new TreeChange[0];
            }
            int index = parentNode.getIndex(removedNode);
            repositoryTreeModel.removeNode(removedNode);
            return new TreeChange[] { new TreeChange.Removal(parentNode, new RepositoryTreeNode[] { removedNode }, new int[] { index }) };
        }
    }
}
