package com.owc.singularity.studio.gui.logging;

import static org.apache.logging.log4j.core.layout.HtmlLayout.DEFAULT_FONT_FAMILY;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.parameter.ParameterChangeListener;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.dialog.SearchDialog;
import com.owc.singularity.studio.gui.dialog.SearchableJTextComponent;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.components.composite.SplitButton;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class LogViewPanel extends JPanel implements Dockable {

    private static final long serialVersionUID = 1068233175420713262L;

    public static final int DEFAULT_NUMBER_OF_VISIBLE_LINES = 10_000;
    public static final String LOG_VIEWER_DOCK_KEY = "log_viewer";
    private static final DockKey DOCK_KEY = new ResourceDockKey(LOG_VIEWER_DOCK_KEY);

    static {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }


    private String currentLog;
    private int numberOfVisibleRows;

    private final JTextPane textArea = new JTextPane();
    private JLabel logNameLabel = new JLabel();
    private final LinkedHashMap<String, LogProvider> logProvidersMap = new LinkedHashMap<>();
    private final LinkedHashMap<String, ExtendedStyledDocument> logDocumentsMap = new LinkedHashMap<>();

    private final ResourceAction searchLogAction = ResourceAction.of("log_view.search_log", true, e -> performSearch());
    private final ResourceAction closeLogAction = ResourceAction.of("log_view.close_log", true, e -> closeLog(currentLog));
    private SplitButton selectLogButton = new SplitButton(ResourceAction.of("log_view.switch_log", true, e -> switchToNextLog(currentLog)), false);

    private SimpleAttributeSet documentStyleSet;


    public LogViewPanel() {
        super(new BorderLayout());

        documentStyleSet = new SimpleAttributeSet();
        textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));

        numberOfVisibleRows = getPropertyAsInt(StudioProperties.PROPERTY_SINGULARITY_GUI_LOGVIEWER_ROWLIMIT, DEFAULT_NUMBER_OF_VISIBLE_LINES);
        // listen for changes to the maxRows limit at runtime
        PropertyService.registerParameterChangeListener(new ParameterChangeListener() {

            @Override
            public void informParameterSaved() {
                // don't care
            }

            @Override
            public void informParameterChanged(String key, String value) {
                if (StudioProperties.PROPERTY_SINGULARITY_GUI_LOGVIEWER_ROWLIMIT.equals(key)) {
                    numberOfVisibleRows = getPropertyAsInt(StudioProperties.PROPERTY_SINGULARITY_GUI_LOGVIEWER_ROWLIMIT, DEFAULT_NUMBER_OF_VISIBLE_LINES);
                    logDocumentsMap.values().forEach(d -> d.setMaxRows(numberOfVisibleRows));
                }
            }
        });

        // create components
        {
            // toolbar
            ExtendedJToolBar toolBar = new ExtendedJToolBar();
            toolBar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.TEXTFIELD_BORDER));

            { // actions
                toolBar.add(closeLogAction);
                closeLogAction.setEnabled(false); // cannot close studio log
                toolBar.add(ResourceAction.of("log_view.search_log", true, e -> performSearch()));
                toolBar.add(ResourceAction.of("log_view.clear_message_viewer", true, e -> clearLog(currentLog)));
                toolBar.addSeparator();
            }


            // drop-down buttons for selecting content
            toolBar.add(Box.createHorizontalGlue());
            selectLogButton.setHideActionText(true);
            toolBar.add(selectLogButton);


            add(toolBar, BorderLayout.NORTH);
        }

        { // main content
            JPanel mainContent = new JPanel(new BorderLayout());
            mainContent.add(logNameLabel, BorderLayout.NORTH);

            textArea.setBackground(Colors.WHITE);
            textArea.setEditable(false);

            { // text pane pop up menu
                JPopupMenu textPanePopupMenu = new JPopupMenu();

                textPanePopupMenu.add(searchLogAction);
                textPanePopupMenu.add(ResourceAction.of("log_view.clear_message_viewer", e -> textArea.setText("")));
                textPanePopupMenu.addSeparator();
                textPanePopupMenu.add(new LogLevelMenu());

                textArea.addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        evaluatePopup(e);
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                        evaluatePopup(e);
                    }

                    private void evaluatePopup(MouseEvent e) {
                        if (e.isPopupTrigger()) {
                            textPanePopupMenu.show(textArea, e.getX(), e.getY());
                        }
                    }

                });
            }
            searchLogAction.addToActionMap(textArea, JComponent.WHEN_FOCUSED);

            JScrollPane scrollPane = new ExtendedJScrollPane(textArea);
            scrollPane.setBorder(null);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            new SmartScroller(scrollPane);

            mainContent.add(scrollPane, BorderLayout.CENTER);

            add(mainContent, BorderLayout.CENTER);
        }
    }

    private void switchToNextLog(String from) {
        Iterator<String> iterator = logDocumentsMap.keySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().equals(from)) {
                if (iterator.hasNext())
                    switchToLog(iterator.next());
                else
                    switchToLog(logDocumentsMap.keySet().iterator().next());
                return;
            }
        }
    }

    private void clearLog(String log) {
        logDocumentsMap.put(log, createNewStyledDocumentWithLogStyles(numberOfVisibleRows));
        if (currentLog.equals(log))
            switchToLog(log);
    }

    private void closeLog(String log) {
        if (logProvidersMap.get(log).isClosable()) {
            if (currentLog.equals(log)) {
                switchToNextLog(currentLog);
            }
            logDocumentsMap.remove(log);
            logProvidersMap.remove(log).close();

            // we have to update the secondary actions of selectLogButton
            selectLogButton.setSecondaryActions(logDocumentsMap.keySet()
                    .stream()
                    .map(name -> new DefaultResourceAction(a -> switchToLog(name), "log_view.switch_to_log", name))
                    .collect(Collectors.toList()));
        }
    }

    public void registerProvider(LogProvider provider) {
        String logName = provider.getName();
        if (!logDocumentsMap.containsKey(logName)) {

            logDocumentsMap.put(logName, createNewStyledDocumentWithLogStyles(numberOfVisibleRows));
            logProvidersMap.put(logName, provider);

            provider.configure(l -> {
                ExtendedStyledDocument document = logDocumentsMap.get(logName);
                l.forEach(s -> document.appendLineForBatch(s, documentStyleSet));
                document.executeBatchAppend();
            });

            // we have to update the secondary actions of selectLogButton
            selectLogButton.setSecondaryActions(logDocumentsMap.keySet()
                    .stream()
                    .map(name -> new DefaultResourceAction(a -> switchToLog(name), "log_view.switch_to_log", name))
                    .collect(Collectors.toList()));

        }
        switchToLog(logName);
    }

    public ExtendedStyledDocument createNewStyledDocumentWithLogStyles(int numberOfVisibleRows) {
        ExtendedStyledDocument document = new ExtendedStyledDocument(numberOfVisibleRows);
        Style fatal = document.addStyle(Level.FATAL.toString(), null);
        StyleConstants.setForeground(fatal, new Color(142, 3, 3));
        StyleConstants.setBold(fatal, true);
        StyleConstants.setFontFamily(fatal, DEFAULT_FONT_FAMILY);
        Style error = document.addStyle(Level.ERROR.toString(), null);
        StyleConstants.setForeground(error, new Color(152, 6, 6));
        StyleConstants.setBold(error, true);
        StyleConstants.setFontFamily(error, DEFAULT_FONT_FAMILY);
        Style warning = document.addStyle(Level.WARN.toString(), null);
        StyleConstants.setForeground(warning, new Color(171, 76, 3));
        StyleConstants.setBold(warning, true);
        StyleConstants.setFontFamily(warning, DEFAULT_FONT_FAMILY);
        Style debug = document.addStyle(Level.DEBUG.toString(), null);
        StyleConstants.setForeground(debug, Color.BLACK);
        StyleConstants.setFontFamily(debug, DEFAULT_FONT_FAMILY);
        Style trace = document.addStyle(Level.TRACE.toString(), null);
        StyleConstants.setForeground(trace, Color.BLACK);
        StyleConstants.setFontFamily(trace, DEFAULT_FONT_FAMILY);
        Style info = document.addStyle(Level.INFO.toString(), null);
        StyleConstants.setForeground(info, Color.BLACK);
        StyleConstants.setFontFamily(info, DEFAULT_FONT_FAMILY);
        return document;
    }

    private void switchToLog(String logName) {
        currentLog = logName;
        textArea.setDocument(logDocumentsMap.get(logName));
        logNameLabel.setText(logName);

        // update toolbar
        closeLogAction.setEnabled(logProvidersMap.get(logName).isClosable());
    }

    private void performSearch() {
        new SearchDialog(textArea, new SearchableJTextComponent(textArea)).setVisible(true);
    }


    private int getPropertyAsInt(String propertyKey, int defaultValue) {
        try {
            String maxRowsString = PropertyService.getParameterValue(propertyKey);
            if (maxRowsString != null) {
                return Integer.parseInt(maxRowsString);
            }
        } catch (NumberFormatException e) {
            LogService.getRoot().log(Level.WARN, "com.owc.singularity.studio.gui.tools.LoggingViewer.bad_integer_format_for_property");
        }
        return defaultValue;
    }

    /**
     * This is the menu for selecting the log level of the current log.
     */
    private static class LogLevelMenu extends ResourceMenu {

        private static final long serialVersionUID = 1L;

        public LogLevelMenu() {
            super("log_level");
            Level[] levels = Level.values();
            Arrays.sort(levels, Comparator.comparingInt(Level::intLevel));
            for (final Level level : levels) {
                Action action = new AbstractAction(level.name()) {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        changeLogLevelTo(level);
                    }
                };
                action.putValue("Level", level);
                JMenuItem item = new JMenuItem(action);
                add(item);
            }
            updateSelection();
        }

        private void updateSelection() {
            int itemCount = getItemCount();
            Level currentLevel = LogService.getRootLevel();
            for (int i = 0; i < itemCount; i++) {
                JMenuItem item = getItem(i);
                // highlight current log level
                Object level = item.getAction().getValue("Level");
                if (level != null && level.equals(currentLevel)) {
                    item.setFont(item.getFont().deriveFont(Font.BOLD));
                } else {
                    item.setFont(item.getFont().deriveFont(Font.PLAIN));
                }
            }
        }

        private void changeLogLevelTo(Level level) {
            ProgressThread progressThread = new ProgressThread("log_level") {

                @Override
                public void run() {
                    PropertyService.setParameterValue(StudioProperties.PROPERTY_SINGULARITY_STUDIO_LOG_LEVEL, level.name());
                    PropertyService.saveParameters();
                    SwingUtilities.invokeLater(LogLevelMenu.this::updateSelection);
                }
            };
            progressThread.start();
        }
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    @Override
    public Component getComponent() {
        return this;
    }
}
