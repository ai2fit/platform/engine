package com.owc.singularity.studio.gui.vcs.model;


import java.nio.file.Files;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffMergeConflictTreeNode extends AbstractMountTreeDiffFileTreeNode {

    private final EntryVersion ourVersion;
    private final EntryVersion theirVersion;

    public MountTreeDiffMergeConflictTreeNode(EntryVersion ourVersion, EntryVersion theirVersion, RepositoryPath path) {
        super(theirVersion, RepositoryPath.of(path, Files.exists(RepositoryPath.of(path, ourVersion.getId())) ? ourVersion.getId() : theirVersion.getId()));
        this.ourVersion = ourVersion;
        this.theirVersion = theirVersion;
    }

    public EntryVersion getOurVersion() {
        return ourVersion;
    }

    public EntryVersion getTheirVersion() {
        return theirVersion;
    }
}
