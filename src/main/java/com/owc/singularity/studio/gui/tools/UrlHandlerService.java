/*

 * 

 * 

 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;


import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.event.HyperlinkListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.update.internal.UpdateManagerRegistry;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.dialog.BrowserUnavailableDialogFactory;
import com.owc.singularity.studio.gui.documentation.OperatorDocumentationDockable;
import com.owc.singularity.studio.gui.perspective.Perspective;
import com.owc.singularity.studio.gui.security.PasswordManager;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * Convenience class for invoking certain actions from URLs, e.g. from inside a
 * {@link HyperlinkListener}. URLs of the form "rm://actionName" will be interpreted. Actions can
 * register themselves by invoking {@link UrlHandlerService#register(String, Action)}.
 *
 * @author Simon Fischer, Marco Boeck
 *
 */
public class UrlHandlerService {

    /** URL prefix of URLs that can be opened with SingularityEngine. */
    public static final String SINGULARITY_URL_PREFIX = "singularity://";
    /** internal rm:// url, e.g. for triggering actions */
    public static final String ACTION_URL_PREFIX = "singularity-action://";

    public static final String ACTION_MANAGE_DB_CONNECTIONS = "manage_db_connections";
    public static final String ACTION_MANAGE_DB_DRIVERS = "manage_db_drivers";
    public static final String ACTION_MANAGE_BUILDING_BLOCKS = "manage_building_blocks";
    public static final String ACTION_RUN_PROCESS_BACKGROUND = "run_process_background";
    public static final String ACTION_RUN_PROCESS_RMSERVER_NOW = "run_process_rmserver_now";
    public static final String ACTION_RUN_PROCESS_RMSERVER_SCHEDULE = "run_process_rmserver_schedule";
    public static final String ACTION_MARKETPLACE = "marketplace";
    /** @since 9.5.0 */
    public static final String ACTION_MARKETPLACE_UPDATES = "marketplace-updates";
    public static final String ACTION_MANAGE_EXTENSIONS = "manage_extensions";
    public static final String ACTION_MANAGE_LICENSES = "manage_licenses";

    /** use together with the appended name of the view you want to switch to */
    private static final String ACTION_VIEW_NEW = "new_view";
    private static final String ACTION_VIEW_RESTORE = "restore_view";
    private static final String ACTION_VIEW_OPEN_PREFIX = "open_view_";
    private static final String ACTION_STOP_PROCESS = "stop_process";
    private static final String ACTION_RUN_PROCESS = "run_process";
    private static final String ACTION_MANAGE_PASSWORDS = "manage_passwords";
    private static final String ACTION_MANAGE_CONFIGURABLES = "manage_configurables";
    private static final String ACTION_SETTINGS = "preferences";
    private static final String ACTION_TUTORIALS = "tutorials";
    private static final String ACTION_ABOUT = "about";
    private static final String ACTION_PROCESS_NEW = "process_new";
    private static final String ACTION_PROCESS_OPEN = "process_open";
    private static final String ACTION_PROCESS_SAVE = "process_save";
    private static final String ACTION_PROCESS_SAVE_AS = "process_save_as";
    private static final String ACTION_PROCESS_IMPORT = "process_import";
    private static final String ACTION_PROCESS_EXPORT = "process_export";
    private static final String ACTION_NEW_MOUNT = "new_mount";
    private static final String ACTION_AUTOWIRE = "autowire";
    private static final String ACTION_UNDO = "undo";
    private static final String ACTION_REDO = "redo";
    private static final String ACTION_SCREEN_EXPORT = "screen_export";

    private static final Map<String, Action> ACTION_MAP = new HashMap<>();

    private static final Logger LOGGER = LogManager.getLogger(UrlHandlerService.class.getCanonicalName());
    private static final String SCHEMA_SINGULARITY = "singularity";
    private static final String SINGULARITY_SCHEMA_EXTENSION = "extension";
    private static final String SINGULARITY_SCHEMA_REPOSITORY = "repository";
    private static final String SINGULARITY_SCHEMA_OPERATOR_TUTORIAL_PROCESS = "operator_tutorial_process";
    private static final String SINGULARITY_SCHEMA_PROCESS_URL = "process_url";
    private static final String INTERNAL_SCHEMA_OPDOC = "opdoc/";
    private static final String INTERNAL_SCHEMA_OPERATOR = "operator/";
    private static final String SCHEMA_HTTP = "http://";
    private static final String SCHEMA_HTTPS = "https://";


    /**
     * Inits all actions. Has no effect if called more than once.
     *
     * @since 9.0.0
     */
    public static void initActions() {
        register(ACTION_PROCESS_NEW, MainFrame.INSTANCE.NEW_ANALYSIS_ACTION);
        register(ACTION_PROCESS_OPEN, MainFrame.INSTANCE.OPEN_ACTION);
        register(ACTION_PROCESS_SAVE, MainFrame.INSTANCE.SAVE_ACTION);
        register(ACTION_PROCESS_SAVE_AS, MainFrame.INSTANCE.SAVE_AS_ACTION);
        register(ACTION_UNDO, MainFrame.INSTANCE.UNDO_ACTION);
        register(ACTION_REDO, MainFrame.INSTANCE.REDO_ACTION);
        register(ACTION_RUN_PROCESS, MainFrame.INSTANCE.RUN_ACTION);
        register(ACTION_STOP_PROCESS, MainFrame.INSTANCE.STOP_ACTION);
        register(ACTION_AUTOWIRE, MainFrame.INSTANCE.AUTO_WIRE);
        register(ACTION_VIEW_NEW, MainFrame.INSTANCE.NEW_PERSPECTIVE_ACTION);
        register(ACTION_SCREEN_EXPORT, MainFrame.INSTANCE.EXPORT_ACTION);
        register(ACTION_SETTINGS, MainFrame.INSTANCE.SETTINGS_ACTION);
        register(ACTION_VIEW_RESTORE, MainFrame.INSTANCE.RESTORE_PERSPECTIVE_ACTION);
        register(ACTION_ABOUT, MainFrame.INSTANCE.ABOUT_ACTION);
        register(ACTION_MANAGE_PASSWORDS, PasswordManager.OPEN_WINDOW);

        // add view switch actions
        for (Perspective perspective : MainFrame.INSTANCE.getPerspectiveController().getAllPerspectives()) {
            if (!perspective.isUserDefined()) {
                register(ACTION_VIEW_OPEN_PREFIX + perspective.getName(), MainFrame.INSTANCE.getPerspectiveController().createPerspectiveAction(perspective));
            }
        }
    }

    /**
     * Handles the given url string. Can understand urls like "https://" etc, but also
     * SingularityEngine specific urls like "singularity:///extension/extension_id" and
     * rm://{action}. Action names are only available since Studio 9.0!
     *
     * @return true iff we understand the url
     */
    public static boolean handleUrl(String url) {
        if (url.startsWith(SCHEMA_HTTP) || url.startsWith(SCHEMA_HTTPS)) {
            openInBrowser(url);
            return true;
        } else if (url.startsWith(SINGULARITY_URL_PREFIX)) {
            handleSingularityURL(url);
            return true;
        } else if (url.startsWith(ACTION_URL_PREFIX)) {
            handleRMUrl(url);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Registers the given action under the given name as a URL handler. The URL of the format
     * {@code singularity-action://name} can then be passed to {@link #handleUrl(String)}, which
     * then triggers the registered action.
     *
     * @param name
     *            the name under which the action can be called again
     * @param action
     *            the action to trigger when a url of the format {@code rm://name} is passed to
     *            {@link #handleUrl(String)}
     */
    public static void register(String name, Action action) {
        ACTION_MAP.put(name, action);
    }

    /**
     * Tries to open a {@link URL} in the system browser
     * <p>
     * Fallback: Shows the URL in a dialog
     * </p>
     *
     * @param url
     *            the url to open
     */
    public static void openInBrowser(URL url) {
        try {
            URI uri = url.toURI();
            openInBrowser(uri);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid URL: " + url, e);
        }
    }

    /**
     * Tries to open an URI String in the system browser
     * <p>
     * Fallback: Shows the URI String in a dialog
     * </p>
     *
     * @param uriString
     *            the uri as a string
     */
    public static void openInBrowser(String uriString) {
        try {
            URI uri = new URI(uriString);
            openInBrowser(uri);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid URI String: " + uriString, e);
        }
    }

    /**
     * Tries to open an URI in the system browser
     * <p>
     * Fallback: Shows the URI String in a dialog
     * </p>
     */
    public static void openInBrowser(URI uri) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().browse(uri);
            } catch (IOException e) {
                openInBrowserWithTempFile(uri);
            }
        } else {
            showBrowserUnavailableMessage(uri.toString());
        }

    }

    /**
     * Browse the uri using a local temp file
     * <p>
     * On systems where Desktop.getDesktop().browse() does not work for http://, creates an HTML
     * page which redirects to the given URI and calls Desktop.browse() with this file through the
     * file:// url which seems to work better, at least for KDE.
     * </p>
     *
     * @param uri
     *            the uri to open
     */
    private static void openInBrowserWithTempFile(URI uri) {
        try {
            File tempFile = File.createTempFile("redirect", ".html");
            tempFile.deleteOnExit();
            try (FileWriter out = new FileWriter(tempFile)) {
                out.write(String.format(
                        "<!DOCTYPE html>\n" + "<html><meta http-equiv=\"refresh\" content=\"0; URL=%s\"><body>You are redirected to %s</body></html>",
                        uri.toString(), uri));
                Desktop.getDesktop().browse(tempFile.toURI());
            }
        } catch (IOException e) {
            showBrowserUnavailableMessage(uri.toString());
        }
    }

    /**
     * Displays the uri in a {@link ButtonDialog}
     *
     * @param uri
     *            the uri which could not be displayed
     */
    private static void showBrowserUnavailableMessage(String uri) {
        ButtonDialog dialog = BrowserUnavailableDialogFactory.createNewDialog(uri);
        dialog.setVisible(true);
        LOGGER.error("Failed to open web page in browser, browsing is not supported on this platform.");

    }

    /**
     * Handles rm:// urls. Supported patterns are {@value #INTERNAL_SCHEMA_OPDOC},
     * {@value #INTERNAL_SCHEMA_OPERATOR} and registered actions.
     *
     * @param url
     *            the url starting with rm://
     * @see #initActions()
     * @see #register(String, Action)
     */
    private static void handleRMUrl(String url) {
        String suffix = url.substring(ACTION_URL_PREFIX.length());

        if (suffix.startsWith(INTERNAL_SCHEMA_OPDOC)) {
            // operator doc display change
            String opName = suffix.substring(INTERNAL_SCHEMA_OPDOC.length());
            try {
                MainFrame.INSTANCE.getOperatorDocViewer().setDisplayedOperator(OperatorService.createOperator(opName));
                DockingTools.openDockable(OperatorDocumentationDockable.OPERATOR_HELP_DOCK_KEY);
            } catch (OperatorCreationException e) {
                LogService.getRoot().warn("com.owc.singularity.engine.tools.UrlHandler.creating_operator_error", opName, e);
            }
        } else if (suffix.startsWith(INTERNAL_SCHEMA_OPERATOR)) {
            // operator selection
            String opName = suffix.substring(INTERNAL_SCHEMA_OPERATOR.length());
            MainFrame mainFrame = MainFrame.INSTANCE;
            AbstractPipeline process = mainFrame.getMainProcessPanel().getProcess();
            mainFrame.getMainProcessPanel().getProcessEditor().selectAndShowOperator(process.getOperator(opName), true);
        } else {
            // try if an action is registered under than name and trigger it
            Action action = ACTION_MAP.get(suffix);
            if (action != null) {
                action.actionPerformed(null);
            } else {
                LogService.getRoot().warn("com.owc.singularity.engine.tools.RMUrlHandler.no_action_associated_with_url", url);
            }
        }
    }

    /**
     * Called with {@link #SCHEMA_SINGULARITY singularity://} URLs passed as command line arguments.
     * Currently the only supported patterns are {@link #SINGULARITY_SCHEMA_EXTENSION
     * singularity://extension/&#123;extensionId&#125;} to install an extension with the given name
     * and {@link #SINGULARITY_SCHEMA_REPOSITORY
     * singularity://repository/&#123;repository_path&#125;} to open data from a repository path.
     * The path has to escape "/" with ":".
     *
     * This method just logs a warning method if it cannot handle the singularity:// URL for
     * syntactical reasons.
     *
     * @throws IllegalArgumentException
     *             if this is not a singularity:// url
     */
    private static void handleSingularityURL(String urlStr) {
        URI url;

        try {
            url = new URI(urlStr);
        } catch (URISyntaxException e) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.malformed_singularity_url", new Object[] { urlStr, e.getMessage() });
            return;
        }

        if (!SCHEMA_SINGULARITY.equals(url.getScheme())) {
            // in this case we should not be called, so throw
            throw new IllegalArgumentException("Can handle only " + SINGULARITY_URL_PREFIX + " URLs!");
        }
        String path = url.getPath();
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (url.getAuthority() != null) {
            path = url.getAuthority() + "/" + path;
        }
        String[] components = path.split("/");
        if (components.length < 2) {
            LogService.getRoot().warn("com.owc.singularity.studio.gui.unknown_singularity_url", new Object[] { urlStr });
            return;
        }
        switch (components[0]) {
            case SINGULARITY_SCHEMA_EXTENSION:
                String extensionKey = components[1];
                // asynchronous
                try {
                    UpdateManagerRegistry.INSTANCE.get().showUpdateDialog(false, extensionKey);
                } catch (URISyntaxException | IOException e) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.error_connecting_to_updateserver", e);
                }
                break;
            case SINGULARITY_SCHEMA_REPOSITORY:
                String locString = components[1].replaceAll(":", "/");
                try {
                    final RepositoryPath location = RepositoryPath.of(locString);
                    if (!Files.exists(location)) {
                        LogService.getRoot().warn("com.owc.singularity.studio.gui.singularity_url_repo_not_found", new Object[] { locString });
                        return;
                    }
                    Entry entry = Entries.getEntry(location);
                    ClassLoader majorClassLoader = ModuleService.getMajorClassLoader();
                    if (entry.isInstanceOf(AbstractPipeline.class, majorClassLoader)) {
                        OpenEntryAction.showProcess(entry);
                    } else if (entry.isInstanceOf(ConnectionParametersIOObject.class, majorClassLoader)) {
                        OpenEntryAction.showConnectionInformationDialog(entry);
                    } else if (entry.isInstanceOf(IOObject.class, majorClassLoader)) {
                        OpenEntryAction.showAsResult(entry);
                    } else {
                        LogService.getRoot()
                                .warn("com.owc.singularity.studio.gui.singularity_url_repo_unknown_type",
                                        new Object[] { entry.getClass().getName(), locString });
                    }
                } catch (IOException | ClassNotFoundException e) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.singularity_url_repo_broken_location", new Object[] { locString });
                }
                break;
            case SINGULARITY_SCHEMA_OPERATOR_TUTORIAL_PROCESS:
                try {
                    int lastColumnPos = components[1].lastIndexOf(':');
                    String operatorKey = components[1].substring(0, lastColumnPos);
                    int tutorialPos = Integer.parseInt(components[1].substring(lastColumnPos + 1));
                    OperatorDocumentationDockable.openTutorialProcess(operatorKey, tutorialPos);
                } catch (Exception e) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.unknown_singularity_url", new Object[] { urlStr });
                }
                break;
            default:
                LogService.getRoot().warn("com.owc.singularity.studio.gui.unknown_singularity_url", new Object[] { urlStr });
        }
    }
}
