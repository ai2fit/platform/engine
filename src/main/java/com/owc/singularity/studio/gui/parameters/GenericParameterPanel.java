/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters;


import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;


/**
 * This panel might be used, where ever ParameterTypes should be editable without presence of an
 * operator or special circumstances.
 * 
 * @author Sebastian Land
 * 
 */
public class GenericParameterPanel extends ParameterPanel {

    private static final long serialVersionUID = -8633435565053835262L;

    private Parameters parameters = new Parameters();

    /**
     * Constructor if parameters are not yet available.
     */
    public GenericParameterPanel() {
        super();
    }

    public GenericParameterPanel(Parameters parameters) {
        super();
        setParameters(parameters);
    }

    @Override
    protected Operator getOperator() {
        return null;
    }

    @Override
    protected Collection<ParameterType> getParameterTypes() {
        if (parameters == null) {
            return Collections.emptyList();
        }
        return parameters.getParameterTypes().stream().filter(pt -> !pt.isHidden()).collect(Collectors.toList());
    }

    @Override
    protected String getRawValue(ParameterType type) {
        if (parameters != null) {
            try {
                return parameters.getRawParameter(type.getKey());
            } catch (UndefinedParameterError e) {
                return type.getDefaultValueAsRawString();
            }
        } else {
            return null;
        }
    }

    /**
     * This implementation ignores the operator, since it is null anyway.
     */
    @Override
    protected void setRawValue(Operator operator, ParameterType type, String value) {
        if (parameters != null) {
            parameters.setRawParameter(type.getKey(), value);
            // keyValueMap.put(type.getKey(), value);
            setupComponents();
        }
    }

    public void setRawValue(String key, String value) {
        parameters.setRawParameter(key, value);
        setupComponents();
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;

        // calling super method for rebuilding panel
        setupComponents();
    }

    public void clearProperties() {
        this.parameters = new Parameters();

        setupComponents();
    }

    public Parameters getParameters() {
        return parameters;
    }

    @Override
    protected void setRawValue(Operator operator, ParameterType type, String value, boolean updateComponents) {
        if (updateComponents) {
            setRawValue(operator, type, value);
        } else {
            parameters.setRawParameter(type.getKey(), value);
        }
    }

}
