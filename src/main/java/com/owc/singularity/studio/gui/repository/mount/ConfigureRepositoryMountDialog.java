/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.mount;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

import javax.swing.*;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.repository.*;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.Form;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.components.FixedWidthLabel;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * Dialog to configure an existing mount.
 *
 * @author Hatem Hamad
 *
 */
public class ConfigureRepositoryMountDialog extends ButtonDialog {

    private static final long serialVersionUID = 1L;

    private final RepositoryMountProvider mountProvider;
    private final Form<Map<String, String>> editForm;
    private final FixedWidthLabel validationResult;
    private final JButton saveButton;

    public ConfigureRepositoryMountDialog(Map<String, String> mountOptions, String mountType) {
        super(ApplicationFrame.getApplicationFrame(), "configure_mount_dialog", ModalityType.APPLICATION_MODAL, new Object[0]);
        mountProvider = RepositoryFileSystemProvider.getMountProviderFor(mountType);

        validationResult = new FixedWidthLabel(300, "");
        editForm = RepositoryUIManager.getMountEditForm(mountType, mountOptions);

        List<AbstractButton> buttons = new LinkedList<>();
        buttons.add(new JButton(new ResourceAction("repository_configuration_dialog.check") {

            @Override
            protected void loggedActionPerformed(ActionEvent e) {
                validateOptionsAndDisplayMessages();
            }
        }));

        saveButton = makeOkButton("repository_configuration_dialog.save");
        buttons.add(saveButton);
        buttons.add(makeCancelButton());

        decideSaveButtonState();
        editForm.addInputChangeListener((inputKey, inputValue) -> decideSaveButtonState());

        JPanel main = new JPanel(new BorderLayout());
        main.add(editForm.getComponent(), BorderLayout.CENTER);
        main.add(validationResult, BorderLayout.SOUTH);
        layoutDefault(main, DEFAULT_SIZE, buttons);
    }

    private void validateOptionsAndDisplayMessages() {
        Map<String, String> options = editForm.getData();
        validationResult.setText(null);
        validationResult.setForeground(Colors.PANEL_BACKGROUND);
        Map<String, String> validationMessages = mountProvider.validate(options);
        // check if there is any validation error message
        if (validationMessages.values().stream().anyMatch(Objects::nonNull)) {
            validationResult.setForeground(Color.RED);
            StringBuilder errorMessage = new StringBuilder("Invalid options:<br/>");
            for (Map.Entry<String, String> messageEntry : validationMessages.entrySet()) {
                if (messageEntry.getValue() == null)
                    continue;
                errorMessage.append(messageEntry.getKey())
                        .append(": ")
                        .append(Objects.requireNonNullElse(
                                I18N.getErrorMessageOrNull("repository.mount.validation.error." + messageEntry.getValue(), options.get(messageEntry.getKey())),
                                messageEntry.getValue()))
                        .append("<br/>");
            }
            validationResult.setText(errorMessage.toString());
        } else {
            validationResult.setText("Options are valid");
            validationResult.setForeground(Colors.SUCCESS);
        }
    }

    private void decideSaveButtonState() {
        saveButton.setEnabled(editForm.isReadyToSubmit());
    }

    public Map<String, String> getOptions() {
        return editForm.getData();
    }
}
