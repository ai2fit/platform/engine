package com.owc.singularity.studio.gui.tools;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.JPopupMenu;

import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.engine.tools.function.BiIntFunction;

/**
 * 
 * This popup menu allows to add specific popup menu actions, that can be disabled depending on the
 * selected entry.
 */
public class ExtendedJPopupMenu<E> extends JPopupMenu {

    private static final long serialVersionUID = 1312576300559328390L;

    private BiIntFunction<E> locationToEntryFunction;
    private List<Pair<ResourceAction, Predicate<E>>> actionsTestList = new LinkedList<>();

    private E currentEntry = null;

    public ExtendedJPopupMenu(BiIntFunction<E> locationToEntryFunction) {
        super();
        this.locationToEntryFunction = locationToEntryFunction;
    }


    @Override
    public void show(Component invoker, int x, int y) {
        currentEntry = locationToEntryFunction.apply(x, y);
        actionsTestList.forEach(p -> p.getFirst().setEnabled(p.getSecond().test(currentEntry)));
        super.show(invoker, x, y);
    }

    public void add(ResourceAction action, Predicate<E> isEnabled) {
        add(action);
        actionsTestList.add(new Pair<>(action, isEnabled));
    }

    public E getSelectedEntry() {
        return currentEntry;
    }
}
