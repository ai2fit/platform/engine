/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor;


import java.awt.BorderLayout;
import java.awt.Component;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.parameters.ParameterPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.owc.singularity.studio.gui.tools.UpdateQueue;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 *
 * A Dockable, that can display all defined variables and their values and allows the user to modify
 * the while the process is running.
 *
 * @author Philipp Kersting
 *
 */

public class VariableViewer extends JPanel implements Dockable, FocusProcessChangeEventListener {

    private static final long serialVersionUID = 1L;

    private static final String VARIABLE_VIEWER_DOCK_KEY = "variable_viewer";
    private static final DockKey DOCK_KEY = new ResourceDockKey(VARIABLE_VIEWER_DOCK_KEY);

    private final ExtendedJTable variableTable;
    private final ExtendedJScrollPane scrollPane;

    private List<Pair<String, String>> data = new ArrayList<>();
    private final String[] names = { "Variable", "Value" };
    private AbstractPipeline currentProcess;

    private final UpdateQueue updateQueue;
    private final Observer variableObserver;
    private boolean readOnly;

    private final AbstractTableModel dataModel = new AbstractTableModel() {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public Object getValueAt(int row, int col) {

            if (col == 0) {
                return data.get(row).getFirst();
            } else {
                return data.get(row).getSecond();
            }

        }

        @Override
        public String getColumnName(int column) {
            return names[column];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if (isReadOnly())
                return false;
            return columnIndex != 0;
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            String content = (String) aValue;
            data.get(rowIndex).setSecond(content);
            AbstractPipeline process = MainFrame.INSTANCE.getProcess();
            if (process == null)
                return;
            process.getVariableHandler().addVariable(data.get(rowIndex).getFirst(), content);
        }

    };

    public VariableViewer() {
        setLayout(new BorderLayout());

        variableTable = new ExtendedJTable(dataModel, true, false, true);
        variableTable.setRowHeight(ParameterPanel.VALUE_CELL_EDITOR_HEIGHT);
        variableTable.setRowHighlighting(true);

        scrollPane = new ExtendedJScrollPane(variableTable);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(10, 5, 0, 5));
        add(scrollPane, BorderLayout.CENTER);

        updateQueue = new UpdateQueue(VARIABLE_VIEWER_DOCK_KEY);
        updateQueue.start();
        variableObserver = new Observer() {

            @Override
            public void update(Observable o, Object arg) {
                updateQueue.execute(new Runnable() {

                    @Override
                    public void run() {
                        updateVariables();
                        try {
                            Thread.sleep(1000); // Sleep 1sec to avoid update flooding
                        } catch (InterruptedException e) {
                        }
                    }
                });
            }
        };
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    @Override
    public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
        if (currentProcess != null) {
            currentProcess.getVariableHandler().deleteObserver(variableObserver);
        }
        currentProcess = currentlyFocusedPanel != null ? currentlyFocusedPanel.getProcess() : null;
        if (currentProcess != null) {
            variableTable.removeEditor();
            setReadOnly(currentlyFocusedPanel.isReadOnly());
            currentProcess.getVariableHandler().addObserver(variableObserver);
        }
    }

    public void updateVariables() {
        final List<Pair<String, String>> newData = new ArrayList<>();
        AbstractPipeline process = MainFrame.INSTANCE.getMainProcessPanel().getProcess();
        Arrays.stream(process.getVariableHandler().getMostUsedPredefinedVariables())
                .map(variable -> new Pair<>(variable, process.getVariableHandler().getVariable(variable)))
                .forEach(newData::add);
        Iterator<String> variableNames = process.getVariableHandler().getDefinedVariableNames();

        while (variableNames.hasNext()) {
            String name = variableNames.next();
            String value = process.getVariableHandler().getVariable(name);
            newData.add(new Pair<>(name, value));
        }

        newData.sort((o1, o2) -> o1.getFirst().compareToIgnoreCase(o2.getFirst()));

        SwingUtilities.invokeLater(() -> {
            data = newData;
            dataModel.fireTableDataChanged();
        });
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public boolean isReadOnly() {
        return readOnly;
    }
}
