/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * This action will show the current process in the repository tree
 * 
 * @author Marcin Skirzynski
 */
public class ShowProcessInRepositoryAction extends ResourceAction {

    private static final long serialVersionUID = -430582650605522700L;

    private final RepositoryTree tree;

    public ShowProcessInRepositoryAction(RepositoryTree tree) {
        super(true, "link");
        this.tree = tree;
        setCondition(Condition.PROCESS_HAS_REPOSITORY_LOCATION, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        AbstractPipeline process = MainFrame.INSTANCE.getMainProcessPanel().getProcess();
        if (process != null) {
            RepositoryPath repoLoc = process.getPath();
            if (repoLoc != null) {
                // scroll to location
                tree.expandAndSelectIfExists(repoLoc);
            }
        }
    }
}
