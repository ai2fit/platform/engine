/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;

/**
 * A class that presents helper methods for interacting with the repository.
 *
 * @author Sebastian Land
 */
public class RepositoryGuiTools {

    public static final String[] BLACKLISTED_STRINGS = new String[] { "\\", ":", "<", ">", "*", "?", "\"", "|" };

    /**
     * Checks if the given name is valid as a repository entry. Checks against a blacklist of
     * characters.
     *
     * @param name
     *            the entry name to check
     * @return true if valid
     */
    public static boolean isNameValid(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null!");
        }
        if (name.isBlank()) {
            return false;
        }

        for (String forbiddenString : BLACKLISTED_STRINGS) {
            if (name.contains(forbiddenString)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the sub{@link String} in the given name which is invalid or <code>null</code> if
     * there are no illegal characters.
     *
     */
    public static String getIllegalCharacterInName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return null;
        }

        for (String forbiddenString : BLACKLISTED_STRINGS) {
            if (name.contains(forbiddenString)) {
                return forbiddenString;
            }
        }
        return null;
    }
}
