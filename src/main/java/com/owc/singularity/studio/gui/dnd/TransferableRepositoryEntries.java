/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.dnd;


import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.Arrays;
import java.util.List;

import com.owc.singularity.repository.RepositoryPath;


/**
 * Provides a transferable wrapper for {@link com.owc.singularity.repository.entry.Entry}s in order
 * to drag-n-drop them.
 * 
 * @author Marco Boeck
 * @since 8.1
 */
public class TransferableRepositoryEntries implements Transferable {


    public static final DataFlavor LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR = new DataFlavor(
            DataFlavor.javaJVMLocalObjectMimeType + ";class=" + RepositoryPath.class.getName(), "repository location");

    public static final DataFlavor LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR = new DataFlavor(
            DataFlavor.javaJVMLocalObjectMimeType + ";class=\"" + RepositoryPath[].class.getName() + "\"", "repository locations");

    private static final DataFlavor[] DATA_FLAVORS = { LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR, LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR,
            DataFlavor.stringFlavor };

    private final RepositoryPath[] locations;
    private final DataFlavor contentFlavor;

    /**
     * The transferable location(s).
     *
     * @param location
     *            the location(s) of the entry/entries which should be drag & dropped
     */
    public TransferableRepositoryEntries(RepositoryPath... location) {
        if (location == null || location.length == 0) {
            throw new IllegalArgumentException("location must not be null or empty!");
        }

        this.locations = location;
        this.contentFlavor = location.length == 1 ? LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR : LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
        if (flavor.equals(LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR)) {
            return locations[0];
        }
        if (flavor.equals(LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR)) {
            return locations;
        }
        if (flavor.equals(DataFlavor.stringFlavor)) {
            return locations.length == 1 ? locations[0].toString() : Arrays.toString(locations);
        }
        throw new UnsupportedFlavorException(flavor);
    }

    public List<RepositoryPath> getLocations() {
        return Arrays.asList(locations);
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(contentFlavor) || flavor.equals(DataFlavor.stringFlavor);
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { contentFlavor, DataFlavor.stringFlavor };
    }

}
