/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.perspective;


import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.text.html.HTMLEditorKit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.ProcessTabViewDockable;
import com.owc.singularity.studio.gui.actions.WorkspaceAction;
import com.owc.singularity.studio.gui.documentation.OperatorDocumentationDockable;
import com.owc.singularity.studio.gui.editor.ProcessLogTab;
import com.owc.singularity.studio.gui.editor.operators.OperatorsDockable;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.parameters.OperatorParameterPanel;
import com.owc.singularity.studio.gui.perspective.Perspective.ModifiableWSDesktop;
import com.owc.singularity.studio.gui.repository.RepositoryBrowserDockable;
import com.owc.singularity.studio.gui.results.ResultDisplay;
import com.owc.singularity.studio.gui.results.ResultTab;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.vcs.conflict.MergeConflictResolvingView;
import com.vlsolutions.swing.docking.*;
import com.vlsolutions.swing.docking.ws.WSDockKey;
import com.vlsolutions.swing.docking.ws.WorkspaceException;


/**
 * The {@link PerspectiveController} manages a {@link PerspectiveModel} to show, delete and
 * manipulates application {@link Perspective}s. The {@link PerspectiveModel} itself is an
 * {@link Observable} and can notify listeners about new registered perspectives and perspective
 * changes.
 *
 * @author Marcel Michel
 * @since 7.0.0
 */
public class PerspectiveController {

    public static final String RESULT = "result";
    public static final String DESIGN = "design";
    public static final String VCS = "vcs";
    public static final DockGroup DOCK_GROUP_VCS = new DockGroup("vcs");
    public static final String TURBO_PREP = "turbo_prep";
    public static final String MODEL_WIZARD = "model_wizard";
    public static final String DEPLOYMENTS = "deployments";
    public static final String HADOOP_DATA = "hadoop_data";

    private static final Logger log = LogManager.getLogger(PerspectiveController.class);

    private final DockingContext context;

    private final PerspectiveModel model;


    /**
     * Creates a new {@link PerspectiveController} with the given docking context.
     *
     * @param context
     *            the docking context which should be used
     */
    public PerspectiveController(final DockingContext context) {
        this.context = context;
        this.model = new PerspectiveModel();
        context.setDockableResolver(key -> {
            if (key.startsWith(ResultTab.DOCKKEY_PREFIX)) {
                ResultTab tab = new ResultTab(key);
                tab.showResult(null, null);
                return tab;
            } else if (key.startsWith(ProcessLogTab.DOCKKEY_PREFIX)) {
                ProcessLogTab tab = new ProcessLogTab(key);
                tab.setDataTableViewer(null);
                return tab;
            } else {
                return null;
            }
        });
    }

    private void makePredefined() {
        getModel().addPerspective(DESIGN, false);
        restoreDefault(DESIGN);
        getModel().addPerspective(RESULT, false);
        restoreDefault(RESULT);
        getModel().addPerspective(VCS, false);
        restoreDefault(VCS);
    }

    /**
     * Restores the default layout of the perspectives. This method only works for predefined
     * perspectives (like {@link #DESIGN} and {@link #RESULT}).
     *
     * @param perspectiveName
     *            the name of the perspective which should be restored
     *
     * @throws IllegalArgumentException
     *             if the perspective is not known
     */
    private void restoreDefault(String perspectiveName) {
        WSDockKey processPanelKey = new WSDockKey(ProcessTabViewDockable.PROCESS_PANEL_DOCK_KEY);
        WSDockKey propertyTableKey = new WSDockKey(OperatorParameterPanel.PROPERTY_EDITOR_DOCK_KEY);
        WSDockKey resultsKey = new WSDockKey(ResultDisplay.RESULT_DOCK_KEY);
        WSDockKey repositoryKey = new WSDockKey(RepositoryBrowserDockable.REPOSITORY_BROWSER_DOCK_KEY);
        WSDockKey newOperatorEditorKey = new WSDockKey(OperatorsDockable.NEW_OPERATOR_DOCK_KEY);
        WSDockKey vcsMergeConflictResolverKey = new WSDockKey(MergeConflictResolvingView.DOCKABLE_KEY);
        WSDockKey operatorHelpKey = new WSDockKey(OperatorDocumentationDockable.OPERATOR_HELP_DOCK_KEY);

        if (DESIGN.equals(perspectiveName)) {
            Perspective designPerspective = getModel().getPerspective(DESIGN);
            ModifiableWSDesktop designDesktop = designPerspective.getWorkspace().getDesktop(0);
            designDesktop.clear();
            designDesktop.addDockable(processPanelKey);
            designDesktop.split(processPanelKey, propertyTableKey, DockingConstants.SPLIT_RIGHT, 0.8);
            designDesktop.split(propertyTableKey, operatorHelpKey, DockingConstants.SPLIT_BOTTOM, .66);
            designDesktop.split(processPanelKey, repositoryKey, DockingConstants.SPLIT_LEFT, 0.25);
            designDesktop.split(repositoryKey, newOperatorEditorKey, DockingConstants.SPLIT_BOTTOM, 0.5);
        } else if (RESULT.equals(perspectiveName)) {
            Perspective resultPerspective = getModel().getPerspective(RESULT);
            ModifiableWSDesktop resultsDesktop = resultPerspective.getWorkspace().getDesktop(0);
            resultsDesktop.clear();
            resultsDesktop.addDockable(resultsKey);
            resultsDesktop.split(resultsKey, repositoryKey, DockingConstants.SPLIT_RIGHT, 0.8);
        } else if (VCS.equals(perspectiveName)) {
            Perspective vcsPerspective = getModel().getPerspective(VCS);
            ModifiableWSDesktop designDesktop = vcsPerspective.getWorkspace().getDesktop(0);
            designDesktop.clear();
            designDesktop.addDockable(processPanelKey);
            designDesktop.split(processPanelKey, propertyTableKey, DockingConstants.SPLIT_RIGHT, 0.8);
            designDesktop.split(processPanelKey, repositoryKey, DockingConstants.SPLIT_LEFT, 0.25);
            designDesktop.split(propertyTableKey, operatorHelpKey, DockingConstants.SPLIT_BOTTOM, .66);
            designDesktop.split(repositoryKey, newOperatorEditorKey, DockingConstants.SPLIT_BOTTOM, 0.4);
            designDesktop.split(newOperatorEditorKey, vcsMergeConflictResolverKey, DockingConstants.SPLIT_BOTTOM, 0.5);
        } else {
            throw new IllegalArgumentException("Not a predefined view: " + perspectiveName);
        }
    }

    public DockingContext getContext() {
        return context;
    }

    /**
     * Displays the given perspective, identified by the name.
     *
     * @param perspectiveName
     *            the perspective which should be shown.
     */
    public void showPerspective(final String perspectiveName) {
        showPerspective(getPerspective(perspectiveName));
    }


    /**
     * Displays the given perspective.
     *
     * @param target
     *            the perspective which should be shown.
     */
    public void showPerspective(final Perspective target) {
        SwingTools.invokeAndWait(() -> {
            if (target == null) {
                return;
            }
            Perspective oldPerspective = getModel().getSelectedPerspective();
            if (oldPerspective == target) {
                return;
            }
            if (!getModel().getAllPerspectives().contains(target)) {
                return;
            }
            getModel().setSelectedPerspective(target);
            if (oldPerspective != null) {
                try {
                    oldPerspective.store(context);
                } catch (WorkspaceException e) {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Cannot save workspace", e);
                }
                ActionStatisticsCollector.getInstance().stopTimer(oldPerspective);
            }
            if (!tryApplying(target)) {
                // retry if switching did not work the first time
                if (!tryApplying(target)) {
                    if (oldPerspective != null) {
                        ActionStatisticsCollector.getInstance()
                                .startTimer(oldPerspective, ActionStatisticsCollector.TYPE_PERSPECTIVE, oldPerspective.getName(), null);
                    }
                    return;
                }
            }
            MainFrame.INSTANCE.RESTORE_PERSPECTIVE_ACTION
                    .setEnabled(!target.isUserDefined() && (DESIGN.equals(target.getName()) || RESULT.equals(target.getName())));
        });

    }

    private boolean tryApplying(Perspective target) {
        try {
            target.apply(context);
            return true;
        } catch (WorkspaceException e) {
            String errorMessage = e.getCause().getMessage();
            if (errorMessage != null && errorMessage.contains("not registered") && e.getCause().getMessage().contains("Dockable")) {
                // dockable is missing. create a placeholder with a warning
                String nodeKey = errorMessage.substring(errorMessage.indexOf("Dockable ") + "Dockable ".length(), errorMessage.indexOf(" not registered"));
                MainFrame.INSTANCE.registerDockable(new ErrorDockable(nodeKey));
                // and retry
                try {
                    target.apply(context);
                    return true;
                } catch (Exception error) {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Cannot apply workspace: {}", target.getName(), error);
                    return false;
                }
            }
            log.warn(LogService.MARKER_DEVELOPMENT, "Cannot apply workspace: {}", target.getName(), e);
        } catch (Exception e) {
            log.warn(LogService.MARKER_DEVELOPMENT, "Cannot apply workspace: {}", target.getName(), e);
            return false;
        }
        return false;
    }

    private static class ErrorDockable extends JPanel implements Dockable {

        private final DockKey dockKey;

        ErrorDockable(String dockKey) {
            super(new GridLayout());
            this.dockKey = new DockKey(dockKey);
            this.setBorder(null);
            JEditorPane textComponent = new JEditorPane();
            textComponent.setEditorKit(new HTMLEditorKit());
            textComponent.setContentType("text/html");
            textComponent.setText("<div style= 'margin-top:20px;'>The Panel <b>" + dockKey + "</b> is no longer supported and cannot be displayed.</div>");
            textComponent.setBackground(Colors.PANEL_BACKGROUND);
            textComponent.setEditable(false);
            this.add(textComponent, BorderLayout.CENTER);
        }

        @Override
        public DockKey getDockKey() {
            return dockKey;
        }

        @Override
        public Component getComponent() {
            return this;
        }
    }

    /**
     * Removes the given perspective. If the perspective which should be deleted is also the
     * selected perspective, the first perspective will be shown.
     *
     * @param name
     *            the name of the perspective which should be removed
     */
    public void removePerspective(String name) {
        Perspective perspective = getModel().getPerspective(name);
        if (perspective != null) {
            removePerspective(perspective);
        }
    }

    /**
     * Removes the given perspective. If the perspective which should be deleted is also the
     * selected perspective, the first perspective will be shown.
     *
     * @param perspective
     *            the perspective which should be deleted
     */
    public void removePerspective(Perspective perspective) {
        SwingTools.invokeAndWait(() -> {
            if (!perspective.isUserDefined()) {
                return;
            }
            getModel().deletePerspective(perspective);
            if (getModel().getSelectedPerspective() == perspective && !getModel().getAllPerspectives().isEmpty()) {
                showPerspective(getModel().getAllPerspectives().get(0));
            }
        });
    }

    /**
     * Removes all perspectives and the given dockable.
     */
    public void removeFromAllPerspectives(final Dockable dockable) {
        SwingTools.invokeAndWait(() -> {
            if (context.getDockableState(dockable) != null)
                context.unregisterDockable(dockable);
        });
        // Should also be removed from the workspaces, but the
        // vldocking framework does not support this
        removeFromInvisiblePerspectives(dockable);
    }

    /**
     * Removes the given {@link Dockable} from all perspectives except the one currently displayed.
     *
     * @param dockable
     *            the dockable to close
     */

    public void removeFromInvisiblePerspectives(final Dockable dockable) {
        WSDockKey key = new WSDockKey(dockable.getDockKey().getKey());
        for (Perspective persp : model.getAllPerspectives()) {
            if (persp == model.getSelectedPerspective()) {
                continue;
            }
            ((ModifiableWSDesktop) persp.getWorkspace().getDesktop(0)).removeNode(key);
        }
    }


    /**
     * Shows the tab as a child of the given dockable in all perspectives.
     */
    public void showTabInAllPerspectives(final Dockable dockable, final Dockable parent) {
        SwingTools.invokeAndWait(() -> {
            DockableState dstate = context.getDockableState(dockable);
            if (dstate != null && !dstate.isClosed()) {
                return;
            }

            DockingDesktop dockingDesktop = (DockingDesktop) context.getDesktopList().get(0);
            context.registerDockable(dockable);

            WSDockKey parentKey = new WSDockKey(parent.getDockKey().getKey());
            WSDockKey key = new WSDockKey(dockable.getDockKey().getKey());
            for (Perspective persp : getModel().getAllPerspectives()) {
                if (persp == getModel().getSelectedPerspective()) {
                    continue;
                }

                // We don't need to show it if
                // 1. We don't know the parent
                // 2. We already have the child
                /*
                 * boolean containsParent =
                 * persp.getWorkspace().getDesktop(0).containsNode(parentKey); boolean containsChild
                 * = persp.getWorkspace().getDesktop(0).containsNode(key); if (containsParent &&
                 * !containsChild) { persp.getWorkspace().getDesktop(0).createTab(parentKey, key,
                 * 1);
                 * 
                 * // for result tabs, make sure to switch actively viewed tab to new result if
                 * (dockable instanceof ResultTab &&
                 * parent.getDockKey().getKey().equals(DockableResultDisplay.RESULT_DOCK_KEY)) {
                 * persp.getProperties().setNewFocusedResultTab(dockable); } }
                 */
            }

            DockableState[] states = dockingDesktop.getDockables();
            for (DockableState state : states) {
                if (state.getDockable() == parent && !state.isClosed()) {
                    dockingDesktop.createTab(state.getDockable(), dockable, 1, true);
                    break;
                }
            }
        });
    }

    /**
     * Saves all perspectives to the file system.
     */
    public void saveAll() {
        log.debug(LogService.MARKER_DEVELOPMENT, "Saving perspectives");
        if (getModel().getSelectedPerspective() != null) {
            try {
                getModel().getSelectedPerspective().store(context);
            } catch (WorkspaceException e) {
                log.warn(LogService.MARKER_DEVELOPMENT, "Cannot save workspace", e);
            }
        }
        for (Perspective perspective : getModel().getAllPerspectives()) {
            try {
                perspective.save();
            } catch (IOException e) {
                log.warn(LogService.MARKER_DEVELOPMENT, "Cannot save perspective to {}", perspective.getFile(), e);
            }
        }
    }

    /**
     * Loads the default and user perspectives from the file system.
     */
    public void loadAll() {
        makePredefined();
        log.trace(LogService.MARKER_DEVELOPMENT, "Loading perspectives");
        for (Perspective perspective : getModel().getAllPerspectives()) {
            try {
                perspective.load();
                log.trace(LogService.MARKER_DEVELOPMENT, "Load perspective: {}", perspective::getName);
            } catch (Exception e) {
                if (!perspective.isUserDefined()) {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Cannot read view from file: {}. Restoring default", e, e);
                } else {
                    log.warn(LogService.MARKER_DEVELOPMENT, "Cannot read view from file: {}. Clearing view", e, e);
                    perspective.clear();
                }
            }
        }
        File[] userPerspectiveFiles = FileSystemService.getUserSingularityDirectory().listFiles((dir, name) -> name.startsWith("vlperspective-user-"));
        if (userPerspectiveFiles == null)
            return;
        for (File file : userPerspectiveFiles) {
            String name = file.getName();
            name = name.substring("vlperspective-user-".length());
            name = name.substring(0, name.length() - ".xml".length());
            Perspective perspective = getOrCreatePerspective(name, true);
            try {
                perspective.load();
                log.trace(LogService.MARKER_DEVELOPMENT, "Load perspective: {}", perspective::getName);
            } catch (Exception e) {
                log.warn(LogService.MARKER_DEVELOPMENT, "Cannot read view from file: {}. Clearing view", e, e);
                perspective.clear();
            }
        }
    }

    public List<Perspective> getAllPerspectives() {
        return getModel().getAllPerspectives();
    }

    /**
     * Creates a user-defined perspectives, and possibly switches to this new perspective
     * immediately. The new perspective will be a copy of the current one.
     */
    public Perspective getOrCreatePerspective(final String name, boolean userDefined) {
        Perspective perspective = getModel().getPerspective(name);
        if (perspective == null) {
            perspective = getModel().addPerspective(name, userDefined);
        }
        return perspective;
    }

    public void restoreDefaultPerspective() {
        if (!getModel().getSelectedPerspective().isUserDefined()) {
            String viewName = getModel().getSelectedPerspective().getName();
            restoreDefault(viewName);
            tryApplying(getModel().getSelectedPerspective());

            log.info(LogService.MARKER_DEVELOPMENT, "Restoring {} view default", viewName);
        }
    }

    /**
     * Getter for the underlying model.
     *
     * @return The used {@link PerspectiveModel}
     */
    private PerspectiveModel getModel() {
        return model;
    }

    /**
     * Creates the workspace switch action for the given perspective.
     *
     * @param p
     *            the perspective
     * @return the action, never {@code null}
     * @since 8.1
     */
    public WorkspaceAction createPerspectiveAction(final Perspective p) {
        String name = p.getName();
        WorkspaceAction action = new WorkspaceAction(name);

        if (p.isUserDefined()) {
            action.putValue(Action.ACTION_COMMAND_KEY, "perspective-" + name);
            action.putValue(Action.NAME, name);
            ImageIcon createIconSmall = SwingTools.createIcon("16/" + I18N.getGUIMessage("gui.action.workspace_user.icon"));
            ImageIcon createIconLarge = SwingTools.createIcon("24/" + I18N.getGUIMessage("gui.action.workspace_user.icon"));
            action.putValue(Action.LARGE_ICON_KEY, createIconLarge);
            action.putValue(Action.SMALL_ICON, createIconSmall);
            action.putValue(Action.SHORT_DESCRIPTION, I18N.getGUIMessage("gui.action.workspace_user.tip", name));
        }

        return action;
    }

    /**
     * Called as part of the gui shutdown hook.
     */
    public void shutdown() {
        ActionStatisticsCollector.getInstance().stopTimer(getModel().getSelectedPerspective());
    }

    /**
     * Registers a new {@link PerspectiveChangeListener}.
     *
     * @param listener
     *            the listener which should be notified about perspective changes.
     */
    public void addPerspectiveChangeListener(PerspectiveChangeListener listener) {
        getModel().addPerspectiveChangeListener(listener);
    }

    public boolean isValidPerspectiveName(String perspectiveName) {
        return getModel().isValidName(perspectiveName);
    }

    public Perspective getPerspective(String name) {
        return getModel().getPerspective(name);
    }

    public void removePerspectiveChangeListener(PerspectiveChangeListener listener) {
        getModel().removePerspectiveChangeListener(listener);
    }

    public Perspective getSelectedPerspective() {
        return getModel().getSelectedPerspective();
    }

    /**
     * The {@link PerspectiveModel} is managed by a {@link PerspectiveController} and stores all
     * necessary information about the registered perspectives. This model can notify listeners/
     * observers about perspective changes and new registered perspectives.
     *
     * @author Marcel Michel
     * @since 7.0.0
     */
    private static class PerspectiveModel {

        private final Map<String, Perspective> perspectives = new LinkedHashMap<>();
        private final EventListenerList perspectiveChangeListenerList = new EventListenerList();

        private Perspective selectedPerspective;


        /**
         * Creates a new perspective, and possibly switches to this new perspective immediately. The
         * new perspective will be a copy of the current one.
         *
         * @throws IllegalArgumentException
         *             if name is already used
         */
        public Perspective addPerspective(final String name, final boolean userDefined) {
            if (!isValidName(name)) {
                throw new IllegalArgumentException("Invalid or duplicate view name: " + name);
            }
            final Perspective p = new Perspective(name);
            p.setUserDefined(userDefined);
            perspectives.put(name, p);

            fireAdded(p);
            return p;
        }

        /**
         * Removes the given perspective by name from the model.
         *
         * @param name
         *            the name of the perspective which should be removed
         */
        public void deletePerspective(final String name) {
            if (perspectives.containsKey(name)) {
                deletePerspective(perspectives.get(name));
            }
        }

        /**
         * Removes the given perspective from the model.
         *
         * @param name
         *            the perspective which should be removed
         */
        public void deletePerspective(final Perspective p) {
            if (!p.isUserDefined()) {
                return;
            }
            perspectives.remove(p.getName());
            p.delete();
            fireRemoved(p);
        }

        /**
         * Gets a perspective by name.
         *
         * @param name
         *            the name of the perspective
         * @return the resolved {@link Perspective}
         * @throws NoSuchElementException
         *             if the perspective is not known
         */
        public Perspective getPerspective(final String name) {
            return perspectives.get(name);
        }

        /**
         * Gets a perspective by name.
         *
         * @param name
         *            the name of the perspective
         * @return the resolved {@link Perspective}
         * @throws NoSuchElementException
         *             if the perspective is not known
         */
        public Perspective getPerspectiveOrThrow(final String name) {
            Perspective result = getPerspective(name);
            if (result != null) {
                return result;
            } else {
                throw new NoSuchElementException("No such view: " + name);
            }
        }

        /**
         * Getter for all registered perspectives
         *
         * @return all perspectives as {@link List}
         */
        public List<Perspective> getAllPerspectives() {
            return new ArrayList<>(perspectives.values());
        }

        /**
         * Registers a new {@link PerspectiveChangeListener}.
         *
         * @param listener
         *            the listener which should be notified about perspective changes.
         */
        public void addPerspectiveChangeListener(final PerspectiveChangeListener listener) {
            perspectiveChangeListenerList.add(PerspectiveChangeListener.class, listener);
        }

        /**
         * Removes the given {@link PerspectiveChangeListener} from the listener list.
         *
         * @param listener
         *            the listener which should be removed
         * @return {@code true} if the listener could be removed, otherwise {@code false}
         */
        public void removePerspectiveChangeListener(final PerspectiveChangeListener listener) {
            perspectiveChangeListenerList.remove(PerspectiveChangeListener.class, listener);
        }

        /**
         * Getter for the current selected perspective.
         *
         * @return the selected perspective
         */
        public Perspective getSelectedPerspective() {
            return selectedPerspective;
        }

        /**
         * Updates the selected perspective and notifies the {@link PerspectiveChangeListener}.
         *
         * @param perspective
         *            the new selected perspective
         */
        void setSelectedPerspective(Perspective perspective) {
            if (selectedPerspective == perspective) {
                return;
            }
            Perspective before = selectedPerspective;
            selectedPerspective = perspective;
            fireSelectionChange(before, selectedPerspective);
        }

        /**
         * Checks if the given string is valid as name of a new perspective.
         *
         * @param name
         * @return validity
         */
        public boolean isValidName(final String name) {
            if (name == null) {
                return false;
            }
            if (name.trim().isEmpty()) {
                return false;
            }
            for (Perspective perspective : perspectives.values()) {
                if (perspective.getName().equalsIgnoreCase(name)) {
                    return false;
                }
            }
            return true;
        }

        private void fireAdded(Perspective newPerspective) {
            PerspectiveChangeListener[] listeners = perspectiveChangeListenerList.getListeners(PerspectiveChangeListener.class);
            for (PerspectiveChangeListener listener : listeners) {
                listener.onPerspectiveAdd(newPerspective);
            }
        }

        private void fireRemoved(Perspective newPerspective) {
            PerspectiveChangeListener[] listeners = perspectiveChangeListenerList.getListeners(PerspectiveChangeListener.class);
            for (PerspectiveChangeListener listener : listeners) {
                listener.onPerspectiveRemove(newPerspective);
            }
        }

        /**
         * Notifies the registered {@link PerspectiveChangeListener}s about the
         * {@link #selectedPerspective}.
         */
        public void fireSelectionChange(Perspective before, Perspective after) {
            PerspectiveChangeListener[] listeners = perspectiveChangeListenerList.getListeners(PerspectiveChangeListener.class);
            for (PerspectiveChangeListener listener : listeners) {
                listener.onPerspectiveChange(before, after);
            }
        }
    }
}
