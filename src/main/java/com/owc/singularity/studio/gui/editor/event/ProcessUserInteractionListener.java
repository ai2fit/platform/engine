/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.event;


import java.util.EventListener;
import java.util.List;

import javax.swing.JPopupMenu;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ZoomState;


/**
 * Interface for callbacks received from the {@link ProcessEditorPanel} when the user opens a
 * context menu.
 * 
 * @author Simon Fischer
 * 
 */
public interface ProcessUserInteractionListener extends EventListener {

    /** Called on right-clicking an operator. */
    default void onBeforeOperatorContextMenuOpen(JPopupMenu menu, Operator operator) {}

    /** Called on right-clicking a port. */
    default void onBeforePortContextMenuOpen(JPopupMenu menu, Port port) {}

    /** Called when an operator is moved. */
    default void onOperatorMoved(Operator op) {}

    /** Called when the currently displayed {@link OperatorChain} changed. */
    default void onDisplayedChainChanged(OperatorChain displayedChain) {}

    default void onZoomStateChange(ZoomState newState) {}

    default void onOperatorSelectionChange(List<Operator> selectedOperators) {}


    /**
     * Notifies the listener that the view on a process was changed, e.g. when the user
     * enters/leaves a subprocess in the process design view.
     *
     * @param process
     */
    default void onProcessViewChange(AbstractPipeline process, OperatorChain from, OperatorChain to) {}
}
