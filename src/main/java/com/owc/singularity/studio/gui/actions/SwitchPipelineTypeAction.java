/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * Start the corresponding action.
 *
 * @author Marco Boeck
 */
public class SwitchPipelineTypeAction extends ResourceAction {

    private static final long serialVersionUID = -1317229512005928206L;
    private Class<? extends AbstractPipeline> targetClass;


    public SwitchPipelineTypeAction(String subKey, Class<? extends AbstractPipeline> targetClass) {
        super(false, "switch_pipeline_type." + subKey);
        this.targetClass = targetClass;
        setCondition(Condition.EDIT_IN_PROGRESS, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        AbstractPipeline currentPipeline = MainFrame.INSTANCE.getProcess();
        RepositoryPath currentPath = currentPipeline.getPath();

        try {
            AbstractPipeline newPipeline = targetClass.getConstructor().newInstance();
            AbstractRootOperator newRoot = newPipeline.getRootOperator();
            AbstractRootOperator currentRoot = currentPipeline.getRootOperator();

            // check whether we can copy all subprocesses
            if (currentRoot.getNumberOfSubprocesses() > newRoot.getNumberOfSubprocesses() && !newRoot.areSubprocessesExtendable()) {
                if (ConfirmDialog.CANCEL_OPTION == SwingTools.showConfirmDialog(MainFrame.INSTANCE, "pipeline.conversion_looses_operators",
                        ConfirmDialog.OK_CANCEL_OPTION, newRoot.getNumberOfSubprocesses()))
                    return;
            }

            // if we continue steal operators
            newRoot.getSubprocess(0).stealOperatorsFrom(currentRoot.getSubprocess(0));
            if (newRoot.areSubprocessesExtendable() && currentRoot.areSubprocessesExtendable()) {
                for (int i = 1; i < currentRoot.getNumberOfSubprocesses(); i++) {
                    newRoot.addSubprocess(i);
                    newRoot.getSubprocess(i).stealOperatorsFrom(currentRoot.getSubprocess(i));
                }
            }

            // copy parameters
            for (String key : currentRoot.getParameters().getDefinedKeys()) {
                newRoot.setParameter(key, currentRoot.getParameters().getParameterOrNull(key));
            }

            newPipeline.setPath(currentPath);
            MainFrame.INSTANCE.replaceProcess(currentPipeline, newPipeline);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                | SecurityException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
}
