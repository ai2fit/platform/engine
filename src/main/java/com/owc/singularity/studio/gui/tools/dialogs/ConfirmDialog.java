/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.dialogs;


import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

import javax.swing.*;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceActionAdapter;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * @author Tobias Malbrecht, Adrian Wilke
 */
public class ConfirmDialog extends ButtonDialog {

    private static final long serialVersionUID = -5825873580778775409L;

    public static final int OK_OPTION = JOptionPane.OK_OPTION;

    public static final int YES_OPTION = JOptionPane.YES_OPTION;

    public static final int NO_OPTION = JOptionPane.NO_OPTION;

    public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;

    public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;

    public static final int OK_CANCEL_OPTION = JOptionPane.OK_CANCEL_OPTION;

    public static final int YES_NO_OPTION = JOptionPane.YES_NO_OPTION;

    public static final int YES_NO_CANCEL_OPTION = JOptionPane.YES_NO_CANCEL_OPTION;
    private final String okButtonKey;
    private final String cancelButtonKey;
    private final String yesButtonKey;
    private final String noButtonKey;

    protected int returnOption = CANCEL_OPTION;

    private JCheckBox dontAskAgainCheckbox = null;
    private JButton okButton;
    private JButton cancelButton;
    private JButton yesButton;
    private JButton noButton;

    /**
     * Creates a confirm dialog where the user can chose his action.
     *
     * @param owner
     *            the owner of the dialog
     * @param key
     *            the i18n key
     * @param mode
     *            see the static constants of {@link ConfirmDialog}
     * @param showAskAgainCheckbox
     *            the user can chose to never be asked again
     * @param arguments
     *            additional i18n arguments
     */
    public ConfirmDialog(Dialog owner, String key, int mode, boolean showAskAgainCheckbox, Object... arguments) {
        this((Window) owner, key, mode, showAskAgainCheckbox, arguments);
    }

    /**
     * Creates a confirm dialog where the user can chose his action.
     *
     * @param owner
     *            the owner of the dialog
     * @param key
     *            the i18n key
     * @param mode
     *            see the static constants of {@link ConfirmDialog}
     * @param showAskAgainCheckbox
     *            the user can chose to never be asked again
     * @param arguments
     *            additional i18n arguments
     * @since 6.5.0
     */
    public ConfirmDialog(Window owner, String key, int mode, boolean showAskAgainCheckbox, Object... arguments) {
        this(owner, key, mode, showAskAgainCheckbox, null, null, null, null, arguments);
    }

    /**
     * Creates a confirm dialog where the user can chose his action.
     *
     * @param owner
     *            the owner of the dialog
     * @param key
     *            the i18n key
     * @param mode
     *            see the static constants of {@link ConfirmDialog}
     * @param showAskAgainCheckbox
     *            the user can chose to never be asked again
     * @param arguments
     *            additional i18n arguments
     * @since 6.5.0
     */
    public ConfirmDialog(Window owner, String key, int mode, boolean showAskAgainCheckbox, String okButtonKey, String cancelButtonKey, String yesButtonKey,
            String noButtonKey, Object... arguments) {
        super(owner, "confirm." + key, ModalityType.APPLICATION_MODAL, arguments);
        this.okButtonKey = Objects.requireNonNullElse(okButtonKey, "ok");
        this.cancelButtonKey = Objects.requireNonNullElse(cancelButtonKey, "cancel");
        this.yesButtonKey = Objects.requireNonNullElse(yesButtonKey, "confirm.yes");
        this.noButtonKey = Objects.requireNonNullElse(noButtonKey, "confirm.no");
        constructConfirmDialog(mode, showAskAgainCheckbox);
    }

    private void constructConfirmDialog(int mode, boolean showAskAgainCheckbox) {
        Collection<AbstractButton> buttons = new LinkedList<>();
        switch (mode) {
            case OK_CANCEL_OPTION:
                buttons.add(okButton = makeOkButton());
                buttons.add(cancelButton = makeCancelButton());
                break;
            case YES_NO_OPTION:
                buttons.add(yesButton = makeYesButton());
                buttons.add(noButton = makeNoButton());
                break;
            case YES_NO_CANCEL_OPTION:
                buttons.add(yesButton = makeYesButton());
                buttons.add(noButton = makeNoButton());
                buttons.add(cancelButton = makeCancelButton());
                break;
            default:
                break;
        }

        if (showAskAgainCheckbox) {
            this.dontAskAgainCheckbox = new JCheckBox(new ResourceActionAdapter("dont_ask_again"));
        }
        layoutDefault(this.dontAskAgainCheckbox, buttons);
    }

    @Override
    protected Icon getInfoIcon() {
        String iconKey = I18N.getGUIMessageOrNull(getKey() + ".icon");
        if (iconKey == null) {
            return SwingTools.createIcon("48/" + I18N.getGUIMessage("gui.dialog.confirm.icon"), IconSize.HUGE);
        } else {
            return SwingTools.createIcon("48/" + iconKey, IconSize.HUGE);
        }
    }

    @Override
    protected JButton makeOkButton() {
        JButton okButton = new JButton(new ResourceAction(okButtonKey) {

            private static final long serialVersionUID = -8887199234055845095L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                ok();
            }
        });
        getRootPane().setDefaultButton(okButton);
        return okButton;
    }

    @Override
    protected JButton makeCancelButton() {
        return makeCancelButton(cancelButtonKey);
    }

    @Override
    protected JButton makeCancelButton(String i18nKey) {
        ResourceAction cancelAction = new ResourceAction(i18nKey) {

            private static final long serialVersionUID = -8887199234055845095L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                returnOption = CANCEL_OPTION;
                cancel();
            }
        };
        JButton cancelButton = new JButton(cancelAction);
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "CANCEL");
        getRootPane().getActionMap().put("CANCEL", cancelAction);

        return cancelButton;
    }

    protected JButton makeYesButton() {
        return makeYesButton(yesButtonKey);
    }

    protected JButton makeYesButton(String i18nKey) {
        JButton yesButton = new JButton(new ResourceAction(i18nKey) {

            private static final long serialVersionUID = -8887199234055845095L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                returnOption = YES_OPTION;
                yes();
            }
        });
        getRootPane().setDefaultButton(yesButton);
        return yesButton;
    }

    protected JButton makeNoButton() {
        return makeNoButton(noButtonKey);
    }

    protected JButton makeNoButton(String i18nKey) {
        ResourceAction noAction = new ResourceAction(i18nKey) {

            private static final long serialVersionUID = -8887199234055845095L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                returnOption = NO_OPTION;
                no();
            }
        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "NO");
        getRootPane().getActionMap().put("NO", noAction);
        return new JButton(noAction);
    }

    @Override
    protected void ok() {
        setReturnOption(OK_OPTION);
        dispose();
    }

    @Override
    protected void cancel() {
        setReturnOption(CANCEL_OPTION);
        dispose();
    }

    protected void yes() {
        setReturnOption(YES_OPTION);
        dispose();
    }

    protected void no() {
        setReturnOption(NO_OPTION);
        dispose();
    }

    public int getReturnOption() {
        return returnOption;
    }

    public boolean isDontAskAgainCheckboxSelected() {
        if (dontAskAgainCheckbox == null) {
            return false;
        } else {
            return dontAskAgainCheckbox.isSelected();
        }
    }

    protected void setReturnOption(int option) {
        this.returnOption = option;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getYesButton() {
        return yesButton;
    }

    public JButton getNoButton() {
        return noButton;
    }

    /**
     * Shows a dialog and returns the return code of the dialog.
     *
     * @param owner
     *            the owner where the dialog will be shown in
     * @param mode
     *            One of {@link #OK_CANCEL_OPTION}, {@link #YES_NO_OPTION} and
     *            {@link #YES_NO_CANCEL_OPTION}
     * @param key
     *            The i18n-key. Will be prepended with gui.dialog.confirm and prefixed with .title,
     *            .message and .icon .
     * @param i18nArgs
     *            Arguments to the i18n string.
     * @return The return code of the dialog, one of the constants specified above.
     */
    public static int showConfirmDialog(Window owner, int mode, String key, Object... i18nArgs) {
        return showConfirmDialogWithOptionalCheckbox(owner, key, mode, null, -1, false, i18nArgs);
    }

    /**
     * Shows a dialog and returns the return code of the dialog.
     *
     * @param owner
     *            the owner where the dialog will be shown in
     * @param key
     *            The i18n-key. Will be prepended with gui.dialog.confirm and prefixed with .title,
     *            .message and .icon .
     * @param mode
     *            One of {@link #OK_CANCEL_OPTION}, {@link #YES_NO_OPTION} and
     *            {@link #YES_NO_CANCEL_OPTION}
     * @param propertyConfirmExit
     *            If null, the dialog is shown in any case. If not null, the parameter specified
     *            will be from fetched from {@link PropertyService}. If it is "false", the dialog
     *            will not be shown.
     * @param defaultOption
     *            Will be returned if the dialog is not shown (see parameter propertyConfirmExit.
     * @param i18nArgs
     *            Arguments to the i18n string.
     * @return The return code of the dialog, one of the constants specified above.
     * @since 6.5.0
     */
    public static int showConfirmDialog(Window owner, String key, int mode, String propertyConfirmExit, int defaultOption, Object... i18nArgs) {
        return showConfirmDialogWithOptionalCheckbox(owner, key, mode, propertyConfirmExit, defaultOption, true, i18nArgs);
    }

    /**
     * Same as {@link #showConfirmDialog(Window, String, int, String, int, Object...)} with the
     * additional option showAskAgainCheckbox.
     *
     * @since 6.5.0
     */
    public static int showConfirmDialogWithOptionalCheckbox(Window owner, String key, int mode, String propertyConfirmExit, int defaultOption,
            boolean showAskAgainCheckbox, Object... i18nArgs) {
        if (propertyConfirmExit == null) {
            ConfirmDialog dialog = new ConfirmDialog(owner, key, mode, showAskAgainCheckbox, i18nArgs);
            dialog.setVisible(true);
            return dialog.getReturnOption();
        } else {
            String askProperty = PropertyService.getParameterValue(propertyConfirmExit);
            if (!"false".equals(askProperty)) {
                ConfirmDialog dialog = new ConfirmDialog(owner, key, mode, showAskAgainCheckbox, i18nArgs);
                dialog.setVisible(true);
                PropertyService.setParameterValue(propertyConfirmExit, Boolean.toString(!dialog.dontAskAgainCheckbox.isSelected()));
                PropertyService.saveParameters();
                return dialog.getReturnOption();
            } else {
                return defaultOption;
            }
        }
    }
}
