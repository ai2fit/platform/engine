package com.owc.singularity.studio.gui.tools.actions;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.Serial;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.*;
import javax.swing.table.TableModel;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.LoggedAbstractAction;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationModel;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationSettings;
import com.owc.singularity.studio.gui.tools.ExtendedJPopupMenu;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.components.ButtonBarCardPanel;


/**
 * This action is only used by the {@link ExtendedJTable} to open the Visualisation panel for the
 * selected column.
 *
 * @author Alexander Mahler
 */
public class OpenColumnChartAction extends LoggedAbstractAction {

    private static final String ICON_NAME = "presentation_chart.png";
    private static final String VISUALIZATIONS_CLASS_NAME = "com.owc.singularity.studio.gui.plotter.PlotterPanel";
    private static final String GET_PLOTTER_SETTINGS = "getPlotterSettings";
    @Serial
    private static final long serialVersionUID = -1042609307057346686L;
    private final ExtendedJTable table;

    public OpenColumnChartAction(ExtendedJTable table, IconSize size) {
        super("Open Visualizations", SwingTools.createIcon(size.getSize() + "/" + ICON_NAME));
        this.table = table;
        putValue(SHORT_DESCRIPTION, "Open Visualisation");
        putValue(MNEMONIC_KEY, KeyEvent.VK_V);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        int selectedColumnIndex = this.table.getSelectedColumn();
        if (selectedColumnIndex != -1) {
            TableModel model = this.table.getModel();
            if (model != null) {
                String attributeName = model.getColumnName(selectedColumnIndex);
                // look up the panel invoking the pop up invoking the action
                Container parent = ((JComponent) e.getSource()).getParent();
                ButtonBarCardPanel cardPanel = (ButtonBarCardPanel) SwingUtilities.getAncestorOfClass(ButtonBarCardPanel.class,
                        ((ExtendedJPopupMenu<?>) parent).getInvoker());

                // select the visualizations view
                cardPanel.selectCard("plot_view");

                // get the opened visualization
                JPanel outerPanel = (JPanel) cardPanel.getShownComponent();
                for (Component innerComp : outerPanel.getComponents()) {
                    if (innerComp != null && innerComp.getClass().getName().equals(VISUALIZATIONS_CLASS_NAME)) {
                        // adjust settings
                        int exampleCount = model.getRowCount();
                        try {
                            Class<?> columnClass = model.getColumnClass(selectedColumnIndex);
                            if (columnClass.isAssignableFrom(String.class)) {
                                Method showHistogramChart = innerComp.getClass().getDeclaredMethod(GET_PLOTTER_SETTINGS);
                                showHistogramChart.setAccessible(true);
                                PlotterConfigurationModel configurationModel = (PlotterConfigurationModel) showHistogramChart.invoke(innerComp);
                                configurationModel.setPlotter(PlotterConfigurationModel.BAR_CHART);
                                configurationModel.setParameterAsString(PlotterConfigurationSettings.GROUP_BY_COLUMN, attributeName);
                            } else if (columnClass.isAssignableFrom(Double.class) || columnClass.isAssignableFrom(Long.class)) {
                                Method showHistogramChart = innerComp.getClass().getDeclaredMethod(GET_PLOTTER_SETTINGS);
                                showHistogramChart.setAccessible(true);
                                PlotterConfigurationModel configurationModel = (PlotterConfigurationModel) showHistogramChart.invoke(innerComp);
                                configurationModel.setPlotter(PlotterConfigurationModel.HISTOGRAM_PLOT);
                                configurationModel.setParameterAsString(PlotterConfigurationSettings.AXIS_PLOT_COLUMNS, attributeName);
                                configurationModel.setParameterAsInt(PlotterConfigurationSettings.NUMBER_OF_BINS, Math.min(exampleCount, 10));
                            }
                        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e1) {
                            LogService.getRoot().warn("com.owc.singularity.studio.gui.viewer.metadata.actions.OpenChartAction.cannot_show_visualization", e1);
                        }
                        break;
                    }
                }
            }
        }
    }

}
