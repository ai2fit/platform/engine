/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */

package com.owc.singularity.studio.gui.repository;


import java.util.Comparator;

import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * These are the sorting methods, which are supported by the {@link RepositoryBrowserDockable}.
 *
 * @author Marcel Seifert
 * @since 7.4
 *
 */
public enum RepositorySortingMethod implements Comparator<RepositoryTreeNode> {

    NAME_ASC(RepositoryPathComparators.ENTRY_COMPARATOR), LAST_MODIFIED_DATE_DESC(RepositoryPathComparators.ENTRY_COMPARATOR_LAST_MODIFIED);

    private final Comparator<RepositoryTreeNode> comparator;

    RepositorySortingMethod(Comparator<RepositoryTreeNode> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int compare(RepositoryTreeNode o1, RepositoryTreeNode o2) {
        return comparator.compare(o1, o2);
    }
}
