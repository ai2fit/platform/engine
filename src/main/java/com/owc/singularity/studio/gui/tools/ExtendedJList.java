/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;


import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;


/**
 * Extended JList which provides tool tips in combination with an {@link ExtendedListModel}.
 *
 * @author Tobias Malbrecht, Ingo Mierswa
 */
public class ExtendedJList<E> extends MenuShortcutJList<E> {

    public static final long serialVersionUID = 9032182018402L;

    private int preferredWidth = -1;

    private boolean showPopopUpMenu;

    public ExtendedJList(ExtendedListModel<E> model) {
        this(model, -1);

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(final MouseEvent e) {
                mouseReleased(e);
            }

            @Override
            public void mousePressed(final MouseEvent e) {
                mouseReleased(e);
            }

            @Override
            public void mouseReleased(final MouseEvent e) {
                if (showPopopUpMenu) {
                    if (e.isPopupTrigger()) {
                        Point p = e.getPoint();
                        int row = locationToIndex(p);


                        // don't do anything when outside of table
                        if (row < 0) {
                            return;
                        }
                        ListSelectionModel selectionModel = getSelectionModel();
                        // only set cell selection if clicked cell is outside current selection
                        if (row < selectionModel.getMinSelectionIndex() || row > selectionModel.getMaxSelectionIndex() - 1) {
                            if (row < getModel().getSize()) {
                                // needed because sometimes row could be outside [0,
                                // getRowCount()-1]
                                selectionModel.setSelectionInterval(row, row);
                            }
                        }

                        ExtendedJPopupMenu<E> menu = createPopupMenu();

                        showPopupMenu(menu, e.getPoint());
                    }
                }
            }
        });
    }

    public ExtendedJList(ExtendedListModel<E> model, int preferredWidth) {
        super(model);
        this.setCellRenderer(new ExtendedListCellRenderer(model));

        this.preferredWidth = preferredWidth;
    }

    /** Returns the tooltip of a list entry. */
    @Override
    public String getToolTipText(MouseEvent e) {
        int index = locationToIndex(e.getPoint());
        return ((ExtendedListModel<?>) getModel()).getToolTip(index);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dim = super.getPreferredSize();
        if (this.preferredWidth != -1) {
            if (preferredWidth < dim.getWidth()) {
                return new Dimension(preferredWidth, (int) dim.getHeight());
            } else {
                return dim;
            }
        } else {
            return dim;
        }
    }

    protected void showPopupMenu(final JPopupMenu menu, final Point location) {
        menu.show(this, (int) location.getX(), (int) location.getY());
    }

    public ExtendedJPopupMenu<E> createPopupMenu() {
        ExtendedJPopupMenu<E> menu = new ExtendedJPopupMenu<>((x, y) -> getModel().getElementAt(locationToIndex(new Point(x, y))));
        populatePopupMenu(menu);
        return menu;
    }

    public void populatePopupMenu(final ExtendedJPopupMenu<E> menu) {

    }

    public void setShowPopupMenu(final boolean showPopupMenu) {
        this.showPopopUpMenu = showPopupMenu;
    }

}
