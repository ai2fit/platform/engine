package com.owc.singularity.studio.gui.parameters;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.owc.singularity.engine.pipeline.parameter.ParameterTypeMultipleSelection;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.FilterTextField;
import com.owc.singularity.studio.gui.tools.FilterableListModel;
import com.owc.singularity.studio.gui.tools.ResourceAction;

/**
 * Generalized GUI Dialog for selecting multiple options for a
 * {@link ParameterTypeMultipleSelection} parameter.
 *
 * @author Hatem Hamad
 */
public class MultipleSelectionParameterDialog extends ParameterDialog {

    private static final long serialVersionUID = 5396725225122306231L;
    private final String i18NKey;
    private final List<String> items;

    private final List<String> selectedItems;

    private final FilterTextField itemSearchField;

    private final FilterTextField selectedItemSearchField;

    private final FilterableListModel<String> itemListModel;

    private final FilterableListModel<String> selectedItemListModel;

    private final JList<String> itemList;

    private final JList<String> selectedItemList;

    private final Action selectOptionsAction;

    private final Action deselectOptionsAction;

    protected String getSelectActionKey() {
        return i18NKey + ".select";
    }

    protected String getClearActionKey() {
        return i18NKey + ".clear";
    }

    protected String getApplyActionKey() {
        return i18NKey + ".apply";
    }

    protected String getDeselectActionKey() {
        return i18NKey + ".deselect";
    }

    protected String getAddActionKey() {
        return i18NKey + ".add";
    }

    private String getOptionsBorderMessageKey() {
        return ".options.border";
    }

    protected String getSelectedOptionsBorderMessageKey() {
        return ".selected_options.border";
    }


    /**
     * Constructor with the default options
     *
     * @see #MultipleSelectionParameterDialog(String, ParameterTypeMultipleSelection, Collection,
     *      boolean)
     */
    public MultipleSelectionParameterDialog(final ParameterTypeMultipleSelection type, Collection<String> preselectedItems) {
        this("options", type, preselectedItems, true);
    }

    /**
     * @param i18NKey
     *            the base i18N key that intern actions/messages will base their key on.
     * @param parameter
     *            the parameter instance
     * @param preselectedItems
     *            the preselected options
     * @param sort
     *            if {@code true}, options on the right (selected options) side will be sorted
     *            alpha-numerically.
     */
    public MultipleSelectionParameterDialog(String i18NKey, final ParameterTypeMultipleSelection parameter, Collection<String> preselectedItems, boolean sort) {
        super(parameter, i18NKey);
        this.i18NKey = i18NKey;
        this.selectOptionsAction = new ResourceAction(true, getSelectActionKey()) {

            private static final long serialVersionUID = -3046621274306353077L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                List<String> selectedValues = itemList.getSelectedValuesList();
                itemList.setSelectedIndices(new int[] {});
                for (String item : selectedValues) {
                    selectedItemListModel.addElement(item);
                    itemListModel.removeElement(item);
                    selectedItems.add(item);
                    items.remove(item);
                }
            }
        };
        this.deselectOptionsAction = new ResourceAction(true, getDeselectActionKey()) {

            private static final long serialVersionUID = -3046621278326353077L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                int[] indices = selectedItemList.getSelectedIndices();
                selectedItemList.setSelectedIndices(new int[] {});
                for (int i = indices.length - 1; i >= 0; i--) {
                    String item = selectedItemListModel.getElementAt(indices[i]);
                    itemListModel.addElement(item);
                    selectedItemListModel.removeElementAt(indices[i]);
                    items.add(item);
                    selectedItems.remove(item);
                }
            }
        };
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        List<String> attributeNamesAndTypes = parameter.getOptions();
        items = attributeNamesAndTypes.stream().filter(att -> !preselectedItems.contains(att)).collect(Collectors.toList());
        selectedItems = new ArrayList<>(preselectedItems);
        if (sort) {
            items.sort(FilterableListModel.STRING_COMPARATOR);
            selectedItems.sort(FilterableListModel.STRING_COMPARATOR);
        }

        itemListModel = new FilterableListModel<>(items, false);
        selectedItemListModel = new FilterableListModel<>(selectedItems, false);
        if (sort) {
            itemListModel.setComparator(FilterableListModel.STRING_COMPARATOR);
            selectedItemListModel.setComparator(FilterableListModel.STRING_COMPARATOR);
        }

        itemSearchField = new FilterTextField();
        itemSearchField.addFilterListener(itemListModel);
        JButton itemSearchFieldClearButton = new JButton(new ResourceAction(true, getClearActionKey()) {

            private static final long serialVersionUID = -3046621278306953077L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                itemSearchField.clearFilter();
                itemSearchField.requestFocusInWindow();
            }
        });
        JPanel itemSearchFieldPanel = new JPanel(new GridBagLayout());
        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.fill = GridBagConstraints.BOTH;
        itemSearchFieldPanel.add(itemSearchField, c);

        c.gridx = 1;
        c.weightx = 0;
        itemSearchFieldPanel.add(itemSearchFieldClearButton, c);

        itemList = new JList<>(itemListModel);
        itemList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    selectOptionsAction.actionPerformed(null);
                }
            }
        });
        JScrollPane itemListPane = new ExtendedJScrollPane(itemList);
        itemListPane.setBorder(createBorder());
        JPanel itemListPanel = new JPanel(new GridBagLayout());

        c.insets = new Insets(4, 4, 4, 4);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.fill = GridBagConstraints.BOTH;
        itemListPanel.add(itemSearchFieldPanel, c);

        c.gridy = 1;
        c.weighty = 1;
        itemListPanel.add(itemListPane, c);
        itemListPanel.setBorder(createTitledBorder(I18N.getGUIMessage(getKey() + getOptionsBorderMessageKey())));

        selectedItemSearchField = new FilterTextField();
        selectedItemSearchField.addFilterListener(selectedItemListModel);
        JButton selectedItemSearchFieldClearButton = new JButton(new ResourceAction(true, getClearActionKey()) {

            private static final long serialVersionUID = -3046621278306353032L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                selectedItemSearchField.clearFilter();
                selectedItemSearchField.requestFocusInWindow();
            }
        });
        JPanel selectedItemSearchFieldPanel = new JPanel(new GridBagLayout());
        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.fill = GridBagConstraints.BOTH;
        selectedItemSearchFieldPanel.add(selectedItemSearchField, c);

        JButton addValueButton = new JButton(new ResourceAction(true, getAddActionKey()) {

            private static final long serialVersionUID = 41667438961831572L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                String item = selectedItemSearchField.getText();
                selectedItemSearchField.clearFilter();
                selectedItemSearchField.requestFocusInWindow();
                if (selectedItems.contains(item)) {
                    return;
                }
                if (items.contains(item)) {
                    selectedItemListModel.addElement(item);
                    itemListModel.removeElement(item);
                    selectedItems.add(item);
                    items.remove(item);
                    return;
                }
                selectedItems.add(item);
                selectedItemListModel.addElement(item);
            }
        });

        c.gridx = 1;
        c.weightx = 0;
        selectedItemSearchFieldPanel.add(addValueButton, c);

        c.gridx = 2;
        c.weightx = 0;
        selectedItemSearchFieldPanel.add(selectedItemSearchFieldClearButton, c);

        selectedItemList = new JList<>(selectedItemListModel);
        selectedItemList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    deselectOptionsAction.actionPerformed(null);
                }
            }
        });
        JScrollPane selectedItemListPane = new ExtendedJScrollPane(selectedItemList);
        selectedItemListPane.setBorder(createBorder());
        JPanel selectedItemListPanel = new JPanel(new GridBagLayout());

        c.insets = new Insets(4, 4, 4, 4);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.fill = GridBagConstraints.BOTH;
        selectedItemListPanel.add(selectedItemSearchFieldPanel, c);

        c.gridy = 1;
        c.weighty = 1;
        selectedItemListPanel.add(selectedItemListPane, c);
        selectedItemListPanel.setBorder(createTitledBorder(I18N.getGUIMessage(getKey() + getSelectedOptionsBorderMessageKey())));

        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        panel.add(itemListPanel, c);

        JPanel midButtonPanel = new JPanel(new GridLayout(2, 1));
        JButton selectButton = new JButton(selectOptionsAction);
        JButton deselectButton = new JButton(deselectOptionsAction);
        midButtonPanel.add(deselectButton, 0, 0);
        midButtonPanel.add(selectButton, 1, 0);
        c.insets = new Insets(4, 4, 4, 4);
        c.gridx = 1;
        c.weightx = 0;
        c.weighty = 1;
        c.fill = GridBagConstraints.NONE;
        panel.add(midButtonPanel, c);

        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        panel.add(selectedItemListPanel, c);
        Dimension d = panel.getPreferredSize();
        d.setSize(d.getWidth() / 2, d.getHeight());
        itemListPanel.setPreferredSize(d);
        selectedItemListPanel.setPreferredSize(d);

        layoutDefault(panel, NORMAL, makeOkButton(getApplyActionKey()), makeCancelButton());
    }


    public Collection<String> getSelectedOptions() {
        return selectedItems;
    }

}
