package com.owc.singularity.studio.gui.editor.action;

import java.util.*;

import javax.swing.*;

import com.owc.singularity.studio.gui.tools.ResourceAction;

public class DefaultProcessActionFactory {

    private static DefaultProcessActionFactory INSTANCE;

    public static DefaultProcessActionFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DefaultProcessActionFactory();
        }
        return INSTANCE;
    }

    private final Set<ResourceAction> registeredActions = new LinkedHashSet<>();

    /**
     * This method returns a list of actions shown in the context menu when right-clicking in a
     * process panel. This method creates instances via reflection of all registered actions.
     */
    public List<Action> getActions() {
        return new ArrayList<>(registeredActions);
    }

    /**
     * Adds the given {@link ResourceAction} extending class to the popup menu actions at the given
     * index.
     * <p>
     * The class <b>MUST</b> have one empty public constructor. Otherwise, creating the action via
     * reflection will fail.
     *
     * @param action
     *            a {@link ResourceAction}
     */
    public void addAction(ResourceAction action) {
        if (action == null) {
            throw new IllegalArgumentException("actionClass and condition must not be null!");
        }

        synchronized (registeredActions) {
            registeredActions.add(action);
        }
    }

    /**
     * Removes the given action from the popup menu actions.
     *
     * @param action
     *            the class of the {@link ResourceAction} to remove
     */
    public void removeAction(ResourceAction action) {
        synchronized (registeredActions) {
            registeredActions.removeIf(registeredAction -> registeredAction.getClass().equals(action.getClass()));
        }
    }
}
