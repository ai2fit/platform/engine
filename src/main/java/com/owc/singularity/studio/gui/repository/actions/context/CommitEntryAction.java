package com.owc.singularity.studio.gui.repository.actions.context;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.exception.NoChangesException;
import com.owc.singularity.repository.git.UncommittedEntryVersion;
import com.owc.singularity.repository.vcs.MountTreeChange;
import com.owc.singularity.repository.vcs.MountTreeDifference;
import com.owc.singularity.repository.vcs.SimpleMountTreeDifference;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.repository.OperationProgressMonitorAdapter;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.vcs.MountTreeDiffTree;

public class CommitEntryAction extends AbstractRepositoryContextAction<String> {

    public CommitEntryAction(RepositoryTree tree) {
        super(tree, true, true, null, false, true, "repository_commit_entry");
    }

    @Override
    protected String configureAction(List<RepositoryTreeNode> entries) {
        RepositoryTreeNode node = entries.iterator().next();
        RepositoryPath path = node.getRepositoryPath();
        MountTreeChange uncommittedChanges;
        RepositoryMount mount;
        try {
            mount = RepositoryManager.getFileSystem().getMount(path);
            uncommittedChanges = Entries.getUncommittedChanges(path);
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("cannot_commit_entry", e, path.getFileName().toString(),
                    e.getMessage() == null ? "Commit was not possible!" : e.getMessage());
            return null;
        }
        if (uncommittedChanges.isEmpty()) {
            SwingTools.showMessageDialog("vcs.no_changes_to_commit", path.getFileName().toString());
            return null;
        }
        RepositoryPath mountPath = mount.getMountPath();
        MountTreeDiffTree mountTreeDiffTree = new MountTreeDiffTree(mountPath,
                new SimpleMountTreeDifference(null, new UncommittedEntryVersion(), uncommittedChanges, null, MountTreeChange.NO_CHANGE),
                Collections.emptyList());
        ExtendedJScrollPane changesPanel = new ExtendedJScrollPane(mountTreeDiffTree);
        String commitMessage = SwingTools.showGeneralInputDialog(ApplicationFrame.getApplicationFrame(), "file_chooser.commit", changesPanel,
                message -> message != null && !message.isBlank(), message -> {
                    if (message == null || message.isBlank()) {
                        return I18N.getGUIMessage("gui.dialog.empty_input.label", "Commit Message");
                    }
                    return I18N.getGUIMessage("gui.dialog.invalid_input.label", "Commit Message", "See the documentation for more information.");
                }, "", "commit_entry_message");
        if (commitMessage != null && !commitMessage.isEmpty()) {
            return commitMessage;
        }
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, String commitMessage, ProgressListener progressListener) {
        if (commitMessage == null || commitMessage.isBlank()) {
            SwingTools.showSimpleErrorMessage("cannot_commit_entry", new IllegalArgumentException(), path.getFileName().toString(), commitMessage,
                    "Commit message cannot be empty!");
        } else {
            try {
                MountTreeDifference committed;
                try (OperationProgressMonitorAdapter ignored = new OperationProgressMonitorAdapter(progressListener)) {
                    committed = Entries.commit(path, commitMessage);
                }
                EntryVersion versionAfterCommit = committed.getLeft();
                SwingTools.showMessageDialog("vcs.commit.success", path.toShortString(20), versionAfterCommit);
            } catch (NoChangesException e) {
                SwingTools.showMessageDialog("vcs.no_changes_to_commit", path.getFileName().toString());
            } catch (IOException e) {
                SwingTools.showSimpleErrorMessage("cannot_commit_entry", e, path.getFileName().toString(),
                        e.getMessage() == null ? "Commit was not possible!" : e.getMessage());
            }
        }
    }
}
