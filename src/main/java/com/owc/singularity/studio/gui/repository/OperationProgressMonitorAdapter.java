package com.owc.singularity.studio.gui.repository;

import java.io.Closeable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.OperationProgressMonitor;
import com.owc.singularity.repository.RepositoryFileSystem;
import com.owc.singularity.repository.RepositoryManager;

public class OperationProgressMonitorAdapter implements OperationProgressMonitor, AutoCloseable, Closeable {

    private static final Logger log = LogManager.getLogger(OperationProgressMonitorAdapter.class.getName());

    private final RepositoryFileSystem fileSystem;
    private final ProgressListener progressListener;
    private final OperationProgressMonitor previousOperationProgressMonitor;
    private int numberOfTasks;
    private int numberOfRunningTasks = 0;
    private int numberOfCompletedTasks = 0;
    private boolean scalable = true;


    public OperationProgressMonitorAdapter(ProgressListener progressListener) {
        this(RepositoryManager.getFileSystem(), progressListener);
    }

    public OperationProgressMonitorAdapter(RepositoryFileSystem fileSystem, ProgressListener progressListener) {
        this.previousOperationProgressMonitor = fileSystem.getProgressMonitor();
        this.fileSystem = fileSystem;
        this.progressListener = progressListener;
        fileSystem.setProgressMonitor(this);
    }

    @Override
    public void start(int numberOfTasks) {
        // auto-scale to fit any number of tasks
        scalable = numberOfTasks == 0;
        this.numberOfTasks = scalable ? 1 : numberOfTasks;
    }

    @Override
    public void taskStarts(String title, int totalWork) {
        numberOfRunningTasks++;
        if (scalable) {
            // one step ahead
            numberOfTasks++;
        }
        progressListener.setMessage(title);
        int relativeTotal = totalWork == UNKNOWN ? 100 : totalWork;
        int absoluteTotal = progressListener.getTotal() + relativeTotal;
        progressListener.setTotal(absoluteTotal);
        log.trace("Task: " + title + " starts with total work: " + relativeTotal + "/" + absoluteTotal);
    }

    @Override
    public void update(int completed) {
        progressListener.setCompleted(progressListener.getCompleted() + completed);
        log.trace("completed " + progressListener.getCompleted() + "/" + progressListener.getTotal());
    }

    @Override
    public void taskEnds() {
        numberOfRunningTasks--;
        numberOfCompletedTasks++;
        if (numberOfRunningTasks == 0) {
            log.trace("Task: ends with total work: " + progressListener.getTotal());
            progressListener.setCompleted(0);
            progressListener.setTotal(numberOfTasks);
            progressListener.setCompleted(numberOfCompletedTasks);
        }
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void close() {
        fileSystem.setProgressMonitor(previousOperationProgressMonitor);
    }
}
