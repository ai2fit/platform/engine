package com.owc.singularity.studio.gui.vcs.model;


import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffModificationTreeNode extends AbstractMountTreeDiffFileTreeNode {

    public MountTreeDiffModificationTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }
}
