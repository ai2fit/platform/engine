/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;


/**
 * Renames the selected {@link Operator}.
 *
 * @author Simon Fischer
 * @since 6.4.0
 *
 */
public class RenameOperatorAction extends ProcessChangingAction {

    private static final long serialVersionUID = 1L;

    public RenameOperatorAction() {
        super("rename_in_processrenderer");
        setCondition(Condition.OPERATOR_SELECTED, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(final ActionEvent e) {
        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
        if (mainProcessPanel == null)
            return;
        mainProcessPanel.getProcessEditor().rename();
    }
}
