/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.List;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.RecentLocationsManager;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.event.OnMoveRepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This action renames the selected entry.
 * 
 * @author Simon Fischer
 */
public class RenameRepositoryEntryAction extends AbstractRepositoryContextAction<String> {

    private static final long serialVersionUID = 1L;

    public RenameRepositoryEntryAction(RepositoryTree tree) {
        super(tree, true, true, null, false, true, "repository_rename_entry");
    }

    @Override
    protected String configureAction(List<RepositoryTreeNode> entries) {
        RepositoryPath path = entries.get(0).getRepositoryPath();
        String targetName = SwingTools.showRepositoryEntryInputDialog("file_chooser.rename", path.getFileName().toString(), path.getFileName());
        if (targetName != null && !targetName.isBlank() && !targetName.equals(path.getFileName().toString())) {
            return targetName;
        }
        return null; // aborts the execution
    }

    @Override
    public void executeAction(RepositoryPath path, String targetName, ProgressListener progressListener) {
        try {
            RepositoryPath target = (RepositoryPath) path.resolveSibling(targetName);
            tree.invokeAfterTreeEvent(new OnMoveRepositoryTreeEventTrigger(path, target), ignored -> tree.expandAndSelectIfExists(target));
            RecentLocationsManager.replaceRecentFile(path, target);
            Files.move(path, target);
        } catch (FileAlreadyExistsException e) {
            SwingTools.showSimpleErrorMessage("cannot_rename_entry", e, path.getFileName().toString(), targetName, targetName + " already exists.");
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("cannot_rename_entry", e, path.getFileName().toString(), targetName, e.getMessage());
        }
    }
}
