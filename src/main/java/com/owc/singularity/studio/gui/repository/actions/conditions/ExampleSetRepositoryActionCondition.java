/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.conditions;


import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.studio.gui.repository.actions.context.AbstractRepositoryContextAction;


/**
 * Declares a condition for {@link AbstractRepositoryContextAction}. If the conditions are met, the
 * action is shown, otherwise it will not be shown. This condition only evaluates to {@code true} if
 * the selected entries are ExampleSets.
 * 
 * @author Marco Boeck
 * 
 */
public class ExampleSetRepositoryActionCondition extends TypeRepositoryActionCondition {

    public ExampleSetRepositoryActionCondition() {
        super(new Class[] { ExampleSet.class });
    }


}
