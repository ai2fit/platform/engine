/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.util.function.Supplier;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Provides the new action for the given pipeline type
 *
 * @author Sebastian Land
 */

public class NewPipelineAction extends ResourceAction {

    private static final long serialVersionUID = 1358354112149248404L;

    private MainFrame mainFrame;

    private Supplier<AbstractPipeline> supplier;

    public NewPipelineAction(String key, Supplier<AbstractPipeline> supplier, MainFrame mainFrame) {
        super(key);
        this.mainFrame = mainFrame;
        this.supplier = supplier;
        setCondition(Condition.PROCESS_PAUSED, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RUNNING, ConditionReaction.DISALLOWED);

        // override icon
        putValue(LARGE_ICON_KEY, SwingTools.addIconOverlay(SwingTools.createIcon("24/document_empty.png", IconSize.MEDIUM),
                SwingTools.createIcon(IconSize.MEDIUM.getSize() + "/" + getIconName(), IconSize.MEDIUM, getIconType() == IconType.MONO), 0.6));
        putValue(SMALL_ICON, SwingTools.addIconOverlay(SwingTools.createIcon("16/document_empty.png", IconSize.SMALL),
                SwingTools.createIcon(IconSize.SMALL.getSize() + "/" + getIconName(), IconSize.SMALL, getIconType() == IconType.MONO), 0.6));

    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        this.mainFrame.openProcess(supplier.get(), false);
    }
}
