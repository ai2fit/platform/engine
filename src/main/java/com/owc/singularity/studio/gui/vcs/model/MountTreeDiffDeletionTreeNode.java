package com.owc.singularity.studio.gui.vcs.model;


import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffDeletionTreeNode extends AbstractMountTreeDiffFileTreeNode {

    public MountTreeDiffDeletionTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }
}
