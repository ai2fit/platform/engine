/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.conditions;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.repository.actions.context.AbstractRepositoryContextAction;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * Declares a condition for {@link AbstractRepositoryContextAction}. If the conditions are met, the
 * action is shown, otherwise it will not be shown.
 * 
 * @author Marco Boeck
 * 
 */
public class TypeRepositoryActionCondition implements RepositoryActionCondition {

    /** selection must be of one of the types listed here */
    private final List<Class<?>> requiredSelectionTypeList;


    /**
     * Creates a new RepositoryActionCondition which can be used to check if the selected
     * {@link Entry}s meet the given conditions.
     * 
     * @param requiredSelectionTypeList
     *            a list with {@link Entry} types. Each selected {@link Entry} must be of one of the
     *            types on the list or the condition is not met.
     * @param requiredSelectionRepositoryTypeList
     *            a list with {@link Repository} types. Each selected {@link Entry} must be of the
     *            types on the list or the condition is not met.
     */
    public TypeRepositoryActionCondition(List<Class<?>> requiredSelectionTypeList) {
        if (requiredSelectionTypeList == null) {
            throw new IllegalArgumentException("lists must not be null!");
        }
        this.requiredSelectionTypeList = requiredSelectionTypeList;
    }


    /**
     * Creates a new RepositoryActionCondition which can be used to check if the selected
     * {@link Entry}s meet the given conditions.
     * 
     * @param requiredSelectionTypes
     *            a list with {@link Entry} types. Each selected {@link Entry} must be of one of the
     *            types on the list or the condition is not met.
     * @param requiredSelectionRepositoryTypes
     *            a list with {@link Repository} types. Each selected {@link Entry} must be of the
     *            types on the list or the condition is not met.
     */
    public TypeRepositoryActionCondition(Class<?>[] requiredSelectionTypes) {
        if (requiredSelectionTypes == null) {
            throw new IllegalArgumentException("arrays must not be null!");
        }
        this.requiredSelectionTypeList = Arrays.asList(requiredSelectionTypes);
    }

    @Override
    public boolean test(List<RepositoryTreeNode> entryList) {
        if (entryList == null) {
            return false;
        }

        for (RepositoryTreeNode givenEntry : entryList) {

            // make sure each entry's type is in the requiredTypes list, if not condition evaluates
            // to false
            try {
                boolean entryTypeConditionMet = requiredSelectionTypeList.isEmpty() ? true : false;
                for (Class<?> requiredType : requiredSelectionTypeList) {
                    if (givenEntry.getEntry().isInstanceOf(requiredType, ModuleService.getMajorClassLoader())) {
                        entryTypeConditionMet = true;
                        break;
                    }
                }
                // wrong entry type
                if (!entryTypeConditionMet) {
                    return false;
                }
            } catch (ClassNotFoundException | IOException e) {
                return false;
            }
        }

        // all conditions have been met, so return true
        return true;
    }

}
