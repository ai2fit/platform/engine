/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.pipeline.PipelineState;
import com.owc.singularity.studio.gui.MainFrame;


/**
 * Determines whether the process setup is validated automatically on any update.
 * 
 * @author Simon Fischer
 * 
 */
public class ValidateAutomaticallyAction extends ToggleAction {

    private static final String PROPERTY_VALIDATE_AUTOMATICALLY = "singularity.gui.validate_automatically";

    public ValidateAutomaticallyAction() {
        super(false, "validate_automatically");
        setSelected(!"false".equals(PropertyService.getParameterValue(PROPERTY_VALIDATE_AUTOMATICALLY)));
    }

    private static final long serialVersionUID = 1L;

    @Override
    public void actionToggled(ActionEvent e) {
        MainFrame mainFrame = MainFrame.INSTANCE;
        if (isSelected()) {
            mainFrame.validateProcess(mainFrame.getProcessState() != PipelineState.RUNNING);
        }
        PropertyService.setParameterValue(PROPERTY_VALIDATE_AUTOMATICALLY, Boolean.toString(isSelected()));
        PropertyService.saveParameters();
    }
}
