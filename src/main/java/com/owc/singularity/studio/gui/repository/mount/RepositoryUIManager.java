package com.owc.singularity.studio.gui.repository.mount;

import java.util.*;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.ErrorForm;
import com.owc.singularity.studio.gui.Form;
import com.owc.singularity.studio.gui.repository.StatusBarProgressMonitorAdapter;

public class RepositoryUIManager {

    private static final List<MountConfigurationUIProvider> mountConfigurationUIProviders = new ArrayList<>();
    /**
     * The {@link DefaultMountConfigurationUIProvider} is a default provider that is not listed in
     * the META-INF.services file, so it is not included within the providers list
     */
    private static final MountConfigurationUIProvider defaultMountConfigurationUIProvider = new DefaultMountConfigurationUIProvider();

    public static void initialize(ClassLoader classLoader) {
        ServiceLoader<MountConfigurationUIProvider> loader = ServiceLoader.load(MountConfigurationUIProvider.class, classLoader);
        for (MountConfigurationUIProvider provider : loader) {
            mountConfigurationUIProviders.add(provider);
        }
        RepositoryManager.getFileSystem().setProgressMonitor(new StatusBarProgressMonitorAdapter(ApplicationFrame.getApplicationFrame().getStatusBar()));
    }

    public static boolean supportsEdit(String mountType) {
        MountConfigurationUIProvider uiProvider = getMountConfigurationUIProviderOrThrow(mountType);
        return uiProvider.supportsEditUI();
    }

    public static Form<Map<String, String>> getMountEditForm(String mountType, Map<String, String> options) {
        MountConfigurationUIProvider uiProvider = getMountConfigurationUIProviderOrThrow(mountType);
        if (!uiProvider.supportsEditUI()) {
            throw new UnsupportedOperationException("The " + uiProvider.getClass() + " does not support a create ui");
        }
        try {
            return uiProvider.getEditUI(mountType, options);
        } catch (Exception e) {
            return new ErrorForm<>(I18N.getErrorMessage("repository.mount.configuration_dialog_creation"), e);
        }
    }

    public static boolean supportsCreate(String mountType) {
        MountConfigurationUIProvider uiProvider = getMountConfigurationUIProviderOrThrow(mountType);
        return uiProvider.supportsCreateUI();
    }

    public static Form<Map<String, String>> getMountCreateForm(String mountType) {
        MountConfigurationUIProvider uiProvider = getMountConfigurationUIProviderOrThrow(mountType);
        if (!uiProvider.supportsCreateUI()) {
            throw new UnsupportedOperationException("The " + uiProvider.getClass() + " does not support a create ui");
        }
        try {
            return uiProvider.getCreateUI(mountType);
        } catch (Exception e) {
            return new ErrorForm<>(I18N.getErrorMessage("repository.mount.configuration_dialog_creation"), e);
        }
    }

    private static MountConfigurationUIProvider getMountConfigurationUIProviderOrThrow(String mountType) {
        MountConfigurationUIProvider uiProvider = getMountConfigurationUIProviderFor(mountType);
        if (uiProvider == null) {
            // it should not come to this, since there is a DefaultMountConfigurationUIProvider
            throw new RuntimeException("No MountConfigurationUIProvider was found for mount with type " + mountType);
        }
        return uiProvider;
    }

    public static MountConfigurationUIProvider getMountConfigurationUIProviderFor(String mountType) {
        for (MountConfigurationUIProvider provider : mountConfigurationUIProviders) {
            if (provider.isMountTypeSupported(mountType)) {
                return provider;
            }
        }
        return defaultMountConfigurationUIProvider;
    }
}
