/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.new_plotter.configuration;


import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.visualization.datatable.DataTable;


/**
 * This class describes a column of a {@link DataTable} characterized by a name, a column index and
 * a {@link PlotColumnValueType}.
 *
 * @author Nils Woehler, Marius Helf
 */
public class DataTableColumn implements Cloneable {

    public enum PlotColumnValueType {

        INVALID, UNKNOWN, NOMINAL, NUMERIC, TIMESTAMP;

        public static ValueType convertToValueType(PlotColumnValueType valueType) {
            return switch (valueType) {
                case TIMESTAMP -> ValueType.TIMESTAMP;
                case INVALID -> null;
                case NUMERIC -> ValueType.NUMERIC;
                default -> ValueType.NOMINAL;
            };
        }

        public static PlotColumnValueType convertFromValueType(ValueType rmValueType) {
            if (rmValueType.equals(ValueType.NUMERIC)) {
                return NUMERIC;
            } else if (rmValueType.equals(ValueType.NOMINAL)) {
                return NOMINAL;
            } else if (rmValueType.equals(ValueType.TIMESTAMP)) {
                return TIMESTAMP;
            } else {
                return INVALID;
            }
        }
    }

    private final String columnName;
    private final PlotColumnValueType valueType;

    public DataTableColumn(String name, PlotColumnValueType valueType) {
        super();
        this.columnName = name;
        this.valueType = valueType;
    }

    /**
     * @brief Creates a new {@link DataTableColumn}.
     * 
     *        name and value type are automatically initialized from the specified column in
     *        dataTable. The DataTableColumn does not keep a reference to dataTable.
     */
    public DataTableColumn(DataTable dataTable, int columnIdx) {
        if (columnIdx >= 0 && columnIdx < dataTable.getColumnNumber()) {
            this.columnName = dataTable.getColumnName(columnIdx);
            if (dataTable.isTimestamp(columnIdx)) {
                this.valueType = PlotColumnValueType.TIMESTAMP;
            } else if (dataTable.isNominal(columnIdx)) {
                this.valueType = PlotColumnValueType.NOMINAL;
            } else if (dataTable.isNumeric(columnIdx)) {
                this.valueType = PlotColumnValueType.NUMERIC;
            } else {
                this.valueType = PlotColumnValueType.INVALID;
            }
        } else {
            this.columnName = null;
            this.valueType = PlotColumnValueType.INVALID;
        }
    }

    /**
     * @return the {@link PlotColumnValueType}
     */
    public PlotColumnValueType getValueType() {
        return valueType;
    }

    public boolean isNominal() {
        if (valueType == PlotColumnValueType.NOMINAL) {
            return true;
        }
        return false;
    }

    public boolean isNumerical() {
        if (valueType == PlotColumnValueType.NUMERIC) {
            return true;
        }
        return false;
    }

    public boolean isDate() {
        if (valueType == PlotColumnValueType.TIMESTAMP) {
            return true;
        }
        return false;
    }

    /**
     * @return the name
     */
    public String getName() {
        return columnName;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof DataTableColumn)) {
            return false;
        }

        DataTableColumn other = (DataTableColumn) obj;

        if (valueType != other.valueType) {
            return false;
        }

        if (!(columnName == null ? other.columnName == null : columnName.equals(other.columnName))) {
            return false;
        }

        return true;
    }

    @Override
    public DataTableColumn clone() {
        return new DataTableColumn(getName(), getValueType());
    }

    /**
     * Finds the column index for the column named columnName in dataTable, checks if that column in
     * the data table is compatible with to settings valueType, and updates this.columnIdx.
     * 
     * If the column does not exist or is not compatible, the columnIdx is set to -1.
     */
    public static int getColumnIndex(DataTable dataTable, String columnName, PlotColumnValueType valueType) {
        int columnIdx = dataTable.getColumnIndex(columnName);
        if (columnIdx >= 0) {
            // check value type
            switch (valueType) {
                case NOMINAL:
                    if (!dataTable.isNominal(columnIdx)) {
                        columnIdx = -1;
                    }
                    break;
                case NUMERIC:
                    if (!dataTable.isNumeric(columnIdx)) {
                        columnIdx = -1;
                    }
                    break;
                case TIMESTAMP:
                    if (!dataTable.isTimestamp(columnIdx)) {
                        columnIdx = -1;
                    }
                    break;
                case INVALID:
                    // do nothing
                    break;
                default:
            }
        }
        return columnIdx;
    }

    public boolean isValidForDataTable(DataTable dataTable) {
        int columnIdx = getColumnIndex(dataTable, columnName, valueType);
        return columnIdx >= 0 && valueType != PlotColumnValueType.INVALID && columnName != null;
    }

    /**
     * Finds the column index for the column named like dataTableColumn.getName in dataTable, checks
     * if that column in the data table is compatible with to settings of dataTableColumn (value
     * type etc.), and updates this.columnIdx.
     * 
     * If the column does not exist or is not compatible, the columnIdx is set to -1.
     */
    public static int getColumnIndex(DataTable dataTable, DataTableColumn dataTableColumn) {
        return getColumnIndex(dataTable, dataTableColumn.getName(), dataTableColumn.getValueType());
    }
}
