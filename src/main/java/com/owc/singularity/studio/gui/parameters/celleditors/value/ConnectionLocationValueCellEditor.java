/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.io.IOException;
import java.util.function.Predicate;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeConnectionLocation;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * Repository location cell editor that is specialized for {@link ConnectionParametersIOObject
 * ConnectionEntries} and adds a second button that allows to open/edit the selected connection.
 *
 * @author Jan Czogalla
 * @since 9.3
 */
public class ConnectionLocationValueCellEditor extends RepositoryLocationWithExtraValueCellEditor {

    private static final long serialVersionUID = 1470384492293936919L;


    // keep the connection types from the ParameterTypeConnectionLocation to create a Predicate for
    // filtering the Repository
    private final String[] conTypes;

    public ConnectionLocationValueCellEditor(ParameterTypeConnectionLocation type) {
        super(type);
        conTypes = type.getConnectionType();
    }

    @Override
    protected String getExtraActionKey() {
        return "connection.open_edit_connection";
    }

    @Override
    protected void doExtraAction(RepositoryPath repositoryLocation) {
        OpenEntryAction.open(repositoryLocation.toAbsolutePath().toString());
    }

    @Override
    protected Class<ConnectionParametersIOObject> getExpectedEntryClass() {
        return ConnectionParametersIOObject.class;
    }

    @Override
    protected Predicate<RepositoryTreeNode> getRepositoryFilter() {
        if (conTypes == null)
            return RepositoryLocationChooser.ONLY_CONNECTIONS;
        return RepositoryLocationChooser.ONLY_CONNECTIONS.and(entry -> {
            try {
                ConnectionParametersIOObject parametersObject = entry.getEntry()
                        .loadData(ConnectionParametersIOObject.class, ModuleService.getMajorClassLoader());
                String typeId = parametersObject.getParameters().getTypeId();
                for (String allowedType : conTypes)
                    if (allowedType.equals(typeId))
                        return true;
                return false;
            } catch (IOException ignored) {
            }
            return false;
        });
    }
}
