/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.security;


import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.tools.GlobalAuthenticator;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.cipher.CipherException;
import com.owc.singularity.engine.tools.cipher.CipherTools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.net.URITools;
import com.owc.singularity.tools.VersionNumber;


/**
 * The Wallet stores user credentials (username and passwords). It is used by the
 * {@link GlobalAuthenticator} and can be edited with the {@link PasswordManager}.
 * {@link UserCredential}s are stored per {@link URL}.
 * <p>
 * Note: Though this class has a {@link #getInstance()} method, it is not a pure singleton. In fact,
 * the {@link PasswordManager} sets a new instance via {@link #setInstance(Wallet)} after editing.
 *
 * @author Miguel Buescher, Marco Boeck, Hatem Hamad
 */
public class Wallet {

    private static final Logger log = LogService.getI18NLogger(Wallet.class);
    private static final String CACHE_FILE_NAME = "secrets.xml";
    private final SortedMap<String[], Map<String, UserCredential>> walletContent;

    /**
     * Version of the secrets xml that is necessary, triggers migration by saving the real
     * secrets.xml is below this version
     */
    private static final VersionNumber SECRET_VERSION_NR = new VersionNumber(7, 5, 4);

    private static final String VERSION_ATTRIBUTE = "version";

    private static Wallet instance = new Wallet();

    private static final boolean ACTIVE_MIGRATION_DEFAULT_VALUE = false;
    private boolean activeMigration = ACTIVE_MIGRATION_DEFAULT_VALUE;

    /**
     * Singleton access.
     */
    public static synchronized Wallet getInstance() {
        return instance;
    }

    public static synchronized void setInstance(Wallet wallet) {
        instance = wallet;
    }

    /**
     * Reads the walletContent from the secrets.xml file in the user's home directory.
     */
    public void readCache() {
        final File userConfigFile = FileSystemService.getUserConfigFile(CACHE_FILE_NAME);
        if (!userConfigFile.exists()) {
            return;
        }
        log.trace("com.owc.singularity.studio.gui.security.Wallet.reading_secrets_file");

        Document doc;
        try {
            doc = XMLTools.parse(userConfigFile);
        } catch (Exception e) {
            log.atWarn().withThrowable(e).log("com.owc.singularity.studio.gui.security.Wallet.reading_secrets_file_error", userConfigFile);
            return;
        }
        VersionNumber secretXmlVersion = getVersionNumber(doc);
        activeMigration = isMigrationRequired(secretXmlVersion);
        try {
            readCache(doc);
            if (isMigrationRequired(secretXmlVersion)) {
                saveCache();
            }
        } finally {
            activeMigration = ACTIVE_MIGRATION_DEFAULT_VALUE;
        }
    }

    private VersionNumber getVersionNumber(Document doc) {
        VersionNumber secretXmlVersion = null;
        try {
            NodeList rootNode = doc.getElementsByTagName(CACHE_FILE_NAME);
            NamedNodeMap attributes = rootNode.item(0).getAttributes();
            Node versionValue = attributes.getNamedItem(VERSION_ATTRIBUTE);
            secretXmlVersion = new VersionNumber(versionValue.getNodeValue());
        } catch (Exception e) {
            // attribute missing
        }
        return secretXmlVersion;
    }

    private boolean isMigrationRequired(VersionNumber secretXmlVersion) {
        return secretXmlVersion == null;
    }

    /**
     * Reads the wallet from a {@link Document} object.
     *
     * @param doc
     *            the document which contains the secrets
     */
    public void readCache(Document doc) {
        NodeList secretElems = doc.getDocumentElement().getElementsByTagName("secret");

        for (int i = 0; i < secretElems.getLength(); i++) {
            Element secretElem = (Element) secretElems.item(i);
            String uri = XMLTools.getTagContents(secretElem, "uri");
            URI resourceUri = null;
            if (uri != null) {
                try {
                    resourceUri = new URI(uri);
                } catch (URISyntaxException ignored) {
                }
            }
            if (resourceUri == null) {
                // TODO add warning
                continue;
            }
            String user = XMLTools.getTagContents(secretElem, "username");
            char[] password;
            try {
                String passwordTagContent = XMLTools.getTagContents(secretElem, "password");
                password = getDecodedPassword(passwordTagContent).toCharArray();
            } catch (IOException | CipherException e) {
                log.warn("com.owc.singularity.studio.gui.security.Wallet.reading_entry_in_secrets_file_error", e, e);
                continue;
            }
            addEntry(resourceUri, new UserCredential(resourceUri, user, password));
        }
    }

    /**
     * Adds the given credentials with the given ID. ID is necessary because there may be more
     * credentials than one for the same URL.
     *
     * @param authentication
     * @throws IllegalArgumentException
     *             if the key is <code>null</code>
     */
    public void addEntry(URI uri, UserCredential authentication) {
        uri = URITools.getWithoutQueryAndFragment(uri);
        String[] uriAsSegments = splitUri(uri);
        Map<String, UserCredential> userCredentialMap = walletContent.computeIfAbsent(uriAsSegments, ignored -> new LinkedHashMap<>());
        userCredentialMap.put(authentication.getUsername(), authentication);
    }

    /**
     * Returns the number of entries in the {@link Wallet}.
     */
    public int size() {
        return walletContent.size();
    }

    private Wallet() {
        walletContent = new TreeMap<>(Arrays::compare);
        readCache();
    }

    /**
     * Deep clone constructor.
     */
    public Wallet(Wallet toClone) {
        activeMigration = toClone.activeMigration;
        walletContent = new TreeMap<>(toClone.walletContent);
    }

    /**
     * Returns a {@link List} of {@link String} keys in this {@link Wallet}.
     */
    public Set<String> getKeys() {
        return walletContent.keySet().stream().map(strings -> String.join("/", strings)).collect(Collectors.toSet());
    }

    public List<UserCredential> getEntries(URI uri) {
        String[] key = splitUri(uri);
        Map<String, UserCredential> result = walletContent.get(key);
        // if none found for this uri, search for parents
        for (int i = key.length - 1; result == null && i > 0; i--) {
            // remove last path element and repeat until
            // we find a parent path credential
            String[] parentKey = new String[i];
            System.arraycopy(key, 0, parentKey, 0, i);
            result = walletContent.get(parentKey);
        }
        return result == null ? Collections.emptyList() : new LinkedList<>(result.values());
    }

    /**
     * Returns the {@link UserCredential} for the given id and uri {@link String}s. If there is no
     * key matching the given id and uri tries to return the {@link UserCredential} for the given
     * uri (fallback for old entries). If both fail, returns <code>null</code>.
     *
     * @param uri
     * @param username
     * @return
     */
    public UserCredential getEntry(URI uri, String username) {
        Map<String, UserCredential> result = findCredentialMapFor(uri, username);
        if (result == null)
            return null;
        return result.get(username);
    }

    private Map<String, UserCredential> findCredentialMapFor(URI uri, String username) {
        String[] key = splitUri(uri);
        Map<String, UserCredential> result = walletContent.get(key);
        // if none found for this uri, search for parents
        for (int i = key.length - 1; (result == null || !result.containsKey(username)) && i > 0; i--) {
            // remove last path element and repeat until
            // we find a parent path credential
            String[] parentKey = new String[i];
            System.arraycopy(key, 0, parentKey, 0, i);
            result = walletContent.get(parentKey);
        }
        return result;
    }

    /**
     * Removes the {@link UserCredential} for the given id and uri {@link String}s.
     *
     * @param uri
     * @param username
     */
    public void removeEntry(URI uri, String username) {
        Map<String, UserCredential> credentialMap = findCredentialMapFor(uri, username);
        credentialMap.remove(username);
    }

    /**
     * Creates a XML representation of the walletContent.
     *
     * @return The XML document.
     */
    public Document getWalletAsXML() {
        Document doc = XMLTools.createDocument();
        Element root = doc.createElement(CACHE_FILE_NAME);
        root.setAttribute(VERSION_ATTRIBUTE, SECRET_VERSION_NR.getLongVersion());
        doc.appendChild(root);
        for (Map<String, UserCredential> value : walletContent.values()) {
            for (UserCredential userCredential : value.values()) {
                try {
                    Element entryElem = doc.createElement("secret");
                    XMLTools.setTagContents(entryElem, "uri", userCredential.getURI().toString());
                    XMLTools.setTagContents(entryElem, "username", userCredential.getUsername());
                    XMLTools.setTagContents(entryElem, "password", getEncodedPassword(new String(userCredential.getPassword())));
                    root.appendChild(entryElem);
                } catch (CipherException e) {
                    log.atError().withThrowable(e).log("com.owc.singularity.studio.gui.security.Wallet.store_entry_failed", userCredential.getURI());
                }
            }
        }
        return doc;
    }

    private String getEncodedPassword(String string) throws CipherException {
        return CipherTools.encrypt(string);
    }

    private String getDecodedPassword(String string) throws CipherException, IOException {
        if (CipherTools.isKeyAvailable()) {
            try {
                return CipherTools.decrypt(string);
            } catch (CipherException e) {
                // old password storage detected, use obsolete password decoding mechanism for
                // migration only
                if (!activeMigration) {
                    // not accepting old decoding in a up-to-date secrets file
                    throw e;
                }
            }
        }
        return new String(Base64.getDecoder().decode(string));
    }

    /**
     * Saves the walletContent to the secrets.xml file in the users home directory.
     */
    public void saveCache() {
        log.debug("com.owc.singularity.studio.gui.security.Wallet.saving_secrets_file");
        Document doc = getWalletAsXML();
        File file = FileSystemService.getUserConfigFile(CACHE_FILE_NAME);
        try {
            XMLTools.stream(doc, file, null);
        } catch (XMLException e) {
            log.atError().withThrowable(e).log("com.owc.singularity.studio.gui.security.Wallet.saving_secrets_file_error", file);
        }
    }

    private static String[] splitUri(URI uri) {
        String[] pathSegments = uri.getPath().substring(1).split("/");
        String[] segments = new String[1 + pathSegments.length];

        segments[0] = URITools.getHostUri(uri).toString();
        System.arraycopy(pathSegments, 0, segments, 1, pathSegments.length);
        return segments;
    }

}
