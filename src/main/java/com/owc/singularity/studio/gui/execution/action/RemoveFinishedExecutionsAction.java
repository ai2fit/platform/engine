package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionService;
import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.ConcurrentExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecutionState;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;

public class RemoveFinishedExecutionsAction extends ResourceAction {

    private static final long serialVersionUID = 1L;

    public RemoveFinishedExecutionsAction() {
        super(true, "toolkit.remove_finished_processes");

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConfirmDialog confirmation = new ConfirmDialog(null, "toolkit.remove_finished_executions.confirmation", ConfirmDialog.OK_CANCEL_OPTION, false);

        confirmation.setVisible(true);

        if (confirmation.getReturnOption() == ConfirmDialog.OK_OPTION) {
            removeExecutions();
        }
    }

    protected void removeExecutions() {
        ConcurrentExecutionService service = ConcurrentExecutionServiceProvider.getService();
        for (ConcurrentExecution execution : service.getExecutions()) {
            if (execution instanceof ConcurrentPipelineExecution) {
                ConcurrentPipelineExecution process = (ConcurrentPipelineExecution) execution;
                ConcurrentPipelineExecutionState state = process.getBackgroundExecutionState();
                if (state.isStarted() && (state.isEnded() || state.isStopped())) {

                    service.removePipelineExecution(process);
                }
            }
        }
    }

}
