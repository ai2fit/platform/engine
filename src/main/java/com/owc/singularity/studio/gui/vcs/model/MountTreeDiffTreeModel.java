package com.owc.singularity.studio.gui.vcs.model;

import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.repository.vcs.MountTreeChange;
import com.owc.singularity.repository.vcs.MountTreeDifference;
import com.owc.singularity.studio.gui.tools.tree.TreeUtilities;

public class MountTreeDiffTreeModel extends DefaultTreeModel {

    private static final Logger log = LogManager.getLogger(MountTreeDiffTreeModel.class);
    private final RepositoryPath mountPath;
    private final EntryVersion firstVersion;
    private final EntryVersion secondVersion;
    private Collection<RepositoryPath> conflicts;
    private Collection<RepositoryPath> uncommittedChanges;
    private final Comparator<TreeNode> nodeComparator;
    private MountTreeDifferenceScannerWorker predefinedWorker;

    public MountTreeDiffTreeModel(RepositoryPath mountPath, MountTreeDifference difference, Collection<RepositoryPath> uncommittedPaths,
            Comparator<TreeNode> nodeComparator) {
        this(mountPath, difference.getLeft(), difference.getRight(), difference.getConflictedPaths(), uncommittedPaths, nodeComparator);
        this.predefinedWorker = new MountTreeDifferenceScannerWorker((DefaultMutableTreeNode) root, mountPath, difference, nodeComparator);
    }

    public MountTreeDiffTreeModel(RepositoryPath mountPath, EntryVersion firstVersion, EntryVersion secondVersion, Collection<RepositoryPath> conflicts,
            Collection<RepositoryPath> uncommittedChanges, Comparator<TreeNode> nodeComparator) {
        super(new DefaultMutableTreeNode(""), false);
        this.mountPath = mountPath;
        this.firstVersion = firstVersion;
        this.secondVersion = secondVersion;
        this.conflicts = conflicts;
        this.uncommittedChanges = uncommittedChanges;
        this.nodeComparator = nodeComparator;
    }

    public void setConflicts(Collection<RepositoryPath> conflicts) {
        this.conflicts = conflicts;
    }

    public Collection<RepositoryPath> getConflicts() {
        return conflicts;
    }

    public void setUncommittedChanges(Collection<RepositoryPath> uncommittedChanges) {
        this.uncommittedChanges = uncommittedChanges;
    }

    public Collection<RepositoryPath> getUncommittedChanges() {
        return uncommittedChanges;
    }

    public void scanTreeAsync() {
        MountTreeDifferenceScannerWorker worker = predefinedWorker == null
                ? new MountTreeDifferenceScannerWorker((DefaultMutableTreeNode) root, mountPath, firstVersion, secondVersion, getConflicts(),
                        getUncommittedChanges(), nodeComparator)
                : predefinedWorker;
        worker.execute();
    }


    public void fireNodeChanged(RepositoryPath path) {
        TreeNode node = findNodeFor(path);
        if (node == null)
            return;
        nodeChanged(node);
    }

    private TreeNode findNodeFor(RepositoryPath path) {
        int nameCount = path.getNameCount();
        TreeNode parent = root;
        for (int i = 0; i < nameCount - 1; i++) {
            String name = path.getName(i).toString();
            int foundParentAt = TreeUtilities.findIndex(parent, new DefaultMutableTreeNode(name), Function.identity(), this.nodeComparator);
            if (foundParentAt < 0)
                return null;
            parent = parent.getChildAt(foundParentAt);
        }
        String fileName = path.getFileName().toString();
        int foundChildAt = TreeUtilities.findIndex(parent, new AbstractMountTreeDiffTreeNode(null, path) {

            @Override
            public String getName() {
                return fileName;
            }

            @Override
            public Class<?> getType() {
                return null;
            }
        }, Function.identity(), this.nodeComparator);
        if (foundChildAt < 0) {
            // maybe searching for a folder
            foundChildAt = TreeUtilities.findIndex(parent, new DefaultMutableTreeNode(fileName), Function.identity(), this.nodeComparator);
            if (foundChildAt < 0)
                return null;
        }
        return parent.getChildAt(foundChildAt);
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof AbstractMountTreeDiffTreeNode && ((AbstractMountTreeDiffTreeNode) node).isLeaf();
    }

    private class MountTreeDifferenceScannerWorker extends SwingWorker<DefaultMutableTreeNode, MountTreeDifferenceScannerWorker.Addition> {

        private final DefaultMutableTreeNode root;

        private final RepositoryPath mountPath;
        private final EntryVersion leftVersion;
        private final EntryVersion rightVersion;
        private final Collection<RepositoryPath> predefinedConflicts;
        private final Collection<RepositoryPath> uncommittedChanges;
        private final MountTreeDifference difference;
        private final Comparator<TreeNode> nodeComparator;

        public MountTreeDifferenceScannerWorker(DefaultMutableTreeNode root, RepositoryPath mountPath, MountTreeDifference difference,
                Comparator<TreeNode> nodeComparator) {
            this.root = root;
            this.mountPath = mountPath;
            this.leftVersion = difference.getLeft();
            this.rightVersion = difference.getRight();
            this.predefinedConflicts = difference.getConflictedPaths();
            this.nodeComparator = nodeComparator;
            this.uncommittedChanges = Collections.emptyList();
            this.difference = difference;
        }

        public MountTreeDifferenceScannerWorker(DefaultMutableTreeNode root, RepositoryPath mountPath, EntryVersion leftVersion, EntryVersion rightVersion,
                Collection<RepositoryPath> predefinedConflicts, Collection<RepositoryPath> uncommittedChanges, Comparator<TreeNode> nodeComparator) {
            this.root = root;
            this.mountPath = mountPath;
            this.leftVersion = leftVersion;
            this.rightVersion = rightVersion;
            this.predefinedConflicts = predefinedConflicts;
            this.uncommittedChanges = uncommittedChanges;
            this.nodeComparator = nodeComparator;
            this.difference = null;
        }

        @Override
        protected DefaultMutableTreeNode doInBackground() throws IOException {
            root.removeAllChildren();
            RepositoryMount mount = mountPath.getFileSystem().getMount(mountPath);

            MountTreeDifference treeDifference = difference == null ? mount.compareVersionTrees(leftVersion, rightVersion) : difference;
            MountTreeChange leftTreeChange = treeDifference.getLeftChange();
            MountTreeChange rightTreeChange = treeDifference.getRightChange();

            Map<RepositoryPath, DefaultMutableTreeNode> createdNodes = new HashMap<>();
            createdNodes.put(RepositoryManager.DEFAULT_FILESYSTEM_ROOT, root);
            // create parent directories
            Collection<RepositoryPath> conflictedPaths = predefinedConflicts == null ? treeDifference.getConflictedPaths() : predefinedConflicts;
            // example:
            // path/to/file1
            // path/to/folder/file2
            // will result in a sorted set:
            // <path, path/to, path/to/folder>
            Set<RepositoryPath> parentSet = Stream
                    .of(leftTreeChange.getCreatedPaths(), leftTreeChange.getModifiedPaths(), leftTreeChange.getDeletedPaths(),
                            rightTreeChange.getCreatedPaths(), rightTreeChange.getModifiedPaths(), rightTreeChange.getDeletedPaths(), conflictedPaths,
                            uncommittedChanges)
                    .flatMap(Collection::stream)
                    .flatMap(path -> IntStream.range(0, path.getNameCount() - 1).mapToObj(level -> path.subpath(0, level)))
                    .sorted(Comparator.comparingInt(RepositoryPath::getNameCount).thenComparing(RepositoryPath::getFileName))
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            DefaultMutableTreeNode parentNode;
            for (RepositoryPath directoryPath : parentSet) {
                parentNode = createdNodes.get(directoryPath.getParent());
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(directoryPath.getFileName().toString());
                createdNodes.put(directoryPath, node);
                int childIndex = parentNode.getChildCount();
                parentNode.add(node);
                publish(new Addition(parentNode, childIndex));
            }

            // create directory changes
            createDirectoryChangesNode(leftTreeChange, rightTreeChange, createdNodes);

            Predicate<RepositoryPath> onlyNotConflictedPath = path -> {
                RepositoryPath versionedPath = path.toUnversionedPath();
                return !conflictedPaths.contains(versionedPath) && !uncommittedChanges.contains(versionedPath) && !Files.isDirectory(path);
            };
            createNodesForPaths(createdNodes, leftTreeChange.getCreatedPaths().stream().filter(onlyNotConflictedPath), leftVersion,
                    MountTreeDiffCreationTreeNode::new);
            createNodesForPaths(createdNodes, rightTreeChange.getCreatedPaths().stream().filter(onlyNotConflictedPath), rightVersion,
                    MountTreeDiffCreationTreeNode::new);

            createNodesForPaths(createdNodes, leftTreeChange.getModifiedPaths().stream().filter(onlyNotConflictedPath), leftVersion,
                    MountTreeDiffModificationTreeNode::new);
            createNodesForPaths(createdNodes, rightTreeChange.getModifiedPaths().stream().filter(onlyNotConflictedPath), rightVersion,
                    MountTreeDiffModificationTreeNode::new);

            createNodesForPaths(createdNodes, leftTreeChange.getDeletedPaths().stream().filter(onlyNotConflictedPath), leftVersion,
                    MountTreeDiffDeletionTreeNode::new);
            createNodesForPaths(createdNodes, rightTreeChange.getDeletedPaths().stream().filter(onlyNotConflictedPath), rightVersion,
                    MountTreeDiffDeletionTreeNode::new);

            createNodesForPaths(createdNodes, conflictedPaths.stream(), null,
                    (version, path) -> new MountTreeDiffMergeConflictTreeNode(leftVersion, rightVersion, path));

            createNodesForPaths(createdNodes, uncommittedChanges.stream(), null,
                    (version, path) -> Files.exists(path) ? new MountTreeDiffCreationTreeNode(null, path) : new MountTreeDiffDeletionTreeNode(null, path));

            return root;
        }

        private void createDirectoryChangesNode(MountTreeChange leftTreeChange, MountTreeChange rightTreeChange,
                Map<RepositoryPath, DefaultMutableTreeNode> createdNodes) {
            createNodesForPaths(createdNodes, leftTreeChange.getCreatedPaths().stream().filter(Files::isDirectory), leftVersion,
                    MountTreeDiffDirectoryCreationTreeNode::new);
            createNodesForPaths(createdNodes, rightTreeChange.getCreatedPaths().stream().filter(Files::isDirectory), rightVersion,
                    MountTreeDiffDirectoryCreationTreeNode::new);
            // since the path is deleted, we need to check its type from the other version
            Predicate<RepositoryPath> checkInRight = (RepositoryPath path) -> path.getVersionId() == null
                    && !Files.isRegularFile(RepositoryPath.of(path, rightVersion.getId()));
            Predicate<RepositoryPath> checkInLeft = (RepositoryPath path) -> path.getVersionId() == null
                    && !Files.isRegularFile(RepositoryPath.of(path, leftVersion.getId()));
            createNodesForPaths(createdNodes, leftTreeChange.getDeletedPaths().stream().filter(checkInRight), leftVersion,
                    MountTreeDiffDirectoryDeletionTreeNode::new);
            createNodesForPaths(createdNodes, rightTreeChange.getDeletedPaths().stream().filter(checkInLeft), rightVersion,
                    MountTreeDiffDirectoryDeletionTreeNode::new);
        }

        @Override
        protected void process(List<Addition> chunks) {
            Map<DefaultMutableTreeNode, List<Addition>> nodeListMap = chunks.stream().collect(Collectors.groupingBy(Addition::getParent));
            for (DefaultMutableTreeNode node : nodeListMap.keySet()) {
                int[] childIndices = nodeListMap.get(node).stream().mapToInt(Addition::getChildIndex).sorted().toArray();
                try {
                    nodesWereInserted(node, childIndices);
                } catch (ArrayIndexOutOfBoundsException e) {
                    log.warn("error adding {} to {}", Arrays.toString(childIndices), node.getUserObject());
                }
            }
        }

        @Override
        protected void done() {
            nodeStructureChanged(root);
        }

        private void createNodesForPaths(Map<RepositoryPath, DefaultMutableTreeNode> createdNodes, Stream<RepositoryPath> paths, EntryVersion sourceVersion,
                BiFunction<EntryVersion, RepositoryPath, AbstractMountTreeDiffTreeNode> factory) {
            Iterator<RepositoryPath> iterator = paths.iterator();
            while (iterator.hasNext()) {
                RepositoryPath path = iterator.next();
                RepositoryPath unversionedPath = path.toUnversionedPath();
                DefaultMutableTreeNode parent = createdNodes.get(path.getParent());
                AbstractMountTreeDiffTreeNode child = factory.apply(sourceVersion, path);
                DefaultMutableTreeNode existingNode = createdNodes.put(unversionedPath, child);
                if (existingNode != null) {
                    // the new node overwrite the existing node
                    // we need to inherit its children
                    Enumeration<TreeNode> children = existingNode.children();
                    while (children.hasMoreElements()) {
                        child.add((MutableTreeNode) children.nextElement());
                    }
                    existingNode.removeFromParent();
                }
                int childIndex = parent.getChildCount();
                if (childIndex > 0) {
                    // we need to find where to add the child so the list of children stays sorted
                    childIndex = TreeUtilities.findInsertionIndex(parent, child, nodeComparator);
                }
                parent.insert(child, childIndex);
                publish(new Addition(parent, childIndex));
            }
        }

        private static class Addition {

            private final DefaultMutableTreeNode parent;
            private final int childIndex;

            public Addition(DefaultMutableTreeNode parent, int childIndex) {
                this.parent = parent;
                this.childIndex = childIndex;
            }

            public DefaultMutableTreeNode getParent() {
                return parent;
            }

            public int getChildIndex() {
                return childIndex;
            }
        }
    }
}
