/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.connection;


import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;

import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.object.Reportable;
import com.owc.singularity.repository.connection.ConnectionParameters;
import com.owc.singularity.studio.gui.connection.components.ConnectionInformationPanel;
import com.owc.singularity.studio.gui.renderer.AbstractRenderer;


/**
 * Reportable for {@link ConnectionParametersIOObject}s that reuses the
 * {@link ConnectionInformationPanel}.
 *
 * @author Gisa Meier
 * @since 9.3.0
 */
public class ConnectionInformationRenderer extends AbstractRenderer {

    @Override
    public Reportable createReportable(Object renderable, int desiredWidth, int desiredHeight) {
        return null;
    }

    @Override
    public String getName() {
        return "Connection";
    }

    @Override
    public Component getVisualizationComponent(Object renderable) {
        ConnectionParametersIOObject connectionObject = (ConnectionParametersIOObject) renderable;
        ConnectionParameters connectionParameters = connectionObject.getParameters();

        ConnectionInformationPanel connectionInformationPanel = new ConnectionInformationPanel(null, connectionParameters, false);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(connectionInformationPanel, BorderLayout.NORTH);
        return panel;
    }


}
