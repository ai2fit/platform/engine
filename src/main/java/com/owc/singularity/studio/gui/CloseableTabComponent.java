package com.owc.singularity.studio.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.swing.*;

import com.owc.singularity.studio.gui.look.ui.ButtonUI;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.UIConstants;

/**
 * Component to be used as tabComponent; Contains a JLabel to show the text and a JButton to close
 * the tab it belongs to
 */
public class CloseableTabComponent extends JPanel {

    private final JTabbedPane pane;
    private final JLabel label;
    private final JButton button;
    private final Predicate<Component> beforeClose;
    private final Consumer<Component> closeAction;
    private boolean enabled = true;

    public CloseableTabComponent(final JTabbedPane pane, Predicate<Component> beforeClose, Consumer<Component> closeAction) {
        // unset default FlowLayout' gaps
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.closeAction = closeAction;
        this.beforeClose = beforeClose;
        this.pane = pane;
        setOpaque(false);

        label = new TabLabel();
        add(label);
        // add more space between the label and the button
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        // tab button
        button = new TabButton();
        add(button);
        // add more space to the top of the component
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                if (!CloseableTabComponent.this.isEnabled())
                    return;
                Component component = e.getComponent();
                if (component instanceof CloseableTabComponent) {
                    if (e.isPopupTrigger()) {
                        tabPopupMenu.show(component, e.getX(), e.getY());
                    } else {
                        // show tab
                        int i = pane.indexOfTabComponent(CloseableTabComponent.this);
                        if (i != -1) {
                            pane.setSelectedIndex(i);
                        }
                    }
                    e.consume();
                }
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        // We use the enabled flag without setting
        // the component as disabled/enabled since disabled component cannot have interactions
        // anymore.
        // We try to set the look & feel of the inner components manually
        if (this.enabled != enabled) {
            Color foregroundColor;
            Cursor cursor;
            if (enabled) {
                foregroundColor = UIManager.getColor("Label.foreground");
                cursor = null;
            } else {
                foregroundColor = UIManager.getColor("Label.disabledForeground");
                cursor = UIConstants.INVALID;
            }
            label.setForeground(foregroundColor);
            label.setCursor(cursor);
            this.setCursor(cursor);
            button.setEnabled(enabled);
        }
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * A JLabel that read text, icon and tooltip from the containing {@link JTabbedPane}
     */
    private class TabLabel extends JLabel {

        public TabLabel() {
            ToolTipManager.sharedInstance().registerComponent(this);
        }

        public String getText() {
            int i = pane.indexOfTabComponent(CloseableTabComponent.this);
            if (i != -1) {
                return pane.getTitleAt(i);
            }
            return null;
        }

        @Override
        public Icon getIcon() {
            int i = pane.indexOfTabComponent(CloseableTabComponent.this);
            if (i != -1) {
                return pane.getIconAt(i);
            }
            return null;
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            return getToolTipText();
        }

        @Override
        public String getToolTipText() {
            int i = pane.indexOfTabComponent(CloseableTabComponent.this);
            if (i != -1) {
                invalidate();
                return pane.getToolTipTextAt(i);
            }
            return null;
        }

        @Override
        protected void processMouseEvent(MouseEvent e) {
            super.processMouseEvent(e);
            redirectToTabbedPane(e);
        }

        private void redirectToTabbedPane(MouseEvent e) {
            if (!CloseableTabComponent.this.isEnabled())
                return;
            pane.dispatchEvent(SwingUtilities.convertMouseEvent(this, e, pane));
        }
    }

    private class TabButton extends JButton implements ActionListener {

        public TabButton() {
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("close this tab");
            // Make the button looks the same for all Laf's
            setUI(new ButtonUI());
            // Make it transparent
            setContentAreaFilled(false);
            // No need to be focusable
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            // Making nice rollover effect
            // we use the same listener for all buttons
            addMouseListener(changeButtonStyleOnHoverMouseListener);
            setRolloverEnabled(true);
            // Close the proper tab by clicking the button
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!CloseableTabComponent.this.isEnabled())
                return;
            closeThisTab();
        }

        // we don't want to update UI for this button
        public void updateUI() {}

        // paint the cross
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();
            // shift the image for pressed buttons
            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }
            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.BLACK);
            if (getModel().isRollover()) {
                g2.setColor(Color.MAGENTA);
            }
            int delta = 6;
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
            g2.dispose();
        }
    }

    private void closeThisTab() {
        int i = pane.indexOfTabComponent(CloseableTabComponent.this);
        if (i != -1) {
            closeTab(i);
        }
    }

    private void closeAllTabs() {
        int count = pane.getTabCount();
        for (int i = count - 1; i >= 0; i--) {
            closeTab(i);
        }
    }

    private void closeOtherTabs() {
        int selfIndex = pane.indexOfTabComponent(CloseableTabComponent.this);
        if (selfIndex != -1) {
            closeOtherTabs(selfIndex);
        }
    }

    private void closeOtherTabs(int selfIndex) {
        int count = pane.getTabCount();
        for (int i = count - 1; i >= 0; i--) {
            if (i == selfIndex)
                continue;
            closeTab(i);
        }
    }

    private void closeTab(int i) {
        Component component = pane.getComponentAt(i);
        if (beforeClose.test(component)) {
            closeAction.accept(component);
        }
    }

    private final static JPopupMenu tabPopupMenu = new JPopupMenu() {

        {
            add(ResourceAction.of("tab.close", action -> {
                CloseableTabComponent source = (CloseableTabComponent) tabPopupMenu.getInvoker();
                source.closeThisTab();
            }));
            add(ResourceAction.of("tab.close_others", action -> {
                CloseableTabComponent source = (CloseableTabComponent) tabPopupMenu.getInvoker();
                source.closeOtherTabs();
            }));
            add(ResourceAction.of("tab.close_all", action -> {
                CloseableTabComponent source = (CloseableTabComponent) tabPopupMenu.getInvoker();
                source.closeAllTabs();
            }));
        }
    };
    private final static MouseListener changeButtonStyleOnHoverMouseListener = new MouseAdapter() {

        public void mouseEntered(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(true);
            }
        }

        public void mouseExited(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(false);
            }
        }
    };
}
