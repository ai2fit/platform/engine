/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.viewer;


import java.io.Serial;
import java.util.*;
import java.util.stream.IntStream;

import javax.swing.*;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.operator.tools.ExpressionEvaluationException;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * A model for the {@link ExampleSetViewer}, which supports selection/filtering of attributes
 * (columns) and examples (rows).
 * 
 * @author Alexander Mahler
 */
public class FilterableExampleSetTableModel extends ExampleSetTableModel {

    private int startIndex = 0;
    private int endIndex;
    private boolean invertRangeSelection = false;
    protected List<Attribute> excludedRegularAttributes = new LinkedList<>();
    protected List<Attribute> excludedSpecialAttributes = new LinkedList<>();
    protected ExampleSetRowFilter rowFilter = null;
    protected boolean invertRowFilter = false;
    protected int[] indexMap = null;

    @Serial
    private static final long serialVersionUID = -2813655012534422119L;

    public FilterableExampleSetTableModel(ExampleSet exampleSet) {
        super(exampleSet);
        endIndex = exampleSet.size();
    }

    @Override
    public int getRowCount() {
        if (rowFilter != null && indexMap != null) {
            return indexMap.length;
        } else if (invertRangeSelection)
            return exampleSet.size() - endIndex + startIndex;
        return endIndex - startIndex;
    }

    @Override
    public Object getValueAt(int row, int column) {
        int actualRow;
        if (rowFilter != null && indexMap != null) {
            actualRow = indexMap[row];
        } else if (invertRangeSelection) {
            if (row < startIndex)
                actualRow = row;
            else
                actualRow = row + endIndex - startIndex;
        } else {
            actualRow = row + startIndex;
        }
        if (column == 0) {
            return actualRow + 1;
        } else {
            ExampleSet set = getExampleSet();
            if (set != null && actualRow <= set.size()) {
                return getValueAsObject(set, actualRow, getColumnAttribute(column));
            } else {
                return null;
            }
        }
    }

    /**
     * Updates the selected row range as well and applies any given row filter.
     */
    public void updateRowSelection(int startRow, int endRow, boolean invert, ExampleSetRowFilter rowFilter, boolean rowFilterInvert) {
        if (startRow > exampleSet.size() || endRow < startRow)
            return;
        this.startIndex = startRow;
        this.endIndex = Math.min(endRow, exampleSet.size());
        this.invertRangeSelection = invert;
        invertRowFilter = rowFilterInvert;
        this.rowFilter = rowFilter;
        if (rowFilter != null) {
            boolean[] partitionIndices = new boolean[exampleSet.size()];
            try {
                rowFilter.prepare(exampleSet);
                for (int row = 0; row < exampleSet.size(); row++) {
                    boolean pass = rowFilter.pass(row, exampleSet);
                    partitionIndices[row] = pass ^ rowFilterInvert;
                }
            } catch (UserError | ExpressionEvaluationException exception) {
                indexMap = null;
                SwingTools.showSimpleErrorMessage("row_filter_error", exception, exception.getMessage());
                return;
            }
            if (this.invertRangeSelection) {
                indexMap = IntStream.range(0, exampleSet.size())
                        .filter(row -> row <= this.startIndex || row >= this.endIndex)
                        .filter(row -> partitionIndices[row])
                        .toArray();
            } else {
                indexMap = IntStream.range(this.startIndex, this.endIndex).filter(row -> partitionIndices[row]).toArray();
            }
        } else
            indexMap = null;
    }

    /**
     * Returns the currently applied row range selection and row filter parameters.
     */
    public RowInformation getRowSelectionParameters() {
        RowInformation rowInformation = new RowInformation();
        rowInformation.endIndex = endIndex;
        rowInformation.startIndex = startIndex;
        rowInformation.invertRangeSelection = invertRangeSelection;
        rowInformation.additionalRowFilter = rowFilter;
        rowInformation.invertRowFilter = invertRowFilter;
        return rowInformation;
    }

    /**
     * Returns the sum of the number of special attributes minus the number of excluded special
     * attributes, the number of regular attributes minus the number of excluded regular attributes
     * and 1 for the row no. column.
     */
    @Override
    public int getColumnCount() {
        return this.specialAttributes.length + this.regularAttributes.length + 1 - this.excludedRegularAttributes.size()
                - this.excludedSpecialAttributes.size();
    }

    /**
     * Returns the Attribute for the respective column. The excluded attributes are taken into
     * account here.
     */
    @Override
    public Attribute getColumnAttribute(int column) {
        if (column == 0) {
            return null;
        }
        int col = column - 1;
        Attribute attribute = null;
        Attribute[] remainingSpecialAttributes = getRemainingSpecialAttributes();
        if (col < remainingSpecialAttributes.length) {
            attribute = remainingSpecialAttributes[col];
        } else {
            Attribute[] remainingRegularAttributes = getRemainingRegularAttributes();
            int index = col - remainingSpecialAttributes.length;
            if (index < remainingRegularAttributes.length)
                attribute = remainingRegularAttributes[col - remainingSpecialAttributes.length];
        }
        return attribute;
    }

    public List<Attribute> getSelectedAttributes() {
        List<Attribute> selectedAttributes = new LinkedList<>();
        Collections.addAll(selectedAttributes, getRemainingSpecialAttributes());
        Collections.addAll(selectedAttributes, getRemainingRegularAttributes());
        return selectedAttributes;
    }

    protected Attribute[] getRemainingSpecialAttributes() {
        List<Attribute> remainingSpecialAttributes = new LinkedList<>();
        for (Attribute specialAttribute : specialAttributes) {
            if (!excludedSpecialAttributes.contains(specialAttribute))
                remainingSpecialAttributes.add(specialAttribute);
        }
        return remainingSpecialAttributes.toArray(Attribute[]::new);
    }

    protected Attribute[] getRemainingRegularAttributes() {
        List<Attribute> remainingRegularAttributes = new LinkedList<>();
        for (Attribute specialAttribute : regularAttributes) {
            if (!excludedRegularAttributes.contains(specialAttribute))
                remainingRegularAttributes.add(specialAttribute);
        }
        return remainingRegularAttributes.toArray(Attribute[]::new);
    }

    /**
     * Updates the selected attributes.
     */
    public void updateAttributeSelection(List<Attribute> includeAttributes) {
        this.excludedRegularAttributes = Arrays.stream(regularAttributes).filter(attribute -> !includeAttributes.contains(attribute)).toList();
        this.excludedSpecialAttributes = Arrays.stream(specialAttributes).filter(attribute -> !includeAttributes.contains(attribute)).toList();
    }

    /**
     * A utility class, which holds the selection parameters.
     */
    public static class RowInformation {

        public int startIndex;
        public int endIndex;
        public boolean invertRangeSelection;
        public ExampleSetRowFilter additionalRowFilter;
        public boolean invertRowFilter;
    }
}
