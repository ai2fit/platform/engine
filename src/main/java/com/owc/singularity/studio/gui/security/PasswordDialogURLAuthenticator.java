package com.owc.singularity.studio.gui.security;

import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import com.owc.singularity.engine.tools.GlobalAuthenticator;
import com.owc.singularity.studio.gui.tools.PasswordDialog;

public class PasswordDialogURLAuthenticator implements GlobalAuthenticator.URLAuthenticator {

    @Override
    public PasswordAuthentication getAuthentication(URL url) {
        try {
            URI uri = url.toURI();
            // default authentication
            return PasswordDialog.getPasswordAuthentication(uri, uri.getUserInfo(), false, true, null);
        } catch (URISyntaxException e) {
            return null;
        }
    }
}
