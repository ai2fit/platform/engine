/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JComponent;

import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * 
 * @author Simon Fischer, Marco Boeck
 */
public class CutCopyPasteDeleteAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private final String action;

    private JComponent focusOwner = null;

    CutCopyPasteDeleteAction(String i18nKey, String action) {
        super(i18nKey);
        this.action = action;
        putValue(ACTION_COMMAND_KEY, action);
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addPropertyChangeListener("permanentFocusOwner", evt -> {
            Object o = evt.getNewValue();
            if (o instanceof JComponent) {
                focusOwner = (JComponent) o;
            } else {
                focusOwner = null;
            }
        });
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        if (focusOwner == null) {
            return;
        }
        Action a = focusOwner.getActionMap().get(action);
        if (a != null) {
            a.actionPerformed(new ActionEvent(focusOwner, ActionEvent.ACTION_PERFORMED, e.getActionCommand()));
        }
    }

    @Override
    public boolean isEnabled() {
        if (focusOwner == null)
            return false;
        Action a = focusOwner.getActionMap().get(action);
        if (a != null) {
            return a.isEnabled();
        }
        return super.isEnabled();
    }
}
