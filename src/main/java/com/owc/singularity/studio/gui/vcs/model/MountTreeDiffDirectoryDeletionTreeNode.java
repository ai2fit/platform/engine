package com.owc.singularity.studio.gui.vcs.model;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffDirectoryDeletionTreeNode extends AbstractMountTreeDiffDirectoryTreeNode {

    protected MountTreeDiffDirectoryDeletionTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }
}
