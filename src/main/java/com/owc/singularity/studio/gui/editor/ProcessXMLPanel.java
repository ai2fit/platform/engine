/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessUserInteractionListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawUtils;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.tools.ExtendedJToolBar;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * A text area for editing the process as XML. This editor is the second possible way to edit or
 * create SingularityEngine processes. All changes are reflected by the process. However, it should
 * be more convenient to use the tree view for process design. <br/>
 *
 * This XML editor support very simple syntax highlighting based on keyword parsing.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
public class ProcessXMLPanel extends JPanel implements ProcessEditorEventListener, ProcessUserInteractionListener, FocusProcessChangeEventListener, Dockable {

    private static final long serialVersionUID = 4172143138689034659L;

    public static final String XML_EDITOR_DOCK_KEY = "xml_editor";

    private final DockKey DOCK_KEY = new ResourceDockKey(XML_EDITOR_DOCK_KEY);
    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    private final RSyntaxTextArea editor;

    private Timer delayedUpdateTimer = new Timer("update-timer-xml", true);

    private TimerTask delayedUpdateTask;

    public ProcessXMLPanel() {
        super(new BorderLayout());

        // create text area
        this.editor = new RSyntaxTextArea(new RSyntaxDocument(SyntaxConstants.SYNTAX_STYLE_XML)) {

            @Override
            public synchronized void setText(String t) {
                super.setText(t);
            }
        };
        this.editor.setAnimateBracketMatching(true);
        this.editor.setAutoIndentEnabled(true);
        this.editor.setSelectionColor(Colors.TEXT_HIGHLIGHT_BACKGROUND);
        this.editor.setBorder(null);

        JToolBar toolBar = new ExtendedJToolBar();
        toolBar.setBorder(null);
        toolBar.add(new ResourceAction(true, "xml_editor.apply_changes") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                try {
                    validateProcess();
                } catch (XMLException | OperatorCreationException e1) {
                    LogService.getRoot().warn("com.owc.singularity.studio.gui.processeditor.XMLEditor.failed_to_parse_process");
                }
            }
        });
        toolBar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.TEXTFIELD_BORDER));

        add(toolBar, BorderLayout.NORTH);
        RTextScrollPane rTextScrollPane = new RTextScrollPane(editor);
        rTextScrollPane.setBorder(null);
        add(rTextScrollPane, BorderLayout.CENTER);
    }

    public void setText(String text) {
        this.editor.setText(text);
    }

    @Override
    public void onProcessEdit(AbstractPipeline process) {
        if (process == null) {
            setText("");
            return;
        }
        synchronized (delayedUpdateTimer) {
            if (delayedUpdateTask != null) {
                delayedUpdateTask.cancel();
            }
            delayedUpdateTask = new TimerTask() {

                @Override
                public void run() {
                    String processXML = process.getRootOperator().getXML(false);
                    SwingUtilities.invokeLater(() -> setText(processXML));
                }
            };
            delayedUpdateTimer.schedule(delayedUpdateTask, 500);
        }
    }

    @Override
    public void onProcessLoad(AbstractPipeline process) {
        onProcessEdit(process);
    }

    @Override
    public void onProcessReadOnlyStateChange(boolean readOnly) {
        setEnabled(!readOnly);
    }

    @Override
    public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
        onProcessEdit(currentlyFocusedPanel != null ? currentlyFocusedPanel.getProcess() : null);
        if (currentlyFocusedPanel != null) {
            setEnabled(!currentlyFocusedPanel.isReadOnly());
        }
    }

    /** Just jumps to the position of the currently selected operator. */
    @Override
    public void onOperatorSelectionChange(List<Operator> newSelection) {
        if (!newSelection.isEmpty()) {
            Operator currentOperator = newSelection.get(0);
            String name = currentOperator.getName();
            synchronized (this.editor) {
                String text = this.editor.getText();
                int start = text.indexOf("\"" + name + "\"");
                int end = start + name.length() + 1;
                if (start >= 0 && editor.getDocument().getLength() > end) {
                    this.editor.select(start + 1, end);
                    this.editor.setCaretPosition(end);
                }
            }
        }
    }

    /**
     * Validates the process represented by the current xml. Will update the actual process if the
     * xml representation differs. This diff will ignore white space changes, but will update the
     * displayed xml with the parser conform version.
     * 
     * @throws OperatorCreationException
     */
    public synchronized void validateProcess() throws XMLException, OperatorCreationException {
        String editorContent = getXMLFromEditor();
        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
        AbstractPipeline oldProcess = mainProcessPanel.getProcess();
        String oldXML = oldProcess.getRootOperator().getXML(false);
        if (oldXML.trim().equals(editorContent)) {
            return;
        }
        AbstractPipeline newProcess = XMLImporter.parse(editorContent);
        ProcessEditorPanel processRenderer = mainProcessPanel.getProcessEditor();
        ProcessDrawUtils.ensureOperatorsHaveLocation(newProcess, processRenderer.getModel());
        String newXML = newProcess.getRootOperator().getXML(false);
        if (!newXML.equals(oldXML)) {
            newProcess.setPath(oldProcess.getPath());
            mainProcessPanel.updateProcess(newProcess);
        } else {
            setText(oldXML);
        }
    }

    public String getXMLFromEditor() {
        return this.editor.getText().trim();
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }
}
