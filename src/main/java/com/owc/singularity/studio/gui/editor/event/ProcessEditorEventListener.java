/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.event;


import java.util.EventListener;

import com.owc.singularity.engine.pipeline.AbstractPipeline;


public interface ProcessEditorEventListener extends EventListener {

    default void onProcessLoad(AbstractPipeline process) {}

    default void onProcessReadOnlyStateChange(boolean readOnly) {}

    /** Notifies the component that process was updated in some way. */
    void onProcessEdit(AbstractPipeline process);
}
