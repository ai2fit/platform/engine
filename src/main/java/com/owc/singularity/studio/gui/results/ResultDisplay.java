/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.results;


import java.awt.BorderLayout;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.*;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.object.AbstractIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.engine.pipeline.LoggingListener;
import com.owc.singularity.engine.pipeline.PipelineExecutionListener;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.CloseAllResultsAction;
import com.owc.singularity.studio.gui.editor.ProcessLogTab;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.owc.singularity.studio.gui.tools.UpdateQueue;
import com.owc.singularity.studio.gui.tools.dialogs.DecisionRememberingConfirmDialog;
import com.owc.singularity.studio.gui.viewer.DataTableViewer;
import com.vlsolutions.swing.docking.*;


/**
 * {@link ResultDisplay} that adds each result to an individual {@link Dockable}. In addition, it
 * displays a result history overview.
 *
 * @author Simon Fischer
 *
 */
public class ResultDisplay extends JPanel implements Dockable, FocusProcessChangeEventListener {


    public static final String AUTO_CLOSABLE_RESULT_PREFIX = "auto_";
    public static final String MANUAL_RESULT_PREFIX = "manual_";

    public static final String RESULT_DOCK_KEY = "result";

    private static final long serialVersionUID = 1L;

    private static final DockKey dockKey = new ResourceDockKey(RESULT_DOCK_KEY);
    {
        dockKey.setDockGroup(MainFrame.DOCK_GROUP_RESULTS);
        DockableActionCustomizer customizer = new DockableActionCustomizer() {

            @Override
            public void visitTabSelectorPopUp(JPopupMenu popUpMenu, Dockable dockable) {
                popUpMenu.add(new JMenuItem(new CloseAllResultsAction(MainFrame.INSTANCE)));
            }
        };
        customizer.setTabSelectorPopUpCustomizer(true); // enable tabbed dock custom popup menu
                                                        // entries
        dockKey.setActionCustomizer(customizer);
        dockKey.setCloseEnabled(false);
    }

    private final Map<String, DataTable> dataTables = new HashMap<>();

    private final UpdateQueue tableUpdateQueue = new UpdateQueue("ResultDisplayDataTableViewUpdater");

    private final ResultOverview overview = new ResultOverview();

    public ResultDisplay() {
        setLayout(new BorderLayout());
        ExtendedJScrollPane overviewScrollpane = new ExtendedJScrollPane(overview);
        overviewScrollpane.setBorder(null);
        overviewScrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        overviewScrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        add(overviewScrollpane, BorderLayout.CENTER);
        tableUpdateQueue.start();

        // register shutdown hook to remove result tabs
        SingularityEngine.addShutdownHook(() -> {
            List<Dockable> toClose = new LinkedList<>();
            for (DockableState state : MainFrame.INSTANCE.getDockingDesktop().getContext().getDockables()) {
                if (state.getDockable().getDockKey().getKey().startsWith(ResultTab.DOCKKEY_PREFIX)) {
                    toClose.add(state.getDockable());
                }
            }
            for (Dockable dockable : toClose) {
                MainFrame.INSTANCE.getPerspectiveController().removeFromAllPerspectives(dockable);
            }
        });

    }

    public void init(MainFrame mf) {
        DockingDesktop desktop = mf.getDockingDesktop();

        desktop.addDockableStateChangeListener(e -> {
            DockableState newState = e.getNewState();
            Dockable newDockable = newState.getDockable();
            if (newDockable == ResultDisplay.this) {
                // this is relevant for all perspectives except for the result perspective
                DockableState previousState = e.getPreviousState();
                boolean isClosing = newState.isClosed();
                if (!isClosing && !(previousState == null || previousState.isClosed())) {
                    // neither opening nor closing
                    return;
                }
                for (DockableState dockableState : desktop.getDockables()) {
                    Dockable dockable = dockableState.getDockable();
                    boolean isResult = dockable instanceof ResultTab && ((ResultTab) dockable).getResultViewComponent() != null;
                    boolean isLog = dockable instanceof ProcessLogTab && ((ProcessLogTab) dockable).getDataTable() != null;
                    // only take care of results and logs that were not cleaned up
                    if (!isResult && !isLog) {
                        continue;
                    }
                    if (isClosing) {
                        // close all results for this perspective and this perspective only
                        desktop.close(dockable);
                    } else {
                        // open all results if not already visible
                        showTab(dockable);
                    }
                }
                return;
            }
            if (!newState.isClosed()) {
                return;
            }
            if (desktop.getDockableState(ResultDisplay.this) == null || desktop.getDockableState(ResultDisplay.this).isClosed()) {
                // ignore closed result and process log tabs if the result view was already closed
                return;
            }
            if (newDockable instanceof ResultTab) {
                ResultTab rt = (ResultTab) newDockable;
                rt.freeResources();
                MainFrame.INSTANCE.getPerspectiveController().removeFromAllPerspectives(rt);
                resetPropertyChangeListener(getDockKey());
            } else if (newDockable instanceof ProcessLogTab) {
                ProcessLogTab pt = (ProcessLogTab) newDockable;
                if (pt.getDataTable() != null) {
                    dataTables.remove(pt.getDataTable().getName());
                }
                pt.freeResources();
                MainFrame.INSTANCE.getPerspectiveController().removeFromAllPerspectives(pt);
            }
        });
    }

    private boolean isAskingForPerspectiveSwitch = false;

    private void askForPerspectiveSwitch(boolean clearPreviousResults) throws InterruptedException, InvocationTargetException {
        SwingUtilities.invokeAndWait(() -> {
            MainFrame mainFrame = MainFrame.INSTANCE;
            PerspectiveController perspectiveController = mainFrame.getPerspectiveController();
            DockableState dockableState = mainFrame.getDockingDesktop().getDockableState(this);
            // do not switch perspective if the result history is currently visible
            if (isAskingForPerspectiveSwitch || dockableState != null && !dockableState.isClosed()
                    || perspectiveController.getSelectedPerspective().getName().equals(PerspectiveController.RESULT)) {
                if (clearPreviousResults)
                    clearNow();
                return;
            }

            isAskingForPerspectiveSwitch = true;
            if (DecisionRememberingConfirmDialog.confirmAction("show_results_on_creation",
                    StudioProperties.PROPERTY_SINGULARITY_GUI_AUTO_SWITCH_TO_RESULTVIEW)) {
                perspectiveController.showPerspective(PerspectiveController.RESULT);
            }
            isAskingForPerspectiveSwitch = false;
            if (clearPreviousResults)
                clearNow();
        });
    }

    public void showData(final List<IOObject> objects, final String statusMessage, Icon icon) {
        if (objects == null || objects.size() == 0) {
            return;
        }
        new ProgressThread("creating_display") {

            @Override
            public void run() {
                try {
                    try {
                        askForPerspectiveSwitch(true);
                    } catch (InterruptedException | InvocationTargetException e) {
                        LogService.getRoot().error("com.owc.singularity.studio.gui.flow.ResultDisplay.error_during_perspective_switch", e);
                    }
                    getProgressListener().setTotal(objects.size() + 1);
                    overview.addResults(MainFrame.INSTANCE.getMainProcessPanel().getProcess(), objects, statusMessage);
                    getProgressListener().setCompleted(1);
                    int i = 0;
                    for (IOObject ioobject : objects) {
                        if (ioobject instanceof AbstractIOObject) {
                            i++;
                            showResultNow(ioobject, ResultDisplay.AUTO_CLOSABLE_RESULT_PREFIX + currentId.getAndIncrement(), icon);
                            getProgressListener().setCompleted(i + 1);
                        }
                    }
                } finally {
                    getProgressListener().complete();
                }
            }
        }.start();
    }

    private static AtomicInteger currentId = new AtomicInteger();

    public void showResult(final IOObject result, Icon icon) {
        new ProgressThread("creating_display") {

            @Override
            public void run() {
                try {
                    askForPerspectiveSwitch(false);
                } catch (InterruptedException | InvocationTargetException e) {
                    LogService.getRoot().error("com.owc.singularity.studio.gui.flow.ResultDisplay.error_during_perspective_switch", e);
                }
                getProgressListener().setTotal(100);
                getProgressListener().setCompleted(10);
                showResultNow(result, MANUAL_RESULT_PREFIX + currentId.getAndIncrement(), icon);
                getProgressListener().setCompleted(100);
                getProgressListener().complete();
            }
        }.start();
    }

    /** Creates a display on the current thread and displays it (on the EDT). */
    private void showResultNow(final IOObject result, final String id, Icon icon) {
        ResultTab tab = (ResultTab) MainFrame.INSTANCE.getDockingDesktop().getContext().getDockableByKey(ResultTab.DOCKKEY_PREFIX + id);
        if (tab == null) {
            tab = new ResultTab(ResultTab.DOCKKEY_PREFIX + id);
        }
        showResultTab(tab, result, icon);
    }

    /**
     * Update the DataTableViewers. This does not happen on the EDT and is executed asynchronously
     * by an {@link UpdateQueue}.
     */
    private void updateDataTables() {
        final Collection<DataTable> copy = new LinkedList<>(dataTables.values());
        // this is time consuming, so execute off EDT
        tableUpdateQueue.execute(new Runnable() {

            @Override
            public void run() {
                final Collection<DataTableViewer> viewers = new LinkedList<>();
                for (DataTable table : copy) {
                    viewers.add(new DataTableViewer(table, true, DataTableViewer.TABLE_MODE));
                }
                installDataTableViewers(viewers);
            }

            @Override
            public String toString() {
                return "Update data table list to size " + copy.size();
            }
        });
    }

    /** Adds the collection of components on the EDT (after removing the old tables. */
    private void installDataTableViewers(final Collection<DataTableViewer> viewers) {
        SwingUtilities.invokeLater(() -> {
            for (DataTableViewer viewer : viewers) {
                ProcessLogTab tab = (ProcessLogTab) MainFrame.INSTANCE.getDockingDesktop()
                        .getContext()
                        .getDockableByKey(ProcessLogTab.DOCKKEY_PREFIX + viewer.getDataTable().getName());
                if (tab == null) {
                    tab = new ProcessLogTab(ProcessLogTab.DOCKKEY_PREFIX + viewer.getDataTable().getName());
                }
                showTab(tab);
                tab.setDataTableViewer(viewer);
            }
        });
    }

    private void showTab(final Dockable dockable) {
        SwingUtilities.invokeLater(() -> MainFrame.INSTANCE.getPerspectiveController().showTabInAllPerspectives(dockable, ResultDisplay.this));
    }

    private void showResultTab(final ResultTab tab, final IOObject result, Icon icon) {
        SwingUtilities.invokeLater(() -> {

            MainFrame.INSTANCE.getPerspectiveController().showTabInAllPerspectives(tab, ResultDisplay.this);
            tab.showResult(result, icon);
        });
    }


    public void addDataTable(final DataTable dataTable) {
        ResultDisplay.this.dataTables.put(dataTable.getName(), dataTable);
        updateDataTables();
    }

    private void clear(boolean alsoClearLogs) {
        if (SwingUtilities.isEventDispatchThread()) {
            clearNow(alsoClearLogs);
        } else {
            try {
                SwingUtilities.invokeAndWait(this::clearNow);
            } catch (InterruptedException e) {
                LogService.getRoot()
                        .warn("com.owc.singularity.studio.gui.processeditor.results.DockableResultDisplay.interrupted_while_closing_result_tabs", e);
                Thread.currentThread().interrupt();
            } catch (InvocationTargetException e) {
                LogService.getRoot()
                        .warn("com.owc.singularity.studio.gui.processeditor.results.DockableResultDisplay.exception_while_closing_result_tabs", e, e);
            }
        }
    }


    private void clearNow() {
        clearNow(true);
    }

    private void clearNow(boolean alsoClearLogs) {
        List<Dockable> toClose = new LinkedList<>();
        for (DockableState state : MainFrame.INSTANCE.getDockingDesktop().getContext().getDockables()) {
            if (state.getDockable().getDockKey().getKey().startsWith(ResultTab.DOCKKEY_PREFIX + AUTO_CLOSABLE_RESULT_PREFIX)
                    || alsoClearLogs && state.getDockable().getDockKey().getKey().startsWith(ProcessLogTab.DOCKKEY_PREFIX)
                    || (state.getDockable() instanceof ResultTab && !((ResultTab) state.getDockable()).hasResult())) {
                toClose.add(state.getDockable());
            }
        }
        // fix for "delete old results" dialog after breakpoint resume
        if ((!toClose.isEmpty() || !dataTables.isEmpty())) {
            ResultDisplay.this.dataTables.clear();
            // updateDataTables();
            for (Dockable dockable : toClose) {
                if (dockable instanceof ResultTab) {
                    ((ResultTab) dockable).freeResources();
                } else if (dockable instanceof ProcessLogTab) {
                    ((ProcessLogTab) dockable).freeResources();
                }
                MainFrame.INSTANCE.getPerspectiveController().removeFromAllPerspectives(dockable);
            }
            resetPropertyChangeListener(getDockKey());
        }
    }

    /**
     * Reset a {@link DockKey} change listeners by removing all {@link PropertyChangeListener}
     * 
     * @param dockKey
     *            the dock key to reset
     */
    private static void resetPropertyChangeListener(DockKey dockKey) {
        dockKey.removePropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

            }

            @Override
            public boolean equals(Object obj) {
                return obj instanceof PropertyChangeListener;
            }
        });
    }


    public void clearAll() {
        clearAllExcept();
    }

    public void clearAllExcept(String... key) {
        if (key == null) {
            return;
        }

        for (DockableState state : MainFrame.INSTANCE.getDockingDesktop().getContext().getDockables()) {
            List<String> exemptionKeys = Arrays.asList(key);
            String dockKeyKey = state.getDockable().getDockKey().getKey();
            if ((dockKeyKey.startsWith(ResultTab.DOCKKEY_PREFIX) || dockKeyKey.startsWith(ProcessLogTab.DOCKKEY_PREFIX))
                    && !exemptionKeys.contains(dockKeyKey)) {
                // skipped if dock key is in exemption list
                MainFrame.INSTANCE.getPerspectiveController().removeFromAllPerspectives(state.getDockable());
            }
        }
    }

    // Listeners

    private final LoggingListener logListener = new LoggingListener() {

        @Override
        public void addDataTable(final DataTable dataTable) {
            ResultDisplay.this.addDataTable(dataTable);
        }

        @Override
        public void removeDataTable(final DataTable dataTable) {
            ResultDisplay.this.dataTables.remove(dataTable.getName());
            updateDataTables();
        }
    };

    private final PipelineExecutionListener processListener = new PipelineExecutionListener() {

        @Override
        public void pipelineFinished(AbstractPipeline process) {}

        @Override
        public void pipelineOperatorFinished(AbstractPipeline process, Operator op) {}

        @Override
        public void pipelineOperatorStarted(AbstractPipeline process, Operator op) {}

        @Override
        public void pipelineStarts(AbstractPipeline process) {
            clear(true);
        }
    };

    private final BreakpointListener breakpointListener = new BreakpointListener() {

        @Override
        public void resume() {
            clear(false);
        }

        @Override
        public void breakpointReached(AbstractPipeline process, Operator op, List<IOObject> iocontainer, int location) {}
    };

    // Dockable

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return dockKey;
    }

    // ProcessEditor

    private AbstractPipeline process;

    @Override
    public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
        if (this.process != null) {
            this.process.removeLoggingListener(logListener);
            this.process.getRootOperator().removePipelineListener(processListener);
            this.process.removeBreakpointListener(breakpointListener);
        }
        this.process = currentlyFocusedPanel != null ? currentlyFocusedPanel.getProcess() : null;
        if (this.process != null) {
            this.process.addLoggingListener(logListener);
            this.process.getRootOperator().addPipelineListener(processListener);
            this.process.addBreakpointListener(breakpointListener);
        }
    }
}
