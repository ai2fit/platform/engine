/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa, Tobias Malbrecht
 */
public class PauseAction extends ResourceAction {

    private static final long serialVersionUID = -8416546573798401295L;

    public PauseAction() {
        super("pause");
        setCondition(Condition.PROCESS_RUNNING, ConditionReaction.MANDATORY);
    }

    /**
     * @deprecated use {@link #PauseAction()} instead
     */
    @Deprecated
    public PauseAction(MainFrame mainFrame) {
        this();
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        MainFrame.INSTANCE.pauseProcess();
    }
}
