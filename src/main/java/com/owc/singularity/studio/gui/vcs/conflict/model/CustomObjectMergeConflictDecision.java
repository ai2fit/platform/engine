package com.owc.singularity.studio.gui.vcs.conflict.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.vcs.MergeConflict;

public class CustomObjectMergeConflictDecision implements MergeConflictDecision {

    private final Object data;
    private final RepositoryPath resultPath;

    public CustomObjectMergeConflictDecision(MergeConflict conflict, Object data) {
        this.data = data;
        this.resultPath = conflict.getConflictRepositoryPath().toUnversionedPath();
    }

    @Override
    public InputStream getInputStream() throws IOException {
        // override with our changes
        Entries.getEntry(resultPath).write(null, data);
        return Files.newInputStream(resultPath);
    }

    @Override
    public RepositoryPath getResultRepositoryPath() {
        return resultPath;
    }

    @Override
    public String getDescription() {
        return "using custom changes";
    }
}
