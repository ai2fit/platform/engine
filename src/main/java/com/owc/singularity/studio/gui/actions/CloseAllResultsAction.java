/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.dialogs.DecisionRememberingConfirmDialog;


/**
 * An action to close all currently open results.
 * 
 * @author Marco Boeck
 * 
 */
public class CloseAllResultsAction extends ResourceAction {

    private final MainFrame mainframe;

    public CloseAllResultsAction(MainFrame mainframe) {
        super(true, "close_all_results");
        this.mainframe = mainframe;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        if (mainframe != null) {
            if (mainframe.getResultDisplay() != null) {
                if (DecisionRememberingConfirmDialog.confirmAction("close_all_results", StudioProperties.PROPERTY_CLOSE_ALL_RESULTS_NOW)) {
                    mainframe.getResultDisplay().clearAll();
                }
            }
        }
    }
}
