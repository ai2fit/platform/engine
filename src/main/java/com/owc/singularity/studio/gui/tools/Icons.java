package com.owc.singularity.studio.gui.tools;

import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import com.owc.singularity.engine.tools.I18N;

public class Icons {

    private static final Map<String, Icons> cachedIcons = new HashMap<>();

    public static Icons fromName(String iconName) {
        synchronized (cachedIcons) {
            return cachedIcons.computeIfAbsent(iconName, Icons::new);
        }
    }

    /**
     * the large icon for the constraint violation operators
     */
    private static final String I18N_UNSUPPORTED_ICON_KEY = "gui.constraint.operator.unsupported_datasource.icon";

    private ImageIcon icon16x16 = null;
    private ImageIcon icon24x24 = null;
    private ImageIcon icon48x48 = null;
    private final String iconName;
    private final boolean undefinedIcon;

    private Icons(String iconName) {
        if (iconName == null) {
            this.iconName = I18N.getGUIMessage(I18N_UNSUPPORTED_ICON_KEY);
            this.undefinedIcon = true;
        } else {
            this.iconName = iconName;
            this.undefinedIcon = false;
        }
    }

    public ImageIcon getIcon16x16() {
        if (icon16x16 == null) {
            ImageIcon icon = SwingTools.createIcon("16/" + iconName, IconSize.SMALL);
            // check icon size as icons in cache can be loaded in double resolution (@2x) folder
            if (icon.getIconHeight() != IconSize.SMALL.getSize() && icon.getIconWidth() != IconSize.SMALL.getSize()) {
                icon16x16 = new ScaledImageIcon(icon.getImage(), IconSize.SMALL.getSize(), IconSize.SMALL.getSize());
            } else {
                icon16x16 = icon;
            }
        }
        return icon16x16;
    }

    public ImageIcon getIcon24x24() {
        if (icon24x24 == null) {
            icon24x24 = SwingTools.createIcon("24/" + iconName, IconSize.MEDIUM);
        }
        return icon24x24;
    }

    public ImageIcon getIcon48x48() {
        if (icon48x48 == null) {
            icon48x48 = SwingTools.createIcon("48/" + iconName, IconSize.HUGE);
        }
        return icon48x48;
    }

    /**
     * Returns a small icon
     *
     * @return a 16x16 if available, a 24x24 px or {@code null} otherwise
     */
    public ImageIcon getSmallIcon() {
        if (getIcon16x16() != null) {
            return getIcon16x16();
        } else {
            return getIcon24x24();
        }
    }

    /**
     * Returns a large icon
     *
     * @return a 48x48 px icon if available, 24x24 px or {@code null} otherwise
     */
    public ImageIcon getLargeIcon() {
        if (getIcon48x48() != null) {
            return getIcon48x48();
        } else {
            return getIcon24x24();
        }
    }

    public String getIconName() {
        return iconName;
    }

    public boolean isUndefined() {
        return undefinedIcon;
    }
}
