package com.owc.singularity.studio.gui.repository;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.owc.singularity.engine.tools.PasswordInputCanceledException;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.RecentLocationsManager;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ProgressThreadDialog;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;
import com.owc.singularity.studio.gui.tools.dialogs.SelectionDialog;

class InteractiveCopyLocationTask extends ProgressThread {

    private final RepositoryPath targetPath;
    private final List<RepositoryPath> transferredPaths;
    private final boolean move;

    public InteractiveCopyLocationTask(RepositoryPath targetPath, List<RepositoryPath> transferredPaths, boolean move) {
        super("copy_repository_entry", true);
        this.targetPath = targetPath;
        this.transferredPaths = transferredPaths;
        this.move = move;
    }

    private static final class UserDecisions {

        boolean repeatDecision = false;
        boolean overwriteIfExists = false;
        int lastDecision = 0;
    }

    private static final int INSERT = 1;
    private static final int OVERWRITE = 2;
    private static final int SKIP = 3;

    /**
     * Iteratively perform copy / move
     */
    @Override
    public void run() {
        boolean isSingleEntryOperation = transferredPaths.size() == 1;

        // Initialize progress listener
        getProgressListener().setTotal(transferredPaths.size());

        final UserDecisions userDecisions = new UserDecisions();

        for (int i = 0; i < transferredPaths.size(); i++) {
            RepositoryPath transferredPath = transferredPaths.get(i);
            // check if Entry already exists, overwrite?
            if (Files.exists(targetPath.resolve(transferredPath.getFileName()))) {
                if (!userDecisions.repeatDecision) {
                    final List<String> optionsToSelect = new ArrayList<>(3);
                    optionsToSelect.add("existing_entry.insert");
                    optionsToSelect.add("existing_entry.overwrite");
                    optionsToSelect.add("existing_entry.skip");
                    final List<String> optionsToCheck = new ArrayList<>(1);
                    if (!isSingleEntryOperation) {
                        optionsToCheck.add("existing_entry.repeat");
                    }

                    int result = SwingTools.invokeAndWaitWithResult(() -> {
                        SelectionDialog selectionDialog = new SelectionDialog(ProgressThreadDialog.getInstance(), "existing_entry",
                                SelectionDialog.OK_CANCEL_OPTION, new String[] { transferredPath.getFileName().toString() }, optionsToSelect, optionsToCheck)
                                        .showDialog();

                        if (selectionDialog.isOptionSelected("existing_entry.insert")) {
                            userDecisions.lastDecision = INSERT;
                        } else if (selectionDialog.isOptionSelected("existing_entry.overwrite")) {
                            userDecisions.lastDecision = OVERWRITE;
                        } else {
                            userDecisions.lastDecision = SKIP;
                        }
                        if (selectionDialog.isOptionChecked("existing_entry.repeat")) {
                            userDecisions.repeatDecision = true;
                        }
                        return selectionDialog.getResult();
                    });

                    if (result != SelectionDialog.OK_OPTION) {
                        return;
                    }
                }

                switch (userDecisions.lastDecision) {
                    case INSERT:
                        userDecisions.overwriteIfExists = false;
                        break;
                    case OVERWRITE:
                        userDecisions.overwriteIfExists = true;
                        break;
                    case SKIP:
                    default:
                        continue;
                }
            }


            // Do copy or move operation
            // Extracted to own method for lock handling and retry calls
            boolean done = executeCopyOrMoveOperation(transferredPath, targetPath, isSingleEntryOperation, userDecisions.overwriteIfExists);
            if (!done) {
                break;
            }
            getProgressListener().setCompleted(i + 1);
        }

        getProgressListener().complete();
    }

    /**
     * Runs the copy or move operation using {@link RepositoryManager}
     *
     * @return <code>false</code> if the user chose to cancel the operation; <code>true</code>
     *         otherwise
     */
    private boolean executeCopyOrMoveOperation(RepositoryPath sourcePath, RepositoryPath targetPath, boolean isSingleEntryOperation,
            boolean overwriteIfExists) {
        try {
            RepositoryPath destinationPath = targetPath.resolve(sourcePath.getFileName());

            if (isMoveOperation()) {
                RecentLocationsManager.replaceRecentFile(sourcePath, destinationPath);
                if (overwriteIfExists)
                    Files.move(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
                else if (Files.exists(destinationPath)) {
                    RepositoryPath destinationOverridePath = getDestinationPathWithoutOverwrite(destinationPath);
                    Files.move(sourcePath, destinationOverridePath);
                } else
                    Files.move(sourcePath, destinationPath);
            } else {
                if (overwriteIfExists)
                    Entries.copyRecursively(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
                else if (Files.exists(destinationPath)) {
                    RepositoryPath destinationOverridePath = getDestinationPathWithoutOverwrite(destinationPath);
                    Entries.copyRecursively(sourcePath, destinationOverridePath);
                } else
                    Entries.copyRecursively(sourcePath, destinationPath);
            }
        } catch (Exception e) {
            return handleException(e, sourcePath, targetPath, isSingleEntryOperation, overwriteIfExists);
        }
        return true;
    }

    private static RepositoryPath getDestinationPathWithoutOverwrite(RepositoryPath destinationPath) {
        int copyOverrides = 2;
        RepositoryPath destinationOverridePath = (RepositoryPath) destinationPath.resolveSibling(destinationPath.getFileName().toString() + " - Copy");
        while (Files.exists(destinationOverridePath)) {
            destinationOverridePath = (RepositoryPath) destinationPath
                    .resolveSibling(destinationPath.getFileName().toString() + " - Copy (" + (copyOverrides++) + ")");
        }
        return destinationOverridePath;
    }

    private boolean isMoveOperation() {
        return move;
    }

    /**
     * Handles a repository exception during copy/move.
     *
     * @return {@code true} if user pressed retry and it worked OR if he pressed no-retry;
     *         {@code false} if user pressed cancel
     */
    private boolean handleException(Exception e, RepositoryPath location, RepositoryPath target, boolean isSingleEntryOperation, boolean overwriteIfExists) {
        if (e.getCause() != null && e.getCause() instanceof PasswordInputCanceledException) {
            // no extra dialog if login dialog was canceled
            return false;
        }
        // Do not show "cancel" option, if is is an single entry operation
        final int dialogMode = isSingleEntryOperation ? ConfirmDialog.YES_NO_OPTION : ConfirmDialog.YES_NO_CANCEL_OPTION;
        final String locationName = location.getFileName().toString();
        final AtomicInteger result = new AtomicInteger();

        SwingTools.invokeAndWait(() -> {
            final ConfirmDialog dialog;
            if (e.getMessage() != null && !e.getMessage().isEmpty()) {
                dialog = new ConfirmDialog(ProgressThreadDialog.getInstance(), "error_in_copy_entry_with_cause", dialogMode, false, locationName,
                        e.getMessage());
            } else {
                dialog = new ConfirmDialog(ProgressThreadDialog.getInstance(), "error_in_copy_entry", dialogMode, false, locationName);
            }
            dialog.setVisible(true);
            result.set(dialog.getReturnOption());
        });

        if (result.get() == ConfirmDialog.YES_OPTION) {
            return executeCopyOrMoveOperation(location, target, isSingleEntryOperation, overwriteIfExists);

        } else if (result.get() == ConfirmDialog.CANCEL_OPTION) {
            LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.error_during_copying", e);
            return false;

        } else {
            // user pressed "no-retry", so log and return true
            LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.error_during_copying", e);
            return true;
        }
    }

}
