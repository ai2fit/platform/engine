package com.owc.singularity.studio.gui.vcs.model;

import java.nio.file.Path;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public abstract class AbstractMountTreeDiffDirectoryTreeNode extends AbstractMountTreeDiffTreeNode {

    protected AbstractMountTreeDiffDirectoryTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }

    @Override
    public Class<?> getType() {
        return Path.class;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }
}
