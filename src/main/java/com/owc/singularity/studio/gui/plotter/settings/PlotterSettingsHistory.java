/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.plotter.settings;


import java.util.HashMap;
import java.util.LinkedHashMap;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.tools.container.Pair;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.plotter.HeuristicPlotterConfigurator;
import com.owc.singularity.studio.gui.plotter.Plotter;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationModel;
import com.owc.singularity.studio.gui.plotter.PlotterConfigurationSettings;


/**
 * This class holds information about plotter settings in the processing history since the
 * SingularityEngine startup. They might be used for pre-initializing the plotter with settings from
 * the past processing history.
 * <p>
 * Please note that this class must NOT store all information in
 *
 * @author Sebastian Land
 */
public final class PlotterSettingsHistory {

    private static final HashMap<Pair<RepositoryPath, String>, PlotterConfigurationSettings> settingsHistory = new HashMap<>();

    public static PlotterConfigurationModel getPlotterSettingsFromHistory(IOObject object, DataTable dataTable,
            LinkedHashMap<String, Class<? extends Plotter>> plotterSelections) {
        PlotterConfigurationModel configurationModel = null;
        // first check path and operator
        String operatorName = object.getSourceOperator();
        if (operatorName != null)
            operatorName = operatorName.replaceAll("\\[.*]", "");
        PlotterConfigurationSettings settings = settingsHistory.get(new Pair<>(object.getSourcePath(), operatorName));
        if (settings == null) {
            settings = settingsHistory.get(new Pair<RepositoryPath, String>(object.getSourcePath(), null));
        }

        if (settings != null) {
            configurationModel = new PlotterConfigurationModel(settings, plotterSelections, dataTable);
        }
        // if we didn't find anything: Create new settings (with default or user specified values)
        // and add them to history
        if (configurationModel == null) {
            configurationModel = new PlotterConfigurationModel(plotterSelections, dataTable);
            HeuristicPlotterConfigurator heuristicConfigurator = new HeuristicPlotterConfigurator(configurationModel, dataTable);

            configurationModel.setParameters(heuristicConfigurator.getDefaultSelection(configurationModel.getParameterSettings()));
            configurationModel.setPlotter(heuristicConfigurator.getDefaultPlotter());

            settingsHistory.put(new Pair<>(object.getSourcePath(), operatorName), configurationModel.getPlotterSettings());
            settingsHistory.put(new Pair<>(object.getSourcePath(), null), configurationModel.getPlotterSettings());
        }
        return configurationModel;
    }
}
