package com.owc.singularity.studio.gui.tools;

import java.awt.event.ActionEvent;
import java.util.function.Consumer;

public class DefaultResourceAction extends ResourceAction {

    private static final long serialVersionUID = 2410911146581753133L;
    private Consumer<ActionEvent> action;


    /**
     * Creates a new {@link ResourceAction} with the standard {@link IconType}.
     *
     * @param i18nKey
     * @param i18nArgs
     */
    public DefaultResourceAction(Consumer<ActionEvent> action, String i18nKey, Object... i18nArgs) {
        super(false, i18nKey, i18nArgs);
        this.action = action;
    }

    /**
     * Creates a new {@link ResourceAction} with the specified {@link IconType}.
     *
     * @param i18nKey
     * @param iconType
     * @param i18nArgs
     */
    public DefaultResourceAction(Consumer<ActionEvent> action, String i18nKey, IconType iconType, Object... i18nArgs) {
        super(false, i18nKey, iconType, i18nArgs);
        this.action = action;
    }

    /**
     * Creates a new {@link ResourceAction} with the standard {@link IconType}.
     *
     * @param smallIcon
     * @param i18nKey
     * @param i18nArgs
     */
    public DefaultResourceAction(Consumer<ActionEvent> action, boolean smallIcon, String i18nKey, Object... i18nArgs) {
        super(smallIcon, i18nKey, i18nArgs);
        this.action = action;
    }

    /**
     * Creates a new {@link ResourceAction} with the specified {@link IconType}.
     *
     * @param smallIcon
     * @param i18nKey
     * @param iconType
     * @param i18nArgs
     */
    public DefaultResourceAction(Consumer<ActionEvent> action, boolean smallIcon, String i18nKey, IconType iconType, Object... i18nArgs) {
        super(smallIcon, i18nKey, iconType, i18nArgs);
        this.action = action;
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        action.accept(e);
    }
}
