package com.owc.singularity.studio.gui.repository.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.time.Instant;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.studio.gui.tools.LazySupplier;

public class RepositoryTreeNodeInformation {

    public static Class<?> UNKNOWN_CONTENT_CLASS = Void.class;
    public static long UNKNOWN_FILE_SIZE = Long.MAX_VALUE;
    private transient Boolean directory;
    private transient Boolean writable;
    private transient Class<?> contentClass;
    private transient RepositoryMount mount;
    private transient Long fileSize;
    private transient Instant lastModifiedTimestamp;
    private transient String fileName;
    private final LazySupplier<EntryVersion> version;
    private final Entry entry;

    private boolean loading;

    RepositoryTreeNodeInformation(Entry entry) {
        this.entry = entry;
        this.version = new LazySupplier<>(uncheckedSupply(() -> Entries.getVersion(getRepositoryPath())));
    }

    public void clear() {
        this.directory = null;
        this.writable = null;
        this.contentClass = null;
        this.mount = null;
        this.fileSize = null;
        this.lastModifiedTimestamp = null;
        this.version.reset();
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isDirectory() {
        if (directory == null) {
            directory = Files.isDirectory(getRepositoryPath());
        }
        return directory;
    }

    public boolean isWritable() {
        if (writable == null) {
            writable = Files.isWritable(getRepositoryPath());
        }
        return writable;
    }

    public RepositoryMount getMount() {
        if (mount == null) {
            try {
                mount = getRepositoryPath().getFileSystem().getMount(getRepositoryPath());
            } catch (NoSuchFileException e) {
                return null;
            }
        }
        return mount;
    }

    public boolean hasContentClass() {
        return contentClass != null;
    }

    public Class<?> readContentClass() {
        if (contentClass == null) {
            try {
                contentClass = getEntry().getContentClass(ModuleService.getMajorClassLoader());
            } catch (IOException | ClassNotFoundException e) {
                contentClass = UNKNOWN_CONTENT_CLASS;
            }
        }
        return contentClass;
    }

    public long getFileSize() {
        if (fileSize == null) {
            try {
                fileSize = Files.size(getRepositoryPath());
            } catch (IOException e) {
                fileSize = UNKNOWN_FILE_SIZE;
            }
        }
        return fileSize;
    }

    public Instant getLastModifiedTimestamp() {
        if (lastModifiedTimestamp == null) {
            try {
                lastModifiedTimestamp = Files.getLastModifiedTime(getRepositoryPath()).toInstant();
            } catch (Exception ignored) {
                lastModifiedTimestamp = Instant.EPOCH;
            }
        }
        return lastModifiedTimestamp;
    }

    public boolean hasVersion() {
        return version.isEvaluated();
    }

    public EntryVersion readVersion() {
        return version.get();
    }

    public RepositoryPath getRepositoryPath() {
        return entry.getPath();
    }

    public Entry getEntry() {
        return entry;
    }

    private static <V> Supplier<V> uncheckedSupply(Callable<V> callable) {
        return () -> {
            try {
                return callable.call();
            } catch (Exception e) {
                return null;
            }
        };
    }

    public String getFileName() {
        if (fileName == null) {
            fileName = getRepositoryPath().getFileName().toString();
        }
        return fileName;
    }
}
