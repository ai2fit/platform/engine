/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import javax.swing.Icon;

import com.owc.singularity.engine.pipeline.PipelineState;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Start the corresponding action.
 *
 * @author Ingo Mierswa
 */
public class RunAction extends ResourceAction {

    private static final long serialVersionUID = 1;

    private static final Icon ICON_PLAY_SMALL = SwingTools.createIcon("16/media_play.png", IconSize.SMALL);
    private static final Icon ICON_PLAY_LARGE = SwingTools.createIcon("24/media_play.png", IconSize.MEDIUM);
    private static final Icon ICON_RESUME_SMALL = SwingTools.createIcon("16/media_step_forward.png", IconSize.SMALL);
    private static final Icon ICON_RESUME_LARGE = SwingTools.createIcon("24/media_step_forward.png", IconSize.MEDIUM);

    public RunAction() {
        super("run");
        setCondition(Condition.PROCESS_RUNNING, ConditionReaction.DISALLOWED);
        setCondition(Condition.EDIT_IN_PROGRESS, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
        if (mainProcessPanel != null) {
            // small hack to force currently active parameter editor to save the changes due to
            // focus
            // lost event
            mainProcessPanel.requestFocus();
            MainFrame.INSTANCE.runProcess(true);
        }
    }

    public void setState(PipelineState processState) {
        switch (processState) {
            case PAUSED:
                putValue(LARGE_ICON_KEY, ICON_RESUME_LARGE);
                putValue(SMALL_ICON, ICON_RESUME_SMALL);
                break;
            default:
                putValue(LARGE_ICON_KEY, ICON_PLAY_LARGE);
                putValue(SMALL_ICON, ICON_PLAY_SMALL);

        }
    }

}
