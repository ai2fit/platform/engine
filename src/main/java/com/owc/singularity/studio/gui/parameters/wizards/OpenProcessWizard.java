package com.owc.singularity.studio.gui.parameters.wizards;


import java.util.Map;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.wizards.AbstractConfigurationWizardCreator;
import com.owc.singularity.studio.gui.wizards.ConfigurationListener;

public class OpenProcessWizard extends AbstractConfigurationWizardCreator {

    public static final String PIPELINE_PARAMETER = "pipeline_parameter";
    private static final long serialVersionUID = 4774333804247004553L;
    private String pipelineParameter;


    @Override
    public String getI18NKey() {
        return "open_pipeline";
    }

    @Override
    public void setParameters(Map<String, String> parameters) {
        pipelineParameter = parameters.get(PIPELINE_PARAMETER);
        super.setParameters(parameters);
    }

    @Override
    public void createConfigurationWizard(ParameterType type, ConfigurationListener listener) {
        try {
            RepositoryPath processLocation = ((Operator) listener).getParameterAsRepositoryPath(pipelineParameter);
            // opening linked process
            OpenEntryAction.open(processLocation.toAbsolutePath().toString());
        } catch (UserError e) {
        }
    }
}
