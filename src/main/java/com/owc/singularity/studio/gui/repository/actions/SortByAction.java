/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */

package com.owc.singularity.studio.gui.repository.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.gui.actions.ToggleAction;
import com.owc.singularity.studio.gui.repository.RepositorySortingMethod;
import com.owc.singularity.studio.gui.repository.RepositoryTree;


/**
 * Abstract action for sorting by a {@link RepositorySortingMethod} in the repository browser.
 *
 * @author Marcel Seifert
 * @since 7.4
 *
 */
public abstract class SortByAction extends ToggleAction {

    private static final long serialVersionUID = 1L;

    private transient final RepositoryTree tree;
    private final RepositorySortingMethod method;

    /**
     * Creates a new SortByAction
     *
     * @param i18n
     *            the i18n key
     * @param tree
     *            the repository tree on which the sorting method is applied
     * @param method
     *            the sorting method represented by this action
     */
    public SortByAction(String i18n, RepositoryTree tree, RepositorySortingMethod method) {
        super(true, i18n);
        this.tree = tree;
        this.method = method;
        updateSelectedStatus(tree.getSortingMethod());
    }

    public RepositorySortingMethod getMethod() {
        return method;
    }

    /**
     * Updates the selected status
     *
     * @param selectedMethod
     *            the currently selected method
     */
    private void updateSelectedStatus(RepositorySortingMethod selectedMethod) {
        setSelected(selectedMethod.equals(getMethod()));
    }

    @Override
    public void actionToggled(ActionEvent e) {
        tree.setSortingMethod(method);
    }

}
