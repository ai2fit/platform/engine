/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;
import javax.swing.Timer;

import com.owc.singularity.engine.object.data.exampleset.DirectMemoryStatistics;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.look.Colors;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * The default system monitor which graphically displays the amount of used memory. Please note that
 * the upper line of the system monitor shows the amount of currently reserved memory (total).
 * Unfortunately it is (currently) not possible to ask for the processor usage with methods from
 * Java.
 *
 * @author Ingo Mierswa, Simon Fischer
 */
public class SystemMonitor extends JPanel implements Dockable {

    private static final long serialVersionUID = 1L;

    private static final Color BACKGROUND = Colors.PANEL_BACKGROUND;

    private static final Color TEXT_COLOR = Colors.TEXT_FOREGROUND;

    private static final Color OFF_HEAP_MEMORY_COLOR = new Color(118, 200, 243, 200);
    private static final Color ON_HEAP_MEMORY_COLOR = new Color(240, 240, 165, 200);

    private static final Color GRID_COLOR = Color.LIGHT_GRAY;

    private static final Color LINE_COLOR = SwingTools.VERY_DARK_BLUE;

    private static final int NUMBER_OF_MEASUREMENTS = 128;

    private static final int GRID_X = 10;

    private static final int GRID_Y = 10;

    private static final int MARGIN = 10;

    private static final int DELAY = 500;

    public static final String SYSTEM_MONITOR_DOCK_KEY = "system_monitor";
    private final DockKey DOCK_KEY = new ResourceDockKey(SYSTEM_MONITOR_DOCK_KEY);

    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    private final long[] onHeapMemory = new long[NUMBER_OF_MEASUREMENTS];
    private final long[] offHeapMemory = new long[NUMBER_OF_MEASUREMENTS];
    private long maxOffHeapMemory = DirectMemoryStatistics.getTargetSizeOfDirectMemory();
    private int currentMeasurement = 0;

    private long currentlyUsedOnHeapMemory = 0;
    private long currentlyUsedOffHeapMemory = 0;

    private Color backgroundColor = BACKGROUND;
    private Color textColor = TEXT_COLOR;
    private Color gridColor = GRID_COLOR;
    private Color lineColor = LINE_COLOR;

    private Font font;

    public SystemMonitor() {
        setBackground(backgroundColor);

        // prevent VLDocking popup menu by eating events
        addMouseListener(new MouseAdapter() {});
    }

    public void startMonitorThread() {
        new Timer(DELAY, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!SystemMonitor.this.isShowing()) {
                    return;
                }
                // memory
                SystemMonitor.this.currentlyUsedOnHeapMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                SystemMonitor.this.currentlyUsedOffHeapMemory = DirectMemoryStatistics.getSizeOfUsedMemory();
                SystemMonitor.this.maxOffHeapMemory = Math.max(DirectMemoryStatistics.getTargetSizeOfDirectMemory(),
                        SystemMonitor.this.currentlyUsedOffHeapMemory);
                SystemMonitor.this.onHeapMemory[SystemMonitor.this.currentMeasurement] = currentlyUsedOnHeapMemory;
                SystemMonitor.this.offHeapMemory[SystemMonitor.this.currentMeasurement] = currentlyUsedOffHeapMemory;

                SystemMonitor.this.currentMeasurement = (SystemMonitor.this.currentMeasurement + 1) % SystemMonitor.this.onHeapMemory.length;
                SwingTools.invokeAndWait(() -> SystemMonitor.this.repaint());
            }
        }).start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);


        Dimension d = getSize();
        int monitorWidth = (int) d.getWidth() - 2 * MARGIN;
        int monitorHeight = (int) d.getHeight() - 2 * MARGIN;

        long maxOnHeapMemory = Runtime.getRuntime().maxMemory();

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (this.font == null) {
            this.font = g2d.getFont().deriveFont(12f);
        }

        // background
        g2d.setColor(Colors.WHITE);
        g2d.fillRect(MARGIN, MARGIN, monitorWidth, monitorHeight);


        // off heap memory
        GeneralPath offHeapPath = new GeneralPath();
        offHeapPath.moveTo(MARGIN, MARGIN + monitorHeight);
        for (int i = 0; i < offHeapMemory.length; i++) {
            int index = (currentMeasurement + i) % offHeapMemory.length;
            offHeapPath.lineTo(MARGIN + i * monitorWidth / (offHeapMemory.length - 1),
                    MARGIN + monitorHeight - monitorHeight * offHeapMemory[index] / maxOffHeapMemory);
        }
        offHeapPath.lineTo(MARGIN + monitorWidth, MARGIN + monitorHeight);
        offHeapPath.closePath();


        g2d.setColor(OFF_HEAP_MEMORY_COLOR);
        g2d.fill(offHeapPath);
        g2d.setColor(lineColor);
        g2d.draw(offHeapPath);

        // on heap memory
        GeneralPath onHeapPath = new GeneralPath();
        onHeapPath.moveTo(MARGIN, MARGIN + monitorHeight);
        for (int i = 0; i < onHeapMemory.length; i++) {
            int index = (currentMeasurement + i) % onHeapMemory.length;
            onHeapPath.lineTo(MARGIN + i * monitorWidth / (onHeapMemory.length - 1),
                    MARGIN + monitorHeight - monitorHeight * onHeapMemory[index] / maxOnHeapMemory);
        }
        onHeapPath.lineTo(MARGIN + monitorWidth, MARGIN + monitorHeight);
        onHeapPath.closePath();


        g2d.setColor(ON_HEAP_MEMORY_COLOR);
        g2d.fill(onHeapPath);
        g2d.setColor(lineColor);
        g2d.draw(onHeapPath);

        // grid
        g2d.setColor(gridColor);
        for (int x = 0; x < 1 + 1; x++) {
            g2d.drawLine(MARGIN + x * monitorWidth / 1, MARGIN, MARGIN + x * monitorWidth / 1, MARGIN + monitorHeight);
        }
        for (int y = 0; y < 3; y++) {
            g2d.drawLine(MARGIN, MARGIN + y * monitorHeight / 3, MARGIN + monitorWidth, MARGIN + y * monitorHeight / 3);
        }
        // text
        String maxHeapString = Tools.formatSizeInBytes(maxOnHeapMemory) + " max internal memory";
        String maxOffHeapString = Tools.formatSizeInBytes(maxOffHeapMemory) + " external memory";
        int totalHeight = 2 * font.getSize() + 2 * MARGIN;
        if (totalHeight < getHeight()) {
            g2d.setFont(font);
            g2d.setColor(ON_HEAP_MEMORY_COLOR);
            g2d.drawString(maxHeapString, 2 * MARGIN, 3 * MARGIN);
            g2d.setColor(OFF_HEAP_MEMORY_COLOR);
            g2d.drawString(maxOffHeapString, getWidth() - 2 * MARGIN - g2d.getFontMetrics().stringWidth(maxOffHeapString), 3 * MARGIN);

            g2d.setColor(Color.black);
            g2d.drawString(Tools.formatSizeInBytes(currentlyUsedOnHeapMemory), 2 * MARGIN, getHeight() - font.getSize() - 2 * MARGIN);
            String offHeapString = Tools.formatSizeInBytes(currentlyUsedOffHeapMemory);
            g2d.drawString(offHeapString, getWidth() - 2 * MARGIN - g2d.getFontMetrics().stringWidth(offHeapString), getHeight() - font.getSize() - 2 * MARGIN);

        }
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }
}
