/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.view.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.action.ProcessChangingAction;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;


/**
 * Automatically arranges all {@link Operator}s.
 *
 * @author Simon Fischer
 * @since 6.4.0
 *
 */
public class ArrangeOperatorsAction extends ProcessChangingAction {

    private static final long serialVersionUID = 1L;

    public ArrangeOperatorsAction() {
        super(true, "arrange_operators");
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(final ActionEvent e) {
        ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
        if (mainProcessPanel == null)
            return;
        mainProcessPanel.getProcessEditor().autoArrange();
    }
}
