package com.owc.singularity.studio.gui.repository.event;

import java.util.function.Consumer;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;

/**
 * A trigger watches for a set of events to happen before firing an action.
 *
 * @author Hatem Hamad
 * @see RepositoryTree#invokeAfterTreeEvent(RepositoryTreeEventTrigger, Consumer)
 */
public interface RepositoryTreeEventTrigger {

    /**
     * Check the tree event Whether to fire the trigger
     *
     * @param event
     *            the latest event on the {@link RepositoryTree}
     * @param affectedPaths
     *            the paths affected by that event
     * @return true if the trigger should fire, false otherwise
     */
    boolean shouldFire(RepositoryTreeUpdateType event, RepositoryPath... affectedPaths);

    /**
     * Whether the trigger should be removed from the list of triggers watching for events in the
     * tree.
     * 
     * @return true if the trigger should be removed, false otherwise
     */
    boolean shouldBeRemoved();

    default void reset() {}

    /**
     * Mark the trigger for removal
     * 
     * @see #shouldBeRemoved()
     */
    void remove();
}
