/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa
 */
public class RedoAction extends ResourceAction {

    private static final long serialVersionUID = -3106523347321697652L;


    public RedoAction() {
        super("redo");
        setCondition(Condition.EDIT_IN_PROGRESS, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RENDERER_HAS_REDO_STEPS, ConditionReaction.MANDATORY);
    }

    /**
     * @deprecated use {@link #RedoAction()} instead
     */
    @Deprecated
    public RedoAction(MainFrame mainFrame) {
        this();
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().redo();
    }
}
