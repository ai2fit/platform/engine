/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.List;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * This action copies from the selected location.
 * 
 * @author Simon Fischer
 */
public class CopyLocationAction extends AbstractRepositoryContextAction<Object> {

    private static final long serialVersionUID = 1L;

    public CopyLocationAction(RepositoryTree tree) {
        super(tree, true, true, null, false, false, "repository_copy_location");
    }

    @Override
    protected Object configureAction(List<RepositoryTreeNode> entries) {
        String value = entries.get(0).getRepositoryPath().toString();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection(value), new ClipboardOwner() {

            @Override
            public void lostOwnership(Clipboard clipboard, Transferable contents) {}
        });
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, Object config, ProgressListener progressListener) {
        // All done in AWTEventQueue

    }

}
