/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools;


import java.awt.*;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * Creates a panel which contains a {@link JTextField} and a {@link JLabel} which can be used to
 * enter the name of a repository entry. Invalid names (checked via to signal invalid/valid entries.
 * <p>
 * To listen to the event (firing a simple {@link Boolean} to signal valid (<code>true</code>) and
 * invalid (<code>false</code>) entries), register your listener via
 * {@link #addObserver(Observer, boolean)} or {@link #addObserverAsFirst(Observer, boolean)}.
 * 
 * @author Marco Boeck
 */
public class ValidatableTextField extends JPanel implements Observable<Boolean> {

    private static final long serialVersionUID = -750857028654448541L;

    private JLabel entryTextLabel;
    private JTextField entryTextField;

    private JLabel entryErrorIconLabel;
    private JLabel entryErrorTextLabel;

    private final Color standardTextColor;
    private final Color errorTextColor;

    private final Icon standardIcon;
    private final Icon errorIcon;

    private final List<Observer<Boolean>> observerOnEDTList;
    private final List<Observer<Boolean>> observerNotOnEDTList;

    private final Object lock = new Object();

    private final Predicate<String> validationLogic;
    private final Function<String, String> validationMessageProvider;

    /**
     * Standard constructor.
     */
    public ValidatableTextField(Predicate<String> validationLogic, Function<String, String> validationMessageProvider, String i18nKey) {
        super();
        this.validationLogic = validationLogic;
        this.validationMessageProvider = validationMessageProvider;

        standardIcon = null;
        errorIcon = SwingTools.createIcon("16/" + I18N.getGUIMessage("gui.dialog.invalid_input.icon"));

        setupGUI(i18nKey);

        standardTextColor = entryTextField.getForeground();
        errorTextColor = Color.RED;

        observerOnEDTList = new LinkedList<>();
        observerNotOnEDTList = new LinkedList<>();

        validateTextField();
    }

    /**
     * Sets up this GUI element.
     */
    private void setupGUI(String i18nKey) {
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(5, 0, 5, 5);
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        entryTextField = new JTextField();
        entryTextLabel = new ResourceLabel(i18nKey);
        entryTextLabel.setLabelFor(entryTextField);
        add(entryTextLabel, gbc);

        gbc.gridx = 1;
        gbc.weightx = 1.0;
        gbc.insets = new Insets(5, 0, 5, 0);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 2;
        entryTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                validateTextField();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                validateTextField();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not relevant
            }
        });
        add(entryTextField, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        gbc.insets = new Insets(5, 0, 5, 5);
        entryErrorIconLabel = new JLabel();
        entryErrorIconLabel.setMinimumSize(new Dimension(16, 16));
        entryErrorIconLabel.setPreferredSize(new Dimension(16, 16));
        add(entryErrorIconLabel, gbc);

        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.gridwidth = 2;
        gbc.insets = new Insets(5, 0, 5, 0);
        entryErrorTextLabel = new JLabel();
        add(entryErrorTextLabel, gbc);
    }

    /**
     * Checks if the user text input is valid against the predefined logic
     */
    private void validateTextField() {
        boolean valid = false;
        String userInput = null;
        try {
            userInput = entryTextField.getDocument().getText(0, entryTextField.getDocument().getLength());
            valid = validationLogic.test(userInput);
        } catch (Exception e1) {
            LogService.getRoot().error("com.owc.singularity.studio.gui.tools.ValidatableTextField.bad_field_creation", e1);
        }
        if (valid) {
            entryTextField.setForeground(standardTextColor);
            entryErrorIconLabel.setIcon(standardIcon);
            entryErrorTextLabel.setText("");
        } else {
            entryTextField.setForeground(errorTextColor);
            entryErrorIconLabel.setIcon(errorIcon);
            entryErrorTextLabel.setText(validationMessageProvider.apply(userInput));
        }
        notifyObservers(valid);
    }

    /**
     * Notifies the observers.
     * 
     * @param valid
     */
    private void notifyObservers(final boolean valid) {
        synchronized (lock) {
            if (SwingUtilities.isEventDispatchThread()) {
                for (final Observer<Boolean> observer : observerOnEDTList) {
                    observer.update(ValidatableTextField.this, valid);
                }
            } else {
                for (final Observer<Boolean> observer : observerOnEDTList) {
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            observer.update(ValidatableTextField.this, valid);
                        }

                    });
                }
            }

            for (final Observer<Boolean> observer : observerNotOnEDTList) {
                observer.update(ValidatableTextField.this, valid);
            }
        }
    }

    @Override
    public void addObserver(Observer<Boolean> observer, boolean onEDT) {
        if (onEDT) {
            synchronized (lock) {
                observerOnEDTList.add(observer);
            }
        } else {
            synchronized (lock) {
                observerNotOnEDTList.add(observer);
            }
        }
    }

    @Override
    public void removeObserver(Observer<Boolean> observer) {
        synchronized (lock) {
            if (!observerOnEDTList.remove(observer)) {
                observerNotOnEDTList.remove(observer);
            }
        }
    }

    @Override
    public void addObserverAsFirst(Observer<Boolean> observer, boolean onEDT) {
        if (onEDT) {
            synchronized (lock) {
                observerOnEDTList.add(0, observer);
            }
        } else {
            synchronized (lock) {
                observerNotOnEDTList.add(0, observer);
            }
        }
    }

    /**
     * Sets a text in the textfield and validates it.
     * 
     * @param text
     */
    public void setText(String text) {
        entryTextField.setText(text);
        validateTextField();
    }

    /**
     * Return the entered text.
     * 
     * @return
     */
    public String getText() {
        return entryTextField.getText();
    }

    @Override
    public void addKeyListener(KeyListener l) {
        this.entryTextField.addKeyListener(l);
    }

    @Override
    public void removeKeyListener(KeyListener l) {
        this.entryTextField.removeKeyListener(l);
    }

    @Override
    public boolean requestFocusInWindow() {
        return entryTextField.requestFocusInWindow();
    }
}
