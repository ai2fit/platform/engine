/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.mount;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

import javax.swing.*;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.repository.RepositoryFileSystemProvider;
import com.owc.singularity.repository.RepositoryMountProvider;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.Form;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.components.FixedWidthLabel;
import com.owc.singularity.studio.gui.tools.components.composite.ToggleButtonGroup;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * Dialog to configure a new repository mount.
 *
 * @author Hatem Hamad
 *
 */
public class CreateRepositoryMountDialog extends ButtonDialog {

    private static final long serialVersionUID = 1L;

    private final FixedWidthLabel validationResult;
    private final JButton saveButton;
    private final JLabel mountDescriptionLabel;
    private final JPanel main;

    private RepositoryMountProvider mountProvider;
    private Form<Map<String, String>> createForm;

    public CreateRepositoryMountDialog(RepositoryPath path) {
        super(ApplicationFrame.getApplicationFrame(), "add_mount_dialog", ModalityType.APPLICATION_MODAL, new Object[0]);
        Collection<RepositoryMountProvider> mountProviders = RepositoryFileSystemProvider.getMountProviders();

        validationResult = new FixedWidthLabel(300, "");

        List<AbstractAction> providerActions = new ArrayList<>();
        for (RepositoryMountProvider provider : mountProviders) {
            if (!RepositoryUIManager.supportsCreate(provider.getTypeId())) {
                // skip providers that do not provide a create form, e.g. PlaceholderRepositoryMount
                continue;
            }
            providerActions.add(ResourceAction.of("repository.mount." + provider.getTypeId(), e -> updateForm(provider)));
        }

        List<AbstractButton> buttons = new LinkedList<>();
        buttons.add(new JButton(ResourceAction.of("repository_configuration_dialog.check", e -> validateOptionsAndDisplayMessages())));

        saveButton = makeOkButton("repository_configuration_dialog.save");
        buttons.add(makeCancelButton());

        // info panel
        JPanel infoPanel = new JPanel(new GridBagLayout());
        ToggleButtonGroup toggleButton = new ToggleButtonGroup(providerActions.toArray(AbstractAction[]::new));
        mountDescriptionLabel = new JLabel();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 5, 5, 20);
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        infoPanel.add(toggleButton, gbc);

        gbc.weightx = 0;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridy += 1;
        gbc.insets = new Insets(10, 5, 5, 20);
        infoPanel.add(mountDescriptionLabel, gbc);
        main = new JPanel(new BorderLayout(0, 20));
        main.add(infoPanel, BorderLayout.NORTH);
        main.add(validationResult, BorderLayout.SOUTH);
        buttons.add(saveButton);

        layoutDefault(main, NORMAL, buttons);
        // select default option
        AbstractAction defaultAction = providerActions.get(0);
        toggleButton.setSelected(defaultAction);
        SwingUtilities.invokeLater(() -> defaultAction.actionPerformed(new ActionEvent(this, 0, null)));
    }

    private void updateForm(RepositoryMountProvider provider) {
        mountProvider = provider;
        if (createForm != null)
            main.remove(createForm.getComponent());
        String typeId = provider.getTypeId();
        mountDescriptionLabel.setText(I18N.getGUILabel("repository.mount." + typeId));
        createForm = RepositoryUIManager.getMountCreateForm(typeId);
        decideSaveButtonState();
        createForm.addInputChangeListener((inputKey, inputValue) -> decideSaveButtonState());
        main.add(createForm.getComponent(), BorderLayout.CENTER);
    }

    private void validateOptionsAndDisplayMessages() {
        Map<String, String> validationMessages = null;
        Map<String, String> options = null;
        try {
            options = createForm.getData();
        } catch (IllegalStateException e) {
            validationMessages = Map.of("IllegalStateException", e.getMessage());
        }
        if (options != null) {
            validationResult.setText(null);
            validationResult.setForeground(Colors.PANEL_BACKGROUND);
            validationMessages = mountProvider.validate(options);
        }
        // check if there is any validation error message
        if (validationMessages != null && validationMessages.values().stream().anyMatch(Objects::nonNull)) {
            validationResult.setForeground(Color.RED);
            StringBuilder errorMessage = new StringBuilder("Invalid options:<br/>");
            for (Map.Entry<String, String> messageEntry : validationMessages.entrySet()) {
                if (messageEntry.getValue() == null)
                    continue;
                errorMessage.append(messageEntry.getKey())
                        .append(": ")
                        .append(Objects.requireNonNullElse(I18N.getErrorMessageOrNull("repository.mount.validation.error." + messageEntry.getValue(),
                                options != null ? options.get(messageEntry.getKey()) : "IllegalStateException"), messageEntry.getValue()))
                        .append("<br/>");
            }
            validationResult.setText(errorMessage.toString());
        } else {
            validationResult.setText("Options are valid");
            validationResult.setForeground(Colors.SUCCESS);
        }
    }

    private void decideSaveButtonState() {
        saveButton.setEnabled(createForm.isReadyToSubmit());
    }

    public Map<String, String> getOptions() {
        return createForm.getData();
    }

    public String getMountType() {
        return mountProvider.getTypeId();
    }

}
