package com.owc.singularity.studio.gui.parameters;

import java.awt.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.studio.gui.parameters.celleditors.value.PropertyValueCellEditor;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.SwingTools;

public class WizardParameterPanel extends GenericParameterPanel {

    // combining parameters requires adequate space, meaning only boolean + another parameter type
    // is currently viable
    private final Map<String, String> combinedParameterMap;

    public WizardParameterPanel(Parameters parameters, Map<String, String> combinedParameterMap) {
        this.combinedParameterMap = combinedParameterMap;
        setParameters(parameters);
    }

    @Override
    protected JPanel createParameterPanel(ParameterType type, PropertyValueCellEditor editor, Component editorComponent) {

        JPanel parameterPanel = null;
        if (editor.rendersLabel()) {
            parameterPanel = new JPanel(new BorderLayout());
            parameterPanel.setOpaque(isOpaque());
            parameterPanel.setBackground(getBackground());
            parameterPanel.setPreferredSize(new Dimension((int) parameterPanel.getPreferredSize().getWidth(), 25));
            parameterPanel.add(editorComponent, editorComponent instanceof JCheckBox ? BorderLayout.WEST : BorderLayout.CENTER);
        } else {
            parameterPanel = new JPanel(new GridLayout(1, 2));
            parameterPanel.setOpaque(isOpaque());
            parameterPanel.setBackground(getBackground());
            parameterPanel.setPreferredSize(new Dimension((int) parameterPanel.getPreferredSize().getWidth(), 25));
            final JLabel label = new JLabel(type.getKey().replace('_', ' ') + " ");
            label.setOpaque(isOpaque());
            label.setFont(getFont());
            label.setBackground(getBackground());
            int style = Font.PLAIN;
            if (!type.isOptional()) {
                style |= Font.BOLD;
            }
            if (false) {
                style |= Font.ITALIC;
            }
            label.setFont(label.getFont().deriveFont(style));
            label.setLabelFor(editorComponent);
            if (!isEnabled()) {
                SwingTools.setEnabledRecursive(label, false);
            }

            parameterPanel.add(label);
            parameterPanel.add(editorComponent);
        }

        JPanel surroundingPanel = new JPanel(new BorderLayout());
        surroundingPanel.add(parameterPanel, BorderLayout.CENTER);

        return surroundingPanel;
    }

    @Override
    public void setupComponents() {
        SwingTools.invokeLater(this::setupComponentsNow);
    }

    protected void setupComponentsNow() {
        removeAll();
        currentEditors.clear();
        editorKeyToParameterPanelMap.clear();
        /** Types currently displayed by editors. */
        Collection<ParameterType> currentTypes = getParameterTypes();
        if (currentTypes == null) {
            revalidate();
            repaint();
            return;
        }
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.PAGE_START;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(5, 10, 5, 10);

        int column = 0;
        int row = 0;
        Set<ParameterType> alreadyDrawnTypes = new HashSet<>();
        for (final ParameterType type : currentTypes) {
            if (!alreadyDrawnTypes.contains(type)) {
                instantiateParameterPanel(type, column, row, c);
                alreadyDrawnTypes.add(type);
                // combining parameters requires adequate space, meaning only boolean + another
                // parameter type is currently viable
                if (combinedParameterMap.containsKey(type.getKey())) {
                    String combinedWithKey = combinedParameterMap.get(type.getKey());
                    List<ParameterType> combinedWithType = currentTypes.stream()
                            .filter(parameterType -> parameterType.getKey().equals(combinedWithKey))
                            .toList();
                    if (!combinedWithType.isEmpty()) {
                        instantiateParameterPanel(combinedWithType.get(0), column, row, c);
                        alreadyDrawnTypes.add(combinedWithType.get(0));
                    }
                }
                row++;
                column++;
                if (column > 2)
                    column = 0;
            }
        }

        // label for no parameters case
        if (row == 0) {
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(10, 10, 10, 10);
            c.anchor = GridBagConstraints.CENTER;
            c.weightx = 1;
            c.fill = GridBagConstraints.HORIZONTAL;
            JLabel noParametersLabel = new ResourceLabel("propertyPanel.no_parameters");
            noParametersLabel.setHorizontalAlignment(SwingConstants.CENTER);
            add(noParametersLabel, c);
            row++;
        }

        c.gridx = 0;
        c.gridy = row;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;

        // Push panel contents to top
        JLabel dummyLabel = new JLabel();
        dummyLabel.setOpaque(false);
        layout.setConstraints(dummyLabel, c);
        add(dummyLabel);

        revalidate();
        repaint();
    }

    private void instantiateParameterPanel(ParameterType type, int column, int row, GridBagConstraints c) {
        final PropertyValueCellEditor editor = instantiateValueCellEditor(type);
        currentEditors.put(type.getKey(), editor);
        String value = type.fromRawString(getRawValue(type));
        if (value == null) {
            value = type.getDefaultValueAsRawString();
        }

        Component editorComponent = editor.getTableCellEditorComponent(null, value, false, row, 1);
        if (!isEnabled()) {
            SwingTools.setEnabledRecursive(editorComponent, false);
        }

        final Operator typesOperator = getOperator();
        editor.addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingCanceled(ChangeEvent e) {}

            @Override
            public void editingStopped(ChangeEvent e) {
                Object valueObj = editor.getCellEditorValue();
                String value = type.toRawString(valueObj);
                String last = getRawValue(typesOperator, type);
                // Second check prevents an endless validation loop in case valueObj and last
                // are both null
                if (!Objects.equals(value, last) && valueObj != last) {
                    setRawValue(typesOperator, type, value, false);
                }
            }
        });

        c.gridx = column;
        c.gridy = row / 3;
        c.weightx = 1;
        c.weighty = 0;
        JPanel parameterPanel = createParameterPanel(type, editor, editorComponent);
        editorKeyToParameterPanelMap.put(type.getKey(), parameterPanel);
        add(parameterPanel, c);
    }

}
