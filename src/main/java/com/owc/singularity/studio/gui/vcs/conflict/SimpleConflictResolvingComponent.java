package com.owc.singularity.studio.gui.vcs.conflict;

import javax.swing.*;
import javax.swing.event.EventListenerList;

import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;
import com.owc.singularity.studio.gui.vcs.conflict.model.SimpleMergeConflictDecision;

public class SimpleConflictResolvingComponent implements ConflictResolvingComponent {

    private final MergeConflict conflict;
    private final String i18NDialogKey;
    private final Object[] arguments;
    private final EventListenerList eventListenerList;
    private MergeConflictDecision decision;
    private ConfirmDialog dialog;


    public SimpleConflictResolvingComponent(MergeConflict conflict) {
        this(conflict, "vcs.merge.simple_merge", conflict.getConflictRepositoryPath().getFileName().toString(),
                conflict.getConflictRepositoryPath().toString(false));
    }

    protected SimpleConflictResolvingComponent(MergeConflict conflict, String i18NDialogKey, Object... arguments) {
        this.conflict = conflict;
        this.i18NDialogKey = i18NDialogKey;
        this.arguments = arguments;
        this.eventListenerList = new EventListenerList();
    }

    @Override
    public void show() {
        dialog = new ConfirmDialog(MainFrame.INSTANCE, i18NDialogKey, ConfirmDialog.YES_NO_CANCEL_OPTION, false, null, null, "vcs.merge.use_our_version",
                "vcs.merge.use_their_version", arguments);
        dialog.setVisible(true);
        switch (dialog.getReturnOption()) {
            case ConfirmDialog.YES_OPTION -> resolve(true);
            case ConfirmDialog.NO_OPTION -> resolve(false);
        }
    }

    @Override
    public void dispose() {
        if (dialog != null) {
            dialog.dispose();
        }
    }

    @Override
    public void addResolveListener(ConflictResolvedEventListener listener) {
        eventListenerList.add(ConflictResolvedEventListener.class, listener);
    }

    @Override
    public void removeResolveListener(ConflictResolvedEventListener listener) {
        eventListenerList.remove(ConflictResolvedEventListener.class, listener);
    }

    private void resolve(boolean ours) {
        setDecision(new SimpleMergeConflictDecision(conflict, ours));
    }

    @Override
    public boolean isResolved() {
        return decision != null;
    }

    @Override
    public MergeConflict getConflict() {
        return conflict;
    }

    @Override
    public void setDecision(MergeConflictDecision decision) {
        this.decision = decision;
        fireResolved();
    }

    @Override
    public MergeConflictDecision getDecision() {
        return decision;
    }

    private void fireResolved() {
        ConflictResolvedEventListener[] listeners = eventListenerList.getListeners(ConflictResolvedEventListener.class);
        for (ConflictResolvedEventListener listener : listeners) {
            listener.onResolve(this);
        }
    }

}
