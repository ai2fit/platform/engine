/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.event.OnCreateRepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This action creates a new folder. And is only selectable on directories.
 * 
 * @author Simon Fischer
 */
public class CreateDirectoryAction extends AbstractRepositoryContextAction<String> {

    private static final long serialVersionUID = 1L;

    public CreateDirectoryAction(RepositoryTree tree) {
        super(tree, true, false, null, false, true, "repository_create_folder");
    }

    @Override
    protected String configureAction(List<RepositoryTreeNode> entries) {
        return SwingTools.showRepositoryEntryInputDialog("repository.new_folder", "");
    }

    @Override
    public void executeAction(RepositoryPath path, String name, ProgressListener progressListener) {
        try {
            RepositoryPath directoryToCreate = path.resolve(name);
            tree.invokeAfterTreeEvent(new OnCreateRepositoryTreeEventTrigger(directoryToCreate), ignored -> tree.expandAndSelectIfExists(directoryToCreate));
            Files.createDirectory(directoryToCreate);
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("cannot_create_folder_with_reason", e, name, e.getMessage());
        } catch (Exception e) {
            SwingTools.showSimpleErrorMessage("cannot_create_folder", e, name);
        }
    }

}
