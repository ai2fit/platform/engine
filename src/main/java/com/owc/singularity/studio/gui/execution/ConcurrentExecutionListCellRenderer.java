package com.owc.singularity.studio.gui.execution;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.concurrency.tools.ConcurrentExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentOperatorExecution.OperatorBackgroundTask;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecutionState;
import com.owc.singularity.engine.concurrency.tools.ProcessExecutionStackEntry;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.studio.gui.IOObjectGUIService;
import com.owc.singularity.studio.gui.tools.SwingTools;

public class ConcurrentExecutionListCellRenderer extends JLabel implements ListCellRenderer<Object> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final Icon PROCESS_PENDING_ICON = SwingTools.createIcon("16/alarmclock.png");
    private static final Icon PROCESS_RUNNING_ICON = SwingTools.createIcon("16/media_play.png");
    private static final Icon PROCESS_STOPPED_ICON = SwingTools.createIcon("16/media_stop.png");
    private static final Icon PROCESS_FAILED_ICON = SwingTools.createIcon("16/sign_warning.png");
    private static final Icon PROCESS_DONE_ICON = SwingTools.createIcon("16/check.png");

    private static final Icon OPERATOR_RUNNING_ICON = SwingTools.createIcon("16/element_refresh.png");
    private static final Icon OPERATOR_TASK_RUNNING_ICON = SwingTools.createIcon("16/checkbox_unchecked.png");
    private static final Icon OPERATOR_TASK_QUEUED_ICON = SwingTools.createIcon("16/checkbox.png");

    private static final Icon OPERATOR_ICON = SwingTools.createIcon("16/element_selection.png");
    private static final Icon RESULT_ICON = SwingTools.createIcon("16/plug_lan.png");
    private static final Icon TABLE_ICON = SwingTools.createIcon("16/table.png");

    @Override
    public Component getListCellRendererComponent(final JList<? extends Object> list, final Object value, int index, boolean isSelected, boolean hasFocus) {

        if (value instanceof ConcurrentExecution && !(value instanceof ConcurrentPipelineExecution)) {
            ConcurrentExecution execution = (ConcurrentExecution) value;

            setText(execution.getName());
            setIcon(OPERATOR_RUNNING_ICON);
        } else if (value instanceof OperatorBackgroundTask) {
            OperatorBackgroundTask task = (OperatorBackgroundTask) value;

            setText("Subtask " + task.applyCount);
            if (task.getState().isStarted()) {
                setIcon(OPERATOR_TASK_RUNNING_ICON);
            } else {
                setIcon(OPERATOR_TASK_QUEUED_ICON);
            }
        } else if (value instanceof ConcurrentPipelineExecution) {
            ConcurrentPipelineExecution execution = (ConcurrentPipelineExecution) value;

            setText(execution.getName());

            ConcurrentPipelineExecutionState state = execution.getBackgroundExecutionState();
            if (state.getException() != null) {
                setIcon(PROCESS_FAILED_ICON);
            } else if (state.isStarted()) {
                if (state.isStopped()) {
                    setIcon(PROCESS_STOPPED_ICON);
                } else if (!state.isEnded()) {
                    setIcon(PROCESS_RUNNING_ICON);
                } else if (state.isEnded()) {
                    setIcon(PROCESS_DONE_ICON);
                }
            } else {
                setIcon(PROCESS_PENDING_ICON);
            }
        } else if (value instanceof ProcessExecutionStackEntry) {
            ProcessExecutionStackEntry stackEntry = (ProcessExecutionStackEntry) value;
            setText(stackEntry.getOperatorName() + " [" + stackEntry.getOperatorApplyCount() + ", " + stackEntry.getRuntimeAsString() + "]");
            setIcon(OPERATOR_ICON);
        } else if (value instanceof DataTable) {
            DataTable table = (DataTable) value;
            StringBuilder builder = new StringBuilder();
            builder.append(table.getName());
            builder.append(" (");
            builder.append(table.getNumberOfRows());
            builder.append("x");
            builder.append(table.getNumberOfColumns());
            builder.append(")");
            setText(builder.toString());

            setIcon(TABLE_ICON);
        } else if (value instanceof IOObject) {
            IOObject object = (IOObject) value;
            StringBuilder builder = new StringBuilder();
            builder.append(IOObjectService.getName(value.getClass()));

            if (object.getSource() != null) {
                builder.append(" (");
                builder.append(object.getSource());
                builder.append(")");
            }
            setText(builder.toString());

            Icon icon = IOObjectGUIService.getIcon(object.getClass());
            if (icon != null) {
                setIcon(icon);
            } else {
                setIcon(RESULT_ICON);
            }

        } else if (value instanceof Throwable) {
            Throwable exception = (Throwable) value;
            setText(exception.getLocalizedMessage());
            setIcon(PROCESS_FAILED_ICON);
        }

        // making the selection visible

        if (isSelected) {
            setOpaque(true);
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setOpaque(false);
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }

}
