package com.owc.singularity.studio.gui.repository.event;

public abstract class TimeoutRepositoryTreeEventTrigger implements RepositoryTreeEventTrigger {

    private final long timeoutMillis;
    private long registeredAt = System.currentTimeMillis();
    private boolean disabled = false;

    public TimeoutRepositoryTreeEventTrigger(long timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }

    @Override
    public boolean shouldBeRemoved() {
        return disabled || System.currentTimeMillis() - registeredAt >= timeoutMillis;
    }

    @Override
    public void reset() {
        registeredAt = System.currentTimeMillis();
    }

    @Override
    public void remove() {
        disabled = true;
    }
}
