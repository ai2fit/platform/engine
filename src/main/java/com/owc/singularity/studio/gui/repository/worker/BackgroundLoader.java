package com.owc.singularity.studio.gui.repository.worker;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import javax.swing.*;

public class BackgroundLoader<R> extends SwingWorker<R, Void> {

    public static <R> R loadAndReplace(R initialValue, Callable<R> longRunningLoader, Consumer<R> callback, boolean loaded) {
        if (loaded) {
            try {
                return longRunningLoader.call();
            } catch (Exception e) {
                return initialValue;
            }
        }
        new BackgroundLoader<>(longRunningLoader, callback).execute();
        return initialValue;
    }

    public static <R> void loadAsync(Callable<R> longRunningLoader, Consumer<R> callback) {
        new BackgroundLoader<>(longRunningLoader, callback).execute();
    }

    private final Callable<R> loader;
    private final Consumer<R> callback;

    public BackgroundLoader(Callable<R> loader, Consumer<R> callback) {
        this.loader = loader;
        this.callback = callback;
    }

    @Override
    protected R doInBackground() throws Exception {
        return loader.call();
    }

    @Override
    protected void done() {
        try {
            callback.accept(get());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
