package com.owc.singularity.studio.gui.logging;

import java.util.List;
import java.util.function.Consumer;

import javax.swing.Action;

public interface LogProvider {

    /**
     * This has to return the name of this log. Will be used to select the currently shown log.
     * 
     * @return the name
     */
    public String getName();

    /**
     * This will be called once the LogProvider is registered using the
     * {@link LogViewPanel#registerProvider(LogProvider)} method.
     * 
     * @param logConsumer
     *            can be used to display new content
     */
    public void configure(Consumer<List<String>> logConsumer);

    /**
     * This can be used to add log specific actions
     * 
     * @return
     */
    public List<Action> getActions();

    public boolean isClosable();

    public void close();
}
