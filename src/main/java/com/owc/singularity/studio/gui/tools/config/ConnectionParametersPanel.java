/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.config;


import javax.swing.JDialog;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.tools.parameter.Parameter;
import com.owc.singularity.studio.gui.parameters.GenericParameterPanel;


/**
 * This panel can display the {@link Parameter}s of
 * {@link com.owc.singularity.repository.connection.ConnectionParameters}s and supports live
 * dependency changes.
 *
 * @author Marco Boeck
 *
 */
public class ConnectionParametersPanel extends GenericParameterPanel {

    private static final long serialVersionUID = 1L;
    private final JDialog connectionParametersDialog;

    /**
     * Creates a new {@link ConnectionParametersPanel} for the specified {@link Parameters}.
     *
     * @param connectionParametersDialog
     *
     * @param parameters
     */
    public ConnectionParametersPanel(JDialog connectionParametersDialog, Parameters parameters) {
        super(parameters);
        this.connectionParametersDialog = connectionParametersDialog;
    }

    @Override
    protected void setRawValue(Operator operator, ParameterType type, String value, boolean updateComponents) {
        // always update components (otherwise live dependency changes won't work)
        setRawValue(operator, type, value);
    }

    @Override
    protected JDialog getDialogOwner() {
        return connectionParametersDialog;
    }

}
