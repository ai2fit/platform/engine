package com.owc.singularity.studio.gui.repository.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;

public class RepositoryTreeNode extends DefaultMutableTreeNode implements Comparable<RepositoryTreeNode>, Serializable {

    @Serial
    private static final long serialVersionUID = -4298474751201349152L;

    private final String pathStringWithoutVersion;
    private final Entry entry;
    private final RepositoryPath path;

    private final RepositoryTreeNodeInformation informationHolder;

    public RepositoryTreeNode(RepositoryPath path) {
        this(Entries.getEntry(path));
    }

    public RepositoryTreeNode(Entry entry) {
        this.entry = Objects.requireNonNull(entry, "entry must not be null");
        this.path = entry.getPath();
        this.pathStringWithoutVersion = path.toString(false);
        this.informationHolder = new RepositoryTreeNodeInformation(this.entry);
    }

    public RepositoryTreeNodeInformation getInformation() {
        return informationHolder;
    }

    public void reloadInformation() {
        this.informationHolder.clear();
    }

    public RepositoryPath getRepositoryPath() {
        return path;
    }

    public String getRepositoryPathStringWithoutVersion() {
        return pathStringWithoutVersion;
    }

    public Entry getEntry() {
        return entry;
    }

    public void sortChildren(Comparator<RepositoryTreeNode> comparator) {
        if (!getAllowsChildren() || getChildCount() == 0)
            return;
        Comparator<TreeNode> castedComparator = withComparator(comparator);
        sortChildrenIntern(castedComparator);
    }

    private void sortChildrenIntern(Comparator<TreeNode> comparator) {
        if (!getAllowsChildren() || getChildCount() == 0)
            return;
        this.children.sort(comparator);
        for (TreeNode child : this.children) {
            if (child instanceof RepositoryTreeNode && child.getAllowsChildren()) {
                ((RepositoryTreeNode) child).sortChildrenIntern(comparator);
            }
        }
    }

    private static Comparator<TreeNode> withComparator(Comparator<RepositoryTreeNode> original) {
        return (o1, o2) -> original.compare((RepositoryTreeNode) o1, (RepositoryTreeNode) o2);
    }

    @Override
    public RepositoryTreeNode getParent() {
        return (RepositoryTreeNode) super.getParent();
    }

    @Override
    public boolean getAllowsChildren() {
        return informationHolder.isDirectory();
    }

    public void setLoading(boolean loading) {
        getInformation().setLoading(loading);
    }

    public boolean isLoading() {
        return getInformation().isLoading();
    }

    @Override
    public RepositoryTreeNode clone() {
        return (RepositoryTreeNode) super.clone();
    }

    @Override
    public int compareTo(RepositoryTreeNode o) {
        return getRepositoryPath().compareTo(o.getRepositoryPath());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        RepositoryTreeNode viewModel = (RepositoryTreeNode) o;
        return getRepositoryPath().equals(viewModel.getRepositoryPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRepositoryPath());
    }

    @Override
    public String toString() {
        return "RepositoryTreeNode{" + getRepositoryPath().toShortString(20) + '}';
    }
}
