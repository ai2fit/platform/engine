package com.owc.singularity.studio.gui.repository.mount;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.JComponent;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.repository.RepositoryFileSystemProvider;
import com.owc.singularity.repository.RepositoryMountProvider;
import com.owc.singularity.studio.gui.Form;
import com.owc.singularity.studio.gui.parameters.GenericParameterPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;

public class DefaultMountConfigurationUIProvider implements MountConfigurationUIProvider {

    @Override
    public boolean supportsCreateUI() {
        return true;
    }

    @Override
    public Form<Map<String, String>> getCreateUI(String mountType) {
        return new FormAdapter(Collections.emptyMap(), RepositoryFileSystemProvider.getMountProviderFor(mountType));
    }

    @Override
    public boolean supportsEditUI() {
        return true;
    }

    @Override
    public Form<Map<String, String>> getEditUI(String mountType, Map<String, String> options) {
        return new FormAdapter(options, RepositoryFileSystemProvider.getMountProviderFor(mountType));
    }

    @Override
    public boolean isMountTypeSupported(String mountType) {
        // this provider represents a fallback provider, so it supports any RepositoryMount
        return true;
    }

    private static class FormAdapter implements Form<Map<String, String>> {

        private static final String ADDITIONAL_OPTIONS_KEY = "additional_options";
        private final JComponent formComponent;
        private final GenericParameterPanel propertyPanel;
        private final RepositoryMountProvider mountProvider;

        public FormAdapter(Map<String, String> availableOptions, RepositoryMountProvider mountProvider) {
            this.mountProvider = mountProvider;
            Parameters parameters = new Parameters();
            Set<String> definedOptionsKeys = mountProvider.getOptionsKeys();
            for (String optionKey : definedOptionsKeys) {
                boolean required = mountProvider.isOptionRequired(optionKey);
                boolean confidential = mountProvider.isOptionConfidential(optionKey);
                Class<?> valueType = mountProvider.getOptionType(optionKey);
                String optionValue = availableOptions.get(optionKey);
                String defaultValue = availableOptions.containsKey(optionKey) ? mountProvider.getOptionDefaultValue(optionKey) : null;
                String description = Objects
                        .requireNonNullElse(I18N.getGUIMessageOrNull("repository.mount." + mountProvider.getTypeId() + "." + optionKey + ".description"), "");
                String explicitParameterType = I18N.getGUIMessageOrNull("repository.mount." + mountProvider.getTypeId() + "." + optionKey + ".type");
                final ParameterType type;
                if (explicitParameterType != null && !explicitParameterType.isBlank()) {
                    type = generateParameterFor(optionKey, defaultValue, explicitParameterType, required, description);
                } else {
                    type = generateParameterFor(optionKey, defaultValue, valueType, required, confidential, description);
                }
                parameters.addParameterType(type);
                parameters.setRawParameter(optionKey, optionValue);
            }
            if (mountProvider.allowsAdditionalOptions()) {
                List<String> additionalOptionsKeys = new ArrayList<>();
                // add all available options that were not pre-defined by the mount provider
                for (String availableOptionKey : availableOptions.keySet()) {
                    if (!definedOptionsKeys.contains(availableOptionKey)) {
                        additionalOptionsKeys.add(availableOptionKey);
                    }
                }
                List<String[]> additionalOptionsDefaultValues = additionalOptionsKeys.stream()
                        .map(key -> new String[] { key, mountProvider.getOptionDefaultValue(key) })
                        .collect(Collectors.toList());
                String parameterDescription = Objects.requireNonNullElse(
                        I18N.getGUIMessageOrNull("repository.mount." + mountProvider.getTypeId() + "." + ADDITIONAL_OPTIONS_KEY + ".description"), "");
                parameters.addParameterType(new ParameterTypeList(ADDITIONAL_OPTIONS_KEY, parameterDescription, new ParameterTypeString("key", "", false),
                        new ParameterTypeString("value", "", false), additionalOptionsDefaultValues));

                // set the additional options values
                List<String[]> additionalOptionsValues = new ArrayList<>();
                for (String additionalOptionsKey : additionalOptionsKeys) {
                    additionalOptionsValues.add(new String[] { additionalOptionsKey, availableOptions.get(additionalOptionsKey) });
                }
                parameters.setRawParameter(ADDITIONAL_OPTIONS_KEY, ParameterTypeList.transformList2String(additionalOptionsValues));
            }
            propertyPanel = new GenericParameterPanel(parameters);
            formComponent = new ExtendedJScrollPane(propertyPanel);
        }

        @Override
        public JComponent getComponent() {
            return formComponent;
        }

        @Override
        public Map<String, String> getData() {
            Parameters parameters = propertyPanel.getParameters();
            Map<String, String> options = new LinkedHashMap<>();
            for (String parameter : parameters) {
                String optionValue;
                try {
                    optionValue = parameters.getParameter(parameter);
                } catch (UndefinedParameterError e) {
                    throw new IllegalStateException(e);
                }
                if (parameter.equalsIgnoreCase(ADDITIONAL_OPTIONS_KEY)) {
                    if (optionValue != null) {
                        List<String[]> keyValueList = ParameterTypeList.transformString2List(optionValue);
                        for (String[] keyValueArray : keyValueList) {
                            options.put(keyValueArray[0], keyValueArray[1]);
                        }
                    }
                    continue;
                }
                options.put(parameter, optionValue);
            }
            return options;
        }

        @Override
        public boolean isReadyToSubmit() {
            Set<String> definedOptionsKeys = mountProvider.getOptionsKeys();
            Parameters parameters = propertyPanel.getParameters();
            for (String definedOptionsKey : definedOptionsKeys) {
                if (mountProvider.isOptionRequired(definedOptionsKey) && !parameters.isSet(definedOptionsKey)) {
                    return false;
                }
            }
            return true;
        }

        private static ParameterType generateParameterFor(String key, String defaultValue, Class<?> valueType, boolean required, boolean confidential,
                String description) {
            String parameterType;
            if (confidential) {
                parameterType = ParameterTypePassword.class.getName();
            } else {
                if (valueType.isAssignableFrom(Boolean.class)) {
                    parameterType = ParameterTypeBoolean.class.getName();
                } else if (valueType.isAssignableFrom(Path.class)) {
                    parameterType = ParameterTypeFile.class.getName();
                } else if (valueType.isAssignableFrom(Long.class)) {
                    parameterType = ParameterTypeLong.class.getName();
                } else {
                    // default as string
                    parameterType = ParameterTypeString.class.getName();
                }
            }
            return generateParameterFor(key, defaultValue, parameterType, required, description);
        }

        private static ParameterType generateParameterFor(String key, String defaultValue, String parameterType, boolean required, String description) {
            ParameterType type;
            try {
                Class<?> parameterClass = Class.forName(parameterType, false, ModuleService.getMajorClassLoader());
                type = (ParameterType) parameterClass.getDeclaredConstructor(String.class, String.class).newInstance(key, description);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new IllegalArgumentException("Could not create parameter type of '" + parameterType + "'", e);
            }
            if (defaultValue != null)
                type.setDefaultValue(defaultValue);
            type.setOptional(!required);
            return type;
        }

        private String getInputValue(String inputKey) {
            try {
                return propertyPanel.getParameters().getParameter(inputKey);
            } catch (UndefinedParameterError expected) {
                return null;
            }
        }

        @Override
        public void addInputChangeListener(InputChangeListener listener) {
            propertyPanel.getParameters().addObserver(new ObserverAdapter(listener, this::getInputValue), true);
        }

        @Override
        public void removeInputChangeListener(InputChangeListener listener) {
            propertyPanel.getParameters().removeObserver(new ObserverAdapter(listener, this::getInputValue));
        }

        private static class ObserverAdapter implements Observer<String> {

            private final InputChangeListener listener;
            private final Function<String, String> valueSupplier;

            public ObserverAdapter(InputChangeListener listener, Function<String, String> valueSupplier) {
                this.listener = listener;
                this.valueSupplier = valueSupplier;
            }

            @Override
            public void update(Observable<String> observable, String arg) {
                listener.onInputValueChange(arg, valueSupplier.apply(arg));
            }

            @Override
            public boolean equals(Object o) {
                if (this == o)
                    return true;
                if (o == null || getClass() != o.getClass())
                    return false;
                ObserverAdapter that = (ObserverAdapter) o;
                return Objects.equals(listener, that.listener);
            }

            @Override
            public int hashCode() {
                return Objects.hash(listener);
            }
        }
    }
}
