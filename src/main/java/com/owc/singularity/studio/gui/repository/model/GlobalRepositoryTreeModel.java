/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.model;


import java.nio.file.*;
import java.util.*;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.tree.TreeNode;

import com.owc.singularity.repository.*;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.worker.RepositoryTreeFileSystemScanWorker;
import com.owc.singularity.studio.gui.repository.worker.RepositoryTreeFileSystemWatchWorker;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Model representing the underlying {@link RepositoryFileSystem} {@link Entry entries} as a tree
 * used mainly by {@link RepositoryTree}. It represents {@link Entry} as {@link RepositoryTreeNode
 * nodes} which caches {@link RepositoryTreeNode#getInformation() information} about the entry
 * relevant to the presentation layer.
 * <p>
 * A single instance of this class will suffice since one could use different
 * {@link FilteredRepositoryTreeModel views}. The {@link RepositoryTree} is the main UI element that
 * uses different {@link FilteredRepositoryTreeModel views} over a single instance of the
 * {@link RepositoryTree#getModel() model} acquired by
 * <p>
 * In order to keep the model small and fast, the model uses a lazy-loading concept. That is, the
 * initial scanning of the repository is limited to {@code n} tree levels specified during
 * {@link #initialize(int)}. Later, when its required, users could use the
 * {@link #newTreeScanWorker(RepositoryPath, Predicate, int, Runnable)} to scan and fetch more
 * levels of that node.
 * <p>
 * The model provides a dynamic representation of the repository since it watches the underlying
 * {@link com.owc.singularity.repository.RepositoryFileSystem} for any changes and update itself
 * respectively.
 *
 * @author Hatem Hamad
 *
 */
public final class GlobalRepositoryTreeModel extends AbstractRepositoryTreeModel {

    private static GlobalRepositoryTreeModel INSTANCE;

    public static void initialize(int defaultScanDepth) {
        INSTANCE = new GlobalRepositoryTreeModel(defaultScanDepth);
    }

    public static GlobalRepositoryTreeModel getInstance() {
        return INSTANCE;
    }

    private final Object changeLock = new Object();
    private final Map<String, RepositoryTreeNode> fastAccessContentTreeKeys = new HashMap<>();

    // store a reference to the listener, so that it don't get destroyed until object lifecycle ends
    @SuppressWarnings("FieldCanBeLocal")
    private final RepositoryTreeFileSystemWatchWorker changeWorker;
    private final Map<String[], RepositoryMount> knownMountingPoints = new HashMap<>();

    private GlobalRepositoryTreeModel(int loadFactor) {
        super(new RepositoryTreeNode(RepositoryManager.DEFAULT_FILESYSTEM_ROOT));
        addCacheFor(getRoot());
        this.changeWorker = new RepositoryTreeFileSystemWatchWorker(this);
        RepositoryFileSystem fileSystem = RepositoryManager.getFileSystem();
        fileSystem.registerChangeListener(this.changeWorker);
        this.changeWorker.execute();

        // mark known mounts
        knownMountingPoints.putAll(fileSystem.getMountingPoints());
        // start scanning root filesystem
        newTreeScanWorker(RepositoryManager.DEFAULT_FILESYSTEM_ROOT, null, loadFactor, null).execute();

        // register a watch for mount changes
        RepositoryManager.addRepositoryConfigurationChangeListener(mountingPoints -> {
            // check for new mounts
            synchronized (knownMountingPoints) {
                for (String[] mountPathElements : mountingPoints.keySet()) {
                    if (!knownMountingPoints.containsKey(mountPathElements)) {
                        // a new mount has been added
                        RepositoryMount mount = mountingPoints.get(mountPathElements);
                        RepositoryPath mountPath = mount.getMountPath();
                        knownMountingPoints.put(mountPathElements, mount);
                        newTreeScanWorker(mountPath, null, 1, null).execute();
                    }
                }
                // check for removed mounts
                List<String[]> removedMounts = new ArrayList<>();
                for (String[] mountPathElements : knownMountingPoints.keySet()) {
                    if (!mountingPoints.containsKey(mountPathElements)) {
                        removedMounts.add(mountPathElements);
                        RepositoryPath mountPath = knownMountingPoints.get(mountPathElements).getMountPath();
                        RepositoryTreeNode mountTreeNode = getNodeFor(mountPath);
                        if (mountTreeNode != null) {
                            SwingUtilities.invokeLater(() -> {
                                removeNodeFromParent(mountTreeNode);
                                removeCacheFor(mountTreeNode);
                            });
                        }
                        if (Files.exists(mountPath)) {
                            // re-scan the old mount path since it may contain entries
                            newTreeScanWorker(mountPath, null, 1, null).execute();
                        }
                    }
                }
                for (String[] removedMount : removedMounts) {
                    knownMountingPoints.remove(removedMount);
                }
            }
        });
    }

    /**
     * Create a new worker that will scan and fetch node subtree of the given path until the given
     * depth is reached.
     *
     * @param target
     *            the {@link Entry} path representing the node within the file system
     * @param filter
     *            a filter to apply on paths while scanning
     * @param maxDepth
     *            how many levels should the model fetch
     * @return a {@link SwingWorker} that when {@link SwingWorker#execute() executed}, it runs the
     *         scan in background
     */
    public SwingWorker<Void, ?> newTreeScanWorker(RepositoryPath target, Predicate<RepositoryPath> filter, int maxDepth, Runnable callback) {
        RepositoryTreeNode targetNode = getNodeFor(target);
        if (targetNode != null) {
            if (targetNode.isLoading())
                return new SwingWorker<>() {

                    @Override
                    protected Void doInBackground() {
                        return null;
                    }
                };
            targetNode.setLoading(true);
            SwingTools.invokeLater(() -> nodeChanged(targetNode));
        }
        return new RepositoryTreeFileSystemScanWorker(this, target, filter, maxDepth, () -> {
            if (targetNode != null) {
                targetNode.setLoading(false);
                nodeChanged(targetNode);
            }
            if (callback != null)
                callback.run();
        });
    }


    @Override
    public boolean containsNodeFor(RepositoryPath path) {
        synchronized (changeLock) {
            return fastAccessContentTreeKeys.containsKey(path.toString(false));
        }
    }

    @Override
    public RepositoryTreeNode getNodeFor(RepositoryPath path) {
        synchronized (changeLock) {
            return fastAccessContentTreeKeys.get(path.toString(false));
        }
    }

    @Override
    public int addNode(RepositoryTreeNode parent, RepositoryTreeNode node) {
        synchronized (changeLock) {
            int childIndex = parent.getChildCount();
            parent.insert(node, childIndex);
            addCacheFor(node);
            return childIndex;
        }
    }

    @Override
    public void removeNode(RepositoryTreeNode node) {
        synchronized (changeLock) {
            node.removeFromParent();
            removeCacheFor(node);
        }
    }

    private void addCacheFor(RepositoryTreeNode node) {
        synchronized (changeLock) {
            fastAccessContentTreeKeys.put(node.getRepositoryPathStringWithoutVersion(), node);
        }
    }

    private void removeCacheFor(RepositoryTreeNode node) {
        synchronized (changeLock) {
            fastAccessContentTreeKeys.remove(node.getRepositoryPathStringWithoutVersion());
        }
        Enumeration<TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            removeCacheFor((RepositoryTreeNode) children.nextElement());
        }
    }

}
