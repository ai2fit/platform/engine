/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa, Marco Boeck
 */
public class SaveAsAction extends ResourceAction {

    private static final long serialVersionUID = -6107588898380953147L;

    public SaveAsAction() {
        super("save_as");
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        saveAs(MainFrame.INSTANCE.getMainProcessPanel().getProcess());
    }

    /**
     * Opens a location choser for the user to select where the save the process.
     * 
     * @param process
     *            the process to be saved
     * 
     * 
     */
    public static void saveAs(AbstractPipeline process) {
        saveAs(process, true);
    }

    /**
     * Opens a location choser for the user to select where the save the process.
     *
     * @param process
     *            the process to be saved
     * @param async
     *            if <code>true</code>, will save the process asynchronously after the user has
     *            selected a location; if <code>false</code> saves it synchronously.
     * @return true on success, false on failure, and null if async=true
     */
    public static Boolean saveAs(AbstractPipeline process, boolean async) {
        RepositoryPath initial = null;
        if (process.getPath() != null) {
            initial = process.getPath();
        }
        RepositoryPath location = RepositoryLocationChooser.selectLocation(null, initial, MainFrame.INSTANCE.getExtensionsMenu(), true, false, false, true);
        if (location != null) {
            try {
                Entry entry = Entries.getEntry(location);
                if (Files.exists(entry.getPath()) && entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
                    if (SwingTools.showConfirmDialog("overwrite", ConfirmDialog.YES_NO_OPTION, location) == ConfirmDialog.NO_OPTION) {
                        return false;
                    }
                }
                if (location.getFileSystem().isReadOnly()) {
                    SwingTools.showSimpleErrorMessage("save_to_read_only_repo", "", location.toString());
                    // fall-through intended, SaveAction will go back to SaveAs due to read-only
                    // repository location
                } else {
                    process.setPath(location);
                }
            } catch (IOException | ClassNotFoundException e) {
                SwingTools.showSimpleErrorMessage("cannot_save_process", e, location, e.getMessage());
                // cannot reasonably save here now, we had an unexpected error
                return false;
            }

            if (async) {
                SaveAction.saveAsync(process, true);
                return null;
            } else {
                return SaveAction.save(process, true);
            }
        }
        return false;
    }
}
