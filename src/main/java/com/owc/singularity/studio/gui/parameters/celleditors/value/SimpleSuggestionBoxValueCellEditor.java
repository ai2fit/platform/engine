/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.util.List;

import javax.swing.JButton;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeSuggestion;
import com.owc.singularity.engine.tools.ProgressListener;


/**
 * Reportable for {@link ParameterTypeSuggestion}.
 * 
 * @author Nils Woehler
 */
public class SimpleSuggestionBoxValueCellEditor extends AbstractSuggestionBoxValueCellEditor {

    private static final long serialVersionUID = 1L;

    public SimpleSuggestionBoxValueCellEditor(ParameterTypeSuggestion type) {
        super(type);
        if (type.getSuggestionProvider().getAction() != null) {
            addConfigureButton(new JButton(type.getSuggestionProvider().getAction()));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getSuggestions(Operator operator, ProgressListener progressListener) {
        return (List<Object>) ((ParameterTypeSuggestion) getParameterType()).getSuggestionProvider().getSuggestions(operator, progressListener);
    }

}
