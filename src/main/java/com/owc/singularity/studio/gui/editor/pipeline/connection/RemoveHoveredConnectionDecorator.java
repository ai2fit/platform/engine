/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.connection;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Objects;

import javax.swing.SwingUtilities;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.view.ProcessRendererEventDecorator;


/**
 * Decorator that shows a trash can icon on a connection if the connection is hovered over and
 * deletes the connection if the icon is clicked. The icon is highlighted if the mouse is moved onto
 * it.
 *
 * @author Nils Woehler
 * @since 7.1.0
 */
public final class RemoveHoveredConnectionDecorator implements ProcessDrawDecorator, ProcessRendererEventDecorator {

    private Shape shape = null;
    private OutputPort from = null;
    private boolean enableTrashSymbol = false;

    @Override
    public void draw(final ExecutionUnit process, final Graphics2D g2, final ProcessRendererModel rendererModel) {
        draw(rendererModel, process, g2);
    }

    @Override
    public void print(final ExecutionUnit process, final Graphics2D g2, final ProcessRendererModel rendererModel) {
        draw(rendererModel, process, g2);
    }

    private void draw(ProcessRendererModel rendererModel, final ExecutionUnit process, final Graphics2D g2) {
        OutputPort source = rendererModel.getHoveringConnectionSource();
        if (source == null || !process.getAllOutputPorts().contains(source)) {
            // source undefined or not part of the current subprocess
            return;
        }
        if (source == rendererModel.getSelectedConnectionSource() || rendererModel.getHoveringPort() != null || rendererModel.getHoveringOperator() != null) {
            // connection is hovered indirecty
            return;
        }
        if (rendererModel.getConnectingPortSource() != null) {
            // we are in the process of creating a new connection
            return;
        }

        from = source;
        shape = ConnectionDrawUtils.renderConnectionRemovalIcon(source, enableTrashSymbol, g2, rendererModel);
    }

    @Override
    public void processMouseEvent(ExecutionUnit process, ProcessRendererModel rendererModel, MouseEventType type, MouseEvent e) {
        if (type == MouseEventType.MOUSE_MOVED) {
            if (shape != null && shape.contains(rendererModel.getMousePositionRelativeToProcess())) {
                enableTrashSymbol = true;
                rendererModel.fireMiscChanged();
            } else if (enableTrashSymbol && shape != null && !shape.contains(rendererModel.getMousePositionRelativeToProcess())) {
                enableTrashSymbol = false;
                shape = null;
                rendererModel.fireMiscChanged();
            }
        } else if (type == MouseEventType.MOUSE_PRESSED && SwingUtilities.isLeftMouseButton(e)) {
            if (shape != null && shape.contains(rendererModel.getMousePositionRelativeToProcess())) {
                if (from.isConnected() && (Objects.equals(rendererModel.getDisplayedChain(), (from.getPorts().getOwner().getOperator().getParent())))
                        || Objects.equals(rendererModel.getDisplayedChain(), (from.getPorts().getOwner().getOperator()))) {
                    from.disconnect();
                    enableTrashSymbol = false;
                    shape = null;
                    // set from port back to normal size
                    if (rendererModel.getSelectedConnectionSource() != null && rendererModel.getSelectedConnectionSource().equals(from)) {
                        rendererModel.setSelectedConnectionSource(null);
                    }
                    if (rendererModel.getHoveringConnectionSource() != null && rendererModel.getHoveringConnectionSource().equals(from)) {
                        rendererModel.setHoveringConnectionSource(null);
                    }
                    rendererModel.fireMiscChanged();
                    e.consume();
                }
            }
        }

    }

    @Override
    public void processKeyEvent(ExecutionUnit process, KeyEventType type, KeyEvent e) {
        // noop
    }

}
