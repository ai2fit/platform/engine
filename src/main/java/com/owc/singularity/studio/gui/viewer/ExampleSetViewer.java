/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.viewer;


import static com.owc.singularity.studio.gui.tools.ExtendedJTable.DEFAULT_COLUMN_WIDTH;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serial;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.object.Tableable;
import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.Attributes;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeAttribute;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.parameters.AttributesParameterDialog;
import com.owc.singularity.studio.gui.results.ResultTabActionVisualizer;
import com.owc.singularity.studio.gui.tools.*;


/**
 * Can be used to display (parts of) the data by means of a JTable. Used to display
 * {@link ExampleSet} results. Uses a {@link ExampleSetViewerTable} to display the data. The
 * provided dialogs can be used by the user to select specific attributes and rows.
 *
 * @author Ingo Mierswa, Alexander Mahler
 */
public class ExampleSetViewer extends JPanel implements Tableable {

    @Serial
    private static final long serialVersionUID = -8114228636932871865L;

    private ExampleSetViewerTable dataTable = new ExampleSetViewerTable();

    private JLabel generalInfo;

    private transient ExampleSet originalExampleSet;
    JScrollPane tableScrollPane;
    private transient String filterOptionRegEx = "";
    private transient String filterOptionSpecificValue = "";

    public ExampleSetViewer(ExampleSet exampleSet) {
        super(new BorderLayout());
        setOpaque(true);
        setBackground(Colors.WHITE);
        this.originalExampleSet = exampleSet;

        JPanel headerBar = new JPanel(new GridBagLayout());
        headerBar.setOpaque(true);
        headerBar.setBackground(Colors.WHITE);
        headerBar.setBorder(BorderFactory.createEmptyBorder(7, 0, 0, 10));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;

        final JComponent resultActionsComponent = ResultTabActionVisualizer.createResultActionsComponent(() -> originalExampleSet);
        if (resultActionsComponent != null) {
            gbc.insets = new Insets(0, 0, 0, 0);
            headerBar.add(new JLabel(I18N.getGUILabel("data_view.open_in.label")), gbc);

            gbc.gridx += 1;
            headerBar.add(resultActionsComponent, gbc);
        }

        // add row selection buttons to header
        gbc.weightx = 0.05;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        final JButton filterExamplesButton = new JButton("Filter Examples", Icons.fromName("spreadsheet_row.png").getSmallIcon());
        filterExamplesButton.addActionListener(
                evt -> new FilterRowsFromExampleSetDialog(this, dataTable.getRowSelectionParameters(), filterOptionRegEx, filterOptionSpecificValue)
                        .setVisible(true));

        gbc.gridx = 0;
        gbc.insets = new Insets(0, 9, 0, 0);
        headerBar.add(filterExamplesButton, gbc);

        // add attribute selection buttons to header
        gbc.insets = new Insets(0, 10, 0, 0);
        final JButton filterAttributesButton = new JButton("Select Attributes", Icons.fromName("spreadsheet_column.png").getSmallIcon());
        gbc.gridx = 1;
        // trigger AttributesParameterDialog when pressing the attribute filter button
        filterAttributesButton.addActionListener(evt -> {
            ParameterTypeAttribute parameterTypeAttribute = new ParameterTypeAttribute("", "", () -> ExampleSetMetaData.of(originalExampleSet), false);
            LinkedList<String> preSelectedAttributeNames = new LinkedList<>();
            for (Attribute attribute : dataTable.getSelectedAttributes())
                preSelectedAttributeNames.add(attribute.getName());
            AttributesParameterDialog dialog = new AttributesParameterDialog(parameterTypeAttribute, preSelectedAttributeNames, true, "column");
            dialog.setVisible(true);
            if (dialog.isOk()) {
                Collection<String> attributeNames = dialog.getSelectedAttributeNames();
                preSelectedAttributeNames.clear();
                preSelectedAttributeNames.addAll(attributeNames);
                updateSelectedAttributes(preSelectedAttributeNames);
            }
        });
        headerBar.add(filterAttributesButton, gbc);

        // add jump to line button to header
        final JButton jumpToLineButton = new JButton(new RotatedIcon(Icons.fromName("arrow_to.png").getSmallIcon(), RotatedIcon.Rotate.UPSIDE_DOWN));
        jumpToLineButton.setToolTipText("Jump to row");
        jumpToLineButton.addActionListener(evt -> new GoToLineDialog(this).setVisible(true));
        gbc.weightx = 0.04;
        gbc.gridx = 2;
        headerBar.add(jumpToLineButton, gbc);
        // add jump to next changed value button to header
        final JButton goToNextChangedValueButton = new JButton(Icons.fromName("arrow_down_delta.png").getSmallIcon());
        goToNextChangedValueButton.setToolTipText("Jump to next changed value in column");
        goToNextChangedValueButton.addActionListener(evt -> goToNextChangedValue());
        gbc.gridx = 3;
        headerBar.add(goToNextChangedValueButton, gbc);

        // add search field to header
        gbc.gridx = 4;
        gbc.weightx = 0.1;
        JTextField searchTextField = new JTextField("", 10);
        searchTextField.addActionListener(i -> search(searchTextField.getText()));
        PromptSupport.setPrompt("Search", searchTextField);
        PromptSupport.setFocusBehavior(PromptSupport.FocusBehavior.HIDE_PROMPT, searchTextField);
        searchTextField.setLayout(new BorderLayout());
        JLabel label = new JLabel(Icons.fromName("magnifying_glass.png").getIcon16x16());
        label.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                search(searchTextField.getText());
            }
        });
        searchTextField.add(label, BorderLayout.EAST);
        headerBar.add(searchTextField, gbc);

        // add filler component at end
        gbc.gridx += 1;
        gbc.weightx = 1.0;
        headerBar.add(new JLabel(), gbc);

        add(headerBar, BorderLayout.NORTH);

        // add display for example set
        tableScrollPane = new ExtendedJScrollPane(dataTable);
        tableScrollPane.setOpaque(true);
        tableScrollPane.setBorder(BorderFactory.createEmptyBorder(5, 10, 0, 10));
        tableScrollPane.setBackground(Colors.WHITE);
        tableScrollPane.getViewport().setBackground(Colors.WHITE);
        add(tableScrollPane, BorderLayout.CENTER);
        dataTable.setExampleSet(exampleSet);

        // initialize and add general information label
        generalInfo = new JLabel();
        final Border emptyBorder = BorderFactory.createEmptyBorder(6, 10, 6, 0);
        generalInfo.setBorder(emptyBorder);
        if (!dataTable.canShowAllRows()) {
            // if this happens there are too many rows to be displayed by swing because its an int
            // overflow. Adding information for the user
            final JLabel tooManyRowsLabel = new ResourceLabel("datatable.too_many_rows");
            tooManyRowsLabel.setBorder(emptyBorder);
            JPanel panel = new JPanel(new GridBagLayout());
            panel.setOpaque(false);
            GridBagConstraints panelGbc = new GridBagConstraints();
            panelGbc.gridx = 0;
            panelGbc.gridy = 0;
            panelGbc.weightx = 1;
            panelGbc.anchor = GridBagConstraints.WEST;
            panel.add(tooManyRowsLabel, panelGbc);
            panelGbc.gridy++;
            panel.add(generalInfo, panelGbc);
            add(panel, BorderLayout.SOUTH);
        } else {
            add(generalInfo, BorderLayout.SOUTH);
        }

        dataTable.unpack();
        updateGeneralInformationLabel(originalExampleSet.size(), originalExampleSet.getAttributes().specialSize(), originalExampleSet.getAttributes().size());
    }

    @Override
    public void prepareReporting() {
        dataTable.prepareReporting();
    }

    @Override
    public void finishReporting() {
        dataTable.finishReporting();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return dataTable.getColumnName(columnIndex);
    }

    @Override
    public String getCell(int row, int column) {
        return dataTable.getCell(row, column);
    }

    @Override
    public int getColumnNumber() {
        return dataTable.getColumnNumber();
    }

    @Override
    public int getRowNumber() {
        return dataTable.getRowNumber();
    }

    @Override
    public boolean isFirstLineHeader() {
        return false;
    }

    @Override
    public boolean isFirstColumnHeader() {
        return false;
    }

    public int getOriginalNumberOfRows() {
        return originalExampleSet.size();
    }

    public ExampleSet getOriginalExampleSet() {
        return originalExampleSet;
    }

    /**
     * Updates the displayed rows of the example set with a range selection. The filterOptionRegEx
     * and filterOptionSpecificValue are saved for later retrieval.
     * 
     * @param startRow
     *            Start Row index
     * @param endRow
     *            End Row index
     * @param invertRangeSelection
     *            whether to invert the range selection
     */
    public void updateSelectedExamples(int startRow, int endRow, boolean invertRangeSelection, String filterOptionRegEx, String filterOptionSpecificValue) {
        updateSelectedExamples(startRow, endRow, invertRangeSelection, null, false, filterOptionRegEx, filterOptionSpecificValue);
    }

    /**
     * Updates the displayed rows of the example set with a range selection as well as a provided
     * row filter. The filterOptionRegEx and filterOptionSpecificValue are saved for later
     * retrieval.
     * 
     * @param startRow
     *            Start Row index
     * @param endRow
     *            End Row index
     * @param invertRangeSelection
     *            whether to invert the range selection
     * @param rowFilter
     *            the selected filter condition
     * @param invertRowFilter
     *            whether to invert the filter selection
     */
    public void updateSelectedExamples(int startRow, int endRow, boolean invertRangeSelection, ExampleSetRowFilter rowFilter, boolean invertRowFilter,
            String filterOptionRegEx, String filterOptionSpecificValue) {
        this.filterOptionRegEx = filterOptionRegEx;
        this.filterOptionSpecificValue = filterOptionSpecificValue;
        dataTable.updateRowSelection(startRow, endRow, invertRangeSelection, rowFilter, invertRowFilter);
        List<Attribute> selectedAttributes = dataTable.getSelectedAttributes();
        int specialAttributeCount = (int) selectedAttributes.stream().filter(Attribute::isSpecial).count();
        int regularAttributeCount = selectedAttributes.size() - specialAttributeCount;
        updateGeneralInformationLabel(dataTable.getRowCount(), specialAttributeCount, regularAttributeCount);
    }

    /**
     * Updates the displayed attributes to the given selection by matching the provided attribute
     * names to the real attributes and selecting these attributes in the data table.
     * 
     * @param selectedAttributeNames
     *            List of selected attribute names (Note: attributes not contained within the given
     *            example set are ignored)
     */
    public void updateSelectedAttributes(LinkedList<String> selectedAttributeNames) {
        List<Attribute> selectedAttributes = new LinkedList<>();
        Attributes attributes = originalExampleSet.getAttributes();
        for (String attributeName : selectedAttributeNames) {
            if (attributes.contains(attributeName))
                selectedAttributes.add(attributes.get(attributeName));
        }
        int[] widths = new int[selectedAttributes.size()];
        for (int i = 0; i < selectedAttributes.size(); i++) {
            try {
                widths[i] = dataTable.getColumn(selectedAttributes.get(i).getName()).getWidth();
            } catch (IllegalArgumentException e) {
                // attribute not contained in current table
                widths[i] = DEFAULT_COLUMN_WIDTH;
            }
        }
        dataTable.setSelectedAttributes(selectedAttributes);
        JTableHeader header = dataTable.getTableHeader();
        if (header != null) {
            for (int c = 0; c < selectedAttributes.size(); c++) {
                TableColumn tableColumn = dataTable.getColumn(selectedAttributes.get(c).getName());
                header.setResizingColumn(tableColumn); // this line is very important
                tableColumn.setWidth(widths[c]);
            }
        }
        int specialAttributeCount = (int) selectedAttributes.stream().filter(Attribute::isSpecial).count();
        int regularAttributeCount = selectedAttributes.size() - specialAttributeCount;
        updateGeneralInformationLabel(dataTable.getRowCount(), specialAttributeCount, regularAttributeCount);
    }

    /**
     * Updates the general information label on the bottom left with the provided parameters.
     * 
     * @param displayedRows
     *            currently displayed number of rows
     * @param noSpecial
     *            currently displayed number of special attributes
     * @param noRegular
     *            currently displayed number of regular attributes
     */
    private void updateGeneralInformationLabel(int displayedRows, int noSpecial, int noRegular) {
        String infoTextBegin = I18N.getGUILabel("data_view.filter_result_begin.label");
        String infoTextExample = I18N.getGUILabel(displayedRows == 1 ? "data_view.filter_result_example.label" : "data_view.filter_result_examples.label",
                displayedRows, getOriginalNumberOfRows());
        String infoTextSpecial = I18N.getGUILabel(noSpecial == 1 ? "data_view.filter_result_special.label" : "data_view.filter_result_specials.label",
                noSpecial, originalExampleSet.getAttributes().specialSize());
        String infoTextRegular = I18N.getGUILabel(noRegular == 1 ? "data_view.filter_result_regular.label" : "data_view.filter_result_regulars.label",
                noRegular, originalExampleSet.getAttributes().size());
        generalInfo.setText(infoTextBegin + infoTextExample + " " + infoTextSpecial + " " + infoTextRegular);
    }

    /**
     * Searches the dataTable for the first match of the specified value. The search starts at the
     * currently selected row and column and is case-sensitive. If a match is found the search is
     * canceled and the row and column is selected. Finding a match also adjusts the view point
     * through the adjustment of the scrollbar.
     * 
     * @param searchValue
     *            the value to be searched
     */
    private void search(String searchValue) {
        if (searchValue != null && !searchValue.isEmpty()) {
            int startRow = dataTable.getSelectedRow();
            if (startRow == -1)
                startRow = 0;
            int startColumn = dataTable.getSelectedColumn() + 1;
            if (startColumn == 0)
                startColumn = 1;
            boolean match = false;
            for (int row = startRow; row < dataTable.getRowCount(); row++) {
                if (row != startRow)
                    // column 0 holds the row number information
                    startColumn = 1;
                for (int column = startColumn; column < dataTable.getColumnCount(); column++) {
                    Object value = dataTable.getValueAt(row, column);
                    if (value != null && value.toString().contains(searchValue)) {
                        // select value
                        dataTable.setRowSelectionInterval(row, row);
                        dataTable.setColumnSelectionInterval(column, column);
                        match = true;
                        // scroll to location
                        scrollToRow(row);
                        break;
                    }
                }
                if (match)
                    break;
            }
            if (!match)
                dataTable.clearSelection();
        }
    }

    /**
     * Scrolls the table scroll panel to the specified row
     * 
     * @param row
     *            the row index to scroll to
     */
    public void scrollToRow(int row) {
        tableScrollPane.getVerticalScrollBar().setValue((int) (((double) row / dataTable.getRowCount()) * tableScrollPane.getVerticalScrollBar().getMaximum()));
    }

    /**
     * Iterates over all tale rows and selects the first row with a differing value than the
     * currently selected row.
     */
    public void goToNextChangedValue() {
        int startRow = dataTable.getSelectedRow();
        int selectedColumn = dataTable.getSelectedColumn();
        TableModel model = dataTable.getModel();
        if (model != null && startRow != -1 && selectedColumn != -1) {
            Object valueAt = model.getValueAt(startRow, selectedColumn);
            for (int row = startRow + 1; row < dataTable.getRowCount(); row++) {
                if (!Objects.equals(valueAt, model.getValueAt(row, selectedColumn))) {
                    dataTable.setRowSelectionInterval(row, row);
                    scrollToRow(row);
                    break;
                }
            }
        }
    }
}
