package com.owc.singularity.studio.gui.repository.event;

import com.owc.singularity.repository.RepositoryPath;

public class OnDeleteRepositoryTreeEventTrigger extends TimeoutRepositoryTreeEventTrigger {

    private final RepositoryPath deletedPath;
    private boolean found = false;

    public OnDeleteRepositoryTreeEventTrigger(RepositoryPath deletedPath) {
        this(deletedPath, 5_000);
    }

    public OnDeleteRepositoryTreeEventTrigger(RepositoryPath deletedPath, long timeoutMillis) {
        super(timeoutMillis);
        this.deletedPath = deletedPath;
    }

    @Override
    public boolean shouldFire(RepositoryTreeUpdateType event, RepositoryPath... affectedPaths) {
        if (event == RepositoryTreeUpdateType.Remove) {
            for (RepositoryPath path : affectedPaths) {
                if (deletedPath.equals(path))
                    return found = true;
            }
        }
        return false;
    }

    @Override
    public boolean shouldBeRemoved() {
        return found || super.shouldBeRemoved();
    }
}
