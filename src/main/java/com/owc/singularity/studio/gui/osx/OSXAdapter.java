/**
 * Copyright (C) 2012-2020 by SingularityEngine and the contributors
 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.osx;

import java.awt.*;
import java.awt.desktop.*;

import javax.swing.*;

/**
 * The {@link OSXAdapter} class can be used to adapt a program to the L&F of OS X. It installs a
 * AboutHandler and a PreferencesHandler. Furthermore it sets a dock icon and allows the provided
 * window to be set into fullscreen mode.
 *
 * @author Nils Woehler
 *
 */
@SuppressWarnings("restriction")
public class OSXAdapter {

    private static boolean adapted = false;

    /**
     * The implementation of a AboutHandler. It executes the action provided by {@link #adaptUI}.
     *
     * @author Nils Woehler
     *
     */
    private static final class OSXAboutHandler implements AboutHandler {

        private Action action;

        public OSXAboutHandler(Action action) {
            this.action = action;
        }

        public void handleAbout(AboutEvent e) {
            action.actionPerformed(null);
        }

    };

    /**
     * The implementation of a PreferencesHandler. It executes the action provided by
     * {@link #adaptUI}.
     *
     * @author Nils Woehler
     *
     */
    private static final class OSXPreferencesHandler implements PreferencesHandler {

        private Action action;

        public OSXPreferencesHandler(Action action) {
            this.action = action;
        }

        public void handlePreferences(PreferencesEvent arg0) {
            action.actionPerformed(null);
        }
    };

    private static final class OSXQuitHandler implements QuitHandler {

        private OSXQuitListener quitListener = null;

        public OSXQuitHandler(OSXQuitListener listener) {
            quitListener = listener;
        }

        @Override
        public void handleQuitRequestWith(QuitEvent arg0, QuitResponse arg1) {
            if (quitListener != null) {
                quitListener.quit();
            }
        }

    }


    /**
     * @return <code>true</code> if {@link #adaptUI} has been called successfully.
     */
    public static boolean isAdapted() {
        return adapted;
    }

    /**
     * Adapts the UI to be in line with common Mac OS X design.
     *
     * @param fullScreenWindow
     *            the window that should be allowed to change to fullscreen mode
     * @param preferencesAction
     *            the action that is executed if the preferences are opened
     * @param aboutAction
     *            the action that is executed if the about menu entry is opened
     *
     * @throws Throwable
     *             in case anything goes wrong
     */
    public static void adaptUI(Window fullScreenWindow, Action preferencesAction, Action aboutAction, OSXQuitListener listener) throws Throwable {
        if (isAdapted())
            return;
        // Set correct About and Preference handlers.
        final Desktop desktop = Desktop.getDesktop();
        desktop.setAboutHandler(new OSXAboutHandler(aboutAction));
        desktop.setPreferencesHandler(new OSXPreferencesHandler(preferencesAction));
        desktop.setQuitHandler(new OSXQuitHandler(listener));
        adapted = true;
    }

    /**
     * Sets the given {@link Frame} to fullscreen mode if it fulfills certain criteria and if
     * possible. If an error occurs during toggling, the frame will just be maximized.
     * <p>
     * The conditions namely are:
     * <ol>
     * <li>The extended state is {@link Frame#NORMAL}</li>
     * <li>The y position is smaller or equal to 0</li>
     * <li>The frame's size is bigger than the screen size</li>
     * </ol>
     *
     * @param fullScreenFrame
     *            the frame that should be toggled to fullscreen mode
     * @since 1.0.2
     */
    public static final void checkForFullScreen(Frame fullScreenFrame) {
        int extendedState = fullScreenFrame.getExtendedState();
        if (extendedState != Frame.NORMAL) {
            return;
        }
        Point location = fullScreenFrame.getLocation();
        if (location.y > 0) {
            return;
        }
        Dimension size = fullScreenFrame.getSize();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (size.height < screenSize.height || size.width < screenSize.width) {
            return;
        }
        try {
            GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
            if (gd.isFullScreenSupported()) {
                fullScreenFrame.setUndecorated(true);
                gd.setFullScreenWindow(fullScreenFrame);
            } else {
                System.err.println("Full screen not supported");
                fullScreenFrame.setSize(100, 100); // just something to let you see the window
                fullScreenFrame.setVisible(true);
            }
        } catch (Exception e) {
            fullScreenFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
        }
    }
}
