/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.viewer.metadata.model;


import java.awt.Font;
import java.lang.ref.WeakReference;

import javax.swing.event.EventListenerList;

import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.tools.FontTools;
import com.owc.singularity.studio.gui.viewer.metadata.AttributeStatisticsPanel;
import com.owc.singularity.studio.gui.viewer.metadata.event.AttributeStatisticsEvent;
import com.owc.singularity.studio.gui.viewer.metadata.event.AttributeStatisticsEvent.EventType;
import com.owc.singularity.studio.gui.viewer.metadata.event.AttributeStatisticsEventListener;


/**
 * Abstract model for the {@link AttributeStatisticsPanel}. See implementations for details.
 *
 * @author Marco Boeck
 *
 */
public abstract class AbstractAttributeStatisticsModel {

    /** the {@link Attribute} for this model */
    private final Attribute attribute;

    /** stores the {@link ExampleSet} as a {@link WeakReference} */
    private final WeakReference<ExampleSet> weakExampleSet;

    /** if not <code>null</code>, the attribute is a special attribute */
    private final String specialAttName;

    /** if true, the panel should be drawn in an alternating color scheme */
    private boolean alternating;

    /** if true, the display should be enlarged */
    private boolean enlarged;

    /** the number of missing values */
    protected double missing;

    /** the number of examples */
    protected int examplesCount;

    /** event listener for this model */
    private final EventListenerList eventListener;

    /**
     * Inits the
     *
     * @param exampleSet
     * @param attribute
     */
    protected AbstractAttributeStatisticsModel(final ExampleSet exampleSet, final Attribute attribute) {
        this.attribute = attribute;
        this.weakExampleSet = new WeakReference<>(exampleSet);
        this.specialAttName = attribute.getRole();

        this.eventListener = new EventListenerList();
    }

    /**
     * Adds a {@link AttributeStatisticsEventListener} which will be informed of all changes to this
     * model.
     *
     * @param listener
     */
    public void registerEventListener(final AttributeStatisticsEventListener listener) {
        eventListener.add(AttributeStatisticsEventListener.class, listener);
    }

    /**
     * Removes the {@link AttributeStatisticsEventListener} from this model.
     *
     * @param listener
     */
    public void removeEventListener(final AttributeStatisticsEventListener listener) {
        eventListener.remove(AttributeStatisticsEventListener.class, listener);
    }

    /**
     * Sets if this panel should be drawn in an alternating color scheme (slightly darker) to make
     * reading of many rows easier.
     *
     * @param alternating
     */
    public void setAlternating(final boolean alternating) {
        if (this.alternating != alternating) {
            this.alternating = alternating;

            fireAlternatingChangedEvent();
        }
    }

    /**
     * Returns <code>true</code> if this is an alternating attribute statistics model.
     *
     */
    public boolean isAlternating() {
        return alternating;
    }

    /**
     * Gets the enlarged status which determines how many information to display.
     *
     */
    public boolean isEnlarged() {
        return enlarged;
    }

    /**
     * Sets the enlarged status.
     *
     * @param enlarged
     */
    public void setEnlarged(final boolean enlarged) {
        this.enlarged = enlarged;
        if (enlarged && getExampleSetOrNull() != null) {
            prepareCharts();
        }

        fireEnlargedChangedEvent();
    }


    /**
     * Returns <code>true</code> if this attribute has a special attribute role; <code>false</code>
     * otherwise.
     *
     */
    public boolean isSpecialAtt() {
        return specialAttName != null;
    }

    /**
     * Returns the name of the special attribute role for this {@link Attribute}. If this is not a
     * special attribute, returns <code>null</code> .
     *
     */
    public String getSpecialAttName() {
        return specialAttName;
    }

    /**
     * Gets the {@link Attribute} backing this model.
     *
     */
    public Attribute getAttribute() {
        return attribute;
    }

    /**
     * Gets the {@link ExampleSet} backing this model or <code>null</code> if the
     * {@link WeakReference} to it was removed.
     *
     */
    public ExampleSet getExampleSetOrNull() {
        return weakExampleSet.get();
    }

    /**
     * Fire when the enlarged status has changed.
     */
    protected void fireEnlargedChangedEvent() {
        fireEvent(EventType.ENLARGED_CHANGED);
    }

    /**
     * Fire when the statistics of an attribute have changed.
     */
    protected void fireStatisticsChangedEvent() {
        fireEvent(EventType.STATISTICS_CHANGED);
    }

    /**
     * Fire when alternation has changed.
     */
    protected void fireAlternatingChangedEvent() {
        fireEvent(EventType.ALTERNATING_CHANGED);
    }

    /**
     * Fires the given {@link EventType}.
     *
     * @param type
     */
    protected void fireEvent(final EventType type) {
        Object[] listeners = eventListener.getListenerList();
        // AbstractPipeline the listeners last to first
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == AttributeStatisticsEventListener.class) {
                AttributeStatisticsEvent e = new AttributeStatisticsEvent(type);
                ((AttributeStatisticsEventListener) listeners[i + 1]).modelChanged(e);
            }
        }
    }

    /**
     * Updates the statistics of this model via the given {@link ExampleSet}.
     *
     * @param exampleSet
     *            the {@link ExampleSet} for which the attribute statistics should be updated. No
     *            reference to it is stored to prevent memory leaks.
     */
    public abstract void updateStatistics(ExampleSet exampleSet);

    /**
     * Returns the number of missing values.
     *
     */
    public double getNumberOfMissingValues() {
        return missing;
    }

    /**
     * Returns the number of examples.
     *
     */
    public int getNumberExamples() {
        return examplesCount;
    }


    /**
     * Returns the given {@link JFreeChart} for the given index. If the given index is invalid,
     * returns <code>null</code>.
     *
     * @param index
     */
    public abstract JFreeChart getChartOrNull(int index);

    /**
     * Prepares the charts if needed.
     */
    protected abstract void prepareCharts();

    /**
     * Changes the font of {@link JFreeChart}s to Sans Serif. This method uses a
     * {@link StandardChartTheme} to do so, so any changes to the look of the chart must be done
     * after calling this method.
     *
     * @param chart
     *            the chart to change fonts for
     */
    protected static void setDefaultChartFonts(JFreeChart chart) {
        final ChartTheme chartTheme = StandardChartTheme.createJFreeTheme();

        if (StandardChartTheme.class.isAssignableFrom(chartTheme.getClass())) {
            StandardChartTheme standardTheme = (StandardChartTheme) chartTheme;
            // The default font used by JFreeChart cannot render japanese etc symbols
            final Font oldExtraLargeFont = standardTheme.getExtraLargeFont();
            final Font oldLargeFont = standardTheme.getLargeFont();
            final Font oldRegularFont = standardTheme.getRegularFont();
            final Font oldSmallFont = standardTheme.getSmallFont();

            final Font extraLargeFont = FontTools.getFont(Font.SANS_SERIF, oldExtraLargeFont.getStyle(), oldExtraLargeFont.getSize());
            final Font largeFont = FontTools.getFont(Font.SANS_SERIF, oldLargeFont.getStyle(), oldLargeFont.getSize());
            final Font regularFont = FontTools.getFont(Font.SANS_SERIF, oldRegularFont.getStyle(), oldRegularFont.getSize());
            final Font smallFont = FontTools.getFont(Font.SANS_SERIF, oldSmallFont.getStyle(), oldSmallFont.getSize());

            standardTheme.setExtraLargeFont(extraLargeFont);
            standardTheme.setLargeFont(largeFont);
            standardTheme.setRegularFont(regularFont);
            standardTheme.setSmallFont(smallFont);

            standardTheme.apply(chart);
        }
    }
}
