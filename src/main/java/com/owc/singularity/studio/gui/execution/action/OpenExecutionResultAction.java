package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.visualization.datatable.DataTable;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;

public class OpenExecutionResultAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private Object object;

    public OpenExecutionResultAction(Object object) {
        super("toolkit.open_execution_result");
        this.object = object;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        MainFrame frame = MainFrame.INSTANCE;
        if (object instanceof IOObject)
            frame.getResultDisplay().showResult((IOObject) object, SwingTools.createIcon("16/mouse_pointer.png"));
        else if (object instanceof DataTable)
            frame.getResultDisplay().addDataTable((DataTable) object);

        frame.getPerspectiveController().showPerspective(PerspectiveController.RESULT);
    }

}
