/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.awt.Component;

import javax.swing.*;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeEnumCategory;
import com.owc.singularity.studio.gui.look.RapidLookListCellRenderer;
import com.owc.singularity.studio.gui.parameters.DefaultRMCellEditor;
import com.owc.singularity.studio.gui.parameters.ParameterTable;
import com.owc.singularity.studio.gui.tools.ExtendedJComboBox;


/**
 * Editor for parameter values string, int, double, category, and boolean. This can be used in all
 * {@link ParameterTable}s to show or editing the properties / parameters. For more special
 * parameter types other solutions exist.
 *
 * @see FileValueCellEditor
 * @see ListValueCellEditor
 * @see ColorValueCellEditor
 * @see OperatorValueValueCellEditor
 * @author Ingo Mierswa, Simon Fischer, Nils Woehler
 */
public class EnumCategoryValueCellEditor extends DefaultRMCellEditor implements PropertyValueCellEditor {

    private static final long serialVersionUID = 3594466409311826645L;

    private boolean useEditorAsRenderer = false;

    private boolean rendersLabel = false;

    public EnumCategoryValueCellEditor(final ParameterTypeEnumCategory<?> type) {
        super(new ExtendedJComboBox<>(type.getValues()));
        useEditorAsRenderer = true;
        ((JComboBox<?>) editorComponent).removeItemListener(this.delegate);
        ((JComboBox<?>) editorComponent).setRenderer(new RapidLookListCellRenderer() {

            private static final long serialVersionUID = 9098207672352997886L;

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(EnumCategoryValueCellEditor.this.getText(value));
                Icon icon = EnumCategoryValueCellEditor.this.getIcon(value);
                if (icon != null)
                    setIcon(icon);

                return component;
            }
        });


        this.delegate = new EditorDelegate() {

            private static final long serialVersionUID = -2104662561680969750L;

            @Override
            public void setValue(Object x) {
                if (x == null) {
                    super.setValue(null);
                    ((JComboBox<?>) editorComponent).setSelectedIndex(-1);
                } else {
                    try {
                        Integer index = Integer.valueOf(x.toString());
                        super.setValue(index);
                        ((JComboBox<?>) editorComponent).setSelectedIndex(index);
                    } catch (NumberFormatException e) {
                        // try to get index from string...
                        int index = type.getIndex(x.toString());
                        super.setValue(index);
                        ((JComboBox<?>) editorComponent).setSelectedIndex(index);
                    }
                }
            }

            @Override
            public Object getCellEditorValue() {
                return ((JComboBox<?>) editorComponent).getSelectedItem();
            }
        };
        ((JComboBox<?>) editorComponent).addItemListener(delegate);
    }

    protected String getText(Object value) {
        if (value != null)
            return value.toString().toLowerCase().replace("_", " ");
        return "";
    }

    protected Icon getIcon(Object value) {
        return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        Component c;
        if (table == null) {
            c = editorComponent;
            delegate.setValue(value);
            // otherwise we have a white border around check boxes
            ((JComponent) c).setOpaque(!(c instanceof JCheckBox) && !(c instanceof JComboBox));
        } else {
            c = super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
        return c;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return getTableCellEditorComponent(table, value, hasFocus, row, column);
    }

    @Override
    public boolean useEditorAsRenderer() {
        return useEditorAsRenderer;
    }

    /** Does nothing. */
    @Override
    public void setOperator(Operator operator) {}

    @Override
    public boolean rendersLabel() {
        return rendersLabel;
    }
}
