/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters;


import com.owc.singularity.engine.pipeline.parameter.ParameterType;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * @author Simon Fischer, Tobias Malbrecht
 */
public class ParameterDialog extends ButtonDialog {

    private static final long serialVersionUID = -5112534796600557146L;

    private final ParameterType type;

    public ParameterDialog(final ParameterType type, String key) {
        super(ApplicationFrame.getApplicationFrame(), "parameter." + key, ModalityType.APPLICATION_MODAL, new Object[] {});
        this.type = type;
    }

    protected ParameterType getParameterType() {
        return this.type;
    }

    @Override
    protected String getInfoText() {
        return "<html>" + I18N.getGUIMessage(getKey() + ".title") + ": <b>" + type.getKey().replace("_", " ") + "</b><br/>" + type.getDescription() + "</html>";
    }

    @Override
    protected String getDialogTitle() {
        return super.getDialogTitle() + ": " + type.getKey().replace("_", " ");
    }

    public boolean isOk() {
        return wasConfirmed();
    }
}
