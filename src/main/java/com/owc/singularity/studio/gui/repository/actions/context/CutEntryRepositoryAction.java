/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.Action;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * This action is the standard cut action.
 *
 * @author Adrian Wilke
 */
public class CutEntryRepositoryAction extends AbstractRepositoryContextAction<Object> {

    private static final long serialVersionUID = 1L;

    /** Sets the i18n key and the action command key */
    public CutEntryRepositoryAction(RepositoryTree tree) {
        super(tree, true, true, null, true, true, "repository_cut");
        putValue(ACTION_COMMAND_KEY, "cut");
    }

    /** Fires action event */
    @Override
    public void loggedActionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        Action action = tree.getActionMap().get(actionCommand);
        if (action != null) {
            action.actionPerformed(new ActionEvent(tree, ActionEvent.ACTION_PERFORMED, actionCommand));
        }
    }

    @Override
    protected Object configureAction(List<RepositoryTreeNode> entries) {
        // not needed because we override actionPerformed(ActionEvent e) which is the only caller
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, Object config, ProgressListener progressListener) {
        // not needed because we override actionPerformed(ActionEvent e) which is the only caller
    }

}
