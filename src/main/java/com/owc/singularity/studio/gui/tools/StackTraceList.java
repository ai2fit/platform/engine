package com.owc.singularity.studio.gui.tools;

import java.awt.*;

import javax.swing.*;

import com.owc.singularity.engine.tools.Tools;

public class StackTraceList extends JList<Object> {

    private static final long serialVersionUID = -2482220036723949144L;

    public StackTraceList(Throwable t) {
        super(new DefaultListModel<>());
        setFont(getFont().deriveFont(Font.PLAIN));
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        appendAllStackTraces(t);
    }

    private DefaultListModel<Object> model() {
        return (DefaultListModel<Object>) getModel();
    }

    private void appendAllStackTraces(Throwable throwable) {
        while (throwable != null) {
            appendStackTrace(throwable);
            throwable = throwable.getCause();
            if (throwable != null) {
                model().addElement("");
                model().addElement("Cause");
            }
        }
    }

    private void appendStackTrace(Throwable throwable) {
        model().addElement("Exception: " + throwable.getClass().getName());
        model().addElement("Message: " + throwable.getMessage());
        model().addElement("Stack trace:" + Tools.getLineSeparator());
        for (int i = 0; i < throwable.getStackTrace().length; i++) {
            model().addElement(new FormattedStackTraceElement(throwable.getStackTrace()[i]));
        }
    }

    public static class FormattedStackTraceElement {

        private final StackTraceElement ste;

        private FormattedStackTraceElement(StackTraceElement ste) {
            this.ste = ste;
        }

        @Override
        public String toString() {
            return "  " + ste;
        }
    }
}
