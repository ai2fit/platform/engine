package com.owc.singularity.studio.gui.repository.actions.context;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.*;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryMountAction;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.OperationProgressMonitorAdapter;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;

public class MountSpecificRepositoryTreeContextActionFactory implements RepositoryTreeContextActionFactory {

    private final RepositoryMount repositoryMount;
    private final List<MountActionAdapter> actions = new ArrayList<>();

    private interface ActionExecutable {

        Object execute(List<RepositoryPath> pathsToApplyOn, Map<String, ?> options) throws IOException;
    }

    private record MountActionAdapter(String name, Predicate<List<RepositoryPath>> condition, ActionExecutable action) {

    }

    public MountSpecificRepositoryTreeContextActionFactory(RepositoryMount mount) {
        this.repositoryMount = mount;
        for (RepositoryMountAction mountAction : repositoryMount.getActions()) {
            Set<String> optionsKeys = mountAction.getOptionsKeys();
            if (optionsKeys.isEmpty()) {
                // action does not need an extra input dialog
                actions.add(new MountActionAdapter("repository.mount." + repositoryMount.getType() + ".action." + mountAction.getId(), mountAction::supports,
                        mountAction::apply));
            }
        }
    }

    @Override
    public List<Action> createActionsFor(RepositoryTree tree, List<RepositoryTreeNode> selectedNodes) {
        List<RepositoryTreeNode> mountNodes;
        if (selectedNodes.isEmpty()
                || (mountNodes = selectedNodes.stream().filter(node -> node.getInformation().getMount().equals(repositoryMount)).collect(Collectors.toList()))
                        .isEmpty())
            return Collections.emptyList();
        List<RepositoryPath> paths = mountNodes.stream()
                .map(node -> repositoryMount.getMountPath().relativize(node.getRepositoryPath()))
                .collect(Collectors.toList());
        List<Action> result = new ArrayList<>(actions.size());
        for (MountActionAdapter actionAdapter : actions) {
            if (!actionAdapter.condition.test(paths)) {
                continue;
            }
            result.add(new InternalMountRepositoryContextAction(tree, repositoryMount, actionAdapter.name, actionAdapter.action));
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        MountSpecificRepositoryTreeContextActionFactory that = (MountSpecificRepositoryTreeContextActionFactory) o;
        return Objects.equals(repositoryMount, that.repositoryMount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(repositoryMount);
    }

    private static class InternalMountRepositoryContextAction extends AbstractRepositoryContextAction<Map<String, Object>> {

        private final RepositoryMount mount;
        private final String i18nKey;
        private final ActionExecutable actionExecutable;

        public InternalMountRepositoryContextAction(RepositoryTree tree, RepositoryMount mount, String i18nKey, ActionExecutable actionExecutable) {
            super(tree, true, true, null, true, false, i18nKey);
            this.mount = mount;
            this.i18nKey = i18nKey;
            this.actionExecutable = actionExecutable;
            setEnabled(true);
        }

        @Override
        protected Map<String, Object> configureAction(List<RepositoryTreeNode> entries) {
            if (SwingTools.showConfirmDialog(i18nKey, ConfirmDialog.YES_NO_OPTION) != ConfirmDialog.YES_OPTION) {
                // cancel the action since the user did not confirm
                return null;
            }
            return Map.of();
        }

        @Override
        public void executeAction(RepositoryPath path, Map<String, Object> config, ProgressListener progressListener) {
            try (OperationProgressMonitorAdapter ignored = new OperationProgressMonitorAdapter(progressListener)) {
                actionExecutable.execute(Collections.singletonList(mount.getMountPath().relativize(path)), config);
            } catch (Exception e) {
                SwingTools.showSimpleErrorMessage(i18nKey, e, path.getFileName().toString(), e.getMessage());
            }
        }
    }
}
