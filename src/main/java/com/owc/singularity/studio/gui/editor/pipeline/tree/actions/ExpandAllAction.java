/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.tree.actions;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.owc.singularity.studio.gui.LoggedAbstractAction;
import com.owc.singularity.studio.gui.editor.pipeline.tree.OperatorTree;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa
 */
public class ExpandAllAction extends LoggedAbstractAction {

    private static final long serialVersionUID = -1779888083256884608L;

    private static final String ICON_NAME = "zoom_in.png";

    private final OperatorTree operatorTree;

    public ExpandAllAction(OperatorTree operatorTree, IconSize size) {
        super("Expand Tree", SwingTools.createIcon(size.getSize() + "/" + ICON_NAME));
        putValue(SHORT_DESCRIPTION, "Expands the complete operator tree");
        putValue(MNEMONIC_KEY, KeyEvent.VK_X);
        this.operatorTree = operatorTree;
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        this.operatorTree.expandAll();
    }
}
