/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.results;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;

import javax.swing.*;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.CleanupRequiringComponent;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.CloseAllResultsAction;
import com.owc.singularity.studio.gui.actions.CloseAllResultsExceptCurrentResultAction;
import com.owc.singularity.studio.gui.actions.StoreInRepositoryAction;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;
import com.vlsolutions.swing.docking.DockableActionCustomizer;


/**
 * Dockable containing a single result.
 *
 * @author Simon Fischer
 *
 */
public class ResultTab extends JPanel implements Dockable {

    private static final long serialVersionUID = 1L;

    public static final String DOCKKEY_PREFIX = "result_";

    /** the max length of dockkey names */
    private static final int MAX_DOCKNAME_LENGTH = 80;

    private Component label;
    private JPanel component;
    private final DockKey dockKey;
    private final String id;
    private IOObject resultObject = null;

    public ResultTab(String id) {

        setLayout(new BorderLayout());
        this.id = id;
        this.dockKey = new DockKey(id, "Result " + id);
        this.dockKey.setDockGroup(MainFrame.DOCK_GROUP_RESULTS);
        this.dockKey.setName(id);
        this.dockKey.setFloatEnabled(true);
        this.dockKey.setCloseEnabled(true);
        DockableActionCustomizer customizer = new DockableActionCustomizer() {

            @Override
            public void visitTabSelectorPopUp(JPopupMenu popUpMenu, Dockable dockable) {
                popUpMenu.add(new JMenuItem(new CloseAllResultsExceptCurrentResultAction(MainFrame.INSTANCE, dockKey.getKey())));
                popUpMenu.add(new JMenuItem(new CloseAllResultsAction(MainFrame.INSTANCE)));
                popUpMenu.addSeparator();
                popUpMenu.add(new JMenuItem(new StoreInRepositoryAction(resultObject)));
            }
        };
        customizer.setTabSelectorPopUpCustomizer(true); // enable tabbed dock custom popup menu
        // entries
        this.dockKey.setActionCustomizer(customizer);
        label = makeStandbyLabel();
        add(label, BorderLayout.CENTER);
    }

    public boolean hasResult() {
        return this.resultObject != null;
    }

    /**
     * Creates a component for this object and displays it. This method does not have to be called
     * on the EDT. It executes a time consuming task and should be called from a
     * {@link ProgressThread}.
     */
    public void showResult(final IOObject resultObject, Icon icon) {
        if (resultObject != null) {
            this.resultObject = resultObject;
        }
        SwingUtilities.invokeLater(() -> {
            if (label != null) {
                remove(label);
                label = null;
            }
            if (resultObject != null) {
                String source = getDisplayName(resultObject);

                // without this, the name could be too long and cut off the close button (and
                // even exit the screen)
                String name = SwingTools.getShortenedDisplayName(source, MAX_DOCKNAME_LENGTH);

                dockKey.setName(name);
                dockKey.setTooltip(source);
                label = makeStandbyLabel();
                add(label, BorderLayout.CENTER);
            } else {
                if (id.startsWith(DOCKKEY_PREFIX + "pipeline_")) {
                    String number = id.substring((DOCKKEY_PREFIX + "pipeline_").length());
                    label = new ResourceLabel("resulttab.cannot_be_restored_process_result", number);
                    dockKey.setName("Result #" + number);
                } else {
                    label = new ResourceLabel("resulttab.cannot_be_restored");
                    dockKey.setName("Result " + id);
                }
                ((JComponent) label).setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                add(label, BorderLayout.CENTER);
            }
            // remove old component
            if (component != null) {
                remove(component);
            }

            if (resultObject != null) {
                final JPanel newComponent = createComponent(resultObject);
                SwingUtilities.invokeLater(() -> {
                    if (label != null) {
                        remove(label);
                        label = null;
                    }
                    component = newComponent;
                    add(component, BorderLayout.CENTER);
                    dockKey.setIcon(SwingTools.addSideBySideIcon(icon,
                            (ImageIcon) component.getClientProperty(ResultDisplayTools.CLIENT_PROPERTY_SINGULARITY_RESULT_ICON)));
                });
            }
        });

    }


    private static String getDisplayName(IOObject resultObject) {
        RepositoryPath sourcePath = resultObject.getSourcePath();
        if (sourcePath == null)
            sourcePath = MainFrame.INSTANCE.getProcess().getPath();
        String sourceOperator = resultObject.getSourceOperator();
        String source = "";
        if (sourceOperator != null)
            source += resultObject.getSourcePort() + "@" + sourceOperator;
        if (sourcePath != null)
            source += " (" + sourcePath + ")";
        return source;
    }

    private static JComponent makeStandbyLabel() {
        Box labelBox = new Box(BoxLayout.Y_AXIS);
        labelBox.add(Box.createVerticalGlue());
        Box horizontalBox = Box.createHorizontalBox();
        horizontalBox.add(Box.createHorizontalGlue());
        horizontalBox.add(new ResourceLabel("resulttab.creating_display"));
        horizontalBox.add(Box.createHorizontalGlue());
        labelBox.add(horizontalBox);
        labelBox.add(Box.createVerticalGlue());
        return labelBox;
    }

    /**
     * Creates an appropriate name, appending a number to make names unique, and calls
     * {@link #createVisualizationComponent(IOObject, String)}.
     */
    private JPanel createComponent(IOObject resultObject) {
        final String resultName = IOObjectService.getName(resultObject.getClass());
        String usedResultName = resultObject.getName();
        if (usedResultName == null) {
            usedResultName = resultName;
        }
        this.resultObject = resultObject;
        return ResultDisplayTools.createVisualizationComponent(resultObject, id + ": " + usedResultName);
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return dockKey;
    }

    /**
     * Free up any resources held by this result tab. Also checks every child component for
     * instances of {@link CleanupRequiringComponent} and calls their clean-up methods.
     */
    public void freeResources() {
        if (component != null) {
            cleanUpRecursively(component);
            remove(component);
            component = null;
            resultObject = null;
        }
    }

    /**
     * Looks recursively for components implementing the {@link CleanupRequiringComponent} interface
     * and calls their cleanUp() method.
     *
     * @param component
     *            the component whose children should be searched
     */
    private void cleanUpRecursively(Container component) {
        for (Component child : component.getComponents()) {
            if (child instanceof CleanupRequiringComponent) {
                ((CleanupRequiringComponent) child).cleanUp();
            } else if (child instanceof Container) {
                cleanUpRecursively((Container) child);
            }
        }
    }

    /**
     * @return the panel which displays the actual result
     */
    public JPanel getResultViewComponent() {
        return component;
    }
}
