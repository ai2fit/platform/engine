package com.owc.singularity.studio.gui.repository.worker;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.swing.tree.TreeNode;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.model.FilteredRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingWorkerWithCallback;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.context.Context;
import io.opentelemetry.context.Scope;

public class CreateViewFromDataModelWorker extends SwingWorkerWithCallback<RepositoryTreeNode, RepositoryTreeNode> {

    private static final Tracer tracer = GlobalOpenTelemetry.getTracer("studio.repository");

    private final FilteredRepositoryTreeModel model;
    private final RepositoryTreeNode nodeToClone;

    private Span span;

    public CreateViewFromDataModelWorker(FilteredRepositoryTreeModel model, RepositoryTreeNode nodeToClone, Consumer<RepositoryTreeNode> callback) {
        super(callback);
        this.model = model;
        this.nodeToClone = nodeToClone;
    }

    @Override
    protected RepositoryTreeNode doInBackground() {
        Thread.currentThread().setName(Thread.currentThread().getName() + "/Creating view " + model);
        span = tracer.spanBuilder("create-view-from-data-model").startSpan();
        RepositoryTreeNode clonedNode = cloneTreeDepthFirst(nodeToClone);
        span.addEvent("background work finished");
        return clonedNode;
    }

    @Override
    protected void process(List<RepositoryTreeNode> chunks) {
        // runs on UI thread
        Span uiSpan = tracer.spanBuilder("edt-processing").setAttribute("chunks", chunks.size()).setParent(Context.current().with(span)).startSpan();
        try (Scope ignored = uiSpan.makeCurrent()) {
            // create a map of nodes that shall get children nodes
            Map<RepositoryPath, List<RepositoryTreeNode>> childrenMap = chunks.stream().filter(node -> {
                if (node.getRepositoryPath().getParent() != null) {
                    return true;
                } else {
                    // only the root has no parent and since we handle root separately
                    // we exclude it from the map
                    // assign the root node here so that we don't need to re-iterate the list
                    model.setRoot(node);
                    return false;
                }
            }).collect(Collectors.groupingBy(node -> node.getRepositoryPath().getParent()));
            for (RepositoryPath parentPath : childrenMap.keySet().stream().sorted().collect(Collectors.toList())) {
                RepositoryTreeNode parentNode = model.getNodeFor(parentPath);
                int[] addChildrenIndices = childrenMap.get(parentPath).stream().mapToInt(child -> model.addNode(parentNode, child)).toArray();
                // we add all children and update the UI once per parent node
                model.nodesWereInserted(parentNode, addChildrenIndices);
            }
        }
        uiSpan.end();
    }

    @Override
    protected void done() {
        span.end();
        super.done();
    }

    /**
     * Clones an {@link RepositoryTreeNode} and its subtree. Nodes that are rejected from
     * {@link FilteredRepositoryTreeModel#accept(RepositoryTreeNode)} are not cloned and their
     * subtree gets ignored.
     *
     * @param node
     *            the node to clone with subtree
     * @return cloned node or null if it's filtered out
     */
    private RepositoryTreeNode cloneTreeDepthFirst(RepositoryTreeNode node) {
        if (model.accept(node)) {
            RepositoryTreeNode clonedNode = node.clone();
            publish(clonedNode);
            Enumeration<TreeNode> children = node.children();
            while (children.hasMoreElements()) {
                cloneTreeDepthFirst((RepositoryTreeNode) children.nextElement());
            }
            return clonedNode;
        }
        return null;
    }
}
