package com.owc.singularity.studio.gui.repository;

import java.awt.Component;
import java.awt.Point;
import java.io.IOException;

import javax.swing.tree.TreePath;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.PipelineMetaData;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.PlaceholderRepositoryMount;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.metadata.MetaDataRendererFactoryRegistry;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNodeInformation;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow;

/**
 * @author Simon Fischer
 */
public class RepositoryTreeNodeToolTipProvider implements ToolTipWindow.TipProvider {

    private final RepositoryTree repositoryTree;

    public RepositoryTreeNodeToolTipProvider(RepositoryTree repositoryTree) {
        this.repositoryTree = repositoryTree;
    }

    @Override
    public String getTip(Object o) {
        if (!(o instanceof RepositoryTreeNode)) {
            return null;
        }
        RepositoryTreeNodeInformation entryInformation = ((RepositoryTreeNode) o).getInformation();
        if (entryInformation.isDirectory()) {
            if (entryInformation.getMount() instanceof PlaceholderRepositoryMount) {
                PlaceholderRepositoryMount mount = (PlaceholderRepositoryMount) entryInformation.getMount();
                StringBuilder tooltipText = new StringBuilder();
                tooltipText.append("<h2>").append(entryInformation.getFileName()).append("</h2>");
                tooltipText.append("<p>Mount Type: <span>").append(mount.getTemplateType()).append("</p>");
                tooltipText.append("<p>").append("Please ensure the mount configuration are valid before re-mounting the path").append("</p>");
                if (mount.getErrorMessage() != null) {
                    tooltipText.append("<h3>Error mounting: <i>").append(mount.getErrorMessage()).append("</i></h3>");
                    tooltipText.append("Error Stacktrace:<br/><pre>").append(mount.getErrorStacktrace()).append("</pre>");
                }
                return tooltipText.toString();
            }
            return "<h2>" + entryInformation.getFileName() + "</h2>";
        } else {
            try {
                Entry entry = ((RepositoryTreeNode) o).getEntry();
                if (entry.isInstanceOf(IOObject.class, ModuleService.getMajorClassLoader())) {
                    StringBuilder tip = new StringBuilder();
                    tip.append("<h3>").append(entry.getPath().getFileName().toString()).append("</h3>");
                    MetaData metaData = entry.loadMetaData(MetaData.class, ModuleService.getMajorClassLoader());
                    if (metaData != null) {
                        tip.append("<p>");
                        if (metaData instanceof ExampleSetMetaData) {
                            tip.append(((ExampleSetMetaData) metaData).getShortDescription());
                        } else {
                            tip.append(metaData.getDescription());
                        }
                        tip.append("</p>");
                    }
                    return tip.toString();
                } else if (entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
                    PipelineMetaData pipelineMetaData = entry.loadMetaData(PipelineMetaData.class, ModuleService.getMajorClassLoader());

                    StringBuilder tip = new StringBuilder();
                    tip.append("<h3>").append(entry.getPath().getFileName().toString()).append("</h3>");
                    tip.append(pipelineMetaData.getPipelineTypeName().replaceAll(".+\\.", ""));
                    if (pipelineMetaData.getTags() != null) {
                        tip.append("<h4>Tags: ").append(Tools.escapeHTML(pipelineMetaData.getTags())).append("</h4>");
                    }
                    if (pipelineMetaData.getDocumentation() != null) {
                        Parser parser = Parser.builder().build();
                        Node document = parser.parse(pipelineMetaData.getDocumentation());
                        HtmlRenderer renderer = HtmlRenderer.builder().build();
                        tip.append(renderer.render(document));
                    }
                    return tip.toString();
                } else {
                    return "<h3>" + entry.getPath().getFileName().toString() + "</h3>" + entry.getContentType().getTypeName();
                }
            } catch (IOException | ClassNotFoundException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.repository.tooltip.ToolTipProviderHelper.fetching_meta_data_for_tool_error", e);
                return null;
            }
        }
    }

    @Override
    public Object getIdUnder(Point point) {
        TreePath path = repositoryTree.getPathForLocation((int) point.getX(), (int) point.getY());
        return path == null ? null : path.getLastPathComponent();
    }

    @Override
    public Component getCustomComponent(Object o) {
        if (!(o instanceof RepositoryTreeNode)) {
            return null;
        }
        try {
            Entry entry = ((RepositoryTreeNode) o).getEntry();
            if (entry.isInstanceOf(IOObject.class, ModuleService.getMajorClassLoader())) {
                MetaData metaData = entry.loadMetaData(MetaData.class, ModuleService.getMajorClassLoader());
                return MetaDataRendererFactoryRegistry.getInstance().createRenderer(metaData);
            }
        } catch (Exception ex) {
            LogService.getRoot()
                    .warn("com.owc.singularity.studio.gui.repository.tooltip.ToolTipProviderHelper.retrieving_meta_data_error",
                            ((RepositoryTreeNode) o).getRepositoryPath(), ex, ex);

        }
        return null;
    }
}
