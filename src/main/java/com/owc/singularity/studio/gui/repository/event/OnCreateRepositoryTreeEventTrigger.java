package com.owc.singularity.studio.gui.repository.event;

import com.owc.singularity.repository.RepositoryPath;

public class OnCreateRepositoryTreeEventTrigger extends TimeoutRepositoryTreeEventTrigger {

    private final RepositoryPath createdPath;
    private boolean found = false;

    public OnCreateRepositoryTreeEventTrigger(RepositoryPath createdPath) {
        this(createdPath, 5_000);
    }

    public OnCreateRepositoryTreeEventTrigger(RepositoryPath createdPath, long timeoutMillis) {
        super(timeoutMillis);
        this.createdPath = createdPath;
    }

    @Override
    public boolean shouldFire(RepositoryTreeUpdateType event, RepositoryPath... affectedPaths) {
        if (event == RepositoryTreeUpdateType.Insert) {
            for (RepositoryPath path : affectedPaths) {
                if (createdPath.equals(path))
                    return found = true;
            }
        }
        return false;
    }

    @Override
    public boolean shouldBeRemoved() {
        return found || super.shouldBeRemoved();
    }
}
