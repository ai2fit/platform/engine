/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.operators;


import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.OperatorService.OperatorServiceListener;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.CompositeDummyOperator;
import com.owc.singularity.engine.operator.DummyOperator;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.tools.AbstractOperatorDescriptionTreeNode;
import com.owc.singularity.engine.tools.OperatorDescriptionTreeRoot;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.studio.documentation.OperatorDocumentationService;


/**
 * This is the model for the group selection tree in the new operator editor panel.
 *
 * @author Ingo Mierswa, Tobias Malbrecht, Sebastian Land
 */
public class OperatorGroupTreeModel implements TreeModel, OperatorServiceListener {

    /** Regular expression used as delimiter for search terms. */
    private static final String SEARCH_TERM_DELIMITER = "\\s+";

    /** Consider only the first {@value} search terms. */
    private static final int MAX_SEARCH_TERMS = 5;

    /** Compares operator descriptions based on their usage statistics. */
    private static final class UsageStatsComparator implements Comparator<OperatorDescription>, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public int compare(OperatorDescription op1, OperatorDescription op2) {
            int usageCount1 = (int) ActionStatisticsCollector.getInstance()
                    .getData(ActionStatisticsCollector.TYPE_OPERATOR, op1.getFullyQuallifiedKey(), ActionStatisticsCollector.OPERATOR_EVENT_EXECUTION);
            int usageCount2 = (int) ActionStatisticsCollector.getInstance()
                    .getData(ActionStatisticsCollector.TYPE_OPERATOR, op2.getFullyQuallifiedKey(), ActionStatisticsCollector.OPERATOR_EVENT_EXECUTION);
            return usageCount2 - usageCount1;
        }
    }

    private static final OperatorDescriptionTreeRoot completeTree = new OperatorDescriptionTreeRoot();

    private AbstractOperatorDescriptionTreeNode displayedTree;

    private String filter = null;

    /** The list of all tree model listeners. */
    private final List<TreeModelListener> treeModelListeners = new LinkedList<>();

    private boolean sortByUsage = false;

    /** sorts by <priority> key of operators */
    private final Comparator<OperatorDescription> sortingComparator = Comparator.comparing(OperatorDescription::getKey);
    private final Comparator<String> groupSortingComparator = Comparator.naturalOrder();
    /** sorts by local usage count of operators */
    private final Comparator<OperatorDescription> usageComparator = new UsageStatsComparator();

    public OperatorGroupTreeModel() {
        this.displayedTree = getFilledCompleteTree();
        OperatorService.addOperatorServiceListener(this);
        updateTree();
    }

    public static OperatorDescriptionTreeRoot getFilledCompleteTree() {
        if (completeTree.getOperatorDescriptionCount() == 0) {
            for (String operatorKey : OperatorService.getOperatorKeys()) {
                OperatorDescription operatorDescription = OperatorService.getOperatorDescription(operatorKey);
                if (AbstractRootOperator.class.isAssignableFrom(operatorDescription.getOperatorClass())
                        || operatorDescription.getOperatorClass() == CompositeDummyOperator.class
                        || operatorDescription.getOperatorClass() == DummyOperator.class)
                    continue;
                completeTree.addOperator(operatorDescription);
            }
        }
        return completeTree;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.add(l);
    }

    public boolean contains(Object o) {
        return contains(this.getRoot(), o);
    }

    private boolean contains(Object start, Object o) {
        if (o.equals(start)) {
            return true;
        } else {
            for (int i = 0; i < getChildCount(start); i++) {
                if (contains(getChild(start, i), o)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof AbstractOperatorDescriptionTreeNode) {
            AbstractOperatorDescriptionTreeNode tree = (AbstractOperatorDescriptionTreeNode) parent;
            int numSubGroups = tree.getSubGroups().size();
            if (index < numSubGroups) {
                return tree.getSubGroup(index);
            } else {
                return tree.getOperatorDescriptions().get(index - numSubGroups);
            }
        } else {
            return null;
        }

    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof AbstractOperatorDescriptionTreeNode) {
            AbstractOperatorDescriptionTreeNode tree = (AbstractOperatorDescriptionTreeNode) parent;
            return tree.getSubGroups().size() + tree.getOperatorDescriptions().size();
        } else {
            return 0;
        }
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        AbstractOperatorDescriptionTreeNode tree = (AbstractOperatorDescriptionTreeNode) parent;
        if (child instanceof AbstractOperatorDescriptionTreeNode) {
            return tree.getIndexOfSubGroup((AbstractOperatorDescriptionTreeNode) child);
        } else {
            return tree.getOperatorDescriptions().indexOf(child) + tree.getSubGroups().size();
        }
    }

    @Override
    public AbstractOperatorDescriptionTreeNode getRoot() {
        return displayedTree;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof AbstractOperatorDescriptionTreeNode);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
    }

    /** Will be invoked after editing changes of nodes. */
    @Override
    public void valueForPathChanged(TreePath path, Object node) {
        fireTreeChanged(node, path);
    }

    private void fireTreeChanged(Object source, TreePath path) {
        for (TreeModelListener treeModelListener : treeModelListeners) {
            treeModelListener.treeStructureChanged(new TreeModelEvent(source, path));
        }
    }

    private void fireCompleteTreeChanged(Object source) {
        for (TreeModelListener treeModelListener : treeModelListeners) {
            treeModelListener.treeStructureChanged(new TreeModelEvent(this, new TreePath(getRoot())));
        }
    }

    public List<TreePath> applyFilter(String filter) {
        this.filter = filter;
        return updateTree();
    }

    private AbstractOperatorDescriptionTreeNode restoreTree() {
        return completeTree.clone();
    }

    private List<TreePath> updateTree() {
        AbstractOperatorDescriptionTreeNode filteredTree = restoreTree();
        List<TreePath> expandedPaths = new LinkedList<>();
        if (filter != null && !filter.isBlank()) {
            OperatorFilter[] filters = new OperatorFilter[] { new FullTextFilter(filter), new CamelCaseFilter(filter) };
            int hits = removeFilteredInstances(filters, filteredTree, expandedPaths, new TreePath(getRoot()));

            // If we still couldn't find anything, restore the tree and try the typo search
            if (hits == 0) {
                filteredTree = restoreTree();
                OperatorFilter cctFilter = new CamelCaseTypoFilter(filter);
                removeFilteredInstances(new OperatorFilter[] { cctFilter }, filteredTree, expandedPaths, new TreePath(getRoot()));
            }
        }

        this.displayedTree = filteredTree;
        if (!sortByUsage)
            displayedTree.sort(sortingComparator, groupSortingComparator);
        else
            displayedTree.sort(usageComparator, groupSortingComparator);


        fireCompleteTreeChanged(this);
        return expandedPaths;
    }

    /**
     * Recursively deletes nodes from the filteredTree when neither the name of the node nor the
     * name of one of its children or parents matches the filter. Stores paths to nodes that match
     * the filter (not containing the node itself) in expandedPaths.
     *
     * @param filter
     *            filter for which names to take
     * @param filteredTree
     *            the tree to filter
     * @param expandedPaths
     *            list of paths that should be expanded when the tree is displayed
     * @param path
     *            the current path
     * @return number of hits below the current path
     */
    private int removeFilteredInstances(OperatorFilter[] filters, AbstractOperatorDescriptionTreeNode filteredTree, List<TreePath> expandedPaths,
            TreePath path) {
        int hits = 0;
        Iterator<? extends AbstractOperatorDescriptionTreeNode> g = filteredTree.getSubGroups().iterator();
        while (g.hasNext()) {
            AbstractOperatorDescriptionTreeNode child = g.next();
            boolean matches = false;
            for (OperatorFilter filter : filters)
                matches |= filter.matches(child.getName());
            if (matches) {
                expandedPaths.add(path);
            }

            hits += removeFilteredInstances(filters, child, expandedPaths, path.pathByAddingChild(child));
            if (child.getOperatorDescriptionCount() == 0 && !matches) {
                g.remove();
            }

        }

        // remove non matching operator descriptions if the group does not match, keep all in
        // matching group, count hits even if matching
        boolean groupMatches = false;
        for (OperatorFilter filter : filters)
            groupMatches |= filter.matches(filteredTree.getName());
        Iterator<OperatorDescription> o = filteredTree.getOperatorDescriptions().iterator();
        while (o.hasNext()) {
            OperatorDescription description = o.next();
            boolean matches = false;
            for (OperatorFilter filter : filters)
                matches |= filter.matches(description.getName()) || filter.matches(description.getShortName())
                        || OperatorDocumentationService.getTags(description).stream().anyMatch(filter);

            if (!matches && !groupMatches) {
                o.remove();
            } else {
                hits++;
            }
        }

        if (hits > 0) {
            expandedPaths.add(path);
        }
        return hits;
    }

    public void setSortByUsage(boolean sort) {
        if (sort != this.sortByUsage) {
            this.sortByUsage = sort;
            updateTree();
        }
    }

    @Override
    public void operatorRegistered(OperatorDescription description) {
        updateTree();
    }

    @Override
    public void operatorUnregistered(OperatorDescription description) {
        updateTree();
    }
}
