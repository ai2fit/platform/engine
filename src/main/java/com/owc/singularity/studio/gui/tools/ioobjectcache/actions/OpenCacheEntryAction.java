/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.tools.ioobjectcache.actions;


import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Objects;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.cache.IOObjectMap;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.results.ResultDisplay;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This action opens an entry of the specified {@link IOObjectMap} the results perspective.
 *
 * @author Michael Knopf
 */
public class OpenCacheEntryAction extends ResourceAction {

    private static final long serialVersionUID = 1L;

    private final IOObjectMap map;
    private final String key;

    /**
     * Creates a new {@link OpenCacheEntryAction} for the specified {@link IOObjectMap} and key.
     *
     * @param map
     *            The corresponding {@link IOObjectMap};
     * @param key
     *            The key of the {@link IOObject} to be displayed.
     * @throws NullPointerException
     *             If one of the parameters is <code>null</code>.
     */
    public OpenCacheEntryAction(IOObjectMap map, String key) {
        super(true, "ioobject_viewer.open", key);
        Objects.requireNonNull(map);
        Objects.requireNonNull(key);
        this.map = map;
        this.key = key;
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        ResultDisplay display = MainFrame.INSTANCE.getResultDisplay();
        IOObject object = map.get(key);
        if (object != null) {
            display.showData(Collections.singletonList(object), key, SwingTools.createIcon("16/check.png"));
        } else {
            LogService.getRoot().warn("Could not open '" + key + "', entry does not exist.");
        }
    }

}
