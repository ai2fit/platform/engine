/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.renderer;


import java.awt.*;


/**
 * This is the renderer interface. A renderer is a visualization component for all types of objects.
 * <p>
 * <strong>Note:</strong> If your renderer contains components that require clean-up or should
 * interrupt long-running calculations if the visualization component is no longer needed (i.e. the
 * result tab has been closed), these components should implement the
 * {@link com.owc.singularity.studio.gui.CleanupRequiringComponent} interface. The component
 * hierarchy is traversed and all implementations of the interface are notified that the renderer is
 * no longer needed.
 * </p>
 *
 * @author Ingo Mierswa
 */
public interface Renderer {

    String getName();

    Component getVisualizationComponent(Object renderable);
}
