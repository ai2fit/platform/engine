/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.new_plotter;


import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.new_plotter.configuration.PlotConfiguration;
import com.owc.singularity.studio.gui.new_plotter.listener.MasterOfDesasterListener;


/**
 * This class can be used to register errors which occur during plotting and cannot be caught during
 * the configuration of the plot.
 * 
 * @author Marius Helf
 * 
 */

public class MasterOfDesaster {

    private List<ConfigurationChangeResponse> configChangeResponseList = new LinkedList<ConfigurationChangeResponse>();
    private List<WeakReference<MasterOfDesasterListener>> listeners = new LinkedList<WeakReference<MasterOfDesasterListener>>();
    private boolean calculating = false;

    private static final URL OKAY_ICON_URL = Tools.getResource("icons/16/ok.png");
    private static final URL CALCULATING_ICON_URL = Tools.getResource("icons/16/calculator.png");
    private static final String CALCULATE_LABEL = I18N.getGUILabel("plotter.master_of_desaster.calculating.label");
    private static final String NO_PROBLEMS_LABEL = I18N.getGUILabel("plotter.master_of_desaster.no_problems_detected.label");

    public void clearAll() {
        configChangeResponseList.clear();
        fireChanged();
    }

    private void removeEmptyResponses() {
        List<ConfigurationChangeResponse> copy = new LinkedList<ConfigurationChangeResponse>(configChangeResponseList);
        for (ConfigurationChangeResponse change : copy) {
            if (change.isEmpty()) {
                configChangeResponseList.remove(change);
            }
        }
    }

    public void clearWarnings() {
        for (ConfigurationChangeResponse change : configChangeResponseList) {
            change.clearWarnings();
        }
        removeEmptyResponses();
        fireChanged();
    }

    public void clearErrors() {
        for (ConfigurationChangeResponse change : configChangeResponseList) {
            change.clearErrors();
        }
        removeEmptyResponses();
        fireChanged();
    }

    public void removeConfigurationChangeResponse(ConfigurationChangeResponse response) {
        configChangeResponseList.remove(response);
        fireChanged();
    }

    public void registerConfigurationChangeResponse(ConfigurationChangeResponse error) {
        if (calculating) {
            calculating = false;
        }
        configChangeResponseList.add(error);
        fireChanged();
    }

    public List<ConfigurationChangeResponse> getConfigurationChangeResponses() {
        return configChangeResponseList;
    }

    private void fireChanged() {

        // copy listeners
        List<WeakReference<MasterOfDesasterListener>> listeners = new LinkedList<>(this.listeners);

        Iterator<WeakReference<MasterOfDesasterListener>> it = listeners.iterator();
        while (it.hasNext()) {
            MasterOfDesasterListener listener = it.next().get();
            if (listener == null) {
                it.remove();
            } else {
                listener.masterOfDesasterChanged(this);
            }
        }
    }

    public void addListener(MasterOfDesasterListener l) {
        listeners.add(new WeakReference<MasterOfDesasterListener>(l));
    }

    public void removeListener(MasterOfDesasterListener l) {
        Iterator<WeakReference<MasterOfDesasterListener>> it = listeners.iterator();
        while (it.hasNext()) {
            MasterOfDesasterListener listener = it.next().get();
            if (listener == null || listener == l) {
                it.remove();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (ConfigurationChangeResponse response : configChangeResponseList) {
            if (response != null) {
                builder.append(response.toString());
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    /**
     * @return
     */
    public String toHtmlString(int numberOfRows) {
        StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        if (calculating) {
            builder.append("<div style=\"color:#0066CC;\">");
            builder.append("<img valign=\"middle\" style=\"vertical-align:middle;\" src=\"").append(CALCULATING_ICON_URL).append("\"/>&nbsp;");
            builder.append(CALCULATE_LABEL);
            builder.append("</div>");
            return builder.toString();
        }
        if (!configChangeResponseList.isEmpty()) {
            for (ConfigurationChangeResponse response : configChangeResponseList) {
                if (response != null) {
                    builder.append(response.toHtmlString());
                }
            }
        } else {
            builder.append("<div style=\"color:#000000;\">");
            builder.append("<img valign=\"middle\" style=\"vertical-align:middle;\" src=\"").append(OKAY_ICON_URL).append("\"/>&nbsp;");
            builder.append(NO_PROBLEMS_LABEL);
            builder.append("</div>");
        }
        int maxRows = PlotConfiguration.getMaxAllowedValueCount();
        if (maxRows < numberOfRows) {
            builder.append("<div style=\"color:#cc6600;\">");
            builder.append("Currently displaying ")
                    .append(String.format("%,d", maxRows))
                    .append(" of ")
                    .append(String.format("%,d", numberOfRows))
                    .append(" examples. To adjust this setting navigate to Settings -> Preferences -> User-Interface -> Charts -> Max number of rows in charts");
            builder.append("</div>");
            builder.append("</html>");
        }
        return builder.toString();
    }

    /**
     * 
     */
    public void setCalculating(boolean calculating) {
        this.calculating = calculating;
        fireChanged();
    }
}
