/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository;


import java.util.*;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * A storage for saving and restoring expansion states and selection paths of the repository tree.
 *
 * @author Hatem Hamad
 *
 */
public class RepositoryTreeState {

    private final HashSet<String> expandedNodes = new LinkedHashSet<>();

    /** Saved selected nodes */
    private final HashSet<String> selectedNodes = new LinkedHashSet<>();

    /**
     * Saves the currently selected paths and saves all expanded repositories and nodes.
     *
     * @param tree
     *            The related tree, containing the path(s)
     */
    public void save(JTree tree) {
        Object root = tree.getModel().getRoot();
        if (root != null) {
            Enumeration<TreePath> expandedDescendants = tree.getExpandedDescendants(new TreePath(root));
            while (expandedDescendants.hasMoreElements()) {
                TreePath expandedTreePath = expandedDescendants.nextElement();
                Object lastPathComponent = expandedTreePath.getLastPathComponent();
                if (lastPathComponent instanceof RepositoryTreeNode) {
                    expandedNodes.add(((RepositoryTreeNode) lastPathComponent).getRepositoryPathStringWithoutVersion());
                }
            }
            TreePath[] selectionPaths = tree.getSelectionPaths();
            if (selectionPaths != null) {
                for (TreePath treePath : selectionPaths) {
                    Object lastPathComponent = treePath.getLastPathComponent();
                    if (lastPathComponent instanceof RepositoryTreeNode) {
                        selectedNodes.add(((RepositoryTreeNode) lastPathComponent).getRepositoryPathStringWithoutVersion());
                    }
                }
            }
        } else {
            for (int i = 0; i < tree.getRowCount(); i++) {
                TreePath path = tree.getPathForRow(i);
                boolean isExpanded = tree.isExpanded(path);
                boolean isSelected = tree.isPathSelected(path);
                Object entryObject = path.getLastPathComponent();
                if ((isExpanded || isSelected) && entryObject instanceof RepositoryTreeNode) {
                    RepositoryTreeNode entry = (RepositoryTreeNode) entryObject;
                    String absoluteLocation = entry.getRepositoryPathStringWithoutVersion();
                    if (isExpanded) {
                        expandedNodes.add(absoluteLocation);
                    }
                    if (isSelected) {
                        selectedNodes.add(absoluteLocation);
                    }
                }
            }
        }
    }

    /**
     * Expands all repositories and nodes, which have been saved before. Restores selected paths,
     * which have been saved proviously.
     *
     * @param tree
     *            The related tree, containing the path(s)
     */
    public void restore(JTree tree) {
        List<TreePath> selectedPathList = new ArrayList<>();
        Set<String> locationToSearchFor = new LinkedHashSet<>(expandedNodes);
        locationToSearchFor.addAll(selectedNodes);
        for (int i = 0; i < tree.getRowCount() && !locationToSearchFor.isEmpty(); i++) {
            TreePath path = tree.getPathForRow(i);
            // sanity check for concurrent refreshes
            if (path != null) {
                Object entryObject = path.getLastPathComponent();
                if (entryObject instanceof RepositoryTreeNode) {
                    RepositoryTreeNode entry = (RepositoryTreeNode) entryObject;
                    String absoluteLocation = entry.getRepositoryPathStringWithoutVersion();
                    if (expandedNodes.contains(absoluteLocation)) {
                        locationToSearchFor.remove(absoluteLocation);
                        tree.expandPath(path);
                    }
                    if (selectedNodes.contains(absoluteLocation)) {
                        locationToSearchFor.remove(absoluteLocation);
                        selectedPathList.add(path);
                    }
                }
            }
        }
        if (!selectedPathList.isEmpty()) {
            tree.setSelectionPaths(selectedPathList.toArray(new TreePath[0]));
        }
    }


}
