/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline;


import java.awt.*;

import javax.swing.*;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 *
 * @author Simon Fischer
 */
public class ProcessOverviewDockable implements Dockable, FocusProcessChangeEventListener {

    private final JPanel container;
    private ProcessOverviewPanel processOverviewPanel;

    public ProcessOverviewDockable() {
        container = new JPanel(new BorderLayout());
    }

    public static final String OVERVIEW_DOCK_KEY = "overview";
    private final DockKey DOCK_KEY = new ResourceDockKey(OVERVIEW_DOCK_KEY);
    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    @Override
    public Component getComponent() {
        return container;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    @Override
    public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
        if (currentlyFocusedPanel == null) {
            container.remove(processOverviewPanel);
            container.setEnabled(false);
            processOverviewPanel = null;
        } else {
            if (processOverviewPanel != null) {
                if (oldPanel != null) {
                    oldPanel.getProcessEditor().removeProcessEditorEventListener(processOverviewPanel);
                    oldPanel.getViewPort().addChangeListener(processOverviewPanel);
                }
                container.remove(processOverviewPanel);
            }
            ProcessEditorPanel processEditor = currentlyFocusedPanel.getProcessEditor();
            processOverviewPanel = new ProcessOverviewPanel(processEditor);
            processEditor.addProcessEditorEventListener(processOverviewPanel);
            processEditor.addProcessInteractionListener(processOverviewPanel);
            currentlyFocusedPanel.getViewPort().addChangeListener(processOverviewPanel);
            container.add(processOverviewPanel, BorderLayout.CENTER);
            container.setEnabled(true);
            SwingUtilities.invokeLater(container::repaint);
        }
    }
}
