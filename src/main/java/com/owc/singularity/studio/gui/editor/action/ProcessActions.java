/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.action;


import static com.owc.singularity.studio.gui.ConditionalAction.updateAll;

import java.awt.event.ActionEvent;
import java.util.*;

import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.operator.AbstractRootOperator;
import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.BreakpointListener;
import com.owc.singularity.engine.pipeline.PipelineState;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.ConditionalAction.Condition;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.OperatorActionContext;
import com.owc.singularity.studio.gui.actions.ToggleAction;
import com.owc.singularity.studio.gui.dnd.OperatorTransferHandler;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessUserInteractionListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.view.actions.ArrangeOperatorsAction;
import com.owc.singularity.studio.gui.editor.pipeline.view.actions.AutoFitAction;
import com.owc.singularity.studio.gui.operatormenu.InsertOperatorMenu;
import com.owc.singularity.studio.gui.operatormenu.OperatorMenu;
import com.owc.singularity.studio.gui.operatormenu.ReplaceOperatorMenu;
import com.owc.singularity.studio.gui.tools.EditBlockingProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * A process editor that enables/disables actions depending on the selection of operators.
 *
 * @author Simon Fischer, Tobias Malbrecht
 */
public class ProcessActions implements ProcessEditorEventListener, ProcessUserInteractionListener {

    public static final Action CUT_ACTION = new CutCopyPasteDeleteAction("cut", "cut");
    public static final Action COPY_ACTION = new CutCopyPasteDeleteAction("copy", "copy");
    public static final Action PASTE_ACTION = new CutCopyPasteDeleteAction("paste", "paste");
    public static final Action DELETE_ACTION = new CutCopyPasteDeleteAction("delete", "delete");
    public static final Action ARRANGE_OPERATORS_ACTION = new ArrangeOperatorsAction();
    public static final Action AUTO_FIT_ACTION = new AutoFitAction();
    public static final Action INFO_OPERATOR_ACTION = new InfoOperatorAction() {

        private static final long serialVersionUID = 6758272768665592429L;

        @Override
        protected Operator getOperator() {
            ProcessPanel mainProcessPanel = MainFrame.INSTANCE.getMainProcessPanel();
            if (mainProcessPanel == null)
                return null;
            return mainProcessPanel.getProcessEditor().getSelectedOperator();
        }
    };
    private static final OperatorMenu NEW_OPERATOR_MENU = new InsertOperatorMenu();

    private static final OperatorMenu REPLACE_OPERATOR_MENU = new ReplaceOperatorMenu(false);

    private static final OperatorMenu REPLACE_OPERATORCHAIN_MENU = new ReplaceOperatorMenu(true);

    public static final ToggleActivationItem TOGGLE_ACTIVATION_ITEM = new ToggleActivationItem();

    public static final ResourceAction RENAME_OPERATOR_ACTION = new RenameOperatorAction();

    public static final DeleteOperatorAction DELETE_OPERATOR_ACTION = new DeleteOperatorAction();

    public static final ToggleBreakpointItem[] TOGGLE_BREAKPOINT = { new ToggleBreakpointItem(BreakpointListener.BREAKPOINT_BEFORE),
            new ToggleBreakpointItem(BreakpointListener.BREAKPOINT_AFTER) };

    public static final ToggleAllBreakpointsItem TOGGLE_ALL_BREAKPOINTS = new ToggleAllBreakpointsItem();

    public static final RemoveAllBreakpointsAction REMOVE_ALL_BREAKPOINTS = new RemoveAllBreakpointsAction();

    private final Action SHOW_PROBLEM_ACTION = new ResourceAction(true, "show_potential_problem") {

        private static final long serialVersionUID = -1260942717363137733L;

        {
            setCondition(Condition.OPERATOR_SELECTED, ConditionReaction.MANDATORY);
        }

        @Override
        public void loggedActionPerformed(ActionEvent e) {
            MainFrame.INSTANCE.getMainProcessPanel().getOperatorWarningHandler().showOperatorWarning(getFirstSelectedOperator());
        }
    };
    private final ProcessEditorPanel renderer;

    private List<Operator> selection;

    private AbstractPipeline process;

    private final OperatorActionContext context = new OperatorActionContext() {

        @Override
        public List<Operator> getOperators() {
            return Collections.unmodifiableList(getSelectedOperators());
        }

        @Override
        public Operator getDisplayedChain() {
            return renderer.getModel().getDisplayedChain();
        }

        @Override
        public boolean isReadOnly() {
            return ProcessActions.this.isProcessReadOnly();
        }

    };

    private final PipelineLifeCycleEventListener eventListener = new PipelineLifeCycleEventListener() {

        @Override
        public String getIdentifier() {
            return "process_actions";
        }

        @Override
        public void onBeforePipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            enableActions();
            MainFrame.INSTANCE.RUN_ACTION.setState(process.getState());
        };

        @Override
        public void onAfterPipelinePause(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            enableActions();
            MainFrame.INSTANCE.RUN_ACTION.setState(process.getState());
        }

        @Override
        public void onAfterPipelineResume(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            enableActions();
            MainFrame.INSTANCE.RUN_ACTION.setState(process.getState());
        }

        @Override
        public void onAfterPipelineFinish(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            enableActions();
            MainFrame.INSTANCE.RUN_ACTION.setState(process.getState());
        }

        @Override
        public void onAfterPipelineStop(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            enableActions();
            MainFrame.INSTANCE.RUN_ACTION.setState(process.getState());
        }
    };


    private boolean processReadOnly;

    public ProcessActions(ProcessEditorPanel renderer) {
        this.renderer = renderer;
    }

    /** Creates a new popup menu for the selected operator. */
    public void addToOperatorPopupMenu(JPopupMenu menu, Action... furtherActions) {
        final Operator op = getFirstSelectedOperator();
        final boolean singleSelection = getSelectedOperators().size() == 1;

        OperatorChain displayedChain = renderer.getModel().getDisplayedChain();
        boolean editable = !isProcessReadOnly();
        if (op != null) {
            if (!singleSelection && !(op instanceof AbstractRootOperator) && op.getParent() != null) {
                // enable / disable operator
                JMenuItem multipleActivationItem = TOGGLE_ACTIVATION_ITEM.createMultipleActivationItem(getSelectedOperators());
                multipleActivationItem.setEnabled(editable);
                menu.add(multipleActivationItem);
            } else {
                if (singleSelection && displayedChain != op) {
                    menu.add(INFO_OPERATOR_ACTION);
                    menu.add(TOGGLE_ACTIVATION_ITEM.createMenuItem());
                    menu.add(RENAME_OPERATOR_ACTION);
                    if (!op.getErrorList().isEmpty()) {
                        menu.add(SHOW_PROBLEM_ACTION);
                    }
                    if (editable) {
                        menu.addSeparator();
                        if (op instanceof OperatorChain && !((OperatorChain) op).getAllInnerOperators().isEmpty()) {
                            menu.add(REPLACE_OPERATORCHAIN_MENU);
                        } else {
                            menu.add(REPLACE_OPERATOR_MENU);
                        }
                    }
                }
            }

            // add new operator and building block menu
            if (editable && displayedChain == op) {
                menu.add(NEW_OPERATOR_MENU);
            }
        }

        // populate menu with registered actions (if any)
        synchronized (DefaultProcessActionFactory.getInstance()) {
            for (Action action : DefaultProcessActionFactory.getInstance().getActions()) {
                if (action.isEnabled())
                    menu.add(action);
            }
        }

        menu.addSeparator();
        boolean enableCutCopy = displayedChain != op;
        OperatorTransferHandler.installMenuItems(menu, enableCutCopy && editable, enableCutCopy, enableCutCopy && editable, enableCutCopy && editable);

        // add further actions here
        if (furtherActions.length > 0) {
            menu.addSeparator();
            for (Action a : furtherActions) {
                if (a == null) {
                    continue;
                }

                if (a instanceof ToggleAction) {
                    menu.add(((ToggleAction) a).createMenuItem());
                } else {
                    menu.add(a);
                }
            }
        }
        if (editable) {
            menu.addSeparator();
            if (op != null && !(op instanceof AbstractRootOperator) && singleSelection) {
                for (ToggleBreakpointItem toggleBreakpointItem : TOGGLE_BREAKPOINT) {
                    JMenuItem item = toggleBreakpointItem.createMenuItem();
                    menu.add(item);
                }
            }
            menu.add(REMOVE_ALL_BREAKPOINTS);
            menu.add(TOGGLE_ALL_BREAKPOINTS);
        }
    }

    public Operator getFirstSelectedOperator() {
        if (selection != null && !selection.isEmpty()) {
            return selection.get(0);
        } else {
            return null;
        }
    }

    public List<Operator> getSelectedOperators() {
        return selection;
    }

    /**
     * Enables and disables all actions according to the current state (process running, operator
     * selected...
     */
    public void enableActions() {
        SwingTools.invokeLater(this::enableActionsNow);
        updateCheckboxStates();
    }

    private synchronized void enableActionsNow() {
        EnumMap<Condition, Boolean> currentStates = getCurrentStates();
        updateAll(currentStates);
        updateCheckboxStates();
    }

    private final EnumMap<Condition, Boolean> currentStates = new EnumMap<>(Condition.class);

    public EnumMap<Condition, Boolean> getCurrentStates() {
        for (Condition value : Condition.values()) {
            currentStates.put(value, false);
        }
        Operator op = getFirstSelectedOperator();
        if (op != null) {
            currentStates.put(Condition.OPERATOR_SELECTED, true);
            if (op instanceof OperatorChain) {
                currentStates.put(Condition.OPERATOR_CHAIN_SELECTED, true);
            }
            OperatorChain parent = op.getParent();
            if (parent == null) {
                currentStates.put(Condition.ROOT_SELECTED, true);
            } else {
                currentStates.put(Condition.PARENT_ENABLED, parent.isEnabled());
                if (op.getExecutionUnit().getNumberOfOperators() > 1) {
                    currentStates.put(Condition.SIBLINGS_EXIST, true);
                }
            }
        } else {
            currentStates.put(Condition.OPERATOR_SELECTED, false);
            currentStates.put(Condition.OPERATOR_CHAIN_SELECTED, false);
            currentStates.put(Condition.ROOT_SELECTED, false);
            currentStates.put(Condition.PARENT_ENABLED, false);
            currentStates.put(Condition.SIBLINGS_EXIST, false);
        }

        if (process != null) {
            PipelineState processState = process.getState();
            currentStates.put(Condition.PROCESS_STOPPED, processState == PipelineState.STOPPED);
            currentStates.put(Condition.PROCESS_PAUSED, processState == PipelineState.PAUSED);
            currentStates.put(Condition.PROCESS_RUNNING, processState == PipelineState.RUNNING);
            currentStates.put(Condition.EDIT_IN_PROGRESS, EditBlockingProgressThread.isEditing());
            currentStates.put(Condition.PROCESS_SAVED, process.hasSaveDestination());
            currentStates.put(Condition.PROCESS_HAS_REPOSITORY_LOCATION, process.getPath() != null);
            currentStates.put(Condition.PROCESS_RENDERER_IS_VISIBLE, true);
            currentStates.put(Condition.PROCESS_RENDERER_HAS_UNDO_STEPS, renderer.getModel().hasUndoSteps());
            currentStates.put(Condition.PROCESS_RENDERER_HAS_REDO_STEPS, renderer.getModel().hasRedoSteps());
            currentStates.put(Condition.PROCESS_HAS_BREAKPOINTS, process.hasBreakpoints());
            currentStates.put(Condition.PROCESS_IS_READ_ONLY, isProcessReadOnly());
        } else {
            currentStates.put(Condition.PROCESS_STOPPED, true);
            currentStates.put(Condition.PROCESS_PAUSED, false);
            currentStates.put(Condition.PROCESS_RUNNING, false);
            currentStates.put(Condition.EDIT_IN_PROGRESS, false);
            currentStates.put(Condition.PROCESS_SAVED, false);
            currentStates.put(Condition.PROCESS_HAS_REPOSITORY_LOCATION, false);
            currentStates.put(Condition.PROCESS_RENDERER_IS_VISIBLE, false);
            currentStates.put(Condition.PROCESS_RENDERER_HAS_UNDO_STEPS, false);
            currentStates.put(Condition.PROCESS_RENDERER_HAS_REDO_STEPS, false);
            currentStates.put(Condition.PROCESS_HAS_BREAKPOINTS, false);
            currentStates.put(Condition.PROCESS_IS_READ_ONLY, false);
        }
        return currentStates;
    }

    /** The currently selected operator will be deleted. */
    public void delete() {
        if (isProcessReadOnly()) {
            SwingTools.showVerySimpleErrorMessage("cannot_change_readonly_process");
            return;
        }
        Operator parent = null;

        List<Operator> selectedOperators = getSelectedOperators();
        final String disableBehavior = PropertyService.getParameterValue(StudioProperties.PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR);
        if ("bridged".equals(disableBehavior)) {
            // we need to find ports inside and outside and connect
            HashMap<MetaData, List<OutputPort>> insideConnections = new HashMap<>();
            HashMap<MetaData, List<InputPort>> outsideConnections = new HashMap<>();
            for (Operator operator : selectedOperators) {
                for (InputPort inputPort : operator.getInputPorts().getAllPorts()) {
                    if (inputPort.isConnected() && !selectedOperators.contains(inputPort.getSource().getPorts().getOwner().getOperator())) {
                        MetaData inputMetaData = inputPort.getMetaData();
                        insideConnections.computeIfAbsent(inputMetaData, m -> new LinkedList<>()).add(inputPort.getSource());
                        inputPort.getSource().lock();
                    }
                }
                for (OutputPort outputPort : operator.getOutputPorts().getAllPorts()) {
                    if (outputPort.isConnected() && !selectedOperators.contains(outputPort.getDestination().getPorts().getOwner().getOperator())) {
                        MetaData inputMetaData = outputPort.getMetaData();
                        outsideConnections.computeIfAbsent(inputMetaData, m -> new LinkedList<>()).add(outputPort.getDestination());
                        outputPort.getDestination().lock();
                    }
                }
            }

            // we remove all operators
            for (Operator operator : selectedOperators) {
                if (parent == null) {
                    parent = operator.getParent();
                }
                operator.remove();
            }
            // we restore inside outside connections by bridging
            for (Map.Entry<MetaData, List<OutputPort>> entry : insideConnections.entrySet()) {
                List<InputPort> possibleTargets = outsideConnections.get(entry.getKey());
                if (possibleTargets != null) {
                    Iterator<OutputPort> sourceIterator = entry.getValue().iterator();
                    Iterator<InputPort> targetIterator = possibleTargets.iterator();
                    while (sourceIterator.hasNext() && targetIterator.hasNext()) {
                        sourceIterator.next().connectTo(targetIterator.next());
                    }
                    // unlock ports again
                    possibleTargets.forEach(Port::unlock);
                }
                entry.getValue().forEach(Port::unlock);
            }
        } else {
            // we remove all operators
            for (Operator operator : selectedOperators) {
                if (parent == null) {
                    parent = operator.getParent();
                }
                operator.remove();
            }
        }


        renderer.selectOperators(Collections.singletonList(parent));
    }

    /**
     * The given operators will be inserted at the last position of the currently selected operator
     * chain.
     */
    public void insert(List<Operator> newOperators) {
        if (isProcessReadOnly()) {
            SwingTools.showVerySimpleErrorMessage("cannot_change_readonly_process");
            return;
        }
        Object selectedNode = getSelectedOperator();
        if (selectedNode == null) {
            SwingTools.showVerySimpleErrorMessage("cannot_insert_operator");
            return;
        }
        ProcessEditorPanel processRenderer = renderer;
        ProcessRendererModel model = processRenderer.getModel();
        ExecutionUnit process;
        int i = -1;
        if (model.getDisplayedChain() == selectedNode) {
            int index = processRenderer.getProcessIndexUnder(model.getCurrentMousePosition());
            if (index == -1) {
                index = 0;
            }
            process = ((OperatorChain) selectedNode).getSubprocess(index);
        } else {
            Operator selectedOperator = (Operator) selectedNode;
            process = selectedOperator.getExecutionUnit();
            i = process.getOperators().indexOf(selectedOperator) + 1;
        }

        for (Operator newOperator : newOperators) {
            if (i < 0) {
                process.addOperator(newOperator);
            } else {
                process.addOperator(newOperator, i++);
            }
        }
        // call autofit so each operator has a (valid) location
        AUTO_FIT_ACTION.actionPerformed(null);
        renderer.selectOperators(newOperators);
    }

    public Operator getSelectedOperator() {
        return getFirstSelectedOperator();
    }

    public Operator getRootOperator() {
        if (process != null) {
            return process.getRootOperator();
        }
        return null;
    }

    public AbstractPipeline getProcess() {
        return process;
    }

    @Override
    public void onProcessLoad(AbstractPipeline process) {
        if (this.process != null) {
            this.process.unregisterLifeCycleEventListener(eventListener);
        }
        this.process = process;
        enableActions();
        if (this.process != null) {
            this.process.registerLifeCycleEventListener(eventListener);
        }
    }

    @Override
    public void onProcessEdit(AbstractPipeline process) {
        enableActions();
    }

    @Override
    public void onDisplayedChainChanged(OperatorChain displayedChain) {
        enableActions();
    }

    @Override
    public void onOperatorSelectionChange(List<Operator> newSelection) {
        this.selection = newSelection;
        enableActions();
    }

    private void updateCheckboxStates() {
        Operator op = getSelectedOperator();
        if (op != null) {
            for (int pos = 0; pos < TOGGLE_BREAKPOINT.length; pos++) {
                TOGGLE_BREAKPOINT[pos].setSelected(op.hasBreakpoint(pos));
            }
            TOGGLE_ACTIVATION_ITEM.setSelected(op.isEnabled());
        } else {
            for (ToggleBreakpointItem toggleBreakpointItem : TOGGLE_BREAKPOINT) {
                toggleBreakpointItem.setSelected(false);
            }
            TOGGLE_ACTIVATION_ITEM.setSelected(false);
        }
    }

    @Override
    public void onProcessReadOnlyStateChange(boolean readOnly) {
        if (processReadOnly == readOnly)
            return;
        this.processReadOnly = readOnly;
        enableActions();
    }

    public boolean isProcessReadOnly() {
        return processReadOnly;
    }

    public void setProcess(AbstractPipeline pipeline) {
        process = pipeline;
    }

    public void setSelection(List<Operator> selectedOperatorList) {
        selection = selectedOperatorList;
    }
}
