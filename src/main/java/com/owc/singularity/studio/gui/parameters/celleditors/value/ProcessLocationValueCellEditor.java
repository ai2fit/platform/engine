/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.util.function.Predicate;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeProcessLocation;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * Repository location cell editor that is specialized for processes and adds a second button that
 * allows to open the selected process.
 *
 * @author Marcel Seifert, Nils Woehler, Jan Czogalla
 *
 */
public class ProcessLocationValueCellEditor extends RepositoryLocationWithExtraValueCellEditor {

    private static final long serialVersionUID = 1L;

    public ProcessLocationValueCellEditor(final ParameterTypeProcessLocation type) {
        super(type);
    }

    @Override
    protected String getExtraActionKey() {
        return "execute_process.open_process";
    }

    @Override
    protected void doExtraAction(RepositoryPath repositoryLocation) {
        OpenEntryAction.showProcess(Entries.getEntry(repositoryLocation));
    }

    @Override
    protected Class<AbstractPipeline> getExpectedEntryClass() {
        return AbstractPipeline.class;
    }

    @Override
    protected Predicate<RepositoryTreeNode> getRepositoryFilter() {
        return RepositoryLocationChooser.ONLY_PROCESSES;
    }
}
