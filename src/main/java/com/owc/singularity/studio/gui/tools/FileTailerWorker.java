package com.owc.singularity.studio.gui.tools;

import java.nio.file.Path;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.SwingWorker;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListenerAdapter;

public class FileTailerWorker extends SwingWorker<Void, String> {

    private final Path filePath;
    private final Consumer<List<String>> lineConsumer;
    private final long delay;

    public FileTailerWorker(Path filePath, Consumer<List<String>> lineConsumer) {
        this(filePath, lineConsumer, 1000);
    }

    public FileTailerWorker(Path filePath, Consumer<List<String>> lineConsumer, long delay) {
        this.filePath = filePath;
        this.lineConsumer = lineConsumer;
        this.delay = delay;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Tailer tailer = new Tailer(filePath.toFile(), new TailerListenerAdapter() {

            @Override
            public void handle(String line) {
                publish(line);
            }
        }, delay);
        tailer.run();
        return null;
    }

    @Override
    protected void process(List<String> chunks) {
        lineConsumer.accept(chunks);
    }
}
