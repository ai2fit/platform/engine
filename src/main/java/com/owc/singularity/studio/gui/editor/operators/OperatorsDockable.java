/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.operators;


import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.dnd.DragListener;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * This container contains all available operators in a grouped view (tree). From here the current
 * group can be selected which displays the operators of the group in an operator list on the left
 * side. From here, new operators can be dragged into the operator tree.
 *
 * @author Ingo Mierswa
 */
public class OperatorsDockable extends JPanel implements TreeSelectionListener, Dockable {

    private static final long serialVersionUID = -8910332473638172252L;

    private final OperatorGroupTree operatorGroupTree;

    public OperatorsDockable() {
        this(null);
    }

    /**
     * The drag listener will be registered at the operator tree and will receive drag start events
     * if a drag has started and drag stopped events if dragging has stopped again
     */
    public OperatorsDockable(DragListener dragListener) {
        super(new BorderLayout());
        // will cause the tree half to keep fixed size during resizing
        setBorder(null);

        this.operatorGroupTree = new OperatorGroupTree();
        this.operatorGroupTree.getTree().addTreeSelectionListener(this);
        add(operatorGroupTree, BorderLayout.CENTER);

        if (dragListener != null) {
            operatorGroupTree.getOperatorTreeTransferhandler().addDragListener(dragListener);
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        // do nothing
    }

    public static final String NEW_OPERATOR_DOCK_KEY = "new_operator";
    private final DockKey DOCK_KEY = new ResourceDockKey(NEW_OPERATOR_DOCK_KEY);

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    public OperatorGroupTree getNewOperatorGroupTree() {
        return operatorGroupTree;
    }
}
