/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;

import javax.swing.*;

import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.io.XMLTools;
import com.owc.singularity.engine.tools.WebServiceTools;
import com.owc.singularity.engine.tools.XMLParserException;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.renderer.Renderer;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.Icons;


/**
 * The IOObjectGUIService is the basic provider for the registered renderers of IOObjects and their
 * icons. All {@link IOObject}s which want to provide a Renderer for visualization must place an
 * entry in the <code>ioobjects.xml</xml> file in order to allow for renderer retrieval.
 * 
 * @author Ingo Mierswa, Nils Woehler
 */
public class IOObjectGUIService {

    private static final Logger log = LogService.getI18NLogger(IOObjectGUIService.class);

    private static final Icons ICON_DEFAULT = Icons.fromName("data.png");

    /**
     * Maps names of IOObjects to lists of renderers that can render this object. These instances
     * are shared!
     */
    private static final Map<Class<? extends IOObject>, List<Renderer>> objectRenderers = new HashMap<>();

    private static final Map<Class<? extends IOObject>, Icons> class2IconMap = new HashMap<>();

    public static void registerRenderers(String owner, URL ioObjectsURL, ClassLoader classLoader) {
        InputStream in = null;
        try {
            if (ioObjectsURL != null) {
                in = WebServiceTools.openStreamFromURL(ioObjectsURL);
                registerRenderers(owner, in, classLoader);
            }
        } catch (IOException e) {
            log.warn("com.owc.singularity.studio.gui.renderer.RendererService.initializing_io_object_description_from_plugin_error", owner, e, e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // do nothing
                }
            }
        }
    }

    public static void registerRenderers(String owner, InputStream in, ClassLoader classLoader) {
        log.debug("com.owc.singularity.studio.gui.renderer.RendererService.loading_renderers", owner);
        try {
            Document document = XMLTools.createDocumentBuilder().parse(in);
            Element ioObjectsElement = document.getDocumentElement();
            if (ioObjectsElement.getTagName().equals("ioobjects")) {
                NodeList ioObjectNodes = ioObjectsElement.getElementsByTagName("ioobject");
                for (int i = 0; i < ioObjectNodes.getLength(); i++) {
                    Node ioObjectNode = ioObjectNodes.item(i);
                    if (ioObjectNode instanceof Element ioObjectElement) {
                        String className = ioObjectElement.getAttribute("class");

                        String icon = null;
                        if (ioObjectElement.hasAttribute("icon")) {
                            icon = ioObjectElement.getAttribute("icon");
                        }

                        NodeList rendererNodes = ioObjectElement.getElementsByTagName("renderer");
                        List<String> renderers = new LinkedList<>();
                        for (int k = 0; k < rendererNodes.getLength(); k++) {
                            Node rendererNode = rendererNodes.item(k);
                            if (rendererNode instanceof Element rendererElement) {
                                String rendererName = rendererElement.getTextContent();
                                renderers.add(rendererName);
                            }
                        }

                        registerRenderers(className, icon, renderers, classLoader);
                    }
                }
            } else {
                log.warn("com.owc.singularity.studio.gui.renderer.RendererService.initializing_io_object_description_tag_error");
            }
        } catch (XMLParserException e) {
            log.warn("com.owc.singularity.studio.gui.renderer.RendererService.initializing_io_object_description_error", owner, e);
        } catch (IOException | SAXException e) {
            log.warn("com.owc.singularity.studio.gui.renderer.RendererService.initializing_io_object_description_parsing_error", owner, e);
        }
    }

    @SuppressWarnings("unchecked")
    public static synchronized void registerRenderers(String className, String iconName, List<String> rendererClassNames, ClassLoader classLoader) {
        try {
            Class<? extends IOObject> clazz = (Class<? extends IOObject>) Class.forName(className, true, classLoader);

            List<Renderer> renderers = new LinkedList<>();
            for (String rendererClassName : rendererClassNames) {
                Class<? extends Renderer> rendererClass;
                try {
                    rendererClass = (Class<? extends Renderer>) Class.forName(rendererClassName, true, classLoader);
                } catch (Exception e) {
                    // let's try with the plugin classloader (some Core renderers are now in bundled
                    // extensions)
                    rendererClass = (Class<? extends Renderer>) Class.forName(rendererClassName, false, ModuleService.getMajorClassLoader());
                }

                Renderer renderer = rendererClass.getDeclaredConstructor().newInstance();
                renderers.add(renderer);
            }

            objectRenderers.put(clazz, renderers);

            // try to create icons
            if (iconName != null && !iconName.isEmpty()) {
                class2IconMap.put(clazz, Icons.fromName(iconName));
            }

        } catch (ClassNotFoundException e) {
            log.warn("com.owc.singularity.studio.gui.renderer.RendererService.registering_renderer_error", "Unknown Class: " + className, e);
        } catch (Throwable e) {
            log.warn("com.owc.singularity.studio.gui.renderer.RendererService.registering_renderer_error", e, e);

        }
    }


    /**
     * Returns a list of renderers defined for this IOObject name for the respective object. It is
     * recommended to use {@link #getRenderers(IOObject)} instead.
     */
    public static List<Renderer> getRenderers(Class<? extends IOObject> reportableClass) {
        List<Renderer> rendererList = findMostSpecificValueForClassInMap(reportableClass, objectRenderers);
        if (rendererList == null)
            return new LinkedList<>();
        return rendererList;
    }

    /**
     * Returns a list of shared (i.e. not thread-safe!) renderers defined for this IOObject.
     */
    public static List<Renderer> getRenderers(IOObject ioo) {
        return getRenderers(Objects.requireNonNull(ioo).getClass());
    }

    /**
     * Returns the given renderer, will also return legay renderers.
     */
    public static Renderer getRenderer(Class<? extends IOObject> reportableClass, String rendererName) {
        List<Renderer> renderers = getRenderers(reportableClass);
        for (Renderer renderer : renderers) {
            if (renderer.getName().equals(rendererName)) {
                return renderer;
            }
        }
        return null;
    }

    private static <R, T> R findMostSpecificValueForClassInMap(Class<? extends T> objectClass, Map<Class<? extends T>, R> map) {
        Entry<Class<? extends T>, R> bestMatchList = null;
        for (Entry<Class<? extends T>, R> renderersEntry : map.entrySet()) {
            if (renderersEntry.getKey().isAssignableFrom(objectClass)) {
                if (bestMatchList == null || bestMatchList.getKey().isAssignableFrom(renderersEntry.getKey()))
                    bestMatchList = renderersEntry;
            }
        }
        return bestMatchList == null ? null : bestMatchList.getValue();
    }

    /**
     * This returns the icon registered for the given class or a default icon, if nothing has been
     * registered. Returns size {@link IconSize#SMALL}.
     */
    public static ImageIcon getIcon(Class<? extends IOObject> objectClass) {
        return getIcon(objectClass, IconSize.SMALL);
    }

    public static ImageIcon getIcon(Class<? extends IOObject> objectClass, IconSize iconSize) {
        Icons result = null;
        if (objectClass != null) {
            result = findMostSpecificValueForClassInMap(objectClass, class2IconMap);
        }
        if (result == null) {
            result = ICON_DEFAULT;
        }
        switch (iconSize) {
            case HUGE:
                return result.getIcon48x48();
            case MEDIUM:
                return result.getIcon24x24();

            case SMALL:
            default:
                return result.getIcon16x16();
        }
    }

    public static Icons getIcons(Class<? extends IOObject> objectClass) {
        return findMostSpecificValueForClassInMap(objectClass, class2IconMap);
    }
}
