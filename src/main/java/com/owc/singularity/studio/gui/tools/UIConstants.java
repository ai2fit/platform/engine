package com.owc.singularity.studio.gui.tools;

import java.awt.*;

public class UIConstants {

    public static final Cursor INVALID = getSystemCustomCursorOr("Invalid.32x32", Cursor.getDefaultCursor());

    public static Cursor getSystemCustomCursorOr(String name, Cursor fallback) {
        try {
            Cursor cursor = Cursor.getSystemCustomCursor(name);
            if (cursor == null)
                return fallback;
            return cursor;
        } catch (AWTException e) {
            return fallback;
        }
    }
}
