package com.owc.singularity.studio.gui.vcs.conflict.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.vcs.MergeConflict;

public class SimpleMergeConflictDecision implements MergeConflictDecision {

    private final boolean oursOrTheirs;
    private final RepositoryPath resultPath;

    public SimpleMergeConflictDecision(MergeConflict conflict, boolean oursOrTheirs) {
        this.oursOrTheirs = oursOrTheirs;
        this.resultPath = oursOrTheirs ? RepositoryPath.of(conflict.getConflictRepositoryPath(), conflict.getOurVersionId())
                : RepositoryPath.of(conflict.getConflictRepositoryPath(), conflict.getTheirVersionId());

    }

    @Override
    public InputStream getInputStream() throws IOException {
        return Files.newInputStream(resultPath);
    }

    @Override
    public RepositoryPath getResultRepositoryPath() {
        return resultPath;
    }

    @Override
    public String getDescription() {
        return oursOrTheirs ? "using our changes" : "using their changes";
    }
}
