/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository;


import java.awt.*;
import java.awt.event.ActionListener;
import java.io.Closeable;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.EntryVersion;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.repository.actions.context.DefaultRepositoryTreeContextActionFactory;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * A dialog that shows the repository tree. The static method
 * {@link #selectLocation(RepositoryPath, Component)})} shows a dialog and returns the location
 * selected by the user.
 *
 * @author Simon Fischer, Tobias Malbrecht
 *
 */
public class RepositoryLocationChooser extends JPanel implements AutoCloseable, Closeable {

    private static final long serialVersionUID = -5002963355475760046L;


    /** Removes the Connections folder */
    public static final Predicate<RepositoryTreeNode> NO_CONNECTIONS = Predicate.not(new TypeFilter(ConnectionParametersIOObject.class));

    /** Only Connections and Repositories which support Connections */
    public static final Predicate<RepositoryTreeNode> ONLY_CONNECTIONS = new TypeFilter(ConnectionParametersIOObject.class);

    /** Shows only processes, and hides the connection folder */
    public static final Predicate<RepositoryTreeNode> ONLY_PROCESSES = new TypeFilter(AbstractPipeline.class);


    private final boolean allowFiles;
    private final boolean allowDirectories;
    private final boolean allowVersions;
    private final RepositoryTree tree;
    private RepositoryPath initialValue;

    private transient RepositoryEntryTextField entryNameTextField;
    private transient JLabel errorTextLabel;
    private transient JLabel errorIconLabel;
    private transient Icon standardIcon;
    private transient Icon errorIcon;
    private transient JCheckBox resolveBox;
    private transient final JLabel resultLabel = new JLabel();
    private transient final ExtendedJComboBox<String> version = new ExtendedJComboBox<>();
    private final RepositoryPath resolveRelativeTo;

    private final List<WeakReference<ChangeListener>> listeners = new LinkedList<>();
    private transient TreeSelectionListener treeSelectionListener;
    private transient Observer<Boolean> entryNameTextFieldObserver;
    private transient ActionListener resolveBoxActionListener;
    private boolean updating;

    private static class RepositoryLocationChooserDialog extends ButtonDialog implements ChangeListener, AutoCloseable, Closeable {

        private static final long serialVersionUID = -726540444296013310L;

        private RepositoryLocationChooser chooser;
        private final JButton okButton;

        /**
         * Create a Dialog to choose a {@link RepositoryPath} with the following configuration
         * settings.
         *
         * @param owner
         *            Parent dialog to be blocked by this modal dialog
         * @param resolveRelativeTo
         *            if a relative path is requested, this is the base path to be relative to
         * @param initialValue
         *            preselected value
         * @param allowEntries
         *            if true entries are shown, else only folders will be shown
         * @param allowFolders
         *            if true folders are shown, else no folders will be shown
         * @param onlyWriteableMounts
         *            if true show only those repositories that are writable by the current user
         * @param entryPredicate
         *            if set it will filter the shown entries based on its logic
         * @since 9.4
         */
        public RepositoryLocationChooserDialog(Window owner, RepositoryPath resolveRelativeTo, RepositoryPath initialValue, final boolean allowEntries,
                final boolean allowFolders, boolean allowVersions, final boolean disableUserInput, final boolean onlyWriteableMounts,
                Predicate<RepositoryTreeNode> entryPredicate) {
            super(owner, "repository_chooser", ModalityType.APPLICATION_MODAL);
            okButton = makeOkButton();
            chooser = new RepositoryLocationChooser(this, resolveRelativeTo, initialValue, allowEntries, allowFolders, allowVersions, disableUserInput,
                    onlyWriteableMounts, Colors.WHITE, entryPredicate);
            chooser.addChangeListener(this);
            okButton.setEnabled(chooser.hasSelection());
            JButton cancelButton = makeCancelButton();
            layoutDefault(chooser, NORMAL, okButton, cancelButton);
        }

        @Override
        public void close() {
            chooser.removeChangeListener(this);
            chooser.close();
            chooser = null;
            removeAll();
            super.close();
            dispose();
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            okButton.setEnabled(chooser.hasSelection());
        }
    }

    public RepositoryLocationChooser(Dialog owner, RepositoryPath resolveRelativeTo, RepositoryPath initialValue, boolean allowFiles, boolean allowDirectories,
            boolean allowVersions, boolean enforceValidRepositoryEntryName) {
        this(owner, resolveRelativeTo, initialValue, allowFiles, allowDirectories, allowVersions, enforceValidRepositoryEntryName, false);
    }

    public RepositoryLocationChooser(Dialog owner, RepositoryPath resolveRelativeTo, RepositoryPath initialValue, final boolean allowFiles,
            final boolean allowDirectories, boolean allowVersions, boolean enforceValidRepositoryEntryName, final boolean onlyWriteableRepositories) {
        this(owner, resolveRelativeTo, initialValue, allowFiles, allowDirectories, allowVersions, enforceValidRepositoryEntryName, onlyWriteableRepositories,
                null);
    }

    public RepositoryLocationChooser(Dialog owner, RepositoryPath resolveRelativeTo, RepositoryPath initialValue, final boolean allowFiles,
            final boolean allowDirectories, boolean allowVersions, boolean enforceValidRepositoryEntryName, final boolean onlyWritableRepositories,
            Color backgroundColor) {
        this(owner, resolveRelativeTo, initialValue, allowFiles, allowDirectories, allowVersions, enforceValidRepositoryEntryName, onlyWritableRepositories,
                backgroundColor, null);
    }

    /**
     * Show a dialog to select a {@link RepositoryPath} to be used. Can be configured via the
     * following parameters.
     *
     * @param owner
     *            parent for the modal dialog
     * @param resolveRelativeTo
     *            if a relative path is requested, this is the base path to be relative to
     * @param initialValue
     *            preselected value
     * @param allowFiles
     *            if true entries are shown, else only folders will be shown
     * @param allowDirectories
     *            allow choosing folders
     * @param disableUserInput
     *            if true the user would not be able to input the location explicitly but must
     *            choose it from the tree
     * @param onlyWritableRepositories
     *            if true show only those repositories that are writable by the current user
     * @param backgroundColor
     *            the {@link Color} for the background of the shown tree
     * @param treeEntryFilter
     *            if set it will filter the shown entries based on its logic
     * @since 9.4
     */
    public RepositoryLocationChooser(Dialog owner, RepositoryPath resolveRelativeTo, RepositoryPath initialValue, final boolean allowFiles,
            final boolean allowDirectories, boolean allowVersions, boolean disableUserInput, final boolean onlyWritableRepositories, Color backgroundColor,
            Predicate<RepositoryTreeNode> treeEntryFilter) {
        this.allowFiles = allowFiles;
        this.allowDirectories = allowDirectories;
        this.allowVersions = allowVersions;
        RepositoryPath resolvedLocation;
        if (initialValue != null && !initialValue.toString().isBlank()) {
            try {
                resolvedLocation = !initialValue.isAbsolute() && resolveRelativeTo != null ? resolveRelativeTo.resolve(initialValue) : initialValue;
                this.initialValue = resolvedLocation;
            } catch (IllegalArgumentException e) {
                resolvedLocation = RepositoryPath.of("");
                this.initialValue = null;
            }
        } else {
            resolvedLocation = RepositoryPath.of("");
            this.initialValue = null;
        }
        this.resolveRelativeTo = resolveRelativeTo;
        treeEntryFilter = treeEntryFilter == null ? ignored -> true : treeEntryFilter;
        Predicate<RepositoryTreeNode> acceptAnyDirectory = entryTreeNode -> entryTreeNode.getInformation().isDirectory();
        if (onlyWritableRepositories) {
            Predicate<RepositoryTreeNode> acceptOnlyWritableFolders = entry -> entry.getInformation().isWritable();
            treeEntryFilter = acceptOnlyWritableFolders.and(treeEntryFilter);
        }
        if (!this.allowFiles) {
            // show only directories
            treeEntryFilter = acceptAnyDirectory.and(treeEntryFilter);
        }
        treeEntryFilter = acceptAnyDirectory.or(treeEntryFilter);
        this.tree = new RepositoryTree(owner, false, backgroundColor, treeEntryFilter);
        this.tree.addContextActionFactory(DefaultRepositoryTreeContextActionFactory.getInstance());

        if (resolvedLocation.equals(RepositoryPath.of(""))) {
            // no initial value, select the relative or default file system
            tree.expandIfExists(Objects.requireNonNullElse(resolveRelativeTo, RepositoryManager.DEFAULT_FILESYSTEM_ROOT));
        } else {
            tree.expandAndSelectIfExists(resolvedLocation);
        }


        this.treeSelectionListener = new OnTreeSelectionChange();
        tree.getSelectionModel().addTreeSelectionListener(treeSelectionListener);

        initUI(resolveRelativeTo, disableUserInput);


        if (initialValue != null && !disableUserInput) {
            // check if initial value is valid
            entryNameTextField.triggerCheck();
        }
        updateResult();
    }

    private void initUI(RepositoryPath resolveRelativeTo, boolean disableUserInput) {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridwidth = GridBagConstraints.REMAINDER;

        JScrollPane treePane = new ExtendedJScrollPane(tree);
        treePane.setBorder(ButtonDialog.createBorder());

        add(treePane, c);

        standardIcon = null;
        errorIcon = SwingTools.createIcon("16/" + I18N.getGUIMessage("gui.dialog.repository_location.location_invalid.icon"));
        errorIconLabel = new JLabel();
        errorIconLabel.setMinimumSize(new Dimension(16, 16));
        errorIconLabel.setPreferredSize(new Dimension(16, 16));
        errorTextLabel = new JLabel();
        JPanel selectionErrorPanel = new JPanel();
        selectionErrorPanel.setLayout(new FlowLayout());
        selectionErrorPanel.add(errorIconLabel);
        selectionErrorPanel.add(errorTextLabel);
        c.weightx = 0;
        c.weighty = 0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.NONE;
        add(selectionErrorPanel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 0, 0);
        c.gridwidth = GridBagConstraints.REMAINDER;
        if (!disableUserInput) {
            entryNameTextField = new RepositoryEntryTextField();
            if (initialValue != null) {
                entryNameTextField.setText(initialValue.getFileName().toString());
            }
            entryNameTextFieldObserver = new OnRepositoryEntryNameChange();
            entryNameTextField.addObserver(entryNameTextFieldObserver, true);
            add(entryNameTextField, c);
        }

        c.gridwidth = GridBagConstraints.RELATIVE;
        c.weightx = 0;
        c.insets = new Insets(ButtonDialog.GAP, 0, 0, ButtonDialog.GAP);
        add(new ResourceLabel("repository_chooser.location"), c);
        c.weightx = 1;
        c.insets = new Insets(ButtonDialog.GAP, 0, 0, 0);
        c.gridwidth = GridBagConstraints.REMAINDER;
        add(resultLabel, c);

        if (resolveRelativeTo != null) {
            resolveBox = new JCheckBox(new ResourceActionAdapter("repository_chooser.resolve", resolveRelativeTo.toAbsolutePath().toString()));
            resolveBox.setSelected("true".equals(PropertyService.getParameterValue(StudioProperties.PROPERTY_RESOLVE_RELATIVE_REPOSITORY_LOCATIONS)));
            add(resolveBox, c);
            resolveBoxActionListener = e -> updateResult();
            resolveBox.addActionListener(resolveBoxActionListener);
        }
        if (allowVersions) {
            c.gridwidth = GridBagConstraints.RELATIVE;
            c.weightx = 0;
            c.insets = new Insets(20, 0, 0, ButtonDialog.GAP);
            add(new ResourceLabel("repository_chooser.version"), c);
            c.weightx = 1;
            c.insets = new Insets(20, 0, 0, 0);
            c.gridwidth = GridBagConstraints.REMAINDER;
            add(version, c);
        }
    }

    /**
     * Sets the name of the repository entry for the file chooser.
     *
     * @param name
     *            the new name of the repository entry
     */
    public void setRepositoryEntryName(final String name) {
        if (updating)
            return;
        if (entryNameTextField != null) {
            updating = true;
            entryNameTextField.setText(name);
            updating = false;
        }
        updateResult();
    }

    public String getRepositoryEntryName() {
        if (entryNameTextField == null)
            return null;
        String text = entryNameTextField.getText();
        return text == null || text.isBlank() ? null : text;
    }

    public RepositoryPath getResult() {
        RepositoryPath selectedLocation;
        RepositoryTreeNode selectedEntry = tree.getSelectedEntry();
        String repositoryEntryName = getRepositoryEntryName();
        if (selectedEntry == null && repositoryEntryName == null)
            return null;
        if (selectedEntry != null) {
            selectedLocation = selectedEntry.getRepositoryPath();
            if (repositoryEntryName != null) {
                selectedLocation = resolvePathToSelectedTreeNode(selectedEntry, repositoryEntryName);
            }
        } else {
            selectedLocation = initialValue != null ? initialValue : RepositoryManager.DEFAULT_FILESYSTEM_ROOT.resolve(repositoryEntryName);
        }
        if (allowFiles && !allowDirectories) {
            if (Files.exists(selectedLocation) && Files.isDirectory(selectedLocation))
                return null;
        }
        if (resolveRelativeTo != null && resolveBox.isSelected()) {
            selectedLocation = resolveRelativeTo.relativize(selectedLocation);
        }
        return selectedLocation;
    }

    private static RepositoryPath resolvePathToSelectedTreeNode(RepositoryTreeNode selectedNode, String repositoryEntryName) {
        RepositoryPath selectedRepositoryPath = selectedNode.getRepositoryPath();
        if (selectedRepositoryPath.getFileName().toString().equals(repositoryEntryName)) {
            // same path
            return selectedRepositoryPath;
        } else if (selectedNode.getInformation().isDirectory() || selectedRepositoryPath.getParent() == null) {
            // resolve as sub path
            return selectedRepositoryPath.resolve(repositoryEntryName);
        } else {
            // resolve as sibling
            return selectedRepositoryPath.getParent().resolve(repositoryEntryName);
        }
    }

    /** Returns true if the user entered a valid, non-empty repository location. */
    public boolean hasSelection() {
        return getResult() != null;
    }

    public boolean resolveRelative() {
        return resolveBox.isSelected();
    }

    public void setResolveRelative(boolean resolveRelative) {
        if (resolveBox != null && resolveRelative != resolveBox.isSelected()) {
            resolveBox.doClick();
        }
    }

    public void addChangeListener(ChangeListener l) {
        listeners.add(new WeakReference<>(l));
    }

    public void removeChangeListener(ChangeListener l) {
        listeners.removeIf(ref -> ref.get() == l);
    }

    /**
     * Updates the path selected via the tree.
     */
    private void updateSelection(String entryName) {
        if (updating)
            return;
        RepositoryTreeNode selectedEntry = tree.getSelectedEntry();
        if (selectedEntry != null && entryName != null && !entryName.isBlank()) {
            RepositoryPath entryPathToSelect = resolvePathToSelectedTreeNode(selectedEntry, entryName);
            updating = true;
            tree.expandAndSelectIfExists(entryPathToSelect);
            updating = false;
        }
    }

    private void updateResult() {
        if (updating)
            return;
        updating = true;
        RepositoryPath repositoryLocation = getResult();
        resultLabel.setText(repositoryLocation == null ? "" : repositoryLocation.toString());
        // check if a repository folder is selected, if not, show warning
        if (repositoryLocation == null) {
            errorTextLabel.setText(I18N.getGUIMessage("gui.dialog.repository_location.location_invalid_no_selection.label"));
            errorIconLabel.setIcon(errorIcon);
        } else {
            errorTextLabel.setText("");
            errorIconLabel.setIcon(standardIcon);
        }

        if (repositoryLocation != null && allowVersions) {
            RepositoryPath absolutePath;
            if (resolveRelativeTo != null && resolveBox.isSelected()) {
                absolutePath = resolveRelativeTo.resolve(repositoryLocation);
            } else {
                absolutePath = repositoryLocation;
            }
            try {
                boolean resetVersion = true;
                EntryVersion[] entryVersions = Entries.streamVersions(absolutePath.isHead() ? absolutePath : absolutePath.toUnversionedPath())
                        .toArray(EntryVersion[]::new);
                if (version.getItemCount() > 1) {
                    String previousPathVersion = version.getItemAt(1);
                    for (EntryVersion entryVersion : entryVersions) {
                        if (entryVersion.toString().equals(previousPathVersion)) {
                            resetVersion = false;
                            break;
                        }
                    }
                }
                if (resetVersion) {
                    version.removeAllItems();
                    version.addItem("Current Local Version");
                    for (EntryVersion entryVersion : entryVersions) {
                        version.addItem(entryVersion.toString());
                    }
                }
            } catch (IOException ignored) {
            }
            if (!absolutePath.isHead()) {
                String selectedRepositoryId;
                try {
                    selectedRepositoryId = Entries.getVersion(absolutePath).toString();
                } catch (IOException e) {
                    selectedRepositoryId = absolutePath.getVersionId();
                }
                for (int i = 0; i < version.getItemCount(); i++) {
                    if (version.getItemAt(i).contains(selectedRepositoryId)) {
                        version.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } else {
            version.removeAllItems();
        }

        for (Iterator<WeakReference<ChangeListener>> iterator = listeners.iterator(); iterator.hasNext();) {
            ChangeListener l = iterator.next().get();
            if (l == null) {
                iterator.remove();
            } else {
                l.stateChanged(new ChangeEvent(this));
            }
        }
        updating = false;
    }

    private class OnRepositoryEntryNameChange implements Observer<Boolean> {

        @Override
        public void update(Observable<Boolean> observable, Boolean arg) {
            if (updating)
                return;
            updateSelection(getRepositoryEntryName());
            updateResult();
        }
    }

    private class OnTreeSelectionChange implements TreeSelectionListener {

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            if (updating)
                return;
            if (e.getPath() != null && e.getPath().getLastPathComponent()instanceof RepositoryTreeNode entry) {
                if (!allowDirectories && entry.getInformation().isDirectory()) {
                    // skip directories if folders are not allowed
                    updateResult();
                    return;
                }
                if (!allowFiles && !entry.getInformation().isDirectory()) {
                    // skip files if entries are not allowed
                    updateResult();
                    return;
                }
                setRepositoryEntryName(entry.getRepositoryPath().getFileName().toString());
                updateResult();
            }
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        // this bit allows for easy name entering when the dialog is used for saving
        // instantly allows typing text and pressing Enter afterwards
        if (entryNameTextField != null && entryNameTextField.isVisible()) {
            return entryNameTextField.requestFocusInWindow();
        } else {
            return tree.requestFocusInWindow();
        }
    }

    @Override
    public void close() {
        removeAll();
        getParent().remove(this);
        tree.getSelectionModel().removeTreeSelectionListener(treeSelectionListener);
        treeSelectionListener = null;
        if (resolveBox != null) {
            resolveBox.removeActionListener(resolveBoxActionListener);
            resolveBoxActionListener = null;
        }
        if (entryNameTextField != null) {
            entryNameTextField.removeObserver(entryNameTextFieldObserver);
            entryNameTextFieldObserver = null;
        }
        listeners.clear();
        tree.close();
    }

    /**
     * This will open a window to select a repository entry that is an entry or returns null if the
     * user aborts the operation.
     */
    public static RepositoryPath selectEntry(RepositoryPath resolveRelativeTo, Component c) {
        return selectLocation(resolveRelativeTo, null, c, true, false, false);
    }

    /**
     * This will open a window to select a repository entry that is a folder or null if the user
     * chooses to abort.
     */
    public static RepositoryPath selectFolder(RepositoryPath resolveRelativeTo, Component c) {
        return selectLocation(resolveRelativeTo, null, c, false, true, true);
    }

    public static RepositoryPath selectLocation(RepositoryPath resolveRelativeTo, Component c) {
        return selectLocation(resolveRelativeTo, null, c, true, true, true);
    }

    public static RepositoryPath selectLocation(RepositoryPath resolveRelativeTo, RepositoryPath initialValue, Component c, final boolean selectEntries,
            final boolean selectFolder, boolean allowVersions) {
        return selectLocation(resolveRelativeTo, initialValue, c, selectEntries, selectFolder, allowVersions, false);
    }

    public static RepositoryPath selectLocation(RepositoryPath resolveRelativeTo, RepositoryPath initialValue, Component c, final boolean selectEntries,
            final boolean selectFolder, boolean allowVersions, final boolean forceDisableRelativeResolve) {
        return selectLocation(resolveRelativeTo, initialValue, c, selectEntries, selectFolder, allowVersions, forceDisableRelativeResolve, false);
    }

    public static RepositoryPath selectLocation(RepositoryPath resolveRelativeTo, RepositoryPath initialValue, Component c, final boolean selectEntries,
            final boolean selectFolder, boolean allowVersions, final boolean forceDisableRelativeResolve, final boolean onlyWriteableRepositories) {
        return selectLocation(resolveRelativeTo, initialValue, c, selectEntries, selectFolder, allowVersions, forceDisableRelativeResolve, false,
                onlyWriteableRepositories, null);
    }

    /**
     * Show a dialog to select a {@link RepositoryPath} to be used. Can be configured via the
     * following parameters.
     *
     * @param resolveRelativeTo
     *            if a relative path is requested, this is the base path to be relative to
     * @param initialValue
     *            preselected value
     * @param c
     *            base component to find the ancestor window
     * @param selectEntries
     *            if true entries are shown, else only folders will be shown
     * @param selectFolder
     *            if true folders are shown, else no folders will be shown
     * @param forceDisableRelativeResolve
     *            if true relative resolving cannot be activated by the user and returned
     *            {@link RepositoryPath} are absolute
     * @param disableUserInput
     *            if true the user would not be able to input the location explicitly but must
     *            choose it from the tree
     * @param onlyWriteableRepositories
     *            if true show only those repositories that are writable by the current user
     * @param entryPredicate
     *            if set it will filter the shown entries based on its logic
     * @return the chosen path
     * @since 9.4
     */
    public static RepositoryPath selectLocation(RepositoryPath resolveRelativeTo, RepositoryPath initialValue, Component c, final boolean selectEntries,
            final boolean selectFolder, boolean allowVersions, final boolean forceDisableRelativeResolve, boolean disableUserInput,
            final boolean onlyWriteableRepositories, Predicate<RepositoryTreeNode> entryPredicate) {
        Window owner = c != null ? SwingUtilities.getWindowAncestor(c) : null;

        try (RepositoryLocationChooserDialog dialog = new RepositoryLocationChooserDialog(owner, resolveRelativeTo, initialValue, selectEntries, selectFolder,
                allowVersions, disableUserInput, onlyWriteableRepositories, entryPredicate)) {
            if (forceDisableRelativeResolve) {
                dialog.chooser.setResolveRelative(false);
                if (dialog.chooser.resolveBox != null) {
                    dialog.chooser.resolveBox.setVisible(false);
                }
            }
            dialog.chooser.requestFocusInWindow();
            dialog.setVisible(true);
            // if user has confirmed dialog with double-click or OK button
            if (dialog.wasConfirmed()) {
                if (resolveRelativeTo != null && !forceDisableRelativeResolve) {
                    PropertyService.setParameterValue(StudioProperties.PROPERTY_RESOLVE_RELATIVE_REPOSITORY_LOCATIONS,
                            Boolean.toString(dialog.chooser.resolveRelative()));
                    PropertyService.saveParameters();
                }

                RepositoryPath chosenPath = dialog.chooser.getResult();
                if (dialog.chooser.allowVersions) {
                    if (chosenPath == null)
                        return null;
                    int selectedVersionId = dialog.chooser.version.getSelectedIndex();
                    if (selectedVersionId < 1)
                        return chosenPath;
                    RepositoryPath absolutePath;
                    if (resolveRelativeTo != null && !chosenPath.isAbsolute()) {
                        absolutePath = resolveRelativeTo.resolve(chosenPath);
                    } else {
                        absolutePath = chosenPath;
                    }
                    EntryVersion[] entryVersions;
                    try {
                        entryVersions = Entries.streamVersions(absolutePath).toArray(EntryVersion[]::new);
                    } catch (IOException e) {
                        return chosenPath;
                    }
                    EntryVersion selectedVersion = entryVersions[selectedVersionId - 1];
                    return RepositoryPath.of(chosenPath, selectedVersion.getId());
                }
                return chosenPath;
            }
        }
        return null;
    }

    private static class TypeFilter implements Predicate<RepositoryTreeNode> {

        private final Class<?> type;

        public TypeFilter(Class<?> type) {
            super();
            this.type = type;
        }

        @Override
        public boolean test(RepositoryTreeNode t) {
            try {
                return t.getEntry().isInstanceOf(type, ModuleService.getMajorClassLoader());
            } catch (ClassNotFoundException | IOException e) {
                return false;
            }
        }
    }
}
