package com.owc.singularity.studio.gui;


import javax.swing.*;

public interface Form<V> {

    V getData();

    boolean isReadyToSubmit();

    void addInputChangeListener(InputChangeListener listener);

    void removeInputChangeListener(InputChangeListener listener);

    JComponent getComponent();

    interface InputChangeListener {

        void onInputValueChange(String inputKey, String inputValue);
    }
}
