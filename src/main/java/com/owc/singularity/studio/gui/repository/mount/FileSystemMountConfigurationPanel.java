/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.mount;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.repository.RepositoryFileSystem;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.file.FileRepositoryMount;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.ResourceActionAdapter;
import com.owc.singularity.studio.gui.tools.ResourceLabel;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * @author Simon Fischer
 */
public class FileSystemMountConfigurationPanel extends JPanel implements RepositoryMountConfigurationPanel {

    private static final long serialVersionUID = 1L;

    private final JTextField fileField = new JTextField(30);
    private final JCheckBox isReadOnlyField = new JCheckBox(new ResourceActionAdapter("repositorydialog.read_only"));
    private final JCheckBox standardLocation = new JCheckBox(new ResourceActionAdapter("repositorydialog.use_standard_location"));
    private final JLabel errorLabel = new JLabel(" ");

    private JButton chooseFileButton;

    private JButton finishButton;

    /** The optional finish button will be disabled when invalid information is entered. */
    public FileSystemMountConfigurationPanel(JButton finishButton, boolean isNew) {
        this.finishButton = finishButton;

        standardLocation.setSelected(isNew);
        errorLabel.setForeground(Color.red);
        errorLabel.setFont(errorLabel.getFont().deriveFont(Font.BOLD));

        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.weighty = 0;
        c.weightx = .5;
        c.insets = new Insets(4, 4, 4, 4);
        c.fill = GridBagConstraints.HORIZONTAL;


        // URL
        c.gridwidth = 1;
        c.gridheight = 2;
        JLabel label = new ResourceLabel("repositorydialog.root_directory");
        label.setLabelFor(fileField);
        gbl.setConstraints(label, c);
        add(label);


        if (isNew) {
            c.gridheight = 1;
            c.gridwidth = GridBagConstraints.REMAINDER;
            add(standardLocation, c);
        }
        c.gridwidth = GridBagConstraints.RELATIVE;
        gbl.setConstraints(fileField, c);
        add(fileField);

        c.gridwidth = GridBagConstraints.REMAINDER;
        chooseFileButton = new JButton(new ResourceAction(true, "choose_file") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                File file = SwingTools.chooseFile(FileSystemMountConfigurationPanel.this, null, true, true, (String) null, null);
                if (file != null) {
                    fileField.setText(file.toString());
                }
            }
        });
        add(chooseFileButton, c);


        // Read Only
        c.gridwidth = 1;
        c.gridheight = 1;
        label = new ResourceLabel("repositorydialog.read_only");
        label.setLabelFor(isReadOnlyField);
        gbl.setConstraints(label, c);
        add(label);

        c.gridwidth = GridBagConstraints.REMAINDER;
        gbl.setConstraints(isReadOnlyField, c);
        add(isReadOnlyField);

        // error label
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = GridBagConstraints.REMAINDER;
        add(errorLabel, c);

        JPanel dummy = new JPanel();
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        gbl.setConstraints(dummy, c);
        add(dummy);

        fileField.selectAll();
        fileField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                dumpSettingsCheck();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                dumpSettingsCheck();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                dumpSettingsCheck();
            }
        });
        standardLocation.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                enableFileField();
            }
        });
        enableFileField();
    }

    @Override
    public void mount(RepositoryPath mountPoint) throws IOException {
        HashMap<String, String> options = new HashMap<>();
        options.put(FileRepositoryMount.OPTION_BASE_DIRECTORY, Path.of(fileField.getText()).toAbsolutePath().toString());
        options.put(FileRepositoryMount.OPTION_READ_ONLY, isReadOnlyField.isSelected() ? "true" : "false");
        mountPoint.getFileSystem().mount(mountPoint, FileRepositoryMount.TYPE_NAME, options);
    }

    @Override
    public void configureUIElementsFrom(RepositoryMount mount) {
        FileRepositoryMount fileMount = (FileRepositoryMount) mount;
        isReadOnlyField.setSelected(fileMount.isReadOnly());
        fileField.setText(fileMount.getBasePath().toString());
    }

    @Override
    public boolean configure(RepositoryMount mount) {
        try {
            RepositoryPath mountPoint = mount.getMountPath();
            RepositoryFileSystem fileSystem = mountPoint.getFileSystem();
            fileSystem.unmount(mountPoint, false);
            mount(mountPoint);
        } catch (IOException e) {
            SwingTools.showSimpleErrorMessage("cannot_create_repository", e);
            return false;
        }
        return true;
    }

    @Override
    public JComponent getComponent() {
        return this;
    }


    /** Enables or disable the file field iff {@link #standardLocation} is deselected. */
    private void enableFileField() {
        boolean enabled = !standardLocation.isSelected();
        fileField.setEnabled(enabled);
        chooseFileButton.setEnabled(enabled);
        dumpSettingsCheck();
    }

    private void dumpSettingsCheck() {
        String key = checkSettings();
        if (key == null) {
            // TODO: enable ok button
            errorLabel.setText(" ");
            if (finishButton != null) {
                finishButton.setEnabled(true);
            }
        } else {
            // TODO: disable ok button
            errorLabel.setText(I18N.getGUIMessage("gui.dialog.repositorydialog.error." + key));
            if (finishButton != null) {
                finishButton.setEnabled(false);
            }
        }
    }

    /** Checks the current settings and returns an I18N key if settings are incorrect. */
    private String checkSettings() {
        if (fileField.getText().isEmpty()) {
            return "folder_cannot_be_empty";
        }
        File file = new File(fileField.getText());
        if (file.exists() && !file.isDirectory()) {
            return "root_is_not_a_directory";
        }
        if (file.exists() && !file.canWrite()) {
            return "root_is_not_writable";
        }

        while (!file.exists()) {
            file = file.getParentFile();
            if (file == null) {
                return "cannot_determine_root";
            }
        }
        if (!file.canWrite()) {
            return "cannot_create_root_folder";
        }
        return null;
    }

    @Override
    public void setOkButton(JButton okButton) {
        this.finishButton = okButton;
    }

    @Override
    public List<AbstractButton> getAdditionalButtons() {
        return new LinkedList<>();
    }

}
