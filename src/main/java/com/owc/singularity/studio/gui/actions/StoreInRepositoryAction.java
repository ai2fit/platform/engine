/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;

import com.owc.singularity.engine.IOObjectService;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * An action to store IOObjects in the repository.
 * 
 * @author Simon Fischer
 * 
 */
public class StoreInRepositoryAction extends ResourceAction {

    private final IOObject object;
    private RepositoryPath lastLocation;

    public StoreInRepositoryAction(IOObject object) {
        this(object, null);
    }

    public StoreInRepositoryAction(IOObject object, RepositoryPath initialLocation) {
        super(true, "store_in_repository", IOObjectService.getName(object.getClass()));
        this.object = object;
        this.lastLocation = initialLocation;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        RepositoryPath path = RepositoryLocationChooser.selectLocation(lastLocation, RepositoryPath.of(""), MainFrame.INSTANCE.getExtensionsMenu(), true, false,
                false, true, false, true, null);
        if (path != null) {
            // check for overwrite
            if (Files.exists(path) && SwingTools.showConfirmDialog("overwrite", ConfirmDialog.YES_NO_OPTION, path) != ConfirmDialog.YES_OPTION) {
                return;
            }
            lastLocation = path;
            ProgressThread storePT = new ProgressThread("store_ioobject", false, path.toShortString(20)) {

                @Override
                public void run() {
                    try {
                        Entries.write(path, MetaData.forIOObject(object), object);
                    } catch (IOException ex) {
                        SwingTools.showSimpleErrorMessage("cannot_store_obj_at_location", ex, path, ex.getMessage());
                    }
                }
            };
            storePT.setIndeterminate(true);
            storePT.start();
        }
    }
}
