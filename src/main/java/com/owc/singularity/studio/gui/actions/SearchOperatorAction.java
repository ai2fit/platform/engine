/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.io.Serial;

import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.dialog.SearchOperatorDialog;
import com.owc.singularity.studio.gui.tools.ResourceAction;


/**
 * Creates a new {@link SearchOperatorDialog} and makes it visible
 * 
 * @author Alexander Mahler
 */
public class SearchOperatorAction extends ResourceAction {

    @Serial
    private static final long serialVersionUID = 1705320358099886593L;

    public SearchOperatorAction() {
        super("search");
        setCondition(Condition.PROCESS_RENDERER_IS_VISIBLE, ConditionReaction.MANDATORY);
    }

    public SearchOperatorAction(String key) {
        super(true, key);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        new SearchOperatorDialog(MainFrame.INSTANCE).setVisible(true);
    }

}
