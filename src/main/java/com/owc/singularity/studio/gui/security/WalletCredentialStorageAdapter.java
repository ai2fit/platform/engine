package com.owc.singularity.studio.gui.security;

import java.net.URI;

import com.owc.singularity.repository.security.CredentialStorage;

public class WalletCredentialStorageAdapter implements CredentialStorage<String, char[]> {

    @Override
    public boolean store(URI resourceUri, String principal, char[] credential) {
        if (principal == null)
            return false;
        Wallet.getInstance().addEntry(resourceUri, new UserCredential(resourceUri, principal, credential));
        return true;
    }

    @Override
    public char[] retrieve(URI resourceUri, String principal) {
        UserCredential userCredential = Wallet.getInstance().getEntry(resourceUri, principal);
        if (userCredential != null) {
            return userCredential.getPassword();
        }
        return null;
    }

    @Override
    public void close() {
        // do nothing
    }
}
