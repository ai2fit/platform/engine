/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.background;


import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;
import com.owc.singularity.studio.gui.editor.action.ProcessChangingAction;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;


/**
 * This class manages the process background background image which can be added/edited in the
 * process context editor.
 *
 * @author Marco Boeck
 * @since 7.0.0
 *
 */
public final class ProcessBackgroundImageVisualizer {

    /** the process renderer */
    private final ProcessEditorPanel view;

    /** the draw decorator */
    private final ProcessBackgroundImageDecorator decorator;

    /**
     * Creates the visualizer for {@link ProcessBackgroundImage}s.
     *
     * @param view
     *            the proces renderer instance
     */
    public ProcessBackgroundImageVisualizer(final ProcessEditorPanel view) {
        this.view = view;
        this.decorator = new ProcessBackgroundImageDecorator(view);

        // start background image decorators
        decorator.registerDecorators();
    }

    /**
     * Creates an action which can be used to set the {@link ProcessBackgroundImage}.
     *
     * @param process
     *            the process for which to set the background image. Can be {@code null} for first
     *            process at action event time
     * @return the action, never {@code null}
     */
    public ResourceAction makeSetBackgroundImageAction(final ExecutionUnit process) {

        return new ProcessChangingAction(true, "process_background.set") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                final ExecutionUnit targetProcess = process != null ? process : view.getModel().getProcess(0);

                ButtonDialog dialog = createBackgroundImageDialog(targetProcess, view.getModel());
                dialog.setVisible(true);
            }

        };
    }

    /**
     * Creates an action which can be used to remove the {@link ProcessBackgroundImage}.
     *
     * @param process
     *            the process for which to remove the background image. Can be {@code null} for
     *            first process at action event time
     * @return the action, never {@code null}
     */
    public ResourceAction makeRemoveBackgroundImageAction(final ExecutionUnit process) {

        return new ProcessChangingAction(true, "process_background.remove") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {

                ExecutionUnit targetProcess = process;
                if (process == null) {
                    targetProcess = view.getModel().getProcess(0);
                }

                view.getModel().removeBackgroundImage(targetProcess);
                view.getModel().fireMiscChanged();
                // dirty hack to trigger a process update
                targetProcess.getEnclosingOperator().rename(targetProcess.getEnclosingOperator().getName());
            }
        };
    }

    /**
     * Creates a dialog to set the background image of the given process.
     * 
     * @param process
     *            the process for which the background image should be set
     * @param model
     *            the process renderer model instance
     * @return the dialog
     */
    private static ButtonDialog createBackgroundImageDialog(final ExecutionUnit process, final ProcessRendererModel model) {
        if (process == null) {
            throw new IllegalArgumentException("pipeline must not be null!");
        }
        if (model == null) {
            throw new IllegalArgumentException("model must not be null!");
        }

        final JPanel mainPanel = new JPanel(new BorderLayout());

        ProcessBackgroundImage prevImg = model.getBackgroundImage(process);
        final RepositoryLocationChooser chooser = new RepositoryLocationChooser(null, null, prevImg != null ? prevImg.getLocation() : null, true, false, true,
                false);
        mainPanel.add(chooser, BorderLayout.CENTER);

        return new ButtonDialog(ApplicationFrame.getApplicationFrame(), "process_background", ModalityType.DOCUMENT_MODAL, new Object[] {}) {

            private static final long serialVersionUID = 1L;

            {
                layoutDefault(mainPanel, NORMAL, makeOkButton(), makeCancelButton());
            }

            @Override
            protected void ok() {
                ProcessBackgroundImage img = null;
                img = new ProcessBackgroundImage(-1, -1, -1, -1, chooser.getResult(), process);
                model.setBackgroundImage(img);
                model.fireMiscChanged();

                // dirty hack to trigger a process update
                process.getEnclosingOperator().rename(process.getEnclosingOperator().getName());

                super.ok();
            }
        };
    }

}
