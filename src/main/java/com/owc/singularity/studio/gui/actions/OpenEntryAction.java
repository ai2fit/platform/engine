/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.actions;


import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.SwingUtilities;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.object.ConnectionParametersIOObject;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.repository.actions.context.EditConnectionAction;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Start the corresponding action.
 *
 * @author Ingo Mierswa
 */
public class OpenEntryAction extends ResourceAction {

    private static final long serialVersionUID = -323403851840397447L;

    public OpenEntryAction() {
        super("open");
        setCondition(Condition.PROCESS_PAUSED, ConditionReaction.DISALLOWED);
        setCondition(Condition.PROCESS_RUNNING, ConditionReaction.DISALLOWED);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        RepositoryPath location = RepositoryLocationChooser.selectLocation(null, null, MainFrame.INSTANCE.getExtensionsMenu(), true, false, true);
        if (location != null) {
            Entry entry = Entries.getEntry(location);
            open(entry);
        }
    }

    /**
     * Loads the data held by the given entry (in the background) and opens it as a result.
     */
    public static void showAsResult(final Entry data) {
        if (data == null) {
            throw new IllegalArgumentException("data entry must not be null");
        }
        final ProgressThread downloadProgressThread = new ProgressThread("download_from_repository") {

            @Override
            public void run() {
                try {
                    IOObject result = data.loadData(IOObject.class, ModuleService.getMajorClassLoader());
                    if (isCancelled()) {
                        return;
                    }
                    result.setSourcePath(data.getPath());
                    MainFrame.INSTANCE.getResultDisplay().showResult(result, SwingTools.createIcon("16/folder.png"));
                } catch (Exception e1) {
                    SwingTools.showSimpleErrorMessage("cannot_fetch_data_from_repository", e1);
                }
            }
        };
        downloadProgressThread.start();
    }

    /**
     * This method will open the process specified by the process location. If showInfo is true, the
     * description of the process will be shown depending on the fact if this feature is enabled or
     * disabled in the settings. So if you don't want to silently load a process, this should be
     * true.
     */
    public static void showProcess(final Entry processEntry) {
        ProgressThread openProgressThread = new ProgressThread("open_file") {

            @Override
            public void run() {
                getProgressListener().setTotal(100);
                getProgressListener().setCompleted(10);
                try {
                    if (isCancelled()) {
                        return;
                    }
                    SwingUtilities.invokeLater(() -> MainFrame.INSTANCE.openProcess(processEntry));
                } catch (Exception e) {
                    SwingTools.showSimpleErrorMessage("while_loading", e, processEntry.getPath(), e.getMessage());
                } finally {
                    getProgressListener().complete();
                }
            }
        };
        openProgressThread.start();
    }

    public static void open(String path) {
        open(Entries.getEntry(RepositoryPath.of(path)));
    }

    public static void open(Entry entry) {
        try {
            if (entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
                showProcess(entry);
            } else if (entry.isInstanceOf(ConnectionParametersIOObject.class, ModuleService.getMajorClassLoader())) {
                showConnectionInformationDialog(entry);
            } else if (entry.isInstanceOf(IOObject.class, ModuleService.getMajorClassLoader())) {
                showAsResult(entry);
            } else {
                throw new IOException("Cannot open entries of type " + entry.getContentType() + ".");
            }
        } catch (Exception e) {
            SwingTools.showSimpleErrorMessage("while_loading", e, entry.getPath(), e.getMessage());
        }
    }

    /**
     * ConnectionManagement Frontend : show a dialog
     *
     * @param connectionEntry
     *            the entry to be shown
     * @since 9.3
     */
    public static void showConnectionInformationDialog(Entry connectionEntry) {
        EditConnectionAction.openConnection(connectionEntry.getPath(), false);
    }
}
