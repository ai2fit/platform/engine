package com.owc.singularity.studio.gui.repository.mount;

import java.util.Map;

import com.owc.singularity.repository.mount.defer.DeferredRepositoryMount;
import com.owc.singularity.studio.gui.Form;

public class DeferredMountConfigurationUIProvider implements MountConfigurationUIProvider {

    @Override
    public boolean supportsCreateUI() {
        return false;
    }

    @Override
    public Form<Map<String, String>> getCreateUI(String mountType) {
        throw new UnsupportedOperationException("There is no create UI for Deferred mounts");
    }

    @Override
    public boolean supportsEditUI() {
        return false;
    }

    @Override
    public Form<Map<String, String>> getEditUI(String mountType, Map<String, String> options) {
        throw new UnsupportedOperationException("There is no edit UI for Deferred mounts");
    }

    @Override
    public boolean isMountTypeSupported(String mountType) {
        return DeferredRepositoryMount.TYPE_NAME.equals(mountType);
    }
}
