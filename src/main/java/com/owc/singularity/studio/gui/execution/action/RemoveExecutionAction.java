package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionService;
import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.studio.gui.tools.ResourceAction;

public class RemoveExecutionAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private ConcurrentPipelineExecution execution;


    public RemoveExecutionAction(ConcurrentPipelineExecution execution) {
        super("toolkit.remove_execution");
        this.execution = execution;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ConcurrentExecutionService service = ConcurrentExecutionServiceProvider.getService();
        service.stopPipelineExecution(execution);
        service.removePipelineExecution(execution);
    }

}
