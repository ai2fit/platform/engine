package com.owc.singularity.studio.gui.vcs.model;


import javax.swing.tree.DefaultMutableTreeNode;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public abstract class AbstractMountTreeDiffTreeNode extends DefaultMutableTreeNode {

    private final RepositoryPath repositoryPath;
    private final EntryVersion sourceVersion;
    private String name;

    protected AbstractMountTreeDiffTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        this.sourceVersion = sourceVersion;
        this.repositoryPath = repositoryPath;
    }

    public String getName() {
        if (name == null) {
            name = repositoryPath.getFileName().toString();
        }
        return name;
    }

    public abstract Class<?> getType();

    public RepositoryPath getRepositoryPath() {
        return repositoryPath;
    }


    public EntryVersion getSourceVersion() {
        return sourceVersion;
    }

    @Override
    public String toString() {
        return getName();
    }
}
