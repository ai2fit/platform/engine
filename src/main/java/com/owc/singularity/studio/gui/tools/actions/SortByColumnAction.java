/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.actions;


import java.awt.event.ActionEvent;

import com.owc.singularity.studio.gui.LoggedAbstractAction;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.ExtendedJTableSorterModel;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Start the corresponding action.
 * 
 * @author Ingo Mierswa
 */
public class SortByColumnAction extends LoggedAbstractAction {

    private static final long serialVersionUID = -4154228837058332592L;

    private static final String DESCENDING_ICON_NAME = "sort_descending.png";
    private static final String ASCENDING_ICON_NAME = "sort_ascending.png";

    private final ExtendedJTable table;

    private final int direction;

    public SortByColumnAction(ExtendedJTable table, int direction, IconSize size) {
        super("Sort by Column (" + (direction == ExtendedJTableSorterModel.DESCENDING ? "Descending" : "Ascending") + ")",
                direction == ExtendedJTableSorterModel.DESCENDING ? SwingTools.createIcon(size.getSize() + "/" + DESCENDING_ICON_NAME)
                        : SwingTools.createIcon(size.getSize() + "/" + ASCENDING_ICON_NAME));
        this.table = table;
        this.direction = direction;
        putValue(SHORT_DESCRIPTION, "Sorts the table according to this column.");
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        this.table.setSortingStatus(this.direction, true);
    }
}
