/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.results;


import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.swing.*;

import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.usagestats.ActionStatisticsCollector;
import com.owc.singularity.studio.gui.IOObjectGUIService;
import com.owc.singularity.studio.gui.renderer.DefaultTextRenderer;
import com.owc.singularity.studio.gui.renderer.Renderer;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.components.ButtonBarCardPanel;
import com.owc.singularity.studio.gui.tools.components.CardSelectionEvent;
import com.owc.singularity.studio.gui.tools.components.CardSelectionListener;
import com.owc.singularity.studio.gui.tools.components.ResourceCard;


/**
 * Static methods to generate result visualization components etc.
 *
 * @author Simon Fischer
 */
public class ResultDisplayTools {

    public static final String IOOBJECT_USER_DATA_KEY_RENDERER = ResultDisplayTools.class.getName() + ".renderer";

    static final String CLIENT_PROPERTY_SINGULARITY_RESULT_NAME_HTML = "singularity.result.name.html";
    static final String CLIENT_PROPERTY_SINGULARITY_RESULT_ICON = "singularity.result.icon";
    static final String CLIENT_PROPERTY_SINGULARITY_RESULT_NAME = "singularity.result.name";

    private static final String DEFAULT_RESULT_ICON_NAME = "presentation_chart.png";

    /** the icon shown for "under construction" tabs */
    private static final ImageIcon WAIT_ICON = SwingTools.createIcon("24/hourglass.png");
    private static final ImageIcon ERROR_ICON = SwingTools.createIcon("24/error.png");

    private static Icon defaultResultIcon;

    /**
     * In these cases the unnecessary additional panel is suppressed
     */
    private static final Set<String> NO_CARD_KEYS = new HashSet<>(Arrays.asList("collection", "metamodel", "delegation_model"));

    static {
        defaultResultIcon = SwingTools.createIcon("16/" + DEFAULT_RESULT_ICON_NAME);
    }

    public static JPanel createVisualizationComponent(IOObject resultObject, String usedResultName) {
        return createVisualizationComponent(resultObject, usedResultName, true);
    }

    /**
     * Creates a panel which centers an error message.
     *
     * @param error
     *            the message to display
     * @return
     */
    public static JPanel createErrorComponent(String error) {
        JPanel panel = new JPanel(new BorderLayout());
        JLabel label = new JLabel(error, ERROR_ICON, SwingConstants.CENTER);
        panel.add(label, BorderLayout.CENTER);
        return panel;
    }

    /**
     * @param showCards
     *            if <code>false</code> the cards on the left side of the visualization component
     *            will not be shown
     */
    public static JPanel createVisualizationComponent(IOObject result, String usedResultName, final boolean showCards) {
        Collection<Renderer> renderers = IOObjectGUIService.getRenderers(result);

        // fallback to default toString method!
        if (renderers.isEmpty()) {
            renderers.add(new DefaultTextRenderer());
        }

        // constructing panel of renderers
        final ButtonBarCardPanel visualisationComponent = new ButtonBarCardPanel(NO_CARD_KEYS, showCards);
        final IOObject resultObject = result;
        for (final Renderer renderer : renderers) {
            String cardKey = toCardName(renderer.getName());
            final ResourceCard card = new ResourceCard(cardKey, "result_view." + cardKey);

            // create a placeholder panel which shows "under construction" so the results can be
            // displayed immediately
            final JPanel inConstructionPanel = new JPanel(new BorderLayout());
            String humanName = I18N.getGUIMessageOrNull("gui.cards.result_view." + cardKey + ".title");
            if (humanName == null) {
                humanName = cardKey;
            }
            JLabel waitLabel = new JLabel(I18N.getGUILabel("result_construction", humanName));
            waitLabel.setIcon(WAIT_ICON);
            waitLabel.setHorizontalTextPosition(SwingConstants.TRAILING);
            waitLabel.setHorizontalAlignment(SwingConstants.CENTER);
            inConstructionPanel.add(waitLabel, BorderLayout.CENTER);
            visualisationComponent.addCard(card, inConstructionPanel);
            try {
                ProgressThread resultThread = new ProgressThread("creating_result_tab", false, humanName) {

                    @Override
                    public void run() {
                        getProgressListener().setTotal(100);
                        getProgressListener().setCompleted(1);

                        final Component rendererComponent = renderer.getVisualizationComponent(resultObject);
                        getProgressListener().setCompleted(60);

                        if (rendererComponent != null) {

                            if (rendererComponent instanceof JComponent) {
                                ((JComponent) rendererComponent).setBorder(null);
                            }
                            getProgressListener().setCompleted(80);

                            SwingUtilities.invokeLater(() -> {
                                // update container
                                // renderer is finished, remove placeholder
                                inConstructionPanel.removeAll();

                                // add real renderer
                                inConstructionPanel.add(rendererComponent, BorderLayout.CENTER);

                                inConstructionPanel.revalidate();
                                inConstructionPanel.repaint();
                            });
                            getProgressListener().complete();
                        }
                    }
                };

                // start result calculation progress thread
                resultThread.start();
            } catch (Exception e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.processeditor.results.ResultDisplayTools.error_creating_renderer", e, e);
                String errorMsg = I18N.getErrorMessage("result_display.error_creating_renderer", renderer.getName());
                visualisationComponent.addCard(card, new JLabel(errorMsg));
            }
        }

        // report statistics
        visualisationComponent.addCardSelectionListener(new CardSelectionListener() {

            private String lastKey = "";

            @Override
            public void cardSelected(CardSelectionEvent e) {
                if (e != null) {
                    String key = e.getCardKey();
                    if (key != null && !key.equals(lastKey)) {
                        ActionStatisticsCollector.getInstance().log(ActionStatisticsCollector.TYPE_RENDERER, key, "select");
                        this.lastKey = key;
                    }
                }
            }
        });
        // result panel
        final JPanel resultPanel = new JPanel(new BorderLayout());
        resultPanel.putClientProperty("main.component", visualisationComponent);

        resultPanel.add(visualisationComponent, BorderLayout.CENTER);


        if (IOObjectGUIService.getIcon(resultObject.getClass()) != null) {
            resultPanel.putClientProperty(ResultDisplayTools.CLIENT_PROPERTY_SINGULARITY_RESULT_ICON,
                    IOObjectGUIService.getIcon(resultObject.getClass(), IconSize.SMALL));
        } else {
            resultPanel.putClientProperty(ResultDisplayTools.CLIENT_PROPERTY_SINGULARITY_RESULT_ICON, defaultResultIcon);
        }

        resultPanel.putClientProperty(ResultDisplayTools.CLIENT_PROPERTY_SINGULARITY_RESULT_NAME, usedResultName);
        String source = resultObject.getSource() != null ? resultObject.getSource() : "";
        resultPanel.putClientProperty(ResultDisplayTools.CLIENT_PROPERTY_SINGULARITY_RESULT_NAME_HTML,
                "<html>" + usedResultName + "<br/><small>" + source + "</small></html>");
        return resultPanel;
    }

    private static String toCardName(String name) {
        return name.toLowerCase().replace(' ', '_');
    }

    public static ResultDisplay makeResultDisplay() {
        return new ResultDisplay();
    }

}
