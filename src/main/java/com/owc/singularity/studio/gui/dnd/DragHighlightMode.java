package com.owc.singularity.studio.gui.dnd;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.studio.StudioProperties;

public enum DragHighlightMode {

    FULL, BORDER, NONE;

    public static DragHighlightMode getDragHighlightMode() {
        String dragParameter = PropertyService.getParameterValue(StudioProperties.PROPERTY_DRAG_TARGET_HIGHLIGHTING);
        if (dragParameter.equals(StudioProperties.PROPERTY_DRAG_TARGET_HIGHLIGHTING_VALUES[0])) {
            return FULL;
        } else if (dragParameter.equals(StudioProperties.PROPERTY_DRAG_TARGET_HIGHLIGHTING_VALUES[1])) {
            return BORDER;
        } else {
            return NONE;
        }
    }
}
