/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.event.OnDeleteRepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.ProgressThreadDialog;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * This action deletes the selected entry.
 *
 * @author Simon Fischer, Adrian Wilke
 */
public class DeleteRepositoryEntryAction extends AbstractRepositoryContextAction<Boolean> {

    private static final long serialVersionUID = 1L;

    public static final String I18N_KEY = "repository_delete_entry";


    public DeleteRepositoryEntryAction(RepositoryTree tree) {
        super(tree, true, true, null, true, true, I18N_KEY);
    }

    private void delete(RepositoryPath location) throws IOException {
        Files.walkFileTree(location, new SimpleFileVisitor<>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (exc != null)
                    throw exc;
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }


    @Override
    protected Boolean configureAction(List<RepositoryTreeNode> entries) {
        if (SwingTools.showConfirmDialog("delete_entry", ConfirmDialog.YES_NO_OPTION) == ConfirmDialog.YES_OPTION) {
            return true;
        }
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, Boolean config, ProgressListener progressListener) {
        OnDeleteRepositoryTreeEventTrigger trigger = new OnDeleteRepositoryTreeEventTrigger(path);
        tree.invokeAfterTreeEvent(trigger, (ignored) -> {
            RepositoryPath existingParent = path.getParent();
            while (!Files.exists(existingParent)) {
                existingParent = existingParent.getParent();
            }
            tree.expandAndSelectIfExists(existingParent);
        });
        try {
            // recursively delete file
            delete(path);
        } catch (IOException e) {
            trigger.remove();
            // Retry-dialog on error
            String errorMessage = e.getMessage();
            errorMessage = errorMessage != null ? errorMessage.trim() : "";
            ConfirmDialog dialog = new ConfirmDialog(ProgressThreadDialog.getInstance(),
                    "error_in_delete_entry" + (!errorMessage.isEmpty() ? "_with_cause" : ""), ConfirmDialog.YES_NO_OPTION, false, path.toString(),
                    errorMessage);
            dialog.setVisible(true);
            if (dialog.getReturnOption() == ConfirmDialog.YES_OPTION) {
                executeAction(path, true, progressListener);
            } else {
                LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.error_during_deletion", e);
            }

        }
    }
}
