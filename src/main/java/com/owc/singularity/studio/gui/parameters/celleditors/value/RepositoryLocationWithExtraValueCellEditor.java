/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeRepositoryLocation;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.tools.ProgressThread;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;

/**
 * Abstract repository location cell editor that is specialized for a special {@link Entry} type and
 * adds a second button that allows to interact with the selected entry if it is of the specified
 * type.
 *
 * @see #getExtraActionKey()
 * @see #doExtraAction(RepositoryPath)
 * @see #getExpectedEntryClass()
 *
 * @author Marcel Seifert, Nils Woehler, Jan Czogalla
 * @since 9.3
 */
public abstract class RepositoryLocationWithExtraValueCellEditor extends RepositoryLocationValueCellEditor {

    private static final long serialVersionUID = -8457903601813592125L;
    private final JPanel surroundingPanel = new JPanel(new GridBagLayout());
    private final JButton extraButton = new JButton(new ResourceAction(true, getExtraActionKey()) {

        private static final long serialVersionUID = 1L;

        {
            putValue(NAME, null);
        }

        @Override
        public void loggedActionPerformed(ActionEvent e) {
            try {
                if (getTextField() != null) {
                    RepositoryPath path = getPathFromGUI();
                    doExtraAction(path);
                }
            } catch (IllegalArgumentException e1) {
                SwingTools.showVerySimpleErrorMessage("malformed_repository_location", getTextField().getText());
            }
        }
    });
    private final CellEditorListener listener = new CellEditorListener() {

        @Override
        public void editingStopped(ChangeEvent e) {
            checkExtraButtonEnabled();
        }

        @Override
        public void editingCanceled(ChangeEvent e) {
            // do nothing
        }
    };

    public RepositoryLocationWithExtraValueCellEditor(ParameterTypeRepositoryLocation type) {
        super(type);
        extraButton.setEnabled(false);

        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        surroundingPanel.add(getPanel(), gbc);

        gbc.gridx += 1;
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(0, 2, 0, 0);
        surroundingPanel.add(extraButton, gbc);
        addCellEditorListener(listener);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int col) {
        // ensure text field is filled with correct values
        super.getTableCellEditorComponent(table, value, isSelected, row, col);
        checkExtraButtonEnabled();
        return surroundingPanel;
    }

    @Override
    public void activate() {
        if (extraButton.isEnabled()) {
            extraButton.doClick();
        } else {
            super.activate();
        }
    }

    /** The i18n key for the extra action (i.e. second button). Only the icon will be used. */
    protected abstract String getExtraActionKey();

    /**
     * The action behind the second button.
     *
     * @param repositoryLocation
     *            the location to execute the action on; will never be called with {@code null}
     */
    protected abstract void doExtraAction(RepositoryPath repositoryLocation);

    /** Returns the expected (super) class of allowed entries. */
    protected abstract Class<?> getExpectedEntryClass();

    /**
     * Checks whether the provided repository location is valid and is of the correct type.
     *
     * @see #getExpectedEntryClass()
     */
    private void checkExtraButtonEnabled() {
        final String location = getTextFieldText();
        ProgressThread t = new ProgressThread("check_process_location_available", false, location) {

            @Override
            public void run() {
                RepositoryPath pathFromGUI = getPathFromGUI();
                boolean enabled = pathFromGUI != null && pathFromGUI.isAbsolute() && Files.exists(pathFromGUI);
                if (enabled) {
                    try {
                        // check whether the lcoation can be found and is of correct type
                        enabled = Entries.getEntry(pathFromGUI).isInstanceOf(getExpectedEntryClass(), ModuleService.getMajorClassLoader());
                    } catch (IOException | ClassNotFoundException e) {
                        enabled = false;
                    }
                }
                final boolean enable = enabled;
                SwingUtilities.invokeLater(() -> extraButton.setEnabled(enable));
            }
        };
        t.setIndeterminate(true);
        t.start();

    }

    /**
     * @return the text of the text field which cannot be null
     */
    private String getTextFieldText() {
        return getTextField().getText() != null ? getTextField().getText() : "";
    }

    private RepositoryPath getPathFromGUI() {
        String pathString = getTextFieldText();
        if (pathString.isEmpty())
            return null;
        RepositoryPath path = RepositoryPath.of(pathString);

        RepositoryPath processLocation = null;
        if (getOperator() != null && getOperator().getPipeline() != null)
            processLocation = getOperator().getPipeline().getPath();
        if (processLocation == null)
            return RepositoryPath.of(pathString);
        RepositoryPath absoluteAnchor = processLocation.getParent();
        if (absoluteAnchor != null)
            try {
                path = absoluteAnchor.resolve(path);
            } catch (IllegalArgumentException e) {
            }
        return path;
    }
}
