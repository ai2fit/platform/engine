/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.actions.context;


import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.Action;

import com.owc.singularity.engine.tools.ProgressListener;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;


/**
 * This action is the standard paste action.
 * 
 * @author Simon Fischer
 */
public class PasteEntryRepositoryAction extends AbstractRepositoryContextAction<String> {

    private static final long serialVersionUID = 1L;

    public PasteEntryRepositoryAction(RepositoryTree tree) {
        super(tree, true, true, null, false, true, "repository_paste");
        putValue(ACTION_COMMAND_KEY, "paste");
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        Action a = tree.getActionMap().get(action);
        if (a != null) {
            a.actionPerformed(new ActionEvent(tree, ActionEvent.ACTION_PERFORMED, null));
        }
    }

    @Override
    protected String configureAction(List<RepositoryTreeNode> entries) {
        // not needed because we override actionPerformed(ActionEvent e) which is the only caller
        return null;
    }

    @Override
    public void executeAction(RepositoryPath path, String config, ProgressListener progressListener) {
        // not needed because we override actionPerformed(ActionEvent e) which is the only caller

    }
}
