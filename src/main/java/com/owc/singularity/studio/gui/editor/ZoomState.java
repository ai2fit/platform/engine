package com.owc.singularity.studio.gui.editor;

public class ZoomState {

    private final double[] availableFactors;
    private final int zoomFactorIndex;

    public ZoomState(double[] availableFactors, int zoomFactorIndex) {
        this.availableFactors = availableFactors;
        this.zoomFactorIndex = zoomFactorIndex;
    }

    public double getZoom() {
        return availableFactors[zoomFactorIndex] * 100;
    }

    public boolean canZoomIn() {
        return zoomFactorIndex < availableFactors.length - 1;
    }

    public boolean canZoomOut() {
        return zoomFactorIndex > 0;
    }

}
