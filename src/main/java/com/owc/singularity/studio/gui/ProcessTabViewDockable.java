package com.owc.singularity.studio.gui;

import java.awt.Component;
import java.awt.datatransfer.Transferable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.DefaultSingleSelectionModel;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang.StringUtils;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.ImplementingPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.dnd.DragListener;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessStorageListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotation;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessEditorModelEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererAnnotationEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererModelEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererOperatorEvent;
import com.owc.singularity.studio.gui.repository.RepositoryTreeCellRenderer;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;

public class ProcessTabViewDockable implements Dockable {

    public static final String CONSTANTS_NEW_PROCESS_LABEL = "New Pipeline";
    public static final String CONSTANTS_NEW_PROCESS_TOOLTIP = "Unsaved new pipeline";
    public static final String CONSTANTS_UNSAVED_CHANGES_LABEL = "*";
    public static final String CONSTANTS_UNSAVED_CHANGES_TOOLTIP = "(not saved)";

    private Predicate<ProcessPanel> tabCanBeClosedPredicate;
    private HashSet<String> unsavedProcessPanelNames;
    private final JTabbedPane tabbedPane;
    private final ConditionedSingleSelectionModel selectionModel;
    /** the drag and drop listener */
    private final transient DragListener dragListener;
    public static final String PROCESS_PANEL_DOCK_KEY = "process_panel";
    private static final DockKey DOCK_KEY = new ResourceDockKey("process_panel");
    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
        DOCK_KEY.setCloseEnabled(false);
    }

    public ProcessTabViewDockable() {
        this.tabbedPane = new JTabbedPane();
        this.selectionModel = new ConditionedSingleSelectionModel();
        tabbedPane.setModel(this.selectionModel);
        tabbedPane.addMouseListener(new MouseEventRedirector());
        this.dragListener = new DelegatingDragListener(() -> getMainProcessPanel().getProcessEditor().getDragListener());
        this.unsavedProcessPanelNames = new HashSet<>();
    }

    public ProcessPanel createPanel(AbstractPipeline process, boolean readOnly) {
        RepositoryPath processPath = process.getPath();
        String tabTitle = CONSTANTS_NEW_PROCESS_LABEL;
        String tabTooltip = CONSTANTS_NEW_PROCESS_TOOLTIP;
        // create new process tab
        if (processPath == null) {
            int counter = 1;
            while (true) {
                if (!this.unsavedProcessPanelNames.contains(tabTitle)) {
                    this.unsavedProcessPanelNames.add(tabTitle);
                    break;
                }
                String potentialName = CONSTANTS_NEW_PROCESS_LABEL + " (" + counter + ")";
                if (!this.unsavedProcessPanelNames.contains(potentialName)) {
                    tabTitle = potentialName;
                    tabTooltip = CONSTANTS_NEW_PROCESS_TOOLTIP + " (" + counter + ")";
                    this.unsavedProcessPanelNames.add(potentialName);
                    break;
                }
                counter++;
            }
        } else {
            if (readOnly) {
                tabTitle = processPath.isHead() ? processPath.getFileName().toString() + " (readonly)"
                        : processPath.getFileName().toString() + " #" + processPath.getVersionId().substring(0, 8);
                tabTooltip = processPath.isHead() ? processPath.toString() : processPath + " #" + processPath.getVersionId();
            } else {
                tabTitle = processPath.getFileName().toString();
                tabTooltip = processPath.toString();
            }
        }
        ProcessPanel processPanel = new ProcessPanel();
        processPanel.setReadOnly(readOnly);
        processPanel.setName(tabTitle);

        UpdateTabBindingsListener bindingsListener = new UpdateTabBindingsListener(processPanel);
        processPanel.addProcessStorageListener(bindingsListener);
        processPanel.getProcessEditor().addProcessEditorEventListener(bindingsListener);
        processPanel.getProcessEditor().getModel().registerEditorModelEventListener(bindingsListener);

        processPanel.setProcess(process);


        int index;
        synchronized (tabbedPane) {
            index = tabbedPane.getTabCount();
            tabbedPane.addTab(tabTitle, derivePipelineIcon(process), processPanel, tabTooltip);
        }
        CloseableTabComponent component = new CloseableTabComponent(tabbedPane, (Component panel) -> {
            if (tabCanBeClosedPredicate != null)
                return tabCanBeClosedPredicate.test((ProcessPanel) panel);
            return true;
        }, panel -> close((ProcessPanel) panel, false));
        tabbedPane.setTabComponentAt(index, component);
        component.setEnabled(!selectionModel.isLocked());
        tabbedPane.setEnabledAt(index, !selectionModel.isLocked());


        return processPanel;
    }

    public Icon derivePipelineIcon(AbstractPipeline pipeline) {
        return RepositoryTreeCellRenderer.getPipelineIconFromMetadata(pipeline.getEffectiveType().toString(), pipeline instanceof ImplementingPipeline,
                pipeline.isAbstract());
    }

    public boolean select(ProcessPanel processPanel) {
        int index = tabbedPane.indexOfComponent(processPanel);
        if (index == -1) {
            return false;
        }
        tabbedPane.setSelectedIndex(index);
        return true;
    }

    public synchronized void lockTab(ProcessPanel processPanel) {
        int tabIndex = tabbedPane.indexOfComponent(processPanel);
        if (tabIndex == -1) {
            throw new IllegalArgumentException("PipelinePanel does not belong to the tabbed pane");
        }
        tabbedPane.setSelectedIndex(tabIndex);
        selectionModel.setLocked(true);
        int count = tabbedPane.getTabCount();
        for (int i = 0; i < count; i++) {
            if (i == tabIndex)
                continue;
            Component tabComponentAt = tabbedPane.getTabComponentAt(i);
            int finalI = i;
            SwingUtilities.invokeLater(() -> {
                tabComponentAt.setEnabled(false);
                tabbedPane.setEnabledAt(finalI, false);
            });
        }
    }

    public synchronized void unlock() {
        selectionModel.setLocked(false);
        int count = tabbedPane.getTabCount();
        for (int i = 0; i < count; i++) {
            Component tabComponentAt = tabbedPane.getTabComponentAt(i);
            int finalI = i;
            SwingUtilities.invokeLater(() -> {
                tabComponentAt.setEnabled(true);
                tabbedPane.setEnabledAt(finalI, true);
            });
        }
    }

    public void setTabCloseCondition(Predicate<ProcessPanel> tabCanBeClosedPredicate) {
        this.tabCanBeClosedPredicate = tabCanBeClosedPredicate;
    }

    public ProcessPanel getMainProcessPanel() {
        return (ProcessPanel) tabbedPane.getSelectedComponent();
    }

    public List<ProcessPanel> getProcessPanels() {
        LinkedList<ProcessPanel> panels = new LinkedList<>();
        for (int i = 0; i < tabbedPane.getComponentCount(); i++) {
            Component component = tabbedPane.getComponent(i);
            if (component instanceof ProcessPanel)
                panels.add((ProcessPanel) component);
        }
        return panels;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    @Override
    public Component getComponent() {
        return tabbedPane;
    }

    public DragListener getDragListener() {
        return dragListener;
    }


    public void addTabChangeListener(ChangeListener listener) {
        // @formatter:off
        /*
         * The tabbedPane only fires in 2 cases:
         * 1. when the underlying selection-model (in our case: ConditionedSingleSelectionMode, see below) fires (changes its index)
         * 2. when a tab is removed but the index of the selected tab does not change
         *
         * The second case however does not fire an update in the model since the index does not
         * change. As the model only cares about the index this is a reasonable implementation. This mean however,
         * that in our case the change listeners need to be registered on the tabbedPane itself and not on
         * the model, as a tab change without a resulting index change still results in a change of the selected process.
         *
         */
        // @formatter:on
        tabbedPane.addChangeListener(listener);
    }

    public void removeTabChangeListener(ChangeListener listener) {
        tabbedPane.removeChangeListener(listener);
    }

    public ProcessPanel findPanelWith(RepositoryPath processPath) {
        int index = findTabIndexOf(processPath);
        if (index != -1) {
            return (ProcessPanel) tabbedPane.getComponentAt(index);
        }
        return null;
    }

    public ProcessPanel findPanelWith(AbstractPipeline pipeline) {
        int size = tabbedPane.getTabCount();
        for (int i = 0; i < size; i++) {
            ProcessPanel processPanel = (ProcessPanel) tabbedPane.getComponentAt(i);
            if (processPanel.getProcess() == pipeline)
                return processPanel;
        }
        // if we can't find, we use path to be sure if pipeline object changed
        return findPanelWith(pipeline.getPath());
    }

    private int findTabIndexOf(RepositoryPath processPath) {
        int size = tabbedPane.getTabCount();
        for (int i = 0; i < size; i++) {
            ProcessPanel processPanel = (ProcessPanel) tabbedPane.getComponentAt(i);
            if (processPanel.getProcess() != null && processPath.equals(processPanel.getProcess().getPath()))
                return i;
        }
        return -1;
    }

    public boolean close(ProcessPanel processPanel, boolean force) {
        int index = tabbedPane.indexOfComponent(processPanel);
        if (index != -1) {
            if (!force && processPanel.isEdited()) {
                // switch to the tab with unsaved changes before closing
                select(processPanel);
            }
            if (force || processPanel.isCloseable(true)) {
                this.unsavedProcessPanelNames.remove(tabbedPane.getTabComponentAt(index).getName());
                // disable actions of current panel must come before actual switching of panels
                processPanel.getActions().setProcess(null);
                processPanel.getActions().setSelection(null);
                processPanel.getActions().enableActions();
                tabbedPane.remove(index);
                return true;
            }
        }
        return false;
    }

    private static class DelegatingDragListener implements DragListener {

        private final Supplier<DragListener> listenerSupplier;

        public DelegatingDragListener(Supplier<DragListener> listenerSupplier) {
            this.listenerSupplier = listenerSupplier;
        }

        @Override
        public void dragStarted(Transferable t) {
            listenerSupplier.get().dragStarted(t);
        }

        @Override
        public void dragEnded() {
            listenerSupplier.get().dragEnded();
        }
    }

    private class UpdateTabBindingsListener implements ProcessStorageListener, ProcessEditorEventListener, ProcessEditorModelEventListener {

        private final ProcessPanel processPanel;

        public UpdateTabBindingsListener(ProcessPanel processPanel) {
            this.processPanel = processPanel;
        }

        @Override
        public void onStore(AbstractPipeline process, RepositoryPath storedAt) {
            int index = tabbedPane.indexOfComponent(processPanel);
            if (index != -1) {
                tabbedPane.setTitleAt(index, generateTabTitle(process, storedAt));
                tabbedPane.setToolTipTextAt(index, generateTabToolTipText(process, storedAt));
                tabbedPane.setIconAt(index, derivePipelineIcon(process));
                tabbedPane.getTabComponentAt(index).invalidate();
            }
        }

        @Override
        public void onLoad(AbstractPipeline process, RepositoryPath loadedFrom) {
            int index = tabbedPane.indexOfComponent(processPanel);
            if (index != -1) {
                tabbedPane.setTitleAt(index, generateTabTitle(process, loadedFrom));
                tabbedPane.setToolTipTextAt(index, generateTabToolTipText(process, loadedFrom));
                tabbedPane.setIconAt(index, derivePipelineIcon(process));
                tabbedPane.getTabComponentAt(index).invalidate();
            }
        }

        @Override
        public void onProcessEdit(AbstractPipeline process) {
            int index = tabbedPane.indexOfComponent(processPanel);
            if (index != -1) {
                String title = tabbedPane.getTitleAt(index);
                String toolTipTextAt = tabbedPane.getToolTipTextAt(index);
                boolean edited = processPanel.isEdited();
                if (edited) {
                    tabbedPane.setTitleAt(index, title.endsWith(CONSTANTS_UNSAVED_CHANGES_LABEL) ? title : title + CONSTANTS_UNSAVED_CHANGES_LABEL);
                    tabbedPane.setToolTipTextAt(index,
                            toolTipTextAt.endsWith(CONSTANTS_UNSAVED_CHANGES_TOOLTIP) ? toolTipTextAt : toolTipTextAt + CONSTANTS_UNSAVED_CHANGES_TOOLTIP);
                    tabbedPane.setIconAt(index, derivePipelineIcon(processPanel.getProcess()));
                } else {
                    tabbedPane.setTitleAt(index, StringUtils.removeEnd(title, CONSTANTS_UNSAVED_CHANGES_LABEL));
                    tabbedPane.setToolTipTextAt(index, StringUtils.removeEnd(toolTipTextAt, CONSTANTS_UNSAVED_CHANGES_TOOLTIP));
                }
                tabbedPane.getTabComponentAt(index).invalidate();
            }
        }

        @Override
        public void onProcessLoad(AbstractPipeline process) {
            if (processPanel.getProcess() == null) {
                onLoad(process, process.getPath());
            } else {
                onProcessEdit(process);
            }
        }

        private String generateTabTitle(AbstractPipeline process, RepositoryPath path) {
            return path == null ? CONSTANTS_NEW_PROCESS_LABEL
                    : path.isHead() ? path.getFileName().toString() : path.getFileName().toString() + " #" + path.getVersionId().substring(0, 8);
        }

        private String generateTabToolTipText(AbstractPipeline process, RepositoryPath path) {
            return path == null ? CONSTANTS_NEW_PROCESS_TOOLTIP
                    : path.isHead() ? path.getFileName().toString() : path.getFileName().toString() + " #" + path.getVersionId();
        }

        @Override
        public void modelChanged(ProcessRendererModelEvent e) {}

        @Override
        public void operatorsChanged(ProcessRendererOperatorEvent e, Collection<Operator> operators) {
            onProcessEdit(null);
        }

        @Override
        public void annotationsChanged(ProcessRendererAnnotationEvent e, Collection<WorkflowAnnotation> annotations) {
            onProcessEdit(null);
        }
    }

    private class MouseEventRedirector extends MouseAdapter {

        @Override
        public void mouseReleased(MouseEvent e) {
            redirectToTabComponent(e);
        }

        private void redirectToTabComponent(MouseEvent e) {
            int indexAtLocation = tabbedPane.indexAtLocation(e.getX(), e.getY());
            if (indexAtLocation != -1) {
                Component tabComponent = tabbedPane.getTabComponentAt(indexAtLocation);
                tabComponent.dispatchEvent(SwingUtilities.convertMouseEvent(tabbedPane, e, tabComponent));
            }
        }
    }

    private static class ConditionedSingleSelectionModel extends DefaultSingleSelectionModel {

        private boolean locked = false;

        @Override
        public void setSelectedIndex(int index) {
            if (locked)
                return;
            super.setSelectedIndex(index);
        }

        public void setLocked(boolean locked) {
            this.locked = locked;
        }

        public boolean isLocked() {
            return locked;
        }
    }
}
