/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor;


import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.event.FocusProcessChangeEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.parameters.GenericParameterPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ResourceDockKey;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.vlsolutions.swing.docking.DockKey;
import com.vlsolutions.swing.docking.Dockable;


/**
 * 
 * @author Simon Fischer
 * 
 */
public class DevelopmentExecutionContextPanel extends JPanel implements Dockable, FocusProcessChangeEventListener, ProcessEditorEventListener {

    public static final String DEVELOPMENT_CONTEXT_DOCKKEY = "development_context";

    private static final long serialVersionUID = 1L;

    private final GenericParameterPanel editor = new GenericParameterPanel();

    public DevelopmentExecutionContextPanel() {
        super(null);
        setLayout(new BorderLayout());
        ExtendedJScrollPane scrollPane = new ExtendedJScrollPane(editor);
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public void onFocusedProcessChange(ProcessPanel oldPanel, ProcessPanel currentlyFocusedPanel) {
        editor.setParameters(currentlyFocusedPanel != null ? currentlyFocusedPanel.getProcess().getDevelopmentContext().getParameters() : null);
        if (currentlyFocusedPanel != null) {
            SwingTools.setEnabledRecursive(editor, !currentlyFocusedPanel.isReadOnly());
        }
    }

    private final DockKey DOCK_KEY = new ResourceDockKey(DEVELOPMENT_CONTEXT_DOCKKEY);
    private Component dockComponent;
    {
        DOCK_KEY.setDockGroup(MainFrame.DOCK_GROUP_ROOT);
    }

    @Override
    public Component getComponent() {
        if (dockComponent == null) {
            dockComponent = this;
        }
        return dockComponent;
    }

    @Override
    public DockKey getDockKey() {
        return DOCK_KEY;
    }

    public void setPipeline(AbstractPipeline process) {
        editor.setParameters(process.getDevelopmentContext().getParameters());
    }

    @Override
    public void onProcessEdit(AbstractPipeline process) {
        editor.setParameters(process.getDevelopmentContext().getParameters());
    }
}
