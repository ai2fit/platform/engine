package com.owc.singularity.studio.gui.parameters.celleditors.value;


import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeMultipleSelection;
import com.owc.singularity.engine.pipeline.parameter.UndefinedParameterError;
import com.owc.singularity.studio.gui.parameters.MultipleSelectionParameterDialog;
import com.owc.singularity.studio.gui.tools.ResourceAction;

/**
 * A {@link PropertyValueCellEditor} for the {@link ParameterTypeMultipleSelection} parameter and
 * any of it descendants.
 *
 * @author Hatem Hamad
 */
public class MultipleSelectionValueCellEditor extends AbstractCellEditor implements PropertyValueCellEditor {

    private static final long serialVersionUID = -2387495714767785022L;

    protected String selectedOptionsString = "";
    protected Operator operator;
    protected JButton openEditorBtn;
    protected Insets openEditorBtnMargin = new Insets(0, 0, 0, 0);

    public MultipleSelectionValueCellEditor(final ParameterTypeMultipleSelection type) {
        this.openEditorBtn = new JButton(new ResourceAction(true, getOpenEditorButtonKey()) {

            private static final long serialVersionUID = -4890375754225485831L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                ArrayList<String> selectedOptions = new ArrayList<>();
                String combinedOptions = null;
                try {
                    if (operator != null) {
                        combinedOptions = operator.getParameters().getParameterAsSpecified(type.getKey());
                        if (combinedOptions == null) {
                            throw new UndefinedParameterError(type.getKey(), operator);
                        }
                    }
                } catch (UndefinedParameterError ignored) {
                }
                if (combinedOptions != null) {
                    selectedOptions.addAll(Arrays.asList(combinedOptions.split(type.getSeparatorRegex())));
                }
                MultipleSelectionParameterDialog dialog = getDialog(type, selectedOptions);
                dialog.setVisible(true);
                if (dialog.isOk()) {
                    selectedOptionsString = String.join(type.getSeparator(), dialog.getSelectedOptions());
                    fireEditingStopped();
                } else {
                    fireEditingCanceled();
                }
            }
        });
        openEditorBtn.setMargin(openEditorBtnMargin);
    }

    protected MultipleSelectionParameterDialog getDialog(final ParameterTypeMultipleSelection type, List<String> selectedOptions) {
        return new MultipleSelectionParameterDialog(type, selectedOptions);
    }

    protected String getOpenEditorButtonKey() {
        return "options";
    }

    @Override
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    @Override
    public boolean rendersLabel() {
        return false;
    }

    @Override
    public boolean useEditorAsRenderer() {
        return false;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return openEditorBtn;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return openEditorBtn;
    }

    @Override
    public void activate() {
        openEditorBtn.doClick();
    }

    @Override
    public Object getCellEditorValue() {
        return selectedOptionsString;
    }
}
