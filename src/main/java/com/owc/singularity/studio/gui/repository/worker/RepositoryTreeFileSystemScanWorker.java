package com.owc.singularity.studio.gui.repository.worker;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.model.AbstractRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.SwingWorkerWithCallback;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.context.Context;
import io.opentelemetry.context.Scope;

public class RepositoryTreeFileSystemScanWorker extends SwingWorkerWithCallback<Void, TreeChange> {

    private static final Logger log = LogManager.getLogger(RepositoryTreeFileSystemScanWorker.class.getName());
    private static final Tracer tracer = GlobalOpenTelemetry.getTracer("studio.repository");
    private Span workerSpan;

    private final AbstractRepositoryTreeModel treeModel;
    private final RepositoryPath target;
    private final Predicate<RepositoryPath> filter;
    private final int maxDepth;

    public RepositoryTreeFileSystemScanWorker(AbstractRepositoryTreeModel treeModel, RepositoryPath target, Predicate<RepositoryPath> pathPredicate,
            int maxDepth, Runnable callback) {
        super(callback == null ? ignored -> {} : ignored -> callback.run());
        this.treeModel = treeModel;
        this.target = target;
        this.filter = pathPredicate;
        this.maxDepth = maxDepth;
    }

    @Override
    protected Void doInBackground() throws Exception {
        workerSpan = tracer.spanBuilder("scan-repository-worker")
                .setAttribute("args.path", target.toString())
                .setAttribute("args.maxDepth", maxDepth)
                .setAttribute("args.filter", String.valueOf(filter))
                .startSpan();
        try (Scope ignored = workerSpan.makeCurrent()) {

            if (!Files.exists(target)) {
                workerSpan.addEvent("file.not.found");
                return null;
            }
            RepositoryTreeNode nodeForTarget = treeModel.getNodeFor(target);
            if (nodeForTarget == null) {
                // we should add this new entry
                Stack<RepositoryPath> unknownParentPaths = new Stack<>();
                unknownParentPaths.push(target.getParent());
                RepositoryTreeNode knownParent = null;
                while (!unknownParentPaths.isEmpty()) {
                    RepositoryPath parentPath = unknownParentPaths.pop();
                    knownParent = treeModel.getNodeFor(parentPath);
                    if (knownParent != null) {
                        // we found a known parent in the path
                        // add all unknown ancestors
                        while (!unknownParentPaths.isEmpty()) {
                            RepositoryPath newAncestorPath = unknownParentPaths.pop();
                            RepositoryTreeNode newEntry = new RepositoryTreeNode(newAncestorPath);
                            treeModel.addNode(knownParent, newEntry);
                            publish(new TreeChange.Insertion(knownParent, new RepositoryTreeNode[] { newEntry }));
                            knownParent = newEntry;
                        }
                    } else {
                        if (parentPath.getParent() == null) {
                            // we reached out for the root path, and we could not find
                            // any common ancestor
                            log.warn(getClass().getSimpleName() + " could not find any common ancestor for path " + target);
                            return null;
                        }
                        // try to go up a level by trying out the parent path
                        unknownParentPaths.push(parentPath);
                        unknownParentPaths.push(parentPath.getParent());
                    }
                }
                nodeForTarget = new RepositoryTreeNode(target);
                treeModel.addNode(knownParent, nodeForTarget);
                publish(new TreeChange.Insertion(knownParent, new RepositoryTreeNode[] { nodeForTarget }));
            }
            try {
                if (filter == null)
                    walkFileTree();
                else {
                    walkFileTree(filter);
                }
            } catch (Exception e) {
                workerSpan.recordException(e);
                throw e;
            }
        } finally {
            workerSpan.addEvent("background work finished");
        }
        return null;
    }

    @Override
    protected void done() {
        workerSpan.end();
        try {
            get();
        } catch (InterruptedException e) {
            log.atWarn().withMarker(LogService.MARKER_DEVELOPMENT).withThrowable(e).log("The worker for '{}' was interrupted", target);
        } catch (ExecutionException e) {
            log.atWarn().withThrowable(e.getCause()).log("Unable to scan repository tree for '{}' due to exception", target);
            SwingTools.showSimpleErrorMessage("cannot_scan_repository", e.getCause(), target, e.getCause().getMessage());
        }
        super.done();
    }

    @Override
    protected void process(List<TreeChange> chunks) {
        // runs on UI thread
        Span uiSpan = tracer.spanBuilder("edt-processing").setAttribute("chunks", chunks.size()).setParent(Context.current().with(workerSpan)).startSpan();
        Collection<TreeChange> accumulatedTreeChanges = chunks.stream().collect(TreeChange.combining());
        uiSpan.setAttribute("accumulatedChanges", accumulatedTreeChanges.size());
        for (TreeChange change : accumulatedTreeChanges) {
            change.applyOn(treeModel);
        }
        uiSpan.end();
    }

    private void walkFileTree(Predicate<RepositoryPath> filter) throws IOException {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void walkFileTree() throws IOException {

        Files.walkFileTree(target, EnumSet.noneOf(FileVisitOption.class), maxDepth, new SimpleFileVisitor<>() {

            private final Deque<DirectoryVisitContext> contextStack = new ArrayDeque<>(maxDepth + 1);
            private Scope currentScope;

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                RepositoryPath repositoryPath = (RepositoryPath) dir;
                if (!contextStack.isEmpty()) {
                    contextStack.peek().subTreeEntries.add(repositoryPath);
                    firePropertyChange("path", contextStack.peek().directoryNode.getRepositoryPath(), repositoryPath);
                } else {
                    firePropertyChange("path", target, repositoryPath);
                }
                Span directorySpan = tracer.spanBuilder("visiting-directory").setAttribute("args.path", repositoryPath.toString()).startSpan();
                currentScope = directorySpan.makeCurrent();

                RepositoryTreeNode nodeForDirectory = treeModel.getNodeFor(repositoryPath);
                if (nodeForDirectory == null) {
                    RepositoryTreeNode parentNode = treeModel.getNodeFor(repositoryPath.getParent());
                    nodeForDirectory = new RepositoryTreeNode(repositoryPath);
                    treeModel.addNode(parentNode, nodeForDirectory);
                    publish(new TreeChange.Insertion(parentNode, new RepositoryTreeNode[] { nodeForDirectory }));
                }
                DirectoryVisitContext context = new DirectoryVisitContext(nodeForDirectory, directorySpan);
                contextStack.push(context);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                RepositoryPath repositoryPath = (RepositoryPath) file;
                DirectoryVisitContext context = contextStack.peek();
                assert context != null;
                RepositoryTreeNode node = treeModel.getNodeFor(repositoryPath);
                if (node == null) {
                    RepositoryTreeNode newNode = new RepositoryTreeNode(repositoryPath);
                    treeModel.addNode(context.directoryNode, newNode);
                    context.createdChildren.add(newNode);
                }
                context.subTreeEntries.add(repositoryPath);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                DirectoryVisitContext context = contextStack.pop();
                RepositoryTreeNode directoryNode = context.directoryNode;
                if (!context.createdChildren.isEmpty()) {
                    publish(new TreeChange.Insertion(directoryNode, context.createdChildren.toArray(RepositoryTreeNode[]::new)));
                }
                List<Integer> removedIndices = new ArrayList<>(context.subTreeEntries.size());
                List<RepositoryTreeNode> removedNodes = new ArrayList<>(context.subTreeEntries.size());
                for (int i = 0; i < directoryNode.getChildCount(); i++) {
                    RepositoryTreeNode child = (RepositoryTreeNode) directoryNode.getChildAt(i);
                    if (!context.subTreeEntries.contains(child.getRepositoryPath())) {
                        // entry has been removed from filesystem, and it should
                        // be removed from the index as well
                        removedIndices.add(i);
                        removedNodes.add(child);
                    }
                }
                if (!removedIndices.isEmpty()) {
                    for (RepositoryTreeNode node : removedNodes) {
                        treeModel.removeNode(node);
                    }
                    publish(new TreeChange.Removal(directoryNode, removedNodes.toArray(RepositoryTreeNode[]::new),
                            removedIndices.stream().mapToInt(Integer::intValue).toArray()));
                }
                try {
                    if (exc != null) {
                        context.span.recordException(exc);
                        throw exc;
                    }
                } finally {
                    currentScope.close();
                    context.span.end();
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private static class DirectoryVisitContext {

        private final Set<RepositoryPath> subTreeEntries = new HashSet<>();
        private final List<RepositoryTreeNode> createdChildren = new ArrayList<>(50);

        private final RepositoryTreeNode directoryNode;
        private final Span span;

        private DirectoryVisitContext(RepositoryTreeNode directoryNode, Span directorySpan) {
            this.directoryNode = directoryNode;
            this.span = directorySpan;
        }
    }
}
