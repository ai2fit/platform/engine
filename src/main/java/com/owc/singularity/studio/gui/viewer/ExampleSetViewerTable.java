/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.viewer;


import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.JTableHeader;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.Attributes;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.transformer.filter.ExampleSetRowFilter;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.look.RapidLookTools;
import com.owc.singularity.studio.gui.tools.AttributeGuiTools;
import com.owc.singularity.studio.gui.tools.ExtendedJPopupMenu;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.actions.RemoveSelectedColumnAction;


/**
 * Can be used to display (parts of) the data by means of a JTable. Used to display
 * {@link ExampleSet} results in the {@link ExampleSetViewer}.
 *
 * @author Ingo Mierswa
 */
public class ExampleSetViewerTable extends ExtendedJTable {

    private static final int MAXIMAL_CONTENT_LENGTH = 200;

    protected static final int MAX_ROW_HEIGHT = 30;
    protected static final int MIN_ROW_HEIGHT = 25;

    private static final long serialVersionUID = 5535239693801265693L;

    protected static final Color MISSING_VALUE_COLOR = new Color(255, 155, 155);

    private Map<String, Color> mappingAttributeNamesToColor;

    private FilterableExampleSetTableModel tableModel;


    private Map<Integer, String> toolTipMessagesMap = new HashMap<>();

    private final Action REMOVE_SELECTED_COLUMNS = new RemoveSelectedColumnAction(this, IconSize.SMALL);


    public ExampleSetViewerTable() {
        this.mappingAttributeNamesToColor = new HashMap<>();
        setAutoResizeMode(AUTO_RESIZE_OFF);
        setFixFirstColumnForRearranging(true);
        installToolTip();

        // handles the highlighting of the currently hovered row
        setRowHighlighting(true);
    }

    public void setExampleSet(ExampleSet exampleSet) {
        this.tableModel = new FilterableExampleSetTableModel(exampleSet);
        setModel(tableModel);
        styleTable(tableModel.specialAttributes, tableModel.regularAttributes, tableModel.getRowCount());
    }

    public void updateRowSelection(int startRow, int endRow, boolean invert, ExampleSetRowFilter rowFilter, boolean invertRowFilter) {
        tableModel.updateRowSelection(startRow, endRow, invert, rowFilter, invertRowFilter);
        tableModel.fireTableDataChanged();
    }

    public FilterableExampleSetTableModel.RowInformation getRowSelectionParameters() {
        return tableModel.getRowSelectionParameters();
    }

    public List<Attribute> getSelectedAttributes() {
        return tableModel.getSelectedAttributes();
    }

    public void setSelectedAttributes(List<Attribute> selectedAttributes) {
        tableModel.updateAttributeSelection(selectedAttributes);
        setModel(tableModel);
        styleTable(tableModel.specialAttributes, tableModel.regularAttributes, tableModel.getRowCount());
    }

    public void styleTable(Attribute[] specialAttributes, Attribute[] regularAttributes, int size) {
        for (Attribute attribute : specialAttributes) {
            String attributeRoleLabel = attribute.getRole();
            Color specialColor = AttributeGuiTools.getColorForAttributeRole(attributeRoleLabel);
            mappingAttributeNamesToColor.put(attribute.getName(), specialColor);
        }

        for (Attribute attribute : regularAttributes) {
            mappingAttributeNamesToColor.put(attribute.getName(), Colors.WHITE);
        }
        setCellColorProvider((row, column) -> {
            int modelColumn = convertColumnIndexToModel(column);
            int modelRow = getModelIndex(convertRowIndexToModel(row));
            Color color;

            Attribute attribute = tableModel.getColumnAttribute(modelColumn);
            if (tableModel != null && attribute != null) {
                if (tableModel.getValueAt(modelRow, modelColumn) == null) {
                    color = MISSING_VALUE_COLOR;
                } else {
                    Color potentialColor = mappingAttributeNamesToColor.get(attribute.getName());
                    if (potentialColor == null)
                        color = Color.WHITE;
                    else
                        color = potentialColor;
                }
            } else {
                color = Color.WHITE;
            }
            if (row % 2 == 1)
                color = new Color(color.getRed() - 10, color.getGreen() - 10, color.getBlue() - 10);
            return color;
        });

        setGridColor(Colors.TABLE_CELL_BORDER);

        setCutOnLineBreak(true);
        setMaximalTextLength(MAXIMAL_CONTENT_LENGTH);

        setRowHeight(calcRowHeight(MAX_ROW_HEIGHT, MIN_ROW_HEIGHT, size));
    }

    private static int calcRowHeight(int maxRowHeight, int minRowHeight, int count) {
        if (count == 0) {
            return maxRowHeight;
        }
        final int f = Integer.MAX_VALUE / count;
        final int m = Math.max(minRowHeight, f);
        return Math.min(maxRowHeight, m);
    }

    /**
     * This method ensures that the correct tool tip for the current column is delivered.
     */
    @Override
    protected JTableHeader createDefaultTableHeader() {
        JTableHeader header = new JTableHeader(columnModel) {

            private static final long serialVersionUID = 1L;

            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realColumnIndex = convertColumnIndexToModel(index);
                return ExampleSetViewerTable.this.getHeaderToolTipText(realColumnIndex);
            }
        };
        header.putClientProperty(RapidLookTools.PROPERTY_TABLE_HEADER_BACKGROUND, Colors.WHITE);
        return header;
    }

    private String getHeaderToolTipText(int realColumnIndex) {
        if (realColumnIndex == 0) {
            // tooltip text for the column containing the example index
            return I18N.getGUIMessage("gui.label.data_view.example_index.tooltip");
        }
        if (toolTipMessagesMap.containsKey(realColumnIndex)) {
            // if the tooltip string was already constructed, use the cached string
            return toolTipMessagesMap.get(realColumnIndex);
        }
        Attributes attributes = tableModel.getExampleSet().getAttributes();

        Attribute attribute = attributes.get(getModel().getColumnName(realColumnIndex));
        if (attribute != null) {
            String attRole = attribute.getRole();

            String valueTypeString = attribute.getValueType().toString();
            valueTypeString = valueTypeString.replaceAll("_", " ");
            valueTypeString = String.valueOf(valueTypeString.charAt(0)).toUpperCase() + valueTypeString.substring(1);

            String toolTipText = "<html>";
            toolTipText += "<b>" + getModel().getColumnName(realColumnIndex) + "</b>";
            if (attRole != null) {
                toolTipText += "<font color=\"#666666\"> (" + attRole + ")</font>";
            }
            toolTipText += "<br><i>" + valueTypeString + "</i><br><br style=\"font-size:3px;\">";
            toolTipText += "</html>";

            toolTipMessagesMap.put(realColumnIndex, toolTipText);
            return toolTipText;
        } else {
            return "";
        }
    }

    @Override
    public void populatePopupMenu(ExtendedJPopupMenu<Integer> menu) {
        super.populatePopupMenu(menu);
        menu.add(REMOVE_SELECTED_COLUMNS);
    }

    @Override
    public void populateColumnPopupMenu(final ExtendedJPopupMenu<Integer> menu) {
        super.populateColumnPopupMenu(menu);
        menu.add(REMOVE_SELECTED_COLUMNS);
    }
}
