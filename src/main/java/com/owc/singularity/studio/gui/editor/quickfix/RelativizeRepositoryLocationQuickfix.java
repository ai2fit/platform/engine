/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.editor.quickfix;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * Replaces an absolute reference to a repository entry by an entry resolved relative to the current
 * process.
 *
 * @author Simon Fischer
 *
 */
public class RelativizeRepositoryLocationQuickfix extends AbstractQuickFix {

    private String key;
    private Operator operator;

    public RelativizeRepositoryLocationQuickfix(Operator operator, String key, String value) {
        super(10, false, "relativize_repository_location", key, value);
        this.key = key;
        this.operator = operator;
    }

    @Override
    public void apply() {
        RepositoryPath absLoc;
        try {
            absLoc = operator.getParameterAsRepositoryPath(key);
            final RepositoryPath processLoc = operator.getPipeline().getPath();
            if (processLoc == null) {
                SwingTools.showVerySimpleErrorMessage("quickfix_failed", "Pipeline is not stored in repository.");
            } else {

                String relative = processLoc.getParent().relativize(absLoc).toString();
                operator.setParameter(key, relative);
            }
        } catch (UserError e) {
            // Should not happen. Parameter should be set, otherwise we would not have created this
            // prefix.
            SwingTools.showVerySimpleErrorMessage("quickfix_failed", e.toString());
        }
    }

}
