package com.owc.singularity.studio.gui.repository.model;

import java.io.Closeable;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.gui.repository.RepositorySortingMethod;
import com.owc.singularity.studio.gui.repository.RepositoryTreeState;
import com.owc.singularity.studio.gui.repository.RepositoryTreeStateOwner;
import com.owc.singularity.studio.gui.repository.event.RepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.event.RepositoryTreeUpdateType;
import com.owc.singularity.studio.gui.repository.worker.CreateViewFromDataModelWorker;
import com.owc.singularity.studio.gui.repository.worker.FilterEntriesTreeModelListener;
import com.owc.singularity.studio.gui.tools.SwingTools;

/**
 * A {@link FilteredRepositoryTreeModel} represent a view over the {@link GlobalRepositoryTreeModel}
 * which has its own entries filter and listeners.
 *
 * @author Hatem Hamad
 */
public class FilteredRepositoryTreeModel extends AbstractRepositoryTreeModel implements AutoCloseable, Closeable {

    private final Object treeLock = new Object();

    private final List<RepositoryTreeStateOwner> stateOwners = new LinkedList<>();
    private final Map<String, RepositoryTreeNode> fastAccessContentTreeKeys = new HashMap<>();
    private final CreateViewFromDataModelWorker initializationWorker;
    private final AbstractRepositoryTreeModel dataTreeModel;
    private final Predicate<RepositoryTreeNode> entryFilter;
    private final RepositoryTreeViewEventManager eventManger;
    private final FilterEntriesTreeModelListener modelListener;
    private RepositorySortingMethod sortingMethod;
    private final Queue<Runnable> runnableQueueToRunAfterLoad = new LinkedList<>();

    public FilteredRepositoryTreeModel(Predicate<RepositoryTreeNode> entryFilter) {
        this(GlobalRepositoryTreeModel.getInstance(), entryFilter, RepositorySortingMethod.NAME_ASC);
    }

    public FilteredRepositoryTreeModel(AbstractRepositoryTreeModel dataTreeModel, Predicate<RepositoryTreeNode> entryFilter,
            RepositorySortingMethod sortingMethod) {
        super(null, true);
        this.dataTreeModel = dataTreeModel;
        this.entryFilter = entryFilter == null ? ignored -> true : entryFilter;
        this.sortingMethod = sortingMethod;
        this.eventManger = new RepositoryTreeViewEventManager();
        this.modelListener = new FilterEntriesTreeModelListener(this, this.entryFilter);
        final RepositoryTreeNode modelRoot = dataTreeModel.getRoot();
        this.initializationWorker = new CreateViewFromDataModelWorker(this, modelRoot, (clonedRoot) -> {
            dataTreeModel.addTreeModelListener(modelListener);
            for (Runnable runnable : runnableQueueToRunAfterLoad) {
                runnable.run();
            }
        });
        initializationWorker.execute();
        addTreeModelListener(eventManger);
        eventManger.execute();
    }

    public void addTreeStateOwner(RepositoryTreeStateOwner owner) {
        stateOwners.add(owner);
    }

    public List<RepositoryTreeStateOwner> getStateOwners() {
        return stateOwners;
    }

    /**
     * Registers a new {@link RepositoryTreeEventTrigger trigger} with a callback.
     *
     * @param trigger
     *            a trigger that decides when the callback gets called
     * @param callback
     *            the callback that will be called when the event trigger gets fired. The callback
     *            will get an array of paths that made the trigger fire.
     */
    public void on(RepositoryTreeEventTrigger trigger, Consumer<RepositoryPath[]> callback) {
        eventManger.on(trigger, callback);
    }

    public RepositorySortingMethod getSortingMethod() {
        return sortingMethod;
    }

    /**
     * Sets the {@link RepositorySortingMethod} with which this {@link GlobalRepositoryTreeModel} is
     * sorted
     *
     * @param method
     *            The {@link RepositorySortingMethod}
     * @since 7.4
     */
    public void setSortingMethod(RepositorySortingMethod method) {
        sortingMethod = method;
        synchronized (treeLock) {
            // Save expansion state and notify listeners to prevent GUI misbehavior
            List<RepositoryTreeStateOwner> stateOwners = getStateOwners();
            RepositoryTreeState[] oldStates = new RepositoryTreeState[stateOwners.size()];
            for (int i = 0; i < oldStates.length; i++) {
                oldStates[i] = stateOwners.get(i).getTreeState();
            }
            getRoot().sortChildren(sortingMethod);

            SwingUtilities.invokeLater(() -> {
                synchronized (treeLock) {
                    reload(getRoot());
                    for (int i = 0; i < oldStates.length; i++) {
                        stateOwners.get(i).setTreeState(oldStates[i]);
                    }
                }
            });
        }
    }

    public void invokeAfterLoad(Runnable callback) {
        if (isLoading()) {
            runnableQueueToRunAfterLoad.add(callback);
        } else {
            SwingTools.invokeLater(callback);
        }
    }

    private boolean isLoading() {
        return getRoot() == null || getRoot().isLoading();
    }

    @Override
    public boolean containsNodeFor(RepositoryPath path) {
        synchronized (treeLock) {
            return fastAccessContentTreeKeys.containsKey(path.toString(false));
        }
    }

    @Override
    public boolean containsNodeFor(RepositoryTreeNode node) {
        synchronized (treeLock) {
            return fastAccessContentTreeKeys.containsKey(node.getRepositoryPathStringWithoutVersion());
        }
    }

    public boolean accept(RepositoryTreeNode node) {
        return entryFilter.test(node);
    }

    @Override
    public RepositoryTreeNode getNodeFor(RepositoryPath path) {
        synchronized (treeLock) {
            return fastAccessContentTreeKeys.get(path.toString(false));
        }
    }

    @Override
    public RepositoryTreeNode getNodeFor(RepositoryTreeNode node) {
        synchronized (treeLock) {
            return fastAccessContentTreeKeys.get(node.getRepositoryPathStringWithoutVersion());
        }
    }

    @Override
    public void insertNodeInto(MutableTreeNode newChild, MutableTreeNode parent, int index) {
        synchronized (treeLock) {
            index = addNode((RepositoryTreeNode) parent, (RepositoryTreeNode) newChild);
            super.insertNodeInto(newChild, parent, index);
        }
    }

    @Override
    public int addNode(RepositoryTreeNode parent, RepositoryTreeNode newChild) {
        synchronized (treeLock) {
            int index;
            Enumeration<? extends TreeNode> children = parent.children();
            index = 0;
            while (children.hasMoreElements()) {
                if (sortingMethod.compare(newChild, (RepositoryTreeNode) children.nextElement()) <= 0)
                    break;
                index++;
            }
            fastAccessContentTreeKeys.put(newChild.getRepositoryPathStringWithoutVersion(), newChild);
            parent.insert(newChild, index);
            return index;
        }
    }

    @Override
    public void removeNode(RepositoryTreeNode node) {
        synchronized (treeLock) {
            node.removeFromParent();
            removeCacheFor(node);
        }
    }

    private void removeCacheFor(RepositoryTreeNode node) {
        synchronized (treeLock) {
            fastAccessContentTreeKeys.remove(node.getRepositoryPathStringWithoutVersion());
        }
        Enumeration<TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            removeCacheFor((RepositoryTreeNode) children.nextElement());
        }
    }

    @Override
    public void setRoot(TreeNode root) {
        RepositoryTreeNode treeNode = (RepositoryTreeNode) root;
        synchronized (treeLock) {
            fastAccessContentTreeKeys.put(treeNode.getRepositoryPathStringWithoutVersion(), treeNode);
        }
        super.setRoot(root);
    }

    @Override
    public void close() {
        dataTreeModel.addTreeModelListener(modelListener);
        initializationWorker.cancel(true);
        eventManger.cancel(true);
    }

    private static class RepositoryTreeViewEventManager extends SwingWorker<Void, Runnable> implements TreeModelListener {

        private final Map<RepositoryTreeEventTrigger, List<Consumer<RepositoryPath[]>>> eventMap = new WeakHashMap<>();
        private final BlockingQueue<EventContext> workQueue = new LinkedBlockingQueue<>();

        @Override
        protected Void doInBackground() {
            try {
                while (!isCancelled()) {
                    EventContext workItem = workQueue.take();
                    notifyConsumer(workItem.updateType, workItem.affectedPaths);
                }
            } catch (InterruptedException expected) {
                // when canceled while waiting on take()
            } catch (Exception e) {
                // TODO log.log(Level.ERROR, "An error occurred while watching for " +
                // filteredTreeModel + " changes in background", e);
            }
            return null;
        }

        @Override
        protected void process(List<Runnable> chunks) {
            for (Runnable runnable : chunks) {
                try {
                    runnable.run();
                } catch (Exception e) {
                    // TODO log e
                }
            }
        }

        private void on(RepositoryTreeEventTrigger trigger, Consumer<RepositoryPath[]> eventConsumer) {
            List<Consumer<RepositoryPath[]>> consumerList = eventMap.computeIfAbsent(trigger, ignored -> Collections.synchronizedList(new LinkedList<>()));
            consumerList.add(eventConsumer);
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            if (affectsSubEntries(e)) {
                // changes files
                RepositoryPath[] affectedPaths = getAffectedPaths(e);
                queueEvent(RepositoryTreeUpdateType.Update, affectedPaths);
            } else {
                RepositoryPath[] affectedPaths = new RepositoryPath[] { ((RepositoryTreeNode) e.getTreePath().getLastPathComponent()).getRepositoryPath() };
                queueEvent(RepositoryTreeUpdateType.Update, affectedPaths);
            }
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            if (affectsSubEntries(e)) {
                // changes files
                RepositoryPath[] affectedPaths = getAffectedPaths(e);
                queueEvent(RepositoryTreeUpdateType.Insert, affectedPaths);
            } else {
                RepositoryPath[] affectedPaths = new RepositoryPath[] { ((RepositoryTreeNode) e.getTreePath().getLastPathComponent()).getRepositoryPath() };
                queueEvent(RepositoryTreeUpdateType.Insert, affectedPaths);
            }
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
            if (affectsSubEntries(e)) {
                // changes files
                RepositoryPath[] affectedPaths = getAffectedPaths(e);
                queueEvent(RepositoryTreeUpdateType.Remove, affectedPaths);
            } else {
                RepositoryPath[] affectedPaths = new RepositoryPath[] { ((RepositoryTreeNode) e.getTreePath().getLastPathComponent()).getRepositoryPath() };
                queueEvent(RepositoryTreeUpdateType.Remove, affectedPaths);
            }
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {}

        private void queueEvent(RepositoryTreeUpdateType updateType, RepositoryPath[] affectedPaths) {
            workQueue.add(new EventContext(updateType, affectedPaths));
        }

        private void notifyConsumer(RepositoryTreeUpdateType updateType, RepositoryPath[] affectedPaths) {
            Iterator<Map.Entry<RepositoryTreeEventTrigger, List<Consumer<RepositoryPath[]>>>> entryIterator = eventMap.entrySet().iterator();
            while (entryIterator.hasNext()) {
                Map.Entry<RepositoryTreeEventTrigger, List<Consumer<RepositoryPath[]>>> entry = entryIterator.next();
                RepositoryTreeEventTrigger repositoryTreeEventTrigger = entry.getKey();
                if (repositoryTreeEventTrigger.shouldBeRemoved()) {
                    entryIterator.remove();
                    continue;
                }
                if (repositoryTreeEventTrigger.shouldFire(updateType, affectedPaths)) {
                    List<Consumer<RepositoryPath[]>> consumers = entry.getValue();
                    publish(() -> {
                        for (Consumer<RepositoryPath[]> consumer : consumers) {
                            consumer.accept(affectedPaths);
                        }
                    });
                }
            }
        }

        private static RepositoryPath[] getAffectedPaths(TreeModelEvent e) {
            return Arrays.stream(e.getChildren())
                    .filter(o -> o instanceof RepositoryTreeNode)
                    .map(o -> ((RepositoryTreeNode) o).getRepositoryPath())
                    .toArray(RepositoryPath[]::new);
        }

        private static boolean affectsSubEntries(TreeModelEvent event) {
            int[] children = event.getChildIndices();
            return children != null && children.length > 0;
        }

        private static class EventContext {

            private final RepositoryTreeUpdateType updateType;
            private final RepositoryPath[] affectedPaths;

            public EventContext(RepositoryTreeUpdateType updateType, RepositoryPath... affectedPaths) {
                this.updateType = updateType;
                this.affectedPaths = affectedPaths;
            }
        }
    }

}
