package com.owc.singularity.studio.gui.vcs.conflict;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.EventListenerList;

import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.ToggleAction;
import com.owc.singularity.studio.gui.editor.event.ProcessStorageListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;
import com.owc.singularity.studio.gui.vcs.conflict.model.CustomObjectMergeConflictDecision;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;
import com.owc.singularity.studio.gui.vcs.conflict.model.SimpleMergeConflictDecision;

public class ProcessConflictResolvingComponent implements ConflictResolvingComponent {

    private final MergeConflict conflict;
    private final AbstractPipeline ourProcess;
    private final AbstractPipeline theirProcess;

    private enum UserChoice {
        Ours, Theirs, Custom
    }

    private final EventListenerList eventListenerList = new EventListenerList();
    private MergeConflictDecision decision;
    private ProcessPanel comparingProcessPanel, customProcessPanel;

    public ProcessConflictResolvingComponent(MergeConflict conflict, AbstractPipeline ourProcess, AbstractPipeline theirProcess) {
        this.conflict = conflict;
        this.ourProcess = ourProcess;
        this.theirProcess = theirProcess;
        RepositoryPath conflictRepositoryPath = conflict.getConflictRepositoryPath();
        this.ourProcess.setPath(conflictRepositoryPath.getParent().resolve(conflictRepositoryPath.getFileName().toString() + " - (Ours)"));
        this.theirProcess.setPath(conflictRepositoryPath.getParent().resolve(conflictRepositoryPath.getFileName().toString() + " - (Theirs)"));
    }

    private void setUserChoice(UserChoice userChoice) {
        setDecision(switch (userChoice) {
            case Ours -> new SimpleMergeConflictDecision(conflict, true);
            case Theirs -> new SimpleMergeConflictDecision(conflict, false);
            case Custom -> new CustomObjectMergeConflictDecision(conflict, customProcessPanel.getProcess());
        });
    }

    private void fireResolved() {
        ConflictResolvedEventListener[] listeners = eventListenerList.getListeners(ConflictResolvedEventListener.class);
        for (ConflictResolvedEventListener listener : listeners) {
            listener.onResolve(this);
        }
    }

    @Override
    public void show() {
        if (customProcessPanel != null) {
            // already opened
            if (MainFrame.INSTANCE.setFocusedProcessPanel(customProcessPanel)) {
                return;
            }
            // panel was closed, continue as new show
        }
        if (comparingProcessPanel != null) {
            // already opened
            if (MainFrame.INSTANCE.setFocusedProcessPanel(comparingProcessPanel)) {
                return;
            }
            // panel was closed, continue as new show
        }
        RepositoryPath conflictRepositoryPath = conflict.getConflictRepositoryPath();

        comparingProcessPanel = MainFrame.INSTANCE.openProcess(ourProcess, true);
        JLabel processHasBeenDeletedLabel = comparingProcessPanel.getProcessEditor().getProcessRenderer().getEmptyProcessHint();
        processHasBeenDeletedLabel.setText("Pipeline has been deleted");
        processHasBeenDeletedLabel.setForeground(Color.RED.darker());
        JToggleButton switchVersionsButton = new JToggleButton(new ToggleAction(false, "vcs.merge.resolver.process.switch_version") {

            @Override
            public void actionToggled(ActionEvent e) {
                if (isSelected()) {
                    // theirs
                    comparingProcessPanel.setProcess(theirProcess);
                } else {
                    // ours
                    comparingProcessPanel.setProcess(ourProcess);
                }
            }
        });
        switchVersionsButton.setEnabled(true);
        switchVersionsButton.setSelected(true);
        JPanel controlPanel = new JPanel(new BorderLayout());

        JButton oursButton = new JButton(ResourceAction.of("vcs.merge.use_our_version", e -> setUserChoice(UserChoice.Ours)));
        JButton theirsButton = new JButton(ResourceAction.of("vcs.merge.use_their_version", e -> setUserChoice(UserChoice.Theirs)));
        JButton customButton = new JButton(ResourceAction.of("vcs.merge.use_custom_version", e -> {
            MainFrame.INSTANCE.closeProcessPanel(comparingProcessPanel, true);
            comparingProcessPanel = null;
            AbstractPipeline cloneProcessFromOurChanges = ourProcess.clone();
            cloneProcessFromOurChanges.setPath(conflictRepositoryPath);
            customProcessPanel = MainFrame.INSTANCE.openProcess(cloneProcessFromOurChanges, false);

            // disable custom button
            ((JComponent) e.getSource()).setEnabled(false);
            customProcessPanel.addProcessStorageListener(new ProcessStorageListener() {

                @Override
                public void onStore(AbstractPipeline process, RepositoryPath storedAt) {
                    setUserChoice(UserChoice.Custom);
                }

                @Override
                public void onLoad(AbstractPipeline process, RepositoryPath loadedFrom) {

                }
            });
            SwingTools.showMessageDialog("vcs.merge.resolver.process.use_custom_version");
        }));
        JPanel buttonPanel = ButtonDialog.makeButtonPanel(oursButton, theirsButton, customButton);
        controlPanel.add(switchVersionsButton, BorderLayout.WEST);
        controlPanel.add(buttonPanel, BorderLayout.EAST);
        comparingProcessPanel.add(controlPanel, BorderLayout.SOUTH);
    }

    @Override
    public void dispose() {
        if (comparingProcessPanel != null) {
            MainFrame.INSTANCE.closeProcessPanel(comparingProcessPanel, true);
            comparingProcessPanel = null;
        }
        if (customProcessPanel != null) {
            MainFrame.INSTANCE.closeProcessPanel(customProcessPanel, true);
            customProcessPanel = null;
        }
    }

    @Override
    public void addResolveListener(ConflictResolvedEventListener listener) {
        eventListenerList.add(ConflictResolvedEventListener.class, listener);
    }

    @Override
    public void removeResolveListener(ConflictResolvedEventListener listener) {
        eventListenerList.remove(ConflictResolvedEventListener.class, listener);
    }

    @Override
    public boolean isResolved() {
        return decision != null;
    }

    @Override
    public MergeConflict getConflict() {
        return conflict;
    }

    @Override
    public void setDecision(MergeConflictDecision decision) {
        this.decision = decision;
        fireResolved();
    }

    @Override
    public MergeConflictDecision getDecision() {
        return decision;
    }
}
