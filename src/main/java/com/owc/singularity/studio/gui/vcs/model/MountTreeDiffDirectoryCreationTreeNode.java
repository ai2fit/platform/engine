package com.owc.singularity.studio.gui.vcs.model;

import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.EntryVersion;

public class MountTreeDiffDirectoryCreationTreeNode extends AbstractMountTreeDiffDirectoryTreeNode {

    protected MountTreeDiffDirectoryCreationTreeNode(EntryVersion sourceVersion, RepositoryPath repositoryPath) {
        super(sourceVersion, repositoryPath);
    }
}
