/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.animation;


import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.studio.gui.GeneralProcessListener;
import com.owc.singularity.studio.gui.MainFrame;


/**
 * {@link GeneralProcessListener} that ensures that {@link Animation}s for {@link Operator}s are
 * added to the {@link ProcessAnimationManager} when the operator starts and removed if the operator
 * or the process finished. Also ensures that a {@link AnimationTimerPipelineLifeCycleEventListener}
 * is associated to a running process.
 *
 * @author Gisa Schaefer
 * @since 7.1.0
 */
public class OperatorAnimationProcessListener extends GeneralProcessListener {

    private AnimationTimerPipelineLifeCycleEventListener timerListener;

    @Override
    public void pipelineStarts(AbstractPipeline process) {
        if (timerListener == null) {
            timerListener = new AnimationTimerPipelineLifeCycleEventListener();
        }
        process.registerLifeCycleEventListener(timerListener);
        // need to start timerListener here since it is added after the event
        timerListener.startTimer();
    }

    @Override
    public void pipelineOperatorStarted(AbstractPipeline process, Operator op) {
        ProcessAnimationManager.INSTANCE.addAnimationForOperator(op);
    }

    @Override
    public void pipelineOperatorFinished(AbstractPipeline process, Operator op) {
        if (!op.isAnimating()) {
            ProcessAnimationManager.INSTANCE.removeAnimationForOperator(op);
        }
    }

    @Override
    public void pipelineFinished(AbstractPipeline process) {
        if (timerListener == null) {
            // indicates that processStarts has not been called
            // this could be due to an exception during setup
            return;
        }
        // need to remove operators here in case the process was stopped
        for (Operator operator : process.getAllOperators()) {
            ProcessAnimationManager.INSTANCE.removeAnimationForOperator(operator);
        }

        // need to stop the timer here since only processEnded tells if every operator is done
        timerListener.stopTimer();
        process.unregisterLifeCycleEventListener(timerListener);

        // needed to remove the OperatorAnimation when the process ended via checkForStop
        MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().getModel().fireMiscChanged();
    }

}
