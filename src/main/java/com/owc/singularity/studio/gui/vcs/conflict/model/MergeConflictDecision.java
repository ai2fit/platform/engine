package com.owc.singularity.studio.gui.vcs.conflict.model;

import java.io.IOException;
import java.io.InputStream;

import com.owc.singularity.repository.RepositoryPath;


public interface MergeConflictDecision {

    RepositoryPath getResultRepositoryPath();

    InputStream getInputStream() throws IOException;

    String getDescription();
}
