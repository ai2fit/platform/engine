/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.bubble;


import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.owc.singularity.engine.event.PipelineLifeCycleEventListener;
import com.owc.singularity.engine.event.ProcessLifeCycleEventDetails;
import com.owc.singularity.engine.operator.ExecutionUnit;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.engine.tools.Observable;
import com.owc.singularity.engine.tools.Observer;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.ProcessTabViewDockable;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.model.WorkflowAnnotation;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessDrawUtils;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessRenderer;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessEditorModelEventListener;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererAnnotationEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererModelEvent;
import com.owc.singularity.studio.gui.editor.pipeline.event.ProcessRendererOperatorEvent;
import com.owc.singularity.studio.gui.perspective.PerspectiveController;


/**
 * This class creates a speech bubble-shaped JDialog, which can be attached to a {@link Port}. See
 * {@link BubbleWindow} for more details.
 * <p>
 * If the perspective is incorrect, the dockable not shown or the subprocess currently viewed is
 * wrong, automatically corrects everything to ensure the bubble is shown if the
 * {@code ensureVisibility} parameter is set.
 * </p>
 *
 * @author Marco Boeck
 * @since 6.5.0
 *
 */

public class PortInfoBubble extends BubbleWindow {

    /**
     * Builder for {@link PortInfoBubble}s. After calling all relevant setters, call
     * {@link #build()} to create the actual dialog instance.
     *
     * @author Marco Boeck
     * @since 6.5.0
     *
     */
    public static class PortBubbleBuilder extends BubbleWindowBuilder<PortInfoBubble, PortBubbleBuilder> {

        private Port attachTo;
        private boolean hideOnConnection;
        private boolean hideOnDisable;
        private boolean hideOnRun;
        private boolean ensureVisible;
        private boolean killOnPerspectiveChange;

        public PortBubbleBuilder(final Window owner, final Port attachTo, final String i18nKey, final Object... arguments) {
            super(owner, i18nKey, arguments);
            this.attachTo = attachTo;
            this.killOnPerspectiveChange = true;
        }

        /**
         * Sets whether to hide the bubble when the port is connected. Defaults to {@code false}.
         *
         * @param hideOnConnection
         *            {@code true} if the bubble should be hidden upon connection; {@code false}
         *            otherwise
         * @return the builder instance
         */
        public PortBubbleBuilder setHideOnConnection(final boolean hideOnConnection) {
            this.hideOnConnection = hideOnConnection;
            return this;
        }

        /**
         * Sets whether to hide the bubble when the operator the port is attached to is disabled.
         * Defaults to {@code false}.
         *
         * @param hideOnDisable
         *            {@code true} if the bubble should be hidden upon disable; {@code false}
         *            otherwise
         * @return the builder instance
         */
        public PortBubbleBuilder setHideOnDisable(final boolean hideOnDisable) {
            this.hideOnDisable = hideOnDisable;
            return this;
        }

        /**
         * Sets whether to hide the bubble when the process is run. Defaults to {@code false}.
         *
         * @param hideOnRun
         *            {@code true} if the bubble should be hidden upon running a process;
         *            {@code false} otherwise
         * @return the builder instance
         */
        public PortBubbleBuilder setHideOnProcessRun(final boolean hideOnRun) {
            this.hideOnRun = hideOnRun;
            return this;
        }

        /**
         * Sets whether to make sure the bubble is visible by automatically switching perspective,
         * opening/showing the process dockable and changing the subprocess. Defaults to
         * {@code false}.
         *
         * @param ensureVisible
         *            {@code true} if the bubble should be hidden upon disable; {@code false}
         *            otherwise
         * @return the builder instance
         */
        public PortBubbleBuilder setEnsureVisible(final boolean ensureVisible) {
            this.ensureVisible = ensureVisible;
            return this;
        }

        /**
         * Sets whether the bubble should be automatically killed by switching perspective. Defaults
         * to {@code true}.
         *
         * @param killOnPerspectiveChange
         *            {@code true} if the bubble should be killed on perspective change;
         *            {@code false} otherwise
         * @return the builder instance
         */
        public PortBubbleBuilder setKillOnPerspectiveChange(final boolean killOnPerspectiveChange) {
            this.killOnPerspectiveChange = killOnPerspectiveChange;
            return this;
        }

        @Override
        public PortInfoBubble build() {
            return new PortInfoBubble(owner, style, alignment, i18nKey, attachTo, componentsToAdd, hideOnConnection, hideOnDisable, hideOnRun, ensureVisible,
                    moveable, showCloseButton, killOnPerspectiveChange, arguments);
        }

        @Override
        public PortBubbleBuilder getThis() {
            return this;
        }
    }

    private static final long serialVersionUID = 1L;

    private Port port;
    private OperatorChain portChain;
    private boolean hideOnConnection;
    private boolean hideOnDisable;
    private boolean hideOnRun;
    private final boolean killOnPerspectiveChange;
    private ProcessEditorPanel renderer = MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor();
    private JViewport viewport = MainFrame.INSTANCE.getMainProcessPanel().getViewPort();
    private ProcessEditorModelEventListener rendererModelListener;
    private ProcessEditorEventListener processEditorEventListener;
    private final PipelineLifeCycleEventListener pipelineLifeCycleEventListener = new PipelineLifeCycleEventListener() {

        @Override
        public String getIdentifier() {
            return "port-info-bubble";
        }

        @Override
        public void onBeforePipelineStart(AbstractPipeline pipeline, ProcessLifeCycleEventDetails details) {
            killBubble(true);
        }

    };
    private ChangeListener viewPortListener;
    private Observer<Port> portObserver;

    /**
     * Creates a BubbleWindow which points to a {@link Port}.
     *
     * @param owner
     *            the {@link Window} on which this {@link BubbleWindow} should be shown.
     * @param preferredAlignment
     *            offer for alignment but the Class will calculate by itself whether the position is
     *            usable.
     * @param i18nKey
     *            of the message which should be shown
     * @param toAttach
     *            the port the bubble should be attached to
     * @param style
     *            the bubble style
     * @param componentsToAdd
     *            array of JComponents which will be added to the Bubble or {@code null}
     * @param hideOnConnection
     *            if {@code true}, the bubble will be removed once the port is connected
     * @param hideOnDisable
     *            if {@code true}, the bubble will be removed once the operator of the port becomes
     *            disabled
     * @param hideOnRun
     *            if {@code true}, the bubble will be removed once the process is executed
     * @param ensureVisible
     *            if {@code true}, will automatically make sure the bubble will be visible by
     *            manipulating the GUI
     * @param moveable
     *            if {@code true} the user can drag the bubble around on screen
     * @param showCloseButton
     *            if {@code true} the user can close the bubble via an "x" button in the top right
     *            corner
     * @param killOnPerspectiveChange
     *            if {@code true} the bubble will be automatically killed if the perspective changes
     * @param arguments
     *            arguments to pass thought to the I18N Object
     */
    private PortInfoBubble(Window owner, BubbleStyle style, AlignedSide preferredAlignment, String i18nKey, Port toAttach, JComponent[] componentsToAdd,
            boolean hideOnConnection, boolean hideOnDisable, boolean hideOnRun, boolean ensureVisible, boolean moveable, boolean showCloseButton,
            boolean killOnPerspectiveChange, Object... arguments) {
        super(owner, style, preferredAlignment, i18nKey, ProcessTabViewDockable.PROCESS_PANEL_DOCK_KEY, null, null, moveable, showCloseButton, componentsToAdd,
                arguments);
        if (toAttach == null) {
            throw new IllegalArgumentException("toAttach must not be null!");
        }
        this.hideOnConnection = hideOnConnection;
        this.hideOnDisable = hideOnDisable;
        this.hideOnRun = hideOnRun;
        this.killOnPerspectiveChange = killOnPerspectiveChange;
        // the following is a best effort approach to find the port and portChain inside the main
        // process. Concurrent executions would otherwise crash the GUI
        this.portChain = (OperatorChain) MainFrame.INSTANCE.getMainProcessPanel()
                .getProcessEditor()
                .getProcess()
                .getOperator(toAttach.getPorts().getOwner().getPortHandler().getName());
        // in the case that the port belongs to an execution unit
        if (toAttach.getPorts().getOwner().getOperator().getName().equals(this.portChain.getName())) {
            Optional<ExecutionUnit> subprocess = this.portChain.getSubprocesses()
                    .stream()
                    .filter(process -> Arrays.equals(toAttach.getPorts().getPortNames(), process.getInnerSinks().getPortNames()))
                    .findFirst();
            subprocess.ifPresent(executionUnit -> this.port = executionUnit.getInnerSinks().getPortByName(toAttach.getName()));
            if (this.port == null) {
                subprocess = this.portChain.getSubprocesses()
                        .stream()
                        .filter(process -> Arrays.equals(toAttach.getPorts().getPortNames(), process.getInnerSources().getPortNames()))
                        .findFirst();
                subprocess.ifPresent(executionUnit -> this.port = executionUnit.getInnerSources().getPortByName(toAttach.getName()));
            }
        } else {
            Operator operator = MainFrame.INSTANCE.getMainProcessPanel()
                    .getProcessEditor()
                    .getProcess()
                    .getOperator(toAttach.getPorts().getOwner().getOperator().getName());
            if (Arrays.equals(operator.getInputPorts().getPortNames(), toAttach.getPorts().getPortNames())) {
                this.port = operator.getInputPorts().getPortByName(toAttach.getName());
            }
        }


        // if we need to ensure that the bubble is visible:
        if (ensureVisible) {
            // switch to correct subprocess (if the bubble is part of a subprocess!)
            // if it belongs to a different process, don't switch displayed process
            if (portChain != null && !renderer.getModel().getDisplayedChain().equals(portChain)) {
                renderer.getModel().setDisplayedChainAndFire(portChain);
            }
            // switch to correct perspective
            if (!MainFrame.INSTANCE.getPerspectiveController().getSelectedPerspective().getName().equals(PerspectiveController.DESIGN)) {
                MainFrame.INSTANCE.getPerspectiveController().showPerspective(PerspectiveController.DESIGN);
                this.myPerspective = PerspectiveController.DESIGN;
            }
            if (portChain != null) {
                MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().selectOperators(Collections.singletonList(portChain));
            }
        }

        // keyboard accessibility
        ActionListener closeOnEscape = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                PortInfoBubble.this.killBubble(true);
            }
        };
        getRootPane().registerKeyboardAction(closeOnEscape, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        // try to scroll to operator. If not possible, scroll to port
        ProcessPanel processPanel = MainFrame.INSTANCE.getMainProcessPanel();
        if (!processPanel.scrollToOperator(port.getPorts().getOwner().getOperator())) {
            processPanel.scrollToViewPosition(getObjectLocation());
        }
        super.paint(false);
    }

    @Override
    protected void registerSpecificListener() {
        viewPortListener = new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                PortInfoBubble.this.paint(false);
            }
        };
        rendererModelListener = new ProcessEditorModelEventListener() {

            @Override
            public void modelChanged(ProcessRendererModelEvent e) {
                switch (e.getEventType()) {
                    case DISPLAYED_CHAIN_CHANGED:
                    case DISPLAYED_EXECUTION_UNITS_CHANGED:
                        if (!renderer.getModel().getDisplayedChain().equals(portChain)) {
                            killBubble(true);
                        }
                        break;
                    // TODO case PROCESS_ZOOM_CHANGED:
                    case PROCESS_SIZE_CHANGED:
                        if (!viewport.isShowing()) {
                            dispose();
                        }
                        PortInfoBubble.this.paint(false);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void operatorsChanged(ProcessRendererOperatorEvent e, Collection<Operator> operators) {
                switch (e.getEventType()) {
                    case OPERATORS_MOVED:
                    case PORTS_CHANGED:
                        for (Operator op : operators) {
                            if (op.equals(port.getPorts().getOwner().getOperator())) {
                                PortInfoBubble.this.paint(false);
                                break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void annotationsChanged(ProcessRendererAnnotationEvent e, Collection<WorkflowAnnotation> annotations) {
                // don't care
            }

        };
        processEditorEventListener = new ProcessEditorEventListener() {

            @Override
            public void onProcessEdit(AbstractPipeline process) {
                if (hideOnDisable) {
                    // check if operator was disabled and kill bubble if that is desired
                    if (!port.getPorts().getOwner().getOperator().isEnabled()) {
                        killBubble(true);
                    }
                    // check if operator was removed from process and kill bubble
                    if (port.getPorts().getOwner().getOperator().getExecutionUnit() == null) {
                        killBubble(true);
                    }
                }
                // don't care
            }

            @Override
            public void onProcessLoad(AbstractPipeline process) {
                killBubble(true);
            }
        };
        portObserver = new Observer<Port>() {

            @Override
            public void update(Observable<Port> observable, Port arg) {
                if (port.isConnected()) {
                    killBubble(true);
                }
            }
        };

        if (hideOnConnection) {
            port.addObserver(portObserver, false);
        }
        if (hideOnRun) {
            MainFrame.INSTANCE.getMainProcessPanel().getProcess().registerLifeCycleEventListener(pipelineLifeCycleEventListener);
        }
        viewport.addChangeListener(viewPortListener);
        renderer.getModel().registerEditorModelEventListener(rendererModelListener);
        MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().addProcessEditorEventListener(processEditorEventListener);
    }

    @Override
    protected void unregisterSpecificListeners() {
        if (hideOnConnection) {
            port.removeObserver(portObserver);
        }
        if (hideOnRun) {
            MainFrame.INSTANCE.getMainProcessPanel().getProcess().unregisterLifeCycleEventListener(pipelineLifeCycleEventListener);
        }
        renderer.getModel().removeEditorModelEventListener(rendererModelListener);
        viewport.removeChangeListener(viewPortListener);
        MainFrame.INSTANCE.getMainProcessPanel().getProcessEditor().removeProcessEditorEventListener(processEditorEventListener);
    }

    @Override
    protected Point getObjectLocation() {
        if (!viewport.isShowing()) {
            return new Point(0, 0);
        }
        // get all necessary parameters
        if (!getDockable().getComponent().isShowing()) {
            return new Point(0, 0);
        }

        Point portLoc = ProcessDrawUtils.createPortLocation(port, renderer.getModel());
        if (portLoc == null) {
            return new Point(0, 0);
        }
        portLoc.x = (int) (portLoc.x * renderer.getModel().getZoomFactor());
        portLoc.y = (int) (portLoc.y * renderer.getModel().getZoomFactor());
        portLoc = ProcessDrawUtils.convertToAbsoluteProcessPoint(portLoc,
                renderer.getModel().getProcessIndex(port.getPorts().getOwner().getConnectionContext()), renderer.getModel());
        if (portLoc == null) {
            return new Point(0, 0);
        }
        portLoc.translate(-getObjectWidth() / 2, -getObjectHeight() / 2);

        // calculate actual on screen loc of the port loc and return it
        Point rendererLoc = renderer.getVisibleRect().getLocation();
        Point absoluteLoc = new Point((int) (viewport.getLocationOnScreen().x + (portLoc.getX() - rendererLoc.getX())),
                (int) (viewport.getLocationOnScreen().y + (portLoc.getY() - rendererLoc.getY())));

        // return validated Point
        return this.validatePointForBubbleInViewport(absoluteLoc);

    }

    @Override
    protected int getObjectWidth() {
        // double x width because we want a bit of distance from a port
        return (int) (ProcessRenderer.PORT_SIZE * 2 * renderer.getModel().getZoomFactor());
    }

    @Override
    protected int getObjectHeight() {
        return (int) (ProcessRenderer.PORT_SIZE * renderer.getModel().getZoomFactor());
    }

    @Override
    protected void changeToAssistant(final AssistantType type) {
        if (AssistantType.WRONG_PERSPECTIVE == type && !killOnPerspectiveChange) {
            setVisible(false);
        } else {
            killBubble(true);
        }
    }

    /**
     * validates the position of a Bubble and manipulates the position so that the Bubble won't
     * point to a Point outside of the Viewport if the Operator is not in the Viewport (the
     * Alignment of the Bubble is considered).
     *
     * @param position
     *            Point to validate
     * @return returns a Point inside the {@link JViewport}
     */
    private Point validatePointForBubbleInViewport(Point position) {
        // calculate Offset which is necessary to consider the Alignment
        int xOffset = 0;
        int yOffset = 0;
        int x = position.x;
        int y = position.y;
        if (getRealAlignment() != null) {
            switch (getRealAlignment()) {
                case LEFTBOTTOM:
                case LEFTTOP:
                    xOffset = this.getObjectWidth();
                    //$FALL-THROUGH$
                case RIGHTBOTTOM:
                case RIGHTTOP:
                    yOffset = (int) (this.getObjectHeight() * 0.5);
                    break;
                case TOPLEFT:
                case TOPRIGHT:
                    yOffset = this.getObjectHeight();
                    //$FALL-THROUGH$
                case BOTTOMLEFT:
                case BOTTOMRIGHT:
                    xOffset = (int) (this.getObjectWidth() * 0.5);
                    break;
                // $CASES-OMITTED$
                default:
            }
        }
        // manipulate invalid coordinates
        if (!(position.x + xOffset >= viewport.getLocationOnScreen().x)) {
            // left
            x = viewport.getLocationOnScreen().x - xOffset;
        }
        if (!(position.x + xOffset <= viewport.getLocationOnScreen().x + viewport.getSize().width)) {
            // right
            x = viewport.getLocationOnScreen().x + viewport.getSize().width - xOffset;
        }
        if (!(position.y + yOffset >= viewport.getLocationOnScreen().y)) {
            // top
            y = viewport.getLocationOnScreen().y - yOffset;
        }
        if (!(position.y + yOffset <= viewport.getLocationOnScreen().y + viewport.getSize().height)) {
            // bottom
            y = viewport.getLocationOnScreen().y + viewport.getSize().height - yOffset;
        }

        return new Point(x, y);
    }
}
