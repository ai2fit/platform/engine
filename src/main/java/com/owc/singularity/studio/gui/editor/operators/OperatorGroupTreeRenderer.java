/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.operators;


import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.owc.singularity.engine.OperatorService;
import com.owc.singularity.engine.operator.OperatorDescription;
import com.owc.singularity.engine.tools.AbstractOperatorDescriptionTreeNode;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.Icons;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * The renderer for the group tree (displays a small group icon).
 *
 * @author Ingo Mierswa
 */
public class OperatorGroupTreeRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;

    /** Greyscale version of the blacklisted icon */
    private static final Icon BLACKLISTED_ICON = SwingTools.createIcon("16/" + I18N.getGUILabel("operator.blacklisted.icon"), IconSize.SMALL, true);

    /** border so the operators have a little bit more breathing space */
    private static final Border EMPTY_BORDER = BorderFactory.createEmptyBorder(4, 0, 4, 0);

    /**
     * Blacklisted operator name color
     * <p>
     * The {@link OperatorsDockable Operators Panel} has a grey background, instead of a white one
     * like the {@link ProcessPanel AbstractPipeline Panel}, therefore darker.
     */
    private static final Color BLACKLISTED_OPERATOR_NAME_COLOR = new Color(150, 150, 150).darker();

    public OperatorGroupTreeRenderer() {
        setLeafIcon(getDefaultClosedIcon());
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean isSelected, final boolean expanded, final boolean leaf,
            final int row, final boolean hasFocus) {

        if (value instanceof AbstractOperatorDescriptionTreeNode) {
            AbstractOperatorDescriptionTreeNode operatorDescriptionTreeNode = (AbstractOperatorDescriptionTreeNode) value;
            setToolTipText("This group contains all operators of the group '" + operatorDescriptionTreeNode.getName() + "'.");
            JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, operatorDescriptionTreeNode.toString(), isSelected, expanded, leaf, row, hasFocus);
            label.setBorder(EMPTY_BORDER);
            return label;
        } else {
            OperatorDescription op = (OperatorDescription) value;
            setToolTipText(null);
            String labelText = op.getName();

            JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, labelText, isSelected, expanded, leaf, row, hasFocus);
            label.setBorder(EMPTY_BORDER);

            label.setIcon(Icons.fromName(op.getIconName()).getSmallIcon());

            if (OperatorService.isOperatorBlacklisted(op.getFullyQuallifiedKey())) {
                label.setIcon(BLACKLISTED_ICON);
                label.setForeground(BLACKLISTED_OPERATOR_NAME_COLOR);
            }

            return label;
        }
    }

}
