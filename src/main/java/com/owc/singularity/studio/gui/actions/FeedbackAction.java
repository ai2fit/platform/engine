package com.owc.singularity.studio.gui.actions;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.swing.*;

import org.apache.commons.lang.text.StrSubstitutor;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeString;
import com.owc.singularity.engine.pipeline.parameter.ParameterTypeText;
import com.owc.singularity.engine.pipeline.parameter.Parameters;
import com.owc.singularity.engine.pipeline.parameter.TextType;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.parameters.GenericParameterPanel;
import com.owc.singularity.studio.gui.tools.ExtendedJScrollPane;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.dialogs.ButtonDialog;

public class FeedbackAction extends ResourceAction {

    private static String feedbackEmailAddress;
    private static String feedbackTemplate;
    private final Window owner;

    public FeedbackAction() {
        super("feedback");
        this.owner = ApplicationFrame.getApplicationFrame();
    }

    public FeedbackAction(Window owner) {
        super("feedback");
        this.owner = owner;
    }

    private String getFeedbackTemplate() {
        if (feedbackTemplate == null) {
            String loadedTemplate;
            try (InputStream templateInputStream = getClass().getResourceAsStream("/com/owc/singularity/resources/gui/text-templates/user-feedback.md")) {
                if (templateInputStream != null) {
                    loadedTemplate = new String(templateInputStream.readAllBytes(), StandardCharsets.UTF_8);
                } else {
                    // no feedback template is defined
                    loadedTemplate = null;
                }
            } catch (IOException ignored) {
                loadedTemplate = null;
            }
            feedbackTemplate = loadedTemplate;
        }
        return feedbackTemplate;
    }

    private static String getFeedbackEmailAddress() {
        if (feedbackEmailAddress == null) {
            // retrieve the feedback email from properties
            feedbackEmailAddress = I18N.getGUIMessageOrNull("gui.feedback.email.address");
        }
        return feedbackEmailAddress;
    }

    @Override
    protected void loggedActionPerformed(ActionEvent e) {
        String template = getFeedbackTemplate();
        String emailAddress = getFeedbackEmailAddress();
        if (template == null) {
            SwingTools.showMessageDialog("feedback.form.send_manually", emailAddress);
        }
        Parameters filledParameters = askForUserFeedbackDetails();
        if (filledParameters == null) {
            // user cancelled dialog
            return;
        }
        String subject = Objects.requireNonNullElse(filledParameters.getParameterOrNull("subject"), "");
        String summary = Objects.requireNonNullElse(filledParameters.getParameterOrNull("summary"), "");

        StrSubstitutor substitutor = new StrSubstitutor(Map.of("summary", summary, "modules",
                ModuleService.getAllModules().stream().map(module -> module.getKey() + ":" + module.getVersion()).collect(Collectors.joining("\n", "* ", "")),
                "version", SingularityEngine.getLongVersion(), "os",
                SystemInfoUtilities.getOperatingSystemName() + ":" + SystemInfoUtilities.getOperatingSystemVersion(), "timezone",
                ZoneId.systemDefault().toString()));

        String issueBody = substitutor.replace(template);
        Desktop desktop;
        if (Desktop.isDesktopSupported() && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
            URI mailto = URI.create("mailto:" + emailAddress + "?subject=" + encode(subject) + "&body=" + encode(issueBody));
            try {
                desktop.mail(mailto);
            } catch (IOException ex) {
                // if the user default mail client is not found or fails to be launched
                SwingTools.showMessageDialog("feedback.form.mail_unavailable", new JTextArea(issueBody), emailAddress);
            }
        } else {
            SwingTools.showMessageDialog("feedback.form.mail_unavailable", new JTextArea(issueBody), emailAddress);
        }
    }

    private Parameters askForUserFeedbackDetails() {
        return SwingTools.invokeAndWaitWithResult(() -> {
            Parameters parameters = new Parameters();
            parameters.addParameterType(new ParameterTypeString("subject", "A subject describing the issue", false));
            parameters.addParameterType(new ParameterTypeText("summary", "A summary which gives details about steps to reproduce", TextType.PLAIN, true));

            JPanel mainComponent = new JPanel(new BorderLayout());
            GenericParameterPanel panel = new GenericParameterPanel(parameters);
            mainComponent.add(new ExtendedJScrollPane(panel));
            Path runtimeLogFilePath = getRuntimeLogFilePath();
            JButton showLogFileOnFileManager = new JButton(createAction(runtimeLogFilePath));
            ButtonDialog dialog = ButtonDialog.create(owner, "feedback.form")
                    .setContent(mainComponent, ButtonDialog.MESSAGE_EXTENDED)
                    .setModalityType(Dialog.ModalityType.DOCUMENT_MODAL)
                    .withButton(showLogFileOnFileManager)
                    .withOkButton("feedback.send")
                    .withCancelButton()
                    .build();
            dialog.setVisible(true);
            if (dialog.wasConfirmed()) {
                return panel.getParameters();
            } else {
                return null;
            }
        });
    }


    /**
     * // TODO this should be replaced by LogConfigurator#getRuntimeLogFilePath once moved to studio
     * 
     * @return the log file path
     */
    private Path getRuntimeLogFilePath() {
        return Path.of(PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_STUDIO_LOG_DIRECTORY,
                FileSystemService.getUserSingularityDirectory().toPath().resolve("logs").toString())).resolve("runtime.log");
    }

    private ResourceAction createAction(Path filePath) {
        return ResourceAction.of("file.browse", false, actionEvent -> {
            Desktop desktop;
            if (Desktop.isDesktopSupported()) {
                desktop = Desktop.getDesktop();
                try {
                    if (desktop.isSupported(Desktop.Action.BROWSE_FILE_DIR)) {
                        desktop.browseFileDirectory(filePath.toFile());
                    } else {
                        desktop.open(filePath.getParent().toFile());
                    }
                } catch (IOException ignored) {

                }
            }
        }, "Log file", filePath.getFileName().toString());
    }

    private static String encode(String content) {
        return URLEncoder.encode(content, StandardCharsets.UTF_8).replace("+", "%20");
    }
}
