/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository;


import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.*;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.studio.StudioProperties;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.dnd.AbstractPatchedTransferHandler;
import com.owc.singularity.studio.gui.dnd.DragListener;
import com.owc.singularity.studio.gui.dnd.TransferableRepositoryEntries;
import com.owc.singularity.studio.gui.repository.actions.context.*;
import com.owc.singularity.studio.gui.repository.event.OnCreateRepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.event.RepositoryTreeEventTrigger;
import com.owc.singularity.studio.gui.repository.model.FilteredRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.GlobalRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow.TooltipLocation;


/**
 * A tree displaying repository contents.
 * <p>
 * To add context actions to the popup menu, add a factory using
 * {@link #addContextActionFactory(RepositoryTreeContextActionFactory)}}
 *
 * @author Simon Fischer, Tobias Malbrecht, Marco Boeck, Adrian Wilke, Hatem Hamad
 */
public class RepositoryTree extends JTree implements RepositoryTreeStateOwner, AutoCloseable, Closeable {

    private static final int TREE_ROW_HEIGHT = 24;
    private transient final ToolTipWindow toolTipWindow;

    private static final long serialVersionUID = -6613576606220873341L;

    private EnableActionsOnSelectionChange treeSelectionListener;
    private LocationSelectionMouseListener mouseListener;
    private LocationSelectionKeyListener keyListener;
    private LazyLoadingTreeWillExpandListener treeWillExpandListener;
    private final Set<RepositoryTreeContextActionFactory> contextActionFactories = new LinkedHashSet<>();

    public RepositoryTree() {
        this(null);
    }

    public RepositoryTree(Dialog owner) {
        this(owner, false);
    }

    public RepositoryTree(Dialog owner, boolean onlyFolders) {
        this(owner, onlyFolders, false, true);
    }

    public RepositoryTree(Dialog owner, boolean onlyFolders, boolean onlyWritableRepositories) {
        this(owner, onlyFolders, onlyWritableRepositories, true);
    }

    public RepositoryTree(Dialog owner, boolean onlyFolders, boolean onlyWritableRepositories, boolean installDraghandler) {
        this(owner, onlyFolders, onlyWritableRepositories, installDraghandler, null);
    }

    public RepositoryTree(Dialog owner, boolean onlyFolders, boolean onlyWritableRepositories, boolean installDraghandler, final Color backgroundColor) {
        this(owner, installDraghandler, backgroundColor, createPredicateFor(onlyFolders, onlyWritableRepositories));
    }

    /**
     * Create a new RepositoryTree, configure it directly in the constructor
     *
     * @param owner
     *            the dialog that is the owner of this tree, used when creating subdialogs
     * @param installDraghandler
     *            when true, the {@link RepositoryTreeTransferHandler} is installed and the user is
     *            able to drag/drop data.
     * @param backgroundColor
     *            if {@code null} the default background color will be used, otherwise the provided
     *            background color will be used
     * @param predicate
     *            if supplied only those repositories, folders and entries will be shown that match
     *            the predicate
     */
    public RepositoryTree(Dialog owner, boolean installDraghandler, final Color backgroundColor, Predicate<RepositoryTreeNode> predicate) {
        super(new FilteredRepositoryTreeModel(predicate));
        setLargeModel(true);
        setRowHeight(0);
        getModel().addTreeStateOwner(this);
        // these actions are a) needed for the action map or b) needed by other classes for toolbars
        // etc
        List<AbstractRepositoryContextAction<?>> toolbarActions = new ArrayList<>();
        AbstractRepositoryContextAction<?> deleteAction = new DeleteRepositoryEntryAction(this);
        toolbarActions.add(deleteAction);
        AbstractRepositoryContextAction<?> renameAction = new RenameRepositoryEntryAction(this);
        toolbarActions.add(renameAction);
        CommitEntryAction commitEntryAction = new CommitEntryAction(this);
        toolbarActions.add(commitEntryAction);
        toolbarActions.add(new OpenEntryRepositoryAction(this));
        toolbarActions.add(new CreateDirectoryAction(this));
        renameAction.addToActionMap(this, WHEN_FOCUSED);
        commitEntryAction.addToActionMap(this, WHEN_FOCUSED);
        deleteAction.addToActionMap(WHEN_FOCUSED, false, false, true, "delete", this);
        for (AbstractRepositoryContextAction<?> action : toolbarActions) {
            action.enable();
        }

        setRowHeight(Math.max(TREE_ROW_HEIGHT, getRowHeight()));
        setRootVisible(true);
        setShowsRootHandles(true);
        if (backgroundColor != null) {
            // in case of a custom background color we need to overwrite the getBackground() method
            // as the constructor of DefaultTreeCellRenderer uses the method to query the background
            // image. Thus we cannot provide the color within the RepositoryTreeCellRenderer
            // constructor.
            setCellRenderer(new RepositoryTreeCellRenderer() {

                private static final long serialVersionUID = 1L;

                @Override
                public Color getBackground() {
                    return backgroundColor;
                }

                @Override
                public Color getBackgroundNonSelectionColor() {
                    return backgroundColor;
                }
            });
        } else {
            setCellRenderer(new RepositoryTreeCellRenderer());
        }
        getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);

        mouseListener = new LocationSelectionMouseListener();
        addMouseListener(mouseListener);


        keyListener = new LocationSelectionKeyListener();
        addKeyListener(keyListener);

        treeWillExpandListener = new LazyLoadingTreeWillExpandListener(
                Integer.parseInt(PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_GUI_REPOSITORY_SCAN_MAX_DIRECTORY_LEVELS, "2")));
        addTreeWillExpandListener(treeWillExpandListener);
        if (installDraghandler) {
            setDragEnabled(true);
            setTransferHandler(new RepositoryTreeTransferHandler());
        }


        treeSelectionListener = new EnableActionsOnSelectionChange(toolbarActions);
        getSelectionModel().addTreeSelectionListener(treeSelectionListener);

        toolTipWindow = new ToolTipWindow(owner, new RepositoryTreeNodeToolTipProvider(this), this, TooltipLocation.RIGHT);

        if (backgroundColor != null) {
            setBackground(backgroundColor);
        }
    }

    @Override
    public FilteredRepositoryTreeModel getModel() {
        return (FilteredRepositoryTreeModel) super.getModel();
    }

    public RepositoryTreeState getTreeState() {
        RepositoryTreeState state = new RepositoryTreeState();
        state.save(this);
        return state;
    }

    public void setTreeState(RepositoryTreeState state) {
        state.restore(this);
    }

    public void addRepositorySelectionListener(RepositoryEntrySelectionListener listener) {
        listenerList.add(RepositoryEntrySelectionListener.class, listener);
    }

    public void removeRepositorySelectionListener(RepositoryEntrySelectionListener listener) {
        listenerList.remove(RepositoryEntrySelectionListener.class, listener);
    }

    /**
     * Adds a {@link RepositorySortingMethodListener}
     *
     * @since 7.4
     */
    public void addRepositorySortingMethodListener(RepositorySortingMethodListener l) {
        listenerList.add(RepositorySortingMethodListener.class, l);
    }

    /**
     * Removes a {@link RepositorySortingMethodListener}
     *
     * @since 7.4
     */
    public void removeRepositorySortingMethodListener(RepositorySortingMethodListener l) {
        listenerList.remove(RepositorySortingMethodListener.class, l);
    }

    private void fireLocationSelected(RepositoryTreeNode entry) {
        RepositorySelectionEvent event = new RepositorySelectionEvent(entry);
        for (RepositoryEntrySelectionListener l : listenerList.getListeners(RepositoryEntrySelectionListener.class)) {
            l.onSelectionChanged(event);
        }
    }

    /**
     * Similar as {@link #scrollPathToVisible(TreePath)}, but centers it instead. Used for explicit
     * highlighting.
     *
     * @param path
     *            the path to center
     * @since 8.1
     */
    private void scrollPathToVisibleCenter(TreePath path) {
        if (path == null) {
            return;
        }

        // set y and height in a way that the path always appears in the center
        Rectangle bounds = getPathBounds(path);
        if (bounds == null) {
            return;
        }
        Rectangle visibleRect = getVisibleRect();
        // try to stay as far left as possible. If very deep folder structure, let Swing decide
        // where to start
        if ((bounds.x + 50) < visibleRect.x + visibleRect.width) {
            bounds.x = 0;
        }
        bounds.y = bounds.y - visibleRect.height / 2;
        bounds.height = visibleRect.height;
        scrollRectToVisible(bounds);
    }

    /**
     * Expands as much as possible of the given path to the given location.
     */
    public void expandIfExists(RepositoryPath location) {
        if (location == null)
            return;
        getModel().invokeAfterLoad(() -> {
            RepositoryPath locationToSelect = location;
            RepositoryTreeNode node = getModel().getNodeFor(locationToSelect);
            while (node == null && locationToSelect.getParent() != null) {
                locationToSelect = locationToSelect.getParent();
                node = getModel().getNodeFor(locationToSelect);
            }
            if (node == null)
                return;
            TreePath pathTo = new TreePath(node.getPath());
            expandPath(pathTo);
            new PathLoadedTimer(pathTo, () -> scrollPathToVisibleCenter(pathTo)).start();
        });
    }

    /**
     * Expands the tree to select the given entry if it exists.
     */
    public void expandAndSelectIfExists(RepositoryPath location) {
        if (location == null)
            return;
        getModel().invokeAfterLoad(() -> {
            RepositoryTreeNode node = getModel().getNodeFor(location);
            if (node != null) {
                selectAndScrollTo(node);
                return;
            }
            // Node was not found
            // Try to query the path step-by-step since the path may have been too deep
            // and was not queried
            // We search for all ancestors that are not yet queried
            Deque<RepositoryPath> unknownParentPaths = new ArrayDeque<>(location.getNameCount());
            unknownParentPaths.push(location.getParent());
            RepositoryTreeNode knownParent;
            while (!unknownParentPaths.isEmpty()) {
                RepositoryPath parentPath = unknownParentPaths.pop();
                knownParent = getModel().getNodeFor(parentPath);
                if (knownParent == null) {
                    if (parentPath.getParent() != null) {
                        // try to go up a level by trying out the parent path
                        unknownParentPaths.push(parentPath);
                        unknownParentPaths.push(parentPath.getParent());
                    } else {
                        // we reached out for the root path, and we could not find
                        // any common ancestor
                        // TODO log.warning("Could not find any common ancestor for path " +
                        // target);
                        return;
                    }
                } else {
                    // we found a known parent in the path
                    break;
                }
            }
            // while selecting the node, the ancestor nodes will be expanded
            // however, their content may change and new nodes will be added along the
            // expansion path. Thus, we need to wait until the direct parent node
            // finishes loading
            // and then scroll to the selected path
            RepositoryPath nearestAncestorPath = unknownParentPaths.isEmpty() ? location.getParent() : unknownParentPaths.removeLast();
            SwingWorker<Void, ?> lastWorker = GlobalRepositoryTreeModel.getInstance().newTreeScanWorker(nearestAncestorPath, null, 1, () -> {
                if (getModel() == null) {
                    // tree was closed before this callback is executed
                    return;
                }
                RepositoryTreeNode newlyAddedNode = getModel().getNodeFor(location);
                if (newlyAddedNode == null) {
                    // the event from GlobalRepositoryTreeModel hasn't reached this model yet
                    // try to register an event trigger
                    invokeAfterTreeEvent(new OnCreateRepositoryTreeEventTrigger(location), ignored -> {
                        RepositoryTreeNode createdNode = getModel().getNodeFor(location);
                        if (createdNode == null) {
                            // too late, the node has already been removed
                            return;
                        }
                        selectAndScrollTo(createdNode);
                    });
                } else {
                    // the model already caught the event and knows about the node
                    selectAndScrollTo(newlyAddedNode);
                }
            });
            watchWorkerPathAndUpdateStatusBar(nearestAncestorPath, lastWorker);
            Runnable lastStep = lastWorker::execute;
            // make a call chain where each level wait until the level above
            // has finished its scan
            while (!unknownParentPaths.isEmpty()) {
                RepositoryPath unknownAncestorPath = unknownParentPaths.removeLast();
                Runnable step = lastStep;
                lastStep = () -> {
                    SwingWorker<Void, ?> worker = GlobalRepositoryTreeModel.getInstance().newTreeScanWorker(unknownAncestorPath, null, 1, step);
                    watchWorkerPathAndUpdateStatusBar(unknownAncestorPath, worker);
                    worker.execute();
                };
            }
            lastStep.run();
        });
    }

    private void selectAndScrollTo(RepositoryTreeNode node) {
        TreePath path = new TreePath(node.getPath());
        setSelectionPath(path);
        new PathLoadedTimer(path, () -> scrollPathToVisibleCenter(path)).start();
    }

    public void addContextActionFactory(RepositoryTreeContextActionFactory factory) {
        contextActionFactories.add(factory);
    }

    private static class PathLoadedTimer extends Timer {

        private final TreePath treePath;
        private final Runnable callback;
        private int maxRetries = 50;

        public PathLoadedTimer(TreePath treePath, Runnable callback) {
            this(400, treePath, callback);
        }

        public PathLoadedTimer(int delay, TreePath treePath, Runnable callback) {
            super(delay, null);
            this.treePath = treePath;
            this.callback = callback;
            setRepeats(true);
        }

        @Override
        protected void fireActionPerformed(ActionEvent e) {
            if (maxRetries-- == 0) {
                // TODO log that max retries has been reached
                stop();
                return;
            }
            int pathCount = treePath.getPathCount();
            for (int i = 0; i < pathCount; i++) {
                if (((RepositoryTreeNode) treePath.getPathComponent(i)).isLoading())
                    return;
            }
            // all path component has been loaded
            callback.run();
            stop();
        }
    }

    private JPopupMenu createRepositoryContextMenuPopup() {
        List<RepositoryTreeNode> entries = getSelectedEntries();
        JPopupMenu menu = new JPopupMenu();
        if (entries.isEmpty()) {
            return menu;
        }

        // Add actions
        List<Action> actionList = new LinkedList<>();
        for (RepositoryTreeContextActionFactory factory : contextActionFactories) {
            actionList.addAll(factory.createActionsFor(this, entries));
        }
        // Go through ordered list of actions and add them
        for (Action action : actionList) {
            if (action == null) {
                menu.addSeparator();
            } else {
                menu.add(action);
            }
        }
        return menu;
    }

    public RepositoryTreeNode getSelectedEntry() {
        TreePath path = getSelectionPath();
        if (path == null) {
            return null;
        }
        Object selected = path.getLastPathComponent();
        if (selected instanceof RepositoryTreeNode) {
            return (RepositoryTreeNode) selected;
        } else {
            return null;
        }
    }

    /**
     * Gets list of selected entries of this tree
     */
    public List<RepositoryTreeNode> getSelectedEntries() {
        List<RepositoryTreeNode> selectedEntries = new ArrayList<>();
        TreePath[] paths = getSelectionPaths();
        if (paths != null) {
            for (TreePath treePath : paths) {
                Object selected = treePath.getLastPathComponent();
                if (selected instanceof RepositoryTreeNode) {
                    selectedEntries.add((RepositoryTreeNode) selected);
                }
            }
        }
        return selectedEntries;
    }

    /**
     * Sets the {@link RepositorySortingMethod} with which the {@link GlobalRepositoryTreeModel} is
     * sorted
     *
     * @param method
     *            The {@link RepositorySortingMethod}
     * @since 7.4
     */
    public void setSortingMethod(RepositorySortingMethod method) {
        getModel().setSortingMethod(method);
        for (RepositorySortingMethodListener l : listenerList.getListeners(RepositorySortingMethodListener.class)) {
            l.changedRepositorySortingMethod(method);
        }
    }

    /**
     * Gets the {@link RepositorySortingMethod} with which this {@link GlobalRepositoryTreeModel} is
     * sorted
     *
     * @since 7.4
     */
    public RepositorySortingMethod getSortingMethod() {
        return getModel().getSortingMethod();
    }

    public void invokeAfterTreeEvent(RepositoryTreeEventTrigger trigger, Consumer<RepositoryPath[]> eventConsumer) {
        getModel().on(trigger, eventConsumer);
    }

    /**
     * @author Nils Woehler, Adrian Wilke
     *
     */
    private class RepositoryTreeTransferHandler extends AbstractPatchedTransferHandler {

        private static final long serialVersionUID = 1L;

        // Remember whether the last cut/copy action was a MOVE
        // A move will result in the entry being deleted upon drop / paste
        // Unfortunately there is no easy way to know this from the TransferSupport
        // passed to importData(). It is not even kno wn in createTransferable(), so we
        // cannot even attach it to the Transferable
        // This implementation implies that we can only transfer from one repository tree
        // to the same instance since this state is not passed to other instances.
        // REASON THIS IS HERE:
        // ctrl+x followed by ctrl+v would copy the entry instead of moving it due to
        // the way the transfer system is implemented. See RM-10 for further details.
        int latestAction = 0;

        public RepositoryTreeTransferHandler() {
            addDragListener(new DragListener() {

                @Override
                public void dragStarted(Transferable t) {
                    // reset latestAction because a new drag makes the last action irrelevant
                    latestAction = 0;
                }

                @Override
                public void dragEnded() {}
            });
        }


        @Override
        public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
            List<DataFlavor> flavors = Arrays.asList(transferFlavors);
            boolean contains = flavors.contains(DataFlavor.javaFileListFlavor);
            contains |= flavors.contains(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR);
            contains |= flavors.contains(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR);
            return contains;
        }

        @Override
        protected void exportDone(JComponent c, Transferable data, int action) {
            if (action == MOVE) {
                latestAction = MOVE;
            } else {
                latestAction = 0;
            }
        }

        @Override
        public int getSourceActions(JComponent c) {
            return COPY_OR_MOVE;
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            final List<RepositoryTreeNode> selectedEntries = getSelectedEntries();
            if (selectedEntries.isEmpty()) {
                // Nothing selected
                return null;
            }

            return new TransferableRepositoryEntries(getSelectedEntries().stream().map(RepositoryTreeNode::getRepositoryPath).toArray(RepositoryPath[]::new));
        }

        @Override
        public Icon getVisualRepresentation(Transferable t) {
            return null;
        }

        @Override
        public boolean importData(final TransferSupport ts) {
            // Determine where to insert
            final RepositoryTreeNode droppedOnEntry;
            if (ts.isDrop()) {
                Point dropPoint = ts.getDropLocation().getDropPoint();
                TreePath path = getPathForLocation((int) dropPoint.getX(), (int) dropPoint.getY());
                if (path == null) {
                    return false;
                }
                droppedOnEntry = (RepositoryTreeNode) path.getLastPathComponent();
            } else {
                droppedOnEntry = getSelectedEntry();
            }
            if (droppedOnEntry == null) {
                return false;
            }
            // Execute operation chosen by flavor
            try {
                List<DataFlavor> flavors = Arrays.asList(ts.getDataFlavors());
                if (flavors.contains(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR)) {
                    // Single repository entry
                    final RepositoryPath location = (RepositoryPath) ts.getTransferable()
                            .getTransferData(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_FLAVOR);
                    return copyOrMoveRepositoryEntries(droppedOnEntry, Collections.singletonList(location), ts);

                } else if (flavors.contains(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR)) {
                    // Multiple repository entries
                    Object transferData = ts.getTransferable().getTransferData(TransferableRepositoryEntries.LOCAL_TRANSFERRED_REPOSITORY_LOCATION_LIST_FLAVOR);
                    List<RepositoryPath> locations;
                    if (transferData instanceof TransferableRepositoryEntries) {
                        locations = ((TransferableRepositoryEntries) transferData).getLocations();
                    } else if (transferData instanceof RepositoryPath[]) {
                        locations = Arrays.asList((RepositoryPath[]) transferData);
                    } else {
                        // should not happen
                        return false;
                    }
                    return copyOrMoveRepositoryEntries(droppedOnEntry, locations, ts);
                } else {
                    // Flavor not supported
                    return false;
                }
            } catch (UnsupportedFlavorException e) {
                LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.accepting_flavor_error", e, e);
                return false;
            } catch (IOException | RuntimeException e) {
                LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.error_during_drop", e, e);
                return false;
            }
        }

        /**
         * Copies / moves multiple repository locations.
         * <p>
         * Additionally, at first it is checked, if the operations are allowed.
         */
        private boolean copyOrMoveRepositoryEntries(RepositoryTreeNode targetEntry, final List<RepositoryPath> transferredPaths, final TransferSupport ts) {
            if (isMoveOperation(ts) && !targetEntry.getInformation().isDirectory()) {
                // ignore move operations that don't target a folder
                return false;
            }

            RepositoryPath targetPath = targetEntry.getRepositoryPath();

            // First check, if operation is allowed for all locations
            for (RepositoryPath location : transferredPaths) {
                if (!copyOrMoveCheck(targetPath, location, ts)) {
                    return false;
                }
            }

            // Execute operations in new thread
            InteractiveCopyLocationTask copyLocationTask = new InteractiveCopyLocationTask(targetPath, transferredPaths, isMoveOperation(ts));

            if (transferredPaths.size() > 1) {
                // On multi-operations, select the target folder after finishing copy/move
                // operation
                copyLocationTask.addProgressThreadListener(thread -> SwingUtilities.invokeLater(() -> expandAndSelectIfExists(targetPath)));
            }
            copyLocationTask.start();

            // No failures in initial check
            return true;
        }

        /**
         * Checks, if desired copy or move operation is allowed. If not, false is returned and an
         * additional error message is shown.
         *
         * @throws IllegalArgumentException
         *             If an argument is null
         */
        private boolean copyOrMoveCheck(final RepositoryPath targetPath, final RepositoryPath sourcePath, final TransferSupport ts) {
            if (targetPath == null) {
                throw new IllegalArgumentException("Entry must not be null.");
            }
            if (sourcePath == null) {
                throw new IllegalArgumentException("RepositoryPath must not be null.");
            }
            if (ts == null) {
                throw new IllegalArgumentException("TransferSupport must not be null.");
            }
            if (!(Files.isDirectory(targetPath))) {
                // Copy / move only to folder
                return false;

            }
            // Check for unknown parameters
            String targetPathString = targetPath.toAbsolutePath().toString();
            String sourcePathString = sourcePath.toAbsolutePath().toString();

            String effectiveNewName = sourcePath.getFileName().toString();
            if (effectiveNewName.isEmpty()) {
                LogService.getRoot().warn("com.owc.singularity.repository.RepositoryTree.parameter_missing.name");
                return false;
            }

            if (isMoveOperation(ts)) {
                // Check for MOVE

                // Make sure same folder moves are forbidden
                if (sourcePathString.equals(targetPathString)) {
                    SwingTools.showVerySimpleErrorMessage("repository_move_same_folder");
                    return false;
                }

                // Make sure moving parent folder into subfolder is forbidden
                if (targetPath.startsWith(sourcePath)) {
                    SwingTools.showVerySimpleErrorMessage("repository_move_into_subfolder");
                    return false;
                }

                // Entry should be moved into its own parent folder, invalid
                if (sourcePath.getParent().equals(targetPath)) {
                    SwingTools.showVerySimpleErrorMessage("repository_move_same_folder");
                    return false;
                }

            } else {
                // Check for COPY

                // Make sure same folder moves are forbidden
                if (sourcePath.equals(targetPath)) {
                    SwingTools.showVerySimpleErrorMessage("repository_copy_same_folder");
                    return false;
                }

                // Make sure moving parent folder into subfolder is forbidden
                if (targetPath.startsWith(sourcePath)) {
                    SwingTools.showVerySimpleErrorMessage("repository_copy_into_subfolder");
                    return false;
                }
            }
            return true;
        }


        /**
         * Checks, if the current operation is a MOVE operation. If not, it is a COPY operation.
         *
         * @param ts
         *            Provides info about the current operation moved. This results in a copy
         *            operation.
         * @return false, if the operation is not a move operation (e.g. copy)
         */
        private boolean isMoveOperation(TransferSupport ts) {
            return (latestAction == MOVE || ts.isDrop() && ts.getDropAction() == MOVE);
        }
    }

    @Override
    public void close() {
        removeMouseListener(mouseListener);
        mouseListener = null;

        removeKeyListener(keyListener);
        keyListener = null;

        removeTreeWillExpandListener(treeWillExpandListener);
        treeWillExpandListener = null;

        setTransferHandler(null);

        getSelectionModel().removeTreeSelectionListener(treeSelectionListener);
        treeSelectionListener = null;

        getActionMap().clear();
        getInputMap().clear();

        getModel().close();
        setModel(null);

        toolTipWindow.close();

        removeAll();
        getParent().remove(this);
        listenerList = null;
    }

    private static class EnableActionsOnSelectionChange implements TreeSelectionListener {

        private final List<AbstractRepositoryContextAction<?>> actionList;

        public EnableActionsOnSelectionChange(List<AbstractRepositoryContextAction<?>> actionList) {
            this.actionList = actionList;
        }

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            for (AbstractRepositoryContextAction<?> abstractRepositoryContextAction : actionList) {
                abstractRepositoryContextAction.enable();
            }
        }

    }

    private static class LazyLoadingTreeWillExpandListener implements TreeWillExpandListener {

        private final int maxDirectoryLevelsForTreeScan;

        public LazyLoadingTreeWillExpandListener(int maxDirectoryLevelsForTreeScan) {
            this.maxDirectoryLevelsForTreeScan = maxDirectoryLevelsForTreeScan;
        }

        @Override
        public void treeWillExpand(TreeExpansionEvent event) {
            TreePath path = event.getPath();

            if (path.getLastPathComponent()instanceof RepositoryTreeNode node) {
                // we could find changes
                SwingWorker<Void, ?> worker = GlobalRepositoryTreeModel.getInstance()
                        .newTreeScanWorker(node.getRepositoryPath(), null, maxDirectoryLevelsForTreeScan, null);
                watchWorkerPathAndUpdateStatusBar(node.getRepositoryPath(), worker);
                worker.execute();
            }
        }

        @Override
        public void treeWillCollapse(TreeExpansionEvent event) {
            // do nothing
        }

    }

    private class LocationSelectionKeyListener implements KeyListener {

        // status variable to fix bug 987
        private int lastPressedKey;

        @Override
        public void keyTyped(KeyEvent e) {}

        /**
         * Opens entries on enter pressed; collapses/expands folders
         */
        @Override
        public void keyReleased(KeyEvent e) {
            if (lastPressedKey != e.getKeyCode()) {
                e.consume();
                return;
            }
            lastPressedKey = 0;

            if (e.getModifiersEx() == 0) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                    case KeyEvent.VK_SPACE:
                        TreePath path = getSelectionPath();
                        if (path == null) {
                            return;
                        }
                        RepositoryTreeNode entry = (RepositoryTreeNode) path.getLastPathComponent();
                        if (entry.getInformation().isDirectory()) {
                            if (isExpanded(path)) {
                                collapsePath(path);
                            } else {
                                expandPath(path);
                            }
                        } else {
                            fireLocationSelected((RepositoryTreeNode) path.getLastPathComponent());
                        }
                        e.consume();
                        break;
                }
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            lastPressedKey = e.getKeyCode();
        }
    }

    private class LocationSelectionMouseListener extends MouseAdapter {

        private transient JPopupMenu currentMenuPopup;

        @Override
        public void mouseClicked(MouseEvent e) {
            mouseClickPressReleasePopup(e);

            // Doubleclick
            if (getSelectionCount() == 1) {
                if (e.getClickCount() == 2) {
                    TreePath path = getSelectionPath();
                    if (path != null && path.getLastPathComponent() instanceof RepositoryTreeNode) {
                        fireLocationSelected((RepositoryTreeNode) path.getLastPathComponent());
                    }
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            mouseClickPressReleasePopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            mouseClickPressReleasePopup(e);
        }

        private void mouseClickPressReleasePopup(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3) {

                int mouseRow = getRowForLocation(e.getX(), e.getY());

                // Mouse is not over row -> Remove current selection
                if (mouseRow == -1) {
                    setSelectionInterval(mouseRow, mouseRow);
                }

                // No multiple row selection -> Update selected element
                if (getSelectionCount() <= 1) {
                    setSelectionInterval(mouseRow, mouseRow);
                }

                // Multiple rows selected -> Update if mouse over other element
                int[] selectionRows = getSelectionRows();
                if (selectionRows != null && selectionRows.length > 1) {
                    boolean inMultiRow = false;
                    for (int selectedRow : selectionRows) {
                        if (selectedRow == mouseRow) {
                            inMultiRow = true;
                            break;
                        }
                    }
                    if (!inMultiRow) {
                        setSelectionInterval(mouseRow, mouseRow);
                    }
                }

                // Finally show popup
                if (e.isPopupTrigger()) {
                    if (currentMenuPopup != null) {
                        // free the resources of the old menu
                        final JPopupMenu oldMenu = currentMenuPopup;
                        SwingTools.invokeLater(oldMenu::removeAll);
                    }
                    currentMenuPopup = createRepositoryContextMenuPopup();
                    currentMenuPopup.show(RepositoryTree.this, e.getX(), e.getY());
                }
            } else if (e.getButton() == MouseEvent.BUTTON1) {
                int mouseRow = getRowForLocation(e.getX(), e.getY());

                // Mouse is not over row -> Remove current selection
                if (mouseRow == -1) {
                    setSelectionInterval(mouseRow, mouseRow);
                }
            }
        }
    }

    private static Predicate<RepositoryTreeNode> createPredicateFor(boolean onlyFolders, boolean onlyWritableRepositories) {
        Predicate<RepositoryTreeNode> predicate = null;
        if (onlyWritableRepositories) {
            predicate = node -> node.getInformation().isWritable();
        }
        if (onlyFolders) {
            Predicate<RepositoryTreeNode> onlyDirectories = node -> node.getInformation().isDirectory();
            predicate = predicate == null ? onlyDirectories : onlyDirectories.and(predicate);
        }
        return predicate;
    }


    private static void watchWorkerPathAndUpdateStatusBar(RepositoryPath startPath, SwingWorker<Void, ?> worker) {
        worker.getPropertyChangeSupport()
                .addPropertyChangeListener("path",
                        evt -> ApplicationFrame.getApplicationFrame()
                                .getStatusBar()
                                .setIndeterminateProgress("Scanning " + ((RepositoryPath) evt.getNewValue()).toShortString(50) + " ...", 50, 100));
        worker.getPropertyChangeSupport().addPropertyChangeListener("state", evt -> {
            if (evt.getNewValue()instanceof SwingWorker.StateValue state && state == SwingWorker.StateValue.DONE) {
                ApplicationFrame.getApplicationFrame().getStatusBar().setIndeterminateProgress("Scanning " + startPath.toShortString(50) + " DONE", 100, 100);
            }
        });
    }
}
