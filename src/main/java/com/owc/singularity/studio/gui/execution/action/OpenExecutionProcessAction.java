package com.owc.singularity.studio.gui.execution.action;

import java.awt.event.ActionEvent;
import java.io.IOException;

import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.concurrency.tools.ConcurrentPipelineExecution;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.OpenEntryAction;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.tools.ResourceAction;

public class OpenExecutionProcessAction extends ResourceAction {

    private static final long serialVersionUID = 1L;
    private ConcurrentPipelineExecution execution;

    public OpenExecutionProcessAction(ConcurrentPipelineExecution execution) {
        super("toolkit.open_execution_process");
        this.execution = execution;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        OpenEntryAction.showProcess(Entries.getEntry(execution.getOriginalPipeline().getPath()));
    }

    public static void open() throws UserError {

        RepositoryPath location = RepositoryLocationChooser.selectLocation(null, null, MainFrame.INSTANCE, true, false, true);
        if (location != null) {
            try {
                Entry entry = Entries.getEntry(location);
                if (entry.isInstanceOf(AbstractPipeline.class, ModuleService.getMajorClassLoader())) {
                    ConcurrentExecutionServiceProvider.getService()
                            .executePipelineConcurrently(entry.loadData(AbstractPipeline.class, ModuleService.getMajorClassLoader()));
                } else {
                }
            } catch (IOException e) {
            } catch (ClassNotFoundException e) {
            }
        }
    }
}
