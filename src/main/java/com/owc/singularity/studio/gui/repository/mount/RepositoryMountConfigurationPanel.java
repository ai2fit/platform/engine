/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.repository.mount;


import java.io.IOException;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;

import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.RepositoryMount;
import com.owc.singularity.repository.RepositoryPath;


/**
 * Panel to configure a repository.
 * 
 * @author Simon Fischer
 * 
 */
public interface RepositoryMountConfigurationPanel {

    /**
     * Mounts a new RepositoryMount into the given path and adds it to the
     * {@link RepositoryManager}.
     * 
     * @throws IOException
     *             thrown if errors during mount
     */
    void mount(RepositoryPath mountPoint) throws IOException;

    /** Configures the UI elements to show the properties defined by the given repository. */
    void configureUIElementsFrom(RepositoryMount mount);

    /**
     * Configures given repository with the values entered into the dialog.
     * 
     * @return true if configuration is ok
     */
    boolean configure(RepositoryMount repository);

    /** Returns the actual component. */
    JComponent getComponent();

    /** This button should be disabled when invalid values are entered. */
    void setOkButton(JButton okButton);

    /**
     * Additional buttons that will be shown left of the cancel button. If no additional buttons
     * should be added an empty list should be returned.
     */
    List<AbstractButton> getAdditionalButtons();

}
