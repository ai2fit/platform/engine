package com.owc.singularity.studio.gui.repository.actions.context;

import java.util.List;

import javax.swing.*;

import com.owc.singularity.studio.gui.repository.RepositoryTree;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;

public interface RepositoryTreeContextActionFactory {

    List<Action> createActionsFor(RepositoryTree tree, List<RepositoryTreeNode> selectedNodes);
}
