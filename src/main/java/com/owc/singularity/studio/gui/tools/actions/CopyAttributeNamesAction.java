/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.tools.actions;


import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.Serial;

import javax.swing.table.TableModel;

import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.studio.gui.LoggedAbstractAction;
import com.owc.singularity.studio.gui.tools.ExtendedJTable;
import com.owc.singularity.studio.gui.tools.IconSize;
import com.owc.singularity.studio.gui.tools.SwingTools;


/**
 * This action copies the selected column names to the clipboard.
 *
 * @author ALexander Mahler
 */
public class CopyAttributeNamesAction extends LoggedAbstractAction {

    private static final String ICON_NAME = "copy.png";
    @Serial
    private static final long serialVersionUID = 6043346498896580319L;

    private final ExtendedJTable table;

    public CopyAttributeNamesAction(ExtendedJTable table, IconSize size) {
        super("Copy Attribute Names", SwingTools.createIcon(size.getSize() + "/" + ICON_NAME));
        this.table = table;
        putValue(SHORT_DESCRIPTION, "Copy Attribute Names");
        putValue(MNEMONIC_KEY, KeyEvent.VK_C);
    }

    @Override
    public void loggedActionPerformed(ActionEvent e) {
        int[] selectedColumnIndexes = this.table.getSelectedColumns();
        if (selectedColumnIndexes.length != 0) {
            StringBuilder builder = new StringBuilder();
            TableModel model = this.table.getModel();
            if (model != null) {
                for (int index : selectedColumnIndexes) {
                    builder.append(model.getColumnName(index));
                    builder.append(" ");
                }
            }
            Tools.copyStringToClipboard(builder.toString());
        }

    }

}
