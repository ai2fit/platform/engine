package com.owc.singularity.studio.gui.editor.pipeline;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ZoomState;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessUserInteractionListener;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessRenderer;

public class ProcessOverviewPanel extends JPanel implements ProcessEditorEventListener, ChangeListener, ProcessUserInteractionListener {

    private static final long serialVersionUID = 1L;

    private static final Color FILL_COLOR = new Color(140, 140, 200, 30);

    private static final Color DRAW_COLOR = new Color(140, 140, 200);

    private final ProcessEditorPanel processEditorPanel;

    private Point dragStartMousePos;
    private Rectangle dragStartRect;
    private double scale = 1d;

    public ProcessOverviewPanel(ProcessEditorPanel processEditorPanel) {
        this.processEditorPanel = processEditorPanel;
        addMouseListener(new VisibleRectExtractor());
        addMouseMotionListener(new ScrollOnMouseDrag());
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        ProcessEditorPanel editorPanel = processEditorPanel;
        int panelWidth = editorPanel.getWidth();
        int panelHeight = editorPanel.getHeight();
        double scaleX = (double) getWidth() / (double) panelWidth;
        double scaleY = (double) getHeight() / (double) panelHeight;
        scale = Math.min(scaleX, scaleY);
        double scaledW = panelWidth * scale;
        double scaledH = panelHeight * scale;

        Graphics2D g = (Graphics2D) graphics.create();
        g.translate((getWidth() - scaledW) / 2d, (getHeight() - scaledH) / 2d);
        g.scale(scale, scale);

        g.setRenderingHints(ProcessRenderer.LOW_QUALITY_HINTS);
        editorPanel.getOverviewPanelDrawer().draw(g, true);

        g.setStroke(new BasicStroke((int) (1d / scale)));

        Rectangle visibleRect = editorPanel.getVisibleRect();
        Rectangle drawRect = new Rectangle((int) visibleRect.getX(), (int) visibleRect.getY(), (int) visibleRect.getWidth() - 1,
                (int) visibleRect.getHeight() - 1);

        g.setColor(FILL_COLOR);
        g.fill(drawRect);

        g.setColor(DRAW_COLOR);
        g.draw(drawRect);

        g.dispose();
    }

    @Override
    public void onProcessEdit(AbstractPipeline process) {
        repaint();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        repaint();
    }

    @Override
    public void onZoomStateChange(ZoomState newState) {
        repaint();
    }

    @Override
    public void onOperatorMoved(Operator op) {
        repaint();
    }

    @Override
    public void onOperatorSelectionChange(List<Operator> selectedOperators) {
        repaint();
    }

    @Override
    public void onProcessViewChange(AbstractPipeline process, OperatorChain from, OperatorChain to) {
        repaint();
    }

    private class VisibleRectExtractor extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            dragStartMousePos = e.getPoint();
            dragStartRect = processEditorPanel.getVisibleRect();
        }
    }

    private class ScrollOnMouseDrag implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
            double diffX = (e.getX() - dragStartMousePos.getX()) / scale;
            double diffY = (e.getY() - dragStartMousePos.getY()) / scale;
            processEditorPanel.scrollRectToVisible(new Rectangle((int) (dragStartRect.getX() + diffX), (int) (dragStartRect.getY() + diffY),
                    (int) dragStartRect.getWidth(), (int) dragStartRect.getHeight()));
        }

        @Override
        public void mouseMoved(MouseEvent e) {}
    }
}
