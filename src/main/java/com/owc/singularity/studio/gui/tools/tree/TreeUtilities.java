package com.owc.singularity.studio.gui.tools.tree;

import java.util.Comparator;
import java.util.function.Function;

import javax.swing.tree.TreeNode;


public class TreeUtilities {

    public static int findInsertionIndex(TreeNode parent, TreeNode child, Comparator<TreeNode> comparator) {
        int low = 0;
        int high = parent.getChildCount() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            TreeNode midVal = parent.getChildAt(mid);
            int cmp = comparator.compare(midVal, child);

            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid;
        }
        return low;
    }

    public static <V> int findIndex(TreeNode parent, V childValue, Function<TreeNode, V> valueExtractor, Comparator<V> comparator) {
        int low = 0;
        int high = parent.getChildCount() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            TreeNode midVal = parent.getChildAt(mid);
            int cmp = comparator.compare(valueExtractor.apply(midVal), childValue);
            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid;
        }
        // key not found.
        return -(low + 1);
    }
}
