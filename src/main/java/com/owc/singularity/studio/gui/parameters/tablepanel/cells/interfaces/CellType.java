/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.parameters.tablepanel.cells.interfaces;


import com.owc.singularity.studio.gui.parameters.tablepanel.TablePanel;
import com.owc.singularity.studio.gui.parameters.tablepanel.model.TablePanelModel;


/**
 * This interface must be extended by all specific cell type interfaces for the
 * {@link TablePanelModel} (see {@link TablePanelModel#getColumnClass(int, int)} and
 * {@link TablePanel}.
 * 
 * @author Marco Boeck
 * 
 */
public interface CellType {

}
