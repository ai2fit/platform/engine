/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.tools.dialogs;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.tools.FontTools;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.PlatformUtilities;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.tools.ResourceAction;
import com.owc.singularity.studio.gui.tools.UrlHandlerService;
import com.owc.singularity.studio.gui.tools.components.LinkRemoteButton;
import com.owc.singularity.tools.VersionNumber;
import com.owc.singularity.tools.VersionNumber.VersionNumberException;


/**
 * This dialog displays some informations about the product. The product logo should have a size of
 * approximately 270 times 70 pixels.
 *
 * @author Ingo Mierswa
 */
public class AboutBox extends JDialog {

    private static final long serialVersionUID = -3889559376722324215L;

    private static final int MAX_SHOWN_LINK_LENGTH = 50;

    private static final String PROPERTY_FILE = "about_infos.properties";

    private static final String DEFAULT_VENDOR = "AI2FITEngine";
    private static final String DEFAULT_VENDOR_OLD = "Rapid-I";
    private static final String JAVA_VERSION = "java.version";
    private static final String COPYRIGHT = "copyright";
    private static final String LICENSOR = "licensor";
    private static final String LICENSE = "license";

    public static Image backgroundImage = null;

    static {
        URL url = Tools.getResource("splash/background.png");
        if (url != null) {
            try {
                backgroundImage = ImageIO.read(url);
            } catch (IOException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.tools.dialogs.AboutBox.loading_background_error");
            }
        }
    }

    private static class ContentPanel extends JPanel {

        private static final String[] DISPLAYED_KEYS = new String[] { JAVA_VERSION, COPYRIGHT, LICENSOR, LICENSE };

        private static final Font FONT_SANS_SERIF_11 = FontTools.getFont(Font.SANS_SERIF, Font.PLAIN, 11);
        private static final Font FONT_SANS_SERIF_BOLD_11 = FontTools.getFont(Font.SANS_SERIF, Font.BOLD, 11);
        private static final Font FONT_SANS_SERIF_BOLD_26 = FontTools.getFont(Font.SANS_SERIF, Font.BOLD, 26);
        private static final Font FONT_OPEN_SANS_15 = FontTools.getFont("Open Sans", Font.PLAIN, 15);

        private static final List<Font> FONTS_PRODUCT_NAME = new ArrayList<>(15);

        static {
            for (int size = 60; size >= 8; size -= 4) {
                FONTS_PRODUCT_NAME.add(FontTools.getFont("Open Sans Light", Font.PLAIN, size));
            }
        }

        private static final long serialVersionUID = -1763842074674706654L;

        private static final int LOGO_INSET_Y = 342;
        private static final int LOGO_INSET_X = 10;

        private static final int ADDITIONAL_LINE_HEIGHT = 15;

        private static final Paint MAIN_PAINT = new Color(96, 96, 96);

        private static final int MARGIN = 20;

        private final Properties properties;

        private transient Image productLogo;

        public ContentPanel(Properties properties, Image productLogo) {
            this.properties = properties;
            this.productLogo = productLogo;
            int width = 550;
            int height = 400;
            if (backgroundImage != null) {
                width = backgroundImage.getWidth(this);
                height = backgroundImage.getHeight(this);
            }

            // Add additional space, if we display more than two keys
            int foundKeys = 0;
            for (String key : DISPLAYED_KEYS) {
                if (properties.containsKey(key)) {
                    foundKeys++;
                }
            }
            if (foundKeys >= 2) {
                height += (foundKeys - 1) * ADDITIONAL_LINE_HEIGHT;
            }
            setPreferredSize(new Dimension(width, height));
            setMinimumSize(new Dimension(width, height));
            setMaximumSize(new Dimension(width, height));
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            drawMain(g2d);
            g2d.dispose();
        }

        public void drawMain(Graphics2D g) {
            g.setPaint(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());

            // draw the background image
            if (backgroundImage != null) {
                g.drawImage(backgroundImage, 0, 0, this);
            }

            g.setFont(FONT_SANS_SERIF_BOLD_26);
            if (productLogo != null) {
                g.drawImage(productLogo, 5, 5, (int) (productLogo.getWidth(null) * 70d / productLogo.getHeight(null)), 70, this);
            }

            String productName = removeLeadingName(properties.getProperty("name"));
            g.setFont(FONTS_PRODUCT_NAME.get(FONTS_PRODUCT_NAME.size() - 1));
            g.setPaint(Color.WHITE);
            for (Font f : FONTS_PRODUCT_NAME) {
                if (getFontMetrics(f).stringWidth(productName) <= getSize().width - 2 * MARGIN) {
                    g.setFont(f);
                    break;
                }
            }
            FontMetrics fm = getFontMetrics(g.getFont());
            int x_product = (getSize().width - fm.stringWidth(productName)) / 2;
            int y_product = ((backgroundImage != null ? backgroundImage.getHeight(null) : getSize().height) - 70 - fm.getHeight()) / 2 + fm.getAscent();
            g.drawString(productName, x_product, y_product);

            StringBuilder builder = new StringBuilder();
            builder.append(I18N.getGUILabel("version"));
            builder.append(" ");
            VersionNumber versionNumber = null;
            if (properties.getProperty("version") != null) {
                try {
                    versionNumber = new VersionNumber(properties.getProperty("version"));
                } catch (VersionNumberException e) {
                    // nothing to do
                }
            }
            builder.append(versionNumber != null ? versionNumber.getShortVersion() : I18N.getGUILabel("unknown_version"));

            // this would draw the version number (Version 9.6) below and right to the logo
            /*
             * String version = builder.toString(); int x_version = x_product +
             * fm.stringWidth(productName); Rectangle2D bounds = getStringBounds(g, productName,
             * x_product, y_product); int y_version = (int) (bounds.getY() + bounds.getHeight());
             * g.setFont(FONT_OPEN_SANS_15); fm = getFontMetrics(g.getFont());
             * 
             * x_version -= fm.stringWidth(version); y_version += fm.getHeight();
             * g.drawString(version, x_version, y_version);
             */
            g.setPaint(MAIN_PAINT);

            int y = 285;
            g.setFont(FONT_SANS_SERIF_BOLD_11);
            builder = new StringBuilder();
            builder.append(properties.getProperty("name"));
            builder.append(" ");
            builder.append(properties.getProperty("version"));
            String revision = properties.getProperty("revision");
            if (revision != null) {
                builder.append(" (rev: ");
                builder.append(revision, 0, 6);
                String platform = properties.getProperty("platform");
                if (platform != null) {
                    builder.append(", platform: ");
                    builder.append(platform);
                }
                builder.append(")");
            }

            drawString(g, builder.toString(), y);
            y += 15;

            g.setFont(FONT_SANS_SERIF_11);
            drawString(g, "Licensed under AGPL3", y);
            y += 30;

            for (String key : DISPLAYED_KEYS) {
                y = drawStringAndAdvance(g, properties.getProperty(key), y);
            }
        }

        private Rectangle getStringBounds(Graphics2D g2, String str, float x, float y) {
            FontRenderContext frc = g2.getFontRenderContext();
            GlyphVector gv = g2.getFont().createGlyphVector(frc, str);
            return gv.getPixelBounds(null, x, y);
        }

        private int drawStringAndAdvance(Graphics2D g, String string, int y) {
            if (string == null) {
                return y;
            } else {
                List<String> lines = new LinkedList<>();
                String[] words = string.split("\\s+");
                String current = "";
                for (String word : words) {
                    if (current.length() + word.length() < 80) {
                        current += word + " ";
                    } else {
                        lines.add(current);
                        current = word + " ";
                    }
                }
                if (!current.isEmpty()) {
                    lines.add(current);
                }
                for (String line : lines) {
                    drawString(g, line, y);
                    y += 15;
                }
                return y;
            }
        }

        private void drawString(Graphics2D g, String text, int y) {
            drawString(g, text, y, MARGIN);
        }

        private void drawString(Graphics2D g, String text, int y, int x) {
            if (text == null) {
                return;
            }
            g.drawString(text, (float) x, (float) y);
        }
    }

    public AboutBox(Frame owner, String productName, String productVersion, String licensor, String url, String text, boolean renderTextNextToLogo,
            Image productLogo) {
        this(owner, createProperties(productName, productVersion, licensor, url, text, renderTextNextToLogo), productLogo);
    }

    public AboutBox(Frame owner, String productVersion, Image productLogo) {
        this(owner, createProperties(productVersion), productLogo);
    }

    public AboutBox(Frame owner, String productVersion) {

        this(owner, createProperties(productVersion), null);
    }

    public AboutBox(Frame owner, Properties properties, Image productLogo) {
        super(owner, "About", true);
        setResizable(false);

        setLayout(new BorderLayout());

        String name = properties.getProperty("name");
        if (name != null) {
            setTitle("About " + name);
        }
        ContentPanel contentPanel = new ContentPanel(properties, productLogo);
        add(contentPanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final String url = properties.getProperty("url");
        if (url != null) {
            String shownURL = url;
            if (shownURL.length() > MAX_SHOWN_LINK_LENGTH) {
                shownURL = shownURL.substring(0, MAX_SHOWN_LINK_LENGTH) + "...";
            }
            buttonPanel.add(new LinkRemoteButton(new ResourceAction("link_action", url, shownURL) {

                private static final long serialVersionUID = 1L;

                @Override
                public void loggedActionPerformed(ActionEvent e) {
                    UrlHandlerService.openInBrowser(url);
                }
            }));
        }

        ResourceAction closeAction = new ResourceAction("close") {

            private static final long serialVersionUID = 1407089394491740308L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                dispose();
            }

        };
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "CANCEL");
        getRootPane().getActionMap().put("CANCEL", closeAction);

        pack();
        if (owner != null) {
            setLocationRelativeTo(owner);
        } else {
            setLocationRelativeTo(ApplicationFrame.getApplicationFrame());
        }
    }

    public static Properties createProperties(InputStream inputStream, String productVersion) {
        Properties properties = new Properties();
        if (inputStream != null) {
            try {
                properties.load(inputStream);
            } catch (Exception e) {
                LogService.getRoot().error("com.owc.singularity.studio.gui.tools.dialogs.AboutBox.reading_splash_screen_error", e.getMessage());
            }
        }
        properties.setProperty("version", productVersion);
        return properties;
    }

    private static Properties createProperties(String productVersion) {
        Properties properties = new Properties();
        try {
            URL propUrl = Tools.getResource(PROPERTY_FILE);
            if (propUrl != null) {
                InputStream in = propUrl.openStream();
                properties.load(in);
                in.close();
            }
        } catch (Exception e) {
            LogService.getRoot().error("com.owc.singularity.studio.gui.tools.dialogs.AboutBox.reading_splash_screen_error", e.getMessage());
        }
        properties.setProperty("name", SingularityEngine.getApplicationTitle());
        properties.setProperty("version", productVersion);
        if (PlatformUtilities.getReleaseRevision() != null) {
            properties.setProperty("revision", PlatformUtilities.getReleaseRevision());
            properties.setProperty("platform", PlatformUtilities.getReleasePlatform().toString());
        }

        properties.setProperty(JAVA_VERSION,
                I18N.getGUILabel("about.java_version.label", System.getProperty("java.vendor") + " " + System.getProperty("java.version")));

        return properties;
    }

    private static Properties createProperties(String productName, String productVersion, String licensor, String url, String text,
            boolean renderTextNextToLogo) {
        Properties properties = new Properties();
        properties.setProperty("name", productName);
        properties.setProperty("version", productVersion);
        properties.setProperty(LICENSOR, licensor);
        properties.setProperty(LICENSE, "Website: " + url);
        properties.setProperty("more", text);
        properties.setProperty("textNextToLogo", "" + renderTextNextToLogo);
        properties.setProperty("url", url);
        return properties;
    }

    /**
     * This method removes a leading "SingularityEngine " String from e.g. product names.
     */
    private static String removeLeadingName(String productString) {
        return productString.startsWith("AI2FITEngine ") ? productString.substring(11) : productString;
    }

}
