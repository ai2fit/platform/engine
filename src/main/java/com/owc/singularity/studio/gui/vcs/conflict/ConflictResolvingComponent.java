package com.owc.singularity.studio.gui.vcs.conflict;

import java.util.EventListener;

import com.owc.singularity.repository.vcs.MergeConflict;
import com.owc.singularity.studio.gui.vcs.conflict.model.MergeConflictDecision;

public interface ConflictResolvingComponent {

    void show();

    void dispose();

    interface ConflictResolvedEventListener extends EventListener {

        void onResolve(ConflictResolvingComponent self);
    }

    void addResolveListener(ConflictResolvedEventListener listener);

    void removeResolveListener(ConflictResolvedEventListener listener);

    boolean isResolved();

    MergeConflict getConflict();

    void setDecision(MergeConflictDecision decision);

    MergeConflictDecision getDecision();
}
