package com.owc.singularity.studio.gui.repository.worker;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.tree.DefaultTreeModel;

import com.owc.singularity.studio.gui.repository.model.AbstractRepositoryTreeModel;
import com.owc.singularity.studio.gui.repository.model.RepositoryTreeNode;

/**
 * Represent the UI event that affects the tree. The {@link #applyOn(AbstractRepositoryTreeModel)}
 * will be called on the {@link DefaultTreeModel tree model} on the EDT thread.
 * <p>
 * Instances of {@link TreeChange} are processed in batches, meaning that "combining" multiple
 * instances with a similar effect could reduce UI events. See the {@link #getCombineKey()} and
 * {@link #combine(TreeChange)}
 *
 * @author Hatem Hamad
 */
interface TreeChange {

    void applyOn(AbstractRepositoryTreeModel treeModel);

    TreeChange combine(TreeChange otherTreeChange);

    default String getCombineKey() {
        return getClass().getName();
    }

    static Collector<TreeChange, Map<String, TreeChange>, Collection<TreeChange>> combining() {
        return combiner;
    }

    Collector<TreeChange, Map<String, TreeChange>, Collection<TreeChange>> combiner = new Collector<>() {

        @Override
        public Supplier<Map<String, TreeChange>> supplier() {
            return LinkedHashMap::new;
        }

        @Override
        public BiConsumer<Map<String, TreeChange>, TreeChange> accumulator() {
            return (stringTreeChangeMap, rightChange) -> {
                TreeChange leftChange = stringTreeChangeMap.get(rightChange.getCombineKey());
                TreeChange combined = leftChange == null ? rightChange : leftChange.combine(rightChange);
                if (combined == null)
                    throw new NullPointerException("Cannot accumulate null changes. Combining " + leftChange + " with" + rightChange + "resulted in null");
                stringTreeChangeMap.put(rightChange.getCombineKey(), combined);
            };
        }

        @Override
        public BinaryOperator<Map<String, TreeChange>> combiner() {
            return (mainMap, otherMap) -> {
                for (String combinationKey : otherMap.keySet()) {
                    mainMap.computeIfPresent(combinationKey, (s, treeChange) -> treeChange.combine(otherMap.get(combinationKey)));
                }
                return mainMap;
            };
        }

        @Override
        public Function<Map<String, TreeChange>, Collection<TreeChange>> finisher() {
            return Map::values;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED));
        }
    };

    private static int[] cleanupIndices(RepositoryTreeNode parentNode, int[] newIndices) {
        final int childCount = parentNode.getChildCount();
        return Arrays.stream(newIndices).sorted().filter(value -> value >= 0 && value < childCount).distinct().toArray();
    }

    class Insertion implements TreeChange {

        private final RepositoryTreeNode parentNode;
        private final RepositoryTreeNode[] newNodes;

        Insertion(RepositoryTreeNode parentNode, RepositoryTreeNode[] newNodes) {
            this.parentNode = parentNode;
            this.newNodes = newNodes;
        }

        @Override
        public void applyOn(AbstractRepositoryTreeModel treeModel) {
            int[] newIndices = cleanupIndices(parentNode, Arrays.stream(newNodes).mapToInt(parentNode::getIndex).toArray());
            treeModel.nodesWereInserted(parentNode, newIndices);
        }

        @Override
        public TreeChange combine(TreeChange otherTreeChange) {
            return new Insertion(parentNode,
                    Stream.concat(Arrays.stream(newNodes), Arrays.stream(((Insertion) otherTreeChange).newNodes)).toArray(RepositoryTreeNode[]::new));
        }

        @Override
        public String getCombineKey() {
            return "insert-" + parentNode.getRepositoryPath();
        }

        @Override
        public String toString() {
            return "Inserting children of " + parentNode.getRepositoryPath();
        }
    }

    class Modification implements TreeChange {

        private final RepositoryTreeNode parentNode;
        private final RepositoryTreeNode[] updatedNodes;

        Modification(RepositoryTreeNode parentNode, RepositoryTreeNode[] updatedNodes) {
            this.parentNode = parentNode;
            this.updatedNodes = updatedNodes;
        }

        @Override
        public void applyOn(AbstractRepositoryTreeModel treeModel) {
            if (updatedNodes == null) {
                // this is the root node
                treeModel.nodesChanged(parentNode, null);
                return;
            }
            int[] updatedIndices = cleanupIndices(parentNode, Arrays.stream(updatedNodes).mapToInt(parentNode::getIndex).toArray());
            if (updatedIndices.length == 0) {
                // updates indices may get deprecated and removed during cleanup call
                // meaning that these updates were related to old nodes
                return;
            }
            treeModel.nodesChanged(parentNode, updatedIndices);
        }

        @Override
        public TreeChange combine(TreeChange otherTreeChange) {
            return new Modification(parentNode, Stream.concat(Arrays.stream(updatedNodes), Arrays.stream(((Modification) otherTreeChange).updatedNodes))
                    .toArray(RepositoryTreeNode[]::new));
        }

        @Override
        public String getCombineKey() {
            return "modify-" + parentNode.getRepositoryPath();
        }

        @Override
        public String toString() {
            return "Modified children of " + parentNode.getRepositoryPath();
        }
    }

    class Removal implements TreeChange {

        private final RepositoryTreeNode parentNode;
        private final RepositoryTreeNode[] removedNodes;
        private final int[] removedIndices;

        public Removal(RepositoryTreeNode parentNode, RepositoryTreeNode[] removedNodes, int[] removedIndices) {
            this.parentNode = parentNode;
            this.removedNodes = removedNodes;
            this.removedIndices = removedIndices;
        }

        @Override
        public void applyOn(AbstractRepositoryTreeModel treeModel) {
            List<RepositoryTreeNode> copy = Arrays.asList(removedNodes);
            List<RepositoryTreeNode> sorted = Arrays.stream(removedNodes)
                    .sorted(Comparator.comparingInt(node -> removedIndices[copy.indexOf(node)]))
                    .collect(Collectors.toList());
            Arrays.sort(this.removedIndices);
            treeModel.nodesWereRemoved(parentNode, removedIndices, sorted.toArray(RepositoryTreeNode[]::new));
        }

        @Override
        public TreeChange combine(TreeChange otherTreeChange) {
            return new Removal(parentNode,
                    Stream.concat(Arrays.stream(removedNodes), Arrays.stream(((Removal) otherTreeChange).removedNodes)).toArray(RepositoryTreeNode[]::new),
                    IntStream.concat(Arrays.stream(removedIndices), Arrays.stream(((Removal) otherTreeChange).removedIndices)).toArray());
        }

        @Override
        public String getCombineKey() {
            return "remove-" + parentNode.getRepositoryPath();
        }

        @Override
        public String toString() {
            return "Removed children of " + parentNode.getRepositoryPath();
        }
    }

    class StructureChanged implements TreeChange {

        private final RepositoryTreeNode entryNode;

        public StructureChanged(RepositoryTreeNode entryNode) {
            this.entryNode = entryNode;
        }

        @Override
        public void applyOn(AbstractRepositoryTreeModel treeModel) {
            treeModel.nodeStructureChanged(entryNode);
        }

        @Override
        public TreeChange combine(TreeChange otherTreeChange) {
            return this;
        }

        @Override
        public String getCombineKey() {
            return "changing-structure-" + entryNode.getRepositoryPath();
        }

        @Override
        public String toString() {
            return "Changed node structure for " + entryNode.getRepositoryPath();
        }
    }
}
