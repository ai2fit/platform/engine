/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity.studio.gui.editor.pipeline;


import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.EventListenerList;

import org.apache.commons.io.FilenameUtils;

import com.owc.singularity.SingularityEngine;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.operator.CompositeDummyOperator;
import com.owc.singularity.engine.operator.DummyOperator;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.OperatorChain;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.pipeline.PipelineState;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entries;
import com.owc.singularity.studio.gui.ApplicationFrame;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.actions.AutoWireAction;
import com.owc.singularity.studio.gui.actions.SearchOperatorAction;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ProcessLayoutXMLFilter;
import com.owc.singularity.studio.gui.editor.ZoomState;
import com.owc.singularity.studio.gui.editor.action.ProcessActions;
import com.owc.singularity.studio.gui.editor.action.ProcessChangingAction;
import com.owc.singularity.studio.gui.editor.event.ProcessEditorEventListener;
import com.owc.singularity.studio.gui.editor.event.ProcessStorageListener;
import com.owc.singularity.studio.gui.editor.event.ProcessUserInteractionListener;
import com.owc.singularity.studio.gui.editor.pipeline.annotation.AnnotationsVisualizer;
import com.owc.singularity.studio.gui.editor.pipeline.connection.RemoveHoveredConnectionDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.connection.RemoveSelectedConnectionDecorator;
import com.owc.singularity.studio.gui.editor.pipeline.draw.ProcessRenderer;
import com.owc.singularity.studio.gui.editor.pipeline.view.RenderPhase;
import com.owc.singularity.studio.gui.editor.pipeline.view.components.OperatorWarningHandler;
import com.owc.singularity.studio.gui.look.Colors;
import com.owc.singularity.studio.gui.repository.RepositoryLocationChooser;
import com.owc.singularity.studio.gui.tools.*;
import com.owc.singularity.studio.gui.tools.dialogs.ConfirmDialog;


/**
 * Contains the main {@link ProcessEditorPanel} and a {@link ProcessNavigationPanel} to navigate
 * through the process.
 *
 * @author Simon Fischer, Tobias Malbrecht, Jan Czogalla, Hatem Hamad
 */
public class ProcessPanel extends JPanel {

    private final RemoveSelectedConnectionDecorator removeSelectedConnectionDecorator;
    private final RemoveHoveredConnectionDecorator removeHoveredConnectionDecorator;

    private static final long serialVersionUID = -4419160224916991497L;

    /** the process renderer instance */
    private final ProcessEditorPanel editorPanel;

    /** the handler for operator warning bubbles */
    private final OperatorWarningHandler operatorWarningHandler;

    private final ProcessNavigationPanel processNavigationPanel;

    private final JButton resetZoom;
    private final JButton zoomIn;
    private final JButton zoomOut;
    private final JButton search;

    private final EventListenerList eventListenerList = new EventListenerList();
    private final JScrollPane scrollPane;
    private final Map<OperatorChain, Rectangle> operatorChainViewportPositionMap = new HashMap<>();
    private boolean readOnly;

    public ProcessPanel() {
        this.processNavigationPanel = new ProcessNavigationPanel();

        this.editorPanel = new ProcessEditorPanel();
        UpdatePanelsOnProcessChange editorEventListener = new UpdatePanelsOnProcessChange();
        editorPanel.addProcessEditorEventListener(editorEventListener);
        // listen for display chain changes and update breadcrumbs accordingly
        // listen for operator selection and view changes (displayed chain/zoom) to adjust view
        // position as needed
        editorPanel.addProcessInteractionListener(new ProcessUserInteractionListener() {

            private ZoomState currentZoomState;
            private OperatorChain currentOperatorChain;

            @Override
            public void onDisplayedChainChanged(OperatorChain displayedChain) {
                if (currentOperatorChain != null)
                    operatorChainViewportPositionMap.put(currentOperatorChain, getViewPort().getViewRect());
                currentOperatorChain = displayedChain;
                if (operatorChainViewportPositionMap.containsKey(currentOperatorChain)) {
                    SwingUtilities.invokeLater(() -> getViewPort().scrollRectToVisible(operatorChainViewportPositionMap.get(currentOperatorChain)));
                }
                processNavigationPanel.setSelectedNode(displayedChain);
                // after change: restore zoom lvl first
                if (displayedChain != null) {

                    Double zoom = ProcessLayoutXMLFilter.lookupOperatorChainZoom(displayedChain);
                    if (zoom != null && currentZoomState != null) {
                        if (zoom != currentZoomState.getZoom()) {
                            editorPanel.setZoom(zoom);
                        }
                    } else {
                        editorPanel.resetZoom();
                    }
                }
            }

            @Override
            public void onZoomStateChange(ZoomState zoomState) {
                currentZoomState = zoomState;
                zoomIn.setEnabled(zoomState.canZoomIn());
                zoomOut.setEnabled(zoomState.canZoomOut());
                double zoom = zoomState.getZoom();
                resetZoom.setText(zoom + "%");
                resetZoom.setVisible(zoom != 100);
            }

            @Override
            public void onOperatorSelectionChange(List<Operator> selectedOperators) {
                if (selectedOperators.isEmpty()) {
                    return;
                }

                Operator operator = selectedOperators.iterator().next();
                processNavigationPanel.addToHistory(operator);
                Rectangle2D opRect = ProcessLayoutXMLFilter.lookupOperatorRectangle(operator);
                OperatorChain parent = operator.getParent();
                if (opRect == null || parent == null) {
                    return;
                }
                if (ProcessLayoutXMLFilter.lookupRestore(operator)) {
                    // don't scroll further after undo/redo
                    ProcessLayoutXMLFilter.resetRestore(operator);
                    return;
                }
                if (selectedOperators.size() != 1) {
                    // only scroll to operator if it is the single one selected;
                    // there is no sensible behavior for scrolling to multiple operators yet
                    return;
                }
                // scroll to operator directly (chain is displayed)
                scrollToOperator(operator);
            }
        });

        // listen for process panel resizing events to adapt the render size
        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(final ComponentEvent e) {
                super.componentResized(e);
                editorPanel.resize();
            }
        });
        addHierarchyListener(e -> editorPanel.resize());


        removeSelectedConnectionDecorator = new RemoveSelectedConnectionDecorator();
        removeHoveredConnectionDecorator = new RemoveHoveredConnectionDecorator();
        addEditorDecorators();

        ViewToolBar toolBar = new ViewToolBar(ViewToolBar.LEFT);
        search = new JButton(new SearchOperatorAction("processpanel.search"));

        zoomIn = new JButton(new ResourceActionAdapter(true, "processpanel.zoom_in") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                editorPanel.zoomIn();
            }
        });
        zoomOut = new JButton(new ResourceActionAdapter(true, "processpanel.zoom_out") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                editorPanel.zoomOut();
            }
        });
        resetZoom = new JButton(new ResourceActionAdapter(true, "processpanel.reset_zoom") {

            private static final long serialVersionUID = 1L;

            @Override
            public void loggedActionPerformed(ActionEvent e) {
                editorPanel.resetZoom();
            }
        });
        resetZoom.setHorizontalTextPosition(SwingConstants.LEADING);
        resetZoom.setVisible(false);

        JButton copyProcess = new JButton(new CopyProcessAction());

        JButton pasteProcess = new JButton(new PasteProcessAction());

        toolBar.add(search);
        toolBar.add(resetZoom);
        toolBar.add(zoomIn);
        toolBar.add(zoomOut);
        toolBar.addSeparator();
        toolBar.add(copyProcess);
        toolBar.add(pasteProcess);
        toolBar.addSeparator();

        ResourceAction addAnnotationAction = editorPanel.getAnnotationsVisualizer().makeAddAnnotationAction(null);
        toolBar.add(addAnnotationAction, ViewToolBar.RIGHT);
        AutoWireAction autoWireAction = new AutoWireAction();
        toolBar.add(autoWireAction, ViewToolBar.RIGHT);

        // toolBar.add(editorPanel.getFlowVisualizer().SHOW_ORDER_TOGGLEBUTTON, ViewToolBar.RIGHT);
        Action autoFitAction = ProcessActions.AUTO_FIT_ACTION;
        toolBar.add(autoFitAction, ViewToolBar.RIGHT);

        setLayout(new BorderLayout());

        JLayeredPane processLayeredPane = new JLayeredPane();
        processLayeredPane.setLayout(new BorderLayout());
        processLayeredPane.add(processNavigationPanel, BorderLayout.WEST, 1);
        processLayeredPane.add(toolBar, BorderLayout.EAST, 0);
        processLayeredPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        processLayeredPane.setDoubleBuffered(true);
        add(processLayeredPane, BorderLayout.NORTH);

        scrollPane = new ExtendedJScrollPane(editorPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Colors.TEXTFIELD_BORDER));
        scrollPane.setDoubleBuffered(true);
        add(scrollPane, BorderLayout.CENTER);

        new ProcessPanelScroller(editorPanel, scrollPane);

        // add event decorator for operator warning icons
        operatorWarningHandler = new OperatorWarningHandler();
        editorPanel.addEventDecorator(operatorWarningHandler, RenderPhase.OPERATOR_ADDITIONS);
    }

    private void addEditorDecorators() {
        editorPanel.addDrawDecorator(removeSelectedConnectionDecorator, RenderPhase.CONNECTIONS);
        editorPanel.addEventDecorator(removeSelectedConnectionDecorator, RenderPhase.CONNECTIONS);
        editorPanel.addDrawDecorator(removeHoveredConnectionDecorator, RenderPhase.CONNECTIONS);
        // event decorator must be in phase OVERLAY such that it comes before selecting of
        // connections which is done in between phases OPERATOR_ADDITIONS and OPERATORS
        editorPanel.addEventDecorator(removeHoveredConnectionDecorator, RenderPhase.OVERLAY);
    }

    private void removeEditorDecorators() {
        editorPanel.removeDrawDecorator(removeSelectedConnectionDecorator, RenderPhase.CONNECTIONS);
        editorPanel.removeEventDecorator(removeSelectedConnectionDecorator, RenderPhase.CONNECTIONS);
        editorPanel.removeDrawDecorator(removeHoveredConnectionDecorator, RenderPhase.CONNECTIONS);
        editorPanel.removeEventDecorator(removeHoveredConnectionDecorator, RenderPhase.OVERLAY);
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    /**
     * The {@link ProcessEditorPanel} which is responsible for displaying the current process as
     * well as interaction with it
     *
     * @return the instance, never {@code null}
     */
    public ProcessEditorPanel getProcessEditor() {
        return editorPanel;
    }

    /**
     * The {@link AnnotationsVisualizer} instance tied to the process renderer.
     *
     * @return the instance, never {@code null}
     */
    public AnnotationsVisualizer getAnnotationsVisualizer() {
        return getProcessEditor().getAnnotationsVisualizer();
    }

    /**
     * The {@link ProcessActions} instance tied to the process renderer.
     *
     * @return the instance, never {@code null}
     */
    public ProcessActions getActions() {
        return getProcessEditor().getActions();
    }

    public JViewport getViewPort() {
        return scrollPane.getViewport();
    }

    /** Returns the center position of the current view. */
    public Point getCurrentViewCenter() {
        Rectangle viewRect = getViewPort().getViewRect();
        return new Point((int) viewRect.getCenterX(), (int) viewRect.getCenterY());
    }

    /**
     * Scrolls the view to the specified {@link Operator}, making it visible. Will not force
     * centering
     *
     * @param operator
     *            the operator to focus on
     * @return whether the scrolling was successful
     * @since 7.5
     * @see #scrollToOperator(Operator, boolean)
     */
    public boolean scrollToOperator(Operator operator) {
        return scrollToOperator(operator, false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }

    /**
     * Scrolls the view to the specified {@link Operator}, making it the center of the view if
     * necessary, indicated by the flag.
     *
     * @param operator
     *            the operator to focus on
     * @param toCenter
     *            flag to indicate whether to force centering
     * @return whether the scrolling was successful
     * @since 7.5
     * @see #scrollToViewPosition(Point)
     */
    public boolean scrollToOperator(Operator operator, boolean toCenter) {
        Rectangle2D opRect = ProcessLayoutXMLFilter.lookupOperatorRectangle(operator);
        if (opRect == null) {
            return false;
        }
        int pIndex = editorPanel.getProcessIndexOfOperator(operator);
        if (pIndex == -1) {
            return false;
        }
        Rectangle opViewRect = getOpViewRect(opRect, pIndex);
        Rectangle viewRect = getViewPort().getViewRect();
        if (!toCenter) {
            if (viewRect.contains(opViewRect)) {
                // if operator visible, do nothing
                return false;
            }
            if (viewRect.intersects(opViewRect)) {
                // if partially visible, just scroll it into view
                opViewRect.translate(-viewRect.x, -viewRect.y);
                getViewPort().scrollRectToVisible(opViewRect);
                // return false nonetheless, see PortInfoBubble
                return false;
            }
        }
        Point opCenter = new Point((int) opViewRect.getCenterX(), (int) opViewRect.getCenterY());
        scrollToViewPosition(opCenter);
        return true;
    }

    /**
     * Calculates the view rectangle of the given process rectangle, adding in some border padding.
     *
     * @param opRect
     *            the operator rectangle in the process
     * @param pIndex
     *            the process index of the corresponding operator
     * @return the view rectangle
     * @since 7.5
     */
    private Rectangle getOpViewRect(Rectangle2D opRect, int pIndex) {
        Rectangle target = new Rectangle();
        target.setLocation(editorPanel.fromProcessSpace(opRect.getBounds().getLocation(), pIndex));
        ZoomState zoomState = editorPanel.getZoomState();
        double zoomFactor = zoomState.getZoom() / 100;
        target.setSize((int) (opRect.getWidth() * zoomFactor), (int) (opRect.getHeight() * zoomFactor));
        target.grow(ProcessRenderer.PORT_SIZE, ProcessRenderer.WALL_WIDTH * 2);
        return target;
    }

    /**
     * Scrolls the view to the specified {@link Point center point}.
     *
     * @param center
     *            the point to focus on
     * @since 7.5
     * @see #scrollToProcessPosition(Point, int)
     */
    public void scrollToViewPosition(Point center) {
        getViewPort().scrollRectToVisible(getScrollRectangle(center));
    }

    /**
     * Scrolls the view to the specified {@link Point process point}.
     *
     * @param center
     *            the point to focus on
     * @param processIndex
     *            the index of the process to focus on
     * @since 7.5
     * @see #scrollToViewPosition(Point)
     */
    public void scrollToProcessPosition(Point center, int processIndex) {
        scrollToViewPosition(editorPanel.fromProcessSpace(center, processIndex));
    }

    /**
     * Calculates the relative scroll rectangle from the current view, so that the specified
     * {@link Point} is in the center if possible.
     *
     * @param center
     *            the point to focus on
     * @return the relative scroll rectangle
     * @since 7.5
     */
    private Rectangle getScrollRectangle(Point center) {
        Point newViewPoint = new Point(center);
        Rectangle currentViewRect = getViewPort().getViewRect();
        newViewPoint.translate((int) -currentViewRect.getCenterX(), (int) -currentViewRect.getCenterY());
        // Don't scroll outside the viewport
        if (newViewPoint.x < 0) {
            newViewPoint.x = 0;
        }
        if (newViewPoint.y < 0) {
            newViewPoint.y = 0;
        }
        return new Rectangle(newViewPoint, currentViewRect.getSize());
    }

    /**
     * Returns the handler for operator warning bubbles.
     *
     * @return the handler for operator warnings, never {@code null}
     */
    public OperatorWarningHandler getOperatorWarningHandler() {
        return operatorWarningHandler;
    }

    public void setProcess(AbstractPipeline process) {
        getProcessEditor().setProcess(process);
        fireProcessLoaded(process, process.getPath());
    }

    public void updateProcess(AbstractPipeline process) {
        MainFrame.INSTANCE.stopProcess();
        getProcessEditor().updateProcess(process);
        MainFrame.INSTANCE.setMainProcessPanel(this);
    }

    /**
     * Closes the current process
     *
     * @param askForConfirmation
     *            if <code>true</code>, will prompt the user if he really wants to close the current
     *            process
     * @return
     */
    public boolean isCloseable(final boolean askForConfirmation) {
        if (!isEdited()) {
            return true;
        }
        AbstractPipeline process = getProcess();
        RepositoryPath destination = process.getPath();
        String fileName = destination == null ? "" : " to " + destination;
        switch (SwingTools.showConfirmDialog("save", ConfirmDialog.YES_NO_CANCEL_OPTION, getName(), fileName)) {
            case ConfirmDialog.YES_OPTION:
                try {
                    if (destination == null) {
                        destination = RepositoryLocationChooser.selectLocation(null, null, MainFrame.INSTANCE.getExtensionsMenu(), true, false, false, true);
                        if (destination == null) {
                            // user canceled the location selection operation
                            // retry the confirmation, since this cancellation could be
                            // unintentional
                            return isCloseable(askForConfirmation);
                        }
                    }
                    saveProcessAt(destination);
                } catch (IOException e) {
                    SwingTools.showSimpleErrorMessage("cannot_save_process", e, destination, e.getMessage());
                } catch (Exception ignored) {
                }

                // it may happen that save() does not actually save the process, because the
                // user hits cancel in the
                // saveAs dialog or an error occurs. In this case the process won't be marked as
                // unchanged. Thus,
                // we return the process changed status.
                return !isEdited();
            case ConfirmDialog.NO_OPTION:
                // ask for confirmation before stopping the currently running process (if
                // askForConfirmation=true)
                if (askForConfirmation) {
                    if (process.getState() == PipelineState.RUNNING || process.getState() == PipelineState.PAUSED) {
                        if (SwingTools.showConfirmDialog("close_running_process", ConfirmDialog.YES_NO_OPTION) != ConfirmDialog.YES_OPTION) {
                            return false;
                        }
                    }
                }
                if (process.getState() != PipelineState.STOPPED) {
                    process.stop();
                }
                return true;
            default: // cancel
                return false;
        }

    }

    public void saveProcessAt(RepositoryPath destination) throws Exception {
        AbstractPipeline process = getProcess();
        if (destination == null) {
            throw new IOException("No pipeline location is specified.");
        }
        for (Operator operator : process.getAllOperators()) {
            if (operator instanceof CompositeDummyOperator || operator instanceof DummyOperator) {
                if (SwingTools.showConfirmDialog("save_dummy", ConfirmDialog.YES_NO_OPTION) == ConfirmDialog.NO_OPTION) {
                    throw new Exception("The pipeline should not be saved.");
                }
                break;
            }
        }
        Entries.write(destination, MetaData.forObject(process), process);
        process.setPath(destination);
        getProcessEditor().getModel().processWasSaved();
        fireProcessStored(process, destination);
    }

    /**
     * Adds the given {@link ProcessStorageListener}.
     *
     * @since 7.5
     */
    public void addProcessStorageListener(final ProcessStorageListener listener) {
        synchronized (eventListenerList) {
            eventListenerList.add(ProcessStorageListener.class, listener);
        }
    }

    /**
     * Removes the given {@link ProcessStorageListener}.
     *
     * @since 7.5
     */
    public void removeProcessStorageListener(final ProcessStorageListener listener) {
        synchronized (eventListenerList) {
            eventListenerList.remove(ProcessStorageListener.class, listener);
        }
    }

    /**
     * Informs the {@link ProcessStorageListener ProcessStorageListeners} that the process was
     * loaded.
     *
     */
    private void fireProcessLoaded(AbstractPipeline process, RepositoryPath path) {
        ProcessStorageListener[] listeners = eventListenerList.getListeners(ProcessStorageListener.class);
        for (ProcessStorageListener l : listeners) {
            l.onLoad(process, path);
        }
    }

    /**
     * Informs the {@link ProcessStorageListener ProcessStorageListeners} that the process was
     * saved.
     *
     */
    private void fireProcessStored(AbstractPipeline process, RepositoryPath path) {
        ProcessStorageListener[] listeners = eventListenerList.getListeners(ProcessStorageListener.class);
        for (ProcessStorageListener l : listeners) {
            l.onStore(process, path);
        }
    }

    public AbstractPipeline getProcess() {
        return getProcessEditor().getProcess();
    }

    public boolean isEdited() {
        return getProcessEditor().isEdited();
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        if (readOnly) {
            removeEditorDecorators();
        } else {
            addEditorDecorators();
        }
        editorPanel.setReadOnly(readOnly);
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    private class ProcessNavigationPanel extends ParentButtonPanel<Operator> {

        private static final long serialVersionUID = -2196273913282600609L;
        private final ProcessParentButtonModel model;

        public ProcessNavigationPanel() {
            super(new ProcessParentButtonModel(null));
            model = (ProcessParentButtonModel) getModel();
            addActionListener(e -> {
                Operator selectedNode = getSelectedNode();
                editorPanel.selectAndShowOperator(selectedNode, false);
            });
        }

        public void setProcess(AbstractPipeline process) {
            model.setProcess(process);
        }
    }

    private class UpdatePanelsOnProcessChange implements ProcessEditorEventListener {

        @Override
        public void onProcessLoad(AbstractPipeline process) {
            processNavigationPanel.clearHistory();
        }

        @Override
        public void onProcessEdit(AbstractPipeline process) {
            editorPanel.processUpdated();
        }
    }

    private class PasteProcessAction extends ProcessChangingAction {

        private static final long serialVersionUID = 1L;

        public PasteProcessAction() {
            super(true, "processpanel.paste_process");
        }

        @Override
        public void loggedActionPerformed(ActionEvent e) {
            if (isFocusedProcessReadOnly())
                return;
            Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
            DataFlavor[] dataFlavors = clipBoard.getContents(this).getTransferDataFlavors();
            AbstractPipeline process = null;
            if (dataFlavors != null && dataFlavors.length > 0) {
                try {
                    // we check for the appropriate data flavor and handle the clipboard
                    // contents accordingly
                    if (Arrays.asList(dataFlavors).contains(DataFlavor.stringFlavor)) {
                        String xml = ((String) clipBoard.getContents(this).getTransferData(DataFlavor.stringFlavor));
                        process = XMLImporter.parse(xml);
                    } else if (Arrays.asList(dataFlavors).contains(DataFlavor.javaFileListFlavor)) {
                        String xml = null;
                        // we did the check in firePasteProcess
                        // the cast is safe
                        @SuppressWarnings("unchecked")
                        List<File> fileList = ((List<File>) clipBoard.getContents(this).getTransferData(DataFlavor.javaFileListFlavor));
                        for (File file : fileList) {
                            if (SingularityEngine.PROCESS_FILE_EXTENSION.equals(FilenameUtils.getExtension(file.getName()))) {
                                xml = new String(Files.readAllBytes(file.toPath()));
                                break;
                            }
                        }
                        if (xml != null) {
                            process = XMLImporter.parse(xml);
                        } // else: no .rmp file in the file list
                    } // else: no string and no .rmp file on the clipboard
                } catch (XMLException | UnsupportedFlavorException | IOException | OperatorCreationException e1) {
                    // Something went wrong. Most likely the xml string / file was not a valid
                    // process
                }
            } // else: nothing on the clipboard

            if (process != null) {
                // Asks the user if he wants to change the currently opened process, or open the
                // pasted content in another tab
                ConfirmDialog dialog = new ConfirmDialog(ApplicationFrame.getApplicationFrame(), "process_panel.paste_process.confirm_overwrite",
                        ConfirmDialog.YES_NO_CANCEL_OPTION, false, null, null, "process_panel.paste_process.overwrite", "process_panel.paste_process.open_new");
                dialog.setVisible(true);
                switch (dialog.getReturnOption()) {
                    case ConfirmDialog.YES_OPTION:
                        // overwrite
                        updateProcess(process);
                        break;
                    case ConfirmDialog.NO_OPTION:
                        // open as new
                        MainFrame.INSTANCE.openProcess(process, false);
                        break;
                    case ConfirmDialog.CANCEL_OPTION:
                        break;
                }
            } else {
                NotificationPopup.showFadingPopup(
                        SwingTools.createNotificationPanel("gui.bubble.error.icon", "gui.dialog.error.paste_process_button.invalid_content.message"),
                        ProcessPanel.this, NotificationPopup.PopupLocation.LOWER_RIGHT, 3000, 30, 40);
            }
        }

    }

    private class CopyProcessAction extends ResourceActionAdapter {

        private static final long serialVersionUID = 1L;

        public CopyProcessAction() {
            super(true, "processpanel.copy_process");
        }

        @Override
        public void loggedActionPerformed(ActionEvent e) {
            String processXML = editorPanel.getProcess().getRootOperator().getXML(false);
            Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
            StringSelection stringSelection = new StringSelection(processXML);
            clipBoard.setContents(stringSelection, null);
        }

    }
}
