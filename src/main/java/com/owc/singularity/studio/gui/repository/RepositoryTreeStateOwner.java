package com.owc.singularity.studio.gui.repository;

/**
 * Represent the owner of the {@link RepositoryTreeState}
 *
 * @author Hatem Hamad
 */
public interface RepositoryTreeStateOwner {

    RepositoryTreeState getTreeState();

    void setTreeState(RepositoryTreeState state);
}
