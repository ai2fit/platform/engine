/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.viewer.metadata.model;


import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;

import com.owc.singularity.engine.object.data.exampleset.Attribute;
import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.object.data.exampleset.ValueType;
import com.owc.singularity.engine.object.data.exampleset.statistics.ExampleSetStatistics;
import com.owc.singularity.studio.gui.tools.AttributeGuiTools;
import com.owc.singularity.studio.gui.viewer.metadata.AttributeStatisticsPanel;


/**
 * Model for {@link AttributeStatisticsPanel}s which are backed by a numerical {@link Attribute}.
 *
 * @author Marco Boeck
 *
 */
public class NumericalAttributeStatisticsModel extends AbstractAttributeStatisticsModel {

    /** the index for the histogram chart */
    private static final int INDEX_HISTOGRAM_CHART = 0;

    /** the max number of bins */
    private static final int MAX_BINS_HISTOGRAM = 10;

    /** used to color the chart background invisible */
    private static final Color COLOR_INVISIBLE = new Color(255, 255, 255, 0);

    /** the average of the numerical values */
    private double average;

    /** the standard deviation of the numerical values */
    private double deviation;

    /** the minimum of the numerical values */
    private double minimum;

    /** the maximum of the numerical values */
    private double maximum;

    /** array of charts for this model */
    private JFreeChart[] chartsArray;

    private ExampleSetStatistics statistics;

    /**
     * Creates a new {@link NumericalAttributeStatisticsModel}.
     *
     * @param exampleSet
     * @param statistics
     * @param attribute
     */
    public NumericalAttributeStatisticsModel(ExampleSet exampleSet, ExampleSetStatistics statistics, Attribute attribute) {
        super(exampleSet, attribute);
        this.statistics = statistics;

        chartsArray = new JFreeChart[1];
    }

    @Override
    public void updateStatistics(ExampleSet exampleSet) {
        average = statistics.getStatistics(getAttribute()).getAverage();
        deviation = Math.sqrt(statistics.getStatistics(getAttribute()).getVariance());
        minimum = statistics.getStatistics(getAttribute()).getMinimumValue();
        maximum = statistics.getStatistics(getAttribute()).getMaximumValue();
        missing = statistics.getStatistics(getAttribute()).getNumberOfUnknownValues();
        examplesCount = statistics.getStatistics(getAttribute()).getNumberOfValues();

        fireStatisticsChangedEvent();
    }

    /**
     * Gets the average of the numerical values.
     *
     * @return
     */
    public double getAverage() {
        return average;
    }

    /**
     * Gets the standard deviation of the numerical values.
     *
     * @return
     */
    public double getDeviation() {
        return deviation;
    }

    /**
     * Gets the minimum of the numerical values.
     *
     * @return
     */
    public double getMinimum() {
        return minimum;
    }

    /**
     * Gets the maximum of the numerical values.
     *
     * @return
     */
    public double getMaximum() {
        return maximum;
    }

    @Override
    public JFreeChart getChartOrNull(int index) {
        prepareCharts();
        if (index == INDEX_HISTOGRAM_CHART) {
            return chartsArray[index];
        }

        return null;
    }

    /**
     * Creates a {@link HistogramDataset} for this {@link Attribute}.
     *
     * @param exampleSet
     * @return
     */
    private HistogramDataset createHistogramDataset(ExampleSet exampleSet) {
        HistogramDataset dataset = new HistogramDataset();

        double[] array = exampleSet.streamNumericAttribute(getAttribute()).filter(Double::isFinite).toArray();
        // add points to data set (if any)
        if (array.length > 0) {
            dataset.addSeries(getAttribute().getName(), array, Math.min(array.length, MAX_BINS_HISTOGRAM));
        }

        return dataset;
    }

    /**
     * Creates the histogram chart.
     *
     * @param exampleSet
     * @return
     */
    private JFreeChart createHistogramChart(ExampleSet exampleSet) {
        JFreeChart chart = ChartFactory.createHistogram(null, null, null, createHistogramDataset(exampleSet), PlotOrientation.VERTICAL, false, false, false);
        AbstractAttributeStatisticsModel.setDefaultChartFonts(chart);
        chart.setBackgroundPaint(null);
        chart.setBackgroundImageAlpha(0.0f);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setRangeGridlinesVisible(false);
        plot.setDomainGridlinesVisible(false);
        plot.setOutlineVisible(false);
        plot.setRangeZeroBaselineVisible(false);
        plot.setDomainZeroBaselineVisible(false);
        plot.setBackgroundPaint(COLOR_INVISIBLE);
        plot.setBackgroundImageAlpha(0.0f);

        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, AttributeGuiTools.getColorForValueType(ValueType.NUMERIC));
        renderer.setBarPainter(new StandardXYBarPainter());
        renderer.setDrawBarOutline(true);
        renderer.setShadowVisible(false);

        return chart;
    }

    @Override
    public void prepareCharts() {
        if (chartsArray[INDEX_HISTOGRAM_CHART] == null && getExampleSetOrNull() != null) {
            chartsArray[INDEX_HISTOGRAM_CHART] = createHistogramChart(getExampleSetOrNull());
        }
    }

}
