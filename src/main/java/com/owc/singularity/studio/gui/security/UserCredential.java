/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.security;


import java.net.URI;
import java.util.Arrays;
import java.util.Objects;


/**
 * The user credentials stored in a {@link Wallet}. Each username belongs to one URL.
 *
 * @author Miguel Buescher
 *
 */
public class UserCredential {

    private final URI uri;
    private String username;
    private char[] password;

    public UserCredential(URI uri, String username, char[] password) {
        super();
        this.uri = uri;
        this.username = username;
        this.password = Objects.requireNonNull(password, "password must not be null").clone();
    }

    public UserCredential(UserCredential entry) {
        this(entry.getURI(), entry.getUsername(), Arrays.copyOf(entry.getPassword(), entry.getPassword().length));
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(char[] password) {
        this.password = Objects.requireNonNull(password, "password must not be null").clone();
    }

    public URI getURI() {
        return uri;
    }

    public String getUsername() {
        return username;
    }

    public char[] getPassword() {
        return password;
    }
}
