/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.editor.pipeline.view.components;


import java.awt.Component;
import java.awt.Point;

import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.MetaDataError;
import com.owc.singularity.engine.object.IOObject;
import com.owc.singularity.engine.operator.error.ProcessSetupError.Severity;
import com.owc.singularity.engine.ports.Port;
import com.owc.singularity.studio.gui.editor.ProcessEditorPanel;
import com.owc.singularity.studio.gui.editor.ProcessRendererModel;
import com.owc.singularity.studio.gui.metadata.MetaDataRendererFactoryRegistry;
import com.owc.singularity.studio.gui.tools.SwingTools;
import com.owc.singularity.studio.gui.tools.components.ToolTipWindow.TipProvider;


/**
 * Provides tooltips for the {@link ProcessEditorPanel}.
 *
 * @author Marco Boeck
 * @since 6.4.0
 *
 */
public class ProcessRendererTooltipProvider implements TipProvider {

    /** Limit the data length in the tooltip so that the html parsing does not take forever */
    private static final int MAX_DATA_LENGTH = 1000;

    private ProcessRendererModel model;

    public ProcessRendererTooltipProvider(ProcessRendererModel model) {
        this.model = model;
    }

    @Override
    public Object getIdUnder(final Point point) {
        if (model.getConnectingPortSource() == null) {
            return model.getHoveringPort();
        } else {
            return null;
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public String getTip(final Object o) {
        Port port = (Port) o;
        StringBuilder tip = new StringBuilder();

        tip.append("<strong>");
        tip.append(port.getSpec());
        tip.append("</strong> (");
        tip.append(port.getName());
        tip.append(")<br/>");
        MetaData metaData = port.getMetaData();
        if (!MetaDataRendererFactoryRegistry.getInstance().hasRenderer(metaData)) {
            tip.append("<em>Meta data:</em> ");
            if (metaData != null) {
                if (metaData instanceof ExampleSetMetaData) {
                    tip.append(((ExampleSetMetaData) metaData).getShortDescription());
                } else {
                    tip.append(metaData.getDescription());
                }
                tip.append("<br>");
            } else {
                tip.append("-<br/>");
            }
        }
        IOObject data = model.getCachedResultOfPort(port);
        if (data != null) {
            tip.append("</br><em>Data:</em> ");
            tip.append(SwingTools.getShortenedDisplayName(data.toString(), MAX_DATA_LENGTH));
            tip.append("<br/>");
        }
        tip.append(port.getDescription());
        if (!port.getErrors().isEmpty()) {
            boolean hasErrors = false;
            boolean hasWarnings = false;
            for (MetaDataError error : port.getErrors()) {
                if (error.getSeverity() == Severity.ERROR) {
                    hasErrors = true;
                }
                if (error.getSeverity() == Severity.WARNING) {
                    hasWarnings = true;
                }
            }
            if (hasErrors) {
                tip.append("<br/><strong style=\"color:red\">");
                tip.append(port.getErrors().size());
                tip.append(" error(s):</strong>");
                for (MetaDataError error : port.getErrors()) {
                    if (error.getSeverity() == Severity.ERROR) {
                        tip.append("<br/> ");
                        tip.append(error.getMessage());
                    }
                }
            }
            if (hasWarnings) {
                tip.append("<br/><strong style=\"color:#FFA500\">");
                tip.append(port.getErrors().size());
                tip.append(" warnings(s):</strong>");
                for (MetaDataError error : port.getErrors()) {
                    if (error.getSeverity() == Severity.WARNING) {
                        tip.append("<br/> ");
                        tip.append(error.getMessage());
                    }
                }
            }
        }
        return tip.toString();
    }

    @SuppressWarnings("deprecation")
    @Override
    public Component getCustomComponent(final Object o) {
        Port hoveringPort = (Port) o;
        MetaData metaData = hoveringPort.getMetaData();
        return MetaDataRendererFactoryRegistry.getInstance().createRenderer(metaData);
    }
}
