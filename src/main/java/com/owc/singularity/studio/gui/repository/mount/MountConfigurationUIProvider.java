package com.owc.singularity.studio.gui.repository.mount;

import java.util.Map;

import com.owc.singularity.studio.gui.Form;

public interface MountConfigurationUIProvider {

    boolean supportsCreateUI();

    Form<Map<String, String>> getCreateUI(String mountType);

    boolean supportsEditUI();

    Form<Map<String, String>> getEditUI(String mountType, Map<String, String> options);

    boolean isMountTypeSupported(String mountType);
}
