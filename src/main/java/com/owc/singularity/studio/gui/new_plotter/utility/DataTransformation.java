/** 
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
*/
package com.owc.singularity.studio.gui.new_plotter.utility;


import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;

import com.owc.singularity.engine.object.data.exampleset.ExampleSet;
import com.owc.singularity.engine.operator.error.OperatorCreationException;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.pipeline.SubPipeline;
import com.owc.singularity.engine.pipeline.SubPipeline.SubPipelineExecutionContext;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.XMLException;
import com.owc.singularity.engine.tools.logging.LogService;


/**
 * This is a utility class which can transform {@link ExampleSet}s for various needs.
 * 
 * @author Marco Boeck
 * 
 */

public class DataTransformation {

    private static final String TO_REPLACE_WITH_DEPIVOT_ATTRIBUTE_LIST = "TO_REPLACE_WITH_DEPIVOT_ATTRIBUTE_LIST";
    private static final String TO_REPLACE_WITH_NOM_TO_NUM_ATTRIBUTE_LIST = "TO_REPLACE_WITH_NOM_TO_NUM_ATTRIBUTE_LIST";
    private static final String INVERT_NOM_TO_NUM_SELECTION = "INVERT_NOM_TO_NUM_SELECTION";

    /**
     * Creates a de-pivotized meta data {@link ExampleSet} from a given {@link ExampleSet}. This set
     * de-pivots the given numerical attributes.
     * 
     * @param exampleSet
     *            the original {@link ExampleSet}
     * @param listOfNumericalAttributes
     *            list with the names of the numerical attributes to de-pivot
     * @param selectedNomToNumericAttributesList
     *            a list of nominal attributes that should be converted to be numerical afterwards
     * @return the meta data {@link ExampleSet} or {@code null} if there was an error/empty
     *         attribute list
     * @throws IOException
     *             thrown if the transformation process cannot be read
     */
    public static ExampleSet createDePivotizedExampleSet(ExampleSet exampleSet, List<String> listOfNumericalAttributes) {
        return createDePivotizedExampleSet(exampleSet, listOfNumericalAttributes, null);
    }

    /**
     * Creates a de-pivotized meta data {@link ExampleSet} from a given {@link ExampleSet}. This set
     * de-pivots the given numerical attributes.
     * 
     * @param exampleSet
     *            the original {@link ExampleSet}
     * @param listOfNumericalAttributes
     *            list with the names of the numerical attributes to de-pivot
     * @param selectedNomToNumericAttributesList
     *            a list of nominal attributes that should be transformed to numerical attributes
     *            before depivotization
     * @return the meta data {@link ExampleSet} or {@code null} if there was an error/empty
     *         attribute list
     * @throws IOException
     *             thrown if the transformation process cannot be read
     */
    public static ExampleSet createDePivotizedExampleSet(ExampleSet exampleSet, List<String> listOfNumericalAttributes,
            Collection<String> selectedNomToNumericAttributesList) {
        if (exampleSet == null) {
            throw new IllegalArgumentException("exampleSet must not be null!");
        }
        if (listOfNumericalAttributes == null) {
            throw new IllegalArgumentException("listOfNumericalAttributes must not be null!");
        }
        if (listOfNumericalAttributes.size() == 0) {
            return null;
        }

        listOfNumericalAttributes.remove("id");
        try {
            // TODO: check if class still needed or implement the method correctly.
            // The mentioned resource does not exists anymore
            InputStream is = DataTransformation.class.getResourceAsStream("/com/owc/singularity/resources/processes/TransformationDepivot.rmp");
            String transformProcessXML = Tools.readTextFile(is);

            StringBuffer defaultValueBuffer = new StringBuffer();
            // modify NominalToNumerical to change only nominal values that have been selected
            if (selectedNomToNumericAttributesList != null) {
                selectedNomToNumericAttributesList.remove("id");
                for (String attName : selectedNomToNumericAttributesList) {
                    defaultValueBuffer.append(Pattern.quote(attName));
                    defaultValueBuffer.append("|");
                }
            }
            String nominalToNumericalValueString;
            String invertNomToNumSelection;
            if (selectedNomToNumericAttributesList == null || selectedNomToNumericAttributesList.isEmpty()) {
                nominalToNumericalValueString = ".*";
                invertNomToNumSelection = "true";
            } else {
                // remove last '|' so length -1
                nominalToNumericalValueString = defaultValueBuffer.substring(0, defaultValueBuffer.length() - 1);
                invertNomToNumSelection = "false";
            }
            transformProcessXML = transformProcessXML.replace(TO_REPLACE_WITH_NOM_TO_NUM_ATTRIBUTE_LIST, nominalToNumericalValueString);
            transformProcessXML = transformProcessXML.replace(INVERT_NOM_TO_NUM_SELECTION, invertNomToNumSelection);

            // modify de-pivot to only de-pivot given list of numerical attributes
            for (String attName : listOfNumericalAttributes) {
                defaultValueBuffer.append(Pattern.quote(attName));
                defaultValueBuffer.append("|");
            }
            // remove last '|' so length -1
            String numericalValuesString;
            numericalValuesString = defaultValueBuffer.substring(0, defaultValueBuffer.length() - 1);
            transformProcessXML = transformProcessXML.replace(TO_REPLACE_WITH_DEPIVOT_ATTRIBUTE_LIST, numericalValuesString);
            SubPipeline transformProcess = (SubPipeline) XMLImporter.parse(transformProcessXML);

            // disable ID generation if ID already exists
            if (exampleSet.getAttributes().getId() != null) {
                transformProcess.getOperator("Generate ID").setEnabled(false);
                transformProcess.getOperator("idToNumerical").setEnabled(false);
            }

            SubPipelineExecutionContext context = new SubPipelineExecutionContext();
            context.logLevel = Level.OFF;
            context.inputs = Collections.singletonList(exampleSet);


            return (ExampleSet) transformProcess.run(context, true).get(0);
        } catch (XMLException | OperatorException | OperatorCreationException | IOException e) {
            LogService.getRoot()
                    .warn("com.owc.singularity.studio.gui.new_plotter.utility.DataTransformation.creating_metainformationdepivotized_transformation_error", e);
        }

        // we only arrive here in case of error, return null
        return null;
    }

}
