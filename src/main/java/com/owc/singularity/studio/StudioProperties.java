/*

 *

 *

 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software Foundation, either version 3
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License along with this program.
 *  If not, see http://www.gnu.org/licenses/.
 */

package com.owc.singularity.studio;

import java.awt.*;
import java.io.*;
import java.util.Properties;

import javax.swing.JFrame;

import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.pipeline.parameter.*;
import com.owc.singularity.engine.tools.FontTools;
import com.owc.singularity.engine.tools.SystemInfoUtilities;
import com.owc.singularity.engine.tools.SystemInfoUtilities.OperatingSystem;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.logging.LogViewPanel;
import com.owc.singularity.studio.gui.osx.OSXAdapter;
import com.owc.singularity.studio.gui.plotter.PlotterPanel;
import com.owc.singularity.studio.gui.tools.dialogs.DecisionRememberingConfirmDialog;

/**
 * @author Hatem Hamad
 */
public class StudioProperties {

    public static final String PROPERTY_GEOMETRY_X = "singularity.gui.geometry.x";
    public static final String PROPERTY_GEOMETRY_Y = "singularity.gui.geometry.y";
    public static final String PROPERTY_GEOMETRY_EXTENDED_STATE = "singularity.gui.geometry.extendedstate";
    public static final String PROPERTY_GEOMETRY_WIDTH = "singularity.gui.geometry.width";
    public static final String PROPERTY_GEOMETRY_HEIGHT = "singularity.gui.geometry.height";
    public static final String PROPERTY_GEOMETRY_DIVIDER_MAIN = "singularity.gui.geometry.divider.main";
    public static final String PROPERTY_GEOMETRY_DIVIDER_EDITOR = "singularity.gui.geometry.divider.editor";
    public static final String PROPERTY_GEOMETRY_DIVIDER_LOGGING = "singularity.gui.geometry.divider.logging";
    public static final String PROPERTY_GEOMETRY_DIVIDER_GROUPSELECTION = "singularity.gui.geometry.divider.groupselection";
    public static final String PROPERTY_SHOW_PARAMETER_HELP = "singularity.gui.show_parameter_help";
    public static final String PROPERTY_SINGULARITY_GUI_MAX_SORTABLE_ROWS = "singularity.gui.max_sortable_rows";
    public static final String PROPERTY_SINGULARITY_GUI_MAX_DISPLAYED_VALUES = "singularity.gui.max_displayed_values";
    public static final String PROPERTY_SINGULARITY_GUI_SNAP_TO_GRID = "singularity.gui.snap_to_grid";
    /**
     * Instead of possibly overlapping operators the connected operators to the right should be
     * moved when moving an operator.
     *
     * @since 9.2
     */
    public static final String PROPERTY_SINGULARITY_GUI_MOVE_CONNECTED_OPERATORS = "singularity.gui.move_connected_operators";
    /**
     * The property name for &quot;Maximum number of states in the undo list.&quot;
     *
     * @since 7.5
     */
    public static final String PROPERTY_SINGULARITY_GUI_UNDOLIST_SIZE = "singularity.gui.undolist.size";
    public static final String PROPERTY_SINGULARITY_GUI_RECENT_PIPELINES_SIZE = "singularity.gui.recent_pipelines.size";
    public static final String PROPERTY_SINGULARITY_GUI_RESTORE_LAST_SESSION_TABS = "singularity.init.restore_session_tabs";

    public static final String PROPERTY_SINGULARITY_GUI_REPOSITORY_SCAN_MAX_DIRECTORY_LEVELS = "singularity.gui.repository.scan.max_directory_levels";
    public static final String PROPERTY_RESOLVE_RELATIVE_REPOSITORY_LOCATIONS = "singularity.gui.resolve_relative_repository_locations";
    public static final String PROPERTY_CLOSE_RESULTS_BEFORE_RUN = "singularity.gui.close_results_before_run";
    public static final String PROPERTY_ADD_BREAKPOINT_RESULTS_TO_HISTORY = "singularity.gui.add_breakpoint_results_to_history";
    public static final String PROPERTY_CONFIRM_EXIT = "singularity.gui.confirm_exit";
    public static final String PROPERTY_CLOSE_ALL_RESULTS_NOW = "singularity.gui.close_all_results_without_confirmation";
    public static final String PROPERTY_FONT_CONFIG = "singularity.gui.font_config";
    public static final String PROPERTY_DRAG_TARGET_HIGHLIGHTING = "singularity.gui.drag_target_highlighting";

    // Admin Settings
    public static final String PROPERTY_SINGULARITY_DISALLOW_REMEMBER_PASSWORD = "singularity.disallow.remember.password";
    // different behavior for disconnecting deleted and disabled operators
    public static final String PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR = "singularity.gui.disable_op_conn_behavior";
    public static final String PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR = "singularity.gui.delete_op_conn_behavior";

    /** The property name for &quot;The pixel size of each plot in matrix plots.&quot; */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_MATRIXPLOT_SIZE = "singularity.gui.plotter.matrixplot.size";
    /**
     * The property name for &quot;The maximum number of rows used for a plotter, using only a
     * sample of this size if more rows are available.&quot;
     */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_ROWS_MAXIMUM = "singularity.gui.plotter.rows.maximum";
    /**
     * The property name for the &quot; The maximum number of examples in a data set for which
     * default plotter settings will be generated.&quot;
     */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_DEFAULT_MAXIMUM = "singularity.gui.plotter.default.maximum";
    /**
     * The property name for &quot;Limit number of displayed classes plotter legends. -1 for no
     * limit.&quot;
     */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_CLASSLIMIT = "singularity.gui.plotter.legend.classlimit";
    /** The property name for &quot;The color for minimum values of the plotter legend.&quot; */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_MINCOLOR = "singularity.gui.plotter.legend.mincolor";
    /** The property name for &quot;The color for maximum values of the plotter legend.&quot; */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_MAXCOLOR = "singularity.gui.plotter.legend.maxcolor";
    /**
     * The property name for &quot;Limit number of displayed classes for colorized plots. -1 for no
     * limit.&quot;
     */
    public static final String PROPERTY_SINGULARITY_GUI_PLOTTER_COLORS_CLASSLIMIT = "singularity.gui.plotter.colors.classlimit";
    /**
     * The property name for &quot;Limit number of displayed rows in the message viewer. -1 for no
     * limit.&quot;
     */
    public static final String PROPERTY_SINGULARITY_GUI_LOGVIEWER_ROWLIMIT = "singularity.gui.messageviewer.rowlimit";
    public static final String PROPERTY_SINGULARITY_GUI_SAVE_BEFORE_RUN = "singularity.gui.save_before_run";

    /**
     * The property determining whether or not to switch to result view when results are produced.
     */
    public static final String PROPERTY_SINGULARITY_GUI_AUTO_SWITCH_TO_RESULTVIEW = "singularity.gui.auto_switch_to_resultview";

    /**
     * The name of the property indicating the path to the global logging directory.
     */
    public static final String PROPERTY_SINGULARITY_STUDIO_LOG_DIRECTORY = "singularity.studio.logging.directory";

    /**
     * The maximum number of archived log files to keep. Log files will roll over for each day or
     * when the exceed 100 MB creating a new log file archive
     */
    public static final String PROPERTY_SINGULARITY_STUDIO_LOG_ARCHIVE_MAX_KEPT_FILE_COUNT = "singularity.studio.logging.archive.max_kept_file_count";
    /** Log level of the LoggingViewer. */
    public static final String PROPERTY_SINGULARITY_STUDIO_LOG_LEVEL = "singularity.studio.logging.level";

    public static void registerGUIConfigurations() {
        // GUI Parameters

        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_MAX_SORTABLE_ROWS, "", 1, Integer.MAX_VALUE, 10000000));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_SINGULARITY_GUI_SNAP_TO_GRID, "", true));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_SINGULARITY_GUI_MOVE_CONNECTED_OPERATORS, "", true));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_RESOLVE_RELATIVE_REPOSITORY_LOCATIONS, "", true));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_REPOSITORY_SCAN_MAX_DIRECTORY_LEVELS, "", 1, 10, 2));
        // PropertyService.registerParameter(new
        // ParameterTypeCategory(PROPERTY_CLOSE_RESULTS_BEFORE_RUN, "",
        // DecisionRememberingConfirmDialog.PROPERTY_VALUES,
        // DecisionRememberingConfirmDialog.TRUE));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_ADD_BREAKPOINT_RESULTS_TO_HISTORY, "", false));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_CONFIRM_EXIT, "", false));
        PropertyService.registerParameter(new ParameterTypeCategory(PROPERTY_CLOSE_ALL_RESULTS_NOW, "", DecisionRememberingConfirmDialog.PROPERTY_VALUES,
                DecisionRememberingConfirmDialog.ASK));
        PropertyService.registerParameter(new ParameterTypeCategory(PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR, "",
                StudioProperties.PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR_VALUES,
                StudioProperties.PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR_DEFAULT_VALUE));
        PropertyService.registerParameter(new ParameterTypeCategory(PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR, "",
                StudioProperties.PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR_VALUES,
                StudioProperties.PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR_DEFAULT_VALUE));
        PropertyService
                .registerParameter(new ParameterTypeCategory(PROPERTY_FONT_CONFIG, "", FontTools.getAvailableFonts(), FontTools.OPTION_INDEX_STANDARD_FONTS));
        ParameterType parameterType = new ParameterTypeCategory(PROPERTY_DRAG_TARGET_HIGHLIGHTING, "",
                StudioProperties.PROPERTY_DRAG_TARGET_HIGHLIGHTING_VALUES, StudioProperties.DRAG_TARGET_HIGHLIGHTING_FULL);
        parameterType.setHidden(true);
        PropertyService.registerParameter(parameterType);

        // GUI Parameters MainFrame

        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_PLOTTER_MATRIXPLOT_SIZE, "", 1, Integer.MAX_VALUE, 200));
        PropertyService.registerParameter(
                new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_PLOTTER_ROWS_MAXIMUM, "", 1, Integer.MAX_VALUE, PlotterPanel.DEFAULT_MAX_NUMBER_OF_DATA_POINTS));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_PLOTTER_DEFAULT_MAXIMUM, "", -1, Integer.MAX_VALUE, 100000));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_CLASSLIMIT, "", -1, Integer.MAX_VALUE, 10));
        PropertyService.registerParameter(new ParameterTypeColor(PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_MINCOLOR, "", Color.blue));
        PropertyService.registerParameter(new ParameterTypeColor(PROPERTY_SINGULARITY_GUI_PLOTTER_LEGEND_MAXCOLOR, "", Color.red));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_PLOTTER_COLORS_CLASSLIMIT, "", -1, Integer.MAX_VALUE, 10));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_UNDOLIST_SIZE, "", 1, Integer.MAX_VALUE, 100));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_RECENT_PIPELINES_SIZE, "", 1, Integer.MAX_VALUE, 10));
        PropertyService.registerParameter(new ParameterTypeBoolean(PROPERTY_SINGULARITY_GUI_RESTORE_LAST_SESSION_TABS, "", true));

        PropertyService.registerParameter(
                new ParameterTypeInt(PROPERTY_SINGULARITY_GUI_LOGVIEWER_ROWLIMIT, "", -1, Integer.MAX_VALUE, LogViewPanel.DEFAULT_NUMBER_OF_VISIBLE_LINES));
        PropertyService.registerParameter(new ParameterTypeCategory(PROPERTY_SINGULARITY_GUI_SAVE_BEFORE_RUN, "",
                DecisionRememberingConfirmDialog.PROPERTY_VALUES, DecisionRememberingConfirmDialog.FALSE));
        PropertyService.registerParameter(new ParameterTypeCategory(PROPERTY_SINGULARITY_GUI_AUTO_SWITCH_TO_RESULTVIEW, "",
                DecisionRememberingConfirmDialog.PROPERTY_VALUES, DecisionRememberingConfirmDialog.TRUE));
        PropertyService.registerParameter(
                new ParameterTypeCategory(PROPERTY_SINGULARITY_STUDIO_LOG_LEVEL, "", LogService.SELECTABLE_LEVEL_NAMES, LogService.DEFAULT_LEVEL_INDEX));
        PropertyService.registerParameter(new ParameterTypeDirectory(PROPERTY_SINGULARITY_STUDIO_LOG_DIRECTORY, "",
                FileSystemService.getUserSingularityDirectory().toPath().resolve("logs").toString()));
        PropertyService.registerParameter(new ParameterTypeInt(PROPERTY_SINGULARITY_STUDIO_LOG_ARCHIVE_MAX_KEPT_FILE_COUNT, "", 1, Integer.MAX_VALUE, 30));
    }

    public static void saveGUIProperties() {
        Properties properties = new Properties();
        MainFrame mainFrame = MainFrame.INSTANCE;
        if (mainFrame != null) {
            properties.setProperty(PROPERTY_GEOMETRY_X, "" + (int) mainFrame.getLocation().getX());
            properties.setProperty(PROPERTY_GEOMETRY_Y, "" + (int) mainFrame.getLocation().getY());
            properties.setProperty(PROPERTY_GEOMETRY_WIDTH, "" + mainFrame.getWidth());
            properties.setProperty(PROPERTY_GEOMETRY_HEIGHT, "" + mainFrame.getHeight());
            properties.setProperty(PROPERTY_GEOMETRY_EXTENDED_STATE, "" + mainFrame.getExtendedState());
            properties.setProperty(PROPERTY_SHOW_PARAMETER_HELP, "" + mainFrame.getOperatorPropertyPanel().isShowParameterHelp());
            File file = FileSystemService.getUserConfigFile("gui.properties");
            try (OutputStream out = new FileOutputStream(file);) {
                properties.store(out, "SingularityEngine Studio GUI properties");
            } catch (IOException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.writing_gui_properties_error", e.getMessage(), e);
            }
            mainFrame.getResultDisplay().clearAll();
            // mainFrame.getApplicationPerspectiveController().saveAll();
            mainFrame.getPerspectiveController().saveAll();
        }
    }

    public static void loadGUIProperties(final MainFrame mainFrame) {
        Properties properties = new Properties();
        File file = FileSystemService.getUserConfigFile("gui.properties");
        if (file.exists()) {
            InputStream in = null;
            try {
                in = new FileInputStream(file);
                properties.load(in);
            } catch (IOException e) {
                StudioProperties.setDefaultGUIProperties();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e) {
                    throw new Error(e); // should not occur
                }
            }
            try {
                int x = Integer.parseInt(properties.getProperty(PROPERTY_GEOMETRY_X));
                int y = Integer.parseInt(properties.getProperty(PROPERTY_GEOMETRY_Y));
                mainFrame.setLocation(x, y);
                int extendedState;
                if (properties.getProperty(PROPERTY_GEOMETRY_EXTENDED_STATE) == null) {
                    extendedState = Frame.NORMAL;
                } else {
                    extendedState = Integer.parseInt(properties.getProperty(PROPERTY_GEOMETRY_EXTENDED_STATE));
                }
                mainFrame.setExtendedState(extendedState);
                if ((extendedState & JFrame.MAXIMIZED_BOTH) != JFrame.MAXIMIZED_BOTH) {
                    int width = Integer.parseInt(properties.getProperty(PROPERTY_GEOMETRY_WIDTH));
                    int height = Integer.parseInt(properties.getProperty(PROPERTY_GEOMETRY_HEIGHT));
                    if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
                        // OS X likes to set up size 0px * 0px if height exceeds available height,
                        // so make sure it does not
                        GraphicsConfiguration graphicsConfig = mainFrame.getGraphicsConfiguration();
                        if (graphicsConfig != null) {
                            Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(graphicsConfig);
                            int xInsets = insets.left + insets.right;
                            // random further -15px because a precise calculation does still produce
                            // above 0px error
                            int yInsets = insets.top + insets.bottom + 15;
                            int availableWidth = graphicsConfig.getDevice().getDisplayMode().getWidth() - xInsets;
                            int availableHeight = graphicsConfig.getDevice().getDisplayMode().getHeight() - yInsets;
                            width = Math.min(width, availableWidth - x);
                            height = Math.min(height, availableHeight - y);
                        }
                    }
                    mainFrame.setSize(new Dimension(width, height));
                }

                // If the property is not set we want the parameter help to be shown
                String showHelpProperty = properties.getProperty(PROPERTY_SHOW_PARAMETER_HELP);
                if (showHelpProperty == null || showHelpProperty.isEmpty()) {
                    mainFrame.getOperatorPropertyPanel().setShowParameterHelp(true);
                } else {
                    mainFrame.getOperatorPropertyPanel().setShowParameterHelp(Boolean.parseBoolean(showHelpProperty));
                }
                if (SystemInfoUtilities.getOperatingSystem() == OperatingSystem.OSX) {
                    OSXAdapter.checkForFullScreen(mainFrame);
                }
            } catch (NumberFormatException e) {
                StudioProperties.setDefaultGUIProperties();
            }
        } else {
            StudioProperties.setDefaultGUIProperties();
        }
    }

    /**
     * This method sets some default GUI properties. This method can be invoked if the properties
     * file was not found or produced any error messages (which might happen after version changes).
     */
    public static void setDefaultGUIProperties() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        MainFrame.INSTANCE.setLocation((int) (0.05d * screenSize.getWidth()), (int) (0.05d * screenSize.getHeight()));
        MainFrame.INSTANCE.setSize((int) (0.9d * screenSize.getWidth()), (int) (0.9d * screenSize.getHeight()));
    }

    public static final int PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR_DEFAULT_VALUE = 1;
    public static final int PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR_DEFAULT_VALUE = 1;
    public static final String[] PROPERTY_DELETE_OPERATOR_CONNECTION_BEHAVIOR_VALUES = { "removed", "bridged" };
    public static final String[] PROPERTY_DISABLE_OPERATOR_CONNECTION_BEHAVIOR_VALUES = { "removed", "bridged", "kept" };
    public static final int DRAG_TARGET_HIGHLIGHTING_FULL = 0;
    public static final String[] PROPERTY_DRAG_TARGET_HIGHLIGHTING_VALUES = { "full", "border", "none" };
    public static final String[] PROPERTY_TRANSFER_USAGESTATS_ANSWERS = { "ask", "always", "never" };
    // GUI flag key for caching
    public static final String IS_GUI_PROCESS = "com.owc.singularity.studio.gui.isGUIProcess";
}
