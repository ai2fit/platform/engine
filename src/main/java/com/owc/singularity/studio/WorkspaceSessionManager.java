package com.owc.singularity.studio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import com.owc.singularity.engine.FileSystemService;
import com.owc.singularity.engine.PropertyService;
import com.owc.singularity.engine.pipeline.AbstractPipeline;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.repository.RepositoryPath;
import com.owc.singularity.repository.entry.Entry;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.singularity.studio.gui.editor.pipeline.ProcessPanel;
import com.owc.singularity.studio.gui.tools.SwingTools;

/**
 * This class handles the persistence of currently open pipeline tabs and their repository locations
 * (by their repository location).
 * 
 * @author Alexander Mahler
 *
 */
public class WorkspaceSessionManager {


    public static boolean saveCurrentSession(List<ProcessPanel> processPanels) {
        if (isSessionFeatureEnabled()) {
            List<RepositoryPath> openAndSavedPanels = new LinkedList<>();
            for (ProcessPanel component : processPanels) {
                AbstractPipeline pipeline = component.getProcess();
                if (pipeline != null) {
                    RepositoryPath path = pipeline.getPath();
                    if (path != null) {
                        openAndSavedPanels.add(path);
                    }
                }
            }

            File file = FileSystemService.getUserConfigFile("session");
            try (FileWriter fw = new FileWriter(file); PrintWriter out = new PrintWriter(fw)) {
                for (RepositoryPath loc : openAndSavedPanels) {
                    if (Files.exists(loc) && Files.isRegularFile(loc))
                        out.println(loc);
                }
            } catch (IOException e) {
                SwingTools.showSimpleErrorMessage("cannot_write_session_file", e);
                return false;
            }
            return true;
        }
        return false;
    }

    public static void loadLastSession() {
        if (isSessionFeatureEnabled()) {
            File file = FileSystemService.getUserConfigFile("session");
            if (!file.exists()) {
                return;
            }
            try {
                for (String line : Files.readAllLines(file.toPath())) {
                    try {
                        RepositoryPath path = RepositoryPath.of(line);
                        if (Files.exists(path) && Files.isRegularFile(path))
                            MainFrame.INSTANCE.openProcess(new Entry(RepositoryPath.of(line)));
                    } catch (IllegalArgumentException e) {
                        LogService.getRoot().warn("com.owc.singularity.studio.gui.unparseable_line", "session", line);
                    }
                }
            } catch (IOException e) {
                LogService.getRoot().warn("com.owc.singularity.studio.gui.reading_session_file_error", e);
            }
        }
    }

    public static boolean isSessionFeatureEnabled() {
        String parameterValue = PropertyService.getParameterValue(StudioProperties.PROPERTY_SINGULARITY_GUI_RESTORE_LAST_SESSION_TABS);
        return Boolean.parseBoolean(parameterValue);
    }
}
