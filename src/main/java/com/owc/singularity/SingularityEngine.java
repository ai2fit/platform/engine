/*

 *

 *

 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/.
 */
package com.owc.singularity;


import java.io.*;
import java.lang.management.ManagementFactory;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.owc.singularity.engine.*;
import com.owc.singularity.engine.concurrency.ConcurrentExecutionService;
import com.owc.singularity.engine.concurrency.ConcurrentExecutionServiceProvider;
import com.owc.singularity.engine.i18n.LocalI18NResourcesProvider;
import com.owc.singularity.engine.modules.ModuleService;
import com.owc.singularity.engine.pipeline.io.XMLImporter;
import com.owc.singularity.engine.service.network.ProxyConfigurationService;
import com.owc.singularity.engine.service.repository.RepositoryConfigurator;
import com.owc.singularity.engine.tools.GlobalAuthenticator;
import com.owc.singularity.engine.tools.I18N;
import com.owc.singularity.engine.tools.PlatformUtilities;
import com.owc.singularity.engine.tools.Tools;
import com.owc.singularity.engine.tools.cipher.CipherTools;
import com.owc.singularity.engine.tools.cipher.KeyGenerationException;
import com.owc.singularity.engine.tools.cipher.KeyGeneratorTool;
import com.owc.singularity.engine.tools.logging.LogService;
import com.owc.singularity.engine.tools.update.internal.MigrationManager;
import com.owc.singularity.repository.RepositoryManager;
import com.owc.singularity.repository.connection.DefaultConnectionDefinitionRegistry;
import com.owc.singularity.tools.VersionNumber;


/**
 * Main program. Entry point for command line program, GUI and wrappers. Please note that
 * applications which use SingularityEngine as a data mining library will have to invoke one of the
 * init methods provided by this class before applying processes or operators. Several init methods
 * exist and choosing the correct one with optimal parameters might drastically reduce runtime and /
 * or initialization time.
 *
 * @implNote (extensions-breaker) <a href=
 *           "https://gitlab.oldworldcomputing.com/singularity/engine/-/wikis/Extensions-Breaker">read
 *           more</a>
 *
 * @author Ingo Mierswa, Adrian Wilke
 */
public class SingularityEngine {

    private static Logger log;
    private static EngineExecutionMode executionMode = EngineExecutionMode.UNKNOWN;
    private static final VersionNumber VERSION = new VersionNumber(PlatformUtilities.getReleaseVersion());


    public static final String PROCESS_FILE_EXTENSION = "singularity-flow";


    /**
     * The name of the property indicating the path to a additional operator description XML
     * file(s). If more than one, then the files have to be separated using the File.pathSeparator
     * character.
     */
    public static final String PROPERTY_AI2FIT_OPERATORS_ADDITIONAL = "singularity.operators.additional";
    /**
     * The name of the property indicating the path to additional ioobjects description XML file(s).
     * If more than one, then the files have to be separated using the File.pathSeparator character.
     */
    public static final String PROPERTY_AI2FIT_OBJECTS_ADDITIONAL = "singularity.objects.additional";

    /**
     * A file path to an operator description XML file.
     */
    public static final String PROPERTY_AI2FIT_INIT_OPERATORS = "singularity.init.operators";

    /**
     * Enables special features for developers: Validate process action, operator doc editor, etc.
     */
    public static final String PROPERTY_DEVELOPER_MODE = "singularity.developermode";

    private static final List<Runnable> shutdownHooks = new LinkedList<>();

    private static final List<Runnable> startupHooks = new LinkedList<>();

    private static boolean isInitiated = false;

    private static boolean performedInitialSettings = false;
    private static boolean isInitialized = false;

    public static String getShortVersion() {
        return VERSION.getShortVersion();
    }

    public static String getLongVersion() {
        return VERSION.getLongVersion();
    }

    public static VersionNumber getVersion() {
        return VERSION;
    }

    public static String getApplicationTitle() {
        return "AI2FIT Studio";
    }

    /**
     * This initializes the engine with the given concurrency execution service to use.
     * 
     * @param concurrencyService
     *            used service
     */
    public static void init(ConcurrentExecutionService concurrencyService) {
        if (!isInitialized) {
            ConcurrentExecutionServiceProvider.setConcurrencyExecutionService(concurrencyService);
            init();
        }
    }

    /**
     * Initializes SingularityEngine. During initialization, the following system properties are
     * evaluated. All can be specified in one of the SingularityEngine configuration files, by using
     * {@link System#setProperty(String, String)}, or by passing a <code>-Dkey=value</code> to the
     * Java executable.
     *
     * <ul>
     * <li>singularity.init.operators (file path)</li>
     * <li>singularity.init.plugins (true or false)</li>
     * <li>singularity.init.plugins.location (directory path)</li>
     * <li>singularity.init.weka (true or false)</li>
     * <li>singularity.init.jdbc.lib (true or false)</li>
     * <li>singularity.init.jdbc.lib.location (directory path)</li>
     * <li>singularity.init.jdbc.classpath (true or false)</li>
     * </ul>
     */
    public static synchronized void init() {
        if (!isInitialized) {
            isInitialized = true;
            long startTime = System.nanoTime();

            if (!I18N.isProviderAvailable()) {
                I18N.setProvider(new LocalI18NResourcesProvider());
            }

            log = LogService.getRoot();
            log.info("com.owc.singularity.engine.init.launch_start", SingularityEngine.getLongVersion(),
                    System.getProperty("java.vm.name") + " " + System.getProperty("java.vm.version"), ProcessHandle.current().pid(),
                    System.getProperty("user.name"), System.getProperty("user.dir"));

            // ensure singularity.home is set
            PlatformUtilities.ensureSingularityHomeSet();

            // check if this version is started for the first time
            performInitialSettings();
            PropertyService.init();

            // set version
            PropertyService.setParameterValue(EngineProperties.VERSION, SingularityEngine.getLongVersion());
            System.setProperty(EngineProperties.VERSION, SingularityEngine.getLongVersion());

            GlobalAuthenticator.initialize();

            // initializing networking tools
            ProxyConfigurationService.initialize();
            ProxyConfigurationService.apply();

            // generate encryption key if necessary
            if (!CipherTools.isKeyAvailable()) {
                try {
                    KeyGeneratorTool.createAndStoreKey();
                } catch (KeyGenerationException e) {
                    log.warn("com.owc.singularity.generating_encryption_key_error", e.getMessage(), e);
                }
            }

            // Base system should be ready here. Now load additional module classes
            boolean loadModules = Tools.booleanValue(PropertyService.getParameterValue(EngineProperties.MODULES_ENABLED), true);
            if (loadModules) {
                ModuleService.init();
            } else {
                log.warn("com.owc.singularity.engine.init.modules_disabled", EngineProperties.MODULES_ENABLED);
            }

            // The following now may access also modules

            // load importing of parse rules
            XMLImporter.init();

            // registering operators
            OperatorService.init();

            // init connection objects
            DefaultConnectionDefinitionRegistry.initializeAndRegisterGlobally();

            if (loadModules) {
                // now engine is initialized. Can load additional stuff from specific modules
                ModuleService.initModules();
            }

            // init repositories: configuration depends on CipherTools for passwords
            try {
                long repositoryLoadStartTime = System.nanoTime();
                log.debug("Loading repository...");
                try {
                    RepositoryManager.initialize(ModuleService.getMajorClassLoader(), ModuleService.getMajorReflections());
                } catch (IOException e) {
                    throw new IOException("Failed to initialize RepositoryManager", e);
                }
                RepositoryConfigurator.initialize();
                log.debug("Loading repository...DONE (took {})", Tools.formatDuration(Duration.ofNanos(System.nanoTime() - repositoryLoadStartTime)));
            } catch (IOException e) {
                log.error(e);
                stop(EngineExitMode.ERROR);
            }

            // finally execute startup hooks
            for (Runnable runnable : startupHooks) {
                try {
                    runnable.run();
                } catch (Exception e) {
                    log.warn("com.owc.singularity.engine.init.executing_startup_hook_error", e.getMessage(), e);
                }
            }

            // finished
            Duration timeTakenToStartup = Duration.ofNanos(System.nanoTime() - startTime);
            log.info("com.owc.singularity.engine.init.launch_finish", Tools.formatDuration(timeTakenToStartup),
                    Tools.formatDuration(ManagementFactory.getRuntimeMXBean().getUptime()));
            isInitiated = true;
        }
    }

    private static void performInitialSettings() {
        if (performedInitialSettings) {
            return;
        }

        VersionNumber lastVersionNumber = null;
        VersionNumber currentVersionNumber = getVersion();

        File lastVersionFile = new File(FileSystemService.getUserSingularityDirectory(), "lastversion");
        if (lastVersionFile.exists()) {
            try (BufferedReader in = new BufferedReader(new FileReader(lastVersionFile))) {
                String versionString = in.readLine();
                lastVersionNumber = new VersionNumber(versionString);
            } catch (IOException e) {
                log.warn("com.owc.singularity.reading_global_version_file_error", e);
            } catch (VersionNumber.VersionNumberException e) {
                log.warn("com.owc.singularity.parsing_global_version_file_error", e);
            }
        }

        boolean versionChanged = lastVersionNumber != null && !currentVersionNumber.equals(lastVersionNumber);

        if (versionChanged) {
            MigrationManager.doMigrate(lastVersionNumber, currentVersionNumber);
        }

        // write version file
        writeLastVersion(lastVersionFile);

        performedInitialSettings = true;
    }

    private static void writeLastVersion(final File versionFile) {
        try (FileWriter fw = new FileWriter(versionFile); PrintWriter out = new PrintWriter(fw)) {
            out.println(getLongVersion());
        } catch (IOException e) {
            log.warn("com.owc.singularity.writing_current_version_error", e);
        }
    }

    public synchronized static void addShutdownHook(final Runnable runnable) {
        shutdownHooks.add(runnable);
    }

    /**
     * Adds the given {@link Runnable} to the list of hooks which will be executed after initiation
     * of SingularityEngine. If SingularityEngine is already initiated the given {@link Runnable}
     * will be executed immediately.
     */
    public synchronized static void addStartupHook(final Runnable runnable) {
        if (isInitiated) {
            try {
                runnable.run();
            } catch (Exception e) {
                log.warn("com.owc.singularity.engine.init.executing_startup_hook_error", e.getMessage(), e);
            }
        }
        startupHooks.add(runnable);
    }

    public static boolean isInitialized() {
        return isInitiated;
    }

    public synchronized static void stop(final EngineExitMode exitMode) {
        for (Runnable hook : shutdownHooks) {
            try {
                hook.run();
            } catch (Throwable e) {
                // catching Throwable because this also accounts for things like
                // ExceptionInInitializerErrors
                log.warn("com.owc.singularity.executing_shotdown_hook_error", e.getMessage(), e);
            }
        }
        try {
            Runtime.getRuntime().runFinalization();
        } catch (Throwable e) {
            // catching Throwable because this also accounts for things like
            // ExceptionInInitializerErrors
            log.warn("com.owc.singularity.error_during_finalization", e.getMessage(), e);
        }
        // TODO ensure every thing is cleared and all services and providers have been stopped
        isInitiated = false;

        System.exit(exitMode.getExitCode());
    }

    public static EngineExecutionMode getExecutionMode() {
        return executionMode;
    }

    public static void setExecutionMode(final EngineExecutionMode executionMode) {
        SingularityEngine.executionMode = executionMode;
    }

}
