## 0.9.18 (2025-03-03)

No changes.

## 0.9.17 (2024-12-02)

### Bug fixes (1 change)

- Resolved problem with Stop Pipeline Graciously blocking ui [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/337)]:  
  This fix prevents the User Interface of Studio from ignoring a pipeline being finished by Stop Pipeline Graciously operator and maintining in a blocked state.

### Improvements (3 changes)

- Resolve "Add tags to pipelines" [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/338)]:  
  Renderer for tool tip in repository view now shows type, tags and description.
- Merge branch '757-fileobjects-should-show-content-in-result-view' into 'master' [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/339)]:  
  Renderer of file objects now shows first 100kb of data in text view.
- Improved About box [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/336)]:  
  About Box now shows sharp logo and a license statement

## 0.9.16 (2024-08-26)

### New features (3 changes)

- Add i18n labels to support the remove lock action from git mount [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/330)]
- Add Progress bar updates for when the repository tree gets expanded lazily [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/330)]
- Add support for configuring the max levels of scanned directories during repository tree scan [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/330)]:  
  The `singularity.gui.repository.scan.max_directory_levels` allows configuring the max levels of directories that will get queried when a tree scan starts, i.e., when directories get expanded

### Dependency changes (1 change)

- Bump repository version from `0.9.8` to `0.9.9` [[Merge request](https://gitlab.com/ai2fit/platform/engine/-/merge_requests/330)]
