<!--

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Description

## MR acceptance checklist

- [ ] Code is documented
- [ ] Code is linted
- [ ] Changes are tested
- [ ] Project/Usage documentation is updated
- [ ] Changed dependencies are documented
- [ ] Reviewer is assigned <!-- Project Maintainer(s): @sebastianLand -->
- [ ] Merge request commit message contain a correct [Changelog entry](https://gitlab.com/ai2fit/templates/description/-/wikis/Changelog#how-to-use-it)  

## Dependent Projects

Relevant projects that should be updated after the branch is merged into main

| Project                                                   | Change                                                       |
|-----------------------------------------------------------|--------------------------------------------------------------|
| [All Modules](https://gitlab.com/ai2fit/platform/modules) | (e.g. Should use `this_method()` instead of `that_method()`) |

## What issue(s) will be closed by this merge request?

/assign me
